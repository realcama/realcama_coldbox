
<CFSET jsRowNumber = rc.itemNumber - 1>
<cfsetting showdebugoutput="No">

<cfoutput>
<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading#jsRowNumber#"  aria-expanded="false" >
	<table class="land-line-summary">
	<tr class="section-bar-expander">
		<td width="20%" class="section-bar-label">###rc.itemNumber#</td>
		<td width="15%" class="section-bar-label">(Unsaved)</td>
		<td width="15%" class="section-bar-label"></td>
		<td width="25%" class="section-bar-label" colspan="2"></td>
		<td width="20%" class="section-bar-label"></td>
		<td width="5%" align="right" class="section-bar-label"></td>
	</tr>
	</table>
</div>

<div id="edit_mode_item_#rc.itemNumber#">
	<div class="row fix-bs-row">
		<div class="col-xs-4 databox">
			<span class="formatted-data-label">Permit Number</span><br/>
			<input type="text" name="permitNumber" id="permitNumber" value="" class="form-control" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Permit Amount</span><br/>
			<input type="text" name="amount" id="amount" value="" class="form-control numeric" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Date Issued</span><br/>
			<input type="text" name="dateIssued" id="dateIssued" value="" class="form-control datefield-permits" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-4 databox">
			<span class="formatted-data-label">Type</span><br/>
			<select name="permitCode" id="permitCode" class="form-control select2-2piece-new" data-tags="true" onchange="changeVal(#jsRowNumber#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.getAddPermitsData.getPermitTypes">
					<option value="#prc.getAddPermitsData.getPermitTypes.code#" data-val="#prc.getAddPermitsData.getPermitTypes.code#" data-desc="#prc.getAddPermitsData.getPermitTypes.PermitCodeDescription#">#prc.getAddPermitsData.getPermitTypes.code#: #prc.getAddPermitsData.getPermitTypes.PermitCodeDescription#</option>
				</cfloop>
			</select>
		</div>

		<div class="clearfix"></div>


		<div class="col-xs-3 databox">
			<span class="formatted-data-label">#prc.permitsInspectionDateLabel#</span><br/>
			<input type="text" name="FirstInspectionSchedDate" id="FirstInspectionSchedDate" value="" class="form-control datefield-permits" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">#prc.permitsNextDateLabel#</span><br/>
			<input type="text" name="InspectionSchedDate" id="InspectionSchedDate" value="" class="form-control datefield-permits" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">#prc.permitsFinalDateLabel#</span><br/>
			<input type="text" name="FinalInspectionDate" id="FinalInspectionDate" value="" class="form-control datefield-permits" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">#prc.permitsCODateLabel#</span><br/>
			<input type="text" name="ActualCompletionDate" id="ActualCompletionDate" value="" class="form-control datefield-permits" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="clearfix"></div>

		<div class="col-xs-6 databox">
			<span class="formatted-data-label">Issued By</span><br/>
			<select name="issuedBy" id="issuedBy" class="form-control select2-2piece-new" data-tags="true" onchange="changeVal(#jsRowNumber#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.getAddPermitsData.getIssuedBy">
					<option value="#prc.getAddPermitsData.getIssuedBy.field_Option#" data-val="#prc.getAddPermitsData.getIssuedBy.ID#" data-desc="#prc.getAddPermitsData.getIssuedBy.field_Option#">>#prc.getAddPermitsData.getIssuedBy.field_Option#</option>
				</cfloop>
			</select>
		</div>
		<div class="col-xs-6 databox">
			<span class="formatted-data-label">Issued To</span><br/>
			<select name="issuedTo" id="issuedTo" class="form-control select2-2piece-new" data-tags="true" onchange="changeVal(#jsRowNumber#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.getAddPermitsData.getIssuedTo">
					<option value="#prc.getAddPermitsData.getIssuedTo.field_Option#" data-val="#prc.getAddPermitsData.getIssuedTo.ID#" data-desc="#prc.getAddPermitsData.getIssuedTo.field_Option#">>#prc.getAddPermitsData.getIssuedTo.field_Option#</option>
				</cfloop>
			</select>
		</div>
		<div class="clearfix"></div>

<!---
		<div class="col-xs-12 databox-textbox">
			<span class="formatted-data-label">Notes</span><br/>
			<cfloop index="iPosition" from="1" to="3">
				<CFSET iNotesCurrentLine = iNotesCurrentLine + 1>
				<input type="text" name="note" data-rownumber="#iNotesCurrentLine-1#" value="" class="form-control" onchange="changeNotesVal(this)">
			</cfloop>
		</div>
		<div class="clearfix"></div>
--->
	</div>
	<div class="permits-part-action-buttons">
		<a href="javascript:cancelEverything()"><img src="/includes//images/v2/cancel.png" alt="Cancel" border="0"></a>
		&nbsp;&nbsp;
		<a href="javascript:saveWDDX('#rc.rowNum#')"><img src="/includes//images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
		&nbsp;&nbsp;
	</div>
	<br clear="all"/>
</div>

</cfoutput>