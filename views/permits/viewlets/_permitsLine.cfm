<cfoutput>

	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading#prc.getPermitsData.getPermitsInfo.currentrow#"  aria-expanded="false" >
		<table class="land-line-summary" border="0">
		<tr href="##collapse#prc.getPermitsData.getPermitsInfo.currentrow#" class="section-bar-expander" data-parent="heading#prc.getPermitsData.getPermitsInfo.currentrow#" data-toggle="collapse" aria-controls="collapse#prc.getPermitsData.getPermitsInfo.currentrow#" onclick="toggleChevronChild(#prc.getPermitsData.getPermitsInfo.currentrow#)">
			<td width="5%" class="section-bar-label">###prc.getPermitsData.getPermitsInfo.sequenceNumber#</td>
			<cfif mode EQ "delete">
				<td width="15%" class="section-bar-label section-bar-label-permits-deleted">DELETED</td>
				<td width="15%" class="section-bar-label">#prc.getPermitsData.getPermitsInfo.permitNumber#</td>
			<cfelse>
				<td width="30%" class="section-bar-label">#prc.getPermitsData.getPermitsInfo.permitNumber#</td>
			</cfif>
			<td width="15%" class="section-bar-label">#DateFormat(prc.getPermitsData.getPermitsInfo.DateIssued,"mm/dd/yyyy")#</td>
			<cfif Len(Trim(prc.getPermitsData.getPermitsInfo.permitCode)) NEQ 0>
				<td width="5%" class="section-bar-label text-right">#permitCode#:</td>
				<td width="20%" class="section-bar-label">#prc.getPermitsData.getPermitsInfo.PermitCodeDescription#</td>
			<cfelse>
				<td width="25%" class="section-bar-label" colspan="2"></td>
			</cfif>
			<td width="20%" class="section-bar-label text-right">$ #NumberFormat(prc.getPermitsData.getPermitsInfo.Amount,",")#</td>
			<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_#prc.getPermitsData.getPermitsInfo.currentrow#"></span></td>
		</tr>
		</table>
	</div>

	<div id="collapse#prc.getPermitsData.getPermitsInfo.currentrow#" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading#prc.getPermitsData.getPermitsInfo.currentrow#">
		<div id="view_mode_item_#prc.getPermitsData.getPermitsInfo.currentrow#" class="row fix-bs-row">
			<div class="row fix-bs-row">
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Permit Number</span><br/>
					<span class="formatted-data-values">#prc.getPermitsData.getPermitsInfo.permitNumber#</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Permit Amount</span><br/>
					<span class="formatted-data-values">#DollarFormat(prc.getPermitsData.getPermitsInfo.Amount)#</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Date Issued</span><br/>
					<span class="formatted-data-values">#DateFormat(prc.getPermitsData.getPermitsInfo.DateIssued,"mm/dd/yyyy")#</span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Type</span><br/>
					<span class="formatted-data-values"><cfif LEN(TRIM(prc.getPermitsData.getPermitsInfo.permitCode)) NEQ 0>#permitCode#: #prc.getPermitsData.getPermitsInfo.PermitCodeDescription#</cfif></span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsInspectionDateLabel#</span><br/>
					<span class="formatted-data-values">#DateFormat(prc.getPermitsData.getPermitsInfo.FirstInspectionSchedDate,"mm/dd/yyyy")#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsNextDateLabel#</span><br/>
					<span class="formatted-data-values">#DateFormat(prc.getPermitsData.getPermitsInfo.InspectionSchedDate,"mm/dd/yyyy")#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsFinalDateLabel#</span><br/>
					<span class="formatted-data-values">#DateFormat(prc.getPermitsData.getPermitsInfo.FinalInspectionDate,"mm/dd/yyyy")#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsCODateLabel#</span><br/>
					<span class="formatted-data-values">#DateFormat(prc.getPermitsData.getPermitsInfo.ActualCompletionDate,"mm/dd/yyyy")#</span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Issued By</span><br/>
					<span class="formatted-data-values">#prc.getPermitsData.getPermitsInfo.issuedBy#</span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Issued To</span><br/>
					<span class="formatted-data-values">#prc.getPermitsData.getPermitsInfo.issuedTo#</span>
				</div>
				<div class="clearfix"></div>

<!---
				<div class="col-xs-12 databox-textbox">
					<span class="formatted-data-label">Notes</span><br/>
					<cfloop index="iPosition" from="1" to="3">
						<CFSET iNotesCurrentLineDisplay = iNotesCurrentLineDisplay + 1>
						<div class="formatted-data-values">#qryNotesHolder.nonote[iNotesCurrentLineDisplay]#</div>
					</cfloop>

				</div>
--->
			</div>

			<div class="clearfix">&nbsp;</div>

			<div class="col-xs-12">
				<cfif mode EQ "delete">
					<div class="permits-delete-marker">
						PERMIT DELETED
					</div>
				</cfif>

				<div class="permits-part-action-buttons">
					<a href="javascript:editThisRow(#prc.getPermitsData.getPermitsInfo.currentrow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
				</div>
			</div>
		</div>

		<cfif mode EQ "delete">
			<CFSET strDisplayMode = "readonly">
		<cfelse>
			<CFSET strDisplayMode = "">
		</cfif>

		<div id="edit_mode_item_#prc.getPermitsData.getPermitsInfo.currentrow#" class="row fix-bs-row" style="display:none;">
			<div class="row fix-bs-row">
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Permit Number</span><br/>
					<input type="text" name="permitNumber" id="permitNumber" #strDisplayMode# value="#prc.getPermitsData.getPermitsInfo.permitNumber#" class="form-control permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Permit Amount</span><br/>
					<input type="text" name="amount" id="amount" #strDisplayMode# value="#prc.getPermitsData.getPermitsInfo.amount#" class="form-control permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Date Issued</span><br/>
					<input type="text" name="dateIssued" id="dateIssued" #strDisplayMode# value="#DateFormat(prc.getPermitsData.getPermitsInfo.DateIssued,"mm/dd/yyyy")#" class="form-control permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" >
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Type</span><br/>
					<select name="permitCode" id="permitCode" #strDisplayMode# class="form-control select2-2piece-new permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" data-tags="true" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getPermitsData.getPermitTypes">
							<option value="#prc.getPermitsData.getPermitTypes.code#" data-val="#prc.getPermitsData.getPermitTypes.code#" data-desc="#prc.getPermitsData.getPermitTypes.PermitCodeDescription#" <cfif prc.getPermitsData.getPermitsInfo.permitCode EQ prc.getPermitsData.getPermitTypes.code> selected</cfif>>#prc.getPermitsData.getPermitTypes.code#: #prc.getPermitsData.getPermitTypes.PermitCodeDescription#</option>
						</cfloop>
					</select>
				</div>

				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsInspectionDateLabel#</span><br/>
					<input type="text" name="FirstInspectionSchedDate" #strDisplayMode# id="FirstInspectionSchedDate" value="#DateFormat(prc.getPermitsData.getPermitsInfo.FirstInspectionSchedDate,"mm/dd/yyyy")#" class="form-control datefield-permits permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsNextDateLabel#</span><br/>
					<input type="text" name="InspectionSchedDate" #strDisplayMode# id="InspectionSchedDate" value="#DateFormat(prc.getPermitsData.getPermitsInfo.InspectionSchedDate,"mm/dd/yyyy")#" class="form-control datefield-permits permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsFinalDateLabel#</span><br/>
					<input type="text" name="FinalInspectionDate" #strDisplayMode# id="FinalInspectionDate" value="#DateFormat(prc.getPermitsData.getPermitsInfo.FinalInspectionDate,"mm/dd/yyyy")#" class="form-control datefield-permits permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">#prc.permitsCODateLabel#</span><br/>
					<input type="text" name="ActualCompletionDate" #strDisplayMode# id="ActualCompletionDate" value="#DateFormat(prc.getPermitsData.getPermitsInfo.ActualCompletionDate,"mm/dd/yyyy")#"  class="form-control datefield-permits permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" onChange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Issued By</span><br/>
					<select name="issuedBy" id="issuedBy" #strDisplayMode# class="form-control select2-2piece-new" data-tags="true" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getPermitsData.getIssuedBy">
							<option value="#prc.getPermitsData.getIssuedBy.field_Option#" data-val="#prc.getPermitsData.getIssuedBy.ID#" data-desc="#prc.getPermitsData.getIssuedBy.field_Option#" <cfif prc.getPermitsData.getPermitsInfo.issuedBy EQ prc.getPermitsData.getIssuedBy.field_Option> selected</cfif>>#prc.getPermitsData.getIssuedBy.field_Option#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Issued To</span><br/>
					<select name="issuedTo" id="issuedTo" #strDisplayMode# class="form-control select2-2piece-new" data-tags="true" onchange="changeVal(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getPermitsData.getIssuedTo">
							<option value="#prc.getPermitsData.getIssuedTo.field_Option#" data-val="#prc.getPermitsData.getIssuedTo.ID#" data-desc="#prc.getPermitsData.getIssuedTo.field_Option#" <cfif prc.getPermitsData.getPermitsInfo.issuedTo EQ prc.getPermitsData.getIssuedTo.field_Option> selected</cfif>>#prc.getPermitsData.getIssuedTo.field_Option#</option>
						</cfloop>
					</select>
				</div>
				<div class="clearfix"></div>

<!---
				<div class="col-xs-12 databox-textbox">
					<span class="formatted-data-label">Notes</span><br/>
					<cfloop index="iPosition" from="1" to="3">
						<CFSET iNotesCurrentLine = iNotesCurrentLine + 1>
						<input type="text" name="note" data-rownumber="#iNotesCurrentLine-1#" #strDisplayMode# value="#HTMLEditFormat(qryNotesHolder.nonote[iNotesCurrentLine])#" class="form-control permit_row_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" onchange="changeNotesVal(this)">
					</cfloop>
				</div>
--->
			</div>
			<div class="clearfix">&nbsp;</div>

			<div class="col-xs-3">
				<div class="permits-part-action-buttons">
					<cfif mode EQ "edit">
						<a id="deletePermit_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" href="javascript:deletePermit(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#)"><img src="/includes/images/v2/DeletePermit.png" alt="Delete Permit" border="0"></a>
					<cfelse>
						<a id="restorePermit_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" href="javascript:restoreDeletedPermit(#prc.getPermitsData.getPermitsInfo.CurrentRow-1#)"><img src="/includes/images/v2/restorePermit.png" alt="Restore Deleted Permit" border="1"></a>
					</cfif>
				</div>
			</div>
			<div class="col-xs-9">
<!---				<div id="permitState_delete_marker_#prc.getPermitsData.getPermitsInfo.CurrentRow-1#" class="permits-delete-marker" <cfif mode EQ "edit">style="display:none;"</cfif>>
					PERMIT DELETED
				</div> --->
				<div class="permits-part-action-buttons">
					<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
					&nbsp;&nbsp;
					<a href="javascript:saveWDDX(#TRIM(prc.getPermitsData.getPermitsInfo.itemNumber)#)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
				</div>
			</div>
		</div>
	</div>
</cfoutput>

<cfoutput>
	<script>
		var iNotesCurrentLine = #iNotesCurrentLine#;
	</script>
</cfoutput>