<cfsetting showdebugoutput="No">

<cfoutput>
<cfif NOT prc.getPermitsData.RecordCount>
	#addAsset('/includes/css/select2.css,/includes/js/select2.js,/includes/js/jquery.js,/includes/js/jquery.fancybox.js')#
	<script type="text/javascript">
		parent.$.fancybox.close();
	</script>
	<cfabort>
<cfelse>
	#addAsset('/includes/css/select2.css,/includes/js/select2.js,/includes/js/resequence.js')#
</cfif>

<form action="#event.buildLink('permits/permitResequenceSave')#" method="post" name="resequenceForm" id="resequenceForm">
<div id="popLandUDF" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-land-popupUDF">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Resequence Permit Items</h4>
			</div>
			<div class="modal-body" id="adjContainer">
				<div class="col-xs-12 formatted-data-label">*** Do you want any instruction text here *** Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<p></div>
				<div class="clearfix"></div>
				<div class="clearfix"></div>

				<div class="col-xs-4 formatted-data-label right">Current Permit Sequence List</div>
				<div class="col-xs-4 databox">
					<SELECT NAME="resequenceIDs" id="resequenceIDs" SIZE="7" MULTIPLE>
						<cfloop query="prc.getPermitsData">
							<OPTION VALUE="#permitID#">#permitID# - #permitNumber# - #PermitCodeDescription#<BR>
						</cfloop>
					</SELECT>
				</div>
				<div class="col-xs-4 databox">
					<button type="button"  class="btn btn-default"  onclick="moveOptionUp(this.form['resequenceIDs'])"><i class="glyphicon glyphicon-arrow-up"></i>&nbsp;&nbsp;Up&nbsp;&nbsp;&nbsp;</button>
					<BR><BR>
					<button type="button"  class="btn btn-default"  onclick="moveOptionDown(this.form['resequenceIDs'])"><i class="glyphicon glyphicon-arrow-down"></i>&nbsp;Down</button>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="modal-footer">
				<div id="modalPleaseWait" class="pull-left progress">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span>Please Wait</span></div>
				</div>
				<button id="addRowButton" class="btn btn-primary btn-add-option-udf" type="button" onclick="submitReseqForm()"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
				<button type="button"  class="btn btn-default"  onclick="parent.$.fancybox.close();"><i class="glyphicon glyphicon-ok-sign"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="parcelID" id="ParcelID" value="#rc.parcelID#">
<input type="hidden" name="parcelType" id="ParcelID" value="#rc.parcelType#">
<input type="hidden" name="taxYear" id="taxYear" value="#rc.taxYear#">
<input type="hidden" name="status" id="ParcelID" value="#rc.status#">
</form>

</cfoutput>