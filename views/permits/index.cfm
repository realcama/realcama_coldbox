<cfoutput>

	<cfif prc.getPermitsData.getPermitsInfo.recordcount EQ 0>
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				No permit records found for the selected parcel
			</div>
		</div>

	<cfelse>
		<cfset qryNotesHolder = QueryNew("noitem,nonote,noprop,noptyp,nopyr,norecno,noscrn,mode","integer,VarChar,VarChar,VarChar,VarChar,integer,VarChar,varchar")>

		<!---
		<cfloop query="prc.getPermitsInfo">
			<cfstoredproc procedure="p_GetNotes" datasource="#client.dsn#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ParcelID#" null="No">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="Yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="R" null="No">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="PRMT" null="No">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#qryRecords.cpitem#" null="No">
				<cfprocresult name="qryNotes">
			</cfstoredproc>

			<cfloop query="qryNotes">
				<cfset QueryAddRow(qryNotesHolder,1)>
				<cfset QuerySetCell(qryNotesHolder,"noitem",qryNotes.noitem)>
				<cfset QuerySetCell(qryNotesHolder,"nonote",qryNotes.nonote)>
				<cfset QuerySetCell(qryNotesHolder,"noprop",qryNotes.noprop)>
				<cfset QuerySetCell(qryNotesHolder,"noptyp",qryNotes.noptyp)>
				<cfset QuerySetCell(qryNotesHolder,"nopyr",qryNotes.nopyr)>
				<cfset QuerySetCell(qryNotesHolder,"norecno",qryNotes.norecno)>
				<cfset QuerySetCell(qryNotesHolder,"noscrn",qryNotes.noscrn)>
				<cfset QuerySetCell(qryNotesHolder,"mode","update")>
			</cfloop>

			<cfif qryNotes.RecordCount LT 3>
				<cfif qryNotes.RecordCount EQ 0>
					<cfset iStart = 1>
				<CFELSE>
					<cfset iStart = qryNotes.RecordCount + 1>
				</cfif>

				<cfloop index="iPosition" from="#iStart#" to="3">
					<cfset QueryAddRow(qryNotesHolder,1)>
					<cfset QuerySetCell(qryNotesHolder,"noprop",ParcelID)>
					<cfset QuerySetCell(qryNotesHolder,"noitem",qryRecords.cpitem)>
					<cfset QuerySetCell(qryNotesHolder,"norecno",iPosition)>
					<cfset QuerySetCell(qryNotesHolder,"noptyp","R")>
					<cfset QuerySetCell(qryNotesHolder,"noscrn","PRMT")>
					<cfset QuerySetCell(qryNotesHolder,"mode","insert")>
				</cfloop>
			</cfif>
		</cfloop>
--->
		<cfset iNotesCurrentLine = 0>
		<cfset iNotesCurrentLineDisplay = 0>
		<cfset iPageNumber = 0>

		<script>
			var taxYear = #prc.stPersistentInfo.taxYear#;
			var parcelID = "#prc.stPersistentInfo.parcelID#";

			var iCurrentPage = 1;

			<cfwddx action="CFML2JS"
					input="#prc.getPermitsData.getPermitsInfo#"
					toplevelvariable="permitsData">
<!---
			<cfwddx action="CFML2JS"
					input="#qryNotesHolder#"
					toplevelvariable="notesData">
--->

			function pageIncrement(position) {
				if((iCurrentPage == 1 && position == -1) || (iCurrentPage == #prc.iTotalPages# && position == 1 ))
				{
					/* Do Nothing */
				}
				else {
					for(i=1; i <= #prc.iTotalPages#; i++ ) {
						$("##page-" + i).hide();
					}
					iCurrentPage = iCurrentPage + position;
					$("##page-" + iCurrentPage).show();
					$("##current-page").html(iCurrentPage);
				}

				if(iCurrentPage == #prc.iTotalPages# && position == 1) {
					$("##permits-page-next").addClass("disabled");
				} else {
					$("##permits-page-next").removeClass("disabled");
				}

				if(iCurrentPage == 1 && position == -1) {
					$("##permits-page-prev").addClass("disabled");
				} else {
					$("##permits-page-prev").removeClass("disabled");
				}

			}
		</script>

		<form action="#event.buildLink('permits/permitSave')#" method="post" name="frmSave" id="frmSave">
			<input type="hidden" name="ParcelID" id="ParcelID" value="#prc.stPersistentInfo.parcelID#">
			<input type="hidden" name="taxYear" id="taxYear" value="#prc.stPersistentInfo.taxYear#">
			<input type="hidden" name="permitsData" id="permitsData" value="">
			<input type="hidden" name="notesData" id="notesData" value="">
		</form>
		<cfif Controller.getSetting( "bShowPacketLinks")>
			<div class="alert alert-info">
				<A href="javascript:void(0);" onclick="WriteRaw(permitsData);">Permit Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(notesData);">Notes Packet</A>&nbsp;&nbsp;&nbsp;
			</div>
		</cfif>

		#addAsset('/includes/js/header.js,/includes/js/permits.js,/includes/js/formatSelect.js')#

		<!--- BUTTON ROW --->
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				Permits <span id="totalPermiLines">(#prc.getPermitsData.getPermitsInfo.recordcount#)</span>
				<cfset showTabOptions = "permits">
				<cfoutput>#renderView('_templates/miniNavigationBar')#</cfoutput>
			</div>
			<cfset iPageNumber = 0>
			<div id="expander-holders" class="panel panel-default">
				<cfloop query="prc.getPermitsData.getPermitsInfo">

					<cfif prc.getPermitsData.getPermitsInfo.CurrentRow MOD prc.page_size EQ 1>
						<cfset iPageNumber = iPageNumber + 1>
						<div id="page-#iPageNumber#" <cfif iPageNumber NEQ 1>style="display:none;"</cfif>>
					</cfif>

					<cfoutput>#renderView('/permits/viewlets/_permitsLine')#</cfoutput>

					<cfif prc.getPermitsData.getPermitsInfo.CurrentRow MOD prc.page_size EQ 0>
						</div>
					</cfif>
				</cfloop>

				<cfif prc.getPermitsData.getPermitsInfo.CurrentRow MOD prc.page_size NEQ 0>
					</div> <!--- This ensures the ' div id="page-#iPageNumber#" ' to be closed correctly --->
				</cfif>


			</div> <!--- div id="expander-holders" class="panel panel-default" --->
		</div>
	</cfif>

</cfoutput>