
<cfoutput>

<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		<CFSET showTabOptions = "Summary">
		#renderView('_templates/miniNavigationBar')#
		Summary&nbsp;&nbsp;
	</div>

	<div id="expander-holders" class="panel panel-default">
		#renderView('summary/_templates/summaryData')#
		#renderView('summary/_templates/gis')#
		#renderView('summary/_templates/previousYears')#
	</div>
</div>


</cfoutput>