<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" >
		<table class="land-line-summary">
		<tr>
			<td width="95%" class="section-bar-label">Previous Years</td>
			<!--- <td width="5%" align="right"><a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="##collapse#qryRecords.currentrow#"></a></td> --->
		</tr>
		</table>
	</div>

	<table class="table">
		<thead>
			<td><span class="formatted-data-label">Year</span></td>
			<td><div class="text-right formatted-data-label">Land</div></td>
			<td><div class="text-right formatted-data-label">Features</div></td>
			<td><div class="text-right formatted-data-label">Building</div></td>
			<td><div class="text-right formatted-data-label">Exempt</div></td>
			<td><div class="text-right formatted-data-label">Non School Taxable</div></td>
		</thead>

		<cfloop query="prc.getSummaryPreviousYearsData">
			<tr>
				<td><span class="formatted-data-values">#prc.getSummaryPreviousYearsData.taxYear#</span></td>
				<td width="100"><div class="text-right formatted-data-values">$#NumberFormat(prc.getSummaryPreviousYearsData.landAssessedValueCounty,",")#</div></td>
				<td width="100"><div class="text-right formatted-data-values">$#NumberFormat(prc.getSummaryPreviousYearsData.featureAssessedValueCounty,",")#</div></td>
				<td width="100"><div class="text-right formatted-data-values">$#NumberFormat(prc.getSummaryPreviousYearsData.buildingAssessedValueCounty,",")#</div></td>
				<td width="100"><div class="text-right formatted-data-values">$#NumberFormat(prc.getSummaryPreviousYearsData.CountyExemptionTotal,",")#</div></td>
				<td width="140"><div class="text-right formatted-data-values">$#NumberFormat(prc.getSummaryPreviousYearsData.totalAssessedValueCounty,",")#</div></td>
			</tr>
		</cfloop>
	</table>
</cfoutput>
