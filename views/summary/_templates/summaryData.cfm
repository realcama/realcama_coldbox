<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" >
		<table class="land-line-summary">
		<tr>
			<td width="95%" class="section-bar-label">Summary Data</td>
			<!--- <td width="5%" align="right"><a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="##collapse#qryRecords.currentrow#"></a></td> --->
		</tr>
		</table>
	</div>

	<table class="table table-viewmode table-condensed">

		<tr>
			<td><span class="formatted-data-label">Primary Use</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.UseCode#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Land Value</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.qryPersistentInfoTotals.plvalue, ",")#</span></td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Description</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.useCodeDescription#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Building(s) Value</span></td>
			<td><span class="formatted-data-values">$#numberformat(prc.qryPersistentInfoTotals.pbvalue, ",")#</span></td>
		</tr>

		<tr>
			<td><span class="formatted-data-label">Tax District</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.pefltxdist#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Feature(s) Value</span></td>
			<td><span class="formatted-data-values">$#numberformat(prc.qryPersistentInfoTotals.pxvalbld, ",")#</span></td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Description</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.taxDistrictDescription#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Appraised Value</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(iTotalHeaderValue,",")#</span></td>
		</tr>

		<tr>
			<td><span class="formatted-data-label">Neighborhood</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.neighborhoodCode#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Non School Assessed Value</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.asfltaco,",")#</span></td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Description</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.neighborhoodCodeDecription#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Exempt Amount</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.asflextot1,",")#</span></td>
		</tr>

		<tr>
			<td><span class="formatted-data-label">Market Area</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.marketAreaCode#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Non School Taxable Value</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.asfltxtot1,",")#</span></td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Census Track</span></td>
			<td><span class="formatted-data-values">#prc.getSummaryData.pvcensus#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Non School Differential</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.asflsohdif,",")#</span></td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Other Exemptions</span></td>
			<cfquery name="qryOther" dbtype="query">
				SELECT *
				FROM prc.getSummaryExemptions
				WHERE
					exemptionCode NOT IN ('HX','HA','DH','WX','WA','WR','SX')
			</cfquery>
			<CFSET iOtherExemptions = 0>
			<cfloop query="qryOther">
				<CFSET iOtherExemptions = iOtherExemptions + exemptionAmount>
			</cfloop>
			<td><span class="formatted-data-values">$#NumberFormat(iOtherExemptions,",")#</span></td>
			<td></td>
			<td><span class="formatted-data-label">School Assessed Value</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.ASFLTASC,",")#</span</td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Homestead</span></td>
			<cfquery name="qryHomestead" dbtype="query">
				SELECT *
				FROM prc.getSummaryExemptions
				WHERE
					exemptionCode IN ('HX','HA','DH')
			</cfquery>
			<CFSET dblHomestead = 0>
			<cfloop query="qryHomestead">
				<CFSET dblHomestead = dblHomestead + exemptionAmount>
			</cfloop>

			<td><span class="formatted-data-values">$#NumberFormat(dblHomestead,",")#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Exempt Value</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.asflextot4,",")#</span></td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Widows</span></td>
			<cfquery name="qryWidows" dbtype="query">
				SELECT *
				FROM prc.getSummaryExemptions
				WHERE
					exemptionCode IN ('WX','WA','WR')
			</cfquery>
			<CFSET dblWidows = 0>
			<cfloop query="qryWidows">
				<CFSET dblWidows = dblWidows + exemptionAmount>
			</cfloop>
			<td><span class="formatted-data-values">$#NumberFormat(dblWidows,",")#</span></td>
			<td></td>
			<td><span class="formatted-data-label">School Taxable Value</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.asfltxtot4,",")#</span></td>
		</tr>
		<tr>
			<td><span class="formatted-data-label">Senior</span></td>
			<cfquery name="qrySenior" dbtype="query">
				SELECT *
				FROM prc.getSummaryExemptions
				WHERE
					exemptionCode = 'SX'
			</cfquery>
			<CFSET dblSenior = 0>
			<cfloop query="qrySenior">
				<CFSET dblSenior = dblSenior + exemptionAmount>
			</cfloop>
			<td><span class="formatted-data-values">$#NumberFormat(dblSenior,",")#</span></td>
			<td></td>
			<td><span class="formatted-data-label">Differential</span></td>
			<td><span class="formatted-data-values">$#NumberFormat(prc.getSummaryData.asflagldco,",")#</span></td>
		</tr>

	</table>
</cfoutput>
