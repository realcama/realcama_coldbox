<CFSET baseLocation = "">
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusHousePrefix)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusHousePrefix) & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusHouseNumber)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusHouseNumber)  & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusHouseSuffix)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusHouseSuffix)  & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusStreetName)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusStreetName)  & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusStreetType)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusStreetType)  & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusStreetDirection)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusStreetDirection)  & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusCity)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusCity)  & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusState)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusState)  & " ">
</CFIF>
<CFIF LEN(TRIM(prc.qryPersistentInfo.SitusZip)) NEQ 0>
	<CFSET baseLocation = baseLocation & TRIM(prc.qryPersistentInfo.SitusZip)  & " ">
</CFIF>


<cfoutput>
<div class="panel-heading" role="tab" class="panel-collapse collapse" >						
<table class="land-line-summary">
<tr>
	<td width="95%" class="section-bar-label">GIS Map</td>						
	<!--- <td width="5%" align="right"><a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="##collapse#qryRecords.currentrow#"></a></td> --->
</tr>
</table>
</div>
	
<div id="map" class="gis-map" style="height: 600px; width: 95%; margin: 20px;"></div>
<script type="text/javascript">				
function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 17,
		center: {lat: 0, lng: 0},
		mapTypeId: google.maps.MapTypeId.HYBRID,
		mapTypeControl: true,						
		streetViewControl: false,
		scrollwheel: false
	});
	var geocoder = new google.maps.Geocoder();	
	geocodeAddress(geocoder, map);			
}				
function geocodeAddress(geocoder, resultsMap) {
	var address = '#BaseLocation#'
	geocoder.geocode({'address': address}, function(results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: resultsMap,
				position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}	 
</script>	
<!--- Do ***NOT*** move the script call below here to the top of the page, it MUST be located here!!!!! --->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwRzuEtYCxT0dBtsz1PUPo3susc9GJg1Q&callback=initMap"></script>
</cfoutput>


