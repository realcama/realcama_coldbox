<cfoutput>

<table class="table">
<col width="152">
<col span="2" width="215">

<tr>
	<td class="formatted-data-label">Property - Parcel ID Number</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.parcelDisplayID#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.parcelDisplayID#</td>
</tr>
<tr>
	<td class="formatted-data-label">Property - Tax Year</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.taxYear#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.taxYear#</td>
</tr>
<tr>
	<td class="formatted-data-label">Property - Situs Address</td>
	<td class="text-right formatted-data-values">
		#prc.qLeftRecord.situsHousePrefix# #prc.qLeftRecord.situsHouseNumber# #prc.qLeftRecord.situsHouseSuffix# #prc.qLeftRecord.situsStreetName# #prc.qLeftRecord.situsStreetType# #prc.qLeftRecord.situsStreetDirection#<br />
		#prc.qLeftRecord.situsCity# #prc.qLeftRecord.situsState# #prc.qLeftRecord.situsZip#
	</td>
	<td class="text-right formatted-data-values">
		#prc.qRightRecord.situsHousePrefix# #prc.qRightRecord.situsHouseNumber# #prc.qRightRecord.situsHouseSuffix# #prc.qRightRecord.situsStreetName# #prc.qRightRecord.situsStreetType# #prc.qRightRecord.situsStreetDirection#<br />
		#prc.qRightRecord.situsCity# #prc.qRightRecord.situsState# #prc.qRightRecord.situsZip#
	</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Property - Use Code</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.useCode#<br/>#prc.qLeftRecord.useCodeDescription#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.useCode#<br/>#prc.qRightRecord.useCodeDescription#</td>
</tr>
<tr>
	<td class="formatted-data-label">Property - Neighborhood</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.NeighborhoodCode#<br/>#prc.qLeftRecord.neighborhoodCodeDescription#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.NeighborhoodCode#<br/>#prc.qRightRecord.neighborhoodCodeDescription#</td>
</tr>
<tr>
	<td class="formatted-data-label">Property - Tax District</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.PEFLTXDIST#<br/>#prc.qLeftRecord.taxdistrictCodeDescription#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.PEFLTXDIST#<br/>#prc.qRightRecord.taxdistrictCodeDescription#</td>
</tr>

<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Sales Date</td>
	<td class="text-right formatted-data-values"><CFIF LEN(TRIM(prc.qLeftRecord.recordDate)) NEQ 0>#DateFormat(formatter.parse(prc.qLeftRecord.recordDate),"mm/dd/yyyy")#</CFIF></td>
	<td class="text-right formatted-data-values"><CFIF LEN(TRIM(prc.qRightRecord.recordDate)) NEQ 0>#DateFormat(formatter.parse(prc.qRightRecord.recordDate),"mm/dd/yyyy")#</CFIF></td>
</tr>
<tr>
	<td class="formatted-data-label">Sales Price</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.salePrice)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.salePrice)#</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Appraised Value - Total</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.__PRTOTAL)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.__PRTOTAL)#</td>
</tr>

<tr>
	<td class="formatted-data-label">Differential Amount County</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.CountyDifferential)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.CountyDifferential)#</td>
</tr>
<tr>
	<td class="formatted-data-label">Differential Amount School</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.SchoolDifferential)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.SchoolDifferential)#</td>
</tr>

<tr>
	<td class="formatted-data-label">Assessed Value - Land</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLLATOT)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLLATOT)#</td>
</tr>
<tr>
	<td class="formatted-data-label">Assessed Value - Land - Ag</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLCLASS)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLCLASS)#</td>
</tr>

<tr>
	<td class="formatted-data-label">Assessed Value - Building</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLBATOT)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLBATOT)#</td>
</tr>
<tr>
	<td class="formatted-data-label">Assessed Value - Extra Features</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLXATOT)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLXATOT)#</td>
</tr>
<tr>
	<td class="formatted-data-label">Assessed Value - Total</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLTACO)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLTACO)#</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Exemption(s)</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.ExemptionsAmount#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.ExemptionsAmount#</td>
</tr>
<tr>
	<td class="formatted-data-label">Exemption(s) Amount County</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLEXTOT1)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLEXTOT1)#</td>
</tr>
<tr>
	<td class="formatted-data-label">Exemption(s) Amount School</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLEXTOT4)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLEXTOT4)#</td>
</tr>

<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Non-School Taxable Value</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLTXTOT1)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLTXTOT1)#</td>
</tr>
<tr>
	<td class="formatted-data-label">School Taxable Value</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLTXTOT4)#</td>
	<td class="text-right formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLTXTOT4)#</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>

<tr>
	<td class="formatted-data-label">Land - Total Acres</td>
	<td class="text-right formatted-data-values">#prc.qleftComparisonRates.fTotalAcres#</td>
	<td class="text-right formatted-data-values">#prc.qRightComparisonRates.fTotalAcres#</td>
</tr>
<tr>
	<td class="formatted-data-label">Land - Total - Sq Ft</td>
	<td class="text-right formatted-data-values">#prc.qleftComparisonRates.fTotalSqFeet#</td>
	<td class="text-right formatted-data-values">#prc.qRightComparisonRates.fTotalSqFeet#</td>
</tr>

<tr>
	<td class="formatted-data-label">Land - Front Feet</td>
	<td class="text-right">
		<span class="formatted-data-values">
			<CFIF prc.qLeftRecord.UnitTypeCode EQ "FF" OR prc.qLeftRecord.UnitTypeCode EQ "EF">
				#prc.qLeftRecord.NumberUnits#
			<!--- <CFELSE>
				N/A --->
			</CFIF>
		</span>
	</td>
	<td class="text-right">
		<span class="formatted-data-values">
			<CFIF prc.qRightRecord.UnitTypeCode EQ "FF" OR prc.qRightRecord.UnitTypeCode EQ "EF">
				#prc.qRightRecord.NumberUnits#
			<!--- <CFELSE>
				N/A --->
			</CFIF>
		</span>
	</td>
</tr>
<tr>
	<td class="formatted-data-label">Land - Utilities</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.UtilitiesCode#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.UtilitiesCode#</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Heated Sq Ft</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.HeatedSqFt#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.HeatedSqFt#</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Total Sq Ft</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.ActualSqFt#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.ActualSqFt#</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Quality</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftAdjustments">
			<cfif prc.qLeftAdjustments.adjustmentType eq "Quality">
				<p>#prc.qLeftAdjustments.adjustmentCode#<br>#prc.qLeftAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- #prc.qLeftRecord.BLQUAL#<br/>#prc.qLeftRecord.qualds# --->
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightAdjustments">
			<cfif prc.qRightAdjustments.adjustmentType eq "Quality">
				<p>#prc.qRightAdjustments.adjustmentCode#<br>#prc.qRightAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- #prc.qRightRecord.BLQUAL#<br/>#prc.qRightRecord.qualds# --->
	</td>
</tr>

<tr>
	<td class="formatted-data-label">Building - Effective Year Built</td>
	<td class="text-right formatted-data-values">#prc.qLeftBuildingLine.actualYearBuilt#</td>
	<td class="text-right formatted-data-values">#prc.qRightBuildingLine.actualYearBuilt#</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Depreciation (%)</td>
	<td class="text-right formatted-data-values">#prc.qLeftBuildingLine.normalDepreciationPct#</td>
	<td class="text-right formatted-data-values">#prc.qRightBuildingLine.normalDepreciationPct#</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Exterior Wall</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftAdjustments">
			<cfif prc.qLeftAdjustments.adjustmentType eq "ExteriorWall">
				<p>#prc.qLeftAdjustments.adjustmentCode#<br>#prc.qLeftAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="formatted-data-values">#prc.qLeftRecord.blexw1#<br/>#prc.qLeftRecord.wall1#<CFIF LEN(TRIM(prc.qLeftRecord.blexw2)) NEQ 0><br/>#prc.qLeftRecord.blexw2#<br/>#prc.qLeftRecord.wall2#</CFIF></span> --->
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightAdjustments">
			<cfif prc.qRightAdjustments.adjustmentType eq "ExteriorWall">
				<p>#prc.qRightAdjustments.adjustmentCode#<br>#prc.qRightAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="formatted-data-values">#prc.qRightRecord.blexw1#<br/>#prc.qRightRecord.wall1#<CFIF LEN(TRIM(prc.qRightRecord.blexw2)) NEQ 0><br/>#prc.qRightRecord.blexw2#<br/>#prc.qRightRecord.wall2#</CFIF></span> --->
	</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Air</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftAdjustments">
			<cfif prc.qLeftAdjustments.adjustmentType eq "AirConditioning">
				<p>#prc.qLeftAdjustments.adjustmentCode#<br>#prc.qLeftAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="">#prc.qLeftRecord.blairc#<br/>#prc.qLeftRecord.aircds#</span> --->
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightAdjustments">
			<cfif prc.qRightAdjustments.adjustmentType eq "AirConditioning">
				<p>#prc.qRightAdjustments.adjustmentCode#<br>#prc.qRightAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="">#prc.qRightRecord.blairc#<br/>#prc.qRightRecord.aircds#</span> --->
	</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Heat</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftAdjustments">
			<cfif prc.qLeftAdjustments.adjustmentType eq "Heat">
				<p>#prc.qLeftAdjustments.adjustmentCode#<br>#prc.qLeftAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="formatted-data-values">#prc.qLeftRecord.blheat#<br/>#prc.qLeftRecord.heatds#</span> --->
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightAdjustments">
			<cfif prc.qRightAdjustments.adjustmentType eq "Heat">
				<p>#prc.qRightAdjustments.adjustmentCode#<br>#prc.qRightAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="formatted-data-values">#prc.qRightRecord.blheat#<br/>#prc.qRightRecord.heatds#</span> --->
	</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Bedrooms</td>
	<td class="text-right formatted-data-values">#prc.qLeftBuildingLine.numberBedrooms#</td> <!--- #prc.qLeftRecord.BLBDR# --->
	<td class="text-right formatted-data-values">#prc.qRightBuildingLine.numberBedrooms#</td> <!--- #prc.qRightRecord.BLBDR# --->
</tr>
<tr>
	<td class="formatted-data-label">Building - Bathrooms</td>
	<td class="text-right formatted-data-values">#prc.qLeftBuildingLine.numberBaths#</td> <!--- #prc.qLeftRecord.BLBAT# --->
	<td class="text-right formatted-data-values">#prc.qRightBuildingLine.numberBaths#</td> <!--- #prc.qRightRecord.BLBAT# --->
</tr>
<tr>
	<td class="formatted-data-label">Building - Ceiling</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftAdjustments">
			<cfif prc.qLeftAdjustments.adjustmentType eq "Ceiling">
				<p>#prc.qLeftAdjustments.adjustmentCode#<br>#prc.qLeftAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- #prc.qLeftRecord.BLCEIL#<br/>#prc.qLeftRecord.ceilds# --->
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightAdjustments">
			<cfif prc.qRightAdjustments.adjustmentType eq "Ceiling">
				<p>#prc.qRightAdjustments.adjustmentCode#<br>#prc.qRightAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- #prc.qRightRecord.BLCEIL#<br/>#prc.qRightRecord.ceilds# --->
	</td>
</tr>
<tr>
	<td class="formatted-data-label"> Building - Kitchen</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftAdjustments">
			<cfif prc.qLeftAdjustments.adjustmentType eq "Kitchen">
				<p>#prc.qLeftAdjustments.adjustmentCode#<br>#prc.qLeftAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- #prc.qLeftRecord.BLKTCH#<br/>#prc.qLeftRecord.ktchds# --->
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightAdjustments">
			<cfif prc.qRightAdjustments.adjustmentType eq "Kitchen">
				<p>#prc.qRightAdjustments.adjustmentCode#<br>#prc.qRightAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- #prc.qRightRecord.BLKTCH#<br/>#prc.qRightRecord.ktchds# --->
	</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Roof Structure</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftAdjustments">
			<cfif prc.qLeftAdjustments.adjustmentType eq "RoofStructure">
				<p>#prc.qLeftAdjustments.adjustmentCode#<br>#prc.qLeftAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="formatted-data-values">#prc.qLeftRecord.BLRSTR#<br/>#prc.qLeftRecord.rstrds#</span> --->
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightAdjustments">
			<cfif prc.qRightAdjustments.adjustmentType eq "RoofStructure">
				<p>#prc.qRightAdjustments.adjustmentCode#<br>#prc.qRightAdjustments.adjustment#</p>
			</cfif>
		</cfloop>
		<!--- <span class="formatted-data-values">#prc.qRightRecord.BLRSTR#<br/>#prc.qRightRecord.rstrds#</span> --->
	</td>
</tr>
<tr>
	<td class="formatted-data-label">Building - Add-Ins</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qLeftBuildingAddInsData">
			<p>#prc.qLeftBuildingAddInsData.code#<br>#prc.qLeftBuildingAddInsData.codeDescription#</p>
		</cfloop>
	</td>
	<td class="text-right formatted-data-values">
		<cfloop query="prc.qRightBuildingAddInsData">
			<p>#prc.qRightBuildingAddInsData.code#<br>#prc.qRightBuildingAddInsData.codeDescription#</p>
		</cfloop>
	</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Extra Features (listed)</td>
	<td class="text-right formatted-data-values">#prc.qLeftRecord.__prxfea#</td>
	<td class="text-right formatted-data-values">#prc.qRightRecord.__prxfea#</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td class="formatted-data-label">Image</td>
	<!--- /images/camera-no-image.jpg --->
	<td class="text-right formatted-data-values"><a href="##" id="pop-left"><img id="imageresource-left" src="<CFIF prc.qLeftImage.Recordcount AND LEN(TRIM(prc.qLeftImage.propImage)) NEQ 0>/propertyPhotos/#prc.qLeftImage.propImage#<CFELSE>/propertyPhotos/house.jpg</CFIF>" alt="" width="120" border="0"></a></td>
	<td class="text-right formatted-data-values"><a href="##" id="pop-right"><img id="imageresource-right" src="<CFIF prc.qRightImage.Recordcount AND LEN(TRIM(prc.qRightImage.propImage)) NEQ 0>/propertyPhotos/#prc.qRightImage.propImage#<CFELSE>/propertyPhotos/house2.jpg</CFIF>" alt="" width="120" border="0"></a></td>
</tr>
</table>
</cfoutput>


<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Image preview</h4>
			</div>
			<div class="modal-body">
				<img src="" id="imagepreview" class="img-responsive" style="width: 500px;" >
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
