<cfoutput>
<cfif Isdefined("action")>
	<cfset formatter = createObject("java","java.text.SimpleDateFormat")>
	<cfset formatter.init("yyyyMMdd")>

	<cfif prc.qRightRecord.RecordCount EQ 0>
		<div class="alert alert-info" role="alert">Parcel ##2 - No Parcel found for year #rightYear#.</div>
	<cfelse>
		</div>
		<div class="table-header content-bar-label">
			<cfset showTabOptions = "compare">
			#renderView('_templates/miniNavigationBar')#
			Comparison Details
		</div>
		<div id="expander-holders" class="panel panel-default">
			<div class="panel-heading" role="tab" class="panel-collapse collapse" >
				<table class="land-line-summary">
				<tr>
					<td width="95%" class="section-bar-label">Comparison Information</td>
					
				</tr>
				</table>
			</div>
			<div id="view-compare">
				#renderView('/compare/viewlets/_comparisonLines')#
			</div>
	</cfif>


	<script type="text/javascript">
		$(document).ready(function() {
			$("tr").each(function() {
				var trIsEmpty = true;
				var tr = $(this);

				tr.find("td[align=right]").each(function() {
					console.log($(this).html());
					td = $(this);

					if (isEmpty(td) === true)  {
						$(this).html("NA");
					}
				});
			});

		});
	</script>
</cfif>
</cfoutput>