<cfoutput>
<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		<CFSET showTabOptions = "compare">
		#renderView('_templates/miniNavigationBar')#
		Comparision Report&nbsp;&nbsp;
	</div>

	<div id="expander-holders" class="panel compare-panel-top panel-default">
		<div class="panel-heading" role="tab" class="panel-collapse collapse">
			<table class="land-line-summary">
			<tr>
				<td width="95%" class="section-bar-label">Comparison Criteria</td>
				<!--- <td width="5%" align="right"><a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="##collapse#qryRecords.currentrow#"></a></td> --->
			</tr>
			</table>
		</div>

		<div id="edit-compare">
			<form  role="form" name="frmCompare" id="frmCompare" action="#event.buildLink('compare/compare')##strQueryString#" method="post" data-toggle="validator">
				<input type="hidden" name="s" id="s" value="compare">
				<input type="hidden" name="action" id="action" value="compare">
				<div class="row fix-bs-row">
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Parcel ##1</span><br>
						<input type="text" name="leftParcel" id="leftParcel" value="#prc.stPersistentInfo.parcelDisplayID#" readonly class="form-control">
						<input type="hidden" name="leftParcelID" id="leftParcelID" value="#prc.stPersistentInfo.parcelID#" readonly class="form-control">
					</div>
					<div class="col-xs-2 databox">
						<span class="formatted-data-label">Year</span><br>
						<select name="leftYear" id="leftYear" size="1" data-tags="true" class="select2-1piece-new">
							<cfloop index="iYear" from="#client.currentTaxYear#" to="#year(now())-10#" step="-1">
								<option value="#iYear#" data-desc="#iYear#" <CFIF client.currentTaxYear EQ iYear>selected</CFIF>>#iYear#</option>
							</cfloop>
						</select>
					</div>
					
					<div class="col-xs-2 databox text-center">	<br/>
						<a href="javascript:copyParcelOneToTwo();"><img id="copy-parcel" src="/includes/images/right-compare-arrow.png" data-toggle="tooltip" data-placement="top" title="Copy Parcel 1 To Parcel 2"></a>
					</div>
					
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Parcel ##2</span><br>
						<input type="text" name="rightParcel" id="rightParcel" required="true" required="true" onchange="checkVals()" class="form-control" <CFIF isDefined("rightParcel")>value="#rightParcel#"</CFIF>>
						<input type="hidden" name="rightParcelID" id="rightParcelID" class="form-control" readonly <cfif isDefined("rc.rightParcelID")>value="#rc.rightParcelID#"</cfif>>
					</div>
					<div class="col-xs-2 databox">
						<span class="formatted-data-label">Year</span><br>
						<select name="rightYear" id="rightYear" size="1" data-tags="true" class="select2-1piece-new">
							<cfloop index="iYear" from="#client.currentTaxYear#" to="#year(now())-10#" step="-1">
								<option value="#iYear#" data-desc="#iYear#" <CFIF (isDefined("rightYear") AND rightYear EQ iYear) OR (client.currentTaxYear EQ iYear)>selected</CFIF>>#iYear#</option>
							</cfloop>
						</select>
					</div>
					<div class="clearfix"></div>

					<div class="col-xs-12 text-right">
						<input type="image" src="/includes/images/v2/realCAMA_icon_compare2.png">
					</div>
					<div class="clearfix"></div>

					<div class="col-xs-12">
						&nbsp;
					</div>
				</div>
			</form>
		</div>
		#renderView('compare/viewlets/_comparisonDetails')#
	</div>
</cfoutput>
	
	<!---
	<CFIF Isdefined("action")>
		

		<cfset formatter = createObject("java","java.text.SimpleDateFormat")>
		<cfset formatter.init("yyyyMMdd")>

		<CFIF prc.qRightRecord.RecordCount EQ 0>
			<div class="alert alert-info" role="alert">Parcel #2 - No Parcel found for year <cfoutput>#rightYear#</cfoutput></div>
		<CFELSE>
		</div>
		<div class="table-header content-bar-label">
			<CFSET showTabOptions = "compare">
			<cfoutput>#renderView('_templates/miniNavigationBar')#</cfoutput>
			Comparison Details
		</div>
		<div id="expander-holders" class="panel panel-default">
			<div class="panel-heading" role="tab" class="panel-collapse collapse" >
				<table class="land-line-summary">
				<tr>
					<td width="95%" class="section-bar-label">Comparison Information</td>
					<!--- <td width="5%" align="right"><a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="##collapse#qryRecords.currentrow#"></a></td> --->
				</tr>
				</table>
			</div>

			<div id="view-compare">
				<cfoutput>#renderView('compare/_templates/compare_land')#</cfoutput>
			</div>

			<script type="text/javascript">
				$(document).ready(function() {
					$("tr").each(function() {
						var trIsEmpty = true;
						var tr = $(this);

						tr.find("td[align=right]").each(function() {
							console.log($(this).html());
							td = $(this);

							if (isEmpty(td) === true)  {
								$(this).html("NA");
							}
						});
					});

				});

			</script>
		</CFIF>
		
	</CFIF>
	--->
	

</div>
<cfoutput>
	#addAsset('includes/js/compares.js, includes/js/formatSelect.js')#
</cfoutput>