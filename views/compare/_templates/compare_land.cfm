<cfdump var="#prc#" label="prc" expand="false">
<table class="table">
<col width="152">
<col span="2" width="215">

<cfoutput>
<tr>
	<td><span class="formatted-data-label">Property - Parcel ID Number</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.parcelDisplayID#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.parcelDisplayID#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Property - Tax Year</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.taxYear#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.taxYear#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Property - Situs Address</span></td>
	<td class="text-right"><span class="formatted-data-values">
		#prc.qLeftRecord.situsHousePrefix# #prc.qLeftRecord.situsHouseNumber# #prc.qLeftRecord.situsHouseSuffix# #prc.qLeftRecord.situsStreetName# #prc.qLeftRecord.situsStreetType# #prc.qLeftRecord.situsStreetDirection#<br />
		#prc.qLeftRecord.situsCity# #prc.qLeftRecord.situsState# #prc.qLeftRecord.situsZip#
	</span></td>
	<td class="text-right"><span class="formatted-data-values">
		#prc.qRightRecord.situsHousePrefix# #prc.qRightRecord.situsHouseNumber# #prc.qRightRecord.situsHouseSuffix# #prc.qRightRecord.situsStreetName# #prc.qRightRecord.situsStreetType# #prc.qRightRecord.situsStreetDirection#<br />
		#prc.qRightRecord.situsCity# #prc.qRightRecord.situsState# #prc.qRightRecord.situsZip#
	</span></td>
</tr>

<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Property - Use Code</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.useCode#<br/>#prc.qLeftRecord.useCodeDescription#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.useCode#<br/>#prc.qRightRecord.useCodeDescription#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Property - Neighborhood</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.NeighborhoodCode#<br/>#prc.qLeftRecord.neighborhoodCodeDescription#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.NeighborhoodCode#<br/>#prc.qRightRecord.neighborhoodCodeDescription#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Property - Tax District</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.PEFLTXDIST#<br/>#prc.qLeftRecord.taxdistrictCodeDescription#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.PEFLTXDIST#<br/>#prc.qRightRecord.taxdistrictCodeDescription#</span></td>
</tr>

<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Sales Date</span></td>
	<td class="text-right"><span class="formatted-data-values"><CFIF LEN(TRIM(prc.qLeftRecord.recordDate)) NEQ 0>#DateFormat(formatter.parse(prc.qLeftRecord.recordDate),"mm/dd/yyyy")#</CFIF></span></td>
	<td class="text-right"><span class="formatted-data-values"><CFIF LEN(TRIM(prc.qRightRecord.recordDate)) NEQ 0>#DateFormat(formatter.parse(prc.qRightRecord.recordDate),"mm/dd/yyyy")#</CFIF></span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Sales Price</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.salePrice)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.salePrice)#</span></td>
</tr>
<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Appraised Value - Total</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.__PRTOTAL)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.__PRTOTAL)#</span></td>
</tr>

<tr>
	<td><span class="formatted-data-label">Differential Amount County</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.CountyDifferential)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.CountyDifferential)#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Differential Amount School</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.SchoolDifferential)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.SchoolDifferential)#</span></td>
</tr>

<tr>
	<td><span class="formatted-data-label">Assessed Value - Land</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLLATOT)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLLATOT)#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Assessed Value - Land - Ag</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLCLASS)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLCLASS)#</span></td>
</tr>

<tr>
	<td><span class="formatted-data-label">Assessed Value - Building</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLBATOT)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLBATOT)#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Assessed Value - Extra Features</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLXATOT)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLXATOT)#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Assessed Value - Total</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLTACO)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLTACO)#</span></td>
</tr>
<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Exemption(s)</span></td>
	<td class="text-right"><span class="formatted-data-values">
		#prc.qLeftRecord.ExemptionsAmount#
	</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.ExemptionsAmount#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Exemption(s) Amount County</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLEXTOT1)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLEXTOT1)#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Exemption(s) Amount School</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLEXTOT4)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLEXTOT4)#</span></td>
</tr>

<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Non-School Taxable Value</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLTXTOT1)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLTXTOT1)#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">School Taxable Value</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qLeftRecord.ASFLTXTOT4)#</span></td>
	<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.qRightRecord.ASFLTXTOT4)#</span></td>
</tr>
<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>

<tr>
	<td><span class="formatted-data-label">Land - Total Acres</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qleftComparisonRates.fTotalAcres#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightComparisonRates.fTotalAcres#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Land - Total - Sq Ft</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qleftComparisonRates.fTotalSqFeet#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightComparisonRates.fTotalSqFeet#</span></td>
</tr>

<tr>
	<td><span class="formatted-data-label">Land - Front Feet</span></td>
	<td class="text-right">
		<span class="formatted-data-values">
			<CFIF prc.qLeftRecord.UnitTypeCode EQ "FF" OR prc.qLeftRecord.UnitTypeCode EQ "EF">
				#prc.qLeftRecord.NumberUnits#
			<!--- <CFELSE>
				N/A --->
			</CFIF>
		</span>
	</td>
	<td class="text-right">
		<span class="formatted-data-values">
			<CFIF prc.qRightRecord.UnitTypeCode EQ "FF" OR prc.qRightRecord.UnitTypeCode EQ "EF">
				#prc.qRightRecord.NumberUnits#
			<!--- <CFELSE>
				N/A --->
			</CFIF>
		</span>
	</td>
</tr>
<tr>
	<td><span class="formatted-data-label">Land - Utilities</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.UtilitiesCode#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.UtilitiesCode#</span></td>
</tr>
<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Heated Sq Ft</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.HeatedSqFt#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.HeatedSqFt#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Total Sq Ft</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.ActualSqFt#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.ActualSqFt#</span></td>
</tr>
<!---
<tr>
	<td><span class="formatted-data-label">Building - Quality</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLQUAL#<br/>#prc.qLeftRecord.qualds#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLQUAL#<br/>#prc.qRightRecord.qualds#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Effective Year Built</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLEYB#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLEYB#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Depreciation (%)</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLNORM#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLNORM#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Exterior Wall</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.blexw1#<br/>#prc.qLeftRecord.wall1#<CFIF LEN(TRIM(prc.qLeftRecord.blexw2)) NEQ 0><br/>#prc.qLeftRecord.blexw2#<br/>#prc.qLeftRecord.wall2#</CFIF></span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.blexw1#<br/>#prc.qRightRecord.wall1#<CFIF LEN(TRIM(prc.qRightRecord.blexw2)) NEQ 0><br/>#prc.qRightRecord.blexw2#<br/>#prc.qRightRecord.wall2#</CFIF></span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Air</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.blairc#<br/>#prc.qLeftRecord.aircds#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.blairc#<br/>#prc.qRightRecord.aircds#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Heat</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.blheat#<br/>#prc.qLeftRecord.heatds#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.blheat#<br/>#prc.qRightRecord.heatds#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Bedrooms</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLBDR#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLBDR#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Bathrooms</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLBAT#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLBAT#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Ceiling</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLCEIL#<br/>#prc.qLeftRecord.ceilds#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLCEIL#<br/>#prc.qRightRecord.ceilds#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label"> Building - Kitchen</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLKTCH#<br/>#prc.qLeftRecord.ktchds#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLKTCH#<br/>#prc.qRightRecord.ktchds#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Roof Structure</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.BLRSTR#<br/>#prc.qLeftRecord.rstrds#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.BLRSTR#<br/>#prc.qRightRecord.rstrds#</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Building - Add-Ins</span></td>
	<td class="text-right"><span class="formatted-data-values"></span></td>
	<td class="text-right"><span class="formatted-data-values"></span></td>
</tr>
<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
--->
<tr>
	<td><span class="formatted-data-label">Extra Features (listed)</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qLeftRecord.__prxfea#</span></td>
	<td class="text-right"><span class="formatted-data-values">#prc.qRightRecord.__prxfea#</span></td>
</tr>
<tr>
	<td colspan="3">&nbsp;</span></td>
</tr>
<tr>
	<td><span class="formatted-data-label">Image</span></td>
	<!--- /images/camera-no-image.jpg --->
	<td class="text-right"><span class="formatted-data-values"><a href="##" id="pop-left"><img id="imageresource-left" src="<CFIF prc.qLeftImage.Recordcount AND LEN(TRIM(prc.qLeftImage.propImage)) NEQ 0>/propertyPhotos/#prc.qLeftImage.propImage#<CFELSE>/propertyPhotos/house.jpg</CFIF>" alt="" width="120" border="0"></a></span></td>
	<td class="text-right"><span class="formatted-data-values"><a href="##" id="pop-right"><img id="imageresource-right" src="<CFIF prc.qRightImage.Recordcount AND LEN(TRIM(prc.qRightImage.propImage)) NEQ 0>/propertyPhotos/#prc.qRightImage.propImage#<CFELSE>/propertyPhotos/house2.jpg</CFIF>" alt="" width="120" border="0"></a></span></td>
</tr>

</cfoutput>
</table>

 <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" class="img-responsive" style="width: 500px;" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
