<cfoutput>
<cfscript>
	showTabOptions = "features";
	iPageNumber = 0;
</cfscript>
<cfif prc.getFeaturesInfo.recordcount EQ 0>

	<!----- Popup Model saying no records found and then redirect back to search ------>
	<!---
	<script type="text/javascript">
		$(document).ready(function() {
		     $('##myModal').modal('show')
		});
	</script>
	#application.content.modalStatic('page-no-records')#
 	--->
	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			No extra feature records found for the selected parcel
		</div>
	</div>

<cfelse>
	<script type="text/javascript">
		<CFWDDX ACTION="CFML2JS" INPUT="#prc.getFeaturesInfo#" TOPLEVELVARIABLE="extraFeaturesData">
	</script>
	<cfif Controller.getSetting( "bShowPacketLinks")>
		<div class="alert alert-info">
			<A href="javascript:void(0);" onclick="WriteRaw(extraFeaturesData);" title="Features Packet">Features Packet</A>
		</div>
	</cfif>

	<!--- BUTTON ROW --->
	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			#renderView('_templates/miniNavigationBar')#
			Extra Features <span id="totalPermiLines">(#prc.getFeaturesInfo.recordcount#)</span>
		</div>
		<div id="expander-holders" class="panel panel-default">
			<cfloop query="prc.getFeaturesInfo">
				<cfif prc.getFeaturesInfo.CurrentRow MOD rc.page_size EQ 1>
					<cfset iPageNumber = iPageNumber + 1>
					<div id="page-#iPageNumber#" <cfif rc.page neq iPageNumber>class="hide"</cfif>>
				</cfif>

				#renderView('features/_templates/featuresLine')#

				<cfif prc.getFeaturesInfo.CurrentRow MOD rc.page_size EQ 0>
					</div>
				</cfif>
			</cfloop>

			<cfif prc.getFeaturesInfo.CurrentRow MOD rc.page_size NEQ 0>
				</div> <!--- This ensures the ' div id="page-#iPageNumber#" ' to be closed correctly --->
			</cfif>
		</div>
	</div>
</cfif>

<script type="text/javascript">
	var totalPages = #ceiling(prc.getFeaturesInfo.RecordCount/rc.page_size)#;
	var iCurrentPage = 1;
</script>
#addAsset('/includes/js/header.js,/includes/js/features.js,/includes/js/formatSelect.js')#
<hr>

<form action="#event.buildLink('features/index')#/taxYear/#rc.taxYear#/parcelID/#rc.parcelID#" method="post" name="frmPageSize" id="frmPageSize">
<cfloop collection="#rc#" item="structKey">
	<cfif structKey neq "fieldnames" and structKey neq "taxYear" and structKey neq "parcelID">
		<input type="hidden" name="#lcase(structKey)#" id="#lcase(structKey)#" value="#Evaluate("rc." & structKey)#">
	</cfif>
</cfloop>
</form>

<form action="#event.buildLink('features/featureSave')#" method="post" name="frmSave" id="frmSave">
	<input type="hidden" name="ParcelID" id="ParcelID" value="#prc.stPersistentInfo.ParcelID#">
	<input type="hidden" name="taxYear" id="taxYear" value="#prc.stPersistentInfo.taxYear#">
	<input type="hidden" name="extraFeaturesData" id="extraFeaturesData" value="">
</form>

</cfoutput>


<!---
<cfoutput>

<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		<CFSET showTabOptions = "features">
		#renderView('_templates/miniNavigationBar')#
		Exta Features&nbsp;&nbsp;
	</div>

	<div id="expander-holders" class="panel panel-default">
		<table class="table">
		<div id="tabContent">
			<cfinclude template ="includes/property/features.inc">
		</div>
		</table>
	</div>
</div>

<div class="siteContent">
	<div class="container">
		<cfinclude template="includes/real-property-tabs.cfm">

		<div id="tabContent">
			<cfinclude template ="includes/property/features.inc">
		</div>

	</div>
</div>


<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		<CFSET showTabOptions = "features">
		#renderView('_templates/miniNavigationBar')#
		Exta Feature&nbsp;&nbsp;
	</div>

	<div id="expander-holders" class="panel panel-default">
		#renderView('features/_templates/features')#

	</div>
</div>
</cfoutput>
--->