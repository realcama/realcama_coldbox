
<CFOUTPUT>

	<cfif prc.getFeaturesInfo.recordcount EQ 0>

		<!----- Popup Model saying no records found and then redirect back to search ------>
		<!---
		<script type="text/javascript">
			$(document).ready(function() {
			     $('##myModal').modal('show')
			});
		</script>
		#application.content.modalStatic('page-no-records')#
	 	--->
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				No extra feature records found for the selected parcel
			</div>
		</div>

	<cfelse>
		<CFSET iPageNumber = 0>
		<script>
			var taxYear = #prc.stPersistentInfo.taxYear#;
			var ParcelID = #prc.stPersistentInfo.ParcelID#;

			var iCurrentPage = 1;

			<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.getFeaturesInfo#"
					TOPLEVELVARIABLE="extraFeaturesData">


			function pageIncrement(position) {
				if((iCurrentPage == 1 && position == -1) || (iCurrentPage == #prc.iTotalPages# && position == 1 ))
				{
					/* Do Nothing */
				}
				else {
					for(i=1; i <= #prc.iTotalPages#; i++ ) {
						$("##page-" + i).hide();
					}
					iCurrentPage = iCurrentPage + position;
					$("##page-" + iCurrentPage).show();
					$("##current-page").html(iCurrentPage);
				}

				if(iCurrentPage == #prc.iTotalPages# && position == 1) {
					$("##extrafeatures-page-next").addClass("disabled");
				} else {
					$("##extrafeatures-page-next").removeClass("disabled");
				}

				if(iCurrentPage == 1 && position == -1) {
					$("##extrafeatures-page-prev").addClass("disabled");
				} else {
					$("##extrafeatures-page-prev").removeClass("disabled");
				}

			}
		</script>


		<script src="/js/features.js?g=#now()#"></script>

		<form action="featureSave.cfm" method="post" name="frmSave" id="frmSave">
			<input type="hidden" name="ParcelID" id="ParcelID" value="#prc.stPersistentInfo.ParcelID#">
			<input type="hidden" name="taxYear" id="taxYear" value="#prc.stPersistentInfo.taxYear#">
			<input type="hidden" name="extraFeaturesData" id="extraFeaturesData" value="">
		</form>
		<CFIF Controller.getSetting( "bShowPacketLinks")>
			<div class="alert alert-info">
				<A href="javascript:void(0);" onclick="WriteRaw(extraFeaturesData);">Features Packet</A>&nbsp;&nbsp;&nbsp;
			</div>
		</CFIF>


		<!--- BUTTON ROW --->
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				<CFSET showTabOptions = "features">


				Extra Features <span id="totalPermiLines">(#prc.getFeaturesInfo.recordcount#)</span>
			</div>
			<CFSET iPageNumber = 0>
			<div id="expander-holders" class="panel panel-default">
				<cfloop query="prc.getFeaturesInfo">

					<CFIF prc.getFeaturesInfo.CurrentRow MOD page_size EQ 1>
						<CFSET iPageNumber = iPageNumber + 1>
						<div id="page-#iPageNumber#" <CFIF iPageNumber NEQ 1>style="display:none;"</CFIF>>
					</CFIF>


					<cfinclude template="featuresLine.inc">



					<CFIF prc.getFeaturesInfo.CurrentRow MOD page_size EQ 0>
						</div>
					</CFIF>
				</cfloop>

				<CFIF prc.getFeaturesInfo.CurrentRow MOD page_size NEQ 0>
					</div> <!--- This ensures the ' div id="page-#iPageNumber#" ' to be closed correctly --->
				</CFIF>


			</div> <!--- div id="expander-holders" class="panel panel-default" --->
		</div>

	</cfif>


</CFOUTPUT>