<CFSET LumpSumAddOns = 0>
<CFSET PercentageAddOns = 0>
<CFSET RateAddOns = 0>
<CFSET PointsAddOns = 0>

<cfsavecontent variable="strOptions"> <!--- Adjustments is not ready --->
	<!---
	<cfloop query="qryGetLineBasedUDFs">
		<cfswitch expression="#bladjtyp#">
			<cfcase value="R">
				<tr>
					<td class="formatted-data-values">#ReplaceNoCase(adjdesc,"CAMBUD","Adjustment ","ALL")#</td>
					<td class="formatted-data-values" width="80">#adjcd#</td>
					<td class="formatted-data-values">#adjds#</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="rightAlign formatted-data-values">#NumberFormat(bladjvalue,".000")#</td>
				</tr>
				<CFSET RateAddOns = RateAddOns + NumberFormat(bladjvalue,".000")>
			</CFCASE>
			<cfcase value="L">
				<tr>
					<td class="formatted-data-values">#ReplaceNoCase(adjdesc,"CAMBUD","Adjustment ","ALL")#</td>
					<td class="formatted-data-values" width="80">#adjcd#</td>
					<td class="formatted-data-values">#adjds#</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="rightAlign formatted-data-values">#NumberFormat(bladjvalue,".000")#</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="formatted-data-values">&nbsp;</td>
				</tr>
				<CFSET LumpSumAddOns = LumpSumAddOns + NumberFormat(bladjvalue,".000")>
			</CFCASE>
			<cfcase value="F">
				<tr>
					<td class="formatted-data-values">#ReplaceNoCase(adjdesc,"CAMBUD","Adjustment ","ALL")#</td>
					<td class="formatted-data-values" width="80">#adjcd#</td>
					<td class="formatted-data-values">#adjds#</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="rightAlign formatted-data-values">#NumberFormat(bladjvalue,".000")#</td>
					<td class="formatted-data-values">&nbsp;</td>
				</tr>
				<CFIF PercentageAddOns EQ 0>
					<CFSET PercentageAddOns = NumberFormat(bladjvalue,".000")>
				<CFELSE>
					<CFSET PercentageAddOns = PercentageAddOns * NumberFormat(bladjvalue,".000")>
				</CFIF>

			</cfcase>
			<cfcase value="P">
				<tr>
					<td class="formatted-data-values">#ReplaceNoCase(adjdesc,"CAMBUD","Adjustment ","ALL")#</td>
					<td class="formatted-data-values" width="80">#adjcd#</td>
					<td class="formatted-data-values">#adjds#</td>
					<td class="rightAlign formatted-data-values">#NumberFormat(bladjvalue,".000")#</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="formatted-data-values">&nbsp;</td>
					<td class="formatted-data-values">&nbsp;</td>
				</tr>
				<CFSET PointsAddOns = PointsAddOns + NumberFormat(bladjvalue,".000")>
			</cfcase>
		</cfswitch>
	</cfloop>
	--->
</cfsavecontent>

<cfoutput>
<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading#prc.getFeaturesInfo.CurrentRow#"  aria-expanded="false" >
	<table class="land-line-summary ">
	<tr href="##collapse#prc.getFeaturesInfo.CurrentRow#" class="section-bar-expander" data-parent="heading#prc.getFeaturesInfo.CurrentRow#" data-toggle="collapse" aria-controls="collapse#prc.getFeaturesInfo.CurrentRow#" onclick="toggleChevronChild(#prc.getFeaturesInfo.CurrentRow#)">
		<td width="10%" class="section-bar-label">###prc.getFeaturesInfo.CurrentRow#</td>
		<td width="20%" class="section-bar-label">#prc.getFeaturesInfo.ExtraFeaturesCodeDescription#</td>
		<td width="10%" class="section-bar-label">#prc.getFeaturesInfo.effectiveYearBuilt#</td>
		<td width="40%" class="section-bar-label">#prc.getFeaturesInfo.neighborhoodCode# #prc.getFeaturesInfo.neighborhoodCodeDescription#</td>
		<td width="15%" class="section-bar-label">$#NumberFormat(prc.getFeaturesInfo.value,",.00")#</td>
		<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_#prc.getFeaturesInfo.CurrentRow#"></span></td>
	</tr>
	</table>
</div>

<div id="collapse#prc.getFeaturesInfo.CurrentRow#" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading#prc.getFeaturesInfo.CurrentRow#">
	<div id="view_mode_item_#prc.getFeaturesInfo.CurrentRow#" class="row fix-bs-row">
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Building Number</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.buildingItemNumber#</span>
		</div>
		<div class="col-xs-4 databox">
			<span class="formatted-data-label">Feature</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.ExtraFeaturesCode#: #prc.getFeaturesInfo.ExtraFeaturesCodeDescription#</span>
		</div>
		<div class="col-xs-6 databox">
			<span class="formatted-data-label">Neighborhood Code</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.neighborhoodCode#: #prc.getFeaturesInfo.neighborhoodCodeDescription#</span>
		</div>
		<div class="clearfix"></div>

		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Length</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.dimension1#</span>
		</div>
		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Width</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.dimension2#</span>
		</div>
		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Height</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.dimension3#</span>
		</div>
		<div class="col-xs-5 databox">
			<span class="formatted-data-label">Unit Type</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.unitTypeCode#: #prc.getFeaturesInfo.unitTypeCodeDescription#</span>
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Units</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.numberUnits#</span>
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Unit Price</span><br />
			<span class="formatted-data-values">$#NumberFormat(prc.getFeaturesInfo.unitPrice, ",.00")#</span>
		</div>
		<div class="clearfix"></div>

		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Year</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.effectiveYearBuilt#</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Quality</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.qualityCode#</span>
		</div>
		<div class="col-xs-4 databox">
			<span class="formatted-data-label">Special Conditions / %</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.specialConditioncode#</span> <!--- Not sure how\where to display on page. #prc.getFeaturesInfo.specialConditionPercent# --->
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Depreciation</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.depreciationTable#</span>
		</div>

		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Effective Rate</span><br />
			<span class="formatted-data-values">$#NumberFormat(prc.getFeaturesInfo.unitPrice1,",.00")#</span>
		</div>
		<div class="clearfix"></div>

		<div class="col-xs-2 databox text-center">
			<span class="formatted-data-label">Capping Eligible</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.amendment10Flag#</span>
		</div>
		<div class="col-xs-2 databox text-center">
			<span class="formatted-data-label">Replacement</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.replacementCapFlag#</span>
		</div>
		<div class="col-xs-2 databox text-center">
			<span class="formatted-data-label">New Construction</span><br />
			<span class="formatted-data-values">#prc.getFeaturesInfo.newConstructionFlag#</span>
		</div>
		<div class="col-xs-4 databox">
			<span class="formatted-data-label">&nbsp;</span><br />
			<span class="formatted-data-values"></span>
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Appraised Value</span><br />
			<span class="formatted-data-values">$#NumberFormat(prc.getFeaturesInfo.value,",.00")#</span>
		</div>
		<div class="clearfix"><br/>&nbsp;</div>

		<div class="row fix-bs-row add-in-box">
			<div class="col-xs-12">
				<table class="building-udf-table">
				<thead>
					<tr>
						<td width="150" class="formatted-data-label">Adjustments</td>
						<td colspan="2"><span class="land-line-adj-button"></span></td>
						<td width="15%" class="rightAlign formatted-data-label">Points</td>
						<td width="15%" class="rightAlign formatted-data-label">Lump Sum</td>
						<td width="15%" class="rightAlign formatted-data-label">Percentage</td>
						<td width="15%" class="rightAlign formatted-data-label">Rate Add-On</td>
					</tr>
				</thead>
				<tbody id="udf_#prc.getFeaturesInfo.CurrentRow#">
					<CFIF LEN(TRIM(strOptions)) EQ 0><tr><td colspan="7">None</td></tr><CFELSE>#strOptions#</CFIF>
				</tbody>
				<tfoot class="adjustmentFooter">
					<tr>
						<td width="100"></td>
						<td colspan="2" class="rightAlign formatted-data-label">Net Adjustments</td>
						<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(PointsAddOns,"0.000")#</td>
						<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(LumpSumAddOns,"0.000")#</td>
						<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(PercentageAddOns,"0.000")#</td>
						<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(RateAddOns,"0.000")#</td>
					</tr>
				</tfoot>
				</table>
			</div>

		</div>


		<div class="col-xs-12">
			<div class="land-part-action-buttons">
				<a href="javascript:editThisRow(#prc.getFeaturesInfo.CurrentRow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
			</div>
		</div>
	</div>

	<div id="edit_mode_item_#prc.getFeaturesInfo.CurrentRow#" class="row fix-bs-row" style="display:none;">

		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Building Number</span><br />
			<input type="text" class="form-control" name="buildingItemNumber" value="#prc.getFeaturesInfo.buildingItemNumber#" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
		</div>
		<div class="col-xs-4 databox">
			<span class="formatted-data-label">Feature</span><br />
			<select data-tags="true" name="ExtraFeaturesCode" class="form-control select2-2piece-search-new" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.getFeaturesCode">
					<option value="#prc.getFeaturesCode.code#" data-val="#prc.getFeaturesCode.code#" data-desc="#prc.getFeaturesCode.codeDescription#" data-adjval="" <cfif prc.getFeaturesInfo.ExtraFeaturesCode EQ prc.getFeaturesCode.code> selected</cfif>>#prc.getFeaturesCode.code#: #prc.getFeaturesCode.codeDescription#</option>
				</cfloop>

			</select>
		</div>
		<div class="col-xs-6 databox">
			<span class="formatted-data-label">Neighborhood Code</span><br />
			<select data-tags="true" row="0" class="form-control two-items-on-row select2-3piece-new" name="neighborhoodCode"  onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.getNeighborhoods">
					<option value="#prc.getNeighborhoods.nbhdcd#" data-val="#prc.getNeighborhoods.nbhdcd#" data-desc="#prc.getNeighborhoods.nbhdds#" data-adjval="#NumberFormat(prc.getNeighborhoods.nbhdfa,'9.999')#" <cfif prc.getFeaturesInfo.neighborhoodCode EQ prc.getNeighborhoods.nbhdcd> selected</cfif>>#prc.getNeighborhoods.nbhdcd#: #prc.getNeighborhoods.nbhdds#</option>
				</cfloop>
			</select>
		</div>
		<div class="clearfix"></div>


		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Length</span><br />
			<input type="text" name="dimension1" class="form-control numeric" value="#prc.getFeaturesInfo.dimension1#" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
		</div>
		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Width</span><br />
			<input type="text" name="dimension2" class="form-control numeric" value="#prc.getFeaturesInfo.dimension2#"  onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
		</div>
		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Height</span><br />
			<input type="text" name="dimension3" class="form-control numeric" value="#prc.getFeaturesInfo.dimension3#"  onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
		</div>
		<div class="col-xs-5 databox">
			<span class="formatted-data-label">Unit Type</span><br />
			<select data-tags="true" class="form-control select2-2piece-new" name="unitTypeCode" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.qryFeatureUnitTypeCodes">
					<option value="#prc.qryFeatureUnitTypeCodes.code#" data-val="#prc.qryFeatureUnitTypeCodes.code#" data-desc="#prc.qryFeatureUnitTypeCodes.codeDescription#"  <cfif prc.getFeaturesInfo.unitTypeCode EQ prc.qryFeatureUnitTypeCodes.code> selected</cfif>>#prc.qryFeatureUnitTypeCodes.code#: #prc.qryFeatureUnitTypeCodes.codeDescription#</option>
				</cfloop>
			</select>
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Units</span><br/>
			<input type="text" class="form-control numeric"  name="numberUnits" value="#prc.getFeaturesInfo.numberUnits#" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Unit Price</span><br/>
			<span class="formatted-data-values">$<input type="text" anem="unitPrice" class="form-control numeric width-90percent" value="#NumberFormat(prc.getFeaturesInfo.unitPrice,",.00")#" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)"></span>
		</div>
		<div class="clearfix"></div>


		<div class="col-xs-1 databox">
			<span class="formatted-data-label">Year</span><br/>
			<input type="text" name="effectiveYearBuilt" class="form-control numeric datefield-yearonly " value="#prc.getFeaturesInfo.effectiveYearBuilt#" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Quality</span><br/> <!--- #prc.getFeaturesInfo.xfxqal# --->
			<select data-tags="true" class="form-control select2-2piece-new" name="qualityCode" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.getFeaturesQualityCodes">
					<option value="#prc.getFeaturesQualityCodes.code#" data-val="#prc.getFeaturesQualityCodes.code#" data-adjval="#prc.getFeaturesQualityCodes.costAdjustment#" data-desc="#prc.getFeaturesQualityCodes.codeDescription#" <cfif prc.getFeaturesInfo.qualityCode EQ prc.getFeaturesQualityCodes.code> selected</cfif> >#prc.getFeaturesQualityCodes.codeDescription#: #prc.getFeaturesQualityCodes.codeDescription#</option>
				</cfloop>
			</select>
		</div>

		<div class="col-xs-4 databox">
			<span class="formatted-data-label">Special Conditions / %</span><br/>
			<select data-tags="true" class="form-control select2-2piece-new" name="specialConditionCode" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.qryGetSpecialConditions">
					<option value="#prc.qryGetSpecialConditions.code#" data-val="#prc.qryGetSpecialConditions.code#" data-desc="#prc.qryGetSpecialConditions.codeDescription#" <cfif prc.getFeaturesInfo.specialConditionCode EQ prc.qryGetSpecialConditions.code> selected</cfif>>#prc.qryGetSpecialConditions.code#: #prc.qryGetSpecialConditions.codeDescription#</option>
				</cfloop>
			</select>
		</div>

		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Depreciation</span><br />
			<span class="formatted-data-values"><input type="text" name="depreciationTable" value="#prc.getFeaturesInfo.depreciationTable#" class="form-control numeric width-80percent" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">%</span>
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Effective Rate</span><br />
			<span class="formatted-data-values">$<input type="text" name="unitPrice1" value="#NumberFormat(prc.getFeaturesInfo.unitPrice1,",.00")#" class="form-control numeric width-90percent" onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)"></span>
		</div>
		<div class="clearfix"></div>


		<div class="col-xs-2 databox text-center">
			<span class="formatted-data-label">Capping Eligible</span><br />
			<span class="formatted-data-values">
				<input type="checkbox" name="amendment10Flag" value="Y" <CFIF trim(prc.getFeaturesInfo.amendment10Flag) EQ "Y">checked</CFIF> onclick="changeCheckboxVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
			</span>
		</div>
		<div class="col-xs-2 databox text-center">
			<span class="formatted-data-label">Replacement</span><br />
			<span class="formatted-data-values">
				<input type="checkbox" name="replacementCapFlag" value="Y" <CFIF trim(prc.getFeaturesInfo.replacementCapFlag) EQ "Y">checked</CFIF> onclick="changeCheckboxVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
			</span>
		</div>
		<div class="col-xs-2 databox text-center">
			<span class="formatted-data-label">New Construction</span><br />
			<span class="formatted-data-values">
				<input type="checkbox" name="newConstructionFlag" value="Y" <CFIF trim(prc.getFeaturesInfo.newConstructionFlag) EQ "Y">checked</CFIF> onclick="changeCheckboxVal(#prc.getFeaturesInfo.CurrentRow-1#,this)">
			</span>
		</div>
		<div class="col-xs-4 databox">
			<span class="formatted-data-label">&nbsp;</span>
			<span class="formatted-data-values"></span>
		</div>
		<div class="col-xs-2 databox">
			<span class="formatted-data-label">Appraised Value</span><br />
			<span class="formatted-data-values">$<input type="text" name="value" value="#NumberFormat(prc.getFeaturesInfo.value,",.00")#" class="form-control numeric width-90percent"  onchange="changeVal(#prc.getFeaturesInfo.CurrentRow-1#,this)"></span>
		</div>
		<div class="clearfix"><br/>&nbsp;</div>

		<div class="row fix-bs-row add-in-box">
			<div class="col-xs-12">
				<table class="building-udf-table">
				<thead>
					<tr>
						<td width="150" class="formatted-data-label">Adjustments</td>
						<td colspan="2"><span class="land-line-adj-button" onclick="comingSoon()"<!--- onclick="doBuildingAdjPopup(#prc.getFeaturesInfo.CurrentRow#,#prc.getFeaturesInfo.bl_id#, #prc.getFeaturesInfo.blmodl#)" ---> rel="tooltip" title="Click to alter Add-Ins"><img src="/includes//images/v2/addedit_blue.gif" alt="" width="156" height="26" border="0"></span></td>
						<td width="15%" class="rightAlign formatted-data-label">Points</td>
						<td width="15%" class="rightAlign formatted-data-label">Lump Sum</td>
						<td width="15%" class="rightAlign formatted-data-label">Percentage</td>
						<td width="15%" class="rightAlign formatted-data-label">Rate Add-On</td>
					</tr>
				</thead>
				<tbody id="udf_<!--- #prc.getFeaturesInfo.CurrentRow# --->">
					<CFIF LEN(TRIM(strOptions)) EQ 0><tr><td colspan="7">None</td></tr><CFELSE>#strOptions#</CFIF>
				</tbody>
				<tfoot class="adjustmentFooter">
					<tr>
						<td width="100"></td>
						<td colspan="2" class="rightAlign formatted-data-label">Net Adjustments</td>
						<td width="15%" class="rightAlign formatted-data-label" id="udf_netadj_points_<!--- #prc.getFeaturesInfo.CurrentRow# --->">#NumberFormat(PointsAddOns,"0.000")#</td>
						<td width="15%" class="rightAlign formatted-data-label" id="udf_netadj_lumpsum_<!--- #prc.getFeaturesInfo.CurrentRow# --->">#NumberFormat(LumpSumAddOns,"0.000")#</td>
						<td width="15%" class="rightAlign formatted-data-label" id="udf_netadj_percentage_<!--- #prc.getFeaturesInfo.CurrentRow# --->">#NumberFormat(PercentageAddOns,"0.000")#</td>
						<td width="15%" class="rightAlign formatted-data-label" id="udf_netadj_rateaddon_<!--- #prc.getFeaturesInfo.CurrentRow# --->">#NumberFormat(RateAddOns,"0.000")#</td>
					</tr>
				</tfoot>
				</table>
			</div>

		</div>

		<div class="col-xs-12">
			<div class="land-part-action-buttons">
				<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
				&nbsp;&nbsp;
				<a href="javascript:saveWDDX(#TRIM(prc.getFeaturesInfo.itemNumber)#)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
			</div>
		</div>

	</div>
</div>
</cfoutput>
