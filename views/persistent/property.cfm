<cfoutput>
<cfset iStartYear = client.currentTaxYear>
<cfset iEndYear = prc.qryPropYearRange.firstYear>
<cfset strQueryString = "/taxYear/#rc.taxYear#/parcelID/#rc.parcelID#">

<form action="#event.buildLink('parcel/index')#" method="post" name="frmHeader" id="frmHeader">
<input type="hidden" class="form-control" name="favoriteMarker" id="favoriteMarker" value="#val(prc.qryFavoriteMarkerStatus.favoriteID)#">
<input type="hidden" class="form-control" name="parcelDisplayID" value="#TRIM(prc.qryPersistentInfo.parcelDisplayID)#">
<input type="hidden" class="form-control" name="parcelID" value="#TRIM(rc.parcelID)#">
<input type="hidden" name="currentTab" value="#rc.event#">
	<div id="header-container">
		<cfif isdefined("session.cbstorage.parcelSandBox")>
			<div class="pageNumDisplay">Record Selection: #val(prc.currentParcelPosition)# of #val(prc.totalParcels)#</div>
		</cfif>


		<cfif len(trim(prc.qryPrevProp.pvprop)) gt 0 and isdefined("prc.qryPrevProp.pvTaxYear")>
			<a href="#event.buildLink(rc.event)#/taxYear/#prc.qryPrevProp.pvTaxYear#/parcelID/#TRIM(prc.qryPrevProp.pvprop)#/show/prev"><span id="header-previous"></span></a>
		<cfelseif len(trim(prc.qryPrevProp.pvprop)) gt 0 and isdefined("rc.taxYear")>
			<a href="#event.buildLink(rc.event)#/taxYear/#rc.taxYear#/parcelID/#TRIM(prc.qryPrevProp.pvprop)#/show/prev"><span id="header-previous"></span></a>
		<cfelse>
			<span id="header-previous" class="nav-link-light"></span>
		</cfif>

		<div id="parcelNumber" class="input-group">
			<input type="text" class="form-control text-center" name="parcelNumber" id="search-header-parcel-number" value="#TRIM(prc.qryPersistentInfo.parcelDisplayID)#" aria-describedby="sizing-addon1">
			<span class="input-group-addon <CFIF prc.qryFavoriteMarkerStatus.recordCount EQ 0>favoriteOff<CFELSE>favoriteOn</CFIF>" id="favorites" onclick="favorite()" alt="Save To Favorites"></span>
		</div>

		<cfif len(trim(prc.qryNextProp.pvprop)) gt 0 and isdefined("prc.qryNextProp.pvTaxYear")>
			<a href="#event.buildLink(rc.event)#/taxYear/#prc.qryNextProp.pvTaxYear#/parcelID/#TRIM(prc.qryNextProp.pvprop)#/show/prev"><span id="header-next"></span></a>
		<cfelseif len(trim(prc.qryNextProp.pvprop)) gt 0 and isdefined("rc.taxYear")>
			<a href="#event.buildLink(rc.event)#/taxYear/#rc.taxYear#/parcelID/#TRIM(prc.qryNextProp.pvprop)#/show/next"><div id="header-next"></div></a>
		<cfelse>
			<div id="header-next" class="nav-link-light"></div>
		</cfif>

		<div id="header-year-container">
			<select name="taxYear" id="search-header-y" size="1" onchange="frmHeader.submit();" class="form-control"<cfif isdefined("session.cbstorage.parcelSandBox")> disabled</cfif>>
				<cfloop index="iYear" from="#iStartYear#" to="#iEndYear#" step="-1">
					<option data-val="#iYear#" value="#iYear#" <cfif prc.qryPersistentInfo.TaxYear EQ iYear>selected</cfif>>#iYear#</option>
				</cfloop>
			</select>
		</div>

		<span id="header-year-marker">
			<cfif TRIM(prc.qryPersistentInfo.TaxYear) LT client.currentTaxYear>
				PRIOR YEAR
			<cfelseif (prc.qryPersistentInfo.TaxYear) EQ client.currentTaxYear>
				CURRENT YEAR
			<cfelse>
				FUTURE YEAR
			</cfif>
		</span>
	</div>
</form>

<!--- PROPERTY TABS --->
<div id="property-nav">
	<ul class="nav nav-tabs">
		<li data-property="land" <cfif FindNoCase("land", rc.event)>class="active"</cfif>><a href="#event.buildLink('land/index')##strQueryString#">Land</a></li>
		<li data-property="building" <cfif FindNoCase("building", rc.event) GT 0>class="active"</cfif>><a href="#event.buildLink('building/index')##strQueryString#">Building</a></li>
		<li data-property="permits" <cfif FindNoCase("permits", rc.event)>class="active"</cfif>><a href="#event.buildLink('permits/index')##strQueryString#">Permits</a></li>
		<li data-property="features" <cfif FindNoCase("features", rc.event)>class="active"</cfif>><a href="#event.buildLink('features/index')##strQueryString#">Features</a></li>
		<li data-property="parcel" <cfif FindNoCase("parcel", rc.event)>class="active"</cfif>><a href="#event.buildLink('parcel/index')##strQueryString#">Parcel</a></li>
		<li data-property="correlation" <cfif FindNoCase("correlation", rc.event)>class="active"</cfif>><a href="#event.buildLink('correlation/index')##strQueryString#">Correlation</a></li>
		<li data-property="condomaster" <cfif FindNoCase("condomaster", rc.event)>class="active"</cfif>><a href="#event.buildLink('condoMaster/index')##strQueryString#">Condo Master</a></li>
		<li data-property="condounit" <cfif FindNoCase("condounit", rc.event)>class="active"</cfif>><a href="#event.buildLink('condoUnit/index')##strQueryString#">Condo Unit</a></li>
		<li data-property="history" <cfif FindNoCase("history", rc.event)>class="active"</cfif>><a href="#event.buildLink('history/index')##strQueryString#">History</a></li>
		<li data-property="summary" <cfif FindNoCase("summary", rc.event)>class="active"</cfif>><a href="#event.buildLink('summary/index')##strQueryString#">Summary</a></li>
		<li data-property="compare" <cfif FindNoCase("compare", rc.event)>class="active"</cfif>><a href="#event.buildLink('compare/index')##strQueryString#">Compare</a></li>
		<li data-property="income" <cfif FindNoCase("income", rc.event)>class="active"</cfif>><a href="#event.buildLink('income/index')##strQueryString#">Income</a></li>
		<li data-property="documents" <cfif FindNoCase("documents", rc.event)>class="active"</cfif>><a href="#event.buildLink('documents/index')##strQueryString#">Documents</a></li>
	</ul>
</div>

<div class="container-border container-spacer">
	<div class="persistent-tab-container">
		<table>
			<tr>
				<td class="persistent-data-box">
					<span class="formatted-data-label">Site Address</span><br />
					<span class="formatted-data-values">
					#prc.qryPersistentInfo.SitusHousePrefix# #prc.qryPersistentInfo.SitusHouseNumber# #prc.qryPersistentInfo.SitusHouseSuffix# #prc.qryPersistentInfo.SitusStreetName# #prc.qryPersistentInfo.SitusStreetType# #prc.qryPersistentInfo.SitusStreetDirection#<br />
					#prc.qryPersistentInfo.SitusCity# #prc.qryPersistentInfo.SitusState# #prc.qryPersistentInfo.SitusZip# <cfif LEN(TRIM(prc.qryPersistentInfo.SitusZip4)) NEQ 0>-#prc.qryPersistentInfo.SitusZip4#</cfif><BR />
					</span>
					<br/>
					<span class="formatted-data-label">TPP Account Number</span><br />
					<span class="formatted-data-values header-cmid-values-na">
						<cfif prc.qryTPPInfo.RecordCount GT 0>
							<a href="#event.buildLink('tangible/index')#/s/a/tpp/#TRIM(prc.qryTPPInfo.AccountNumber)##strQueryString#">#formatTPPNumber(prc.qryTPPInfo.AccountNumber)#</a>
							<CFIF prc.qryTPPInfo.RecordCount GT 1>
								&nbsp;&nbsp;<a class="glyphicon glyphicon-tags" href="##more-TPP-links" id="more-TPP-links-anchor" data-toggle="tooltip" title="Click to view additional TPP Accounts"></a>
								<div id="more-TPP-links">
									<span class="formatted-data-label">TPP Account Number</span>
									<span class="formatted-data-values">
										<cfloop query="prc.qryTPPInfo" startrow="2">
											<br/><a href="#event.buildLink('tangible/index')#/s/a/tpp/#TRIM(prc.qryTPPInfo.AccountNumber)##strQueryString#">#formatTPPNumber(prc.qryTPPInfo.AccountNumber)#</a>
										</cfloop>
									</span>
								</div>

							</CFIF>
						<cfelse>
							N/A
						</cfif>
					</span>
				</td>
				<td class="persistent-data-box">
					<span class="formatted-data-label">Owners</span><br />
					<span class="formatted-data-values">
					#prc.qryPersistentInfo.OwnerName#<br>
					#prc.qryPersistentInfo.Ownername2#<br/>
					</span>
					<br/>

					<span class="formatted-data-label">Exemption</span>&nbsp;<sup>(<em><a href="##" data-toggle="modal" data-target="##bannerformmodal">Apply</a></em>)</sup><br/>
					<span class="formatted-data-values header-cmid-values-na"><CFIF LEN(TRIM(prc.qryPersistentInfo.Exemptions)) NEQ 0>#prc.qryPersistentInfo.Exemptions#<CFELSE>None</CFIF></span>
				</td>

				<td class="persistent-data-box value-box">
					<table>
					<tr>
						<td></td>
						<td class="text-right"><span class="formatted-data-label">Appr Value</span></td>
						<td class="text-right"><span class="formatted-data-label">Items</span></td>
					</tr>
					<tr>
						<td><span class="formatted-data-label">Land</span></td>
						<td class="text-right formatted-data-values">$ <span id="landValue" class="header-cmid-values">#NumberFormat(prc.qryPersistentInfoTotals.plvalue, ",")#</span></td>
						<td class="text-right formatted-data-values"><span id="landLineCount" class="header-cmid-values">#prc.qryPersistentInfoCounts.totalLandLines#</span></td>
					</tr>
					<tr>
						<td><span class="formatted-data-label">Agricultural</span></td>
						<td class="text-right formatted-data-values">$ <span id="agriculturalValue" class="header-cmid-values">#numberformat(prc.qryPersistentInfoTotals.agtotal, ",")#</span></td>
						<td class="text-right formatted-data-values"><span id="agriculturalLineCount" class="header-cmid-values">#prc.qryPersistentInfoCounts.aglines#</span></td>
					</tr>
					<tr>
						<td><span class="formatted-data-label">Building</span></td>
						<td class="text-right formatted-data-values">$ <span id="buildingValue" class="header-cmid-values">#numberformat(prc.qryPersistentInfoTotals.pbvalue, ",")#</span></td>
						<td class="text-right formatted-data-values"><span id="buildingLineCount" class="header-cmid-values">#prc.qryPersistentInfoCounts.totalBuildingLines#</span></td>
					</tr>
					<tr>
						<td><span class="formatted-data-label">Extra Features</span></td>
						<td class="text-right formatted-data-values">$ <span id="extrafeaturesValue" class="header-cmid-values">#numberformat(prc.qryPersistentInfoTotals.pxvalbld, ",")#</span></td>
						<td class="text-right formatted-data-values"><span id="extrafeaturesLineCount" class="header-cmid-values">#prc.qryPersistentInfoCounts.pxcntbld#</span></td>
					</tr>
					<tr>
						<td colspan="3" height="1" class="hr-spacer"></td>
					</tr>
					<CFSET iTotalHeaderValue = val(prc.qryPersistentInfoTotals.plvalue) + val(prc.qryPersistentInfoTotals.agtotal) + val(prc.qryPersistentInfoTotals.pbvalue) + val(prc.qryPersistentInfoTotals.PXVALBLD)>
					<CFSET iTotalHeaderLines = val(prc.qryPersistentInfoCounts.totalLandLines) + val(prc.qryPersistentInfoCounts.aglines) + val(prc.qryPersistentInfoCounts.totalBuildingLines) + val(prc.qryPersistentInfoCounts.pxcntbld)>
					<script>
						var iTotalHeaderValue = #iTotalHeaderValue#;
					</script>
					<tr>
						<td><span class="formatted-data-label">Total <span class="warning" id="income_total_warning"></span></span></td>
						<td class="text-right formatted-data-values">$ <span id="tavValue" class="header-cmid-values">#numberformat(iTotalHeaderValue, ",")#</span></td>
						<td class="text-right formatted-data-values"><span id="headerTotalLineCount" class="header-cmid-values">#iTotalHeaderLines#</span></td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</div>

<CFIF Controller.getSetting( "bShowPacketLinks")>
	<cfswitch expression="#rc.event#">
		<cfcase value="parcel.index">
			<div class="alert alert-info">
				<A href="javascript:void(0);" onclick="WriteRaw(parcelData);">Parcel Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(situsData);">Situs Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(salesData);">Sales Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(multiSalesData);">Multi-Sales Packet</A><br/>
				<!--- <A href="javascript:void(0);" onclick="WriteRaw(legalData);">Legal Packet</A>&nbsp;&nbsp;&nbsp; --->
				<A href="javascript:void(0);" onclick="WriteRaw(legalDescriptionData);">Legal Description</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(exemptionData);">Exemption Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(exemptionApplicationData);">Exemption Application Packet</A>&nbsp;&nbsp;&nbsp;
			</div>
			<form action="#event.buildLink('parcel/parcelSave')#" method="post" name="frmSave" id="frmSave">
				<input type="hidden" name="parcelID" value="#rc.parcelID#">
				<input type="hidden" name="taxYear" value="#rc.taxYear#">
				<input type="hidden" name="packet1" id="packet1" value="">
				<input type="hidden" name="packet2" id="packet2" value="">
				<input type="hidden" name="row" id="row" value=""> <!--- Only used when a user saves a Sales row --->
				<input type="hidden" name="tab" id="tab" value="">
				<input type="hidden" name="parcelType" id="parcelType" value="R">
			</form>
		</cfcase>
	</cfswitch>
</CFIF>

#addAsset( '/includes/js/search.js' )#
</cfoutput>