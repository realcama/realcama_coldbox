<cfoutput>
<cfset iStartYear = client.currentTaxYear>
<!--- <CFSET iEndYear = prc.qryPropYearRange.firstYear> --->
<cfset strQueryString = "/tpp/#prc.qGetPersistentInfoTPP.accountNumber#/taxYear/#prc.qGetPersistentInfoTPP.taxYear#/parcelID/#prc.qGetPersistentInfoTPP.ParcelID#">

<form action="#event.buildLink('tangible/lookup')#" method="post" name="frmHeader" id="frmHeader">
<input type="hidden" class="form-control" name="favoriteMarker" id="favoriteMarker" value="#val(prc.qryFavoriteMarkerStatus.favoriteID)#">
<input type="hidden" class="form-control" name="parcelDisplayID" value="#TRIM(prc.qGetPersistentInfoTPP.parcelDisplayID)#">
<input type="hidden" class="form-control" name="ParcelID" value="#TRIM(prc.qGetPersistentInfoTPP.ParcelID)#">
<input type="hidden" name="currentTab" value="#rc.event#">
	<div id="header-container">
		<cfif isdefined("session.cbstorage.parcelSandBox")>
			<div class="pageNumDisplay">Record Selection: #val(prc.currentParcelPosition)# of #val(prc.totalParcels)#</div>
		</cfif>

		<!--- The cfelseif is here until stored procedure is updated to return taxYear and parcelID --->
		<cfif len(trim(prc.qPrevTangibleProp.accountNumber)) gt 0 and isdefined("prc.qPrevTangibleProp.taxYear")>
			<a href="#event.buildLink(rc.event)#/tpp/#prc.qPrevTangibleProp.accountNumber#/taxYear/#prc.qPrevTangibleProp.taxYear#/parcelID/#TRIM(prc.qPrevTangibleProp.ParcelID)#/show/prev"><span id="header-previous"></span></a>
		<cfelseif len(trim(prc.qPrevTangibleProp.accountNumber)) gt 0 and isdefined("rc.taxYear")>
			<a href="#event.buildLink(rc.event)#/tpp/#prc.qPrevTangibleProp.accountNumber#/taxYear/#rc.taxYear#/parcelID/#TRIM(rc.ParcelID)#/show/prev"><span id="header-previous"></span></a>
		<cfelse>
			<span id="header-previous" class="nav-link-light"></span>
		</cfif>

		<div id="parcelNumber" class="input-group">
			<input type="text" class="form-control text-center" name="tpp" id="search-header-parcel-number" value="#formatTPPNumber(TRIM(prc.qGetPersistentInfoTPP.accountNumber))#" aria-describedby="sizing-addon1">
			<span class="input-group-addon <CFIF prc.qryFavoriteMarkerStatus.recordCount EQ 0>favoriteOff<CFELSE>favoriteOn</CFIF>" id="favorites" onclick="favorite()" alt="Save To Favorites"></span>
		</div>

		<cfif len(trim(prc.qNextTangibleProp.accountNumber)) gt 0 and isdefined("prc.qNextTangibleProp.taxYear")>
			<a href="#event.buildLink(rc.event)#/tpp/#prc.qNextTangibleProp.accountNumber#/taxYear/#prc.qNextTangibleProp.taxYear#/parcelID/#TRIM(prc.qNextTangibleProp.ParcelID)#/show/next"><div id="header-next"></div></a>
		<cfelseif len(trim(prc.qNextTangibleProp.accountNumber)) gt 0 and isdefined("rc.taxYear")>
			<a href="#event.buildLink(rc.event)#/tpp/#prc.qNextTangibleProp.accountNumber#/taxYear/#rc.taxYear#/parcelID/#TRIM(rc.ParcelID)#/show/next"><div id="header-next"></div></a>
		<cfelse>
			<span id="header-next" class="nav-link-light"></span>
		</cfif>

		<div id="header-year-container">
			<select name="taxYear" id="search-header-y" size="1" onchange="frmHeader.submit();" class="form-control"<cfif isdefined("session.cbstorage.parcelSandBox")> disabled</cfif>>
				<cfloop index="iYear" from="#iStartYear#" to="#2000#" step="-1">
					<option data-val="#iYear#" value="#iYear#" <cfif prc.qGetPersistentInfoTPP.taxYear EQ iYear>selected</cfif>>#iYear#</option>
				</cfloop>
			</select>
		</div>

		<span id="header-year-marker">
			<cfif TRIM(prc.qGetPersistentInfoTPP.taxYear) LT client.currentTaxYear>
				PRIOR YEAR
			<cfelseif (prc.qGetPersistentInfoTPP.taxYear) EQ client.currentTaxYear>
				CURRENT YEAR
			<cfelse>
				FUTURE YEAR
			</cfif>
		</span>
	</div>
</form>


<!--- TPP PROPERTY TABS --->
<div id="property-nav">
	<div class="pull-right">
		<span class="nextPrev">
			<CFIF LEN(TRIM(prc.qGetPersistentInfoTPP.ParcelID)) NEQ 0>
				<a href="#event.buildLink('parcel/index')#?taxYear=#prc.qGetPersistentInfoTPP.taxYear#&ParcelID=#prc.qGetPersistentInfoTPP.ParcelID#"><span class="formatted-data-label">Switch To Property Info</span></a>
			</CFIF>
			<!--- <a href="index.cfm?s=parcel&y=#parcel_year#&ParcelID=#ParcelID#"><button class="btn btn-default btn-xs btn-inverse" style="margin-left:10px;"><i class="glyphicon glyphicon-chevron-left glyphicon glyphicon-white"></i> <span class="formatted-data-label">Return To Property Info</span></button></a>
			 ---><!--- <button class="btn btn-default btn-xs btn-inverse"><span class="formatted-data-label">Return to Property Info</span></button> --->
		</span>
	</div>
	<ul class="nav nav-tabs">
		<li data-property="tangible" <cfif rc.event eq "tangible.index">class="active"</cfif>><a href="#event.buildLink('tangible/index')##strQueryString#">Owner/DBA</a></li>
		<li data-property="assets" <cfif rc.event eq "tangible.assets">class="active"</cfif>><a href="#event.buildLink('tangible/assets')##strQueryString#">Assets</a></li>
	</ul>
</div>


<!--- PRESISTENT INFORMATION --->
<div class="container-border container-spacer">
	<div style="padding: 5px 0 10px 0;">
		<table width="100%">
			<tr>
				<td class="persistent-data-box">
					<span class="formatted-data-label">Parcel Number</span><br />
					<span class="formatted-data-values">#prc.qGetPersistentInfoTPP.parcelDisplayID#</span><br />

					<span class="formatted-data-label">Real Property Owner</span><br />
					<span class="formatted-data-values">#prc.qGetPersistentInfoTPP.PropertyOwnerName#</span><br />

					<span class="formatted-data-label">Tax District</span><br />
					<span class="formatted-data-values">#prc.qGetPersistentInfoTPP.taxDistrictCode#</span>
				</td>
				<td class="persistent-data-box">
					<span class="formatted-data-label">Site Address</span><br />
					<span class="formatted-data-values">
						#prc.qGetPersistentInfoTPP.locationHouse#<br/>
						#prc.qGetPersistentInfoTPP.locationStreet#<br/>
						#prc.qGetPersistentInfoTPP.locationCity#<br/>
						#prc.qGetPersistentInfoTPP.locationZip#
					</span>
				</td>
				<td class="persistent-data-box">
					<span class="formatted-data-label">Owners</span><br />
					<span class="formatted-data-values">#prc.qGetPersistentInfoTPP.ownerName#</span>
					<BR><BR><span class="formatted-data-label">DBA</span><br />
					<span class="formatted-data-values">#prc.qGetPersistentInfoTPP.DBAName#</span>
				</td>
				<td class="persistent-data-box" width="25%">

					<table width=100%>
						<tr>
							<td><span class="formatted-data-label">Total Value</span></td>
							<td align="right" class="formatted-data-values">$ <span id="tavValue">#NumberFormat(prc.qGetPersistentInfoTPP.assessedValue,",")#</span></td>
						</tr>
						<tr>
							<td valign=top><span class="formatted-data-label">Exempt Amount</span></td>
							<td align="right" class="formatted-data-values">$ <span id="extrafeaturesValue">#NumberFormat(prc.qGetPersistentInfoTPP.totalExemption1,",")#</span></td>
						</tr>
						<tr>
							<td><span class="formatted-data-label">Taxable Value</span></td>
							<td align="right" class="formatted-data-values">$ <span id="buildingValue">#NumberFormat(prc.qGetPersistentInfoTPP.Taxable1,",")#</span></td>
						</tr>
						<tr>
							<td><span class="formatted-data-label">Original Cost</span></td>
							<td align="right" class="formatted-data-values">$ <span id="landValue">#NumberFormat(prc.qGetPersistentInfoTPP.OriginalCost,",")#</span></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</div>

#addAsset( '/includes/js/search.js' )#;
</cfoutput>