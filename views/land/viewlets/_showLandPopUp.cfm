<cfsetting showdebugoutput="No">
<cfparam name="rc.row" default="0">
<cfoutput>

<cfif NOT IsNumeric(rc.row)>
	#addAsset('/includes/css/select2.css,/includes/js/select2.js,/includes/js/jquery.js,/includes/js/jquery.fancybox.js')#
	<script type="text/javascript">
		parent.$.fancybox.close();
	</script>
	<cfabort>
</cfif>

<div id="popLandUDF" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-land-popupUDF">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="cancelChanges();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Adjustments for Item ###rc.row#</h4>
			</div>
			<div class="modal-body">
				<div id="errorContainer" class="row"></div>
				<div id="adjContainer">
					<div id="adjHeader"><span class="adjHeaderAdj">Adjustments</span><span class="adjHeaderCode">Code</span><span class="adjHeaderDesc">Description</span><span class="adjHeaderPercentage adjHeaderSmall text-center">Contributing<br>Percentage</span><span class="adjHeaderPoints adjHeaderSmall text-center">Points</span><span class="adjHeaderLump adjHeaderSmall text-center">Lump Sum</span><span class="adjHeaderFactor adjHeaderSmall text-center">Factor</span><span class="adjHeaderRate adjHeaderSmall text-center">Rate<br>Add-On</span></div>
					<!--- The body of the modal window is generated via javascript. --->
					<div id="adjContent"></div>
				</div>
			</div>

			<div class="modal-footer">
				<div id="modalPleaseWait" class="pull-left progress">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span>Please Wait</span></div>
				</div>
				<span class="col-ends"></span>
				<span class="text-center col-center">
					<button id="addRowButton" class="btn btn-primary btn-add-option-udf" type="button" onclick="addAdjRow()"><i class="glyphicon glyphicon-plus-sign"></i> Add Another Option</button>
				</span>
				<span class="col-ends">
					<button type="button"  class="btn btn-default"  onclick="cancelChanges();"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</button>
					<button type="button"  class="btn btn-default"  onclick="savePopUpChanges();"><i class="glyphicon glyphicon-ok-sign"></i> Finished</button>
				</span>
			</div>
		</div>
	</div>
</div>

<!--- Remember that JS is 0 based, and CF is 1 based --->
<script type="text/javascript">
	var iUrlRow = #rc.row#; // Represents the land line row
	var la_id = "#rc.itemNumber#";
	var jsRowNumber = #evaluate(val(rc.row) - 1)#;
	var counterUDF = 0;
	var arrlandAdjustments = ["Custom","Depths","Roads","Topography","Utilities","Zoning"]; // Used in landAdjustments.js


	function createSelectOption3 (optValue, optDesc, optData1, optData2, optData3) {
		var strOptions = "";

		strOptions = strOptions + "<option value='" + optValue + "'";
		strOptions = strOptions + " data-val='" + optValue + "'";
		strOptions = strOptions + " data-desc='" + optDesc + "'";
		strOptions = strOptions + " data-adjval='" + optData1 + "#TRIM(prc.qryRoads.adjustment)#'";
		strOptions = strOptions + " data-adjtyp='" + optData2 + "#prc.qryRoads.adjustmenttype#'";
		strOptions = strOptions + " data-codetyp='" + optData3 + "#prc.qryRoads.codetype#'";
		strOptions = strOptions + ">";
		strOptions = strOptions + optValue + optDesc;
		strOptions = strOptions + "</option>";

		return strOptions;
	}

	// Called in the buildmodal() function
	function createAdjEntryRow(iRow, adjdesc, mode) {
		var strHTML = "";
		strHTML = strHTML + "<div id='adjRow_" + iRow + "' class='udfrow'>"

		/*if(mode == "Insert") {
			strHTML = strHTML + "	<span class='icon-nothing'></span>";
		} else {
			strHTML = strHTML + "	<span class='glyphicon glyphicon-remove-sign' onclick='removeAdjRow(" + iRow + ")'></span>";
		}*/

		strHTML = strHTML + "<i class='glyphicon glyphicon-remove-sign' onclick='removeAdjRow(" + iRow + ")' data-placement='right' data-toggle='tooltip' data-original-title='Remove This Adjustment'></i>";

		// Create the list of the options for this Adjustment
		// form-control select2-1piece-new
		strHTML = strHTML + "<select name='adjdesc' id='adjdesc_" + iRow + "' data-rownumber='" + iRow + "' class='form-control select2-1piece-new' onchange='chgAdjustments(" + iRow +",this); changeUDFMethod(this)'>"; // changeAdjVal(" + iRow +",this);
			for ( var i = 0; i < arrlandAdjustments.length; i++ ) {
				if ( arrlandAdjustments[i] == adjdesc ) {
					//strHTML = strHTML + "<option value='" + arrlandAdjustments[i] + "' data-adjustmentname='" + arrlandAdjustments[i] + "' selected>" + arrlandAdjustments[i] + "</option>";
					strHTML = strHTML + "<option value='" + arrlandAdjustments[i] + "' data-val='" + arrlandAdjustments[i] + "' data-desc='" + arrlandAdjustments[i] + "' data-adjustmentname='" + arrlandAdjustments[i] + "' selected>" + arrlandAdjustments[i] + "</option>";
				} else {
					//strHTML = strHTML + "<option value='" + arrlandAdjustments[i] + "' data-adjustmentname='" + arrlandAdjustments[i] + "'>" + arrlandAdjustments[i] + "</option>";
					strHTML = strHTML + "<option value='" + arrlandAdjustments[i] + "' data-val='" + arrlandAdjustments[i] + "' data-desc='" + arrlandAdjustments[i] + "' data-adjustmentname='" + arrlandAdjustments[i] + "'>" + arrlandAdjustments[i] + "</option>";
				}
			}
		strHTML = strHTML + "</select>";

		<!--- Custom --->
		strDisplay = (adjdesc == 'Custom') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select name='Custom' id='Custom_" + iRow + "' data-tags='true'  class='form-control select2-2piece-new'" + strDisplay +" onchange='changeAdjVal(" + iRow +",this)'>"; // changeAdjDefaultAdjustment(" + iRow +",this);
		strHTML = strHTML + "<option value='N/A' data-val='0' data-adjcd='N/A' data-desc='&nbsp;'  data-adjtyp='F'>N/A</option>";
		<cfloop query="prc.qryAdjustmentTypes">
			strHTML = strHTML + createSelectOption3( '#trim(prc.qryAdjustmentTypes.adj_id)#', '#trim(prc.qryAdjustmentTypes.adjds)#', '#trim(prc.qryAdjustmentTypes.adjval)#', '#trim(prc.qryAdjustmentTypes.adjcd)#', '#trim(prc.qryAdjustmentTypes.adjtyp)#' );
			//strHTML = strHTML + "<option value='#prc.qryAdjustmentTypes.adj_id#' data-val='#prc.qryAdjustmentTypes.adj_id#' data-desc='#TRIM(prc.qryAdjustmentTypes.adjds)#' data-default='#prc.qryAdjustmentTypes.adjval#' data-adjcd='#prc.qryAdjustmentTypes.adjcd#'  data-adjtyp='#prc.qryAdjustmentTypes.adjtyp#'><CFIF prc.qryAdjustmentTypes.adjcd NEQ "">#prc.qryAdjustmentTypes.adjcd#: </cfif>#prc.qryAdjustmentTypes.adjds# <CFIF prc.qryAdjustmentTypes.adjcd NEQ "">[Default: #prc.qryAdjustmentTypes.adjval#]</cfif></option>";
		</cfloop>
		strHTML = strHTML + "</select></span>";

		<!--- Depths --->
		strDisplay = (adjdesc == 'Depths') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select data-tags='true' name='Depths' id='Depths_" + iRow + "' class='form-control select2-2piece-new'" + strDisplay +" onchange='changeAdjVal(" + iRow +",this);changeDepthTable(" + iRow +",this)'>";
		strHTML = strHTML + "<option value='N/A' data-val='N/A' data-adjdescds='N/A'>N/A</option>";
		<cfloop query="prc.qryLandDepthRecords">
			strHTML = strHTML + createSelectOption3( '#trim(prc.qryLandDepthRecords.code)#', '#trim(prc.qryLandDepthRecords.depthTableDescription)#', '', '', '' );
			//strHTML = strHTML + "<option value='#trim(prc.qryLandDepthRecords.code)#' data-val='#prc.qryLandDepthRecords.code#' data-desc='#prc.qryLandDepthRecords.depthTableDescription#'>#prc.qryLandDepthRecords.code#: #prc.qryLandDepthRecords.depthTableDescription#</option>";
		</cfloop>
		strHTML = strHTML + "</select></span>";

		<!--- Roads--->
		strDisplay = (adjdesc == 'Roads') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select data-tags='true' name='Roads' id='Roads_" + iRow + "' class='form-control select2-2piece-new'" + strDisplay +" onchange='changeAdjVal(" + iRow +",this)'>"; //two-part-large select2-2piece-dropdown-new
		strHTML = strHTML + "<option value='' data-val='' data-desc='Select...'>Select</option>";
		<cfloop query="prc.qryRoads">
			strHTML = strHTML + createSelectOption3( '#trim(prc.qryRoads.code)#', '#trim(prc.qryRoads.codedescription)#', '#trim(prc.qryRoads.adjustment)#', '#trim(prc.qryRoads.adjustmenttype)#', '#trim(prc.qryRoads.codetype)#' );
			//strHTML = strHTML + "<option value='#trim(prc.qryRoads.code)#' data-val='#trim(prc.qryRoads.code)#' data-desc='#trim(prc.qryRoads.codedescription)#' data-adjval='#TRIM(prc.qryRoads.adjustment)#' data-adjtyp='#prc.qryRoads.adjustmenttype#' data-codetyp='#prc.qryRoads.codetype#'>#prc.qryRoads.code#: #prc.qryRoads.codedescription#</option>";
		</cfloop>
		strHTML = strHTML + "</select></span>";

		<!--- Topography --->
		strDisplay = (adjdesc == 'Topography') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select data-tags='true' name='Topography' id='Topography_" + iRow + "' class='form-control select2-2piece-new'" + strDisplay +" onchange='changeAdjVal(" + iRow +",this)'>";
		strHTML = strHTML + "<option value='' data-val='' data-desc='Select...'>Select</option>";
		<cfloop query="prc.qryTopo">
			strHTML = strHTML + createSelectOption3( '#trim(prc.qryTopo.code)#', '#trim(prc.qryTopo.codedescription)#', '#trim(prc.qryTopo.codedescription)#', '#trim(prc.qryTopo.adjustmenttype)#', '#trim(prc.qryTopo.codetype)#' );
			//strHTML = strHTML + "<option value='#trim(prc.qryTopo.code)#' data-val='#trim(prc.qryTopo.code)#' data-desc='#trim(prc.qryTopo.codedescription)#' data-adjval='#TRIM(prc.qryTopo.adjustment)#' data-adjtyp='#prc.qryTopo.adjustmenttype#' data-codetyp='#prc.qryTopo.codetype#'>#prc.qryTopo.code#: #prc.qryTopo.codedescription#</option>";
		</cfloop>
		strHTML = strHTML + "</select></span>";

		<!--- Utilities--->
		strDisplay = (adjdesc == 'Utilities') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select data-tags='true' name='Utilities' id='Utilities_" + iRow + "' class='form-control select2-2piece-new'" + strDisplay +" onchange='changeAdjVal(" + iRow +",this)'>";
		strHTML = strHTML + "<option value='' data-val='' data-desc='Select...'>Select</option>";
		<cfloop query="prc.qryUtils">
			strHTML = strHTML + createSelectOption3( '#trim(prc.qryUtils.code)#', '#trim(prc.qryUtils.codedescription)#', '#trim(prc.qryUtils.adjustment)#', '#trim(prc.qryUtils.adjustmenttype)#', '#trim(prc.qryUtils.codetype)#' );
			//strHTML = strHTML + "<option value='#trim(prc.qryUtils.code)#' data-val='#trim(prc.qryUtils.code)#'  data-desc='#trim(prc.qryUtils.codedescription)#' data-adjval='#TRIM(prc.qryUtils.adjustment)#' data-adjtyp='#prc.qryUtils.adjustmenttype#' data-codetyp='#prc.qryUtils.codetype#'>#prc.qryUtils.code#: #prc.qryUtils.codedescription#</option>";
		</cfloop>
		strHTML = strHTML + "</select></span>";

		<!--- Zoning --->
		strDisplay = (adjdesc == 'Zoning') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select data-tags='true' name='Zoning' id='Zoning_" + iRow + "' class='form-control select2-2piece-new'" + strDisplay +" onchange='changeAdjVal(" + iRow +",this)'>";
		strHTML = strHTML + "<option value='' data-val='' data-desc='Select...'>Select</option>";
		<cfloop query="prc.qryZoning">
			strHTML = strHTML + createSelectOption3( '#trim(prc.qryZoning.code)#', '#trim(prc.qryZoning.codedescription)#', '#trim(prc.qryZoning.adjustment)#', '#trim(prc.qryZoning.adjustmenttype)#', '#trim(prc.qryZoning.codetype)#' );
			//strHTML = strHTML + "<option value='#trim(prc.qryZoning.code)#' data-val='#trim(prc.qryZoning.code)#' data-desc='#trim(prc.qryZoning.codedescription)#' data-adjval='#trim(prc.qryZoning.adjustment)#' data-adjtyp='#trim(prc.qryZoning.adjustmenttype)#' data-codetyp='#trim(prc.qryZoning.codetype)#'><cfif len(trim(prc.qryZoning.code)) neq 0>#trim(prc.qryZoning.code)#: </cfif>#trim(prc.qryZoning.codedescription)#</option>";	
		</cfloop>
		strHTML = strHTML + "</select></span>";

		strHTML = strHTML + "<input name='percentageFactor' id='percentageFactor_" + iRow + "' class='form-control two-part-small numeric' type='text' value='0' disabled>%";

		/* These inputs are populated by the buildModal function */
		strHTML = strHTML + "<input name='bLPoints' id='bLPoints_" + iRow + "' type='text' value='' class='form-control two-part-small numeric'>";
		strHTML = strHTML + "<input name='bLLumpSum' id='bLLumpSum_" + iRow + "' type='text' value='' class='form-control two-part-small numeric'>";
		strHTML = strHTML + "<input name='bLFactor' id='bLFactor_" + iRow + "' type='text' value='' class='form-control two-part-small numeric'>";
		strHTML = strHTML + "<input name='bLRateAddOn' id='bLRateAddOn_" + iRow + "' type='text' value='' class='form-control two-part-small numeric'";
		strHRML = strHTML;

		<!--- Adjustment Type (only used for CUSTOM)
		strDisplay = (adjdesc == 'Depths') ? " disabled" : "";
		strHTML = strHTML + "	<select name='laadjtyp' id='laadjtyp_" + iRow + "' class='form-control select2-1piece-dropdown-new' " + strDisplay + " onchange='changeAdjVal(" + iRow +",this)'>";
		strHTML = strHTML + "		<option value='0' data-adjustmentname='N/A'>N/A</option>";
		strHTML = strHTML + "		<option value='L' data-adjustmentname='Lump Sum Add-On'>Lump Sum Add-On</option>";
		strHTML = strHTML + "		<option value='F' data-adjustmentname='Percentage'>Percentage</option>";
		strHTML = strHTML + "		<option value='R' data-adjustmentname='Rate Add-On'>Rate Add-On</option>";
		strHTML = strHTML + "	</select>"; --->

		<!--- Adjustment value
		strDisplay = (adjdesc == 'Depths') ? " readonly" : "";
		strHTML = strHTML + "	<input name='laadjvalue' id='laadjvalue_" + iRow + "'  class='form-control two-part-small numeric' " + strDisplay + " type='text' value='0' onchange='changeAdjVal(" + iRow +",this)'>"; --->
		strHTML = strHTML + "</div>";
		//jsRowNumber++;

		return strHTML;

	}
</script>

#addAsset('/includes/css/select2.css,/includes/js/select2.js,/includes/js/landAdjustments.js,/includes/js/formatSelect.js')#
</cfoutput>

<script type="text/javascript">
	$( document ).ready(function() {
		buildModal();
		//changeSelectsToSelect2s();
		toggleAdjustmentAttrib();
	});
</script>
