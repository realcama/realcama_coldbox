<cfoutput>
<cfparam name="strOptions" default="">
<cfset PercentageAddOns = 0>
<cfset pointAddOns = 0>
<cfset LumpSumAddOns = 0>
<cfset factorAddOns = 0>
<cfset RateAddOns = 0>

<cfif isdefined("prc.qryUDFS")>
	<cfsavecontent variable="strOptions">
		<cfset i = 0 />
		<cfloop query="prc.qryUDFS">
			<tr>
				<td id="udfCodeType_#i#" class="formatted-data-values">#codeType#</td>
				<td id="udfCode_#i#" class="formatted-data-values">#code#</td>
				<td id="udfCodeDesc_#i#" class="formatted-data-values">#codeDescription#</td>
				<td id="udfPercent_#i#" class="text-right smallCell formatted-data-values">
					<cfset marker = "">
					<cfif codeType EQ "Depths">
						<cfif prc.qryGetLandInfo.unitTypeCode EQ "EF" OR prc.qryGetLandInfo.unitTypeCode EQ "FF">
						<cfelse>
							<cfset marker = " <span class='warning' data-toggle='tooltip' data-placement='top' title='Value is ignored due to unit type'>*</span>">
						</cfif>
					</cfif>
					#percentageFactor#%#marker#
					<cfif ( codeType EQ "Depths" AND (prc.qryGetLandInfo.unitTypeCode EQ "EF" OR prc.qryGetLandInfo.unitTypeCode EQ "FF") )
						OR (codeType NEQ "Depths")> <!--- adjdesc --->
						<cfif PercentageAddOns eq 0>
							<cfset PercentageAddOns = percentageFactor />
						<cfset>
							<cfset PercentageAddOns = PercentageAddOns * (percentageFactor/100) />
						</cfif>
					</cfif>
				</td>
				<td id="udfPoints_#i#" class="text-right smallCell formatted-data-values">
				</td>
				<td id="udfLumpSum_#i#" class="text-right smallCell formatted-data-values">
					<cfif adjustmentType eq "L">
						#NumberFormat(adjustment,".000")#
						<cfset LumpSumAddOns = LumpSumAddOns + NumberFormat(laadjvalue,".000")>
					</cfif>
				</td>
				<td id="udfFactor_#i#" class="text-right smallCell formatted-data-values">
					<cfif adjustmentType eq "F">
						#NumberFormat(adjustment,".000")#
						<cfset factorAddOns = factorAddOns + NumberFormat(adjustment,".000")>
					</cfif>
				</td>
				<td id="udfRateAddOn_#i#" class="text-right smallCell formatted-data-values">
					<cfif adjustmentType eq "R">
						#NumberFormat(adjustment,".000")#
						<cfset RateAddOns = RateAddOns + NumberFormat(adjustment,".000")>
					</cfif>					
				</td>
				<!---<cfswitch expression="#adjustmentType#">
					<cfcase value="F">
						<cfset marker = "">
						<cfif codeType EQ "Depths">
							<cfif prc.qryGetLandInfo.unitTypeCode EQ "EF" OR prc.qryGetLandInfo.unitTypeCode EQ "FF">
							<cfelse>
								<cfset marker = " <span class='warning' data-toggle='tooltip' data-placement='top' title='Value is ignored due to unit type'>*</span>">
							</cfif>
						</cfif>
						<td class="text-right smallCell formatted-data-values">#percentageFactor#%#marker#</td>
						<td class="smallCell formatted-data-values">&nbsp;</td>
						<td class="smallCell">&nbsp;</td>

						<cfif ( codeType EQ "Depths" AND (prc.qryGetLandInfo.unitTypeCode EQ "EF" OR prc.qryGetLandInfo.unitTypeCode EQ "FF") )
							OR (codeType NEQ "Depths")> <!--- adjdesc --->
							<cfif PercentageAddOns eq 0>
								<cfset PercentageAddOns = percentageFactor />
							<cfset>
								<cfset PercentageAddOns = PercentageAddOns * (percentageFactor/100) />
							</cfif>
						</cfif>
					</cfcase>

					<cfcase value="L">
						<td class="smallCell formatted-data-values">&nbsp;</td>
						<td class="text-right smallCell formatted-data-values">#NumberFormat(adjustment,".000")#</td>
						<td class="smallCell formatted-data-values">&nbsp;</td>

						<cfset LumpSumAddOns = LumpSumAddOns + NumberFormat(laadjvalue,".000")>
					</cfcase>
					<cfcase value="R">
						<td class="smallCell formatted-data-values">&nbsp;</td>
						<td class="smallCell formatted-data-values">&nbsp;</td>
						<td class="text-right smallCell formatted-data-values">#NumberFormat(adjustment,".000")#</td>

						<cfset RateAddOns = RateAddOns + NumberFormat(adjustment,".000")>
					</cfcase>
				</cfswitch> --->
			</tr>
			<cfset i = i + 1 />
		</cfloop>
	</cfsavecontent>
</cfif>

	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading#prc.qryGetLandInfo.currentrow#"  aria-expanded="false" >
		<table class="land-line-summary">
		<tr href="##collapse#prc.qryGetLandInfo.currentrow#" class="section-bar-expander" onclick="toggleChevronChild(#prc.qryGetLandInfo.currentrow#)" data-parent="heading#prc.qryGetLandInfo.currentrow#" data-toggle="collapse" aria-controls="collapse#prc.qryGetLandInfo.currentrow#" >
			<td width="10%" class="section-bar-label">###prc.qryGetLandInfo.currentrow#</td>
			<td width="15%" align="right" class="section-bar-label">#prc.qryGetLandInfo.landUseCode#</td>
			<td width="22%" align="right" class="section-bar-label">#prc.qryGetLandInfo.landUseCodeDescription#</td>
			<td width="16%" align="right" class="section-bar-label">#prc.qryGetLandInfo.numberUnits#</td>
			<td width="16%" class="section-bar-label">#prc.qryGetLandInfo.unitTypeCode#</td>
			<td width="16%" align="right" class="section-bar-label"><span id="finalLandViewValue2_#prc.qryGetLandInfo.CurrentRow-1#"></span></td>
			<td width="5%" align="right" class="section-bar-label"><span id="chevron_#prc.qryGetLandInfo.currentrow#" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse#prc.qryGetLandInfo.currentrow#" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading#prc.qryGetLandInfo.currentrow#">
		<div id="view_mode_item_#prc.qryGetLandInfo.currentrow#">
			<div class="row fix-bs-row">
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Type</span><br />
					<span class="formatted-data-values">#prc.qryGetLandInfo.landUseCode# #prc.qryGetLandInfo.landUseCodeDescription#</span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Neighborhood Code</span><br />
					<span class="formatted-data-values">#prc.qryGetLandInfo.NeighborhoodCode#: #prc.qryGetLandInfo.NeighborhoodCodeDescription#</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Appraised Value</span><br/>
					<span class="formatted-data-values" id="finalLandViewValue_#prc.qryGetLandInfo.CurrentRow-1#"></span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Frontage</span><br />
					<span class="formatted-data-values">#NumberFormat(prc.qryGetLandInfo.front,"0.000")#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Back</span><br />
					<span class="formatted-data-values">#NumberFormat(prc.qryGetLandInfo.back,"0.000")#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Depth</span><br />
					<span class="formatted-data-values">#NumberFormat(prc.qryGetLandInfo.depth,"0.000")#</span>
				</div>
				<div class="col-xs-2 databox">

				</div>
				<div class="clearfix"></div>

				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Unit Type</span><br />
					<span class="formatted-data-values">#prc.qryGetLandInfo.unitTypeCode#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Units</span><br />
					<span class="formatted-data-values">#prc.qryGetLandInfo.numberUnits#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Unit Price</span><br />
					<span class="formatted-data-values">#dollarformat(prc.qryGetLandInfo.unitPrice)#</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Effective Rate</span><br/>
					<span class="formatted-data-values">#NumberFormat(prc.qryGetLandInfo.effectiveBaseRate,",.00")#</span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-2 databox text-center">
					<span class="formatted-data-label">Capping Eligible?</span><br /><div class="formatted-data-values text-center">#prc.qryGetLandInfo.leflae#</div>
				</div>
				<div class="col-xs-2 databox text-center">
					<span class="formatted-data-label">Fair Market Value?</span><br /><div class="formatted-data-values text-center">#prc.qryGetLandInfo.leflfmvq#</div>
				</div>
				<div class="col-xs-3 databox text-center">
					<span class="formatted-data-label">Replacement?</span><br /><div class="formatted-data-values text-center">#prc.qryGetLandInfo.lefldemol#</div>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Market Unit Price</span><br />
					<span class="formatted-data-values">#dollarformat(prc.qryGetLandInfo.marketUnitPrice)#</span>
				</div>

				<div class="col-xs-2 databox">
					<span class="formatted-data-label">AG Market Value</span><br />
					<span class="formatted-data-values" id="AGMarketValueFinal_#prc.qryGetLandInfo.CurrentRow-1#" align="center">#NumberFormat(prc.qryGetLandInfo.marketValue,",.00")#</span>
				</div>
				<div class="clearfix">&nbsp;</div>

				<cfset currentRow = prc.qryGetLandInfo.CurrentRow />
				<div class="row fix-bs-row add-in-box">
					<div class="col-xs-12">
						#renderView('land/viewlets/_landAdjustments')#
					</div>
				</div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(#prc.qryGetLandInfo.currentrow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div> <!--- id="view_mode_item_#prc.qryGetLandInfo.currentrow#" --->


		<div id="edit_mode_item_#prc.qryGetLandInfo.currentrow#" style="display:none;">
			<div class="row fix-bs-row">
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Type</span><br />
					<select data-tags="true" row="#prc.qryGetLandInfo.CurrentRow-1#" class="form-control select2-2piece-new" name="landusecode" onchange="changeLandType(#prc.qryGetLandInfo.CurrentRow-1#,this)"> <!--- two-part-large select2-2piece-dropdown-new --->
						<option value="" data-val="&nbsp;" data-desc="Select..." data-cellname="lalval">Select...</option>
						<cfloop query="prc.qryLandUseCodes">
							<option value="#prc.qryLandUseCodes.code#" data-cellname="lalval" data-val="#prc.qryLandUseCodes.code#" data-desc="#trim(encodeForHTML(prc.qryLandUseCodes.landUseCodeDescription))#" <cfif prc.qryGetLandInfo.landUseCodeDescription EQ prc.qryLandUseCodes.landUseCodeDescription> selected</cfif>>#code#: #trim(encodeForHTML(landUseCodeDescription))#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Neighborhood Code</span><br />
					<select data-tags="true" class="form-control select2-3piece-new" name="neighborhoodCode" onchange="changeNeighborhood(#prc.qryGetLandInfo.CurrentRow-1#,this)" row="#prc.qryGetLandInfo.CurrentRow-1#">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.qryNeighborhoods">
							<option value="#prc.qryNeighborhoods.nbhdcd#" data-val="#prc.qryNeighborhoods.nbhdcd#" data-desc="#trim(encodeForHTML(prc.qryNeighborhoods.nbhdds))#" data-adjval="#NumberFormat(prc.qryNeighborhoods.nbhdfa,'9.999')#" <cfif prc.qryGetLandInfo.neighborhoodCode EQ prc.qryNeighborhoods.nbhdcd> selected</cfif>>#prc.qryNeighborhoods.nbhdcd#: #trim(encodeForHTML(prc.qryNeighborhoods.nbhdds))#</option>
						</cfloop>
					</select>
					<input  type="hidden" readonly id="neighborhoodAdjustment_#prc.qryGetLandInfo.CurrentRow-1#" value="#NumberFormat(prc.qryGetLandInfo.NeighborhoodAdjustment,".000")#">
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Appraised Value</span><br/>
					<span class="formatted-data-values" id="appraisedValue_#prc.qryGetLandInfo.CurrentRow-1#"></span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Frontage</span><br />
					<input type="text" name="front" class="form-control numeric" value="#NumberFormat(prc.qryGetLandInfo.front,"0.000")#" onchange="changeVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Back</span><br />
					<input type="text" name="back" class="form-control numeric" value="#NumberFormat(prc.qryGetLandInfo.back,"0.000")#" onchange="changeVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Depth</span><br />
					<input type="text" name="depth" class="form-control numeric" value="#NumberFormat(prc.qryGetLandInfo.depth,"0.000")#" onblur="updateDepthValue(#prc.qryGetLandInfo.CurrentRow-1#,this)" onchange="updateDepthValue(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox">

				</div>
				<div class="clearfix"></div>

				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Unit Type</span><br/>
					<select data-tags="true" name="unittypecode" id="unittypecode" class="form-control select2-2piece-new"  onchange="unitTypeChange(#prc.qryGetLandInfo.CurrentRow-1#,this)">
						<option data-val="" data-desc="Select One" data-cellname="laut" value="">Select One</option>
						<cfloop query="prc.qryLandCodes">
							<option data-val="#prc.qryLandCodes.code#" data-desc="#trim(encodeForHTML(prc.qryLandCodes.codeDescription))#" data-cellname="laut" value="#prc.qryLandCodes.code#"<cfif prc.qryGetLandInfo.unitTypeCode IS prc.qryLandCodes.code> selected</cfif>>#prc.qryLandCodes.code#: #prc.qryLandCodes.codeDescription#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Units</span><br/>
					<input type="text" name="numberUnits" id="numberunits_#prc.qryGetLandInfo.CurrentRow-1#" class="form-control numeric" value="#prc.qryGetLandInfo.numberUnits#" onchange="changeVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Unit Price</span> $<br />
					<input type="text" name="unitprice" value="#NumberFormat(prc.qryGetLandInfo.unitPrice,"0.00")#"  class="form-control numeric" onchange="changeVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Effective Rate</span><br/>
					<span class="formatted-data-values" id="effectiveBaseRate_#prc.qryGetLandInfo.CurrentRow-1#">#NumberFormat(prc.qryGetLandInfo.effectiveBaseRate,",.00")#</span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-2 databox" align="center">
					<span class="formatted-data-label">Capping Eligible?</span><br />
					<input type="checkbox" name="leflae" id="leflae" value="Y" <CFIF prc.qryGetLandInfo.leflae EQ "Y">checked</CFIF> onchange="changeCheckboxVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox" align="center">
					<span class="formatted-data-label">Fair Market Value?</span><br />
					<input type="checkbox" value="Y" name="leflfmvq" id="leflfmvq" <CFIF prc.qryGetLandInfo.leflfmvq EQ "Y">checked</CFIF> onchange="changeCheckboxVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox" align="center">
					<span class="formatted-data-label">Replacement?</span><br />
					<input type="checkbox" value="Y" name="lefldemol" id="lefldemol" <CFIF prc.qryGetLandInfo.lefldemol EQ "Y">checked</CFIF> onchange="changeCheckboxVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Market Unit Price</span><br />
					<input type="text" name="marketunitprice" value="#NumberFormat(prc.qryGetLandInfo.marketUnitPrice,"0.00")#"  class="form-control numeric" onchange="changeVal(#prc.qryGetLandInfo.CurrentRow-1#,this)">
				</div>

				<div class="col-xs-2 databox">
					<span class="formatted-data-label">AG Market Value</span><br />
					<span class="formatted-data-values" id="AGMarketValue_#prc.qryGetLandInfo.CurrentRow-1#">#NumberFormat(prc.qryGetLandInfo.marketValue,",.00")#</span>
				</div>
				<div class="clearfix">&nbsp;</div>

				<cfset currentRow = prc.qryGetLandInfo.CurrentRow-1 />
				<div class="row fix-bs-row add-in-box">
					<div class="col-xs-12">
						#renderView('land/viewlets/_landAdjustments')#
					</div>
					<div class="col-xs-12 text-right">
						<span class="land-line-adj-button" onclick="doLandPopup(#prc.qryGetLandInfo.CurrentRow#,#prc.qryGetLandInfo.landID#)" rel="tooltip" title="Click to alter Adjustments"><img src="/includes/images/v2/addEdit_blue.gif" alt="" width="156" height="26" border="0"></span>
					</div>
				</div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
						&nbsp;&nbsp;
						<a href="javascript:saveWDDX('itemNumber', #TRIM(prc.qryGetLandInfo.itemNumber)#, 'landData')"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
					</div>
				</div>
			</div>

		</div> <!--- id="edit_mode_item_#prc.qryGetLandInfo.currentrow#" --->
	</div> <!--- id="collapse#prc.qryGetLandInfo.currentrow#" --->
</cfoutput>

<!--- Revert Settings Modal --->
<div class="modal fade" id="modalLautChange" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Warning</h4>
			</div>
			<div class="modal-body">
				Changing the Unit Type will modify how the land value is calculated.  Are you sure you wish to change this?
			</div>
			<div class="modal-footer">
				<button type="button"  class="btn btn-default"  onclick="updateLautValue();">Yes</button>
				<button type="button"  class="btn btn-default"  onclick="killLautChange();">No</button>
			</div>
		</div>
	</div>
</div>
