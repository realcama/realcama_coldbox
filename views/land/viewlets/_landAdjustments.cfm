<cfoutput>
	<cfparam name="CurrentRow" default="1">
	<cfparam name="strOptions" default="">
	<cfparam name="PercentageAddOns" default="">
	<cfparam name="pointsAddOns" default="">
	<cfparam name="LumpSumAddOns" default="">
	<cfparam name="factorAddOns" default="">
	<cfparam name="RateAddOns" default="">
		<table class="land-udf-table">
		<thead>
			<tr>
				<td class="adjustmentCell"><span class="formatted-data-label">Adjustments</span></td>
				<td class="codeCell"></td>
				<td class="codeDescCell"></td>
				<td class="text-right smallCell formatted-data-label">Contributing<br>Percentage</td>
				<td class="text-right smallCell formatted-data-label">Points</td>
				<td class="text-right smallCell formatted-data-label">Lump Sum</td>
				<td class="text-right smallCell formatted-data-label">Factor</td>
				<td class="text-right smallCell formatted-data-label">Rate<br>Add-On</td>
			</tr>
		</thead>
		<tbody id="udf_#CurrentRow#">
			<CFIF LEN(TRIM(strOptions)) EQ 0><tr><td colspan="6">None</td></tr><CFELSE>#strOptions#</CFIF>
		</tbody>
		<tfoot class="adjustmentFooter">
			<tr>
				<td></td>
				<td></td>
				<td class="text-right formatted-data-label">Net Adjustments</td>
				<td class="text-right smallCell formatted-data-label" id="udf_netadj_percentage_#CurrentRow#">#PercentageAddOns#%</td>
				<td class="text-right smallCell formatted-data-label" id="udf_netadj_points_#CurrentRow#">#NumberFormat(pointsAddOns,"0.000")#</td>
				<td class="text-right smallCell formatted-data-label" id="udf_netadj_lumpsum_#CurrentRow#">#NumberFormat(LumpSumAddOns,"0.000")#</td>
				<td class="text-right smallCell formatted-data-label" id="udf_netadj_factor_#CurrentRow#">#NumberFormat(factorAddOns,"0.000")#</td>
				<td class="text-right smallCell formatted-data-label" id="udf_netadj_rateaddon_#CurrentRow#">#NumberFormat(RateAddOns,"0.000")#</td>
			</tr>
		</tfoot>
		</table>
</cfoutput>	