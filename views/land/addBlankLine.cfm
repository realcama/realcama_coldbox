<cfoutput>
<cfparam name="jsRowNumber" default="0">
<cftry>
<cfset jsRowNumber = evaluate(val(rc.itemNumber)-1) >

<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading#rc.itemNumber#" aria-expanded="false" >
	<table class="land-line-summary">
	<tr>
		<td width="10%" class="section-bar-label">###rc.itemNumber#</td>
		<td width="15%" align="right" class="section-bar-label">(Unsaved)</td>
		<td width="22%" align="right" class="section-bar-label"></td>
		<td width="16%" align="right" class="section-bar-label"></td>
		<td width="16%" class="section-bar-label"></td>
		<td width="16%" align="right" class="section-bar-label"><span id="finalLandViewValue2_#rc.itemNumber#"></span></td>
		<td width="5%" align="right" class="section-bar-label"></td>
	</tr>
	</table>
</div>

<div id="edit_mode_item_#rc.itemNumber#">
	<a name="edit_mode_item_#rc.itemNumber#"></a>
	<div class="row fix-bs-row">
		<div class="col-xs-4 databox">
			<strong>Type</strong><br />
			<select data-tags="true" row="#jsRowNumber#" class="form-control two-items-on-row select2-2piece-new" name="landusecode" onchange="changeLandType(#jsRowNumber#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select...">Select...</option>
				<cfloop query="prc.qryLandUseCodes">
					<option value="#prc.qryLandUseCodes.code#" data-val="#prc.qryLandUseCodes.code#" data-desc="#trim(encodeForHTML(prc.qryLandUseCodes.landUseCodeDescription))#">#prc.qryLandUseCodes.code#: #trim(encodeForHTML(prc.qryLandUseCodes.landUseCodeDescription))#</option>
				</cfloop>
			</select>
		</div>

		<div class="col-xs-6 databox">
			<strong>Neighborhood Code</strong><br />
			<select data-tags="true" row="#jsRowNumber#" class="form-control two-items-on-row select2-3piece-new" name="neighborhoodCode" onchange="changeNeighborhood(#jsRowNumber#,this)">
				<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
				<cfloop query="prc.qryNeighborhoods">
					<option value="#prc.qryNeighborhoods.nbhdcd#" data-val="#prc.qryNeighborhoods.nbhdcd#" data-desc="#trim(encodeForHTML(prc.qryNeighborhoods.nbhdds))#" data-adjval="#NumberFormat(prc.qryNeighborhoods.nbhdfa,'9.999')#">#prc.qryNeighborhoods.nbhdcd#: #trim(encodeForHTML(prc.qryNeighborhoods.nbhdds))#</option>
				</cfloop>
			</select>
			<input class="form-control two-part" type="hidden" readonly id="neighborhoodAdjustment_#rc.itemNumber#" value="0.000" rel="tooltip" title="Neighborhood Adjustment Factor">
		</div>

		<div class="col-xs-2 databox">
			<strong>Appraised Value</strong><br/>
			<span class="appraisedValue" id="finalLandValue_#rc.itemNumber#"></span>
		</div>
	</div>

	<div class="row fix-bs-row">
		<div class="col-xs-4 databox">
			<strong>Frontage</strong><br />
			<input type="text" name="front" class="form-control numeric" value="0.000" onblur="changeVal(#jsRowNumber#,this)" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox">
			<strong>Back</strong><br />
			<input type="text" name="back" class="form-control numeric" value="0.000" onblur="changeVal(#jsRowNumber#,this)" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox">
			<strong>Depth</strong><br />
			<input type="text" name="depth" class="form-control numeric" value="0.000" onblur="updateDepthValue(#jsRowNumber#,this)" onchange="updateDepthValue(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-2 databox">

		</div>
	</div>

	<div class="row fix-bs-row">
		<div class="col-xs-4 databox">
			<strong>Unit Type</strong><br/>
			<select name="laut" row="#jsRowNumber#" class="form-control select2-2piece-new" onchange="unitTypeChange(#jsRowNumber#,this)">
				<option data-val="" data-desc="Select One" data-cellname="laut" value="">Select One</option>
				<cfloop query="prc.qryLandCodes">
					<option data-val="#prc.qryLandCodes.code#" data-desc="#trim(encodeForHTML(prc.qryLandCodes.codeDescription))#" data-cellname="laut" value="#prc.qryLandCodes.code#">#prc.qryLandCodes.code#: #trim(encodeForHTML(prc.qryLandCodes.codeDescription))#</option>
				</cfloop>
			</select>
		</div>

		<div class="col-xs-3 databox">
			<strong>Units</strong><br/>
			<input type="text" name="numberUnits" id="numberunits_#rc.itemNumber#" class="form-control numeric" value="0.000" onblur="changeVal(#jsRowNumber#,this)" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox">
			<strong>Unit Price</strong> $<br />
			<input type="text" name="unitprice" value="0.000"  class="form-control numeric" onblur="changeVal(#jsRowNumber#,this)" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-2 databox">
			<strong>Effective Base Rate</strong><br/>
			<span id="effectiveBaseRate_#rc.itemNumber#"></span>
		</div>
	</div>

	<div class="row fix-bs-row">
		<div class="col-xs-2 databox" align="center">
			<strong>Capping Eligible?</strong><br />
			<input type="checkbox" name="capping" id="capping" value="Y">
		</div>
		<div class="col-xs-2 databox" align="center">
			<strong>Fair Market Value?</strong><br />
			<input type="checkbox" value="Y" name="leflfmvq" id="leflfmvq" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox" align="center">
			<strong>Replacement?</strong><br />
			<input type="checkbox" value="Y" name="lefldemol" id="lefldemol" onchange="changeVal(#jsRowNumber#,this)">
		</div>
		<div class="col-xs-3 databox">
			<strong>Market Unit Price</strong><br />
			<input type="text" name="marketunitprice" value="0.00"  class="form-control numeric" onblur="changeVal(#jsRowNumber#,this)" onchange="changeVal(#jsRowNumber#,this)">
		</div>

		<div class="col-xs-2 databox">
			<strong>AG Market Price</strong><br />
			<span id="marketValue_#rc.itemNumber#"></span>
		</div>
	</div>

	<div class="row fix-bs-row add-in-box">
		<div class="col-xs-12">
			<table class="land-udf-table">
			<thead>
				<tr>
					<td width="138"><strong>Adjustments</strong></td>
					<td width="156"></td>
					<td width="25%"></td>
					<td width="15%" class="text-right">Lump Sum</td>
					<td width="15%" class="text-right">Percentage</td>
					<td width="15%" class="text-right">Rate Add-On</td>
				</tr>
			</thead>
			<tbody id="udf_#rc.itemNumber#">
				<tr><td colspan="6">None</td></tr>
			</tbody>
			<tfoot class="adjustmentFooter">
				<tr>
					<td width="138"></td>
					<td width="156"></td>
					<td width="25%" class="text-right">Net Adjustments</td>
					<td width="15%" class="text-right" id="udf_netadj_lumpsum_#rc.itemNumber#">0.00</td>
					<td width="15%" class="text-rightlign" id="udf_netadj_percentage_#rc.itemNumber#">0.00</td>
					<td width="15%" class="text-right" id="udf_netadj_rateaddon_#rc.itemNumber#">0.00</td>
				</tr>
			</tfoot>
			</table>
		</div>
		<div class="col-xs-12">
			<div class="land-part-action-buttons">
				<span class="land-line-adj-button" onclick="doLandPopup(#rc.itemNumber#,'X_#rc.itemNumber#')" rel="tooltip" title="Click to alter Adjustments"><img src="/includes/images/v2/addEdit_blue.gif" alt="" width="156" height="26" border="0"></span>
				<!--- <a href="javascript:editThisRow(#prc.qryGetLandInfo.currentrow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a> --->
			</div>
		</div>
	</div>

	<div class="row fix-bs-row">
		<div class="col-xs-12">
			<div class="land-part-action-buttons">
				<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
				&nbsp;&nbsp;
				<a href="javascript:saveWDDX('itemNumber', #rc.itemNumber#, 'landData')"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
			</div>
		</div>

	</div>
</div>
<!--- #addAsset('/includes/js/landinfo.js')# --->

	<cfcatch>
		<br><cfdump var="#cfcatch#" label="cfcatch" expand="yes">
	</cfcatch>
</cftry>
</cfoutput>