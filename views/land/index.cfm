<cfoutput>
<cfscript>
	showTabOptions = "land";
	iPageNumber = 0;
</cfscript>

	<cfif prc.qryGetLandInfo.recordcount EQ 0>
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				No Land records found for the selected parcel
			</div>
		</div>
	<cfelse>
		<!--- <cfdump var="#prc.qryUDFs#" label="qryUDFs" expand="no"> --->
		<!--- <cfdump var="#prc.qryAllAdjustments#" label="qryAllAdjustments" expand="no"> --->
		#renderView('/_templates/alertMsgs')#
		<CFIF Controller.getSetting( "bShowPacketLinks")>
			<div class="alert alert-info">
				<a href="javascript:void(0);" title="Land Packet" onclick="WriteRaw(landData);">Land Packet</A>&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0);" title="UDF Packet" onclick="WriteRaw(udfData);">UDF Packet</A>&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0);" title="All Ajustments Packet" onclick="WriteRaw(qryAllAdjustments);">All Ajustments Packet</A>&nbsp;&nbsp;&nbsp;
			</div>
		</CFIF>
		<cfset tab = "land">
		#renderView('/_templates/audit-boxes')#

		<form action="#event.buildLink('land/landSave')#" method="post" name="frmSave" id="frmSave">
			<input type="hidden" name="parcelID" id="parcelID" value="#prc.qryPersistentInfo.parcelID#">
			<input type="hidden" name="taxYear" id="taxYear" value="#prc.qryPersistentInfo.taxYear#">
			<input type="hidden" name="plvalue" id="plvalue" value="#prc.qryPersistentInfoTotals.plvalue#">
			<input type="hidden" name="agtotal" id="agtotal" value="#prc.qryPersistentInfoTotals.agtotal#">
			<input type="hidden" name="pbvalue" id="pbvalue" value="#prc.qryPersistentInfoTotals.pbvalue#">
			<input type="hidden" name="pxvalbld" id="pxvalbld" value="#prc.qryPersistentInfoTotals.pxvalbld#">

			<input type="hidden" name="totalLandLines" id="totalLandLines" value="#prc.qryPersistentInfoCounts.totalLandLines#">
			<input type="hidden" name="totalAgLines" id="totalAgLines" value="#prc.qryPersistentInfoCounts.aglines#">
			<input type="hidden" name="totalBuildingLines" id="totalBuildingLines" value="#prc.qryPersistentInfoCounts.totalBuildingLines#">
			<input type="hidden" name="totalFeaturesLines" id="totalFeaturesLines" value="0">

			<input type="hidden" name="totalsqfeet" id="totalsqfeet" value="#prc.qryAppraisalRecords.totsqft#">
			<input type="hidden" name="totalacres" id="totalacres" value="#prc.qryAppraisalRecords.placres#">

			<input type="hidden" name="landData" id="landData" value="">
			<input type="hidden" name="udfData" id="udfData" value="">
		</form>

		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				#renderView('_templates/miniNavigationBar')#
				Land Lines <span id="totalLandLines">(#prc.qryGetLandInfo.recordcount#)</span>
			</div>

			<div id="expander-holders" class="panel panel-default">
				<cfloop query="prc.qryGetLandInfo">
					<cfif prc.qryGetLandInfo.CurrentRow MOD rc.page_size EQ 1>
						<cfset iPageNumber = iPageNumber + 1>
						<div id="page-#iPageNumber#" <cfif rc.page neq iPageNumber>class="hide"</cfif>>
					</cfif>

					#renderView('land/viewlets/_landline')#

					<cfif prc.qryGetLandInfo.CurrentRow MOD rc.page_size EQ 0>
						</div>
					</cfif>
				</cfloop>

				<cfif prc.qryGetLandInfo.CurrentRow MOD rc.page_size NEQ 0>
					</div> <!--- This ensures the ' div id="page-#iPageNumber#" ' to be closed correctly --->
				</cfif>
			</div> <!--- div id="expander-holders" class="panel panel-default" --->

			<!--- Revert Settings Modal --->
			<div class="modal fade" id="modalCloneLine" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form action="cloneLandLine.cfm" method="post" name="frmClone">
				<input type="hidden" name="ParcelID" value="#prc.qryPersistentInfo.parcelID#">
				<input type="hidden" name="taxYear" value="#prc.qryPersistentInfo.taxYear#">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Clone</h4>
						</div>
						<div class="modal-body">
							Which item do you wish to clone?</p>
							<div class="btn-group" data-toggle="buttons">
								<cfloop query="prc.qryGetLandInfo">
									<label class="btn btn-default">
										<input type="radio" value="#prc.qryGetLandInfo.landID#" name="la_id">#prc.qryGetLandInfo.CurrentRow#
									</label>
								</cfloop>
							</div>
						</div>
						<div class="modal-footer">
							<a href="##" onclick="frmClone.submit()"  class="btn btn-primary">Clone</a>
							<a href="##" data-dismiss="modal" class="btn btn-primary">Cancel</a>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</cfif>

	<script type="text/javascript">
		var ParcelID = '#prc.stPersistentInfo.ParcelID#';
		var taxYear = #prc.stPersistentInfo.taxYear#;
		var fBuilding = #prc.qryPersistentInfoTotals.pbvalue#;
		var fExtraFeatures = #prc.qryPersistentInfoTotals.PXVALBLD#;
		var addBlankLineURL = '#event.buildLink('land/addBlankLine')#';
		var totalPages = #ceiling(prc.qryGetLandInfo.RecordCount / rc.page_size)#;
		var iCurrentPage = 1;

		<CFWDDX ACTION="CFML2JS" INPUT="#prc.qryGetLandInfo#" TOPLEVELVARIABLE="landData">
		<CFWDDX ACTION="CFML2JS" INPUT="#prc.qryUDFs#" TOPLEVELVARIABLE="udfData">
		<CFWDDX ACTION="CFML2JS" INPUT="#prc.qryAllAdjustments#" TOPLEVELVARIABLE="qryAllAdjustments">

	</script>
	#addAsset('/includes/js/header.js,/includes/js/landinfo.js,/includes/js/formatSelect.js')#
</cfoutput>