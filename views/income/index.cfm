<!---
<cfdump var="#prc.getIncomeInfo#">
--->

<!---
<CFIF prc.getIncomeInfo.RecordCount EQ 0>
	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			No income records found for the selected parcel
		</div>
	</div>
<CFELSE>
--->
<script src="/includes/js/income.js"></script>

<div class="container-border container-spacer">
	<div class="table-header content-bar-label">			
		<CFSET showTabOptions = "income">				
		<cfoutput>#renderView('_templates/miniNavigationBar')#</cfoutput>
		Income
	</div>
	<cfoutput>
		<div id="expander-holders" class="panel panel-default">
		
			<div id="view_mode_item_1" class="row fix-bs-row">
			
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >						
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading1" data-toggle="collapse">
						<td width="95%" class="section-bar-label">&nbsp;</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>
				<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">		
				
					<div class="row fix-bs-row">
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Appr By</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inlapby#</span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Appr Date</span><br/>
							<span class="formatted-data-values">#DateFormat(fixDbDate(prc.getIncomeInfo.inlapdt),"m/d/yyyy")#</span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Tax District</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.intaxd#</span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Class</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inicls#</span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Neighborhood</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.innbhd#: #prc.getIncomeInfo.CodeDescription#</span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">EYB</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.ineyb#</span>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>
			
			
			
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >			
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading2" data-toggle="collapse">
						<td width="95%" class="section-bar-label">Income and Expenses</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>
				<div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading2">						
					<div class="row fix-bs-row">
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Units</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inunit#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Unit Type</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inut#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Unit Rate</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inrate#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inratef#</span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Potential Gross Income</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.intpot,",")#</span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Vacancy/Collection</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.invac#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.invacf#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Misc Income</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inmisc,",")#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inmiscf#</span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Effective Gross Income</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inteff,",")#</span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Expenses Operating</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inexpo,",")#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inexpof#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Expenses Reserve</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inexpr,"0")#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inexprf#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Net Income</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.intnet,",")#</span>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>	
						
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading3" data-toggle="collapse">
						<td width="95%" class="section-bar-label">Capitalization Information</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>
				<div id="collapse3" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading3">						
					<div class="row fix-bs-row">
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Effective Tax Rate</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.intaxr#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Yield</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inyield#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Remaining Economic Life</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inrel#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Growth</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.ingrow#</span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Term</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.interm#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">End Cap</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inecap#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">End Expense</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.ineexprt,",")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">D.C.F</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inincm9#</span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">D.C.M.</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.indcm,",")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">G.R.M.</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.ingrm#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">% to Land</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inlndpct#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Land</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inland,"0")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Yes / No</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inlandf#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Building</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inbldg,"0")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Yes / No</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inbldgf#</span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Other Values</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inother,"0")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inotherf#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Minus / Plus</span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>	
			
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading4" data-toggle="collapse">
						<td width="95%" class="section-bar-label">Capitalized Values</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>				
				<div id="collapse4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading4">		
					<div class="row fix-bs-row">
						
				
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">GRM</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm7,",")#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">DCM</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm8,",")#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">DCF</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm9,",")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">PRP</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm5,",")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">LRA</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm1,",")#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">LRS</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm2,",")#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">BRA</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm3,",")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">BRS</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inincm4,",")#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-2 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Selected Method</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.inmeth#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Value Method</span><br/>
							<span class="formatted-data-values">#prc.getIncomeInfo.ValuedByFlag#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Appraised Value</span><br/>
							<span class="formatted-data-values">#NumberFormat(prc.getIncomeInfo.inittotal,",")#</span>
						</div>
						
						
						
						<div class="clearfix"></div>
						<div class="col-xs-12">
							<div class="land-part-action-buttons">
								<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
							</div>
						</div>	
						
					</div>
					
				</div>
			</div>	
			
			<div id="edit_mode_item_1" class="row fix-bs-row" style="display:none;">
			
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >						
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading5" data-toggle="collapse">
						<td width="95%" class="section-bar-label">&nbsp;</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>
				<div id="collapse5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading5">		
				
					<div class="row fix-bs-row">
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Appr By</span><br/>
							<span class="formatted-data-values"><input type="text" name="inlapby" class="form-control" value="#prc.getIncomeInfo.inlapby#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Appr Date</span><br/>
							<span class="formatted-data-values"><input type="text" name="inlapdt" class="form-control" value="#DateFormat(fixDbDate(prc.getIncomeInfo.inlapdt),'m/d/yyyy')#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Tax District</span><br/>
							<span class="formatted-data-values"><input type="text" name="intaxd" class="form-control" value="#prc.getIncomeInfo.intaxd#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Class</span><br/>
							<span class="formatted-data-values"><input type="text" name="inicls" class="form-control" value="#prc.getIncomeInfo.inicls#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Neighborhood</span><br/>
							<span class="formatted-data-values"><input type="text" name="innbhd" class="form-control" value="#prc.getIncomeInfo.innbhd#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">EYB</span><br/>
							<span class="formatted-data-values"><input type="text" name="ineyb" class="form-control" value="#prc.getIncomeInfo.ineyb#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>
			
			
			
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >			
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading6" data-toggle="collapse">
						<td width="95%" class="section-bar-label">Income and Expenses</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>
				<div id="collapse6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading6">						
					<div class="row fix-bs-row">
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Units</span><br/>
							<span class="formatted-data-values"><input type="text" name="inunit" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inunit,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Unit Type</span><br/>
							<span class="formatted-data-values"><input type="text" name="inut" class="form-control" value="#prc.getIncomeInfo.inut#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Unit Rate</span><br/>
							<span class="formatted-data-values"><input type="text" name="inrate" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inrate,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values"><input type="text" name="inratef" class="form-control" value="#prc.getIncomeInfo.inratef#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Potential Gross Income</span><br/>
							<span class="formatted-data-values"><input type="text" name="intpot" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.intpot,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Vacancy/Collection</span><br/>
							<span class="formatted-data-values"><input type="text" name="invac" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.invac,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values"><input type="text" name="invacf" class="form-control" value="#prc.getIncomeInfo.invacf#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Misc Income</span><br/>
							<span class="formatted-data-values"><input type="text" name="inmisc" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inmisc,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values"><input type="text" name="inmiscf" class="form-control" value="#prc.getIncomeInfo.inmiscf#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Effective Gross Income</span><br/>
							<span class="formatted-data-values"><input type="text" name="inteff" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inteff,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Expenses Operating</span><br/>
							<span class="formatted-data-values"><input type="text" name="inexpo" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inexpo,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values"><input type="text" name="inexpof" class="form-control" value="#prc.getIncomeInfo.inexpof#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Expenses Reserve</span><br/>
							<span class="formatted-data-values"><input type="text" name="inexpr" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inexpr,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values"><input type="text" name="inexprf" class="form-control" value="#prc.getIncomeInfo.inexprf#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Net Income</span><br/>
							<span class="formatted-data-values"><input type="text" name="intnet" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.intnet,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>	
						
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading7" data-toggle="collapse">
						<td width="95%" class="section-bar-label">Capitalization Information</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>
				<div id="collapse7" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading7">						
					<div class="row fix-bs-row">
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Effective Tax Rate</span><br/>
							<span class="formatted-data-values"><input type="text" name="intaxr" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.intaxr,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Yield</span><br/>
							<span class="formatted-data-values"><input type="text" name="inyield" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inyield,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Remaining Economic Life</span><br/>
							<span class="formatted-data-values"><input type="text" name="inrel" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inrel,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Growth</span><br/>
							<span class="formatted-data-values"><input type="text" name="ingrow" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.ingrow,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Term</span><br/>
							<span class="formatted-data-values"><input type="text" name="interm" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.interm,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">End Cap</span><br/>
							<span class="formatted-data-values"><input type="text" name="inecap" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inecap,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">End Expense</span><br/>
							<span class="formatted-data-values"><input type="text" name="ineexprt" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.ineexprt,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">D.C.F</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm9" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inincm9,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">D.C.M.</span><br/>
							<span class="formatted-data-values"><input type="text" name="indcm" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.indcm,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">G.R.M.</span><br/>
							<span class="formatted-data-values"><input type="text" name="ingrm" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.ingrm,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">% to Land</span><br/>
							<span class="formatted-data-values"><input type="text" name="inlndpct" class="form-control numeric" value="#NumberFormat(prc.getIncomeInfo.inlndpct,".99")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Land</span><br/>
							<span class="formatted-data-values"><input type="text" name="inland" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inland,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Yes / No</span><br/>
							<span class="formatted-data-values"><input type="text" name="inlandf" class="form-control" value="#prc.getIncomeInfo.inlandf#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Building</span><br/>
							<span class="formatted-data-values"><input type="text" name="inbldg" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inbldg,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Yes / No</span><br/>
							<span class="formatted-data-values"><input type="text" name="inbldgf" class="form-control" value="#prc.getIncomeInfo.inbldgf#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Other Values</span><br/>
							<span class="formatted-data-values"><input type="text" name="inother" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inother,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Type</span><br/>
							<span class="formatted-data-values"><input type="text" name="inotherf" class="form-control" value="#prc.getIncomeInfo.inotherf#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Minus / Plus</span><br/>
							<span class="formatted-data-values"><input type="text" name="" class="form-control" value="" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>	
			
				<div class="panel-heading" role="tab" class="panel-collapse collapse" >
					<table class="land-line-summary">
					<tr class="section-bar-expander" data-parent="heading8" data-toggle="collapse">
						<td width="95%" class="section-bar-label">Capitalized Values</td>						
						<td width="5%" align="right" class="section-bar-label"></td>
					</tr>
					</table>
				</div>				
				<div id="collapse8" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading8">		
					<div class="row fix-bs-row">
					
				
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">GRM</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm7" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm7,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">DCM</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm8" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm8,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">DCF</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm9" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm9,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">PRP</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm5" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm5,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">LRA</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm1" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm1,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">LRS</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm2" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm2,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">BRA</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm3" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm3,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">BRS</span><br/>
							<span class="formatted-data-values"><input type="text" name="inincm4" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inincm4,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-xs-2 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Selected Method</span><br/>
							<span class="formatted-data-values"><input type="text" name="inmeth" class="form-control" value="#prc.getIncomeInfo.inmeth#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label"></span><br/>
							<span class="formatted-data-values"></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Value Method</span><br/>
							<span class="formatted-data-values"><input type="text" name="ValuedByFlag" class="form-control" value="#prc.getIncomeInfo.ValuedByFlag#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Appraised Value</span><br/>
							<span class="formatted-data-values"><input type="text" name="inittotal" class="form-control numeric-onlyint" value="#NumberFormat(prc.getIncomeInfo.inittotal,"0")#" <!---- onchange="changeVal(#prc.getIncomeInfo.CurrentRow-1#,this)" --->></span>
						</div>
						
						
						
						<div class="clearfix"></div>
						<div class="col-xs-12">
							<div class="land-part-action-buttons">
								<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
								&nbsp;&nbsp;
								<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>	
							</div>
						</div>	
						
					</div>
					
				</div>
			
			
			</div>	
		</cfoutput>	
	</div>
</div>

