
<!--- <cfdump var="#prc.qryGetCorrelationInfo#"> --->
<CFIF prc.qryGetCorrelationInfo.Check1 EQ prc.qryGetCorrelationInfo.Check2
	AND prc.qryGetCorrelationInfo.Check2 EQ prc.qryGetCorrelationInfo.Check3
	AND prc.qryGetCorrelationInfo.Check3 EQ prc.qryGetCorrelationInfo.Check4>

	<CFSET bUseBlendedMarker = FALSE>
<CFELSE>
	<CFSET bUseBlendedMarker = TRUE>
</CFIF>

<cfoutput>

<script src="/includes/js/correlation.js?g=#now()#"></script>

<div class="panel-heading" role="tab" class="panel-collapse collapse" >
	<table class="land-line-summary">
	<tr>
		<td width="95%" class="section-bar-label">Correlation Data</td>
		<!--- <td width="5%" align="right"><a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="##collapse#qryRecords.currentrow#"></a></td> --->
	</tr>
	</table>
</div>

<div id="view_mode_item_1">
	<table id="correlation-view-table" class="table">
		<tr>
			<td width="78">&nbsp;</td>
			<td width="138"><div class="text-center formatted-data-label">Cost</div></td>
			<td width="138"><div class="text-center formatted-data-label">MICA</div></td>
			<td width="138"><div class="text-center formatted-data-label">Income</div></td>
			<td width="138"><div class="text-center formatted-data-label">Market</div></td>
			<td width="138"><div class="text-center formatted-data-label">Override</div></td>
			<td width="138"><div class="text-center formatted-data-label">Blended</div></td>
		</tr>

		<tr>
			<td><div class="text-right formatted-data-label">Land</div></td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.costLandValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 1>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.micaLandValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 2>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.incomeLandValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 3>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.marketLandValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 4>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.overrideLandValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 5>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.blendedLandValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 6>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
		</tr>



		<tr>
			<td><div class="text-right formatted-data-label">Land - AG</div></td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.costLandAgricultureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check2 EQ 1>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.micaLandAgricultureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check2 EQ 2>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.incomeLandAgricultureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check2 EQ 3>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.marketLandAgricultureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 4>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.overrideLandAgricultureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check2 EQ 5>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.blendedLandAgricultureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check2 EQ 6>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
		</tr>



		<tr>
			<td><div class="text-right formatted-data-label">Building</div></td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.costBuildingValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check3 EQ 1>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.micaBuildingValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check3 EQ 2>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.incomeBuildingValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check3 EQ 3>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.marketBuildingValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check3 EQ 4>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.overrideBuildingValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check3 EQ 5>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.blendedBuildingValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check3 EQ 6>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
		</tr>



		<tr>
			<td><div class="text-right formatted-data-label">Features</div></td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.costFeatureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check4 EQ 1>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.micaFeatureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check4 EQ 2>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">0</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check4 EQ 3>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.marketFeatureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check4 EQ 4>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.overrideFeatureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check4 EQ 5>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.blendedFeatureValue,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check4 EQ 6>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
		</tr>



		<tr style="background: ##efefef;">
			<td><div class="text-right"><span class="formatted-data-label">TOTALS</span></div></td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.costtotal,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 1 AND prc.qryGetCorrelationInfo.check2 EQ 1 AND prc.qryGetCorrelationInfo.check3 EQ 1 AND prc.qryGetCorrelationInfo.check4 EQ 1>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.micatotal,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check  <CFIF prc.qryGetCorrelationInfo.check1 EQ 2 AND prc.qryGetCorrelationInfo.check2 EQ 2 AND prc.qryGetCorrelationInfo.check3 EQ 2 AND prc.qryGetCorrelationInfo.check4 EQ 2>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.incometotal,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 3 AND prc.qryGetCorrelationInfo.check2 EQ 3 AND prc.qryGetCorrelationInfo.check3 EQ 3 AND prc.qryGetCorrelationInfo.check4 EQ 3>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.markettotal,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 4 AND prc.qryGetCorrelationInfo.check2 EQ 4 AND prc.qryGetCorrelationInfo.check3 EQ 4 AND prc.qryGetCorrelationInfo.check4 EQ 4>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.overridetotal,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF prc.qryGetCorrelationInfo.check1 EQ 5 AND prc.qryGetCorrelationInfo.check2 EQ 5 AND prc.qryGetCorrelationInfo.check3 EQ 5 AND prc.qryGetCorrelationInfo.check4 EQ 5>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
			<td>
				<span class="input-group text-right">
					<label class="formatted-data-values">#NumberFormat(prc.qryGetCorrelationInfo.blendedtotal,",")#</label>
					<span class="correlation-input-group input-group-addon">
						<div class="nothing-check <CFIF bUseBlendedMarker>glyphicon glyphicon-ok</CFIF>"></div>
					</span>
				</span>
			</td>
		</tr>
		<tr>
			<td colspan="7"></td>
		</tr>
		<tr>
			<td colspan="3" class="text-center">
				<span class="glyphicon glyphicon-asterisk"></span> <span class="formatted-data-label">Override Expires after the </span>
				<input type="text" name="peflovrthru" id="peflovrthru" value="#prc.qryGetCorrelationInfo.peflovrthru#" readonly class="correlation-date-entry form-control">
				<span class="formatted-data-label"> roll</span>
			</td>
			<td colspan="4" class="text-center">
				<span class="formatted-data-label">Indicated Appraised Value</span>
				<input type="text" name="indicatedvalue2" id="indicatedvalue_view" value="-1" readonly class="correlation-entry numeric text-right form-control">
			</td>
		</tr>
	</table>


	<div class="land-part-action-buttons">
		<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
	</div>

	<br clear="all"/>

</div>

<form action="#event.buildLink('correlation/correlationSave')#" method="post" name="frmCorrelation" id="frmCorrelation">
<input type="hidden" name="ParcelID" value="#prc.qryPersistentInfo.parcelID#">
<input type="hidden" name="correlationID" value="#prc.qryGetCorrelationInfo.correlationID#">
<input type="hidden" name="taxYear" value="#prc.qryPersistentInfo.taxYear#">
<div id="edit_mode_item_1" style="display:none;">
	<table id="correlation-edit-table" class="table">
		<tr>
			<td width="78">&nbsp;</td>
			<td width="138"><div class="text-center formatted-data-label">Cost</div></td>
			<td width="138"><div class="text-center formatted-data-label">MICA</div></td>
			<td width="138"><div class="text-center formatted-data-label">Income</div></td>
			<td width="138"><div class="text-center formatted-data-label">Market</div></td>
			<td width="138"><div class="text-center formatted-data-label">Override</div></td>
			<td width="138"><div class="text-center formatted-data-label">Blended</div></td>
		</tr>
		<tr>
			<td><div class="formatted-data-label text-right">Land</div></td>
			<td>
				<span class="input-group">
					<input type="text" name="land_cost" id="land_cost" value="#prc.qryGetCorrelationInfo.blendedFeatureValue#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land" <CFIF prc.qryGetCorrelationInfo.check1 EQ 1 OR prc.qryGetCorrelationInfo.check3 EQ 1>checked<cfelse>disabled</CFIF> value="1"  onchange="checkData();calcTotals();" data-toggle="tooltip" data-placement="top" title="Controlled by Building Cost">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_mica" id="land_mica" value="#VAL(prc.qryGetCorrelationInfo.micaLandValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land" value="2" <CFIF prc.qryGetCorrelationInfo.check1 EQ 2>checked</CFIF> onchange="checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_income" id="land_income" value="#VAL(prc.qryGetCorrelationInfo.incomeLandValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land" value="3" <CFIF prc.qryGetCorrelationInfo.check1 EQ 3>checked</CFIF> onclick="setAllToIncome();" onchange="setAllToIncome();checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_market" id="land_market" value="#VAL(prc.qryGetCorrelationInfo.marketLandValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land" disabled value="4" onchange="checkData();calcTotals();" data-toggle="tooltip" data-placement="top" title="Controlled by Building Market"></span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_override" id="land_override" <CFIF prc.qryGetCorrelationInfo.check1 NEQ 5>readonly</CFIF> value="#VAL(prc.qryGetCorrelationInfo.overrideLandValue)#" class="correlation-entry correlation-allow-entry numeric text-right form-control" onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land" value="5" <CFIF prc.qryGetCorrelationInfo.check1 EQ 5>checked</CFIF>  onchange="checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_blended" id="land_blended" value="#VAL(prc.qryGetCorrelationInfo.blendedLandValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<div class="nothing-check"></div>
					</span>
				</span>
			</td>
		</tr>



		<tr>
			<td><div class="formatted-data-label text-right">Land - AG</div></td>
			<td>
				<span class="input-group">
					<input type="text" name="land_ag_cost" id="land_ag_cost" value="#VAL(prc.qryGetCorrelationInfo.costLandAgricultureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land_ag"  <CFIF prc.qryGetCorrelationInfo.check2 EQ 2 OR prc.qryGetCorrelationInfo.check3 EQ 1>checked<cfelse>disabled</CFIF> value="1" onchange="checkData();calcTotals();" data-toggle="tooltip" data-placement="top" title="Controlled by Building Cost">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_ag_mica" id="land_ag_mica" value="#VAL(prc.qryGetCorrelationInfo.micaLandAgricultureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land_ag" value="2" <CFIF prc.qryGetCorrelationInfo.check2 EQ 2>checked</CFIF> onchange="checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_ag_income" id="land_ag_income" value="#VAL(prc.qryGetCorrelationInfo.marketLandAgricultureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land_ag" value="3" <CFIF prc.qryGetCorrelationInfo.check2 EQ 3>checked</CFIF> onclick="setAllToIncome();" onchange="setAllToIncome();checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_ag_market" id="land_ag_market" value="#VAL(prc.qryGetCorrelationInfo.incomeLandAgricultureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land_ag" disabled value="4" <CFIF prc.qryGetCorrelationInfo.check2 EQ 4>checked</CFIF> onchange="checkData();calcTotals();" data-toggle="tooltip" data-placement="top" title="Controlled by Building Market"></span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_ag_override" id="land_ag_override" value="#VAL(prc.qryGetCorrelationInfo.overrideLandAgricultureValue)#" <CFIF prc.qryGetCorrelationInfo.check2 NEQ 5>readonly</CFIF> class="correlation-entry correlation-allow-entry numeric text-right form-control" onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="land_ag" value="5" <CFIF prc.qryGetCorrelationInfo.check2 EQ 5>checked</CFIF> onchange="checkData();calcTotals();" >
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="land_ag_blended" id="land_ag_blended" value="#VAL(prc.qryGetCorrelationInfo.blendedLandAgricultureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<div class="nothing-check"></div>
					</span>
				</span>
			</td>
		</tr>



		<tr>
			<td><div class="formatted-data-label text-right">Building</div></td>
			<td>
				<span class="input-group">
					<input type="text" name="building_cost" id="building_cost" value="#VAL(prc.qryGetCorrelationInfo.costBuildingValue)#" class="correlation-entry numeric text-right form-control" <CFIF prc.qryGetCorrelationInfo.check3 NEQ 1>readonly</CFIF> onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="building" value="1" <CFIF prc.qryGetCorrelationInfo.check3 EQ 1>checked</CFIF> onchange="fixRadios(1);checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="building_mica" id="building_mica" value="#VAL(prc.qryGetCorrelationInfo.micaBuildingValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="building" value="2" <CFIF prc.qryGetCorrelationInfo.check3 EQ 2>checked</CFIF> onchange="fixRadios(2);checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="building_income" id="building_income" value="#VAL(prc.qryGetCorrelationInfo.incomeBuildingValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="building" value="3" <CFIF prc.qryGetCorrelationInfo.check3 EQ 3>checked</CFIF> onclick="setAllToIncome();" onchange="setAllToIncome();fixRadios(3);checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="building_market" id="building_market" value="#VAL(prc.qryGetCorrelationInfo.marketBuildingValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="building" value="4" <CFIF prc.qryGetCorrelationInfo.check2 EQ 4>checked</CFIF> onchange="fixRadios(4);checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="building_override" id="building_override" value="#VAL(prc.qryGetCorrelationInfo.overrideBuildingValue)#" <CFIF prc.qryGetCorrelationInfo.check3 NEQ 5>readonly</CFIF> class="correlation-entry correlation-allow-entry numeric text-right form-control" onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="building" value="5" <CFIF prc.qryGetCorrelationInfo.check3 EQ 5>checked</CFIF> onchange="fixRadios(5);checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="building_blended" id="building_blended" value="#VAL(prc.qryGetCorrelationInfo.blendedBuildingValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<div class="nothing-check"></div>
					</span>
				</span>
			</td>
		</tr>



		<tr>
			<td><div class="formatted-data-label text-right">Features</div></td>
			<td>
				<span class="input-group">
					<input type="text" name="features_cost" id="features_cost" value="#VAL(prc.qryGetCorrelationInfo.costFeatureValue)#" class="correlation-entry numeric text-right form-control" <CFIF prc.qryGetCorrelationInfo.check4 NEQ 1>readonly</CFIF> onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="features" value="1" <CFIF prc.qryGetCorrelationInfo.check4 EQ 1>checked</CFIF> onchange="checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="features_mica" id="features_mica" value="#VAL(prc.qryGetCorrelationInfo.micaFeatureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="features" value="2" <CFIF prc.qryGetCorrelationInfo.check4 EQ 2>checked</CFIF> onchange="checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="features_income" id="features_income" class="correlation-entry numeric text-right form-control" value="0" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="features" disabled <CFIF prc.qryGetCorrelationInfo.check4 EQ 3>checked</CFIF> value="3"  data-toggle="tooltip" data-placement="top" title="N/A">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="features_market" id="features_market" value="#VAL(prc.qryGetCorrelationInfo.marketFeatureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="features" value="4" <CFIF prc.qryGetCorrelationInfo.check4 EQ 4>checked</CFIF> onchange="checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="features_override" id="features_override" value="#VAL(prc.qryGetCorrelationInfo.overrideFeatureValue)#" <CFIF prc.qryGetCorrelationInfo.check4 NEQ 5>readonly</CFIF> class="correlation-entry correlation-allow-entry numeric text-right form-control" onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<input type="radio" name="features" value="5" <CFIF prc.qryGetCorrelationInfo.check4 EQ 5>checked</CFIF> onchange="checkData();calcTotals();">
					</span>
				</span>
			</td>
			<td>
				<span class="input-group">
					<input type="text" name="features_blended" id="features_blended" value="#VAL(prc.qryGetCorrelationInfo.blendedFeatureValue)#" class="correlation-entry numeric text-right form-control" readonly onchange="noNullsEntry(this);checkData();calcTotals();">
					<span class="input-group-addon">
						<div class="nothing-check"></div>
					</span>
				</span>
			</td>
		</tr>
		<tr style="background: ##efefef;">
			<td><div class="text-right"><span class="formatted-data-label">TOTALS</span></div></td>
			<td>
				<span class="input-group"><input type="text" name="cost_total" id="cost_total" value="#NumberFormat(prc.qryGetCorrelationInfo.costtotal,',')#" class="correlation-entry numeric text-right form-control" readonly><span class="input-group-addon"><span id="cost-total-check" class="<CFIF prc.qryGetCorrelationInfo.check1 EQ 1 AND prc.qryGetCorrelationInfo.check2 EQ 1 AND prc.qryGetCorrelationInfo.check3 EQ 1 AND prc.qryGetCorrelationInfo.check4 EQ 1>glyphicon glyphicon-ok</CFIF>"></span></span>
			</td>
			<td>
				<span class="input-group"><input type="text" name="mica_total" id="mica_total" value="#NumberFormat(prc.qryGetCorrelationInfo.micatotal,',')#" class="correlation-entry numeric text-right form-control" readonly><span class="input-group-addon"><span id="mica-total-check" class="<CFIF prc.qryGetCorrelationInfo.check1 EQ 2 AND prc.qryGetCorrelationInfo.check2 EQ 2 AND prc.qryGetCorrelationInfo.check3 EQ 2 AND prc.qryGetCorrelationInfo.check4 EQ 2>glyphicon glyphicon-ok</CFIF>"></span></span>
			</td>
			<td>
				<span class="input-group"><input type="text" name="income_total" id="income_total" value="#NumberFormat(prc.qryGetCorrelationInfo.incometotal,',')#" class="correlation-entry numeric text-right form-control" readonly><span class="input-group-addon"><span id="income-total-check" class="<CFIF prc.qryGetCorrelationInfo.check1 EQ 3 AND prc.qryGetCorrelationInfo.check2 EQ 3 AND prc.qryGetCorrelationInfo.check3 EQ 3 AND prc.qryGetCorrelationInfo.check4 EQ 2>glyphicon glyphicon-ok</CFIF>"></span></span>
			</td>
			<td>
				<span class="input-group"><input type="text" name="market_total" id="market_total" value="#NumberFormat(prc.qryGetCorrelationInfo.markettotal,',')#" class="correlation-entry numeric text-right form-control" readonly><span class="input-group-addon"><span id="market-total-check" class="<CFIF prc.qryGetCorrelationInfo.check1 EQ 4 AND prc.qryGetCorrelationInfo.check2 EQ 4 AND prc.qryGetCorrelationInfo.check3 EQ 4 AND prc.qryGetCorrelationInfo.check4 EQ 4>glyphicon glyphicon-ok</CFIF>"></span></span>
			</td>
			<td>
				<span class="input-group"><input type="text" name="override_total" id="override_total" value="#NumberFormat(prc.qryGetCorrelationInfo.overridetotal,',')#" class="correlation-entry numeric text-right form-control" readonly><span class="input-group-addon"><span id="override-total-check" class="<CFIF prc.qryGetCorrelationInfo.check1 EQ 5 AND prc.qryGetCorrelationInfo.check2 EQ 5 AND prc.qryGetCorrelationInfo.check3 EQ 5 AND prc.qryGetCorrelationInfo.check4 EQ 5>glyphicon glyphicon-ok</CFIF>"></span></span>
			</td>
			<td>
				<span class="input-group"><input type="text" name="blended_total" id="blended_total" value="#NumberFormat(prc.qryGetCorrelationInfo.blendedtotal,',')#" class="correlation-entry numeric text-right form-control" readonly><span class="input-group-addon"><span id="blended-total-check" class="<CFIF bUseBlendedMarker>glyphicon glyphicon-ok</CFIF>"></span></span>
			</td>

		</tr>
		<tr>
			<td colspan="7"></td>
		</tr>
		<tr>
			<td colspan="3" class="text-center">
				<span class="glyphicon glyphicon-asterisk"></span> <span class="formatted-data-label">Override Expires after the </span>
				<input type="text" name="peflovrthru" id="peflovrthru" value="#prc.qryGetCorrelationInfo.peflovrthru#" class="datefield-yearonly-correlation correlation-date-entry form-control">
				<span class="formatted-data-label"> roll</span>
			</td>
			<td colspan="4" class="text-center">
				<span class="formatted-data-label">Indicated Appraised Value</span>
				<input type="text" name="appraisedValueEdit" id="appraisedValueEdit" value="-2" readonly class="correlation-entry numeric text-right form-control">
			</td>
		</tr>
	</table>



	<div class="land-part-action-buttons">
		<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
		&nbsp;&nbsp;
		<a onclick="sendData();"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
	</div>

	<br clear="all"/>


</div>
</form>

</cfoutput>