<cfsetting showdebugoutput="No">
<cfparam name="fltTotalValue" default="0">
<cfparam name="fltOverallTotalValue" default="0">

<cfoutput>

<cfif NOT prc.qryCondoUnitTypesData.RecordCount>
	#addAsset('/includes/js/jquery.js,/includes/js/jquery.fancybox.js')#
	<script type="text/javascript">
		parent.$.fancybox.close();
	</script>
	<cfabort>
<cfelse>
	#addAsset('/includes/css/select2.css')#
</cfif>


<div id="popCondoUDF" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-land-popupUDF">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Complex Totals</h4>
			</div>
			<div class="modal-body" id="adjContainer">
				<div class="col-xs-6 databox">
					<div class="formatted-data-label">Name</div>
					<p>
					<div class="formatted-data-values">#prc.qryCondoMasterData.CondoName#</div>
					<p>
				</div>
				<div class="col-xs-6 databox">
					<div class="formatted-data-label">Parcel ID</div>
					<p>
					<div class="formatted-data-values">#prc.qryCondoMasterData.parcelDisplayID#</div>
					<p>
				</div>

				<div class="clearfix"><p></div>

				<div class="col-xs-12 databox">
					<table id="conodomaster-view-table" class="table" border="0">
					<tr>
						<td width="138"><div class="text-center formatted-data-label">Unit Type</div></td>
						<td width="138"><div class="text-center formatted-data-label">Number Of Units</div></td>
						<td width="100"><div class="text-center formatted-data-label">Total Value of Unit Types</div></td>
					</tr>
					<cfloop query="prc.qryCondoUnitTypesData">
						<cfset fltTotalValue = (numberUnits * unitBaseAmt)>
						<cfset fltOverallTotalValue = fltOverallTotalValue + fltTotalValue>
						<tr>
							<td><div class="text-center formatted-data-values">#unitType#</div></td>
							<td><div class="text-center formatted-data-values">#numberUnits#</div></td>
							<td><div class="text-right formatted-data-values">$ <span id="tavValue" class="header-cmid-values">#numberformat(fltTotalValue, ",")#</span></div></td>
						</tr>
					</cfloop>
					<tfoot class="condomasterFooter">
						<td><div class="text-center formatted-data-values">Totals</div></td>
						<td><div class="text-center formatted-data-values">#columnTotal("prc.qryCondoUnitTypesData.numberUnits")#</div></td>
						<td><div class="text-right formatted-data-values">$ <span id="tavValue" class="header-cmid-values">#numberformat(fltOverallTotalValue, ",")#</span></div></td>
					</tfoot>
					</table>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="modal-footer">
				<div id="modalPleaseWait" class="pull-left progress">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span>Please Wait</span></div>
				</div>
				<button type="button"  class="btn btn-default"  onclick="parent.$.fancybox.close();"><i class="glyphicon glyphicon-ok-sign"></i> Close</button>
			</div>
		</div>
	</div>
</div>

</cfoutput>
