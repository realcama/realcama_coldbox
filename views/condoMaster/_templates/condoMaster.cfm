<cfoutput>

	<CFIF bShowNoRecordsBlurb>
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				No Condo Master record found for the selected parcel
			</div>
		</div>

	<CFELSE>

		<CFIF IsDefined("URL.redirect") AND URL.redirect EQ "h">
			#application.content.modalStatic('redirectCondoMaster')#
			<script>
				$(function() {
					$("##myHeaderRedirect").modal('show');
				});
			</script>
		</CFIF>
		<!--- <script src="/includes/js/condoMaster.js?g=#now()#"></script> --->
		#addAsset('/includes/js/condoMaster.js,/includes/js/formatSelect.js')#
		<script>
			var condoID = #prc.qryCondoUnitTypesData.condoID#;
			var taxYear = #prc.stPersistentInfo.taxYear#;
			var parcelID = "#prc.stPersistentInfo.ParcelID#";
			var parcelType = "R";
		</script>


		<!----- Stored Proc to retrieve Neighborhoods
		<cfstoredproc procedure="p_getOptTableValues" datasource="#client.dsn#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="stateProv" null="No">
			<cfprocresult name="qryStatesAndProvs">
		</cfstoredproc>

		 ----->
		<script>
			<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.qryCondoMasterData#"
				TOPLEVELVARIABLE="masterData">
			<!--- 	/*
			<CFWDDX ACTION="CFML2JS"
				INPUT="#qryCondoSitusData#"
				TOPLEVELVARIABLE="situsData">

			<CFWDDX ACTION="CFML2JS"
				INPUT="#qryCondoLegalData#"
				TOPLEVELVARIABLE="legalData">
			*/ --->
			<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.qryCondoUnitTypesData#"
				TOPLEVELVARIABLE="unitTypesData">

			<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.qryCondoUnitTypicalCodesData#"
				TOPLEVELVARIABLE="unitTypicalCodesData">

		</script>


		<CFIF Controller.getSetting( "bShowPacketLinks")>
			<div class="alert alert-info">
				<A href="javascript:void(0);" onclick="WriteRaw(masterData);">Condo Master Packet</A>&nbsp;&nbsp;&nbsp;
				<!---
				<A href="javascript:void(0);" onclick="WriteRaw(situsData);">Situs Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(legalData);">Legal Packet</A>&nbsp;&nbsp;&nbsp;
				--->
				<A href="javascript:void(0);" onclick="WriteRaw(unitTypesData);">Unit Types Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(unitTypicalCodesData);">Unit Typical Codes Packet</A>&nbsp;&nbsp;&nbsp;
			</div>
		</CFIF>

		<form action="#event.buildLink('condoMaster/condoMasterSave')#" method="post" name="frmSave" id="frmSave">
			<input type="hidden" name="packet" id="packet" value="">
			<input type="hidden" name="packet2" id="packet2" value="">
			<input type="hidden" name="tab" id="tab" value="">
			<input type="hidden" name="ParcelID" id="ParcelID" value="#prc.stPersistentInfo.ParcelID#">
			<input type="hidden" name="taxYear" id="taxYear" value="#prc.stPersistentInfo.taxYear#">
		</form>



		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				<CFSET showTabOptions = "condomaster">
				#renderView('_templates/miniNavigationBar')#
				Condo Master
			</div>

			<div id="expander-holders" class="panel panel-default">
				#renderView('condoMaster/_templates/condoMasterData')#
				#renderView('condoMaster/_templates/condoMasterUnits')#

				<!---
				<cfinclude template="condoMasterSitus.inc">
				<cfinclude template="condoMasterLegal.inc">
				<cfinclude template="condoMasterUnits.inc">
				 --->


			</div>

		</div>
	</CFIF>
</cfoutput>