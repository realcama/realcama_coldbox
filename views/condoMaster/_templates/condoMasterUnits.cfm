<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading4"  aria-expanded="false" >
		<table class="land-line-summary ">
		<tr>
			<td width="95%" class="section-bar-label">Unit Types</td>
			<td width="5%" align="right"><!--- <a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="##collapse4" onclick="toggleChevron(this);makeTableViable()" data-parent="heading4" data-toggle="collapse" aria-controls="collapse4"></a> ---></td>
		</tr>
		</table>
	</div>

	<CFIF prc.qryCondoUnitTypesData.RecordCount EQ 0>
		<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
			<div id="view_mode_item_4" class="row fix-bs-row">
				<div class="col-xs-12 databox">
					<div class="formatted-data-label">No Data to display</div>
				</div>
			</div>
		</div>

	<CFELSE>
		<div id="collapse4" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading4">
			<div class="row fix-bs-row">

				<div class="col-xs-12">
					<br clear="all"/>
					<CFIF prc.qryCondoUnitTypesData.RecordCount GT 10>
						<div class="pull-left">
							<a href="javascript:moveLeft();" class="btn btn-primary showLeft"><span class="glyphicon glyphicon-arrow-left"></span></a>
						</div>
						<div class="pull-right">
							<a href="javascript:moveRight();" class="btn btn-primary showRight"><span class="glyphicon glyphicon-arrow-right"></span></a>
						</div>
					</cfif>
					<br clear="all"/>
				</div>

					<table class="table-bordered table-striped table-hover" id="condo-master-unit-types">
					<thead>
						<tr>
							<th class="formatted-data-label col_0" width="80">Unit Type</th>
							<th class="formatted-data-label col_1 text-center">Units</th>
							<th class="formatted-data-label col_2 text-center">Floors</th>
							<th class="formatted-data-label col_3 text-center">Beds</th>
							<th class="formatted-data-label col_4 text-center">Baths</th>
							<th class="formatted-data-label col_5 text-center">SqFt</th>
							<th class="formatted-data-label col_6 text-center"><a onclick="massBaseValueUpdater()" data-toggle="tooltip" data-placement="top" title="Open Base Value<br/>Mass Update Tool">Base Value</a></th>
							<th class="formatted-data-label col_7 text-center">Views</th>
							<th class="formatted-data-label col_8 text-center">Balcony</th>
							<th class="formatted-data-label col_9 text-center">Desirability</th>
							<th class="formatted-data-label col_10 text-center">Location</th>
							<th class="formatted-data-label col_11 text-center">Parking</th>
							<th class="formatted-data-label col_12 text-center">Recreation</th>

						</tr>
					</thead>
					<tbody>
						<CFSET lstCodes = "Views,Balcony,Desirability,Location,Parking,Recreation">
						<cfloop query="prc.qryCondoUnitTypesData">

							<tr onclick="editUnitTypes(#condoid#,#condotypicalid#,#prc.qryCondoUnitTypesData.CurrentRow#)" title="Click to edit this information">
								<td class="formatted-data-values align-vertical-top col_0 unittype_#prc.qryCondoUnitTypesData.CurrentRow#">#prc.qryCondoUnitTypesData.unittype#</td>
								<td class="formatted-data-values align-vertical-top text-center col_1 numberunits_#prc.qryCondoUnitTypesData.CurrentRow#">#prc.qryCondoUnitTypesData.numberunits#</td>
								<td class="formatted-data-values align-vertical-top text-center col_2 floorfrom_#prc.qryCondoUnitTypesData.CurrentRow#">#prc.qryCondoUnitTypesData.floorfrom# thru #prc.qryCondoUnitTypesData.floorto#</td>
								<td class="formatted-data-values align-vertical-top text-center col_3 numberbedrooms_#prc.qryCondoUnitTypesData.CurrentRow#">#prc.qryCondoUnitTypesData.numberbedrooms#</td>
								<td class="formatted-data-values align-vertical-top text-center col_4 numberbaths_#prc.qryCondoUnitTypesData.CurrentRow#">#prc.qryCondoUnitTypesData.numberbaths#</td>
								<td class="formatted-data-values align-vertical-top text-center col_5 sqft_#prc.qryCondoUnitTypesData.CurrentRow#">#NumberFormat(prc.qryCondoUnitTypesData.sqft,"00")#</td>
								<td class="formatted-data-values align-vertical-top col_6 unitbaseamt_#prc.qryCondoUnitTypesData.CurrentRow# text-right">$#NumberFormat(prc.qryCondoUnitTypesData.unitbaseamt,",")#</td>

								<CFSET colNumber = 6>
								<cfloop index="strCodeType" list="#lstCodes#">
									<CFSET colNumber = colNumber + 1>
									<cfquery name="qryThisCondosTypicalCodes" dbtype="query">
										SELECT *
										FROM prc.qryCondoUnitTypicalCodesData
										WHERE
											condoTypicalID = #prc.qryCondoUnitTypesData.condoTypicalID#
											AND codeType = '#strCodeType#'
									</cfquery>

									<td class="formatted-data-values align-vertical-top text-center col_#colNumber# #strCodeType#_#prc.qryCondoUnitTypesData.CurrentRow#">
										<CFIF qryThisCondosTypicalCodes.RecordCount LT 3>
											<cfloop query="qryThisCondosTypicalCodes">
												#TRIM(qryThisCondosTypicalCodes.code)#: #TRIM(qryThisCondosTypicalCodes.codedescription)#<br>
											</cfloop>
										<CFELSE>
											<cfsavecontent variable="strTooltipCode"><div class='text-left'><cfloop query="qryThisCondosTypicalCodes">#TRIM(qryThisCondosTypicalCodes.code)#: #TRIM(qryThisCondosTypicalCodes.codedescription)#<br/></cfloop></div></CFSAVECONTENT>
											<span data-toggle="tooltip" data-placement="auto" title="#strTooltipCode#">Multiple</span>
										</CFIF>
									</td>
								</cfloop>
							</tr>
						</cfloop>
					</tbody>
					</table>
				</div>
				<div class="col-xs-12">
					<div class="pull-left">
						<a href="javascript:moveLeft();" class="btn btn-primary showLeft"><span class="glyphicon glyphicon-arrow-left"></span></a>
					</div>
					<div class="pull-right">
						<a href="javascript:moveRight();" class="btn btn-primary showRight"><span class="glyphicon glyphicon-arrow-right"></span></a>
					</div>
					<br clear="all"/>
				</div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
						&nbsp;&nbsp;
						<a href="javascript:saveWDDX('unitTypes')"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
					</div>
				</div>

				<br clear="all"/>


			</div>



			<!---

	<script>


	function doCondoMasterAdjPopup(row,condoTypicalID) {

	    $.ajax({
			type: "get",
			cache: false,
			url: "showCondoMasterUnitPopup.cfm", // preview.php
			data: {
				taxYear: taxYear,
				condoID: condoID,
				condoTypicalID: condoTypicalID,
				row: row
			}, // all form fields
			success: function (data) {
				// on success, post (preview) returned data in fancybox
				$.fancybox(data, {}); // fancybox

			} // success
	    }); // ajax

	}
	</script>



			<cfloop query="prc.qryCondoUnitTypesData">

				<cfquery name="qryThisCondosTypicalCodes" dbtype="query">
					SELECT *
					FROM qryCondoUnitTypicalCodesData
					WHERE
						condoTypicalID = #prc.qryCondoUnitTypesData.condoTypicalID#
				</cfquery>

				<CFSET PercentageAddOns = 0>
				<CFSET RateAddOns = 0>
				<cfsavecontent variable="strOptions">
					<cfloop query="qryThisCondosTypicalCodes">
						<tr>
							<td class="formatted-data-values">#qryThisCondosTypicalCodes.codeType#</td>
							<td class="formatted-data-values" width="25">#qryThisCondosTypicalCodes.code#</td>
							<td class="formatted-data-values">#qryThisCondosTypicalCodes.codedescription#</td>
							<td class="formatted-data-values">&nbsp;</td>
							<td class="formatted-data-values">&nbsp;</td>
							<td class="rightAlign formatted-data-values"><CFIF qryThisCondosTypicalCodes.adjustmentfactor NEQ 0>#NumberFormat(qryThisCondosTypicalCodes.adjustmentfactor,".000")#</CFIF></td>
							<td class="rightAlign formatted-data-values"><CFIF qryThisCondosTypicalCodes.adjustmentamount NEQ 0>#NumberFormat(qryThisCondosTypicalCodes.adjustmentamount,".000")#</CFIF></td>
						</tr>
						<CFIF PercentageAddOns EQ 0>
							<CFSET PercentageAddOns = qryThisCondosTypicalCodes.adjustmentfactor>
						<CFELSE>
							<CFSET PercentageAddOns = PercentageAddOns * qryThisCondosTypicalCodes.adjustmentfactor>
						</CFIF>
						<CFSET RateAddOns = RateAddOns + qryThisCondosTypicalCodes.adjustmentamount>
					</cfloop>
				</cfsavecontent>

				<div id="view_mode_item_unit_#prc.qryCondoUnitTypesData.CurrentRow#">
					<div class="row fix-bs-row">
						<div class="parcel-table-header section-bar-label col-xs-12">
							Line ###prc.qryCondoUnitTypesData.currentRow#
						</div>
						<div class="clearfix"></div>


						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Unit Type</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.unittype#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Units</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.numberunits#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Bedrooms</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.numberbedrooms#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Bathrooms</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.numberbaths#</span>
						</div>
						<div class="clearfix"></div>

						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Rooms</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.numberrooms#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Square Feet</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.sqft#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Floors</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.floorfrom# thru #prc.qryCondoUnitTypesData.floorto#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Base Value</span><br />
							<span class="formatted-data-values">$#NumberFormat(prc.qryCondoUnitTypesData.unitbaseamt,",")#</span>
						</div>

						<div class="clearfix"></div>

						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Floor Adjustment</span><br />
							<span class="formatted-data-values">$#NumberFormat(prc.qryCondoUnitTypesData.FloorRateAdjTotal)#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Floor Adj Type</span><br />
							<span class="formatted-data-values">#prc.qryCondoUnitTypesData.FloorRateAdj#</span>
						</div>
						<div class="col-xs-6 databox">
							<span class="formatted-data-label"></span><br />
							<span class="formatted-data-values"></span>
						</div>
					</div>

					<div class="clearfix">&nbsp;</div>

					<div class="row fix-bs-row add-in-box">
						<div class="col-xs-12">
							<table class="building-udf-table">
							<thead>
								<tr>
									<td width="15%"><span class="formatted-data-label">Add-Ins</span></td>
									<td width="25"></td>
									<td width="20%"></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Points</span></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Lump Sum</span></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Percentage</span></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Rate Add-On</span></td>
								</tr>
							</thead>
							<tbody>
								<CFIF qryThisCondosTypicalCodes.RecordCount EQ 0><tr><td colspan="7">None</td></tr><CFELSE>#strOptions#</CFIF>
							</tbody>
							<tfoot class="adjustmentFooter">
								<tr>
									<td width="15%"></td>
									<td colspan="2" class="rightAlign formatted-data-label">Net Adjustments</td>
									<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(0,"0.000")#</td>
									<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(0,"0.000")#</td>
									<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(PercentageAddOns,"0.000")#</td>
									<td width="15%" class="rightAlign formatted-data-label">#NumberFormat(RateAddOns,"0.000")#</td>
								</tr>
							</tfoot>
							</table>
						</div>

						<div class="col-xs-12 building-button-row text-right">
							&nbsp;
						</div>
					</div>
					<div class="clearfix">&nbsp;</div>


					<div class="building-part-action-buttons">
						<a href="javascript:editThisCondoMasterUnitsRow(#prc.qryCondoUnitTypesData.currentrow#);"><img src="/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>

					<br clear="all"/>
				</div>
				<div id="edit_mode_item_unit_#prc.qryCondoUnitTypesData.CurrentRow#"  style="display:none;">
					<div class="row fix-bs-row">
						<div class="parcel-table-header section-bar-label col-xs-12">
							Line ###prc.qryCondoUnitTypesData.currentRow#
						</div>
						<div class="clearfix"></div>


						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Unit Type</span><br />
							<span class="formatted-data-values">
								<input type="text" name="unittype" value="#prc.qryCondoUnitTypesData.unittype#" class="form-control" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Units</span><br />
							<span class="formatted-data-values">
								<input type="text" name="numberunits" value="#prc.qryCondoUnitTypesData.numberunits#" class="form-control numeric" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Bedrooms</span><br />
							<span class="formatted-data-values">
								<input type="text" name="numberbedrooms" value="#prc.qryCondoUnitTypesData.numberbedrooms#" class="form-control numeric" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Bathrooms</span><br />
							<span class="formatted-data-values">
								<input type="text" name="numberbaths" value="#prc.qryCondoUnitTypesData.numberbaths#" class="form-control numeric" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="clearfix"></div>

						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Rooms</span><br />
							<span class="formatted-data-values">
								<input type="text" name="numberrooms" value="#prc.qryCondoUnitTypesData.numberrooms#" class="form-control numeric" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Square Feet</span><br />
							<span class="formatted-data-values">
								<input type="text" name="sqft" value="#prc.qryCondoUnitTypesData.sqft#" class="form-control numeric" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Floors</span><br />
							<span class="formatted-data-values">
								<input type="text" name="floorfrom" value="#prc.qryCondoUnitTypesData.floorfrom#" class="form-control condo-master-floor numeric" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
								<span class="formatted-data-label"> - </span>
								<input type="text" name="floorto" value="#prc.qryCondoUnitTypesData.floorto#" class="form-control condo-master-floor numeric" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Base Value</span><br />
							<span class="formatted-data-values">
								$<input type="text" name="unitbaseamt" value="#prc.qryCondoUnitTypesData.unitbaseamt#" class="form-control numeric width-90percent" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="clearfix"></div>

						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Floor Adjustment</span><br />
							<span class="formatted-data-values">
								$<input type="text" name="FloorRateAdjTotal" value="#prc.qryCondoUnitTypesData.FloorRateAdjTotal#" class="form-control numeric width-90percent" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Floor Adj Type</span><br />
							<span class="formatted-data-values">
								<input type="text" name="FloorRateAdj" value="#prc.qryCondoUnitTypesData.FloorRateAdj#" class="form-control" onchange="changeVal('unitTypesData',#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</span>
						</div>
						<div class="col-xs-6 databox">
							<span class="formatted-data-label"></span><br />
							<span class="formatted-data-values"></span>
						</div>
					</div>

					<div class="clearfix">&nbsp;</div>

					<div class="row fix-bs-row add-in-box">
						<div class="col-xs-12">
							<table class="building-udf-table">
							<thead>
								<tr>
									<td width="15%"><span class="formatted-data-label">Add-Ins</span></td>
									<td width="25"></td>
									<td width="20%"></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Points</span></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Lump Sum</span></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Percentage</span></td>
									<td width="15%" class="rightAlign"><span class="formatted-data-label">Rate Add-On</span></td>
								</tr>
							</thead>
							<tbody id="udf_#prc.qryCondoUnitTypesData.CurrentRow#">
								<CFIF qryThisCondosTypicalCodes.RecordCount EQ 0><tr><td colspan="7">None</td></tr><CFELSE>#strOptions#</CFIF>
							</tbody>
							<tfoot class="adjustmentFooter">
								<tr>
									<td width="15%"></td>
									<td colspan="2" class="rightAlign formatted-data-label">Net Adjustments</td>
									<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_points_#prc.qryCondoUnitTypesData.CurrentRow#">#NumberFormat(0,"0.000")#</td>
									<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_lumpsum_#prc.qryCondoUnitTypesData.CurrentRow#">#NumberFormat(0,"0.000")#</td>
									<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_percentage_#prc.qryCondoUnitTypesData.CurrentRow#">#NumberFormat(PercentageAddOns,"0.000")#</td>
									<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_rateaddon_#prc.qryCondoUnitTypesData.CurrentRow#">#NumberFormat(RateAddOns,"0.000")#</td>
								</tr>
							</tfoot>
							</table>
						</div>

						<div class="col-xs-12 building-button-row">
							<span class="land-line-adj-button" onclick="doCondoMasterAdjPopup(#prc.qryCondoUnitTypesData.CurrentRow#,#prc.qryCondoUnitTypesData.condoTypicalID#)" rel="tooltip" title="Click to alter Add-Ins"><img src="/images/v2/addedit_ADDins_blue.gif" alt="" width="134" height="26" border="0"></span>
						</div>
					</div>
					<div class="clearfix">&nbsp;</div>




					<div class="col-xs-12">
						<div class="land-part-action-buttons">
							<a href="javascript:cancelEverything()"><img src="/images/v2/cancel.png" alt="Cancel" border="0"></a>
							&nbsp;&nbsp;
							<a href="javascript:saveWDDX('unitTypes',#prc.qryCondoUnitTypesData.condotypicalid#)"><img src="/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
						</div>
					</div>

					<br clear="all"/>
				</div>


			</cfloop>

			--->
		</div>
	</CFIF>
</cfoutput>




