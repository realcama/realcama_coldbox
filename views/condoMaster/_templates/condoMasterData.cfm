<cfoutput>
	<CFSET complexID = formatComplexID(prc.qryCondoMasterData.condoID)>

	<script>
		masterData.setField(0,"condoid", "#complexID#");
	</script>

	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading1"  aria-expanded="false" >
		<table class="land-line-summary ">
		<tr>
			<td width="95%" class="section-bar-label">Condo Master Data</td>
			<td width="5%" align="right"><!--- <a class="block-expander glyphicon glyphicon-chevron-down collapsed" href="#collapse1" onclick="toggleChevron(this)" data-parent="heading1" data-toggle="collapse" aria-controls="collapse1"></a> ---></td>
		</tr>
		</table>
	</div>
	<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
		<div id="view_mode_item_1" class="row fix-bs-row">

			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Year</div>
				<div class="formatted-data-values">#prc.qryCondoMasterData.condoYear#</div>
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Name</div>
				<div class="formatted-data-values">#prc.qryCondoMasterData.CondoName#</div>
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Parcel ID</div>
				<div class="formatted-data-values">#prc.qryCondoMasterData.ParcelDisplayID#</div>
			</div>
			<div class="clearfix"></div>


			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Complex Number</div>
				<div class="formatted-data-values">#complexID#</div>
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Actual Year Built (AYB)</div>
				<div class="formatted-data-values">#prc.qryCondoMasterData.ActualYearBuilt#</div>
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Effective Year Built (EYB)</div>
				<div class="formatted-data-values">#prc.qryCondoMasterData.EffectiveYearBuilt#</div>
			</div>
			<div class="clearfix"></div>

			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Type</div>
				<div class="formatted-data-values">#prc.qryCondoMasterData.ImprovementCode#: #prc.qryCondoMasterData.ImprovementCodeDescription#</div>
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Quality</div>
				<div class="formatted-data-values">#prc.qryCondoMasterData.QualityCode#: #prc.qryCondoMasterData.QualityCodeDescription#</div>
			</div>
			<div class="col-xs-4 databox">

			</div>
			<div class="clearfix"></div>

			<div class="col-xs-12">
				<div class="land-part-action-buttons">
					<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
				</div>
			</div>
		</div>
		<div id="edit_mode_item_1" class="row fix-bs-row" style="display:none;">
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Year</div>
				<input type="text" name="condoyear" value="#prc.qryCondoMasterData.CondoYear#" class="form-control datefield-condo-master numeric" onchange="changeVal('masterData',0,this)" />
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Name</div>
				<input type="text" name="condoname" value="#prc.qryCondoMasterData.CondoName#" class="form-control" onchange="changeVal('masterData',0,this)" />
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Parcel ID</div>
				<input type="text" name="prdisp" value="#prc.qryCondoMasterData.ParcelID#" class="form-control" readonly onchange="changeVal('masterData',0,this)" />
			</div>
			<div class="clearfix"></div>

			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Complex Number</div>
				<input type="text" name="condoID" value="#complexID#" class="form-control numeric" onchange="changeVal('masterData',0,this)" />
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Actual Year Built (AYB)</div>
				<input type="text" name="actualyearbuilt" value="#prc.qryCondoMasterData.ActualYearBuilt#" class="form-control datefield-condo-master numeric" onchange="changeVal('masterData',0,this)" />
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Effective Year Built (EYB)</div>
				<input type="text" name="effectiveyearbuilt" value="#prc.qryCondoMasterData.EffectiveYearBuilt#" class="form-control datefield-condo-master numeric" onchange="changeVal('masterData',0,this)" />
			</div>
			<div class="clearfix"></div>


			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Type</div>
				<select data-tags="true" name="improvementtype" id="improvementtype" class="form-control select2-2piece-new" onchange="changeVal('masterData',0,this)">
					<option value="" data-val="&nbsp;" data-desc="Select...">Select...</option>
					<cfloop query="prc.qryCondoMasterTypes">
						<option value="#prc.qryCondoMasterTypes.code#" data-val="#prc.qryCondoMasterTypes.code#"  data-desc="#prc.qryCondoMasterTypes.Description#" <cfif prc.qryCondoMasterData.ImprovementCode EQ prc.qryCondoMasterTypes.code> selected</cfif>>#prc.qryCondoMasterTypes.code#: #prc.qryCondoMasterTypes.Description#</option>
					</cfloop>
				</select>
			</div>
			<div class="col-xs-4 databox">
				<div class="formatted-data-label">Quality</div>
				<select data-tags="true" name="quality" id="quality" class="form-control select2-2piece-new" onchange="changeVal('masterData',0,this)">
					<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
					<cfloop query="prc.qryCondoQualityCodes">
						<option value="#prc.qryCondoQualityCodes.code#" data-val="#prc.qryCondoQualityCodes.code#"  data-desc="#prc.qryCondoQualityCodes.Description#" <cfif prc.qryCondoMasterData.QualityCode EQ prc.qryCondoQualityCodes.code> selected</cfif>>#prc.qryCondoQualityCodes.code#: #prc.qryCondoQualityCodes.Description#</option>
					</cfloop>
				</select>

			</div>
			<div class="col-xs-4 databox"></div>
			<div class="clearfix"></div>

			<div class="col-xs-12">
				<div class="land-part-action-buttons">
					<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
					&nbsp;&nbsp;
					<a href="javascript:saveWDDX('info')"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
				</div>
			</div>
		</div>

	</div>
</cfoutput>