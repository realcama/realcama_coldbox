
<link href="/includes/css/select2.css" rel="stylesheet" />
<script src="/includes/js/select2.js"></script>

<script>
	var iOptionsInThisModal = 0;
	<cfoutput>
	var condoID = '#condoID#';
	var condoTypicalID = '#condoTypicalID#';

	<cfloop index="strCode" list="#lstCodes#" delimiters=",">
		var #strCode#_data = "";
	</cfloop>

	</cfoutput>

	function doIt() {
		resetLabels(); <!--- Sets all of the data variables to blank values --->
		resetModal(); <!--- Sets the iOptionsInThisModal to 0 and nukes the display of ANY options from a previous load --->
		hideParent(); <!--- Hides the parent modal --->
		buildModalCode(); <!--- Build the modal --->
		changeSelectsToSelect2s();
		$(".numeric").numeric();
	}

	function resetModal() {
		iOptionsInThisModal = 0;
		$("#adjContainerModal").html("");
	}
	function resetLabels() {
		<cfoutput>
		<cfloop index="strCode" list="#lstCodes#" delimiters=",">
			#strCode#_data = "";
			count_#strCode# = 0;
			#strCode#_tooltip = "";
		</cfloop>
		</cfoutput>
	}


	function hideParent() {
		$("#popCondoMasterUnitType").addClass("blur");
	}
	function showParent() {
		$("#popCondoMasterUnitType").removeClass("blur");
	}

	function format1Piece(state) {
		var originalOption = state.element;
		return "<span class='select2-1part-left'>" + $(originalOption).val() + "</span>";
	}

	function format2Piece(state) {
		var originalOption = state.element;
		return "<span class='select2-2part-left'>" + $(originalOption).data('code') + "</span><span class='select2-2part-right'>" + $(originalOption).data('desc')+ "</span>";
	}
	function format3Piece(state) {
		var originalOption = state.element;
		if($(originalOption).data('amount') > 0 && $(originalOption).data('factor') == 0) {
			return "<span class='select2-3part-left'>" + $(originalOption).data('code') + "</span><span class='select2-3part-middle'>" + $(originalOption).data('desc') + "</span><span class='select2-3part-right'>[Amount: " + $(originalOption).data('amount')+ "]</span>";
		} else {
			return "<span class='select2-3part-left'>" + $(originalOption).data('code') + "</span><span class='select2-3part-middle'>" + $(originalOption).data('desc') + "</span><span class='select2-3part-right'>[Factor: " + $(originalOption).data('factor')+ "]</span>";
		}
	}


	function changeSelectsToSelect2s() {

		<!--- turn all of the select2 boxes into formatted select boxes --->
		$('.select2-1piece-dropdown-new').select2({
			width: "180px",
			minimumResultsForSearch: -1,
			formatResult: format1Piece,
			formatSelection: format1Piece,
			escapeMarkup: function(m) { return m; }
		});
		$('.select2-2piece-dropdown-new').select2({
			width: "350px",
			minimumResultsForSearch: -1,
			formatResult: format2Piece,
			formatSelection: format2Piece,
			escapeMarkup: function(m) { return m; }
		});
		$('.select2-3piece-dropdown-new').select2({
			width: "350px",
			minimumResultsForSearch: -1,
			formatResult: format3Piece,
			formatSelection: format3Piece,
			escapeMarkup: function(m) { return m; }
		});

		<!--- Go through all of the DOM elements and change "select2-dropdown-new" to "select2-dropdown-new" --->
		$( ".select2-1piece-dropdown-new" ).each(function( ) {
			$(this).removeClass("select2-1piece-dropdown-new").addClass("select2-1piece-dropdown");
		});
		$( ".select2-2piece-dropdown-new" ).each(function( ) {
			$(this).removeClass("select2-2piece-dropdown-new").addClass("select2-2piece-dropdown");
		});
		$( ".select2-3piece-dropdown-new" ).each(function( ) {
			$(this).removeClass("select2-3piece-dropdown-new").addClass("select2-3piece-dropdown");
		});
	}



	function buildModalCode() {

		for ( var iCurrentRow = 0; iCurrentRow < parent.unitTypicalCodesData.getRowCount(); iCurrentRow++ ) {

			condotypicalid = parent.unitTypicalCodesData.getField(iCurrentRow,"condotypicalid");
			mode = parent.unitTypicalCodesData.getField(iCurrentRow,"mode");

			if(condotypicalid == <cfoutput>#condoTypicalID#</cfoutput>) {
				if(mode != "Delete") {

					codeID = parent.unitTypicalCodesData.getField(iCurrentRow,"condocodeid");
					codetype = parent.unitTypicalCodesData.getField(iCurrentRow,"codetype");

					var strHTML = "";
					strHTML = createAdjEntryRow(iCurrentRow, codeID, codetype, 'Edit');

					if(iOptionsInThisModal === 0) {
						$("#adjContainerModal").html(strHTML);
					} else {
						$("#adjContainerModal").append(strHTML);
					}

					iOptionsInThisModal++;
				} /* END mode != "Delete"*/
			} /* END condotypicalid == [] */
		} /* END for*/

		changeSelectsToSelect2s();
		$('[data-toggle="tooltip"]').tooltip();

		if(iOptionsInThisModal == 0) {
			$("#adjContainerModal").html("No condo unit codes defined");
		}

	}

	function createAdjEntryRow(iRow, inCodeID, inCodeType, mode) {
		var strHTML = "";

		strHTML = strHTML + "<div id='condounitcoderow_" + iRow + "' class='udfrow'>";

		if(mode == "Insert") {
			strHTML = strHTML + "	<span class='icon-nothing'></span>";
		} else {
			strHTML = strHTML + "	<span class='glyphicon glyphicon-remove-sign' onclick='removeAdjRow(" + iRow + ")'></span>";
		}

		<!--- Create the list of the options for this Adjustment --->
		strHTML = strHTML + "	<select name='codetype' id='codetype_" + iRow + "' data-rownumber=" + iRow + " class='select2-1piece-dropdown-new' onchange='changeTypeVal(" + iRow +",this);changeUDFMethod(this)'>";
		<cfoutput>
			strHTML = strHTML + "		<option value='Select' data-code='&nbsp;' data-desc='Select'>Select</option>";
			<cfloop index="strCode" list="#lstCodes#" delimiters=",">
				strChecked = (inCodeType == '#strCode#') ? " selected" : "";
				strHTML = strHTML + "		<option value='#strCode#'" + strChecked + ">#strCode#</option>";
			</cfloop>
		</cfoutput>
		strHTML = strHTML + "	</select>";

		strSelectedType = "";
		strSelectedAmount = "0.00";
		<cfloop index="strCode" list="#lstCodes#" delimiters=",">
			<!--- Individual code drop downs --->
			<CFSET qryCurrentLoop = "qryGet#strCode#Codes">

			<cfoutput>
				strDisplay = (inCodeType == '#strCode#') ? "" : " style='display:none'";
				strHTML = strHTML + "	<select name='#strCode#' id='#strCode#_" + iRow + "' data-tags='true' class='two-part-large select2-2piece-dropdown-new'" + strDisplay +" onchange='changeCodeVal(" + iRow +",this)'>";
			</cfoutput>
			strHTML = strHTML + "		<option value='-1' data-code='&nbsp;' data-desc='Select'>Select</option>";

			<cfoutput query="#Evaluate(DE(qryCurrentLoop))#">
				strSelected = (inCodeID == '#condocodeid#') ? " selected" : "";
				<CFIF adjustmentamount GT 0>
					if(inCodeID == '#condocodeid#') {
						strSelectedType = "Amount";
						strSelectedAmount = "#adjustmentamount#";
					}
				<CFELSE>
					if(inCodeID == '#condocodeid#') {
						strSelectedType = "Factor";
						strSelectedAmount = "#NumberFormat(adjustmentfactor,".000")#";
					}
				</CFIF>
				strHTML = strHTML + "		<option value='#condocodeid#' data-code='#code# ' data-desc='#codedescription#' " + strSelected + " data-amount='#NumberFormat(adjustmentamount,".000")#' data-factor='#NumberFormat(adjustmentfactor,".000")#'><CFIF code NEQ "">#code#: </cfif>#codedescription#</option>";
			</cfoutput>
			strHTML = strHTML + "	</select>";
		</cfloop>


		<!--- Adjustment Type  --->
		strDisplay = (inCodeType != "") ? "" : " style='display:none'";
		strHTML = strHTML + "	<select name='adjustmenttype' id='adjustmenttype_" + iRow + "' disabled data-tags='true' class='select2-1piece-dropdown-new' " + strDisplay + ">";
		strHTML = strHTML + "		<option value='N/A'>N/A</option>";

		if(strSelectedType == "Factor") {
			strHTML = strHTML + "		<option value='Factor' selected>Factor</option>";
			strHTML = strHTML + "		<option value='Amount'>Amount</option>";
		} else {
			if(strSelectedType == "Amount") {
				strHTML = strHTML + "		<option value='Factor'>Factor</option>";
				strHTML = strHTML + "		<option value='Amount' selected>Amount</option>";
			} else {
				strHTML = strHTML + "		<option value='Factor'>Factor</option>";
				strHTML = strHTML + "		<option value='Amount'>Amount</option>";
			}
		}

		strHTML = strHTML + "		<option value='Lump Sum Add-On'>Lump Sum Add-On</option>";
		strHTML = strHTML + "		<option value='Percentage'>Percentage</option>";
		strHTML = strHTML + "		<option value='Rate Add-On'>Rate Add-On</option>";
		strHTML = strHTML + "		<option value='Points'>Points</option>";
		strHTML = strHTML + "	</select>";


		<!--- Adjustment value --->
		<cfoutput>
		strDisplay = (inCodeType != "") ? "" : " style='display:none'";
		strHTML = strHTML + "	<input name='adjustmentvalue' id='adjustmentvalue_" + iRow + "' readonly class='form-control two-part-small numeric' type='text' value='" + strSelectedAmount + "' " + strDisplay + ">";
		</cfoutput>

		strHTML = strHTML + "</div>";

		return strHTML;
	}
	function changeUDFMethod(obj) {
		iRowToSetSelects = $(obj).data("rownumber");

		turnOffSelects(iRowToSetSelects);
		showOption(obj.value,iRowToSetSelects);

	}
	function turnOffSelects(iRowToSetSelects){
		<cfoutput>
			<cfloop index="strCode" list="#lstCodes#" delimiters=",">
				$("###strCode#_" + iRowToSetSelects).hide();
				$("##s2id_#strCode#_" + iRowToSetSelects).hide();
				$("##s2id_#strCode#_" + iRowToSetSelects).select2("val",-1);
				$("###strCode#_" + iRowToSetSelects).val(-1);
			</cfloop>
		</cfoutput>
	}
	function showOption(method,iRowToSetSelects) {
		switch(method) {
			<cfoutput>
				<cfloop index="strCode" list="#lstCodes#" delimiters=",">
					case "#strCode#":
						$("##s2id_#strCode#_" + iRowToSetSelects).show();
						$("##s2id_#strCode#_" + iRowToSetSelects).css("display","inline-block");

						$("##s2id_adjustmenttype_" + iRowToSetSelects).show();
						$("##s2id_adjustmenttype_" + iRowToSetSelects).css("display","inline-block");
						$("##adjustmentvalue_" + iRowToSetSelects).show();

						break;
				</cfloop>
			</cfoutput>
		}
	}


	function changeTypeVal(iRow,obj) {
		switch(obj.name) {
			<cfoutput>
				<cfloop index="strCode" list="#lstCodes#" delimiters=",">
					case "#strCode#":
						var selectedOption = $(obj).find('option:selected');
						var codetype = selectedOption.val();
						parent.unitTypicalCodesData.setField( iRow, "codetype", codetype );
						parent.unitTypicalCodesData.setField( iRow, "condocodeid", -1 );		// Force the user to select a new value
						break;
				</cfloop>
			</cfoutput>
			default:
				parent.unitTypicalCodesData.setField( iRow, obj.name, $(obj).val() );
				break;
		}
		createModalLabel();
		$('[data-toggle="tooltip"]').tooltip();
	}
	function changeCodeVal(iRow,obj) {
		switch(obj.name) {
			<cfoutput>
				<cfloop index="strCode" list="#lstCodes#" delimiters=",">
					case "#strCode#":
						var selectedOption = $(obj).find('option:selected');
						var codeID = selectedOption.val();
						var code = selectedOption.data('code');
						var description = selectedOption.data('desc');

						var amount = selectedOption.data('amount');
						var factor = selectedOption.data('factor');

						$("##s2id_adjustmenttype_" + iRow).select2("val","N/A");
						$("##adjustmentvalue_" + iRow).val("0.00");

						// Default the adjustment values so that they get the correct value based on the selected otpion
						parent.unitTypicalCodesData.setField( iRow, "adjustmentamount", "0.00" );
						parent.unitTypicalCodesData.setField( iRow, "adjustmentfactor", "0.00" );

						if(amount > 0 && factor == 0.000) {
							$("##s2id_adjustmenttype_" + iRow).select2("val","Amount");
							$("##adjustmentvalue_" + iRow).val(amount);
							parent.unitTypicalCodesData.setField( iRow, "adjustmentamount", amount );

						}
						if(amount == 0.000 && factor > 0) {
							var iDispFactor = factor * 1;
							$("##s2id_adjustmenttype_" + iRow).select2("val","Factor");
							$("##adjustmentvalue_" + iRow).val(iDispFactor.toFixed(3));
							parent.unitTypicalCodesData.setField( iRow, "adjustmentfactor", factor );
						}

						parent.unitTypicalCodesData.setField( iRow, "condocodeid", codeID );
						parent.unitTypicalCodesData.setField( iRow, "code", code );
						parent.unitTypicalCodesData.setField( iRow, "codedescription", description );

						break;
				</cfloop>
			</cfoutput>
			default:
				parent.unitTypicalCodesData.setField( iRow, obj.name, $(obj).val() );
				break;
		}
		createModalLabel();
		$('[data-toggle="tooltip"]').tooltip();
	}


	function removeAdjRow(iRow) {
		var choice = confirm("Are you sure you want to delete this row?");
		if(choice == true) {
			parent.unitTypicalCodesData.setField( iRow, "mode", "Delete" );
			$("#condounitcoderow_" + iRow).remove();
			createModalLabel();
			$('[data-toggle="tooltip"]').tooltip();
		}
	}

	function createModalLabel(){
		resetLabels();

		var strLabel = "";

		var iLumpSum = 0;
		var iPercentage = 0;
		var iRateAddOn = 0;
		var iPointsSum = 0;

		/* find out if we have to use depth or not in the calculation of the net adjustment row */
		for ( var iCurrentRow = 0; iCurrentRow < parent.unitTypicalCodesData.getRowCount(); iCurrentRow++ ) {
			mode = parent.unitTypicalCodesData.getField(iCurrentRow,"mode");

			codetype = parent.unitTypicalCodesData.getField( iCurrentRow, "codetype");
			code = parent.unitTypicalCodesData.getField( iCurrentRow, "code");
			codedescription = parent.unitTypicalCodesData.getField( iCurrentRow, "codedescription");

			amount = parseFloat(parent.unitTypicalCodesData.getField( iCurrentRow, "adjustmentamount"));
			factor = parseFloat(parent.unitTypicalCodesData.getField( iCurrentRow, "adjustmentfactor"));


			condotypicalid = parent.unitTypicalCodesData.getField( iCurrentRow, "condotypicalid");

			if(condotypicalid == <cfoutput>#condoTypicalID#</cfoutput>) {
				if(mode != "Delete") {

					if(
						(codetype == null || codetype == "Select" || codetype === "") ||
						(codedescription == null || codedescription == "Select") <!--- ||  codedescription == "N/A" --->
						){
						<!--- this is a junk entry, not all required pieces are selected --->

					} else {

						if(amount > 0 && factor == 0.00) {

							strLabel = strLabel + "<tr>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + codetype + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + code + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values' width='80'>" + codedescription + "</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>" + amount.toFixed(3) + "</td>";
							strLabel = strLabel + "</tr>";
							iLumpSum = iLumpSum + amount;
						}
						if(amount == 0.00 && factor > 0) {
							var iDispFactor = factor * 1;
							strLabel = strLabel + "<tr>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + codetype + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + code + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values' width='80'>" + codedescription + "</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>" + iDispFactor.toFixed(3) +"</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "</tr>";
							if(iPercentage == 0) {
								iPercentage = factor;
							} else {
								iPercentage = iPercentage * factor;
							}
						}

						switch(codetype) {
							<cfoutput>
								<cfloop index="strCode" list="#lstCodes#" delimiters=",">
									case "#strCode#":
										count_#strCode#++;

										if(count_#strCode# < 3){
											#strCode#_data += (code + ": " + codedescription + "<br/>");
										} else {
											if(count_#strCode# == 3){
												// Push the original label into the tooltip holder
												#strCode#_tooltip = #strCode#_data;
												// Clear the original cell contents since we are going to use a custom label
												#strCode#_data = "";
											}
											#strCode#_tooltip +=(code + ": " + codedescription + "<br/>");
										}
										break;
								</cfloop>
							</cfoutput>
						}
					} /* END else*/
				} /* END mode != "Delete"*/
			}  /* END condotypicalid == [] */
		} /* END for*/

		if(strLabel === "") {
			strLabel = "<tr><td colpan='7'>None</td></tr>";
		}


		<cfoutput>
			$("##udf_#row#").html(strLabel);
			$("##condomaster_netadj_points_#row#").html(iPointsSum.toFixed(3));
			$("##condomaster_netadj_lumpsum_#row#").html(iLumpSum.toFixed(3));
			$("##condomaster_netadj_percentage_#row#").html(iPercentage.toFixed(3));
			$("##condomaster_netadj_rateaddon_#row#").html(iRateAddOn.toFixed(3));

			<cfloop index="strCode" list="#lstCodes#" delimiters=",">
				if(count_#strCode# < 3) {
					$(".#strCode#_#row#").html(#strCode#_data);
				} else {
					thisTooltip = "<div class='text-left'>" + #strCode#_tooltip + "</div>";
					thisString = "<span data-toggle=\"tooltip\" data-placement=\"auto\" title=\"" + thisTooltip + "\">Multiple</span>";
					$(".#strCode#_#row#").html(thisString);
				}
			</cfloop>
		</cfoutput>

		$('[data-toggle="tooltip"]').tooltip({html: true});
		fixUnitTypeCellSizes();
	}




	function returnFriendlyType(type) {
		var strValue = "";
		switch(type) {
			case "R":
				strValue = "Rate Add-On";
				break;
			case "L":
				strValue = "Lump Sum Add-On";
				break;
			case "F":
				strValue = "Percentage";
				break;
			case "P":
				strValue = "Points";
				break;
		}
		return strValue;
	}

	function addAdjRow() {

		parent.unitTypicalCodesData.addRows(1);

		iNewRow = parent.unitTypicalCodesData.getRowCount() -1;
		parent.unitTypicalCodesData.setField( iNewRow, "condotypicalid", condoTypicalID );
		parent.unitTypicalCodesData.setField( iNewRow, "mode", "Insert" );
		parent.unitTypicalCodesData.setField( iNewRow, "adjustmentfactor", 0 );
		parent.unitTypicalCodesData.setField( iNewRow, "adjustmentamount", 0 );

		var strHTML = "";
		strHTML = createAdjEntryRow(iNewRow, '', '', 'Insert');

		if(iOptionsInThisModal <= 0) {
			$("#adjContainerModal").html(strHTML);
		} else {
			$("#adjContainerModal").append(strHTML);
		}

		$(".numeric").numeric();

		iOptionsInThisModal++;

		changeSelectsToSelect2s();
	}

</script>


<cfoutput>
	<div id="popCondoMasterUnitTypeAddIns" class="modal">
	  <div class="modal-condomaster-unitTypes-Addins-Popup">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" onclick="showParent()" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Condo Unit Codes</h4>
	      </div>
	      <div class="modal-body" id="adjContainerModal">
	      </div>

	      <div class="modal-footer">
		  	<button id="addRowButton" class="btn btn-primary btn-add-option-udf" type="button" onclick="addAdjRow()"><i class="glyphicon glyphicon-plus-sign"></i> Add Another Option</button>
	        <button type="button" onclick="showParent()" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-ok-sign"></i> Finished</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-land-popupUDF -->
	</div><!-- /.modal -->
</cfoutput>

<cfsetting showdebugoutput="No">

