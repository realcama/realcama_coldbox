<cfparam name="ParcelID" default="">
<cfparam name="taxYear" default="#Year(now())#">
<cfparam name="parcelType" default="R">


<link href="/includes/css/select2.css" rel="stylesheet" />
<script src="/includes/js/select2.js"></script>

<script>
	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qryCondoUnitTypesData#"
		TOPLEVELVARIABLE="modalUnitTypesData">

	function changesDone() {
		wddxSerializer = new WddxSerializer();
		wddxData1 = wddxSerializer.serialize(modalUnitTypesData);
		$("#modalPacket").val(wddxData1);
		/* Show the please wait screen */
		$('#myPleaseWait').modal('show');
		/* Submit the data */
		$("#frmModalSave").submit();
		/* Close the modal window */
		parent.$.fancybox.close();

	}
	function changeModalVal(iRow,obj) {
		inVal = $(obj).val();
		modalUnitTypesData.setField(iRow,"mode","Edit");
		if(inVal.length == 0) {
			inVal = $(obj).data("originalval");
			$(obj).val(inVal);
		}
		modalUnitTypesData.setField(iRow,obj.name,inVal); /* Set the mode to EDIT so that we only process this row */
	}

	$(function() {
		$(".numeric").numeric();
	});
</script>


<cfoutput>

	<form action="#event.buildLink('condoMaster/condoMasterSave')#" method="post" name="frmModalSave" id="frmModalSave">
		<input type="hidden" name="packet" id="modalPacket" value="">
		<input type="hidden" name="packet2" id="packet2" value="">	<!--- LEAVE THIS FIELD BLANK !!!! --->
		<input type="hidden" name="tab" id="tab" value="modalUnitTypes">
		<input type="hidden" name="ParcelID" id="ParcelID" value="#ParcelID#">
		<input type="hidden" name="taxYear" id="taxYear" value="#taxYear#">
	</form>


	<div id="popCondoMasterBaseValue" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-land-popupUDF">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Base Value Mass Update Tool</h4>
	      </div>
	      <div class="modal-body">
			<table class="table-bordered table-striped table-hover"  id="condo-master-unit-types">
				<thead>
				<tr>
					<th width="33%" class="formatted-data-label">Unit Type</th>
					<th width="33%" class="formatted-data-label text-center">Base Value</th>
					<th width="33%" class="formatted-data-label text-center">New Base Value</th>
				</tr>
				</thead>
				<tbody>
					<cfloop query="prc.qryCondoUnitTypesData">
						<tr>
							<td class="formatted-data-values">&nbsp;#prc.qryCondoUnitTypesData.unittype#</td>
							<td class="formatted-data-values text-right">$#NumberFormat(prc.qryCondoUnitTypesData.unitbaseamt,",")#&nbsp;&nbsp;</td>
							<td class="formatted-data-values text-center">
								$<input type="text" name="unitbaseamt" data-originalval="#numberFormat(prc.qryCondoUnitTypesData.unitbaseamt,".00")#" value="#numberFormat(prc.qryCondoUnitTypesData.unitbaseamt,".00")#" class="form-control numeric width-80percent" onchange="changeModalVal(#prc.qryCondoUnitTypesData.CurrentRow-1#,this)">
							</td>
						</tr>
					</cfloop>
				</tbody>
			</table>
		  </div>

	      <div class="modal-footer">
		  	<span class="pull-left"><CFIF Controller.getSetting( "bShowPacketLinks")><a href="javascript:void(0);" onclick="WriteRaw(modalUnitTypesData);">Unit Type Packet</a></span></cfif>

			<button type="button"  class="btn btn-default" onclick="parent.$.fancybox.close();"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</button>
			<button type="button"  class="btn btn-default" onclick="changesDone();"><i class="glyphicon glyphicon-floppy-disk"></i> Save Changes</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-land-popupUDF -->
	</div><!-- /.modal -->
</cfoutput>

<cfsetting showdebugoutput="No">



