
<cfdump Var="#rc#">
<cfdump var="#parent.unitTypesData#">
<cfabort>
<script>
	<cfoutput>
	var condoID = '#rc.condoID#';
	var condoTypicalID = '#rc.condoTypicalID#';

	function populateValues() {
		$("##unittype").val(parent.unitTypesData.getField(#row-1#,"unittype"));
		$("##numberunits").val(parent.unitTypesData.getField(#row-1#,"numberunits"));
		$("##numberbedrooms").val(parent.unitTypesData.getField(#row-1#,"numberbedrooms"));
		$("##numberbaths").val(parent.unitTypesData.getField(#row-1#,"numberbaths"));
		$("##numberrooms").val(parent.unitTypesData.getField(#row-1#,"numberrooms"));
		$("##sqft").val(parent.unitTypesData.getField(#row-1#,"sqft"));
		$("##floorfrom").val(parent.unitTypesData.getField(#row-1#,"floorfrom"));
		$("##floorto").val(parent.unitTypesData.getField(#row-1#,"floorto"));
		$("##unitbaseamt").val(parent.unitTypesData.getField(#row-1#,"unitbaseamt"));
		$("##floorrateadjtotal").val(parent.unitTypesData.getField(#row-1#,"floorrateadjtotal"));
		$("##floorrateadj").val(parent.unitTypesData.getField(#row-1#,"floorrateadj"));
	}
	</cfoutput>



	$( document ).ready(function() {
		populateValues();
		createLabel();
		$(".numeric").numeric();
	});

	function createLabel(){
		noNulls();
		var strLabel = "";

		var iLumpSum = 0;
		var iPercentage = 0;
		var iRateAddOn = 0;
		var iPointsSum = 0;

		/* find out if we have to use depth or not in the calculation of the net adjustment row */
		for ( var iCurrentRow = 0; iCurrentRow < parent.unitTypicalCodesData.getRowCount(); iCurrentRow++ ) {
			mode = parent.unitTypicalCodesData.getField(iCurrentRow,"mode");

			codetype = parent.unitTypicalCodesData.getField( iCurrentRow, "codetype");
			code = parent.unitTypicalCodesData.getField( iCurrentRow, "code");
			codedescription = parent.unitTypicalCodesData.getField( iCurrentRow, "codedescription");

			amount = parseFloat(parent.unitTypicalCodesData.getField( iCurrentRow, "adjustmentamount"));
			factor = parseFloat(parent.unitTypicalCodesData.getField( iCurrentRow, "adjustmentfactor"));


			condotypicalid = parent.unitTypicalCodesData.getField( iCurrentRow, "condotypicalid");

			if(condotypicalid == <cfoutput>#condoTypicalID#</cfoutput>) {
				if(mode != "Delete") {

					if(
						(codetype == null || codetype == "Select" || codetype === "") ||
						(codedescription == null || codedescription == "Select") <!--- ||  codedescription == "N/A" --->
						){
						<!--- this is a junk entry, not all required pieces are selected --->

					} else {

						if(amount > 0 && factor == 0.00) {

							strLabel = strLabel + "<tr>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + codetype + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + code + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values' width='80'>" + codedescription + "</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>" + amount.toFixed(3) + "</td>";
							strLabel = strLabel + "</tr>";
							iLumpSum = iLumpSum + amount;
						}
						if(amount == 0.00 && factor > 0) {
							var iDispFactor = factor * 1;
							strLabel = strLabel + "<tr>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + codetype + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values'>" + code + "</td>";
							strLabel = strLabel + "		<td class='formatted-data-values' width='80'>" + codedescription + "</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>" + iDispFactor.toFixed(3) +"</td>";
							strLabel = strLabel + "		<td class='rightAlign formatted-data-values'>&nbsp;</td>";
							strLabel = strLabel + "</tr>";
							if(iPercentage == 0) {
								iPercentage = factor;
							} else {
								iPercentage = iPercentage * factor;
							}
						}
					} /* END else */
				} /* END mode != "Delete"*/
			} /* END condotypicalid == [] */
		} /* END for*/

		if(strLabel === "") {
			strLabel = "<tr><td colpan='7'>None</td></tr>";
		}


		<cfoutput>
		$("##udf_#row#").html(strLabel);
		$("##condomaster_netadj_points_#row#").html(iPointsSum.toFixed(3));
		$("##condomaster_netadj_lumpsum_#row#").html(iLumpSum.toFixed(3));
		$("##condomaster_netadj_percentage_#row#").html(iPercentage.toFixed(3));
		$("##condomaster_netadj_rateaddon_#row#").html(iRateAddOn.toFixed(3));
		</cfoutput>
	}
</script>

<cfoutput>
<div id="popCondoMasterUnitType" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-condomaster-unitTypesPopup">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Unit Types</h4>
			</div>
			<div class="modal-body" id="adjContainer">

				<div class="row fix-bs-row">

					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Unit Type</span><br />
						<span class="formatted-data-values">
							<input type="text" name="unittype" id="unittype" class="form-control" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Number of Units</span><br />
						<span class="formatted-data-values">
							<input type="text" name="numberunits" id="numberunits" class="form-control numeric" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Bedrooms</span><br />
						<span class="formatted-data-values">
							<input type="text" name="numberbedrooms" id="numberbedrooms" class="form-control numeric" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Bathrooms</span><br />
						<span class="formatted-data-values">
							<input type="text" name="numberbaths" id="numberbaths" class="form-control numeric" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="clearfix"></div>

					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Rooms</span><br />
						<span class="formatted-data-values">
							<input type="text" name="numberrooms" id="numberrooms" class="form-control numeric" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Square Feet</span><br />
						<span class="formatted-data-values">
							<input type="text" name="sqft" id="sqft" class="form-control numeric" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Floors</span><br />
						<span class="formatted-data-values">
							<input type="text" name="floorfrom" id="floorfrom" class="form-control condo-master-floor numeric" onChange="changeVal('unitTypesData',#row-1#,this)">
							<span class="formatted-data-label"> - </span>
							<input type="text" name="floorto" id="floorto" class="form-control condo-master-floor numeric" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Base Value</span><br />
						<span class="formatted-data-values">
							$<input type="text" name="unitbaseamt" id="unitbaseamt" class="form-control numeric width-90percent" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="clearfix"></div>

					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Floor Adjustment</span><br />
						<span class="formatted-data-values">
							$<input type="text" name="floorrateadjtotal" id="floorrateadjtotal" class="form-control numeric width-90percent" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Floor Adj Type</span><br />
						<span class="formatted-data-values">
							<input type="text" name="floorrateadj" id="floorrateadj" class="form-control" onChange="changeVal('unitTypesData',#row-1#,this)">
						</span>
					</div>
					<div class="col-xs-6 databox">
						<span class="formatted-data-label"></span><br />
						<span class="formatted-data-values"></span>
					</div>
				</div>

				<div class="clearfix">&nbsp;</div>

				<div class="row fix-bs-row add-in-box">
					<div class="col-xs-12">
						<table class="building-udf-table">
						<thead>
							<tr>
								<td width="15%"><span class="formatted-data-label">Add-Ins</span></td>
								<td width="25"></td>
								<td width="20%"></td>
								<td width="15%" class="rightAlign"><span class="formatted-data-label">Points</span></td>
								<td width="15%" class="rightAlign"><span class="formatted-data-label">Lump Sum</span></td>
								<td width="15%" class="rightAlign"><span class="formatted-data-label">Percentage</span></td>
								<td width="15%" class="rightAlign"><span class="formatted-data-label">Rate Add-On</span></td>
							</tr>
						</thead>
						<tbody id="udf_#row#">
							<tr><td colspan="7">None</td></tr>
						</tbody>
						<tfoot class="adjustmentFooter">
							<tr>
								<td width="15%"></td>
								<td colspan="2" class="rightAlign formatted-data-label">Net Adjustments</td>
								<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_points_#row#">#NumberFormat(0,"0.000")#</td>
								<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_lumpsum_#row#">#NumberFormat(0,"0.000")#</td>
								<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_percentage_#row#">#NumberFormat(0,"0.000")#</td>
								<td width="15%" class="rightAlign formatted-data-label" id="condomaster_netadj_rateaddon_#row#">#NumberFormat(0,"0.000")#</td>
							</tr>
						</tfoot>
						</table>
					</div>

					<div class="col-xs-12 building-button-row">
						 <a data-toggle="modal" href="##popCondoMasterUnitTypeAddIns" data-keyboard="false" data-backdrop="static" onclick="doIt()"><img src="/images/v2/addedit_ADDins_blue.gif" alt="" width="134" height="26" border="0"></a>
					</div>
				</div>

			</div>

			<div class="modal-footer">
				<button type="button"  class="btn btn-default"  onclick="parent.$.fancybox.close();"><i class="glyphicon glyphicon-ok-sign"></i> Finished</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-land-popupUDF -->
</div><!-- /.modal -->

</cfoutput>
