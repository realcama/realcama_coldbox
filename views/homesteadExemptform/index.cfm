<cfoutput>
<div id="hexform" class="form-wrap">
	<form class="form-inline custom-form">
	<h1>#prc.pageTitle#</h1>
	<h2>Permanent Florida residency required on January 1.<br>
	Application due to property appraiser by March 1.</h2>

	<div class="row form-group form-borders first">
		<div class="col-md-4">
			<label for="county">County</label>
			<select id="county" name="" class="form-control">
				<option>Nassau</option>
			</select>
		</div>
		<div class="col-md-3">
			<label for="tax_year">Tax Year</label>
			<input id="tax_year" type="Text" name="tax_year" value="#rc.taxYear#" class="form-control input-sm" />
		</div>
		<div class="col-md-5">
			<label>Parcel ID</label>
			<input type="Text" name="parcelid" value="#rc.parcelID#" class="form-control" />
		</div>
	</div>
	<div class="row form-group form-borders ">
		<div class="col-md-8">
			<label>I am applying for homestead exemption, $25,000 to $50,000</label>
		</div>
		<div class="col-md-4">
			<span>New</span> <input type="radio" name="hxapply" <cfif prc.getHexInfo.eaflappltp is 'N'>checked</cfif>> <span>Change</span> <input type="radio" name="hxapply" <cfif prc.getHexInfo.eaflappltp is 'C'>checked</cfif>>
		</div>
	</div>
	<div class="row form-group form-borders ">
		<div class="col-md-6">
			<label>Do you claim residency in another county or state?</label>
		</div>
		<div class="col-md-3">
			<span>Applicant?</span> <span>Yes</span> <input type="radio" name="hxres" <cfif prc.getHexInfo.eofltbosq is 'Y'>checked</cfif>> <span>No</span> <input type="radio" name="hxres" <cfif prc.getHexInfo.eofltbosq is 'N'>checked</cfif>>
		</div>
		<div class="col-md-3">
			<span>Co-Applicant?</span> <span>Yes</span> <input type="radio" name="hxcores"> <span>No</span> <input type="radio" name="hxcores">
		</div>
	</div>

	<table>
	<tr class="form-borders ">
		<td class="col-md-2"></td>
		<td class="col-md-5">
			<h3>Applicant</h3>
		</td>
		<td class="col-md-5">
			<h3>Co-Applicant</h3>
		</td>
	</tr>
	<tr class="form-borders ">
		<td class="col-md-2">
			<label>Name</label>
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="#rc.owner_name#" class="form-control input-full" />
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="" class="form-control input-full" />
		</td>
	</tr>
	<tr class="form-borders ">
		<td class="col-md-2">
			<label>*Social Security ##</label>
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="" class="form-control input-full" />
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="" class="form-control input-full" />
		</td>
	</tr>
	<tr class="form-borders ">
		<td class="col-md-2">
			<label>Immigration ##</label>
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="#prc.getHexInfo.eoflimgrn#" class="form-control input-full" />
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="" class="form-control input-full" />
		</td>
	</tr>
	<tr class="form-borders ">
		<td class="col-md-2">
			<label>Date of birth</label>
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="#mid(prc.getHexInfo.eofldob,5,2)#/#mid(prc.getHexInfo.eofldob,7,2)#/#left(prc.getHexInfo.eofldob,4)#" class="form-control input-full" />
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="" class="form-control input-full" />
		</td>
	</tr>
	<tr class="form-borders">
		<td class="col-md-2">
			<label>% of ownership</label>
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="#NumberFormat(prc.getHexInfo.eaflpctown,'999')#%" class="form-control input-full" />
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="" class="form-control input-full" />
		</td>
	</tr>

	<tr class="form-borders">
		<td class="col-md-2">
			<label>Date of permanent residency</label>
		</td>
		<td class="col-md-5">
			<input type="Text" name="" <cfif prc.getHexInfo.eoflodat GT '0'>value="#mid(prc.getHexInfo.eoflodat,5,2)#/#mid(prc.getHexInfo.eoflodat,7,2)#/#left(prc.getHexInfo.eoflodat,4)#"</cfif> class="form-control input-full" />
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="" class="form-control input-full" />
		</td>
	</tr>
	</table>

		<div class="row form-group form-borders ">
			<div class="col-md-2">
				<label>Marital status</label>
			</div>
			<div class="col-md-10">
				<input type="radio"> <span>Single</span> <input type="radio" <cfif prc.getHexInfo.eoflmstat is 'S'>checked</cfif>> <span>Married</span> <input type="radio"<cfif prc.getHexInfo.eoflmstat is 'M'>checked</cfif>> <span>Divorced</span> <input type="radio" <cfif prc.getHexInfo.eoflmstat is 'D'>checked</cfif>> <span>Widowed</span> <input type="radio"<cfif prc.getHexInfo.eoflmstat is 'W'>checked</cfif>>
			</div>

		</div>
		<div class="row form-group form-borders ">
			<div class="col-md-6">
				<label>Homestead address</label><br />
				<textarea class="form-control">#prc.getHexInfo.eoflosaddr#</textarea>
			</div>
			<div class="col-md-6">
				<label>Mailing address, if different</label><br />
				<textarea class="form-control">#trim(prc.getHexInfo.eafladdr1)# #trim(prc.getHexInfo.eaflcity)# #trim(prc.getHexInfo.eaflstate)# #prc.getHexInfo.eaflzip5#


				</textarea>
			</div>
		</div>
		<div class="row form-group form-borders ">
			<div class="col-md-6">
				<label>Legal description</label><br />
				<textarea class="form-control"><cfloop query="prc.getLegalInfo">#LEGALDESCRIPTION#</cfloop></textarea>
			</div>
			<div class="col-md-6">
				<label>Previous address</label><br />
				<textarea class="form-control">#prc.getHexInfo.eaflpvhxad#</textarea>
			</div>
		</div>
		<div class="row form-group form-borders ">
			<div class="col-md-6">
				<label>Type of deed</label> <input type="Text" name="#prc.getHexInfo.eafldeedtp#" value="" class="form-control input-sm" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<label>Date of deed</label> <input type="Text" name="" <cfif prc.getHexInfo.eafldeeddt GT '0'>value="#mid(prc.getHexInfo.eafldeeddt,5,2)#/#mid(prc.getHexInfo.eafldeeddt,7,2)#/#left(prc.getHexInfo.eafldeeddt,4)#"</cfif>" class="form-control input-sm" />
			</div>
			<div class="col-md-6">
				<label>Recorded:</label> <span>Book</span>&nbsp;<input type="Text" name="" value="" class="form-control input-xs" />&nbsp;&nbsp;
				<span>Page</span>&nbsp;<input type="Text" name="#prc.getHexInfo.eaflbook#" value="" class="form-control input-xs" />&nbsp;
				<span>Date</span>&nbsp;<input type="Text" name="#prc.getHexInfo.eaflpage#" value="" class="form-control input-xs" />
			</div>
		</div>
		<div class="row form-group form-borders ">
			<div class="col-md-12">
				<label>Did any applicant receive or file for exemptions last year?</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Yes</span> <input type="radio"> <span>No</span> <input type="radio">
			</div>
		</div>
		<div class="row form-group form-borders last ">
			<div class="col-md-12">
				<label>Phone</label> <input type="Text" name="" value="" class="form-control input-sm" />

			</div>
		</div>
		<div class="row form-group ">
			<div class="col-md-12">
				<span class="disclaimer">Please provide as much information as possible. Your county property appraiser will make the final determination.</span>
			</div>
		</div>
		<table>
		<tr class="form-borders ">
			<td class="col-md-4">
				<h3>Proof of Residence</h3>
			</td>
			<td class="col-md-4">
				<h3>Applicant</h3>
			</td>
			<td class="col-md-4">
				<h3>Co-Applicant/Spouse</h3>
			</td>
		</tr>
		<tr class="form-borders ">
			<td class="col-md-4">
				<label>Previous residency outside Florida and date terminated</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text" name="" value="" class="form-control input-sm" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text" name="" value="" class="form-control input-sm" />
			</td>
		</tr>
		<tr class="form-borders ">
			<td class="col-md-4">
				<label>FL driver license or ID card number</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eofldln#" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text" name="" value="" class="form-control input-sm" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text" name="" value="" class="form-control input-sm" />
			</td>
		</tr>
		<tr class="form-borders ">
			<td class="col-md-4">
				<label>Evidence of relinquishing driver license from other state</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eofltbos#" class="form-control input-full" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-full" />
			</td>
		</tr>
		<tr class="form-borders ">
			<td class="col-md-4">
				<label>Florida vehicle tag number</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eofltagn#" class="form-control input-full" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-full" />
			</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-4">
				<label>Florida voter registration number (if US citizen)</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eoflvregn#" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text"  name="" <cfif prc.getHexInfo.eoflvrdat GT '0'>value="#mid(prc.getHexInfo.eoflvrdat,5,2)#/#mid(prc.getHexInfo.eoflvrdat,7,2)#/#left(prc.getHexInfo.eoflvrdat,4)#"</cfif> class="form-control input-sm" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text"  name="" value="" class="form-control input-sm" />
			</td>
		</tr>

		<tr class="form-borders">
			<td class="col-md-4">
				<label>Declaration of domicile, enter date</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eofldecdom#" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text" name="" value="" class="form-control input-sm" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-md" />&nbsp;&nbsp;&nbsp;&nbsp;
				date <input type="Text" name="" value="" class="form-control input-sm" />
			</td>
		</tr>

		<tr class="form-borders">
			<td class="col-md-4">
				<label>Current employer</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eoflempl#" class="form-control input-full" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-full" />
			</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-4">
				<label>Address on your last IRS return</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eofllirsad1# #prc.getHexInfo.eofllirsad2#" class="form-control input-full" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-full" />
			</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-4">
				<label>School location of dependent children</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="#prc.getHexInfo.eofldepsch#" value="" class="form-control input-full" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-full" />
			</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-4">
				<label>Bank statement and checking account mailing address</label>
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="#prc.getHexInfo.eoflbankad#" class="form-control input-full" />
			</td>
			<td class="col-md-4">
				<input type="Text" name="" value="" class="form-control input-full" />
			</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-4">
				<label>Proof of payment of utilities at homestead address</label>
			</td>
			<td class="col-md-4">
				<span>Yes</span> <input type="radio" name="hxutil" <cfif prc.getHexInfo.eofllutprf is 'Y'>checked</cfif>> <span>No</span> <input type="radio" name="hxutil" <cfif prc.getHexInfo.eofllutprf is 'N'>checked</cfif>>
			</td>
			<td class="col-md-4">
				<span>Yes</span> <input type="radio"> <span>No</span> <input type="radio">
			</td>
		</tr>
		</table>

	<div class="row form-group ">
		<div class="col-md-12">
			<span class="disclaimer">* Disclosure of your social security number is mandatory. It is required by section 196.011(1)(b), Florida Statutes. The social security number will be used to verify taxpayer identity and homestead exemption information submitted to property appraisers.</span>
		</div>
	</div>
	<div class="row form-group border-container">
		<div class="col-md-12">
			<span class="disclaimer"><strong>In addition to homestead exemption, I am applying for the following benefits.</strong><br />
			<a href="##qualification">See below</a> for qualification and required documents.</span>
		</div>
	</div>

	<div class="row form-group border-container">
		<div class="col-md-12">
			<span>By local ordinance only:</span><br />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"><label>&nbsp;&nbsp;Age 65 and older with limited income (amount determined by ordinance)</label><br />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"><label>&nbsp;&nbsp;Age 65 and older with limited income and permanent residency for 25 years or more</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;$500 widowed</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"><label>&nbsp;&nbsp;$500 blind</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"><label>&nbsp;&nbsp;$500 totally and permanently disabled</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Total and permanent disability - quadriplegic</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Certain total and permanent disabilities - limited income and hemiplegic, paraplegic, wheelchair required, or legally blind</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Disabled veteran discount, 65 or older</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Veteran disabled 10% or more</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Disabled veteran confined to wheelchair, service-connected</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Service-connected totally and permanently disabled veteran or surviving spouse</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Surviving spouse of veteran who died while on active duty</label><br />
				<input type="checkbox"><label>&nbsp;&nbsp;Surviving spouse of first responder who died in the line of duty</label><br />
				<label>Other, specify:</label>&nbsp;&nbsp;<input type="Text" name="" value="" class="form-control input-md" />
				<div class="spacer10"></div>
		</div>
	</div>

		<div class="row form-group">
			<div class="col-md-12">
				<div class="spacer10"></div>
				<p>
				<strong>I authorize this agency to obtain information to determine my eligibility for the exemptions applied for.  I qualify for these exemptions under Florida Statutes.  I own the property above and it is my permanent residence or the permanent residence of my legal or natural dependent(s).  (See s. 196.031, F.S.)</strong>
				</p>
				<p>
				<strong>I understand that under section 196.131(2), Florida Statutes, any person who knowingly and willfully gives false information to claim homestead exemption is guilty of a misdemeanor of the first degree, punishable by imprisonment up to one year, a fine up to $5,000, or both.</strong>
				</p>
				<p>
				<strong>Under penalties of perjury, I declare that I have read the foregoing application and the facts in it are true.</strong>
				</p>
				<div class="spacer10"></div>
				<h2>Signed __________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#dateformat(now(),'mm/dd/yyyy')#</h2>
				<div class="spacer10"></div>
				<div class="spacer10"></div>
				<div class="spacer10"></div>
				<h3>Penalties</h3>
				<p>
				The property appraiser has a duty to put a tax lien on your property if you received a homestead exemption during the past 10 years that you were not entitled to. The property appraiser will notify you that taxes with penalties and interest are due.  You will have 30 days to pay before a lien is recorded. If this was not an error by the property appraiser, you will be subject to a penalty of 50 percent of the unpaid taxes and 15 percent interest each year (see s. 196.011(9)(a), F.S.).  For special requirements for estates probated or administered outside Florida, see s. 196.161(1), F.S.
				</p>
				<p>
				The information in this application will be given to the Department of Revenue. Under s. 196.121, F.S., the Department and property appraisers can give this information to any state where the applicant has resided. Social security numbers will remain confidential under s.193.114(5), F.S.
				</p>
				<p style="text-align:center;">
				<strong>Contact your local property appraiser or visit the Department of Revenue's website at <a href="http://dor.myflorida.com/dor/property/" target="_blank">http://dor.myflorida.com/dor/property/</a></strong>

				</p>
				<div class="spacer10"></div>
				<a name="qualification"></a>
				<h3>EXEMPTION AND DISCOUNT REQUIREMENTS</h3>
				<p>
				<strong>Homestead</strong>   Every person who owns real property in Florida on January 1, makes the property his or her permanent residence or the permanent residence of a legal or natural dependent, and files an application may receive a property tax exemption up to $50,000. The first $25,000 applies to all property taxes. The added $25,000 applies to assessed value over $50,000 and only to non-school taxes.
				</p>
				<p>
				Your local property appraiser will determine whether you are eligible. The appraiser may consider information such as the items requested on the bottom of page 1.
				</p>
				<p>
				<strong>Save our Homes (SOH)</strong>   Beginning the year after you receive homestead exemption, the assessment on your home cannot increase by more than the lesser of the change in the Consumer Price Index or 3 percent each year, no matter how much the just value increases. If you have moved from one Florida homestead to another within the last two years, you may be eligible to take some of your SOH savings with you. See your property appraiser for more information.
				</p>
				<p style="text-align:center;font-size:.8em;">
				This page does not contain all the requirements that determine your eligibility for an exemption. Consult your local property appraiser and Chapter 196, Florida Statutes, for details.
				</p>
			</div>
		</div>

		<table>
		<tr class="form-borders">
			<td colspan="5" class="bg-dark">
				<h4 class="invert">Added Benefits Available for Qualified Homestead Properties</h4>
			</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3"></td>
			<td class="col-md-2"><h4>Amount</h4></td>
			<td class="col-md-3"><h4>Qualifications</h4></td>
			<td class="col-md-3"><h4>Forms and Documents*</h4></td>
			<td class="col-md-1"><h4>Statute</h4></td>
		</tr>
		<tr class="form-borders">
			<td colspan="5" class="bg-light"><h5>Exemptions</h5></td>
		</tr>
		<tr class="form-borders">
			<td rowspan="2" class="col-md-3">Local option, age 65 and older</td>
			<td class="col-md-2">Determined by local ordinance</td>
			<td class="col-md-3">Local ordinance, limited income</td>
			<td class="col-md-3">Proof of age DR-501SC, household income</td>
			<td rowspan="2" class="col-md-1">196.075</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-2">The amount of the assessed value</td>
			<td class="col-md-3">Local ordinance, just value under $250,000, permanent residency for 25 years or more.</td>
			<td class="col-md-3">DR-501SC, household income</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Widowed</td>
			<td class="col-md-2">$500</td>
			<td class="col-md-3"></td>
			<td class="col-md-3">Death certificate of spouse</td>
			<td class="col-md-1">196.202</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Blind</td>
			<td class="col-md-2">$500</td>
			<td class="col-md-3"></td>
			<td class="col-md-3">physician, DVA*, or SSA**td>
			<td class="col-md-1">196.202</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3" rowspan="3">Totally and Permanently Disabled</td>
			<td class="col-md-2">$500</td>
			<td class="col-md-3">Disabled</td>
			<td class="col-md-3">Florida physician, DVA*, or SSA**</td>
			<td class="col-md-1" rowspan="3">196.202</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-2">All taxes</td>
			<td class="col-md-3">Quadriplegic</td>
			<td class="col-md-3">2 Florida physicians or DVA*</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-2">All taxes</td>
			<td class="col-md-3">Hemiplegic, paraplegic, wheelchair required for mobility, or legally blind Limited income</td>
			<td class="col-md-3">
				DR-416, DR-416B, or
				letters from 2 FL physicians
				(For the legally blind, one can be an optometrist.)
				Letter from DVA*, and
				DR-501A, household income
			</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3 bg-light" colspan="5"><h5>Veterans and First Responders Exemptions and Discount</h5></td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Disabled veteran discount, age 65 and older</td>
			<td class="col-md-2">% of disability</td>
			<td class="col-md-3">Combat-related disability</td>
			<td class="col-md-3">
				Proof of age, DR-501DV
				Proof of disability, DVA*, or
				US government
			</td>
			<td class="col-md-1">196.082</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Veteran, disabled 10% or more by misfortune or during wartime service</td>
			<td class="col-md-2">Up to $5,000</td>
			<td class="col-md-3">Veteran or surviving spouse of at least 5 years</td>
			<td class="col-md-3">of disability, DVA*, or US government</td>
			<td class="col-md-1">196.24</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Veteran confined to wheelchair, service-connected, totally disabled</td>
			<td class="col-md-2">All taxes</td>
			<td class="col-md-3">Veteran or surviving spouse</td>
			<td class="col-md-3">Proof of disability, DVA*, or US government</td>
			<td class="col-md-1">196.091</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Service-connected, totally and permanently disabled veteran or surviving spouse</td>
			<td class="col-md-2">All taxes</td>
			<td class="col-md-3">Veteran or surviving spouse</td>
			<td class="col-md-3">Proof of disability, DVA*, or US government</td>
			<td class="col-md-1">196.081</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Surviving spouse of veteran who died while on active duty</td>
			<td class="col-md-2">All taxes</td>
			<td class="col-md-3">Surviving spouse</td>
			<td class="col-md-3">Letter attesting to the veteran's death while on active duty</td>
			<td class="col-md-1">196.081</td>
		</tr>
		<tr class="form-borders">
			<td class="col-md-3">Surviving spouse of first responder who died in the line of duty</td>
			<td class="col-md-2">All taxes</td>
			<td class="col-md-3">Surviving spouse</td>
			<td class="col-md-3">Letter attesting to the first responder's death in the line of duty</td>
			<td class="col-md-1">196.081</td>
		</tr>
		</table>

		<div class="row form-group border-container">
			<div class="col-md-12">
				<span class="disclaimer">
				<strong>Department of Revenue (DR) forms are available at <a href="http://dor.myflorida.com/dor/property/forms/" target="_blank">http://dor.myflorida.com/dor/property/forms/.</a>
				<br />
				*DVA is the US Department of Veterans Affairs or its predecessor.**SSA is the Social Security Administration.
				</span>
			</div>
		</div>
	</form>

<!-------- TRANSFER FORM -------->
<cfif ParameterExists(rc.transferchk)>
	<div class="spacer10"></div>
	<div class="spacer10"></div>
	<div class="spacer10"></div>
	<div class="spacer10"></div>

	<form class="form-inline custom-form">
	<div class="row form-group">
		<div class="col-md-12">
			<h1>Transfer Of Homestead Assessment Difference</h1>
			<h2>Attachment to Original Application for Homestead Tax Exemption</h2>
			<p style="text-align:center;"><strong>Section 193.155, Florida Statutes</strong></p>
			<p>If you have applied for a new homestead exemption and are entitled to transfer a homestead assessment
				difference from a previous homestead, file this form with your property appraiser by
				<strong>March 1</strong>.</p>
			<p>Co-applicants transferring from a different homestead must fill out a separate form.</p>
		</div>
	</div>
	<table>
	<tr class="form-borders">
		<td colspan="3" class="bg-light"><h3 class="dark">COMPLETED BY APPLICANT</h3></td>
	</tr>
	<tr class="form-borders">
		<td colspan="3"><h3 class="left dark">PART 1.  New Homestead</h3></td>
	</tr>
	<tr class="form-borders">
		<td class="col-md-2">
			<label>Applicant name</label>
		</td>
		<td class="col-md-5">
			<input type="Text" name="" value="#rc.owner_name#" class="form-control input-full" />
		</td>
		<td class="col-md-5" rowspan="2">
			<table>
				<tr>
					<td class="col-md-2"><label>Phone 1</label></td>
					<td class="col-md-4"><input type="Text" name="" value="#prc.getHexInfo.eoflhphone#" class="form-control input-full" /></td>
					<td class="col-md-2">&nbsp;&nbsp;<label>Phone 2</label></td>
					<td class="col-md-4"><input type="Text" name="" value="#prc.getHexInfo.eoflwphone#" class="form-control input-full" /></td>
				</tr>
				<tr>
					<td class="col-md-2"><label>Parcel ID</label></td>
					<td class="col-md-10" colspan="3"><input type="Text" name="" value="#rc.parcelID#" class="form-control input-full" /></td>
				</tr>
				<tr>
					<td class="col-md-2">
					<label>County</label>
					</td>
					<td class="col-md-10" colspan="3">
					<input type="Text" name="" value="" class="form-control input-full" />
					</td>
				</tr>
				<tr>
					<td class="col-md-10" colspan="4">
					<label>Total number of applicants</label> <input type="Text" name="" value="#prc.getHexInfo.eaflnclm#" class="form-control input-sm" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="form-borders">
		<td class="col-md-2">
			<label>New address</label>
		</td>
		<td class="col-md-5">
			<textarea style="min-height:120px;">#trim(prc.getHexInfo.eafladdr1)# #trim(prc.getHexInfo.eaflcity)# #trim(prc.getHexInfo.eaflstate)# #prc.getHexInfo.eaflzip5#</textarea>
		</td>
	</tr>
	<tr class="form-borders">
		<td colspan="3">
			<h3 class="left dark">PART 2.  Previous Homestead</h3>
		</td>
	</tr>
	<tr class="form-borders">
		<td class="col-md-2">
			<label>Previous address</label>
		</td>
		<td class="col-md-5">
			<textarea style="min-height:120px;">#trim(prc.getHexInfo.eaflpvhxad)#</textarea>
		</td>
		<td class="col-md-5">
			<table>
				<tr>
					<td class="col-md-2">
					<label>Parcel ID</label>
					</td>
					<td class="col-md-10" colspan="3">
					<input type="Text" name="#trim(prc.getHexInfo.eaflptprop)#" value="" class="form-control input-full" />
					</td>
				</tr>
				<tr>
					<td class="col-md-2">
					<label>County</label>
					</td>
					<td class="col-md-10" colspan="3">
					<input type="Text" name="" value="" class="form-control input-full" />
					</td>
				</tr>
				<tr>
					<td class="col-md-10" colspan="4">
					<label>Date sold or no longer used as your homestead</label> <input type="Text" name="" <cfif prc.getHexInfo.eaflptdphl GT '0'>value="#mid(prc.getHexInfo.eaflptdphl,5,2)#/#mid(prc.getHexInfo.eaflptdphl,7,2)#/#left(prc.getHexInfo.eaflptdphl,4)#"</cfif> class="form-control input-xs" />
					</td>
				</tr>
			</table>

		</td>
	</tr>
	<tr class="form-borders">
		<td colspan="3">
			<h3 class="left dark">PART 3.  Signature of Applicant and All Co-applicants</h3>
		</td>
	</tr>
	<tr class="form-borders">
		<td colspan="3">
			<p>
			I affirm that I qualify for the homestead exemption assessment transfer from the previous homestead above.
			Under penalties of perjury, I declare that I have read this application and the facts in it
			are true.
			</p>
			<div class="spacer10"></div>
			<h2>Signed __________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#dateformat(now(),'mm/dd/yyyy')#</h2>
			<div class="spacer10"></div>
			<div class="spacer10"></div>
			<div class="spacer10"></div>
		</td>
	</tr>
	</table>
	<div class="spacer25"></div>
		<table>
		<tr class="form-borders">
			<td colspan="3" class="bg-light">
				<h3 class="dark">COMPLETED BY PROPERTY APPRAISER OF NEW HOMESTEAD</h3>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="spacer10"></div>
				<h2>Signed __________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#dateformat(now(),'mm/dd/yyyy')#</h2>
				<div class="spacer10"></div>
				<div class="spacer10"></div>
				<div class="spacer10"></div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<p><strong>If the previous homestead was in a different county, add your contact information. Send this form with a copy
					of the Original Application for Homestead Tax Exemption (Form DR-501) to the property appraiser's office in the county of the previous homestead.</strong></p>
			</td>
		</tr>
		<tr>
			<td class="col-md-2">
				<label>Previous address</label>
			</td>
			<td class="col-md-5">
				<textarea style="min-height:120px;"></textarea>
			</td>
			<td class="col-md-5">
				<table>
					<tr>
						<td class="col-md-2"><label>Email</label></td>
						<td class="col-md-10"><input type="Text" name="" value="" class="form-control input-full" /></td>
					</tr>
					<tr>
						<td class="col-md-2"><label>Phone 1</label></td>
						<td class="col-md-10"><input type="Text" name="" value="" class="form-control input-full" /></td>
					</tr>
					<tr>
						<td class="col-md-2"><label>Phone 2</label></td>
						<td class="col-md-10"><input type="Text" name="" value="" class="form-control input-full" /></td>
					</tr>
					<tr>
						<td class="col-md-2"><label>Fax</label></td>
						<td class="col-md-10"><input type="Text" name="" value="" class="form-control input-full" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="bg-light">
				<h3 class="dark">INSTRUCTIONS TO PROPERTY APPRAISER OF PREVIOUS HOMESTEAD</h3>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="spacer10"></div>
				<p><strong>Based on your county�s records, complete and return the Certificate of Transfer of Homestead Assessment
					Difference (Form DR-501RVSH) to the contact at the property appraiser's office above by April 1 or within 2 weeks
					after you receive this Transfer of Homestead Assessment Difference (Form DR-501T), whichever is later.</strong></p>
			</td>
		</tr>
		</table>
	</form>

</div>
</cfif>

<div class="row ">
	<div class="col-md-12">
		<div class="spacer10"></div>
		<div class="spacer10"></div>
		<a href="#event.buildLink( 'parcel/index' )#/taxYear/#trim(rc.taxYear)#/parcelID/#URLDecode(rc.ParcelID)#"><center><input type="button" value="Print Exemption Form(s)" class="btn btn-default"></center></a>
		<div class="spacer10"></div>
		<div class="spacer10"></div>
	</div>
</div>

</cfoutput>