<cfoutput>
	<script type="text/javascript">
		var startRow = 0;

		function pageIncrement(iPosition) {

			$("##history-page-prev").addClass("disabled");
			$("##history-page-next").addClass("disabled");

			startRow = startRow + iPosition;

			if(startRow != 1) {
				$("##history-page-prev").removeClass("disabled");
			}
			if((startRow+#Controller.getSetting( "iYearsToDisplay" )#) <= #prc.getHistoryInfo.RecordCount#) {
				$("##history-page-next").removeClass("disabled");
			}
			myApp.showPleaseWait();

			var urlToHit = "#event.buildlink("history/scrollData")#/ParcelID/#prc.stPersistentInfo.ParcelID#/taxYear/#prc.stPersistentInfo.taxYear#/startRow/" + startRow;

			$.ajax({
				url: urlToHit,
				type: "get",
				async: false, //This variable makes the function wait until a response has been returned from the page
			  	success: function(data){
					$("##data-holder").html(data);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					myApp.hidePleaseWait();
				}
			});

			myApp.hidePleaseWait();
			$('[data-toggle="tooltip"]').tooltip({html: true});
		}

		var myApp;
		myApp = myApp || (function () {
		    var pleaseWaitDiv = $('<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h4 class="modal-title">Please Wait</h4></div><div class="modal-body"><div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only"></span></div></div></div></div>');
		    return {
		        showPleaseWait: function() {
		            pleaseWaitDiv.modal();
		        },
		        hidePleaseWait: function () {
		            pleaseWaitDiv.modal('hide');
		        },

		    };
		})();


    	$(document).ready(function() {
			pageIncrement(1)
		});

	</script>

<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		<CFSET showTabOptions = "history">
		#renderView('_templates/miniNavigationBar')#
		History&nbsp;&nbsp;
	</div>

	<div id="expander-holders" class="panel panel-default history-panel">
		<div id="section-header"><span class="formatted-data-values">Parcel Information</span> <span class="formatted-data-values">#prc.getHistoryInfo.parcelDisplayID#</span></div>
		<table class="table">
			<tbody>
			<tr><td id="data-holder">
			</td></tr>
			</tbody>
		</table>
	</div>
</div>
</cfoutput>

<script type="text/javascript">
$(".siteContent").scroll(function() {
	var targetScroll = $('#data-holder').position().top;
	if ($(".siteContent").scrollTop() > "290") {
		$("#section-header").addClass("sticky-history-table");
		$("#parcel-history-year").addClass("sticky-tax-year-hdr");
	} else {
		$("#section-header").removeClass("sticky-history-table");
		$("#parcel-history-year").removeClass("sticky-tax-year-hdr");
	};
	
});
</script>
