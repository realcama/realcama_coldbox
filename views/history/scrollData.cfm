<cfsetting showdebugoutput="No">

<cfoutput>
	<cfset iColumnsToPad = 0>

	<table id="parcel-history-year" class="table">
	<tbody>
	<tr>
		<td><span class="formatted-data-label">Tax Year</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.taxYear#</span></td>
			<cfset iColumnsToPad = iColumnsToPad + 1>
		</cfoutput>
		<cfset iColumnsToPad = 3 - iColumnsToPad>
		<!--- inserts blank column if necessary --->
		<cfsavecontent variable="strPadding">
			<cfloop index="icounter" from="1" to="#iColumnsToPad#">
				<td>&nbsp;</td>
			</cfloop>
		</cfsavecontent>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	</tbody>
	</table>


	<table id="parcel-history-data" class="table">
	<tbody>
	<!--- <tr>
		<td><span class="formatted-data-label">Parcel Number</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.parcelDisplayID#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr> --->

	<tr>
		<td><span class="formatted-data-label">Use Code</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.useCode#<br/>#prc.getHistoryInfo.useCodeDescription#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Neighborhood Code</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.neighborhoodCode#<br/>#prc.getHistoryInfo.neighborhoodCodeDescription#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Tax District Code</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.taxDistrictCode#<br/>#prc.getHistoryInfo.TaxCodeDistrictDescription#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Situs Address</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right">
				<span class="formatted-data-values">
					#prc.getHistoryInfo.SitusHousePrefix# #prc.getHistoryInfo.SitusHouseNumber# #prc.getHistoryInfo.SitusStreetName#
					#prc.getHistoryInfo.SitusHouseSuffix# #prc.getHistoryInfo.SitusStreetType# #prc.getHistoryInfo.SitusStreetDirection#<br/>
					#prc.getHistoryInfo.situsCity# #prc.getHistoryInfo.situsState#
				</span>
			</td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Owner Name</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.ownerName#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	<tr>
		<td><span class="formatted-data-label">E&amp;I Number</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.eiNumber#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">Appraised Value - <cfoutput>#prc.strLabel#</cfoutput></span></td>
	</tr>
	<tr>
		<td><span class="formatted-data-label">AG Land Market</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.agricultureLandValueMarket)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Income</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.appraisedIncome)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.appraisedLandNonAgriculture)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land - AG</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.agricultureValueClassified)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Building</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.appraisedBuildingValue)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Extra Features</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.appraisedFeaturesValue)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label"> Total</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.appraisedTotal)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">Market Value - <cfoutput>#prc.strLabel#</cfoutput></span></td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Total</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.totalJustValue)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">County Assessed Value - <cfoutput>#prc.strLabel#</cfoutput></span></td>
	</tr>


	<tr>
		<td><span class="formatted-data-label">Income</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.incomeAssessedValueCounty)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.landAssessedValueCounty)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land - AG</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.agricultureAssessedValueCounty)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Building</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.buildingAssessedValueCounty)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Extra Features</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.featureAssessedValueCounty)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Total</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.totalAssessedValueCounty)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">School Assessed Value - <cfoutput>#prc.strLabel#</cfoutput></span></td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Income</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.IncomeAssessedValueSchool)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.LandAssessedValueSchool)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land - AG</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.agricultureAssessedValueSchool)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Building</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.BuildingAssessedValueSchool)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Extra Features</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.FeatureAssessedValueSchool)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Total</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.totalAssessedValueSchool)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">Other Assessed Value - <cfoutput>#prc.strLabel#</cfoutput></span></td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Income</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.incomeAssessedValueOther)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.landAssessedValueOther)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land - AG</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.agricultureAssessedValueOther)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Building</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.buildingAssessedValueOther)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Extra Features</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.featureAssessedValueOther)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Total</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.totalAssessedValueOther)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">City Assessed Value - <cfoutput>#prc.strLabel#</cfoutput></span></td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Income</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.incomeAssessedValueCity)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.landAssessedValueCity)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Land - AG</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.agricultureAssessedValueCity)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Building</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.buildingAssessedValueCity)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Extra Features</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.featureAssessedValueCity)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Total</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.totalAssessedValueCity)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">Differential</span></td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">SOH 3%</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.diffAmt)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">AGL 10%</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.agricultureDifferential)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">Exemptions</span></td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Exemption(s)</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#prc.getHistoryInfo.Exemptions#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">County</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.CountyExemptionTotal)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">School</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.SchoolExemptionTotal)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Other</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.OtherExemptionTotal)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">City</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.CityExemptionTotal)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">Taxable Values</span></td>
	</tr>

	<tr>
		<td><span class="formatted-data-label">County</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.CountyTaxableValue)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">School</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.SchoolTaxableValue)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Other</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.OtherTaxableValue)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">City</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<td class="text-right"><span class="formatted-data-values">#DollarFormat(prc.getHistoryInfo.CityTaxableValue)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5"><span class="formatted-data-values">Taxes</span></td>
	</tr>


	<tr>
		<td><span class="formatted-data-label">Total Millage Rate</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<cfset strTooltip = createMillageRollover( prc.getMillageRates[prc.getHistoryInfo.taxYear] )>
			<td class="text-right"><span class="formatted-data-values"data-toggle="tooltip" data-placement="top" title="#strTooltip#">#columnTotal("prc.getMillageRates[prc.getHistoryInfo.taxYear].millageFinal")#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>

	<tr>
		<td><span class="formatted-data-label">Taxes</span></td>
		<cfoutput query="prc.getHistoryInfo" startrow="#rc.startRow#" maxrows="#prc.iYearsToDisplay#">
			<cfset strTooltip = createTaxesRollover( prc.getTaxRates[prc.getHistoryInfo.taxYear] )>
			<td class="text-right"><span class="formatted-data-values" data-toggle="tooltip" data-placement="top" title="#strTooltip#">#DollarFormat(prc.getHistoryInfo.Taxes)#</span></td>
		</cfoutput>
		<cfoutput>#strPadding#</cfoutput>
	</tr>
	</tbody>
	</table>
</cfoutput>