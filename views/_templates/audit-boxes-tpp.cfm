

<!--- temp solution until date formats from db normalized --->

<cffunction name="fixDate">
	<cfargument name="strInDate">


	<cfif isDate(TRIM(ARGUMENTS.strInDate))>
		<cfset strByDate = DateFormat(ARGUMENTS.strInDate,"mm/dd/yyyy")>
	<cfelseif LEN(TRIM(ARGUMENTS.strInDate)) EQ 8>
		<cftry>
			<CFSET strByDate = TRIM(ARGUMENTS.strInDate)>
			<cfset strByDate = insert("/", strByDate, 6) />
			<cfset strByDate = insert("/", strByDate, 4) />
			<cfset strByDate = DateFormat(strByDate,"mm/dd/yyyy")>
			<cfcatch><cfset strByDate = "" /></cfcatch>
		</cftry>
	<CFELSE>
		<cfset strByDate =  "" />
	</cfif>

	<cfreturn TRIM(strByDate)>
</cffunction>

<div class="container-border container-spacer container-border-no-top-spacer">
	<div class="section-divider"  id="appraisal-container">
		<cfoutput>
		<table id="audit-box" class="table table-condensed">
			<TR>
				<TD>
					<span class="formatted-data-label">Return Date:</span>
					<input type="text" value="#fixDate(prc.qGetPersistentInfoTPP.UserYMD1)#" class="form-control tpp-125 datefield formatted-data-values">
				</TD>

				<TD>
					<span class="formatted-data-label">Extension Date:</span>
					<input type="text" value="#fixDate(prc.qGetPersistentInfoTPP.UserYMD2)#" class="form-control tpp-125 datefield formatted-data-values">
				</TD>
				<TD>
					<span class="formatted-data-label">Worked Date:</span>
					<input type="text"  value="#fixDate(prc.qGetPersistentInfoTPP.UserYMD3)#" class="form-control tpp-125 datefield formatted-data-values">
				</TD>
			</TR>
		</TABLE>
		</cfoutput>
	</div>
</div>