<cfoutput>
<cfparam name="msgCode" default="">
<cfparam name="rc.responseMsg" default="">
<cfparam name="prc.stResults.status" default="">

<cfif isdefined("prc.stResults.status")>
	<cfset msgCode = prc.stResults.status />
<cfelseif isdefined("rc.responseMsg")>
	<cfset msgCode = rc.responseMsg />
</cfif>

<cfswitch expression="#msgCode#">
	<cfcase value="200">
		<div class="alert alert-success alert-dismissable fade in">
			<a href="##" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Your changes have been saved.</strong>
		</div>
	</cfcase>
	<cfcase value="-10">
		<div class="alert alert-danger alert-dismissable fade in">
			<a href="##" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>An error has occurred</strong>
			<cfif isdefined("prc") and StructKeyExists(prc.stResults,"message")>
				<p>#prc.stResults.message#</p>
			</cfif>
		</div>
	</cfcase>
</cfswitch>

</cfoutput>