<!---
<CFIF NOT IsDefined("qryAppraisalRecords")>
	<cfstoredproc procedure="P_getAppraisalInfo" datasource="#client.dsn#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ParcelID#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#taxYear#" null="No">
		<cfprocresult name="qryAppraisalRecords">
	</cfstoredproc>
</CFIF>
--->
<!--- temp solution until date formats from db normalized --->
<cfif tab eq "building">
	<cfset lastAppraisedBy = "" />
	<cfset plckBy = "" />
	<cfset totSQFt = "" />
	<cfset plAcres = "" />
	<cfset strByDate = "" />
	<cfset strChkDate = "" />
<cfelse>
	<cfset lastAppraisedBy = prc.qryAppraisalRecords.LastAppraisedBy />
	<cfset plckBy = prc.qryAppraisalRecords.plckby />
	<cfset totSQFt = prc.qryAppraisalRecords.totsqft />
	<cfset plAcres = prc.qryAppraisalRecords.placres />
	<cfset strByDate = fixDbDate(prc.qryAppraisalRecords.LastAppraisedDate)>
	<cfset strChkDate = fixDbDate(prc.qryAppraisalRecords.plckdt)>
</cfif>


<div class="container-border container-spacer container-border-no-top-spacer">
	<div class="section-divider"  id="appraisal-container">
		<cfoutput>
		<table id="audit-box" class="table table-condensed">
			<TR>
				<TD>
					<span class="formatted-data-label">Appr By:</span>
					<input type="text" name="palapby" value="#lastAppraisedBy#" class="form-control land-80 formatted-data-values">
				</TD>
				<TD>
					<input type="text" name="palapdt" value="#dateformat(strByDate, 'mm/dd/yyyy')#" class="form-control datefield land-105 formatted-data-values">
				</TD>
				<TD>
					<span class="formatted-data-label">Checked By:</span>
					<input type="text" name="plckby" value="#plckBy#" class="form-control land-80 formatted-data-values">
				</TD>
				<TD>
					<input type="text" name="plckdt"  value="#dateformat(strChkDate, 'mm/dd/yyyy')#" class="form-control datefield land-105 formatted-data-values">
				</TD>
				<CFIF tab EQ "land">
					<TD>
						<span class="formatted-data-label">Sq Ft:</span>
						<input type="text" name="totsqft" id="land-total-sq-feet" readonly value="#NumberFormat(totSQFt,',')#" class="form-control land-120 formatted-data-values">
					</TD>
					<TD style="padding-bottom:5px">
						<span class="formatted-data-label">Acres:</span>
						<input type="text" name="placres" id="land-total-acres" readonly value="#NumberFormat(plAcres,',.99')#" class="form-control land-80 formatted-data-values">
					</TD>
				<CFELSE>
					<td></td>
					<td></td>
				</CFIF>
			</TR>
		</TABLE>
		</cfoutput>
	</div>
</div>