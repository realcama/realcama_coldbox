<cfoutput>
	<div class="navbar navbar-fixed-bottom siteFooter">
		<div id="siteFooter" class="container">
			<div class="row">
				<div class="col-md-6">
					Real<strong>CAMA</strong> - &copy; Copyright 2009-#year(now())# - All Rights Reserved
				</div>
				<div class="col-md-6 pull-right text-right">
					<a href="##">Privacy Policy</a> |
					<a href="##">Terms &amp; Conditions</a>
				</div>
			</div>
		</div>
	</div>

	<!---
	 __  __  ____  _____          _       _____            _   _ _____    _____        _____ ______   _____ _   _  _____ _     _    _ _____  ______  _____
	|  \/  |/ __ \|  __ \   /\   | |     / ____|     /\   | \ | |  __ \  |  __ \ /\   / ____|  ____| |_   _| \ | |/ ____| |   | |  | |  __ \|  ____|/ ____|
	| \  / | |  | | |  | | /  \  | |    | (___      /  \  |  \| | |  | | | |__) /  \ | |  __| |__      | | |  \| | |    | |   | |  | | |  | | |__  | (___
	| |\/| | |  | | |  | |/ /\ \ | |     \___ \    / /\ \ | . ` | |  | | |  ___/ /\ \| | |_ |  __|     | | | . ` | |    | |   | |  | | |  | |  __|  \___ \
	| |  | | |__| | |__| / ____ \| |____ ____) |  / ____ \| |\  | |__| | | |  / ____ \ |__| | |____   _| |_| |\  | |____| |___| |__| | |__| | |____ ____) |
	|_|  |_|\____/|_____/_/    \_\______|_____/  /_/    \_\_| \_|_____/  |_| /_/    \_\_____|______| |_____|_| \_|\_____|______\____/|_____/|______|_____/

	 --->

	<script type="text/javascript">
		function toggleChevron(e) {
			$(e).toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
		}
		function toggleChevronChild(e) {
			$("##chevron_" + e).toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
		}

		function editThisRow(iRow) {
			$("##edit_mode_item_" + iRow).toggle();
			$("##view_mode_item_" + iRow).toggle();
		}
		function editThisParcelSalesRow(iRow) {
			$("##edit_mode_item_parcel_" + iRow).toggle();
			$("##view_mode_item_parcel_" + iRow).toggle();
		}
		function editThisCondoMasterUnitsRow(iRow) {
			$("##view_mode_item_unit_" + iRow).toggle();
			$("##edit_mode_item_unit_" + iRow).toggle();
		}
		function editThisExemptionRow() {
			$("##edit_mode_exemptions").toggle();
			$("##view_mode_exemptions").toggle();
		}

		function expandAll() {
			$('.panel-collapse').collapse('show');
			$(".block-expander").removeClass('glyphicon-chevron-down');
			$(".block-expander").addClass('glyphicon-chevron-up');
			$('##collapseAll').show();
			$('##expandAll').hide();
		}
		function collapseAll() {
			$('.panel-collapse').collapse('hide');
			$(".block-expander").removeClass('glyphicon-chevron-up');
			$(".block-expander").addClass('glyphicon-chevron-down');
			$('##expandAll').show();
			$('##collapseAll').hide();
		}
		function checkForEntry() {
			if($("##owner_name").val() != "") {
				$("##requestacallform").submit();
			} else {
				alert("Please pick an owner to continue");
			}
		}

		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip({html: true});
			$("[rel='tooltip']").tooltip({ delay: { show: 500, hide: 100 } });
			$(".numeric").numeric();
			$(".datefield").datepicker();

			$('##search-header-y').select2({
				minimumResultsForSearch: -1,
				formatResult: formatHeaderYear,
				formatSelection: formatHeaderYear,
				escapeMarkup: function(m) { return m; }
			});

			$('##search-header-parcel-number').keypress(function (event){

				if(event.keyCode == 13){
				   $("##frmHeader").submit();
				}
			})

		    $("##more-TPP-links-anchor").fancybox({
				'hideOnContentClick': true,
				'minWidth': 200,
				'title': "Additional TPP Accounts"
			});

		});

		function formatHeaderYear(state) {
			var originalOption = state.element;
			return "<span class='formatted-data-values'>" + $(originalOption).data('val') + "</span>";
		}
	</script>


	<div class="modal fade bs-example-modal-sm" id="myComingSoon" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	    <div class="vertical-alignment-helper">
		    <div class="modal-dialog modal-sm vertical-align-center">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h4 class="modal-title">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						    <span class="glyphicon glyphicon-alert"></span> Alert!
		                 </h4>
		            </div>
		            <div class="modal-body">
		            	The selected feature is currently under development and will be available shortly.
		            </div>
					<div class="modal-footer">
						<div align="center"><button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Dismiss this Message</button>	</div>
					</div>
		        </div>
		    </div>
		</div>
	</div>


	<div class="modal bs-example-modal-sm" id="myPleaseWait" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	    <div class="vertical-alignment-helper">
		    <div class="modal-dialog modal-sm vertical-align-center">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h4 class="modal-title">
		                    <span class="glyphicon glyphicon-time"></span> Please Wait
		                 </h4>
		            </div>
		            <div class="modal-body">
		                <div class="progress">
		                    <div class="progress-bar progress-bar-info progress-bar-striped active" style="width: 100%"></div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<CFIF IsDefined("prc.qryPersistentInfo")>
		<!--- The persistent doesn't exist on the search pages --->
		<div class="modal fade bannerformmodal" tabindex="-1" role="dialog" aria-labelledby="bannerformmodal" aria-hidden="true" id="bannerformmodal">
			<div class="vertical-alignment-helper">
				<div class="modal-dialog modal-md vertical-align-center">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Select Exemption Homeowner</h4>
						</div>
						<form action="#event.buildLink('homesteadExemptform/index')#" method="post" name="requestacallform" id="requestacallform">
							<div class="modal-body">
								<input type="hidden" name="ParcelID" value="#prc.qryPersistentInfo.ParcelID#">
								<input type="hidden" name="TaxYear" value="#prc.qryPersistentInfo.TaxYear#">
								<select name="owner_name" id="owner_name" class="form-control">
									<option value="">Choose Owner for Exemption </option>
									<option value="#prc.qryPersistentInfo.OwnerName#">#prc.qryPersistentInfo.OwnerName#</option>
									<option value="#prc.qryPersistentInfo.Ownername2#">#prc.qryPersistentInfo.Ownername2#</option>
								</select>
								<br/>
								<br/>
								<span class="formatted-data-label"><input type="checkbox" name="transferchk" id="transferchk" value="1"> &nbsp;&nbsp;Include HX transfer</span>
							</div>
							<div class="modal-footer">
								<input type="button" value="Continue" class="btn btn-default" onClick="checkForEntry()">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</CFIF>
	<cfswitch expression="#prc.pageTitle#">
		<cfcase value="RealCAMA Search">
			<script src="/includes/js/search.js"></script>

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				 <form class="form-inline" role="form" name="frmNuke" action="#event.buildLink('search/deleteSearch')#" method="post">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
					   <!---  <button type="button" class="glyphicon glyphicon-remove pull-right" data-dismiss="modal" aria-hidden="true"></button> --->
				        <h4 class="modal-title" id="myModalLabel">Delete Search</h4>
				      </div>
				      <div class="modal-body">
						<div align="center">Are you sure you wish to delete this saved search?</div>
						<input type="hidden" name="searchToNuke" id="searchToNuke">

				      </div>
				      <div class="modal-footer">
				       	<button type="button"  class="btn btn-default" onclick="frmNuke.submit();">Yes</button>
						<button type="button"  class="btn btn-default" data-dismiss="modal">No</button>
				      </div>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				  </form>
			</div><!-- /.modal -->

			<!-- Saved searches Delete Modal -->
			<div class="modal fade" id="nukeFavorite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				 <form class="form-inline" role="form" name="frmNukeFav" action="#event.buildLink('search/deleteFavorite')#" method="post">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <!--- <button type="button" class="glyphicon glyphicon-remove pull-right" data-dismiss="modal" aria-hidden="true"></button> --->
				        <h4 class="modal-title" id="myModalLabel">Delete Favorite</h4>
				      </div>
				      <div class="modal-body">
						<div align="center">Are you sure you wish to delete this saved favorite?</div>
						<input type="hidden" name="favoriteToNuke" id="favoriteToNuke">

				      </div>
				      <div class="modal-footer">
				       	<button type="button"  class="btn btn-default" onclick="frmNukeFav.submit();">Yes</button>
						<button type="button"  class="btn btn-default" data-dismiss="modal">No</button>
				      </div>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				  </form>
			</div><!-- /.modal -->
		</cfcase>

		<!--- <cfcase value="RealCAMA Parcel">
			<script src="/includes/js/parcel.js"></script>
			<script src="/includes/js/formatSelect.js"></script>

			<script>
				/* Static values at this point */
				var ParcelID = "#rc.parcelID#";
				var taxYear = "#rc.taxYear#"
				<!--- var la_id = "#qryRecords.la_id#"; --->


				<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.qryParcelInfo#"
					TOPLEVELVARIABLE="parcelData">

				<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.qrySitus#"
					TOPLEVELVARIABLE="situsData">

				<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.qryParcelSales#"
					TOPLEVELVARIABLE="salesData">

				<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.qryParcelMultiSales#"
					TOPLEVELVARIABLE="multiSalesData">

				<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.qryParcelLegalInfo#"
					TOPLEVELVARIABLE="legalDescriptionData">

				<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.qryParcelExemptionInfo#"
					TOPLEVELVARIABLE="exemptionData">

				<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.qryParcelExemptionApplicationInfo#"
					TOPLEVELVARIABLE="exemptionApplicationData">
				</script>


		</cfcase> --->
	</cfswitch>

</cfoutput>
