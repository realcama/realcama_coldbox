<cfoutput>
	<!--- <cfif IsDefined("CLIENT.userID") AND bLoginPage EQ FALSE> --->
		<div class="navbar navbar-fixed-top" ><!--- higher z-index than modal window used on some page (1050) --->
			<!--- SITE HEADER --->
			<div id="siteHeader">
				<div class="container">
					Real<strong>CAMA</strong> - <!--- #client.vendorName# ---> Property Appraiser
					<div class="pull-right">
						<a href="##"><i class="glyphicon glyphicon-user glyphicon-white"></i> <!--- #CLIENT.firstname# #CLIENT.lastname# ---></a>
						<a href="/login.cfm?logout=1"><i class="glyphicon glyphicon-off glyphicon-white"></i> Logout</a>
					</div>
				</div>
			</div>
			<nav class="navbar navbar-default siteHeader" role="navigation">
			  <div class="container">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="##bs-example-navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			    </div>

				<CFSET strURLLocation = "">
				<CFSET bAllowPropertyLinks = TRUE>
				<CFSET bAllowTPPLinks = TRUE>

				<CFIF Isdefined("rc.ParcelID") AND LEN(TRIM(rc.ParcelID)) NEQ 0>
					<CFSET rc.ParcelID = URLDecode(rc.ParcelID)> <!--- Remove the %20's from teh value since some prop ID's have spaces in them --->
					<CFSET strURLLocation = strURLLocation & "/parcelID/#rc.ParcelID#">
				<CFELSE>
					<CFSET bAllowPropertyLinks = FALSE>
				</CFIF>
				<CFIF Isdefined("rc.taxYear")>
					<CFSET strURLLocation = strURLLocation & "/taxYear/#rc.taxYear#">
				</CFIF>
				<CFIF Isdefined("rc.tpp")>
					<CFSET strURLLocation = strURLLocation & "/tpp/#rc.tpp#">
				<CFELSE>
					<CFSET bAllowTPPLinks = FALSE>
				</CFIF>


			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li <CFIF prc.pageTitle EQ "Real Property">class="active"</CFIF>>
						<CFIF NOT bAllowPropertyLinks>
							<a href="##" class="dropdown-toggle not-valid-to-click"><img src="/includes/images/icon-realprop.png"> Real Property</a>
						<CFELSE>
							<a href="##" class="dropdown-toggle" data-toggle="dropdown" id="drop1">
								<img src="/includes/images/icon-realprop.png"> Real Property
							</a>
							<ul class="dropdown-menu">
								<li><a href="#event.buildLink('land/index')##strURLLocation#">Land</a></li>
								<li><a href="#event.buildLink('building/index')##strURLLocation#">Building</a></li>
								<li><a href="#event.buildLink('permits/index')##strURLLocation#">Permits</a></li>
								<li><a href="#event.buildLink('features/index')##strURLLocation#">Extra Features</a></li>
								<li><a href="#event.buildLink('parcel/index')##strURLLocation#">Parcel</a></li>
								<li><a href="#event.buildLink('correlation/index')##strURLLocation#">Correlation</a></li>
								<li><a href="#event.buildLink('condomaster/index')##strURLLocation#">Condo Master</a></li>
								<li><a href="#event.buildLink('condounit/index')##strURLLocation#">Condo Unit</a></li>
								<li><a href="#event.buildLink('history/index')##strURLLocation#">History</a></li>
								<li><a href="#event.buildLink('summary/index')##strURLLocation#">Summary</a></li>
								<li><a href="#event.buildLink('compare/index')##strURLLocation#">Compare</a></li>
							</ul>
						</CFIF>
					</li>

			        <li <CFIF prc.pageTitle EQ "Tangible">class="active"</CFIF>>
						<CFIF NOT bAllowTPPLinks>
							<a href="##" class="dropdown-toggle not-valid-to-click" id="drop2"><img src="/includes/images/icon-tangible.png"> Tangible</a>
						<CFELSE>
							<a href="##" class="dropdown-toggle" data-toggle="dropdown" id="drop2">
								<img src="/includes/images/icon-tangible.png"> Tangible
							</a>
							<ul class="dropdown-menu">
								<li data-url="manageusers"><a tabindex="-1" href="#event.buildLink('tangible/owner')##strURLLocation#" class="tpp-owner">Owner/DBA</a></li>
								<li data-url="managevendors"><a tabindex="-1" href="#event.buildLink('tangible/assets')##strURLLocation#" class="tpp-assets">Assets Listing</a></li>
							</ul>
						</CFIF>
					</li>
					<li <CFIF prc.pageTitle EQ "Reports">class="active"</CFIF>>
						<CFIF NOT bAllowPropertyLinks>
							<a href="##" class="dropdown-toggle not-valid-to-click"><img src="/includes/images/icon-reports.png"> Reports</a>
						<CFELSE>
							<a href="##" class="dropdown-toggle" data-toggle="dropdown">
								<img src="/includes/images/icon-reports.png"> Reports
							</a>
							<ul class="dropdown-menu multi-level">
								<li class="dropdown-submenu">
									<a tabindex="-1" href="##">Appraisal Report</a>
			          				<ul class="dropdown-menu">
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Appeals Tracking Report</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Appraisal Card Print</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Building Reporting</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Building Permit Report</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Capping Reports</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Custom Reporting</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Extra Features Reporting</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Land Reporting</a></li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##">Mass Maintenance</a>
											<ul class="dropdown-menu">
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Building</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Land</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Parcel </a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Extra Features</a></li>
											</ul>
										</li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Options Report</a></li>
									</ul>
								</li>
								<li class="dropdown-submenu">
									<a tabindex="-1" href="##">Assessment Report</a>
									<ul class="dropdown-menu">
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Assessment Reporting</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Custom Reporting</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Exception Reporting</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Exception Reporting (no Building Report)</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Tax District Report</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Top Ten Taxpayers</a></li>
									</ul>
								</li>
								<li class="dropdown-submenu">
									<a tabindex="-1" href="##">Recalculations</a>
									<ul class="dropdown-menu">
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Assessment Recalculation</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Capping Recalculation</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Condo Unit Recalculation</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Recalculate Buildings</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Recalculate Features</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Recalculate Land</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Recalculate All</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Time adjustment Recalculation</a></li>
									</ul>
								</li>

								<li class="dropdown-submenu">
									<a tabindex="-1" href="##">Exemption Processing</a>
									<ul class="dropdown-menu">
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Renewal Flag & Exemption Removal</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Clean N Ag Renewal Flag</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Update Retain Capping Flags</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Print Renewal Notices</a></li>
									</ul>
								</li>
								<li class="dropdown-submenu">
									<a tabindex="-1" href="##">Letter Processing</a>
									<ul class="dropdown-menu">
										<li><a tabindex="-1" href="##" onclick="comingSoon()">New Owner Letter</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Income Survey Letter</a></li>
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Senior Letter</a></li>
									</ul>
								</li>

								<li class="dropdown-submenu">
									<a tabindex="-1" href="##">Analysis Reporting</a>
									<ul class="dropdown-menu">
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Sales Analysis</a></li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##">Comparable Sales Analysis</a>
											<ul class="dropdown-menu">
												<li><a id="popCompare3" href="/analysis_report.cfm">Report</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Update Files</a></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##" onclick="comingSoon()">Residual Analysis</a>
											<ul class="dropdown-menu">
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Land</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Building</a></li>
											</ul>
										</li>
									</ul>
								</li>

								<li class="dropdown-submenu">
									<a tabindex="-1" href="##">Tax Roll Procesing</a>
									<ul class="dropdown-menu">
										<li><a tabindex="-1" href="##" onclick="comingSoon()">Millage File Maintenance</a></li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##">Create Tax Roll</a>
											<ul class="dropdown-menu">
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Preliminary Tax Roll</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Final Tax Roll</a></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##">DOR File Processing</a>
											<ul class="dropdown-menu">
												<li><a tabindex="-1" href="##" onclick="comingSoon()">NAL File w/ Balancing Totals</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">NAP File w/ Balancing Totals</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">SDF File</a></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##">Create Certification Forms</a>
											<ul class="dropdown-menu">
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Preliminary Recaps & Totals</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Final Recaps and Totals</a></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##">TRIM Processing</a>
											<ul class="dropdown-menu">
												<li><a tabindex="-1" href="##" onclick="comingSoon()">TRIM Setup Page</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Create Sample TRIM File</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Create Full Set TRIM File</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">TRIM Report</a></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<a tabindex="-1" href="##">VAB Packet Processing</a>
											<ul class="dropdown-menu">
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Letter to Petitioner</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Comparable Sales Report</a></li>
												<li><a tabindex="-1" href="##" onclick="comingSoon()">Property Appraiser Info to Petitioner</a></li>
											</ul>
										</li>
									</ul>
								</li>

							</ul>
						</CFIF>
					</li>


					<li <CFIF prc.pageTitle EQ "System Config">class="active"</CFIF>>
						<a href="##" class="dropdown-toggle" data-toggle="dropdown" id="drop7">
							<img src="/includes/images/icon-systemconfig.png"> System Config
						</a>
						<ul class="dropdown-menu">
							<li><a tabindex="-1" href="/maint/vendorList.cfm">Vendors</a></li>
							<li class="dropdown-submenu">
								<a tabindex="-1" href="##">System Setup</a>
								<ul class="dropdown-menu">
									<li data-url=""><a tabindex="-1" href="##" onclick="comingSoon()">Parcel Number Format</a></li>
									<li data-url=""><a tabindex="-1" href="##" onclick="comingSoon()">System Defaults</a></li>
									<li data-url=""><a tabindex="-1" href="##" onclick="comingSoon()">User Field Names</a></li>
								</ul>
							</li>
							<li class="dropdown-submenu">
								<a tabindex="-1" href="##">Interfaces</a>
								<ul class="dropdown-menu">
									<li data-url=""><a tabindex="-1" href="##" onclick="comingSoon()">Marshall/Swift</a></li>
									<li data-url=""><a tabindex="-1" href="##" onclick="comingSoon()">Apex</a></li>
								</ul>
							</li>
							<li><a tabindex="-1" href="##" onclick="comingSoon()">User Roles and Rights</a></li>
							<li><a tabindex="-1" href="##" onclick="comingSoon()">Table Maintenance</a></li>
							<li><a tabindex="-1" href="##" onclick="comingSoon()">Sample File Build</a></li>
							<li><a tabindex="-1" href="#event.buildLink('document_admin')#">Document Maintenance</a></li>
							<li><a tabindex="-1" href="#event.buildLink('document_admin/documentReset')#">Document Reset</a></li>
						</ul>
					</li>


					<li <CFIF prc.pageTitle EQ "Search">class="active"</CFIF>>
						<a href="#event.buildLink('search')#" id="drop9">
							<img src="/includes/images/icon-assessment.png"> Search
						</a>
					</li>

			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>

		</div>	<!--- END div class="navbar navbar-fixed-top" style="z-index:2000;" --->
	<!--- </cfif> --->
</cfoutput>
