<cfparam name="rc.page" default="1">
<cfscript>
	p = getInstance( 'PagingService@cbpagination' );
</cfscript>
<span class="pull-right text-small">
	<cfswitch expression="#showTabOptions#">
		<cfcase value="land">
			<!---
			<span data-mode="view" data-type="land" class="dsp-mode link" id="editmode"  style="display:none;"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_view.png"></span></span>
			<span data-mode="edit" data-type="land" class="dsp-mode link" id="viewmode"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_edit.png"></span></span>
			--->
			<cfscript>
				link = "";
			</cfscript>
			<div id="land-line-options">

				<span class="btn-group land-add-lines-drop-options">
					<a class="link dropdown-toggle" data-toggle="dropdown"><img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"></a>
					<ul class="dropdown-menu">
						<cfif prc.qryGetLandInfo.RecordCount neq 0>
							<cfoutput><li><a onclick="addBlankRow('#rc.taxYear#','#rc.parcelID#');"><i class="icon glyphicon glyphicon-file"></i> Blank Land Template</a></li></cfoutput>
							<li><a href="##" data-mode="cloneLine" data-toggle="modal" data-target="#modalCloneLine" data-type="land" class="dsp-mode link" id="cloneLine"><i class="icon glyphicon glyphicon-copy"></i> Clone A Land Line</a></li>
						</cfif>
					</ul>
				</span>
				<!---
				<span data-mode="compare" data-type="land" class="dsp-mode link" id="compareLandPop"><img src="/includes/images/v2/realCAMA_icon_compare.png"></span>
				<span data-mode="imageUpload" data-type="land" class="dsp-mode link" id="imageUploadPop"><img src="/includes/images/v2/realCAMA_icon_imageUpload.png"></span>
				<span data-mode="save" data-toggle="modal" data-target="#modalSave" data-type="land" class="dsp-mode link" id="save"> <img src="/includes/images/v2/realCAMA_icon_saveRecord.png"></span>
				<span data-mode="revert" data-toggle="modal" data-target="#modalRevert" data-type="land" class="dsp-mode link" id="revert"> <img src="/includes/images/v2/realCAMA_icon_revert.png"></span>
				<button type="button" data-toggle="modal" data-target="#myModal" class="btn-custom"><img src="/includes/images/v2/realCAMA_icon_revert.png"></button>
				--->
			</div>

			<cfoutput>
			<div id="pagination-div">
				<span class="btn-group">#p.refresh_div_renderit( prc.qryGetLandInfo.RecordCount, link, rc.page, rc.page_size)#</span>
			</div>
			</cfoutput>
		 	<!--- <div id="land-line-pager">
				<CFIF prc.qryGetLandInfo.RecordCount GT prc.page_size>
					<cfoutput>
					<span class="btn-group">
						<a class="btn btn-default btn-inverse btn-sm disabled" href="##" id="building-page-prev" onclick="pageIncrement(-1);"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></a>
						<a class="btn btn-default btn-inverse btn-sm disabled text-sm" href="##">Page <span id="current-page">1</span> of #iTotalPages#</a>
						<a class="btn btn-default btn-inverse btn-sm" href="##" id="building-page-next" onclick="pageIncrement(1);"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></a>
					</span>
					</cfoutput>
				</CFIF>
			</div> --->

			<div id="land-line-expander">
				<span data-mode="expandAll" class="dsp-mode link" data-toggle="collapse" id="expandAll" onclick="expandAll()"> <img src="/includes/images/v2/realCAMA_icon_expandAll.png" alt="" border="0"></span>
				<span data-mode="collapseAll" class="dsp-mode link" data-toggle="collapse" id="collapseAll" onclick="collapseAll()" style="display:none;"> <img src="/includes//images/v2/realCAMA_icon_collapseAll.png" alt="" border="0"></span>
			</div>

		</cfcase>

		<cfcase value="Building">
			<cfscript>
				link = "";
			</cfscript>
			<div id="building-line-options">
				<!---
				<span data-mode="view" data-type="building" class="dsp-mode link" id="editmode"  style="display:none;"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_view.png"></span></span>
				<span data-mode="edit" data-type="building" class="dsp-mode link" id="viewmode"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_edit.png"></span></span>
				<span data-mode="save" data-type="building" class="dsp-mode link" id="save"> <a href="index.cfm?s=b"><img src="/includes/images/v2/realCAMA_icon_saveRecord.png"></a></span>
				<span data-mode="revert" data-toggle="modal" data-target="#modalRevert" data-type="building" class="dsp-mode link" id="revert"> <img src="/includes/images/v2/realCAMA_icon_revert.png"></span>
				<a href="##"onclick="addBlankRow();"><img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"></a>
				<span data-mode="compare" data-type="building" class="dsp-mode link" id="compare"> <img src="/includes/images/v2/realCAMA_icon_compare.png"></span>
				 --->
				 <a href="##" onclick="popAddBlankRow();"><img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"></a>
			 </div>
			<cfoutput>
			<div id="building-line-pager">
				<span class="btn-group">#p.refresh_div_renderit( prc.getBuldingLineInfo.RecordCount, link, rc.page, rc.page_size)#</span>
			</div>
			</cfoutput>
			<div id="building-line-expander">
				<span data-mode="expandAll" class="dsp-mode link" data-toggle="collapse" id="expandAll" onclick="expandAll()"> <img src="/includes/images/v2/realCAMA_icon_expandAll.png" alt="" border="0"></span>
				<span data-mode="collapseAll" class="dsp-mode link" data-toggle="collapse" id="collapseAll" onclick="collapseAll()" style="display:none;"> <img src="/includes/images/v2/realCAMA_icon_collapseAll.png" alt="" border="0"></span>

			</div>
		</cfcase>

		<cfcase value="permits">
			<cfoutput>
			<!---
			<span data-mode="view" data-type="permits" class="dsp-mode link" id="editmode"  style="display:none;"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_view.png"></span></span>
			<span data-mode="edit" data-type="permits" class="dsp-mode link" id="viewmode"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_edit.png"></span></span>
			<span data-mode="save" data-type="permits" class="dsp-mode link" id="save"><img src="/includes/images/v2/realCAMA_icon_saveRecord.png"></span>
			<span data-mode="revert" data-toggle="modal" data-target="##modalRevert" data-type="permits" class="dsp-mode link" id="revert"> <img src="/includes/images/v2/realCAMA_icon_revert.png"></span>
			--->

			<cfscript>
				link = "";
			</cfscript>

			<div id="permits-line-options">
			<!---
				<cfif (structKeyExists(rc, "status") and rc.status EQ 2)>
					<span data-mode="view-active" data-type="permits" class="dsp-mode link" id="view-active" onclick="javascript:location.href='#event.buildLink('permits/index')##strQueryString#/'" rel="tooltip" title="Show Active Records">Show Active</span>
				<cfelse>
				   <span data-mode="view-deleted" data-type="permits" class="dsp-mode link" id="view-deleted" onclick="javascript:location.href='#event.buildLink('permits/index')##strQueryString#/status/2'" rel="tooltip" title="View Deleted Records">Show Deleted</span>
				</cfif>
				<span data-mode="permit-reseq" data-type="permits" class="dsp-mode link" id="permit-reseq" onclick="doPermitResequencePopup('#prc.viewStatus#', '#prc.stPersistentInfo.parcelID#', '#prc.parcelType#', '#prc.stPersistentInfo.taxYear#')" rel="tooltip" title="Click to Resequence Permit Items">Resequence</span>
			--->
				<cfif (structKeyExists(rc, "status") and rc.status EQ 2)>
					<!---<span data-mode="view-active" data-type="permits" class="dsp-mode link" id="view-active" onclick="javascript:location.href='#event.buildLink('permits/index')##strQueryString#/'" rel="tooltip" title="Show Active Records">Show Active</span> --->
					<span data-mode="view" data-type="permits" class="dsp-mode link" id="active-permits" onclick="javascript:location.href='#event.buildLink('permits/index')##strQueryString#/'" rel="tooltip" title="Show Active Records"> <span id="editview"><img src="/includes/images/v2/showActive.png"></span></span>
				<cfelse>
					<!---<span data-mode="view-deleted" data-type="permits" class="dsp-mode link" id="view-deleted" onclick="javascript:location.href='#event.buildLink('permits/index')##strQueryString#/status/2'" rel="tooltip" title="View Deleted Records">Show Deleted</span> --->
					<span data-mode="view" data-type="permits" class="dsp-mode link" id="deleted-permits" onclick="javascript:location.href='#event.buildLink('permits/index')##strQueryString#/status/2'" rel="tooltip" title="View Deleted Records"> <span id="editview"><img src="/includes/images/v2/showDeleted.png"></span></span>
				</cfif>

				<span data-mode="view" data-type="permits" class="dsp-mode link" id="resequence" onclick="doPermitResequencePopup('#prc.viewStatus#', '#prc.stPersistentInfo.parcelID#', '#prc.parcelType#', '#prc.stPersistentInfo.taxYear#')" rel="tooltip" title="Click to Resequence Permit Items"> <img src="/includes/images/v2/resequence.png"></span>

				<span data-mode="add-permit" data-type="permits" class="dsp-mode link" id="add-permit" onclick="addBlankRow(#prc.iPermitCount#);"> <img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"></span>
			</div>

			<cfif prc.getPermitsData.getPermitsInfo.RecordCount GT rc.page_size>
				<div id="pagination-div">
					<span class="btn-group">#p.refresh_div_renderit( prc.getPermitsData.getPermitsInfo.RecordCount, link, rc.page, rc.page_size)#</span>
				</div>
			</cfif>

			<div id="building-line-expander">
				<span data-mode="expandAll" class="dsp-mode link" data-toggle="collapse" id="expandAll" onclick="expandAll()"> <img src="/includes/images/v2/realCAMA_icon_expandAll.png" alt="" border="0"></span>
				<span data-mode="collapseAll" class="dsp-mode link" data-toggle="collapse" id="collapseAll" onclick="collapseAll()" style="display:none;"> <img src="/includes/images/v2/realCAMA_icon_collapseAll.png" alt="" border="0"></span>
			</div>
			</cfoutput>
		</cfcase>

		<cfcase value="features">
			<cfoutput>
			<cfscript>
				link = "";
			</cfscript>
			<div id="features-line-options">
				<span data-mode="add-feature" data-type="features" class="dsp-mode link" id="add-feature"> <img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"></span>
			</div>
			<div id="pagination-div">
				<span class="btn-group">#p.refresh_div_renderit( prc.getFeaturesInfo.RecordCount, link, rc.page, rc.page_size)#</span>
			</div>
			<div id="extrafeatures-line-expander">
				<span data-mode="expandAll" class="dsp-mode link" data-toggle="collapse" id="expandAll" onclick="expandAll()"> <img src="/includes/images/v2/realCAMA_icon_expandAll.png" alt="" border="0"></span>
				<span data-mode="collapseAll" class="dsp-mode link" data-toggle="collapse" id="collapseAll" onclick="collapseAll()" style="display:none;"> <img src="/includes/images/v2/realCAMA_icon_collapseAll.png" alt="" border="0"></span>
			</div>
			</cfoutput>
		</cfcase>

		<cfcase value="parcel">
			<div id="parcel-line-options">
				<!---
				<span data-mode="view" data-type="parcels" class="dsp-mode link" id="editmode"  style="display:none;"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_view.png"></span></span>
				<span data-mode="edit" data-type="parcels" class="dsp-mode link" id="viewmode"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_edit.png"></span></span>
				<span data-mode="save" data-type="parcels" class="dsp-mode link" id="save"> <img src="/includes/images/v2/realCAMA_icon_saveRecord.png"></span>
				 --->
				<!--- <span data-mode="revert" data-toggle="modal" data-target="#modalRevert" data-type="parcels" class="dsp-mode link" id="revert"> <img src="/includes/images/v2/realCAMA_icon_revert.png"></span>
				 ---><!--- <span data-mode="add-owner" data-type="parcels" class="dsp-mode link" id="add-owner"> <img src="/includes/images/v2/realCAMA_icon_addOwner.png"></span> --->
				<!--- <span data-mode="compare" data-type="parcels" class="dsp-mode link" id="compareParcelPop"> <img src="/includes/images/v2/realCAMA_icon_compare.png"></span> --->
				<!--- <img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"> --->
			</div>

			<div id="parcel-line-expander">
				<span data-mode="expandAll" class="dsp-mode link" data-toggle="collapse" id="expandAll" onclick="expandAll()"> <img src="/includes/images/v2/realCAMA_icon_expandAll.png" alt="" border="0"></span>
				<span data-mode="collapseAll" class="dsp-mode link" data-toggle="collapse" id="collapseAll" onclick="collapseAll()" style="display:none;"> <img src="/includes/images/v2/realCAMA_icon_collapseAll.png" alt="" border="0"></span>
			</div>


			<!---
			<span class="btn-group">
				<a class="btn btn-default btn-inverse btn-sm disabled" href="##"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></a>
				<a class="btn btn-default btn-inverse btn-sm disabled text-sm" href="##">Page 1 of 1</a>
				<a class="btn btn-default btn-inverse btn-sm" href="##"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></a>
			</span>
			 --->
		</cfcase>



		<cfcase value="correlation">
			<!---
			<span data-mode="view" data-type="correlation" class="dsp-mode link" id="editmode"  style="display:none;"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_view.png"></span></span>
			<span data-mode="edit" data-type="correlation" class="dsp-mode link" id="viewmode"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_edit.png"></span></span>
			<span data-mode="save" data-type="correlation" class="dsp-mode link" id="save"> <img src="/includes/images/v2/realCAMA_icon_saveRecord.png"></span>
			 --->
		</cfcase>

		<cfcase value="history">

			<div class="pull-right">

				<span class="btn-group">
					<cfoutput>
					<a class="btn btn-default btn-inverse btn-sm disabled" href="javascript:pageIncrement(-#prc.iYearsToDisplay#);" id="history-page-prev"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></a>
					<a class="btn btn-default btn-inverse btn-sm" href="javascript:pageIncrement(#prc.iYearsToDisplay#);" id="history-page-next"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></a>
					</cfoutput>
				</span>

			</div>
		</cfcase>


		<cfcase value="condomaster">
			<cfoutput>
				<div id="condo-line-options">
					<span data-mode="add-unittype" data-type="condomaster" class="dsp-mode link" id="add-unittype"> <img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"></span>
					<span data-mode="value-popup" data-type="condomaster" class="dsp-mode link" id="value-condomaster" onclick="doCondoMasterValues('#prc.stPersistentInfo.parcelID#',  '#prc.stPersistentInfo.taxYear#')" rel="tooltip" title="Click to view Values"><img src="/includes/images/v2/valuePopup.png"></span>
				</div>
			</cfoutput>
			<!---
					<span data-mode="value-popup" data-type="condomaster" class="dsp-mode link" id="value-popup" onclick="doCondoMasterValues('#prc.stPersistentInfo.parcelID#',  '#prc.stPersistentInfo.taxYear#')" rel="tooltip" title="Click to view Values">Value Pop-Up</span>
			<span class="btn-group">
				<a class="btn btn-default btn-inverse btn-sm disabled" href="##"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></a>
				<a class="btn btn-default btn-inverse btn-sm disabled text-sm" href="##">Page 1 of 1</a>
				<a class="btn btn-default btn-inverse btn-sm" href="##"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></a>
			</span>
			 --->
		</cfcase>

		<cfcase value="condounit">
			<div id="condounit-line-options">
				<!---
				<span data-mode="view" data-type="condounit" class="dsp-mode link" id="editmode"  style="display:none;"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_view.png"></span></span>
				<span data-mode="edit" data-type="condounit" class="dsp-mode link" id="viewmode"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_edit.png"></span></span>
				<span data-mode="save" data-type="condounit" class="dsp-mode link" id="save"> <img src="/includes/images/v2/realCAMA_icon_saveRecord.png"></span>
				 --->
				<!---
				<span data-mode="add-unit" data-type="condounit" class="dsp-mode link" id="add-unit"> <img src="/includes/images/v2/realCAMA_icon_bar_AddLandLine.png"></span>
			 	 --->
			</div>
		</cfcase>


		<cfcase value="tangable-owner">
			<!---
			<span data-mode="view" data-type="tangable-owner" class="dsp-mode link" id="editmode"  style="display:none;"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_view.png"></span></span>
			<span data-mode="edit" data-type="tangable-owner" class="dsp-mode link" id="viewmode"> <span id="editview"><img src="/includes/images/v2/realCAMA_icon_edit.png"></span></span>
			<span data-mode="save" data-type="tangable-owner" class="dsp-mode link" id="save"><a href="tangible.cfm?s=t&p=1"><img src="/includes/images/v2/realCAMA_icon_saveRecord.png"></a></span>
			 --->
			<div id="tangible-owner-line-expander">
				<span data-mode="expandAll" class="dsp-mode link" data-toggle="collapse" id="expandAll" onclick="expandAll()"> <img src="/includes/images/v2/realCAMA_icon_expandAll.png" alt="" border="0"></span>
				<span data-mode="collapseAll" class="dsp-mode link" data-toggle="collapse" id="collapseAll" onclick="collapseAll()" style="display:none;"> <img src="/includes/images/v2/realCAMA_icon_collapseAll.png" alt="" border="0"></span>
			</div>
		</cfcase>

		<cfcase value="tangable-asset">
			<cfoutput>
			<cfscript>
				link = "";
			</cfscript>
			<div id="pagination-div">
				<span class="btn-group">#p.refresh_div_renderit( prc.qGetTangibleAssetInfo.RecordCount, link, rc.page, rc.page_size)#</span>
			</div>
			<div id="tangible-asset-line-expander">
				<span data-mode="expandAll" class="dsp-mode link" data-toggle="collapse" id="expandAll" onclick="expandAll()"> <img src="/includes/images/v2/realCAMA_icon_expandAll.png" alt="" border="0"></span>
				<span data-mode="collapseAll" class="dsp-mode link" data-toggle="collapse" id="collapseAll" onclick="collapseAll()" style="display:none;"> <img src="/includes/images/v2/realCAMA_icon_collapseAll.png" alt="" border="0"></span>
			</div>
			</cfoutput>
		</cfcase>
	</cfswitch>
</span>
