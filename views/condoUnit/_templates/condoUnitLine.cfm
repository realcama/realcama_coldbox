<cfoutput>
	<CFSET LumpSumAddOns = 0>
	<CFSET PercentageAddOns = 0>
	<CFSET RateAddOns = 0>
	<CFSET PointsAddOns = 0>

	<cfsavecontent variable="strOptions">
		<cfloop query="prc.getCondoUnitSpecificCodes">
			<tr>
				<td class="formatted-data-values">#prc.getCondoUnitSpecificCodes.codetype#</td>
				<td class="formatted-data-values" width="80">#prc.getCondoUnitSpecificCodes.codedescription#</td>
				<td class="formatted-data-values"></td>
				<td class="rightAlign formatted-data-values">&nbsp;</td>
				<td class="rightAlign formatted-data-values">&nbsp;</td>
				<CFIF prc.getCondoUnitSpecificCodes.adjustmentfactor GT 0>
					<td class="rightAlign formatted-data-values">#NumberFormat((prc.getCondoUnitSpecificCodes.adjustmentfactor),".000")#</td>
					<CFIF PercentageAddOns EQ 0>
						<CFSET PercentageAddOns = prc.getCondoUnitSpecificCodes.adjustmentfactor>
					<CFELSE>
						<CFSET PercentageAddOns = PercentageAddOns * prc.getCondoUnitSpecificCodes.adjustmentfactor>
					</CFIF>
				<CFELSE>
					<td class="rightAlign formatted-data-values">&nbsp;</td>
				</CFIF>
				<CFIF prc.getCondoUnitSpecificCodes.adjustmentamount GT 0>
					<td class="rightAlign formatted-data-values">#NumberFormat(prc.getCondoUnitSpecificCodes.adjustmentamount,".000")#</td>
					<CFSET LumpSumAddOns = LumpSumAddOns + prc.getCondoUnitSpecificCodes.adjustmentamount>
				<CFELSE>
					<td class="rightAlign formatted-data-values">&nbsp;</td>
				</CFIF>
			</tr>
		</cfloop>
	</cfsavecontent>
	<!--- <CFSET PercentageAddOns = PercentageAddOns * 100> --->


	<div class="panel-heading" role="tab" class="panel-collapse collapse" >
		<table class="land-line-summary">
		<tr href="##collapse#prc.getCondoUnitInfo.CurrentRow#" class="section-bar-expander" data-parent="heading1" data-toggle="collapse" aria-controls="collapse#prc.getCondoUnitInfo.CurrentRow#" onclick="toggleChevronChild(#prc.getCondoUnitInfo.CurrentRow#)">
			<td width="95%" class="section-bar-label">Condo Unit Data</td>
			<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_#prc.getCondoUnitInfo.CurrentRow#"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse#prc.getCondoUnitInfo.CurrentRow#" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading#prc.getCondoUnitInfo.CurrentRow#">
		<div id="view_mode_item_#prc.getCondoUnitInfo.CurrentRow#" class="row fix-bs-row">
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Unit Type</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.unittype#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Unit Number</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.unitnumber#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Floor</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.floornumber#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Appraised By</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.appraisedby#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Date</span><br/>
				<span class="formatted-data-values">#DateFormat(prc.getCondoUnitInfo.appraiseddate,"m/d/yyyy")#</span>
			</div>
			<div class="col-xs-2 databox">

			</div>

			<div class="clearfix"></div>


			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Bedrooms</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.numberbedrooms#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Square Feet</span><br/>
				<span class="formatted-data-values">#NumberFormat(prc.getCondoUnitInfo.sqft,",")#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Base Value</span><br/>
				<span class="formatted-data-values">$#NumberFormat(prc.getCondoUnitInfo.basevalue,",")#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Complex Number</span><br/>
				<span class="formatted-data-values">#complexID#</span>
			</div>
			<div class="col-xs-2 databox">

			</div>
			<div class="col-xs-2 databox">

			</div>
			<div class="clearfix"></div>


			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Baths</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.numberbathrooms#</span>
			</div>
			<div class="col-xs-6 databox">
				<span class="formatted-data-label">Neighborhood Code</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.neighborhoodcode#: #prc.getCondoUnitInfo.NeighborhoodCodeDescription#</span>
			</div>
			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Adjustments</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.totaladjustments#</span>
			</div>

			<div class="clearfix"></div>

			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Actual Year Built</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.actualyearbuilt#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Effective Year Built</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.effectiveyearbuilt#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">RCN</span><br/>
				<span class="formatted-data-values">$#NumberFormat(prc.getCondoUnitInfo.rcnvalue,",")#</span>
			</div>
			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Functional Obsolescence</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.funcobsolescencepct#</span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Percent Good</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.percentgood#%</span>
			</div>


			<div class="clearfix"></div>


			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Economic Obsolescence</span><br/>
				<span class="formatted-data-values">#prc.getCondoUnitInfo.ecoobsolescencepct#</span>
			</div>
			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Special Condition Code</span><br/>
				<span class="formatted-data-values"><CFIF LEN(TRIM(prc.getCondoUnitInfo.SConditionCode)) NEQ 0>#prc.getCondoUnitInfo.SConditionCode#: #prc.getCondoUnitInfo.SConditionCodeDescription#</CFIF></span>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label"></span><br/>
				<span class="formatted-data-values"><CFIF LEN(TRIM(prc.getCondoUnitInfo.SConditionCode)) NEQ 0>#prc.getCondoUnitInfo.SpecialConditionPct#%</CFIF></span>
			</div>
			<div class="col-xs-2 databox text-center">
				<span class="formatted-data-label">Replacement</span><br/>
				<span class="formatted-data-values">#Left(YesNoFormat(prc.getCondoUnitInfo.damageReplacement),1)#</span>
			</div>


			<div class="clearfix">&nbsp;</div>

			<div class="row fix-bs-row add-in-box <!--- add-in-box-view --->">
				<div class="col-xs-12">
					<table class="building-udf-table">
					<thead>
						<tr>
							<td width="150" class="formatted-data-label">Condo Unit Codes</td>
							<td colspan="2"></td>
							<td width="15%" class="text-right formatted-data-label">Points</td>
							<td width="15%" class="text-right formatted-data-label">Lump Sum</td>
							<td width="15%" class="text-right formatted-data-label">Percentage</td>
							<td width="15%" class="text-right formatted-data-label">Rate Add-On</td>
						</tr>
					</thead>
					<tbody>
						<CFIF LEN(TRIM(strOptions)) EQ 0>
							<tr><td colspan="7">None</td></tr>
						<CFELSE>
							#strOptions#
						</CFIF>
					</tbody>
					<tfoot class="adjustmentFooter">
						<tr>
							<td width="100"></td>
							<td colspan="2" class="text-right formatted-data-label">Net Adjustments</td>
							<td width="15%" class="text-right formatted-data-label">#NumberFormat(PointsAddOns,"0.000")#</td>
							<td width="15%" class="text-right formatted-data-label">#NumberFormat(LumpSumAddOns,"0.000")#</td>
							<td width="15%" class="text-right formatted-data-label">#NumberFormat(PercentageAddOns,"0.000")#</td>
							<td width="15%" class="text-right formatted-data-label">#NumberFormat(RateAddOns,"0.000")#</td>
						</tr>
					</tfoot>
					</table>
				</div>

				<div class="col-xs-12 building-button-row">
					&nbsp;
				</div>
			</div>

			<div class="clearfix">&nbsp;</div>

			<div class="col-xs-12">
				<div class="land-part-action-buttons">
					<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
				</div>
			</div>
		</div>

		<div id="edit_mode_item_#prc.getCondoUnitInfo.CurrentRow#" class="row fix-bs-row" style="display:none;">

			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Unit Type</span><br/>
				<input type="text" name="unittype" value="#prc.getCondoUnitInfo.unittype#" class="form-control" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Unit Number</span><br/>
				<input type="text" name="unitnumber" value="#prc.getCondoUnitInfo.unitnumber#" class="form-control numeric" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Floor</span><br/>
				<input type="text" name="floornumber" value="#prc.getCondoUnitInfo.floornumber#" class="form-control numeric" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Appraised By</span><br/>
				<input type="text" name="appraisedby" value="#prc.getCondoUnitInfo.appraisedby#" class="form-control" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Date</span><br/>
				<input type="text" name="appraiseddate" value="#DateFormat(prc.getCondoUnitInfo.appraiseddate,'m/d/yyyy')#" class="form-control datefield" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">

			</div>
			<div class="clearfix"></div>



			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Bedrooms</span><br/>
				<input type="text" name="numberbedrooms" value="#prc.getCondoUnitInfo.numberbedrooms#" class="form-control numeric" readonly data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Square Feet</span><br/>
				<input type="text" name="sqft" value="#prc.getCondoUnitInfo.sqft#" class="form-control numeric" readonly data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Base Value</span><br/>
				$<input type="text" name="basevalue" value="#prc.getCondoUnitInfo.basevalue#" class="form-control width-90percent numeric" readonly data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Complex Number</span><br/>
				<input type="text" name="complexID" value="#complexID#" class="form-control" readonly data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)"/>
			</div>
			<div class="col-xs-2 databox">

			</div>
			<div class="col-xs-2 databox">

			</div>
			<div class="clearfix"></div>



			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Baths</span><br/>
				<input type="text" name="numberbathrooms" value="#prc.getCondoUnitInfo.numberbathrooms#" class="form-control numeric" readonly data-toggle="tooltip" title="This element is controlled by Condo Master"  />
			</div>
			<div class="col-xs-6 databox">
				<span class="formatted-data-label">Neighborhood Code</span><br/>
				<select data-tags="true" class="form-control select2-3piece-new" name="neighborhoodcode" disabled data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)">
					<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
					<cfloop query="prc.getNeighborhoods">
						<option value="#prc.getNeighborhoods.nbhdcd#" data-val="#prc.getNeighborhoods.nbhdcd#" data-desc="#prc.getNeighborhoods.nbhdds#" data-adjval="#NumberFormat(prc.getNeighborhoods.nbhdfa,'9.999')#" <cfif prc.getCondoUnitInfo.neighborhoodcode EQ prc.getNeighborhoods.nbhdcd> selected</cfif>>#prc.getNeighborhoods.nbhdcd#: #prc.getNeighborhoods.nbhdds#</option>
					</cfloop>
				</select>
			</div>
			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Adjustments</span><br/>
				<input type="text" name="totaladjustments" value="#prc.getCondoUnitInfo.totaladjustments#" class="form-control" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>



			<div class="clearfix"></div>


			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Actual Year Built</span><br/>
				<input type="text" name="actualyearbuilt" value="#prc.getCondoUnitInfo.actualyearbuilt#" class="form-control" readonly data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Effective Year Built</span><br/>
				<input type="text" name="effectiveyearbuilt" value="#prc.getCondoUnitInfo.effectiveyearbuilt#" class="form-control" readonly data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">RCN</span><br/>
				$<input type="text" name="rcnvalue" value="#prc.getCondoUnitInfo.rcnvalue#" class="form-control width-90percent numeric" readonly data-toggle="tooltip" title="This element is controlled by Condo Master" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Functional Obsolescence</span><br/>
				<input type="text" name="funcobsolescencepct" value="#prc.getCondoUnitInfo.funcobsolescencepct#" class="form-control numeric" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label">Percent Good</span><br/>
				<input type="text" name="percentgood" value="#prc.getCondoUnitInfo.percentgood#" class="form-control width-80percent numeric" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />%
			</div>

			<div class="clearfix"></div>






			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Economic Obsolescence</span><br/>
				<input type="text" name="ecoobsolescencepct" value="#prc.getCondoUnitInfo.ecoobsolescencepct#" class="form-control numeric" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />
			</div>
			<div class="col-xs-4 databox">
				<span class="formatted-data-label">Special Condition Code</span><br/>
				<select data-tags="true"  class="form-control select2-2piece-new" name="specialconditioncode" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)">
					<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
					<cfloop query="prc.getSpecialConditionCodes">
						<option data-val="#prc.getSpecialConditionCodes.code#" data-desc="#prc.getSpecialConditionCodes.codedescription#" value="#prc.getSpecialConditionCodes.SpecialconditionCodeID#" <CFIF prc.getCondoUnitInfo.specialconditioncode EQ prc.getSpecialConditionCodes.SpecialconditionCodeID>selected</CFIF>>#prc.getSpecialConditionCodes.code#:#prc.getSpecialConditionCodes.codedescription#</option>
					</cfloop>
				</select>
			</div>
			<div class="col-xs-2 databox">
				<span class="formatted-data-label"></span><br/>
				<input type="text" name="specialconditionpct" value="#prc.getCondoUnitInfo.SpecialConditionPct#" class="form-control numeric width-80percent" onchange="changeVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)" />%
			</div>

			<div class="col-xs-2 databox text-center">
				<span class="formatted-data-label">Replacement</span><br/>
				<span class="formatted-data-label"><input type="checkbox" name="damageReplacement" value="1" <CFIF prc.getCondoUnitInfo.damageReplacement EQ 1>checked</CFIF> onclick="changeCheckboxVal(#prc.getCondoUnitInfo.CurrentRow-1#,this)"></span>
			</div>

			<div class="clearfix">&nbsp;</div>

			<div class="row fix-bs-row add-in-box <!--- add-in-box-view --->">
				<div class="col-xs-12">
					<table class="building-udf-table">
					<thead>
						<tr>
							<td width="150" class="formatted-data-label">Condo Unit Codes</td>
							<td colspan="2"></td>
							<td width="15%" class="rightAlign formatted-data-label">Points</td>
							<td width="15%" class="rightAlign formatted-data-label">Lump Sum</td>
							<td width="15%" class="rightAlign formatted-data-label">Percentage</td>
							<td width="15%" class="rightAlign formatted-data-label">Rate Add-On</td>
						</tr>
					</thead>
					<tbody id="condoUnit-options">
						<CFIF LEN(TRIM(strOptions)) EQ 0>
							<tr><td colspan="7">None</td></tr>
						<CFELSE>
							#strOptions#
						</CFIF>
					</tbody>
					<tfoot class="adjustmentFooter">
						<tr>
							<td width="100"></td>
							<td colspan="2" class="rightAlign formatted-data-label">Net Adjustments</td>
							<td width="15%" class="rightAlign formatted-data-label" id="condo_netadj_points">#NumberFormat(PointsAddOns,"0.000")#</td>
							<td width="15%" class="rightAlign formatted-data-label" id="condo_netadj_lumpsum">#NumberFormat(LumpSumAddOns,"0.000")#</td>
							<td width="15%" class="rightAlign formatted-data-label" id="condo_netadj_percentage">#NumberFormat(PercentageAddOns,"0.000")#</td>
							<td width="15%" class="rightAlign formatted-data-label" id="condo_netadj_rateaddon">#NumberFormat(RateAddOns,"0.000")#</td>
						</tr>
					</tfoot>
					</table>
				</div>
<!--- #prc.stPersistentInfo.taxYear#, #prc.getCondoUnitInfo.condoUnitID# --->
				<div class="col-xs-12 building-button-row">
					<a onclick="doCondoPopup()" data-toggle="tooltip" data-placement="top" title="Click to alter condo unit codes"><img src="/includes/images/v2/addEdit_blue.gif" alt="" width="156" height="26" border="0"></a>
				</div>
			</div>

			<div class="clearfix">&nbsp;</div>

			<div class="col-xs-12">
				<div class="land-part-action-buttons">
					<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
					&nbsp;&nbsp;
					<a href="javascript:saveWDDX()"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
				</div>
			</div>
		</div>

		<br clear="all"/>

	</div> <!--- END tabpanel --->

</cfoutput>