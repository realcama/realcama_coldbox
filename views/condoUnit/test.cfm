<cfscript>
    // Build up a new query. Notice that we are using the augmented
    // features of ColdFusion 10 and the queryNew() to build complex
    // queries in one command.
    friends = queryNew(
        "id, name",
        "cf_sql_integer, cf_sql_varchar",
        [
            [ 1, "Tricia" ],
            [ 2, "Sarah" ],
            [ 3, "Joanna" ]
        ]
    );
    // The query object now supports FOR-IN iteration in CFScript. We
    // can iterate over the query, row by row.
    for (friend in friends){
        // When iterating over a query in CFScript, you can use the
        // main query object to get meta-data; then, use the row
        // object to get row-specific properties.
        writeOutput(
            "[ #friends.currentRow# of #friends.recordCount# ] " &
            friend.name &
            "<br />"
        );
    }
</cfscript>