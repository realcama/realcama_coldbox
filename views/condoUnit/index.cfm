<cfoutput>
	<CFIF prc.getCondoUnitInfo.useCode EQ "H" OR prc.getCondoUnitInfo.useCode EQ "H.">
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				Selected parcel is a header record
			</div>
		</div>

	<CFELSEIF LEN(TRIM(prc.getCondoUnitInfo.condoID)) EQ 0>
		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				No Condo Unit record found for the selected parcel
			</div>
		</div>
	<CFELSE>
		<CFSET iTotalPages = Ceiling(prc.getCondoUnitInfo.RecordCount / prc.page_size)>

		<CFSET complexID = formatComplexID(prc.getCondoUnitInfo.condoID)>

		<form action="#event.buildLink('condoUnit/condoUnitSave')#" method="post" name="frmSave" id="frmSave">
			<input type="hidden" name="taxYear" id="taxYear" value="#prc.stPersistentInfo.taxYear#">
			<input type="hidden" name="ParcelID" id="ParcelID" value="#prc.stPersistentInfo.ParcelID#">
			<input type="hidden" name="condoUnitData" id="condoUnitData" value="">
			<input type="hidden" name="condoUnitCodesData" id="condoUnitCodesData" value="">
		</form>
		<CFIF Controller.getSetting( "bShowPacketLinks")>
			<div class="alert alert-info">
				<A href="javascript:void(0);" onclick="WriteRaw(condoUnitData);">Condo Unit Packet</A>&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onclick="WriteRaw(condoUnitCodesData);">Condo Codes Packet</A>&nbsp;&nbsp;&nbsp;
			</div>
		</CFIF>

		<!--- <script src="includes/js/condoUnit.js?g=#now()#"></script> --->
		#addAsset('/includes/js/condoUnit.js,/includes/js/formatSelect.js')#

		<script>
			var taxYear = #prc.stPersistentInfo.taxYear#;
			var ParcelID = '#prc.stPersistentInfo.ParcelID#';
			var thisCondoUnitID = '#TRIM(prc.getCondoUnitInfo.condoUnitID)#';
			var thisCondoID = '#TRIM(prc.getCondoUnitInfo.condoID)#';

			var iCurrentPage = 1;

			<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.getCondoUnitInfo#"
					TOPLEVELVARIABLE="condoUnitData">

			<CFWDDX ACTION="CFML2JS"
					INPUT="#prc.getCondoUnitSpecificCodes#"
					TOPLEVELVARIABLE="condoUnitCodesData">
		</script>


		<div class="container-border container-spacer">
			<div class="table-header content-bar-label">
				<CFSET showTabOptions = "condounit">
				#renderView('_templates/miniNavigationBar')#
				Condo Unit
			</div>
			<div id="expander-holders" class="panel panel-default">
				#renderView('condoUnit/_templates/condoUnitLine')#
				<!--- <cfloop query="prc.getCondoUnitInfo">		 --->
					<!--- <cfinclude template="condounitLine.inc">	--->
				<!--- </cfloop> --->
			</div>
		</div>

	</CFIF>

</cfoutput>