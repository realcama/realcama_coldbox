<cfoutput>
	<div id="popLandUDF" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-land-popupUDF">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Condo Unit Codes</h4>
	      </div>
			<div class="modal-body">
				<div id="errorContainer" class="row"></div>
				<div id="adjContainer">
					<div id="adjContent"></div>
				</div>
			</div>

	      <div class="modal-footer">
		  	<button id="addRowButton" class="btn btn-primary btn-add-option-udf" type="button" onclick="addAdjRow()"><i class="glyphicon glyphicon-plus-sign"></i> Add Another Option</button>
	        <button type="button"  class="btn btn-default"  onclick="parent.$.fancybox.close();"><i class="glyphicon glyphicon-ok-sign"></i> Finished</button>
	      </div>
	    </div>
	  </div>
	</div>
</cfoutput>

<CFSET lstCodes = "Balcony,Desirability,Location,Parking,Recreation,Views">

<script>
	var iOptionsOnModal = 0;
	<cfoutput>
	var condoID = '#condoID#';
	var condoUnitID = '#condoUnitID#';
	</cfoutput>

	function createAdjEntryRow(iRow, inCodeID, inCodeType, mode) {
		var strHTML = "";

		strHTML = strHTML + "<div id='condounitcoderow_" + iRow + "' class='udfrow'>";

		if(mode == "Insert") {
			strHTML = strHTML + "	<span class='icon-nothing'></span>";
		} else {
			strHTML = strHTML + "	<span class='glyphicon glyphicon-remove-sign' onclick='removeAdjRow(" + iRow + ")'></span>";
		}

		<!--- Create the list of the options for this Adjustment --->
		strHTML = strHTML + "	<select name='codetype' id='codetype_" + iRow + "' data-rownumber=" + iRow + " class='form-control select2-1piece-new' onchange='changeTypeVal(" + iRow +",this);changeUDFMethod(this)'>";
		<cfoutput>
			strHTML = strHTML + "		<option value='Select' data-code='&nbsp;' data-desc='Select'>Select</option>";
			<cfloop index="strCode" list="#lstCodes#" delimiters=",">
				strChecked = (inCodeType == '#strCode#') ? " selected" : "";
				strHTML = strHTML + "		<option value='#strCode#' data-desc='#strCode#' " + strChecked + ">#strCode#</option>";
			</cfloop>
		</cfoutput>
		strHTML = strHTML + "	</select>";

		strSelectedType = "";
		strSelectedAmount = "0.00";
		<cfloop index="strCode" list="#lstCodes#" delimiters=",">
			<!--- Individual code drop downs --->
			<CFSET qryCurrentLoop = "prc.qryGet#strCode#Codes">

			<cfoutput>
				strDisplay = (inCodeType == '#strCode#') ? "" : " style='display:none'";
				strHTML = strHTML + "<span class='selectCodeContainer'><select name='#strCode#' id='#strCode#_" + iRow + "' data-tags='true' class='form-control select2-2piece-new'" + strDisplay +" onchange='changeCodeVal(" + iRow +",this)'>";
			</cfoutput>
			strHTML = strHTML + "		<option value='-1' data-val='' data-desc='Select' data-code='&nbsp;'>Select</option>";

			<cfoutput query="#Evaluate(DE(qryCurrentLoop))#">
				strSelected = (inCodeID == '#condocodeid#') ? " selected" : "";
				<CFIF adjustmentamount GT 0>
					if(inCodeID == '#condocodeid#') {
						strSelectedType = "Amount";
						strSelectedAmount = "#adjustmentamount#";
					}
				<CFELSE>
					if(inCodeID == '#condocodeid#') {
						strSelectedType = "Factor";
						strSelectedAmount = "#NumberFormat(adjustmentfactor,".000")#";
					}
				</CFIF>
				strHTML = strHTML + "		<option value='#condocodeid#' data-val='#code#' data-desc='#codedescription#' data-code='#code#' " + strSelected + " data-amount='#NumberFormat(adjustmentamount,".000")#' data-factor='#NumberFormat(adjustmentfactor,".000")#'><CFIF code NEQ "">#code#: </cfif>#codedescription#</option>";
			</cfoutput>
			strHTML = strHTML + "	</select></span>";
		</cfloop>


		<!--- Adjustment Type  --->
		strDisplay = (inCodeType != "") ? "" : " style='display:none'";
		strHTML = strHTML + "<select name='adjustmenttype' id='adjustmenttype_" + iRow + "' disabled data-tags='true' class='form-control select2-1piece-new' " + strDisplay + ">";
		strHTML = strHTML + "		<option value='N/A' data-desc='N/A'>N/A</option>";

		if(strSelectedType == "Factor") {
			strHTML = strHTML + "		<option value='Factor' data-desc='Factor' selected>Factor</option>";
			strHTML = strHTML + "		<option value='Amount' data-desc='Amount'>Amount</option>";
		} else {
			if(strSelectedType == "Amount") {
				strHTML = strHTML + "		<option value='Factor' data-desc='Factor'>Factor</option>";
				strHTML = strHTML + "		<option value='Amount' data-desc='Amount' selected>Amount</option>";
			} else {
				strHTML = strHTML + "		<option value='Factor' data-desc='Factor'>Factor</option>";
				strHTML = strHTML + "		<option value='Amount' data-desc='Amount'>Amount</option>";
			}
		}

		strHTML = strHTML + "		<option value='Lump Sum Add-On' data-desc='Lump Sum Add-On'>Lump Sum Add-On</option>";
		strHTML = strHTML + "		<option value='Percentage' data-desc='Percentage'>Percentage</option>";
		strHTML = strHTML + "		<option value='Rate Add-On' data-desc='Rate Add-On'>Rate Add-On</option>";
		strHTML = strHTML + "		<option value='Points' data-desc='Points'>Points</option>";
		strHTML = strHTML + "	</select>";


		<!--- Adjustment value --->
		<cfoutput>
		strDisplay = (inCodeType != "") ? "" : " style='display:none'";
		strHTML = strHTML + "	<input name='adjustmentvalue' id='adjustmentvalue_" + iRow + "' readonly class='form-control two-part-small numeric' type='text' value='" + strSelectedAmount + "' " + strDisplay + ">";
		</cfoutput>

		strHTML = strHTML + "</div>";

		return strHTML;
	}

	function turnOffSelects(iRowToSetSelects){
		<cfoutput>
			<cfloop index="strCode" list="#lstCodes#" delimiters=",">
				$("###strCode#_" + iRowToSetSelects).hide();
				$("##s2id_#strCode#_" + iRowToSetSelects).hide();
				$("##s2id_#strCode#_" + iRowToSetSelects).select2("val",-1);
				$("###strCode#_" + iRowToSetSelects).val(-1);
			</cfloop>
		</cfoutput>
	}
	function showOption(method,iRowToSetSelects) {
		switch(method) {
			<cfoutput>
				<cfloop index="strCode" list="#lstCodes#" delimiters=",">
					case "#strCode#":
						$("##s2id_#strCode#_" + iRowToSetSelects).show();
						$("##s2id_#strCode#_" + iRowToSetSelects).css("display","inline-block");

						$("##s2id_adjustmenttype_" + iRowToSetSelects).show();
						$("##s2id_adjustmenttype_" + iRowToSetSelects).css("display","inline-block");
						$("##adjustmentvalue_" + iRowToSetSelects).show();

						break;
				</cfloop>
			</cfoutput>
		}
	}


	function changeTypeVal(iRow,obj) {
		switch(obj.name) {
			<cfoutput>
				<cfloop index="strCode" list="#lstCodes#" delimiters=",">
					case "#strCode#":
						var selectedOption = $(obj).find('option:selected');
						var codetype = selectedOption.val();
						parent.condoUnitCodesData.setField( iRow, "codetype", codetype );
						parent.condoUnitCodesData.setField( iRow, "condocodeid", -1 );		// Force the user to select a new value
						break;
				</cfloop>
			</cfoutput>
			default:
				parent.condoUnitCodesData.setField( iRow, obj.name, $(obj).val() );
				break;
		}
		createLabel();
		$('[data-toggle="tooltip"]').tooltip();
	}

	function changeCodeVal(iRow,obj) {
		switch(obj.name) {
			<cfoutput>
				<cfloop index="strCode" list="#lstCodes#" delimiters=",">
					case "#strCode#":
						var selectedOption = $(obj).find('option:selected');
						var codeID = selectedOption.val();
						var description = selectedOption.data('desc');

						var amount = selectedOption.data('amount');
						var factor = selectedOption.data('factor');

						$("##s2id_adjustmenttype_" + iRow).select2("val","N/A");
						$("##adjustmentvalue_" + iRow).val("0.00");

						// Default the adjustment values so that they get the correct value based on the selected otpion
						parent.condoUnitCodesData.setField( iRow, "adjustmentamount", "0.00" );
						parent.condoUnitCodesData.setField( iRow, "adjustmentfactor", "0.00" );

						if(parent.condoUnitCodesData.getField( iRow, "mode") == "Unchanged") {
							parent.condoUnitCodesData.setField( iRow, "mode", "edit" );
						}

						if(amount > 0 && factor == 0.000) {
							$("##s2id_adjustmenttype_" + iRow).select2("val","Amount");
							$("##adjustmentvalue_" + iRow).val(amount);
							parent.condoUnitCodesData.setField( iRow, "adjustmentamount", amount );

						}
						if(amount == 0.000 && factor > 0) {
							var iDispFactor = factor * 1;
							$("##s2id_adjustmenttype_" + iRow).select2("val","Factor");
							$("##adjustmentvalue_" + iRow).val(iDispFactor.toFixed(3));
							parent.condoUnitCodesData.setField( iRow, "adjustmentfactor", factor );
						}

						parent.condoUnitCodesData.setField( iRow, "condocodeid", codeID );
						parent.condoUnitCodesData.setField( iRow, "codedescription", description );

						break;
				</cfloop>
			</cfoutput>
			default:
				parent.condoUnitCodesData.setField( iRow, obj.name, $(obj).val() );
				break;
		}
		createLabel();
		$('[data-toggle="tooltip"]').tooltip();
	}


	function createLabel(){
		var strLabel = "";

		var iLumpSum = 0;
		var iPercentage = 0;
		var iRateAddOn = 0;
		var iPointsSum = 0;

		/* find out if we have to use depth or not in the calculation of the net adjustment row */
		for ( var iCurrentRow = 0; iCurrentRow < parent.condoUnitCodesData.getRowCount(); iCurrentRow++ ) {
			mode = parent.condoUnitCodesData.getField(iCurrentRow,"mode");
			codetype = parent.condoUnitCodesData.getField( iCurrentRow, "codetype");
			codedescription = parent.condoUnitCodesData.getField( iCurrentRow, "codedescription");

			amount = parseFloat(parent.condoUnitCodesData.getField( iCurrentRow, "adjustmentamount"));
			factor = parseFloat(parent.condoUnitCodesData.getField( iCurrentRow, "adjustmentfactor"));

			if(mode != "Delete") {
				if(
					(codetype == null || codetype == "Select" || codetype === "") ||
					(codedescription == null || codedescription == "Select") <!--- ||  codedescription == "N/A" --->
					){
					<!--- this is a junk entry, not all required pieces are selected --->
				} else {

					if(amount > 0 && factor == 0.00) {

						strLabel = strLabel + "<tr>";
						strLabel = strLabel + "		<td class='formatted-data-values'>" + codetype + "</td>";
						strLabel = strLabel + "		<td class='formatted-data-values' width='80'>" + codedescription + "</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>" + amount.toFixed(3) + "</td>";
						strLabel = strLabel + "</tr>";
						iLumpSum = iLumpSum + amount;
					}
					if(amount == 0.00 && factor > 0) {
						var iDispFactor = factor * 1;
						strLabel = strLabel + "<tr>";
						strLabel = strLabel + "		<td class='formatted-data-values'>" + codetype + "</td>";
						strLabel = strLabel + "		<td class='formatted-data-values' width='80'>" + codedescription + "</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>" + iDispFactor.toFixed(3) +"</td>";
						strLabel = strLabel + "		<td class='text-right formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "</tr>";
						if(iPercentage == 0) {
							iPercentage = factor;
						} else {
							iPercentage = iPercentage * factor;
						}
					}


				}

			}
		}

		if(strLabel === "") {
			strLabel = "<tr><td colpan='6'>None</td></tr>";
		}

		//iPercentage = iPercentage * 100;
		$("#condoUnit-options").html(strLabel);
		$("#condo_netadj_points").html(iPointsSum.toFixed(3));
		$("#condo_netadj_lumpsum").html(iLumpSum.toFixed(3));
		$("#condo_netadj_percentage").html(iPercentage);
		$("#condo_netadj_rateaddon").html(iRateAddOn.toFixed(3));

	}
</script>

<cfoutput>
	#addAsset('/includes/css/select2.css,/includes/js/select2.js,/includes/js/condoUnitAdjustments.js,/includes/js/formatSelect.js')#
</cfoutput>
<script type="text/javascript">
	$( document ).ready(function() {
		buildModal();
		changeSelectsToSelect2s();
	});
</script>
