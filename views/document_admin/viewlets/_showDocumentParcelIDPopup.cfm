<cfparam name="row" default="0">

<CFIF NOT IsNumeric(row)>
	<SCRIPT>
		parent.$.fancybox.close();
	</SCRIPT>
	<CFABORT>
</CFIF>

<!--- Remember that JS is 0 based, and CF is 1 based --->
<CFSET jsRowNumber = row - 1>

<script>
	var iOptionsOnModal = 0;
	var intTotalRows = 0;
	<cfoutput>
	var iUrlRow = #row#;
	var jsRowNumber = #jsRowNumber#;
<!---	var bl_id = #bl_id#;
    var iBLItem = '#blitem#';
	var iBudAdjColumn = '#iBudAdjColumn#';
--->

    var mode = "Insert";
	</cfoutput>

	var iCurrentRow = 0;

	// Get the Parcel ID's already associated with this document

	var strFormParcels = $('#documentParcelID').val().split(",");
	console.log("strFormParcels: " + strFormParcels);
	var strFormParcelData = $('#documentParcelID').val();
	console.log("strFormParcelData: " + strFormParcelData);
	$( document ).ready(function() {
		buildModal();
	});

  	$('#btnSave').click(function() {
		var strParcelDisplay = "";
		var strParcelValue = "";
		var strParcelIDToolTip = "";
		var strCurrentRowValue = "";
		var intValidRows = 0;

		for(var i = 0; i < iCurrentRow; i++){

			strCurrentRowObj = $("#Parcelvalue_" + i);

			if (strCurrentRowObj.val() != null) {
				intValidRows++;

				strCurrentRowValue = $("#Parcelvalue_" + i).val();
				console.log("strCurrentRowValue " + strCurrentRowValue );

				if((i > 0) && (i < iCurrentRow) ) {
					console.log("Adding comma:  " + i );
					//strParcelDisplay = strParcelDisplay + '<br>';
					strParcelValue = strParcelValue + ',';
					strParcelIDToolTip = strParcelIDToolTip + "\n";
				}
				//if (typeof var === 'undefined')
				console.log("Setting vars for row: " + i );
				//strParcelDisplay = strParcelDisplay + strCurrentRowObj.val();
				strParcelValue = strParcelValue + strCurrentRowObj.val();
				strParcelIDToolTip = strParcelIDToolTip + strCurrentRowObj.val();
				//strParcelDisplay = strParcelDisplay + $("#Parcelvalue_" + i).val();
				//strParcelValue = strParcelValue + $("#Parcelvalue_" + i).val();
			}
			//console.log("Outside Loop: " + i );

			//console.log("At iCurrentRow " + i + ", there is a value of " + strParcelDisplay);
		}

		console.log("Valid rows: " + intValidRows);


		$("#documentParcelID").val(strParcelValue);

		// Determine how we display the parcel IDs based on how many have been eneted.
		if (intValidRows == 1) {
			console.log("Inside Valid rows");
			strFieldDisplay = "<input type='text' name='DisplayParcelID' id='DisplayParcelID' Value='" + strParcelValue + "' class='form-control doNotLeaveEmpty' readonly='readonly'>";
			$("#ParcelWrapper").html(strFieldDisplay);
			$("#ParcelWrapper").show();
		}
		else if (intValidRows >= 2)  {
			console.log("Inside Multiple Valid rows");

			strParcelDisplay = "<div style='text-align: center;'><div class='formTooltipHolder' data-toggle='tooltip' data-placement='top' title='" + strParcelIDToolTip + "'>Multiple Parcel IDs</div></div>";
			$("#ParcelWrapper").html(strParcelDisplay);
			$("#ParcelWrapper").show();
		}
		else if (intValidRows >= 0)  {
			strParcelDisplay = "";
			$("#ParcelWrapper").html(strParcelDisplay);
			$("#ParcelWrapper").show();
		}


		parent.$.fancybox.close();
  	});

    function buildModal() {

<!---		<cfset strDisplayParcelID = strParcelID>

		for ( var iCurrentRow = 0; iCurrentRow < parent.udfData.getRowCount(); iCurrentRow++ ) {
			iCurrentRowItem = parent.udfData.getField(iCurrentRow,"blitem");

			if(iCurrentRowItem == iBLItem) {


				mode = parent.udfData.getField(iCurrentRow,"mode");
--->
				if(mode != "Delete") {
//					try {
//						adj_id = parent.udfData.getField(iCurrentRow,"adj_id").toString();
//					} catch(err) {adj_id = -1;}
//					adjdesc = parent.udfData.getField(iCurrentRow,"adjdesc");
//					bladjvalue = parseFloat(parent.udfData.getField(iCurrentRow,"bladjvalue"));
//					bladjtyp = parent.udfData.getField(iCurrentRow,"bladjtyp");

					var strHTML = "";
                    ParcelValue = "";

					if(strFormParcels != "") {
						$.each(strFormParcels,function(i){
							ParcelValue = strFormParcels[i];
							//alert(ParcelValue);
							strHTML = strHTML + LoadExistingParcelEntryRow(iCurrentRow, ParcelValue, 'Edit');
							iCurrentRow++;
						});
					} else {
						//alert("creating entry")
						strHTML = createParcelEntryRow(iCurrentRow, ParcelValue, 'Edit');
						iCurrentRow++;
					}

					if(iOptionsOnModal === 0) {
						$("#ParcelContainer").html(strHTML);
					} else {
						$("#ParcelContainer").append(strHTML);
					}
<!---
					<!--- Set the values for these new elements	--->
//					$("#adjdesc_" + iCurrentRow).val(adjdesc);
//					<cfoutput>
//						<cfloop index="iLoopCounter" from="1" to="#iCambudQueries#">
//							$("##cambud#iLoopCounter#_" + iCurrentRow).val(adj_id);
//						</cfloop>
//					</cfoutput>

//					$("#bladjtyp_" + iCurrentRow).val(bladjtyp);
//					if(!isNaN(bladjvalue))
//						$("#bladjvalue_" + iCurrentRow).val(bladjvalue.toFixed(3));
//					else
//						$("#bladjvalue_" + iCurrentRow).val(0.000);
--->
					iOptionsOnModal++;
				}
<!---
			}
		}
--->
		if(iOptionsOnModal == 0) {
			$("#ParcelContainer").html("No Parcels defined for this document");
		}
	}

	function LoadExistingParcelEntryRow(iRow, ParcelIDvalue, Mode) {

		var strHTML = "";

		strHTML = strHTML + "<div id='ParcelRow_" + iRow + "' class='udfrow'>"
/*
		if(mode == "Insert") {
			strHTML = strHTML + "	<span class='icon-nothing'>11</span>";
		} else {
			strHTML = strHTML + "	<span class='glyphicon glyphicon-remove-sign' onclick='removeParcelRow(" + iRow + ")'>222</span>";
		}
*/
		strHTML = strHTML + "	<span class='glyphicon glyphicon-remove-sign' onclick='removeParcelRow(" + iRow + ")'>&nbsp;</span>";

		<!--- Parcel value --->
		strHTML = strHTML + "Parcel ID: 	<input name='Parcelvalue' id='Parcelvalue_" + iRow + "' class='form-control two-part-large' type='text' onchange='changeParcelVal(" + iRow +",this)' Value='" + ParcelIDvalue + "'>";
		strHTML = strHTML + "</div>";

		return strHTML;
	}


	function createParcelEntryRow(iRow, Parcelvalue, Mode) {

		var strHTML = "";

		strHTML = strHTML + "<div id='ParcelRow_" + iRow + "' class='udfrow'>"
/*
		if(mode == "Insert") {
			strHTML = strHTML + "	<span class='icon-nothing'></span>";
		} else {
			strHTML = strHTML + "	<span class='glyphicon glyphicon-remove-sign' onclick='removeParcelRow(" + iRow + ")'>&nbsp;</span>";
		}
*/
		strHTML = strHTML + "	<span class='glyphicon glyphicon-remove-sign' onclick='removeParcelRow(" + iRow + ")'>&nbsp;</span>";

		<!--- Parcel value --->
		strHTML = strHTML + "Parcel ID: 	<input name='Parcelvalue' id='Parcelvalue_" + iRow + "' class='form-control two-part-large' type='text' onchange='changeParcelVal(" + iRow +",this)'>";
		strHTML = strHTML + "</div>";

		return strHTML;

	}


	function removeParcelRow(iRow) {
		var choice = confirm("Are you sure you want to delete this row?");
		if(choice == true) {
			//parent.udfData.setField( iRow, "mode", "Delete" );
			$("#ParcelRow_" + iRow).remove();
			//createLabel();
			//$('[data-toggle="tooltip"]').tooltip();
		}
	}

	function addParcelRow() {

		//parent.udfData.addRows(1);
        iNewRow = 1

		//iNewRow = parent.udfData.getRowCount() -1;
		//parent.udfData.setField( iNewRow, "Parcelvalue", "" );
		//parent.udfData.setField( iNewRow, "bl_id", bl_id );
		//parent.udfData.setField( iNewRow, "mode", "Insert" );

		var strHTML = "";
		strHTML = createParcelEntryRow(iCurrentRow, '', 'Insert');

		if(iOptionsOnModal <= 0) {
			$("#ParcelContainer").html(strHTML);
		} else {
			$("#ParcelContainer").append(strHTML);
		}

		iCurrentRow++;
		//$(".numeric").numeric();
		iOptionsOnModal++;
	}

</script>


<cfoutput>
	<div id="popParcelUDF" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-parcel-popupUDF">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Parcel ID for Item (#fn#)</h4>
	      </div>
	      <div class="modal-body" id="ParcelContainer">
	      </div>

	      <div class="modal-footer">
		  	<button id="addRowButton" class="btn btn-primary btn-add-option-udf" type="button" onclick="addParcelRow()"><i class="glyphicon glyphicon-plus-sign"></i> Add Another Parcel ID</button>
	        <button type="button"  class="btn btn-default" id="btnSave"><i class="glyphicon glyphicon-ok-sign"></i> Finish</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-land-popupUDF -->
	</div><!-- /.modal -->
</cfoutput>

<cfsetting showdebugoutput="No">
