
<cfsetting showdebugoutput="no">
<cfsetting requesttimeout="300">

<cfparam name="ParcelNumber" default="#documentParcelID#">
<cfparam name="Year" default="#Val(documentYear)#">
<cfparam name="DocumentType" default="1">	<!--- #documentType# --->
<cfparam name="DocumentID" default="#documentID#">
<cfparam name="CategoryType" default="#Val(documentCategory)#">
<cfparam name="LandType" default="1">
<cfparam name="OriginalFileName" default="#originalFilename#">
<cfparam name="FileName" default="#ObjectKey#">
<cfparam name="ObjectKey" default="#ObjectKey#">
<cfparam name="Title" default="#documentTitle#">
<cfparam name="Notes" default="#documentNote#">
<cfparam name="Notice" default="">
<cfparam name="ScanDate" default="#originalUploadDate#">
<cfparam name="UpdatedBy" default="#CLIENT.userID#">
<cfparam name="StatusFlag" default="0">
<cfparam name="blnMoveS3File" default="false">

<cfif documentID EQ "">
	<cfset documentID = 0>
</cfif>

<!---
<cfoutput>
<cfdump var="#form#">
ListLen(documentParcelID): #ListLen(documentParcelID)#<BR>
</cfoutput>
<cfabort>
--->

<!--- Check to see how many parcel ID's were submitted. Will handle the inserts differently for multiple parcels --->
<cfif ListLen(ParcelNumber) GT 1>

	<cfloop List="#ParcelNumber#" index="strParcelID">
		<!--- <cfoutput>strParcelID: #strParcelID#<BR></cfoutput> --->
		<!--- Check to see if the Document has all of the required fields entered in --->
		<cfif strParcelID NEQ "" AND Year NEQ "" AND CategoryType GT 0 AND DocumentType GT 0>
			<cfset StatusFlag = 1>
			<cfset blnMoveS3File = TRUE>
		</cfif>

		<cfquery name="qryCheckDocument" datasource="#client.dsn#">
			SELECT DocumentID, ParcelNumber
			FROM Document
			WHERE ParcelNumber = <cfqueryparam value="#strParcelID#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfif qryCheckDocument.RecordCount>
			<cfset fltDocID = DocumentID>
		<cfelse>
			<cfset fltDocID = 0>
		</cfif>

		<!--- Save the search --->
		<cfstoredproc procedure="p_SaveDocument" datasource="#client.dsn#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#fltDocID#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Left( strParcelID, 30 )#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Year#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Val(CategoryType)#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Val(DocumentType)#" null="No">

			<!---
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#DocumentType#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LandType#" null="No">
			--->
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#OriginalFileName#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FileName#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Title#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_BLOB" value="#Notes#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Notice#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_DATE" value="#ScanDate#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#UpdatedBy#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_BIT" value="#StatusFlag#" null="No">
		</cfstoredproc>

	</cfloop>

<cfelse>
	<!--- Single Parcel ID update --->

	<!--- Check to see if the Document has all of the required fields entered in --->
	<cfif ParcelNumber NEQ "" AND Year NEQ "" AND CategoryType GT 0 AND DocumentType GT 0>
		<cfset StatusFlag = 1>
		<cfset blnMoveS3File = TRUE>
	</cfif>

	<!--- Save the search --->
	<cfstoredproc procedure="p_SaveDocument" datasource="#client.dsn#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#DocumentID#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ParcelNumber#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Year#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Val(CategoryType)#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Val(DocumentType)#" null="No">

		<!---
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#DocumentType#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LandType#" null="No">
		--->
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#OriginalFileName#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FileName#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Title#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Notes#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Notice#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_DATE" value="#ScanDate#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#UpdatedBy#" null="No">
		<cfprocparam type="In" cfsqltype="CF_SQL_BIT" value="#StatusFlag#" null="No">
	</cfstoredproc>

</cfif>

<cfif blnMoveS3File>
	<!--- Document values have been entered in and now we need to move the
			document from the staging bucket to the production buket. --->

	<cfset strLocalPath = "D:\Inetpub\realcamadev\htdocs\maint\S3_Temp_files\#OriginalFileName#">
	<cfset strNewDocuemntFilename = strLocalPath>
	<cfset strContentType = "image/jpg">

	<!--- Pull the file down from the S3 Staging Bucket --->
	<cfset application.Amazons3.getFileFromS3(bucket: Application.strAmazonS33StagingBucket, objectKey: ObjectKey, localFilePath: strLocalPath) />

	<cfset privateKey = OriginalFileName />
	<cfset path = expandPath("./" & strNewDocuemntFilename) />


<!---
On-click "Save", the system shall validate the record to determine if all elements needed to create the system file name have been entered.
The elements needed to create the system file as follows:

    State FIPS Code
    County FIPS Code
    Year
    ParcelID
    CategoryID
    DocumentID

The system file name shall be structured as follows:

    STFIPS+CTYFIPS_YEAR_PARCELID_CATID_DOCID_DATE/TIME.EXT
    Example: 12089_2016_123445658910_4_6.PDF

    If true, create the system file name(s) and transmit the file to the appropriate end point bucket.*
    Else, save the tags only. The record shall remain in the listing pane until all elements needed to create the new system file name have been entered.



---->


<!---
	<cfif application.Amazons3.s3FileExists(Application.strAmazonS33StagingBucket, privateKey)>
		<cfset application.Amazons3.deleteS3File(Application.strAmazonS33StagingBucket, privateKey) />
	</cfif>
--->

	<!--- Push the file up to the Production bucket --->
	<cfset application.Amazons3.putFileOnS3(strLocalPath, strContentType, Application.strAmazonS33ProductionBucket, ObjectKey) />

	<!--- Delete the original file from the Staging Bucket --->
	<cfset application.Amazons3.deleteS3File(bucket: Application.strAmazonS33StagingBucket, objectKey: ObjectKey) />

	<!---
	<cfoutput>
		strLocalPath: #strLocalPath#<BR>
		strContentType: #strContentType#<BR>
		Application.strAmazonS33ProductionBucket: #Application.strAmazonS33ProductionBucket#<BR>
		Application.strAmazonS33StagingBucket: #Application.strAmazonS33StagingBucket#<BR>
		ObjectKey: #ObjectKey#<BR>+
		<cfabort>
	</cfoutput>
	 --->
</cfif>

<!--- Get the list of documents to process --->
<cfset qryGetDocuments = application.document.fnGetDocumentsToProcess()>


<!--- Output the data so it displays on the document listing form --->

	<cfoutput query="qryGetDocuments">
		<cfsilent>
			<cfif DocumentID NEQ "">
				<cfset strRowLink = "loadDocumentByID('#qryGetDocuments.DocumentID#','#ObjectKey#')">
			<cfelse>
				<cfset strRowLink = "loadDocument('#qryGetDocuments.Name#','#ObjectKey#','#DateFormat(DateLastModified, 'mm/dd/yyyy')# #TimeFormat(DateLastModified)#')">
			</cfif>

			<cfset qryDocumentParcelIDs = application.document.fnGetDocumentAdditionalParcelIDs(strOriginalFileName = Name)>

			<cfif qryDocumentParcelIDs.RecordCount>
				<cfset strDisplayParcelsIDs = ValueList(qryDocumentParcelIDs.ParcelNumber, "<br>")>
			<cfelse>
				<cfset strDisplayParcelsIDs = "">
			</cfif>
		</cfsilent>

		<tr onclick="#strRowLink#">
			<td>#Name#</td>
			<td>#DateFormat(DateLastModified, 'mm/dd/yyyy')# #TimeFormat(DateLastModified)#</td>
			<td>#strDisplayParcelsIDs#</td>
			<td><cfif Val(Year) GT 0>#Year#</cfif></td>
			<td>#CategoryTypeDescription#</td>
			<td>#DocumentTypeDescription#</td>
			<td>#Title#</td>
			<td align="center"><cfif Notes NEQ ""><img src="/images/search/icon_note.png" data-toggle="tooltip" data-placement="top" title="#Notes#"></cfif></td>
		</tr>

	</cfoutput>


<script>
	ClearDocumentForm();
</script>