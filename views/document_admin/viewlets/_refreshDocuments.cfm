<!--- Output the data so it displays on the document listing form --->

<cfoutput query="prc.getDocumentsToProcess">
	<cfsilent>
		<cfset strRowLink = "javascript:loadDocument('#DocumentID#','#Key#')">

		<cfif prc.getDocumentsToProcess.parcelNumbers <> "">
			<cfset strDisplayParcelsIDs = replace(prc.getDocumentsToProcess.parcelNumbers, ",", "<BR>", "ALL")>
		<cfelse>
			<cfset strDisplayParcelsIDs = "">
		</cfif>
	</cfsilent>

	<tr onclick="#strRowLink#">
		<td>#Key#</td>
		<td>#DateFormat(LastModified, 'mm/dd/yyyy')# #TimeFormat(LastModified)#</td>
		<td>#strDisplayParcelsIDs#</td>
		<td><cfif IsDefined("Year") AND Val(Year) GT 0>#Year#</cfif></td>
		<td><cfif IsDefined("CategoryTypeDescription")>#CategoryTypeDescription#</cfif></td>
		<td><cfif IsDefined("DocumentTypeDescription")>#DocumentTypeDescription#</cfif></td>
		<td><cfif IsDefined("Title")>#Title#</cfif></td>
		<td align="center"><cfif IsDefined("Notes") AND Notes NEQ ""><img src="/includes/images/search/icon_note.png" data-toggle="tooltip" data-placement="top" title="#Notes#"></cfif></td>
	</tr>
</cfoutput>


<script>
	BlockDocumentForm();
</script>