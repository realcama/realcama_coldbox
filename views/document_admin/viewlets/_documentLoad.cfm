
<cfsetting showdebugoutput="No">

<cfoutput>
<!--- Check to see if the user is clicking on a new incoming document --->
<cfif IsDefined("prc.getDocumentDataByID")>

	<cfparam name="strParcelIDToolTip" Default="">

	<!--- Load in the data for the select incoming document--->
	$("##originalFilename").val("#rc.strObjectkey#");
	$("##originalUploadDate").val("#DateFormat(prc.getAmazonS3ObjectData.Last_Modified, 'mm/dd/yyyy')# #TimeFormat(prc.getAmazonS3ObjectData.Last_Modified)#");
	$("##objectkey").val("#rc.strObjectkey#");

	<!--- Fill in the form elements with the saved data --->
	$("##documentID").val("#prc.getDocumentDataByID.DocumentID#");
	$("##documentTitle").val("#prc.getDocumentDataByID.Title#");
	$("##documentNote").val("#Left(prc.getDocumentDataByID.Notes, 100)#");
	$("##documentParcelID").val("#prc.getDocumentDataByID.parcelNumbers#");

	<cfif listLen(prc.getDocumentDataByID.parcelNumbers) EQ 1>
		var strFieldDisplay = "<input type='text' name='DisplayParcelID' id='DisplayParcelID' Value='#prc.getDocumentDataByID.parcelNumbers#' class='form-control doNotLeaveEmpty' readonly='readonly'>";
	<cfelse>
		<cfset strParcelIDToolTip = replaceNoCase( prc.getDocumentDataByID.parcelNumbers, "," , "\n" )>
		var strFieldDisplay = "<div style='text-align: center;'><div class='formTooltipHolder' data-toggle='tooltip' data-placement='top' title='#strParcelIDToolTip#'>Multiple Parcel IDs</div></div>";
	</cfif>

	$("##ParcelWrapper").html(strFieldDisplay);
	$("##ParcelWrapper").show();


	<cfif Val(prc.getDocumentDataByID.Year) NEQ 0>
		<!--- $("##documentYear").val("#prc.getDocumentDataByID.Year#"); --->
		$("##s2id_documentYear").select2("val", #prc.getDocumentDataByID.Year#);
	<cfelse>
		<!--- $("##documentYear").val("2016"); --->
		$("##s2id_documentYear").select2("val", 2016);
	</cfif>

	<cfif prc.getDocumentDataByID.CategoryType NEQ 0>
		<!--- $("##documentCategory").val("#prc.getDocumentDataByID.CategoryType#"); --->
		console.log('documentLoad (30) documentType: (' + #prc.getDocumentDataByID.CategoryType# + ')');
		$("##documentCategory").select2("val","#Val(prc.getDocumentDataByID.CategoryType)#");
		<!--- //$("##s2id_documentCategory").select2("val", #prc.getDocumentDataByID.CategoryType#); --->

		<!--- Update the Document Type Dropdown based on the saved Category Type Value --->
		UpdateDocumentType();
		<cfif prc.getDocumentDataByID.DocumentType NEQ "">
			console.log('documentLoad (37) documentType: (' + #prc.getDocumentDataByID.DocumentType# + ')');


			$("##documentType").select2("val", "#Val(prc.getDocumentDataByID.DocumentType)#");

			<!---
			$("##documentType").select2("val","6");

			//$("##documentType").select2("text", "Condominium");
			//$("##documentType").select2("#prc.getDocumentDataByID.DocumentType#");
			//$("##documentType").select2("val", "#Val(prc.getDocumentDataByID.DocumentType)#");


			$("##documentType").select2("val","#prc.getDocumentDataByID.DocumentType#");
			$("##s2id_documentType").select2("val", #prc.getDocumentDataByID.DocumentType#);
			--->
		</cfif>
	<cfelse>
		<!--- // Update the Document Type Dropdown based on the saved Category Type Value  --->
		UpdateDocumentType();
		$("##documentCategory").val('');
		<!--- //$("##s2id_documentCategory").select2("val", '');  --->
		$("##documentType").val('');
		<!--- //$("##s2id_documentType").select2("val", ''); --->
	</cfif>


<cfelse>

	<!--- Load in the data for the select incoming document--->
	$("##originalFilename").val("#rc.strObjectkey#");
	$("##originalUploadDate").val("#DateFormat(prc.getAmazonS3ObjectData.Last_Modified, 'mm/dd/yyyy')# #TimeFormat(prc.getAmazonS3ObjectData.Last_Modified)#");
	$("##objectkey").val("#rc.strObjectkey#");

</cfif>


<!--- Get the URL to the file in the Amazon S3 Bucket --->
<cfset strFileName = prc.strDocumentURL>

<!---- Document File Preview --->
<cfif FindNoCase(".pdf", strFileName)>
	<cfset strPreviewHTML = "<embed src='#strFileName#' width='100%' height='300px' />">
<cfelseif FindNoCase(".doc", strFileName)>
	<!--- <cfset strPreviewHTML = "<embed src='#strFileName#' width='100%' height='300px' />">
	<cfset strPreviewHTML = "<iframe src='http://docs.google.com/viewer?url=<?=urlencode(http://nassaucamadev.tekgroupweb.com/maint/incoming/test-doc.doc)?>&embedded=true'  style='position: absolute;width:100%; height: 100%;border: none;'></iframe>">
	<cfset strPreviewHTML = "<iframe src='http://docs.google.com/gview?url=http://nassaucamadev.tekgroupweb.com/maint/incoming/test-doc.doc&embedded=true' style='width:100%; height:300px;' frameborder='0'></iframe>">


	<cfset strPreviewHTML = "<iframe style='width:100%; height:300px;' frameborder='0'><cfcontent type = 'application/msword' file = '#strFileName#' deleteFile = 'no'></iframe>">
--->

<cfset strPreviewHTML = "<iframe src='http://docs.google.com/gview?url=#strFileName#&embedded=true' style='width:100%; height:300px;' frameborder='0'></iframe>">

<!---
<cfset strPreviewHTML = "<iframe src='http://docs.google.com/gview?url=#strFileName#&embedded=true' width='100%' height='800' frameborder='0'></iframe>">

<cfset strPreviewHTML = "<iframe src='http://docs.google.com/viewerng/viewer?url=#strFileName#&embedded=true' style='width:100%; height:300px;' frameborder='0'></iframe>">
--->


<cfelse>
	<cfset strPreviewHTML = "<iframe width='100%' height='300px' src='#strFileName#' alt='#strFileName#' /></iframe>">

</cfif>

<!--- Used for debugging the docuemtn data --->
<!---
<cfset strDebugData =
	"documentID: #qryDocumentInfo.DocumentID#<BR>originalFilename: #qryDocumentInfo.Name#<BR>originalUploadDate: #qryDocumentInfo.DateLastModified#<BR>documentParcelID: #qryDocumentInfo.ParcelNumber#<BR>CategoryType: #qryDocumentInfo.CategoryType#<BR>documentType: #qryDocumentInfo.DocumentType#<BR>DocumentYear: #qryDocumentInfo.Year#<BRdocumentTitle: #qryDocumentInfo.Title#<BR>">
$("##document-preview-image").html("#strDebugData#");
 --->

<!--- $("##document-preview-image").html("strFileName: #strFileName# <BR>#strPreviewHTML#");
$("##document-preview-image").html("#strPreviewHTML#");
 --->
 $("##document-preview-image").html("#strPreviewHTML#");

</cfoutput>
