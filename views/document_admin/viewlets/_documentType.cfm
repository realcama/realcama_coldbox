
<cfoutput>

<cfsavecontent variable="documentTypes">
	<select name="documentType" id="documentType" size="1" class="form-control two-items-on-row select2-2piece-new doNotLeaveEmpty">
		<option value="" data-val="" data-desc="ALL">ALL</option>
		<cfloop query="prc.getDocumentTypes">
			<option value="#documentCodeID#" data-desc="#codeDescription#">#codeDescription#</option>
		</cfloop>
	</select>
</cfsavecontent>

$(".documentType").html("#documentTypes#");

</cfoutput>

<!---
	<select name="documentType" id="documentType" size="1" class="form-control two-items-on-row select2-2piece-new doNotLeaveEmpty">
		<option value="" data-val="" data-desc="ALL">Select One</option>
	</select>
--->