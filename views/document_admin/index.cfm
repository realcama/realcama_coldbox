
<cfsetting showdebugoutput="No">

<!---
<cfoutput>
	#addAsset('/includes/js/documentmanagement.js')#
</cfoutput>
--->

<div class="siteContent container-fluid">
	<div class="documentMaintence row container-fluid">
		<div id="document-left-box" class="document-left-box col-md-3">
			<cfoutput>
			<form action="documentList.cfm" method="post" id="frmDocumentInfo" name="frmDocumentInfo">
				<div class="row">
					<div class="col-xs-12">
						<label class="formatted-data-label-white">Original Filename</label><br/>
						<div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected"><input type="text" name="originalFilename" id="originalFilename" class="form-control" readonly="readonly"></div>
					</div>

					<div class="col-xs-12">
						<label class="formatted-data-label-white">Upload Date/Time</label><br/>
						<div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected"><input type="text" name="originalUploadDate" id="originalUploadDate" class="form-control" readonly="readonly"></div>
					</div>

					<div class="col-xs-12">
						<p>
					</div>
					<div class="col-xs-12">

						<span id="InputsWrapper"><label class="formatted-data-label-white">* Parcel IDs</label><span><br/>
						<input type="hidden" name="documentParcelID" id="documentParcelID" class="form-control doNotLeaveEmpty">
						<span id="ParcelWrapper"></span>
						<!--- <div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected"><input type="text" name="documentParcelID" id="documentParcelID" class="form-control doNotLeaveEmpty"></div> --->
					</div>
					<div class="col-xs-12" id="AddMoreParcelID">
						<span id="AddMoreParcelID"><a onclick="doParcelIDPopup()" rel="tooltip" title="Click to add another Parcel ID">Add Parcel IDs</a><BR></span>
					</div>
					<!---
					<div class="col-xs-12 building-button-row">
						<a href="##" id="AddMoreParcels" data-toggle="modal" data-target="##bannerformmodal">Add another Parcel ID</a><br/>
						<a href="##" data-mode="cloneLine" data-toggle="modal" data-target="##modalCloneLine" data-type="land" class="dsp-mode link" id="cloneLine"><i class="icon glyphicon glyphicon-copy"></i> Clone A Land Line</a>
						<span onclick="doParcelIDPopup()" rel="tooltip" title="Click to add another Parcel ID">Add another Parcel ID</span>
					</div>
					--->
					<div class="col-xs-12">
						<label class="formatted-data-label-white">* Year</label><br/>
						<div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected">
							<select name="documentYear" id="documentYear" size="1" class="form-control two-items-on-row select2-2piece-new doNotLeaveEmpty">
								<option value="" data-val="" data-desc="ALL">ALL</option>
								<cfloop index="iYear" from="#year(now())#" to="#year(now())-9#" step="-1">
									<option value="#iYear#" data-val="" data-desc="#iYear#" <CFIF client.currentTaxYear EQ iYear>selected</CFIF>>#iYear#</option>
								</cfloop>
							</select>
						</div>
					</div>
					<div class="col-xs-12">
						<label class="formatted-data-label-white">* Category</label><br/>
						<div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected">
							<select name="documentCategory" id="documentCategory" size="1" class="form-control two-items-on-row select2-2piece-new doNotLeaveEmpty" onchange="UpdateDocumentType();">
								<option value="" data-val="" data-desc="ALL">Select One</option>
								<cfloop Query="prc.getDocumentCategoryTypes">
									<option value="#DocumentCodeID#" data-desc="#CodeDescription#">#CodeDescription#</option>
								</cfloop>
							</select>
						</div>
					</div>
					<div class="col-xs-12">
						<label class="formatted-data-label-white">* Document Type</label><br/>
						<div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected">
							<select name="documentType" id="documentType" size="1" class="form-control two-items-on-row select2-2piece-new doNotLeaveEmpty">
								<option value="" data-val="" data-desc="ALL">Select One</option>
								<option value="1" data-val="" data-desc="1">1</option>
								<option value="2" data-val="" data-desc="2">2</option>
								<option value="3" data-val="" data-desc="3">3</option>
								<option value="4" data-val="" data-desc="4">4</option>
								<option value="5" data-val="" data-desc="5">5</option>
								<option value="6" data-val="" data-desc="6">6</option>
								<option value="7" data-val="" data-desc="7">7</option>
								<option value="8" data-val="" data-desc="8">8</option>
								<option value="9" data-val="" data-desc="9">9</option>
								<option value="10" data-val="" data-desc="10">10</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12">
						<label class="formatted-data-label-white">Title / Description</label><br/>
						<div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected"><input type="text" name="documentTitle" id="documentTitle" class="form-control"></div>
					</div>
					<div class="col-xs-12">
						<label class="formatted-data-label-white">Note</label><br/>
						<div class="formTooltipHolder" data-toggle="tooltip" data-placement="top" title="Option is not available until a document is selected"><textarea rows="4" Cols="16" Name="documentNote" id="documentNote" class="form-control"></textarea></div>
						<p>
					</div>
					<div class="col-xs-12 pull-right">
						<span class="pull-right"><input type="button" onclick="saveDocumentData()" value="Save" class="search-button-text"></span>
					</div>
					<div class="col-xs-12">
						<label class="formatted-data-label-white">* Required Field</label>
						<input type="hidden" name="documentID" id="documentID" class="form-control">
						<input type="hidden" name="objectkey" id="objectkey" class="form-control">
					</div>
				</div>
			</form>

			</cfoutput>
		</div> <!--- END div class="document-left-box col-md-3" --->
		<div class="col-md-9">
			<div class="row">
				<div class="document-preview col-md-12">
					<P>
					<div id="document-preview-image">
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4><cfoutput>Document Listing for #prc.VendorName#</cfoutput></h4>
					<div class="container-border container-spacer">
						<div id="expander-holders" class="panel panel-default">
							<table class="table table-bordered table-hover">
							<thead Class="documentListHeader">
								<tr>
									<th>Filename</th>
									<th>Uploaded</th>
									<th>Parcel ID</th>
									<th>Year</th>
									<th>Category</th>
									<th>Doc Type</th>
									<th>Title / Description</th>
									<th>Note</th>
								</tr>
							</thead>

							<tbody id="document-search-results" Class="documentList">
							<div id="selectedDocument">
								<cfoutput query="prc.getDocumentsToProcess">
									<cfsilent>
										<cfset strRowLink = "javascript:loadDocument('#DocumentID#','#Key#')">

										<cfif prc.getDocumentsToProcess.parcelNumbers <> "">
											<cfset strDisplayParcelsIDs = replace(prc.getDocumentsToProcess.parcelNumbers, ",", "<BR>", "ALL")>
										<cfelse>
											<cfset strDisplayParcelsIDs = "">
										</cfif>
									</cfsilent>

									<tr onclick="#strRowLink#">
										<td>#Key#</td>
										<td>#DateFormat(LastModified, 'mm/dd/yyyy')# #TimeFormat(LastModified)#</td>
										<td>#strDisplayParcelsIDs#</td>
										<td><cfif IsDefined("Year") AND Val(Year) GT 0>#Year#</cfif></td>
										<td><cfif IsDefined("CategoryTypeDescription")>#CategoryTypeDescription#</cfif></td>
										<td><cfif IsDefined("DocumentTypeDescription")>#DocumentTypeDescription#</cfif></td>
										<td><cfif IsDefined("Title")>#Title#</cfif></td>
										<td align="center"><cfif IsDefined("Notes") AND Notes NEQ ""><img src="/includes/images/search/icon_note.png" data-toggle="tooltip" data-placement="top" title="#Notes#"></cfif></td>
									</tr>
								</cfoutput>
							</div>
							</tbody>
							</table>
						</div> <!--- END div id="expander-holders" class="panel panel-default" --->
					</div>	<!--- END div class="container-border container-spacer" --->
				</div> <!--- END div class="col-md-12" --->
			</div> <!--- END div class="col-md-12" --->
		</div> <!--- END div class="row" --->
	</div>	<!--- END div class="col-md-9" --->

<!---
	<cfdump var="#qryGetDocuments#" Label="qryGetDocuments" expand="false">
	<cfdump var="#Application#" Label="Application" expand="false">
	<cfdump var="#Client#" Label="Client" expand="false">
--->
</div>	<!--- <div class="siteContent container-fluid"> --->
<div id="response-div"></div>
<P>


