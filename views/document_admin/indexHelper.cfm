

<script type="text/javascript">
	$(document).ready(function(){

		BlockDocumentForm();
		//$("#frmDocumentInfo :input").prop("disabled", true);
		//$('.formTooltipHolder').tooltip('enable')  // (Re-)enable tooltips


		formatSelectsToSelect2();

		//$("#documentCategory").select2("val","2");
		//$("#documentType").select2("val","7");

		//$("#frmDocumentInfo :input").removeProp("disabled");
		//$("#frmDocumentInfo").prop('title', '');
		//$('.formTooltipHolder').tooltip('disable'); // Turn off the tooltips in the form

	});
</script>


<script>

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format1Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}


function formatSelectsToSelect2(){
	$('.select2-2piece-new').select2({
		minimumResultsForSearch: -1,
		formatResult: format1Piece,
		formatSelection: format1Piece,
		escapeMarkup: function(m) { return m; }
	});

	$( ".select2-2piece-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-new").addClass("select2-2piece");
	});
}


function loadDocument(intDocumentID, strObjectKey) {

	console.log('loadDocument 60:' + strObjectKey);
	ClearDocumentForm();
	console.log('loadDocument 62:');

	<cfoutput>
	if ( intDocumentID == '') {

		<!---
		var url = '#event.buildLink( "document_admin/loadDocument" )#?strObjectKey=' + strObjectKey;
		console.log('loadDocument 65: ' + url);


		$.ajax({
			type: "get",
			cache: false,
			async: false, //This variable makes the function wait until a response has been returned from the page
			url: "#event.buildLink( "document_admin/loadDocument" )#",
			data: {
				strObjectKey: strObjectKey
			}, // all form fields
			success: function (data) {
       			eval( data );
       		} // success
	    }); // ajax
	     --->

		$.ajax({
			url: "#event.buildLink( "document_admin/loadDocument" )#",
			type: "post",
			cache: false,
			data: {
				strObjectKey: strObjectKey
			},
			success: function(data){
				eval(data);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){

			}
		});


	} else {
		<!---
		var url = '#event.buildLink( "document_admin/loadDocument" )#?strObjectKey=' + strObjectKey + '&DocumentID=' + intDocumentID;
		console.log('loadDocument 65: ' + url);
		 --->

		$.ajax({
			url: "#event.buildLink( "document_admin/loadDocument" )#",
			type: "post",
			cache: false,
			data: {
				strObjectKey: strObjectKey,
				documentID: intDocumentID
			},
			success: function(data){
				eval(data);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){

			}
		});

	}
	</cfoutput>

	EnableDocumentForm();

}

function saveDocumentData() {

	var data = $('#frmDocumentInfo').serializeArray();
/*
	$.post("include/documentSave.cfm", data, function(data, status){
        $( "#document-search-results" ).html( data );
    });
*/

	$.ajax({
			url:'/index.cfm/document_admin/documentSave',
			type: "post",
			async: false, //This variable makes the function wait until a response has been returned from the page
			data: data,
			success: function( data ) {
				$( "#document-search-results" ).html( data );
			}
	});

	//BlockDocumentForm();

}

function ClearDocumentForm() {
	console.log('ClearDocumentForm (148):');
	$("#documentID").val('');
	$("#originalFilename").val('');
	$("#originalUploadDate").val('');
	$("#documentParcelID").val('');
	console.log('ClearDocumentForm (153):');
	$("#documentParcelID").hide();
	console.log('ClearDocumentForm (155):');
	$("#documentYear").select2("val","2017");
	$("#documentCategory").select2("val","");
	$("#documentType").select2("val","");

	$("#documentTitle").val('');
	$("#documentNote").val('');
	$("#document-preview-image").html("");
	$("#ParcelWrapper").html("&nbsp;");

	$('#AddMoreParcelID').show();
	console.log('ClearDocumentForm (166):');
}


function BlockDocumentForm() {
	console.log('BlockDocumentForm (171):');
	ClearDocumentForm();
	console.log('BlockDocumentForm (173):');
	$("#frmDocumentInfo :input").prop("disabled", true);

	//$('#AddMoreParcelID').show();
	$('.formTooltipHolder').tooltip('enable')  // (Re-)enable tooltips
	console.log('BlockDocumentForm (181):');
}


function EnableDocumentForm() {
	console.log('EnableDocumentForm (183):');
	//$(".form-control").prop('disabled', function (_, val) { return ! val; });
	$("#frmDocumentInfo :input").prop("disabled", false);
	$("#frmDocumentInfo").prop('title', '');
	$('.formTooltipHolder').tooltip('disable'); // Turn off the tooltips in the form
	console.log('EnableDocumentForm (188):');
}


function UpdateDocumentType() {
	var categoryID = $('#documentCategory option:selected').text();
	console.log ( "UpdateDocumentType (252): categoryID: " + categoryID );

	<cfoutput>
	var url = '#event.buildLink( "document_admin/documentType" )#?categoryID=' + encodeURI(categoryID);
	console.log('UpdateDocumentType (198): ' + url);
	</cfoutput>

	$( '#documentType').load( url, function( data ) {} );

	formatSelectsToSelect2();

/*
	$.ajax({
		type : 'get',
		url:'include/documentType.cfm?categoryID='+categoryID

	}).success(function(response){

		$('#documentType').html(response);	//response as html for Document Type drop down.

	}).error(function(){

		alert('Error Occured');

	});
*/
}


function doParcelIDPopup() {
	var strFileName = $("#originalFilename").val();
	console.log ("strFileName: " + strFileName);

	$.fancybox ({
		'padding' : 0,
		'closeBtn': false,
		'type': 'ajax',
		//'href': 'include/showDocumentParcelIDPopup.cfm?fn=' + strFileName,

		'href': '/index.cfm/document_admin/showParcelIDPopup/?fn=' + strFileName,
		helpers : { overlay : { locked : false } } /* this prevents screen from shifting */
	});

}

</script>
