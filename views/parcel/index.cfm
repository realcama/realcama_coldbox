<CFSET prc.qryQualificationCodes = QueryNew("code,desc")>
<CFSET queryAddRow(prc.qryQualificationCodes,4)>
<CFSET querySetCell(prc.qryQualificationCodes,"code","Q",1)>
<CFSET querySetCell(prc.qryQualificationCodes,"code","U",2)>
<CFSET querySetCell(prc.qryQualificationCodes,"code","V",3)>
<CFSET querySetCell(prc.qryQualificationCodes,"code","C",4)>
<CFSET querySetCell(prc.qryQualificationCodes,"desc","Qualified",1)>
<CFSET querySetCell(prc.qryQualificationCodes,"desc","Unqualified",2)>
<CFSET querySetCell(prc.qryQualificationCodes,"desc","Verified",3)>
<CFSET querySetCell(prc.qryQualificationCodes,"desc","Confirmed",4)>

<CFSET prc.qryImprovementCodes = QueryNew("code,desc")>
<CFSET queryAddRow(prc.qryImprovementCodes,2)>
<CFSET querySetCell(prc.qryImprovementCodes,"code","V",1)>
<CFSET querySetCell(prc.qryImprovementCodes,"code","I",2)>
<CFSET querySetCell(prc.qryImprovementCodes,"desc","Vacant",1)>
<CFSET querySetCell(prc.qryImprovementCodes,"desc","Improved",2)>


<cfoutput>

<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		<CFSET showTabOptions = "parcel">
		#renderView('_templates/miniNavigationBar')#
		Parcel&nbsp;&nbsp;
	</div>

	<div id="expander-holders" class="panel panel-default">
		#renderView('parcel/_templates/parcelInformation')#
		#renderView('parcel/_templates/parcelSitus')#
		#renderView('parcel/_templates/parcelSales')#
		#renderView('parcel/_templates/parcelLegal')#
		#renderView('parcel/_templates/parcelExemptions')#
	</div>
</div>

<script type="text/javascript">
	var ParcelID = "#rc.parcelID#";
	var taxYear = "#rc.taxYear#"
	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qryParcelInfo#"
		TOPLEVELVARIABLE="parcelData">

	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qrySitus#"
		TOPLEVELVARIABLE="situsData">

	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qryParcelSales#"
		TOPLEVELVARIABLE="salesData">

	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qryParcelMultiSales#"
		TOPLEVELVARIABLE="multiSalesData">

	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qryParcelLegalInfo#"
		TOPLEVELVARIABLE="legalDescriptionData">

	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qryParcelExemptionInfo#"
		TOPLEVELVARIABLE="exemptionData">

	<CFWDDX ACTION="CFML2JS"
		INPUT="#prc.qryParcelExemptionApplicationInfo#"
		TOPLEVELVARIABLE="exemptionApplicationData">
	</script>
#addAsset('/includes/js/header.js,/includes/js/parcel.js,/includes/js/formatSelect.js')#
</cfoutput>