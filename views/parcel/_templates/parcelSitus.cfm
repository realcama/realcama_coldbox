<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading2" aria-expanded="false">						
		<table class="land-line-summary">
		<tr href="##collapse2" class="section-bar-expander" data-parent="heading2" data-toggle="collapse" aria-controls="collapse2" onclick="toggleChevronChild(2)">
			<td width="95%" class="section-bar-label">Situs Information</td>						
			<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_2"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
		<div id="view_mode_item_2">
			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Situs Address<br />
					(same as mailing address)</span>
				</div>
				<div class="col-xs-3 databox">	
					<br/><span class="formatted-data-values">Yes</span>					
				</div>
				<div class="col-xs-3 databox">	
																	
				</div>
				<div class="col-xs-3 databox">	
											
				</div>	
				<div class="clearfix"></div>
				
				
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Number</span><br />
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qrySitus.SitusHouseNumber#
						<CFELSE>
							*******
						</CFIF>					
					</span>
				</div>						
				
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Dir</span><br />
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qrySitus.SitusStreetDirection#
						<CFELSE>
							******
						</CFIF>
					</span>
				</div>
				
				<div class="col-xs-4 databox">	
					<span class="formatted-data-label">Street</span><br />
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qrySitus.SitusStreetName#
						<CFELSE>
							************************
						</CFIF>
					</span>	
				</div>
				<div class="col-xs-2 databox">	
					<span class="formatted-data-label">Type</span><br />
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qrySitus.SitusStreetType#
						<CFELSE>
							******
						</CFIF>
					</span>											
				</div>					
				<div class="col-xs-2 databox">	
					<span class="formatted-data-label">Post</span><br />	
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qrySitus.SitusAptNumber#
						<CFELSE>
							*****
						</CFIF>
					</span>				
				</div>	
				<div class="clearfix"></div>
				
				
				
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">City</span><br />
					<span class="formatted-data-values">#prc.qrySitus.SitusCity#</span>
				</div>					
				
				<div class="col-xs-4 databox">	
					<span class="formatted-data-label">State</span><br />
					<span class="formatted-data-values">#prc.qrySitus.SitusState#</span>
				</div>
				<div class="col-xs-4 databox">	
					<span class="formatted-data-label">Zip Code</span><br />
					<span class="formatted-data-values">#prc.qrySitus.SitusZip#-#prc.qrySitus.SitusZip4#</span>
				</div>			
				<div class="clearfix"></div>
						
				<!--- 
				<div class="col-xs-12 databox">	
					<span class="formatted-data-label">Country</span><br />
					<span class="formatted-data-values"></span>
				</div>					
				 --->
				
				<div class="col-xs-12 databox">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(2);"><img src="/includes//images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
		
		<div id="edit_mode_item_2" style="display:none;">
			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Situs Address<br />
					(same as mailing address)</span>
				</div>
				<div class="col-xs-3 databox">	
					<br/>
					<input type="radio" name="situsAddressSame" class="parcel-situs-field" data-original="" value="Y"> Yes  &nbsp;&nbsp;
					<input type="radio" name="situsAddressSame" value="N"> No	
				</div>
				<div class="col-xs-3 databox">	
																	
				</div>
				<div class="col-xs-3 databox">	
											
				</div>	
				<div class="clearfix"></div>
							
				
				
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Number</span><br />
					<input type="text" name="situshousenumber" class="form-control parcel-situs-field" data-original="#prc.qrySitus.SitusHouseNumber#" value="#prc.qrySitus.SitusHouseNumber#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Dir</span><br />
					<input type="text" name="situsstreetdirection" class="form-control parcel-situs-field" data-original="#prc.qrySitus.SitusStreetDirection#" value="#prc.qrySitus.SitusStreetDirection#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >
				</div>
				
				<div class="col-xs-4 databox">	
					<span class="formatted-data-label">Street</span><br />
					<input type="text" name="situsstreetname" class="form-control parcel-situs-field" data-original="#prc.qrySitus.SitusStreetName#" value="#prc.qrySitus.SitusStreetName#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >					
				</div>
				<div class="col-xs-2 databox">	
					<span class="formatted-data-label">Type</span><br />
					<input type="text" name="situsstreettype" class="form-control parcel-situs-field" data-original="#prc.qrySitus.SitusStreetType#" value="#prc.qrySitus.SitusStreetType#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >												
				</div>					
				<div class="col-xs-2 databox">	
					<span class="formatted-data-label">Post</span><br />	
					<input type="text" name="situsaptnumber" class="form-control parcel-situs-field" data-original="#prc.qrySitus.SitusAptNumber#" value="#prc.qrySitus.SitusAptNumber#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >					
				</div>	
				<div class="clearfix"></div>
				
				
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">City</span><br />
					<input type="text" name="situscity" class="form-control parcel-situs-field" data-original="#prc.qrySitus.SitusCity#" value="#prc.qrySitus.SitusCity#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >
				</div>					
				
				<div class="col-xs-4 databox">	
					<span class="formatted-data-label">State</span><br />
					<select data-tags="true"  class="form-control two-items-on-row select2-2piece-new parcel-situs-field" data-original="" name="situsstate" <!----  onchange="changeNeighborhood('situsData',#prc.qrySitus.CurrentRow-1#,this)" --->>
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getStates">
							<option value="#prc.getStates.id#" data-val="#prc.getStates.field_abbrev#" data-desc="#prc.getStates.field_option#" data-adjval="#prc.getStates.field_abbrev#"  <cfif prc.getStates.field_abbrev EQ prc.qrySitus.SitusState> selected</cfif> >#prc.getStates.field_abbrev#: #prc.getStates.field_option#</option>
						</cfloop>
					</select>	
				</div>
				<div class="col-xs-4 databox">	
					<span class="formatted-data-label">Zip Code</span><br />
					<input type="text" name="situszip" class="form-control paszip parcel-situs-field" data-original="#prc.qrySitus.SitusZip#" value="#prc.qrySitus.SitusZip#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >
					-
					<input type="text" name="situszip4" class="form-control paszip4 parcel-situs-field" data-original="#prc.qrySitus.SitusZip4#" value="#prc.qrySitus.SitusZip4#" onblur="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" onchange="changeVal('situsData',#prc.qrySitus.CurrentRow-1#,this)" >
				</div>					
				<!--- 
				<div class="col-xs-12 databox">	
					<span class="formatted-data-label">Country</span><br />
					<select data-tags="true"  class="form-control two-items-on-row select2-2piece-new parcel-situs-field" data-original="" name="darf"  <!---- onchange="changeNeighborhood('situsData',#prc.qrySitus.CurrentRow-1#,this)" --->>
						<option value="" data-val="" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="qryCountries">
							<option value="#qryCountries.id#" data-val="" data-desc="#qryCountries.field_option#" data-adjval="#qryCountries.field_abbrev#" <cfif 2 EQ 1> selected</cfif> >#qryCountries.field_abbrev#: #qryCountries.field_option#</option>
						</cfloop>
					</select>
				</div>
				 --->
				 
				<div class="col-xs-12 databox">						
					<div class="land-part-action-buttons">
						<a href="javascript:cancelEverything()"><img src="/includes//images/v2/cancel.png" alt="Cancel" border="0"></a>
						&nbsp;&nbsp;
						<a href="javascript:saveWDDX('situs',0)"><img src="/includes//images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
					</div>
				</div>
			</div>	
		</div>				
	</div>
</cfoutput>
