<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading1" aria-expanded="false">
		<table class="land-line-summary">
		<tr href="##collapse1" class="section-bar-expander" data-parent="heading1" data-toggle="collapse" aria-controls="collapse1" onclick="toggleChevronChild(1)">
			<td width="95%" class="section-bar-label">Parcel Information</td>
			<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_1"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse1" class="panel-collapse collapse <!--- in --->" role="tabpanel" aria-labelledby="heading1">
		<div id="view_mode_item_1">
			<div class="fix-bs-row row">

				<div class="col-xs-5 databox">
					<span class="formatted-data-label">Owner Name</span><br />
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qryParcelInfo.OwnerName#
						<CFELSE>
							************************
						</CFIF>
					</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Confidential</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.confidential#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Condo Complex Number</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.CondoID#</span>
				</div>
				<div class="col-xs-2 databox"></div>
				<div class="clearfix"></div>


				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Secondary  Name</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.Ownername2#<!--- #prc.qryParcelInfo.owflname# ---></span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Subdivision</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.Subdivision#: #prc.qryParcelInfo.SubDivisionCodeDescription#</span>
				</div>

				<div class="clearfix"></div>


				<div class="col-xs-4">
					<span class="formatted-data-label multiple-line-fix">Mailing Address 1</span><br />
					<span class="formatted-data-values multiple-line-fix">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qryParcelInfo.ownerAddress1#
						<CFELSE>
							************************
						</CFIF>
						<!--- #prc.qryParcelInfo.PAOHOUSE# --->
					</span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Mailing Address 2</span><br />
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qryParcelInfo.ownerAddress2#
						<CFELSE>
							************************
						</CFIF>
					</span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Mailing Address 3</span><br />
					<span class="formatted-data-values">
						<CFIF prc.qryParcelInfo.confidential EQ "N">
							#prc.qryParcelInfo.ownerAddress3#
						<CFELSE>
							************************
						</CFIF>
					</span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">City</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.ownerCity#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">State</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.ownerState#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip Code</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.ownerZip#<CFIF LEN(TRIM(prc.qryParcelInfo.ownerZip4)) NEQ 0>-#prc.qryParcelInfo.ownerZip4#</CFIF></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Country</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.countryDescription#</span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">DBA (Doing Business As)</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.DBAName#</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Tax District</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.taxDistrictCode#<!---: #prc.qryParcelInfo.taxDistrictCodeDescription#---></span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">SOH</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.asflresoh#</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Non Res Cap</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.asflretagl#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Market Area</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.MarketAreaCode#</span>
				</div>
				<div class="clearfix"></div>



				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Primary Use/Description</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.UseCode#: #prc.qryParcelInfo.UseCodeDescription#</span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Neighborhood</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.NeighborhoodCode#: #prc.qryParcelInfo.neighborhoodCodeDescription#</span>
				</div>

				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Exemption Renewal</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.asflrenew#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Ag Renewal</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.asflrenewa#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Just Value Change Code</span><br />
					<span class="formatted-data-values">#prc.qryParcelInfo.pvjvchgcd#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Date</span><br />
					<span class="formatted-data-values">#dateformat(now(),'mm/dd/yyyy')#</span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-12 databox">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>

		</div>

		<div id="edit_mode_item_1" style="display:none;">
			<div class="row fix-bs-row">

				<div class="col-xs-5 databox">
					<span class="formatted-data-label">Owner Name</span><br />
					<input type="text" name="ownername" class="form-control parcel-info-field" value="#prc.qryParcelInfo.OwnerName#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Confidential</span><br />
					<input type="radio" name="confidential" class="parcel-info-field" value="Y" <CFIF prc.qryParcelInfo.confidential EQ "Y">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> Yes  &nbsp;&nbsp;
					<input type="radio" name="confidential" class="parcel-info-field" value="N" <CFIF prc.qryParcelInfo.confidential EQ "N">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> No
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Condo Complex Number</span><br />
					<input type="text" name="condoID" class="form-control parcel-info-field" value="#prc.qryParcelInfo.CondoID#" readonly>
				</div>

				<div class="col-xs-2 databox">
					<button id="modalPop" class="btn btn-inverse btn-xs parcel-info-add-owner">Add Additional<br/>Owner</button>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Secondary Name</span><br />
					<input type="text" name="ownername2" class="form-control parcel-info-field"value="#prc.qryParcelInfo.Ownername2#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Subdivision</span><br/>
					<select name="subdivision" id="subdivision" size="1" class="form-control select2-2piece-new" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
						<option value="" data-val="" data-desc="ALL">ALL</option>
						<cfloop query="prc.getSubdivisions">
							<option value="#prc.getSubdivisions.SubDivisionCode#" data-val="#prc.getSubdivisions.SubDivisionCode#" <CFIF prc.qryParcelInfo.Subdivision EQ prc.getSubdivisions.SubDivisionCode>selected</CFIF> data-desc="#prc.getSubdivisions.SubDivisionCodeDescription#">#prc.getSubdivisions.SubDivisionCode#: #prc.getSubdivisions.SubDivisionCodeDescription#</option>
						</cfloop>
					</select>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Mailing Address 1</span><br />
					<input type="text" name="ownerAddress1" class="form-control parcel-info-field" value="#prc.qryParcelInfo.ownerAddress1#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Mailing Address 2</span><br />
					<input type="text" name="ownerAddress2" class="form-control parcel-info-field" value="#prc.qryParcelInfo.ownerAddress2#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Mailing Address 3</span><br />
					<input type="text" name="ownerAddress3" class="form-control parcel-info-field" value="#prc.qryParcelInfo.ownerAddress3#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">City</span><br />
					<input type="text" name="ownerCity" class="form-control parcel-info-field" value="#prc.qryParcelInfo.ownerCity#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">State</span><br />
					<select data-tags="true" row="#prc.qryParcelInfo.CurrentRow-1#" class="form-control select2-2piece-new" name="ownerState" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getStates">
							<option value="#prc.getStates.id#" data-val="#prc.getStates.field_abbrev#" data-desc="#prc.getStates.field_option#" data-adjval="#prc.getStates.field_abbrev#" <cfif prc.qryParcelInfo.ownerState EQ prc.getStates.field_abbrev> selected</cfif>>#prc.getStates.field_abbrev#: #prc.getStates.field_option#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip Code</span><br />
					<input type="text" name="ownerZip" class="form-control parcel-info-field" value="#prc.qryParcelInfo.ownerZip#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Country</span><br />
					<select data-tags="true" row="#prc.qryParcelInfo.CurrentRow-1#" class="form-control select2-2piece-new" name="ownerCountry" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
						<option value="" data-val="" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getCountries">
							<option value="#prc.getCountries.id#" data-val="" data-desc="#prc.getCountries.field_option#" data-adjval="#prc.getCountries.field_abbrev#" <cfif prc.qryParcelInfo.ownerCountry EQ prc.getCountries.id> selected</cfif>>#prc.getCountries.field_abbrev#: #prc.getCountries.field_option#</option>
						</cfloop>
					</select>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">DBA</span> <span class="minor-label">(Doing Business As)</span>*<br />
					<input type="text" name="DBAName" class="form-control parcel-info-field" value="#prc.qryParcelInfo.DBAName#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Tax District</span><br />
					<input type="text" name="taxDistrictCode" class="form-control parcel-info-field" value="#prc.qryParcelInfo.taxDistrictCode#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">SOH</span><br />
					<input type="radio" name="asflresoh" value="Y" class="parcel-info-field" <CFIF prc.qryParcelInfo.asflresoh EQ "Y">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> Yes &nbsp;&nbsp;
					<input type="radio" name="asflresoh" value="N" class="parcel-info-field" <CFIF prc.qryParcelInfo.asflresoh EQ "N">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> No
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Non Res Cap</span><br />
					<input type="radio" name="asflretagl" value="Y" class="parcel-info-field" <CFIF prc.qryParcelInfo.asflretagl EQ "Y">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> Yes &nbsp;&nbsp;
					<input type="radio" name="asflretagl" value="N" class="parcel-info-field" <CFIF prc.qryParcelInfo.asflretagl EQ "N">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> No
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Market Area*</span><br />
					<input type="text" name="marketareacode" class="form-control parcel-info-field" value="#prc.qryParcelInfo.MarketAreaCode#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Primary Use/Description</span><br />
					<select data-tags="true" row="#prc.qryParcelInfo.CurrentRow-1#" class="form-control select2-2piece-new" name="UseCode" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select...">Select...</option>
						<cfloop query="prc.getPrimaryUse">
							<option value="#prc.getPrimaryUse.useCode#" data-val="#prc.getPrimaryUse.useCode#" data-desc="#prc.getPrimaryUse.UseCodeDescription#" <cfif prc.qryParcelInfo.useCode EQ prc.getPrimaryUse.useCode> selected</cfif>>#prc.getPrimaryUse.useCode#: #prc.getPrimaryUse.useCodeDescription#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Neighborhood</span><br />
					<select data-tags="true" class="form-control select2-3piece-new parcel-info-field" name="neighborhoodcode" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)" row="#prc.qryParcelInfo.CurrentRow-1#">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getNeighborhoods">
							<option value="#prc.getNeighborhoods.nbhdcd#" data-val="#prc.getNeighborhoods.nbhdcd#" data-desc="#prc.getNeighborhoods.nbhdds#" data-adjval="#NumberFormat(prc.getNeighborhoods.nbhdfa,'9.999')#" <cfif prc.qryParcelInfo.NeighborhoodCode EQ prc.getNeighborhoods.nbhdcd> selected</cfif>>#prc.getNeighborhoods.nbhdcd#: #prc.getNeighborhoods.nbhdds#</option>
						</cfloop>
					</select>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Exemption Renewal</span><br />
					<input type="radio" name="asflrenew" class="parcel-info-field" value="Y" <CFIF prc.qryParcelInfo.asflrenew EQ "Y">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> Yes  &nbsp;&nbsp;
					<input type="radio" name="asflrenew" class="parcel-info-field" value="N" <CFIF prc.qryParcelInfo.asflrenew EQ "N">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> No
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Ag Renewal</span><br />
					<input type="radio" name="asflrenewa" class="parcel-info-field" value="Y" <CFIF prc.qryParcelInfo.asflrenewa EQ "Y">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> Yes  &nbsp;&nbsp;
					<input type="radio" name="asflrenewa" class="parcel-info-field" value="N" <CFIF prc.qryParcelInfo.asflrenewa EQ "N">checked</CFIF> onchange="changeRadioVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)"> No
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Just Value Change Code</span><br />
					<input type="text" name="pvjvchgcd" class="form-control parcel-info-field" value="#prc.qryParcelInfo.pvjvchgcd#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Date*</span><br />
					<input type="text" name="lafront" class="form-control datefield parcel-info-field" <!---- value="#NumberFormat(prc.qryParcelInfo.lafront,"0.000")#" onchange="changeVal('parcelData',#prc.qryParcelInfo.CurrentRow-1#,this)" --->>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-12 databox">
					<div class="land-part-action-buttons">
						<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
						&nbsp;&nbsp;
						<a href="javascript:saveWDDX('info',0)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
					</div>
				</div>
			</div>

		</div>  <!--- id="edit_mode_item_1" --->
	</div>  <!--- id="collapse1" --->
</cfoutput>