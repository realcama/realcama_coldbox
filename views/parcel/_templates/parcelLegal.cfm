
<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading4" aria-expanded="false">
		<table class="land-line-summary">
		<tr href="##collapse4" class="section-bar-expander" data-parent="heading4" data-toggle="collapse" aria-controls="collapse4" onclick="toggleChevronChild(4)">
			<td width="95%" class="section-bar-label">Legal Description</td>
			<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_4"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
		<div id="view_mode_item_4">
			<br/>
			<!---
			<div class="row fix-bs-row">
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Subdivision</span><br/>
					<span class="formatted-data-values">&nbsp;</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Block</span><br/>
					<span class="formatted-data-values">&nbsp;</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Lot(s)</span><br/>
					<span class="formatted-data-values">&nbsp;</span>
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
		--->

			<div class="row fix-bs-row">
				<div class="col-xs-12 legal-databox">
					<span class="formatted-data-label">Legal Description</span><br/>
					<span class="formatted-data-values">#prc.qryParcelLegalInfo.LegalDescription#
						<!--- 	#breakStringAtLength(prc.qryParcelLegalInfo.LegalDescription,Controller.getSetting( "legalDescriptionLengthBreakpoint"))# --->
					</span><br/>
				</div>

				<div class="clearfix"></div>


				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(4);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>

		</div>

		<div id="edit_mode_item_4" style="display:none;">
			<br/>
			<div class="row fix-bs-row">
				<div class="col-xs-12 legal-databox">
					<span class="formatted-data-label">Description</span><br/>
					<textarea class="form-control legal-databox" name="legaldescription" onchange="changeVal('legalDescriptionData',#prc.qryParcelLegalInfo.CurrentRow-1#,this)">#prc.qryParcelLegalInfo.legaldescription#</textarea>
				</div>

				<div class="clearfix"></div>


				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
						&nbsp;&nbsp;
						<a href="javascript:saveWDDX('legal',0)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</cfoutput>
