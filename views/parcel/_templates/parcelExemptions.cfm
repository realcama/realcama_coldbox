
<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading5" aria-expanded="false">
		<table class="land-line-summary">
		<tr href="##collapse5" class="section-bar-expander" data-parent="heading5" data-toggle="collapse" aria-controls="collapse5" onclick="toggleChevronChild(5)">
			<td width="95%" class="section-bar-label">Exemptions</td>
			<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_5"></span></td>
		</tr>
		</table>
	</div>

	<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">

		<div id="view_mode_item_5">

			<div id="view_mode_exemptions">

				<div class="panel-heading" role="tab" class="panel" aria-expanded="false">
					<div class="row fix-bs-row">
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Application Year</span><br />
							<span class="formatted-data-values">#prc.qryParcelExemptionApplicationInfo.asflhxappl#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Owners</span><br />
							<span class="formatted-data-values">#prc.qryParcelExemptionApplicationInfo.asflnumown#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Applicants</span><br />
							<span class="formatted-data-values">#prc.qryParcelExemptionApplicationInfo.asflnumclm#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Days Deployed</span><br />
							<span class="formatted-data-values">#prc.qryParcelExemptionApplicationInfo.pvamdays#</span>
						</div>

					</div>
				</div>

				<div class="row fix-bs-row">
					<div class="col-xs-6">
						<br/><span class="formatted-data-label">Exemption</span>
					</div>
					<div class="col-xs-3">
						<br/><span class="formatted-data-label">Amount</span>
					</div>
					<div class="col-xs-3">
						<br/><span class="formatted-data-label">Percentage</span>
					</div>
					<div class="clearfix"></div>

					<cfloop query="prc.qryParcelExemptionInfo">
						<div class="col-xs-6 databox-parcel-exemption">
							<span class="formatted-data-values">#prc.qryParcelExemptionInfo.ExemptionCode#: #prc.qryParcelExemptionInfo.ExemptionCodeDescription#</span>
						</div>
						<div class="col-xs-3 databox-parcel-exemption">
							<span class="formatted-data-values">$#NumberFormat(prc.qryParcelExemptionInfo.ExemptionAmount,",.00")#</span>
						</div>
						<div class="col-xs-3 databox-parcel-exemption">
							<span class="formatted-data-values">#NumberFormat(prc.qryParcelExemptionInfo.ExemptionPct,",.00")#</span>
						</div>

						<div class="clearfix"></div>

					</cfloop>
					<div class="col-xs-12">
						<div class="land-part-action-buttons">
							<a href="javascript:editThisExemptionRow();"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
						</div>
					</div>
				</div>
			</div>

			<div id="edit_mode_exemptions" style="display:none;">

				<div class="panel-heading" role="tab" class="panel" aria-expanded="false">
					<div class="row fix-bs-row">
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Application Year</span><br />
							<input type="text" name="asflhxappl"  class="form-control datefield-yearonly-parcel" value="#prc.qryParcelExemptionApplicationInfo.asflhxappl#" onchange="changeVal('exemptionApplicationData',0,this)">
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Owners</span><br />
							<input type="text" name="asflnumown"  class="form-control numeric" value="#prc.qryParcelExemptionApplicationInfo.asflnumown#" onchange="changeVal('exemptionApplicationData',0,this)">
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Applicants</span><br />
							<input type="text" name="asflnumclm"  class="form-control numeric" value="#prc.qryParcelExemptionApplicationInfo.asflnumclm#" onchange="changeVal('exemptionApplicationData',0,this)">
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Number of Days Deployed</span><br />
							<input type="text" name="pvamdays"  class="form-control numeric" value="#prc.qryParcelExemptionApplicationInfo.pvamdays#" onchange="changeVal('exemptionApplicationData',0,this)">
						</div>
					</div>
				</div>


				<div class="row fix-bs-row">
					<div class="col-xs-6">
						<br/><span class="formatted-data-label">Exemption</span>
					</div>
					<div class="col-xs-3">
						<br/><span class="formatted-data-label">Amount</span>
					</div>
					<div class="col-xs-3">
						<br/><span class="formatted-data-label">Percentage</span>
					</div>
					<div class="clearfix"></div>

					<div id="parcel-ememptions">
						<CFSET strSelectionCode = "">
						<cfloop query="prc.qryParcelExemptionInfo">
							<div class="col-xs-6 databox-parcel-exemption">
								<select name="exemptioncode" id="exemptions_#prc.qryParcelExemptionInfo.CurrentRow#" data-tags="true" class="form-control two-items-on-row select2-2piece-new" onchange="changeExemptions('exemptionData',#prc.qryParcelExemptionInfo.CurrentRow-1#,this)">
									<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="&nbsp;">Select...</option>
									<cfloop query="prc.getExemptionCodes">
										<cfswitch expression="#type#">
											<cfcase value="A">
												<CFSET strAdjustment = NumberFormat(amount,".000")>
												<CFSET strAdjustmentLabel = "$#NumberFormat(amount,",.000")#">
											</cfcase>
											<cfcase value="P">
												<CFSET strAdjustment = NumberFormat(percent,".000")>
												<CFSET strAdjustmentLabel = "#NumberFormat(percent,",.000")#%">
											</cfcase>
											<cfcase value="I">
												<CFSET strAdjustment = "0">
												<CFSET strAdjustmentLabel = "User Defined">
											</cfcase>
										</cfswitch>
										<option <CFIF prc.qryParcelExemptionInfo.ExemptionCode EQ prc.getExemptionCodes.code>selected</CFIF> value="#prc.getExemptionCodes.code#" data-val="#prc.getExemptionCodes.code#" data-type="#prc.getExemptionCodes.type#" data-desc="#prc.getExemptionCodes.ExemptionCodeDescription#" data-adjval-amt="#strAdjustment#" data-adjval="#strAdjustmentLabel#">#prc.getExemptionCodes.code#: #prc.getExemptionCodes.ExemptionCodeDescription#</option>
									</cfloop>
								</select>
							</div>
							<div class="col-xs-3 databox-parcel-exemption">
								<input type="text" name="exemptionamount" id="exemptionamount_#prc.qryParcelExemptionInfo.CurrentRow-1#" class="form-control numeric" value="#NumberFormat(prc.qryParcelExemptionInfo.ExemptionAmount,"0.000")#" onchange="changeVal('exemptionData',#prc.qryParcelExemptionInfo.CurrentRow-1#,this)">
							</div>
							<div class="col-xs-3 databox-parcel-exemption">
								<input type="text" name="exemptionamt" id="exemptionpct_#prc.qryParcelExemptionInfo.CurrentRow-1#" class="form-control numeric" value="#NumberFormat(prc.qryParcelExemptionInfo.ExemptionPct,"0.000")#" onchange="changeVal('exemptionData',#prc.qryParcelExemptionInfo.CurrentRow-1#,this)">
							</div>
							<div class="clearfix"></div>

							<CFSET strSelectionCode = strSelectionCode & "$('##exemptions_#prc.qryParcelExemptionInfo.CurrentRow#').val('#prc.qryParcelExemptionInfo.ExemptionCode#').trigger('change');">
							<CFSET strSelectionCode = strSelectionCode & "$('##exemptionamount_#prc.qryParcelExemptionInfo.CurrentRow#').val('#NumberFormat(prc.qryParcelExemptionInfo.ExemptionAmount,"0.000")#');">
							<CFSET strSelectionCode = strSelectionCode & "$('##exemptionpct_#prc.qryParcelExemptionInfo.CurrentRow#').val('#NumberFormat(prc.qryParcelExemptionInfo.ExemptionPct,"0.000")#');">

						</cfloop>
						<script>
							function fixExemptionSelections() {
								#strSelectionCode#
							}

							$( document ).ready(function() {
								<!--- Have the change event control the amount/percentage field readonly states since it's based on the current selection value --->
								fixExemptionSelections();
							});
						</script>
					</div>

					<div class="col-xs-12">
						<div class="land-part-action-buttons">
							<a href="javascript:addExemption()" id="add-parcel-exemption"><img src="/includes/images/v2/AddExemption.png" alt="Save" border="0"></a>
							&nbsp;&nbsp;
							<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
							&nbsp;&nbsp;
							<a href="javascript:saveWDDX('exemptions',0)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
						</div>
					</div>
				</div>
			</div>


		</div>

	</div>
</cfoutput>