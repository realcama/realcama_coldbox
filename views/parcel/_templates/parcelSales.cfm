<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading3" aria-expanded="false">
		<table class="land-line-summary">
		<tr href="##collapse3" class="section-bar-expander" data-parent="heading3" data-toggle="collapse" aria-controls="collapse3" onclick="toggleChevronChild(3)">
			<td width="95%" class="section-bar-label">Sales Information</td>
			<td width="5%" align="right" class="section-bar-label"><span class="block-expander glyphicon glyphicon-chevron-down collapsed" id="chevron_3"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse3" class="panel-collapse collapse <!--- in --->" role="tabpanel" aria-labelledby="heading3">
		<cfloop query="prc.qryParcelSales">
			<div id="view_mode_item_parcel_#CurrentRow#">
				<div class="row fix-bs-row">
					<div class="parcel-table-header section-bar-label col-xs-12">
						Line ###prc.qryParcelSales.currentRow#
					</div>
					<div class="clearfix"></div>

					<div class="col-xs-3 databox">
						<span class="formatted-data-label">OR Book</span><br />
						<span class="formatted-data-values">#prc.qryParcelSales.bookNumber#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">OR Page</span><br />
						<span class="formatted-data-values">#prc.qryParcelSales.pageNumber#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Instrument Type</span><br />
						<span class="formatted-data-values">#prc.qryParcelSales.InstrumentType# : #prc.qryParcelSales.SINSDS#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Q/U</span><br />
						<span class="formatted-data-values">#prc.qryParcelSales.qualifiedFlag#: #prc.qryParcelSales.qualifiedflagdescription#</span>
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-3 databox">
						<span class="formatted-data-label">V/I (Vacant/Improved)</span><br />
						<span class="formatted-data-values">#prc.qryParcelSales.VacantImprovedFlag# : #prc.qryParcelSales.vacantimprovedflagdescription#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Documentary Stamps</span><br />
						<span class="formatted-data-values">#DollarFormat(prc.qryParcelSales.documentStampAmt)#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Sale Price</span><br />
						<span class="formatted-data-values">#DollarFormat(prc.qryParcelSales.salePrice)#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Ratio</span><br />
						<CFTRY>
							<CFSET iRatio = iTotalHeaderValue/prc.qryParcelSales.salePrice>
							<CFCATCH><CFSET iRatio = 0></CFCATCH>
						</CFTRY>
						<span class="formatted-data-values">#NumberFormat(iRatio,"9.999")#</span>

					</div>
					<div class="clearfix"></div>


					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Document Number</span><br />
						<span class="formatted-data-values"></span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Recorded Date</span><br />
						<span class="formatted-data-values">#makeDatePretty(prc.qryParcelSales.recordDate)#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Sales Change Date</span><br />
						<span class="formatted-data-values">#makeDatePretty(prc.qryParcelSales.saleChangeDate)#</span>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Multi Parcel Sales</span><br />
						<span class="formatted-data-values">#prc.qryParcelSales.multiParcelSales#</span>
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Sales Change Code</span><br />
						<span class="formatted-data-values"><CFIF LEN(TRIM(prc.qryParcelSales.saleChangeCode)) NEQ 0>#prc.qryParcelSales.saleChangeCode# : #prc.qryParcelSales.saleChangeDescription#<CFELSE>N/A</CFIF></span>
					</div>
					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Sales Reason Code</span><br />
						<span class="formatted-data-values"><CFIF LEN(TRIM(prc.qryParcelSales.saleReasonCode)) NEQ 0>#prc.qryParcelSales.saleReasonCode# : #prc.qryParcelSales.saleReasonDescription#<CFELSE>N/A</CFIF></span>
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Grantor</span><br />
						<span class="formatted-data-values">
							<CFIF prc.qryParcelInfo.confidential EQ "N">
								#prc.qryParcelSales.grantor#
							<CFELSE>
								************************
							</CFIF>
						</span>
					</div>
					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Grantee</span><br />
						<span class="formatted-data-values">
							<CFIF prc.qryParcelInfo.confidential EQ "N">
								#prc.qryParcelSales.grantee#
							<CFELSE>
								************************
							</CFIF>
						</span>
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-12 databox">
						<div class="land-part-action-buttons">
							<a href="javascript:editThisParcelSalesRow(#CurrentRow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
						</div>
					</div>
				</div>

			</div>


			<div id="edit_mode_item_parcel_#CurrentRow#" style="display:none;">
				<div class="row fix-bs-row">
					<div class="parcel-table-header section-bar-label col-xs-12">
						Line ###prc.qryParcelSales.currentRow#
					</div>
					<div class="clearfix"></div>

					<div class="col-xs-3 databox-small">
						<span class="formatted-data-label">OR Book</span><br />
						<input type="text" name="booknumber" class="form-control" value="#prc.qryParcelSales.bookNumber#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
					</div>
					<div class="col-xs-3 databox-small">
						<span class="formatted-data-label">OR Page</span><br />
						<input type="text" name="pagenumber" class="form-control" value="#prc.qryParcelSales.pageNumber#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
					</div>
					<div class="col-xs-3 databox-large">
						<span class="formatted-data-label">Instrument Type</span><br />
						<select data-tags="true" row="#prc.qryParcelSales.CurrentRow-1#" class="form-control select2-2piece-new" name="instrumenttype" onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getInstrumentType">
								<option value="#prc.getInstrumentType.sinscd#" data-val="#prc.getInstrumentType.sinscd#" data-desc="#prc.getInstrumentType.sinsds#" <CFIF prc.qryParcelSales.InstrumentType EQ prc.getInstrumentType.sinscd>selected</CFIF>>#prc.getInstrumentType.sinscd# : #prc.getInstrumentType.sinsds#</option>
							</cfloop>
						</select>
					</div>
					<div class="col-xs-3 databox-large">
						<span class="formatted-data-label">Q/U</span><br />
						<select data-tags="true" row="#prc.qryParcelSales.CurrentRow-1#" class="form-control select2-2piece-new" name="qualifiedflag" onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)"> <!--- two-items-on-row select2-2piece-nosearch-new --->
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.qryQualificationCodes">
								<option value="#prc.qryQualificationCodes.code#" data-val="#prc.qryQualificationCodes.code#" data-desc="#prc.qryQualificationCodes.desc#" <CFIF prc.qryParcelSales.qualifiedFlag EQ prc.qryQualificationCodes.code>selected</CFIF>>#prc.qryQualificationCodes.code# : #prc.qryQualificationCodes.desc#</option>
							</cfloop>
						</select>
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-3 databox">
						<span class="formatted-data-label">V/I</span> <span class="minor-label">(Vacant/Improved)</span><br />
						<select data-tags="true" row="#prc.qryParcelSales.CurrentRow-1#" class="form-control select2-2piece-new" name="vacantimprovedflag" onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.qryImprovementCodes">
								<option value="#prc.qryImprovementCodes.code#" data-val="#prc.qryImprovementCodes.code#" data-desc="#prc.qryImprovementCodes.desc#" <CFIF prc.qryParcelSales.VacantImprovedFlag EQ prc.qryImprovementCodes.code>selected</CFIF>>#prc.qryImprovementCodes.code# : #prc.qryImprovementCodes.desc#</option>
							</cfloop>
						</select>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Documentary Stamps</span><br />
						$<input type="text" name="documentstampamt" class="form-control numeric moneyfield" value="#NumberFormat(prc.qryParcelSales.documentStampAmt,"0.00")#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Sale Price</span><br />
						$<input type="text" name="saleprice" class="form-control numeric moneyfield" value="#NumberFormat(prc.qryParcelSales.salePrice,"0.00")#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this);updateRatio(this,#prc.qryParcelSales.CurrentRow#); calcRatio(#prc.qryParcelSales.CurrentRow#)">
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Ratio</span><br />
						<CFTRY>
							<CFSET iRatio = iTotalHeaderValue/prc.qryParcelSales.salePrice>
							<CFCATCH><CFSET iRatio = 0></CFCATCH>
						</CFTRY>
						<input type="text" name="ratio" id="ratio_#prc.qryParcelSales.CurrentRow#" class="form-control numeric" readonly value="#NumberFormat(iRatio,"0.000")#" data-toggle="tooltip" data-placement="top" title="Calculation:<br/>Total Price / Sale Price" <!----  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)" --->>
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Document Number</span><br />
						<input type="text" name="lafront" class="form-control" <!---- value="#NumberFormat(qryrecords.lafront,"0.000")#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)" --->>
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Recorded Date</span><br />
						<input type="text" name="recorddate" class="form-control datefield" value="#makeDatePretty(prc.qryParcelSales.recordDate)#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Sales Change Date</span><br />
						<input type="text" name="salechangedate" class="form-control datefield" value="#makeDatePretty(prc.qryParcelSales.saleChangeDate)#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
					</div>
					<div class="col-xs-3 databox">
						<span class="formatted-data-label">Multi Parcel Sale</span><br />
						<span class="formatted-data-values">
							<input type="radio" class="radioButton" name="multiParcelSales.#prc.qryParcelSales.CurrentRow-1#" value="Yes" onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this);lockParcelEditButton(#prc.qryParcelSales.CurrentRow#,this);" <CFIF prc.qryParcelSales.multiParcelSales EQ "Yes">checked</CFIF>> Yes &nbsp;&nbsp;
							<input type="radio" class="radioButton" name="multiParcelSales.#prc.qryParcelSales.CurrentRow-1#" value="No" onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this);lockParcelEditButton(#prc.qryParcelSales.CurrentRow#,this);" <CFIF prc.qryParcelSales.multiParcelSales EQ "No">checked</CFIF>> No
						</span>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="javascript:doParcelPopup(#prc.qryParcelSales.saleChangeDate#,#prc.qryParcelSales.CurrentRow#,#prc.qryParcelSales.itemNumber#);" id="parcel-edit-button-#prc.qryParcelSales.CurrentRow#" class="btn btn-default" <CFIF prc.qryParcelSales.multiParcelSales EQ "No">disabled="true"</CFIF>>Edit</a>
						<!--- (#prc.qryParcelSales.itemNumber#) --->
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Sales Change Code</span><br />
						<select data-tags="true" row="#prc.qryParcelSales.CurrentRow-1#" class="form-control select2-2piece-new" name="salechangecode" onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getSalesChangeCodes">
								<option value="#prc.getSalesChangeCodes.sccode#" data-val="#prc.getSalesChangeCodes.sccode#" data-desc="#prc.getSalesChangeCodes.scdesc#" <CFIF prc.qryParcelSales.saleChangeCode EQ prc.getSalesChangeCodes.sccode>selected</CFIF>>#prc.getSalesChangeCodes.sccode# : #prc.getSalesChangeCodes.scdesc#</option>
							</cfloop>
						</select>
					</div>
					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Sales Reason Code</span><br />
						<select data-tags="true" row="#prc.qryParcelSales.CurrentRow-1#" class="form-control select2-2piece-new" name="salereasoncode" onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getSalesReasonCodes">
								<option value="#prc.getSalesReasonCodes.srcdcd#" data-val="#prc.getSalesReasonCodes.srcdcd#" data-desc="#prc.getSalesReasonCodes.srcdds#" <CFIF prc.qryParcelSales.saleReasonCode EQ prc.getSalesReasonCodes.srcdcd>selected</CFIF>>#prc.getSalesReasonCodes.srcdcd# : #prc.getSalesReasonCodes.srcdds#</option>
							</cfloop>
						</select>
					</div>
					<div class="clearfix"></div>


					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Grantor</span><br />
						<input type="text" name="grantor" class="form-control" value="#prc.qryParcelSales.grantor#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
					</div>
					<div class="col-xs-6 databox">
						<span class="formatted-data-label">Grantee</span><br />
						<input type="text" name="grantee" class="form-control" value="#prc.qryParcelSales.grantee#"  onchange="changeVal('salesData',#prc.qryParcelSales.CurrentRow-1#,this)">
					</div>

					<div class="col-xs-12">
						<div class="land-part-action-buttons">
							<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
							&nbsp;&nbsp;
							<a href="javascript:saveWDDX('sales',#prc.qryParcelSales.CurrentRow#)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
						</div>
					</div>
				</div>
			</div>

		</cfloop>


	</div>
</cfoutput>