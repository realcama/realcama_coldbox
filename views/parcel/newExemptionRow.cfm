<cfsetting showdebugoutput="No">
<cfoutput>				
	<div class="col-xs-6 databox-parcel-exemption parcel-exemption-added-row">							
		<select name="exemptioncode" id="exemptions_#rc.cfRowNumber#" data-tags="true" class="form-control two-items-on-row select2-2piece-new" onchange="changeExemptions('exemptionData',#rc.cfRowNumber-1#,this)">
			<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="&nbsp;">Select...</option>
			<cfloop query="prc.getExemptionCodes">
				<cfswitch expression="#xmpttyp#">
					<cfcase value="A">
						<CFSET strAdjustment = NumberFormat(xmptamt,".000")>
						<CFSET strAdjustmentLabel = "$#NumberFormat(xmptamt,",.000")#">
					</cfcase>
					<cfcase value="P">
						<CFSET strAdjustment = NumberFormat(xmptpct,".000")>
						<CFSET strAdjustmentLabel = "#NumberFormat(xmptpct,",.000")#%">
					</cfcase>
					<cfcase value="I">
						<CFSET strAdjustment = "0">
						<CFSET strAdjustmentLabel = "User Defined">
					</cfcase>
				</cfswitch>
				<option value="#prc.getExemptionCodes.xmptcd#" data-val="#prc.getExemptionCodes.xmptcd#" data-type="#prc.getExemptionCodes.xmpttyp#" data-desc="#prc.getExemptionCodes.xmptds#" data-adjval-amt="#strAdjustment#" data-adjval="#strAdjustmentLabel#">#prc.getExemptionCodes.xmptcd#: #prc.getExemptionCodes.xmptds#</option>
			</cfloop>
		</select>								
	</div>
	<div class="col-xs-3 databox-parcel-exemption parcel-exemption-added-row">								
		<input type="text" name="exemptionamount" id="exemptionamount_#rc.cfRowNumber-1#" class="form-control numeric" value="" onchange="changeVal('exemptionData',#rc.cfRowNumber-1#,this)">
	</div>
	<div class="col-xs-3 databox-parcel-exemption parcel-exemption-added-row">
		<input type="text" name="exemptionpct" id="exemptionpct_#rc.cfRowNumber-1#" class="form-control numeric" value="" onchange="changeVal('exemptionData',#rc.cfRowNumber-1#,this)">												
	</div>
	<div class="clearfix parcel-exemption-added-row"></div>	
</cfoutput>
