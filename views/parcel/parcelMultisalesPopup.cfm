<cfsetting showdebugoutput="No">

<cfparam name="rc.row" default="0">
<CFIF NOT IsNumeric(rc.row)>
	<SCRIPT>
		parent.$.fancybox.close();
	</SCRIPT>
	<CFABORT>
</CFIF>

<CFSET rc.ParcelID = URLDecode(rc.ParcelID)>
 
<link href="/includes/css/select2.css" rel="stylesheet" />
<script src="/includes/js/select2.js"></script>


<!--- Remember that JS is 0 based, and CF is 1 based --->
<CFSET jsRowNumber = rc.row - 1>
	



<!--- Pull the entries for this item from the UDF packet --->
<script>	
	var iOptionsOnModal = 0;
	<cfoutput>
	var iUrlRow = #rc.row#;
	var jsRowNumber = #jsRowNumber#;
	var itemNumber = #rc.itemNumber#;
	var ParcelID = "#rc.ParcelID#";
	var inYear = "#LEFT(rc.taxYear,4)#";
	</cfoutput>
	
	$( document ).ready(function() {			
		buildModal();
		changeSelectsToSelect2s();		
	});
	
	
	function format2Piece(state) {
		var originalOption = state.element;
		return "<span class='select2-2part-left'>" + $(originalOption).data('useccd') + "</span><span class='select2-2part-right'>" + $(originalOption).data('usecds')+ "</span>";	   
	}
	
	
	function changeSelectsToSelect2s() {
		
		<!--- turn all of the select2 boxes into formatted select boxes --->
		$('.select2-2piece-dropdown-new').select2({
			width: "350px",			
			formatResult: format2Piece,
			formatSelection: format2Piece,
			escapeMarkup: function(m) { return m; }
		});		
		
		<!--- Go through all of the DOM elements and change "select2-dropdown-new" to "select2-dropdown-new" --->
		$( ".select2-2piece-dropdown-new" ).each(function( ) {			
			$(this).removeClass("select2-2piece-dropdown-new").addClass("select2-2piece-dropdown");
		});
		
		
	}
	
	
	
	
	function buildModal() {
		for ( var iCurrentRow = 0; iCurrentRow < parent.multiSalesData.getRowCount(); iCurrentRow++ ) {
			iCurrentRowItem = parent.multiSalesData.getField(iCurrentRow,"smitem");			
							
			if(iCurrentRowItem == itemNumber) {			
				
				mode = parent.multiSalesData.getField(iCurrentRow,"mode");				
				
				if(mode != "Delete") {
					smitem = parent.multiSalesData.getField(iCurrentRow,"smitem");
					smprop = parent.multiSalesData.getField(iCurrentRow,"smprop");
					usecds = parent.multiSalesData.getField(iCurrentRow,"usecds");
				
					var strHTML = "";	
					strHTML = createMultiSalesEntryRow(iCurrentRow, 'Edit');					

					console.log("here");

					if(iOptionsOnModal == 0) {
						$("#adjContainer").html(strHTML);
					} else {
						$("#adjContainer").append(strHTML);
					}
					
					iOptionsOnModal++;
					
					<!--- Set the values for these new elements	--->			
					$("#smprop_" + iCurrentRow).val(smprop);
					$("#usecds_" + iCurrentRow).val(usecds);
				
				}
			}
		}
				
		//createLabel();
		$('[data-toggle="tooltip"]').tooltip();		
	}
	
	
	function createMultiSalesEntryRow(iRow, mode) {
		var strHTML = "";			
		
		strHTML = strHTML + "<div id='adjRow_" + iRow + "' class='udfrow'>"
		if(mode == "Insert") {
			strHTML = strHTML + "	<i class='icon-nothing'></i>";
		} else {
			strHTML = strHTML + "	<i class='glyphicon glyphicon-remove-sign' onclick='removeMultiSalesRow(" + iRow + ")'></i>";
		}
		
		strHTML = strHTML + "	<input name='smprop' id='smprop_" + iRow + "'  class='form-control parcel-popup-input' type='text' value='' onchange='changeMultiSalesInputVal(" + iRow +",this);lookupUseCode(" + iRow +",this)'>";			
		strHTML = strHTML + "	<input name='usecds' id='usecds_" + iRow + "'  class='form-control parcel-popup-input' type='text' value='' disabled='true'>";			
		
		
		strHTML = strHTML + "</div>";		
		
		return strHTML;
		
	}
	function removeMultiSalesRow(iRow) {
		var choice = confirm("Are you sure you want to delete this row?");
		if(choice == true) {
			parent.multiSalesData.setField( iRow, "mode", "Delete" );
			$("#adjRow_" + iRow).remove();
			$('[data-toggle="tooltip"]').tooltip();
			
		}
	}
	function addMultiSalesRow() {
		parent.multiSalesData.addRows(1);
	
		iNewRow = parent.multiSalesData.getRowCount() -1;
		parent.multiSalesData.setField( iNewRow, "smaprop", ParcelID );
		parent.multiSalesData.setField( iNewRow, "smitem", itemNumber );		
		parent.multiSalesData.setField( iNewRow, "mode", "Insert" );
		parent.multiSalesData.setField( iNewRow, "smsnapy", inYear );
		
		
		var strHTML = "";	
		strHTML = createMultiSalesEntryRow(iNewRow, 'Insert');	
				
		if(iOptionsOnModal <= 0) {
			$("#adjContainer").html(strHTML);
		} else {
			$("#adjContainer").append(strHTML);
		}
		
		$(".numeric").numeric();	
		changeSelectsToSelect2s();
		iOptionsOnModal++;	
	}
	
	function changeMultiSalesInputVal(iRow,obj) {
		var smprop = $(obj).val();
		parent.multiSalesData.setField( iRow, "smprop", smprop );
	}	
	
	function lookupUseCode(iRow,obj) {
		var smprop = $(obj).val();		
		
		if(smprop.length > 0) {
			$.ajax({
		        url:'index.cfm/parcel/getUseCode',
		        async: false, //This variable makes the function wait until a response has been returned from the page
				data: {					
					parcelID	: smprop,
					taxYear	: inYear
				},
				success: function( data ) {
					eval(data);
				}
		    });	
		}
	}
</script>
	

<cfoutput>



	<div id="popParcels" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-parcel-parcelPopup">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Parcel(s) for Line ###rc.row#</h4>
	      </div>		 
	      <div class="modal-body">				
	      	<div>
				<span class="icon-nothing">&nbsp;</span>
				<span class="label-parcel-popup">Parcel Number</span>
				<span class="label-parcel-popup">Use Code / Description</span>
			</div>
		  	<div id="adjContainer">			
			</div>
		  </div>
		
	      <div class="modal-footer">		  
		  	<button id="addRowButton" class="btn btn-primary btn-add-option-parcel" type="button" onclick="addMultiSalesRow()"><i class="glyphicon glyphicon-plus-sign"></i> Add Another Parcel</button>
	        <button type="button"  class="btn btn-default"  onclick="parent.$.fancybox.close();"><i class="glyphicon glyphicon-ok-sign"></i> Finished</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-land-parcelUDF -->
	</div><!-- /.modal -->
</cfoutput>



