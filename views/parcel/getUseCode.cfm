<cfsetting showdebugoutput="No">

<CFIF prc.getSpecificParcelUseCode.RecordCount>
	<cfoutput query="prc.getSpecificParcelUseCode">
		parent.multiSalesData.setField( iNewRow, "useCodeDescription", "#TRIM(prc.getSpecificParcelUseCode.useCodeDescription)#" );
		$("##useCodeDescription" + iRow).val("#TRIM(prc.getSpecificParcelUseCode.useCodeDescription)#");

		parent.multiSalesData.setField( iNewRow, "useCode", "#TRIM(prc.getSpecificParcelUseCode.useCode)#" );
		$("##useCode" + iRow).val("#TRIM(prc.getSpecificParcelUseCode.useCode)#");
	</CFOUTPUT>
<CFELSE>
	alert("We were unable to find a use code for this property");
</CFIF>
