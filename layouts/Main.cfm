<cfoutput>
<cfparam name="prc.pageTitle" default="">
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>RealCAMA</title>

	<!---Base URL --->
	<base href="#event.getHTMLBaseURL()#" />
	<!---css --->
	<link href="includes/css/jquery.fancybox.css" rel="stylesheet">
	<link href="includes/css/datepicker3.css" rel="stylesheet">
	<link href="includes/css/bootstrap.css" rel="stylesheet">
	<link href="includes/css/select2.css" rel="stylesheet">
	<link href="includes/css/site.css" rel="stylesheet">

	<!---js --->
    <script src="includes/js/jquery.js"></script>
	<script src="includes/js/bootstrap.js"></script>
	<!--- <script src="includes/js/search.js"></script> --->
	<script src="includes/js/select2.js"></script>
	<script src="includes/js/jquery-validate.bootstrap-tooltip.js"></script>
	<script src="includes/js/jquery.fancybox.js"></script>
	<script src="includes/js/jquery.typeahead.js"></script>
	<script src="includes/js/bootstrap-datepicker.js"></script>
	<script src="includes/js/jquery.numeric.js"></script>
	<script src="includes/js/wddx.js"></script>
	<script src="includes/js/wddxviewer.js"></script>


	<script>
		var arDatePickerOptions = { format: "yyyy", viewMode: "years",  minViewMode: "years", startDate: "#year(now())#",endDate: "#Evaluate(year(now())+ 50)#"};
		var arDatePickerOptions2 = { format: "yyyy", viewMode: "years",  minViewMode: "years", startDate: "1990",endDate: "#year(now())#"};
		var arDatePickerOptions4Permits = { endDate: "#dateformat(now(),"mm/dd/yyyy")#"};

		var cancelURL = "/index.cfm/#event.getCurrentRoutedURL()#?#CGI.QUERY_STRING#";

		Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
		    var n = this,
		    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
		    decSeparator = decSeparator == undefined ? "." : decSeparator,
		    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
		    sign = n < 0 ? "-" : "",
		    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
		    j = (j = i.length) > 3 ? j % 3 : 0;
		    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
		};


		function cancelEverything() {
			var bConfirm = confirm("Are you sure you want to do this (you will lose any changes made to this page)?");
			if(bConfirm) {
				$('##myPleaseWait').modal('show');
				var url = removeURLParameter(cancelURL,"refresh");
				window.location.replace(url + "&refresh=" + Math.random());
			}
		}
		function removeURLParameter(url, parameter) {
		    //prefer to use l.search if you have a location/link object
		    var urlparts= url.split('?');
		    if (urlparts.length>=2) {

		        var prefix= encodeURIComponent(parameter)+'=';
		        var pars= urlparts[1].split(/[&;]/g);

		        //reverse iteration as may be destructive
		        for (var i= pars.length; i-- > 0;) {
		            //idiom for string.startsWith
		            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
		                pars.splice(i, 1);
		            }
		        }

		        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
		        return url;
		    } else {
		        return url;
		    }
		}
	</script>
</head>

<body>
	<cfoutput>#renderView('_templates/head')#</cfoutput>
	<div class="siteContent">

		<div class="container">
			<cfif IsDefined("prc.qGetPersistentInfoTPP") AND prc.qGetPersistentInfoTPP.Recordcount NEQ 0>
				#renderView( "persistent/tpp" )#
			<cfelseif IsDefined("prc.qryPersistentInfo") AND prc.qryPersistentInfo.Recordcount NEQ 0
					and rc.event neq "homesteadExemptform.index">
				#renderView( "persistent/property" )#
			</CFIF>
			#renderView()#
		</div>
	</div>
	<cfoutput>#renderView('_templates/footer')#</cfoutput>


</body>
</html>
</cfoutput>
