/**
* I am a new Model Object
*/
component accessors="true"{
	
	property name="summaryDAO" inject=model;
	// Properties
	

	/**
	 * Constructor
	 */
	summaryServices function init(){
		
		return this;
	}
	
	function getSummaryData(required struct persistentCriteria) {
		return summaryDAO.getSummaryData(persistentCriteria = arguments.persistentCriteria);	
	}
	
	function getSummaryPreviousYearsData(required struct persistentCriteria) {
		return summaryDAO.getSummaryPreviousYearsData(persistentCriteria = arguments.persistentCriteria);	
	}
	
	function getSummaryExemptions(required struct persistentCriteria) {
		return summaryDAO.getSummaryExemptions(persistentCriteria = arguments.persistentCriteria);	
	}
	
	
}