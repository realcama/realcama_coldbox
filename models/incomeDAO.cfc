/**
* I am a new Model Object
*/
component accessors="true"{
	
	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";

	/**
	 * Constructor
	 */
	incomeDAO function init(){
		
		return this;
	}
	
	function getIncomeInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetIncomeInfo (:parcelID, :taxYear, :propType)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);		
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);		
		q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");		
		return q.execute().getResult();	
	}

}