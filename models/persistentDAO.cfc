/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn"					inject="coldbox:datasource:ws_realcama_nassau";
	property name="cache"				inject="cachebox:default";
	property name="log"					inject="logbox:logger:{this}";
	property name="sessionStorage"		inject="sessionStorage@cbstorages";

	/**
	 * Constructor
	 */
	persistentDAO function init(){

		return this;
	}


	function getPropPersistent( required struct persistentCriteria ) {
		var qGetPropPersistentInfo = [];
		var cacheKey = 'q-PropPersistentInfo-#arguments.persistentCriteria.ParcelID#-#arguments.persistentCriteria.taxYear#';

		if( cache.lookup(cacheKey) ){
			qGetPropPersistentInfo = cache.get(cacheKey);
			log.info ( "q-PropPersistentInfo #arguments.persistentCriteria.ParcelID# #arguments.persistentCriteria.taxYear# is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetPersistentInfo (:parcelID, :taxYear, :type)");
				q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
				q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
				q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");

			qGetPropPersistentInfo = q.execute().getResult();
			cache.set(cacheKey, qGetPropPersistentInfo, 30, 30);
			log.info ( "running q-qGetPropPersistentInfo #arguments.persistentCriteria.ParcelID# #arguments.persistentCriteria.taxYear# query" );
		}

		return qGetPropPersistentInfo;
	}


	function getPropPersistentInfoTotals( required struct persistentCriteria ) {
		var qGetPropPersistentInfoTotals = [];
		var cacheKey = 'q-PropPersistentInfoTotals-#arguments.persistentCriteria.ParcelID#-#arguments.persistentCriteria.taxYear#';

		if( cache.lookup(cacheKey) ){
			qGetPropPersistentInfoTotals = cache.get(cacheKey);
			log.info ( "q-PropPersistentInfoTotals #arguments.persistentCriteria.ParcelID# #arguments.persistentCriteria.taxYear# is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetPersistentInfoTotals (:parcelID, :taxYear, :type)");
				q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
				q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
				q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");

			qGetPropPersistentInfoTotals = q.execute().getResult();
			cache.set(cacheKey, qGetPropPersistentInfoTotals, 30, 30);
			log.info ( "running q-qGetPropPersistentInfoTotals #arguments.persistentCriteria.ParcelID# #arguments.persistentCriteria.taxYear# query" );
		}

		return qGetPropPersistentInfoTotals;
	}


	function getPropPersistentInfoCounts( required struct persistentCriteria ) {
		var qGetPropPersistentInfoCounts = [];
		var cacheKey = 'q-PropPersistentInfoCounts-#arguments.persistentCriteria.ParcelID#-#arguments.persistentCriteria.taxYear#';

		if( cache.lookup(cacheKey) ){
			qGetPropPersistentInfoCounts = cache.get(cacheKey);
			log.info ( "q-PropPersistentInfoCounts #arguments.persistentCriteria.ParcelID# #arguments.persistentCriteria.taxYear# is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetPersistentInfoCounts (:parcelID, :taxYear, :type)");
				q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
				q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
				q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");

			qGetPropPersistentInfoCounts = q.execute().getResult();
			cache.set(cacheKey, qGetPropPersistentInfoCounts, 30, 30);
			log.info ( "running q-qGetPropPersistentInfoCounts #arguments.persistentCriteria.ParcelID# #arguments.persistentCriteria.taxYear# query" );
		}

		return qGetPropPersistentInfoCounts;
	}


	function setPropPersistent( required struct persistentCriteria ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_UpdatePersistentInfo (:parcelID, :taxYear, :type, :iLandLines, :iAgriculturalLines, :fTotalLandPrice, :fTotalAgriculturalPrice, :pbvalue, :pxvalbld, :fTotalAcres, :fTotalSqFeet)");
			q.addParam(name="ParcelID",					value=arguments.persistentCriteria.ParcelID,							cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",					value=arguments.persistentCriteria.taxYear,								cfsqltype="cf_sql_integer", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="type",						value="R",																cfsqltype="cf_sql_varchar");
			q.addParam(name="iLandLines",				value=arguments.persistentCriteria.iLandLines,							cfsqltype="cf_sql_integer", 	null=len(trim(arguments.persistentCriteria.iLandLines)) ? false : true);
			q.addParam(name="iAgriculturalLines",		value=arguments.persistentCriteria.iAgriculturalLines,					cfsqltype="cf_sql_integer", 	null=len(trim(arguments.persistentCriteria.iAgriculturalLines)) ? false : true);
			q.addParam(name="fTotalLandPrice",			value=arguments.persistentCriteria.fTotalLandPrice,						cfsqltype="cf_sql_integer", 	null=len(trim(arguments.persistentCriteria.fTotalLandPrice)) ? false : true);
			q.addParam(name="fTotalAgriculturalPrice",	value=arguments.persistentCriteria.fTotalAgriculturalPrice,				cfsqltype="cf_sql_integer", 	null=len(trim(arguments.persistentCriteria.fTotalAgriculturalPrice)) ? false : true);
			q.addParam(name="pbvalue",					value=arguments.persistentCriteria.pbvalue,								cfsqltype="cf_sql_integer", 	null=len(trim(arguments.persistentCriteria.pbvalue)) ? false : true);
			q.addParam(name="pxvalbld",					value=arguments.persistentCriteria.pxvalbld,							cfsqltype="cf_sql_integer", 	null=len(trim(arguments.persistentCriteria.pxvalbld)) ? false : true);
			q.addParam(name="fTotalAcres",				value=NumberFormat(arguments.persistentCriteria.fTotalAcres,'0.00'),	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.fTotalAcres)) ? false : true);
			q.addParam(name="fTotalSqFeet",				value=arguments.persistentCriteria.fTotalSqFeet,						cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.fTotalSqFeet)) ? false : true);

		return q.execute().getResult();
	}

	function getGetFavoriteMarkerStatus(
					required numeric userID,
					required string taxYear,
					required string parcelID,
					string accountNumber) {

		// Check to see which values we should pass in for this SP call Parcel or Tangible
		if (isDefined("accountNumber") and accountNumber <> "") {
			// Pass in the values for the TPP favorite check
			var q = new Query(datasource="#dsn.name#",sql="call p_GetFavoriteMarkerStatus (:p_userID, :p_TaxYear, :p_ParcelID, :p_tpp)");
				q.addParam(name="p_userID",		value=arguments.userID,			cfsqltype="cf_sql_integer", 	null=len(trim(arguments.userID)) ? false : true);
				q.addParam(name="p_TaxYear",	value=arguments.taxYear,		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);
				q.addParam(name="p_ParcelID",									cfsqltype="cf_sql_varchar", 	null=true);
				q.addParam(name="p_tpp",		value=arguments.accountNumber,	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.accountNumber)) ? false : true);
		} else {
			// Pass in the values for the Parcel favorite check
			var q = new Query(datasource="#dsn.name#",sql="call p_GetFavoriteMarkerStatus (:p_userID, :p_TaxYear, :p_ParcelID, :p_tpp)");
				q.addParam(name="p_userID",			value=arguments.userID, 	cfsqltype="cf_sql_integer", 	null=len(trim(arguments.userID)) ? false : true);
				q.addParam(name="p_TaxYear",		value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);
				q.addParam(name="p_ParcelID",		value=arguments.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.parcelID)) ? false : true);
				q.addParam(name="p_tpp",										cfsqltype="cf_sql_integer", 	null=true);
		}

		return q.execute().getResult();
	}


	function getPrevPropID( required struct persistentCriteria ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetPrevPropID (:parcelID, :taxYear, :type)");
			q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="type",		value="R", 	cfsqltype="cf_sql_varchar");

		return q.execute().getResult();
	}


	function getNextPropID( required struct persistentCriteria ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetNextPropID (:parcelID, :taxYear, :type)");
			q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="type",		value="R", 	cfsqltype="cf_sql_varchar");

		return q.execute().getResult();
	}


	function getPropertyYears( required struct persistentCriteria ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetPropertyYears (:parcelID, :type)");
			q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="type",		value="R", 	cfsqltype="cf_sql_varchar");

		return q.execute().getResult();
	}


	function getTppLookup( required struct persistentCriteria ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetTPPLookup (:parcelID, :taxYear)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	function getTPPPersistentInfo( required string accountNumber, required string taxYear ) {
		var qGetTPPPersistentInfo = [];
		var cacheKey = 'q-TPPPersistentInfo-#arguments.accountNumber#-#arguments.taxYear#';

		if( cache.lookup(cacheKey) ){
			qGetTPPPersistentInfo = cache.get(cacheKey);
			log.info ( "q-qGetTPPPersistentInfo #arguments.accountNumber# #arguments.taxYear# is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetPersistentTangibleInfo (:p_accountNumber, :p_taxYear)");
				q.addParam(name="p_accountNumber",	value=arguments.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.accountNumber)) ? false : true);
				q.addParam(name="p_taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

			qGetTPPPersistentInfo = q.execute().getResult();
			cache.set(cacheKey, qGetTPPPersistentInfo, 30, 30);
			log.info ( "running q-qGetTPPPersistentInfo #arguments.accountNumber# #arguments.taxYear# query" );
		}

		return qGetTPPPersistentInfo;
	}


	function getPrevTangiblePropID( required struct persistentCriteria ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetPrevTangibleID (:p_accountNumber, :p_taxYear)");
			q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
			q.addParam(name="p_accountNumber",	value=arguments.persistentCriteria.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.accountNumber)) ? false : true);
			q.addParam(name="p_taxYear",		value=arguments.persistentCriteria.taxYear, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	function getNextTangiblePropID( required struct persistentCriteria ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetNextTangibleID (:p_accountNumber, :p_taxYear)");
			q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
			q.addParam(name="p_accountNumber",	value=arguments.persistentCriteria.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.accountNumber)) ? false : true);
			q.addParam(name="p_taxYear",		value=arguments.persistentCriteria.taxYear, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	function clearPersistentCache( required string parcelID, required string taxYear ) {

		var cacheKeyInfo = 'q-PropPersistentInfo-#arguments.ParcelID#-#arguments.taxYear#';
		var cacheKeyTotals = 'q-PropPersistentInfoTotals-#arguments.ParcelID#-#arguments.taxYear#';
		var cacheKeyCounts = 'q-PropPersistentInfoCounts-#arguments.ParcelID#-#arguments.taxYear#';

		//Clear out the saved Persistent Info cache for this Parcel ID
		if( cache.lookup(cacheKeyInfo) ){
			log.info ( "#cacheKeyInfo# is cleared" );
			cache.clear(cacheKeyInfo);
		}

		//Clear out the saved Persistent Info Totals cache for this Parcel ID
		if( cache.lookup(cacheKeyTotals) ){
			log.info ( "#cacheKeyTotals# is cleared" );
			cache.clear(cacheKeyTotals);
		}

		//Clear out the saved Persistent Info Counts cache for this Parcel ID
		if( cache.lookup(cacheKeyCounts) ){
			log.info ( "#cacheKeyCounts# is cleared" );
			cache.clear(cacheKeyCounts);
		}

	}
}


