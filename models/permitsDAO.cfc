/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";
	property name="cache"	inject="cachebox:default";
	property name="log"		inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	permitsDAO function init(){

		return this;
	}



	/**
     * getPermitInfo method for retrieveing the Parcel Permits Information
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     * @status.hint The Status of the permits that will be displayed. Default is null and will display only the active permits.
     */
	function getPermitInfo( required string parcelID, required string parcelType, required string status ){

		var qGetPermitInfo = [];
		var cacheKey = 'q-GetPermit-#arguments.parcelID#-Status-#arguments.status#';

		if( cache.lookup(cacheKey) ){
			qGetPermitInfo = cache.get(cacheKey);
			log.info ( "#cacheKey# is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetPermitInfo (:parcelID, :type, :status)");
				q.addParam(name="parcelID",	value=arguments.ParcelID, 		cfsqltype="cf_sql_varchar",	null=len(trim(arguments.ParcelID)) ? false : true);
				q.addParam(name="type",		value=arguments.parcelType, 	cfsqltype="cf_sql_varchar",	null=len(trim(arguments.parcelType)) ? false : true);
				q.addParam(name="status",	value=arguments.status,			cfsqltype="cf_sql_varchar",	null=len(trim(arguments.status)) ? false : true);

			qGetPermitInfo = q.execute().getResult();
			cache.set(cacheKey, qGetPermitInfo, 30, 30);
			log.info ( "running #cacheKey# query" );
		}

		return qGetPermitInfo;

	}


	/**
     * getPermitTypes method for retrieveing the list of Parcel Types
     */
	function getPermitTypes( ){

		var qGetPermitTypes = [];
		var cacheKey = 'q-GetPermitTypes';

		if( cache.lookup(cacheKey) ){
			qGetPermitTypes = cache.get(cacheKey);
			log.info ( "#cacheKey# is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetPermitTypes ()");

			qGetPermitTypes = q.execute().getResult();
			cache.set(cacheKey, qGetPermitTypes, 30, 30);
			log.info ( "running #cacheKey# query" );
		}

		return qGetPermitTypes;
	}


	/**
     * setPermitInfo method for Updating the Permit Information for the selected Parcel
     * @parcelID.hint The selected Parcel ID that is being Updated
	 * @parcelType.hint The parcel type of the selected Parcel ID that is being Updated
     * @formData.hint Query Stiructure that contains all of the fields that were submitted on the form to be saved
     */
	function setPermitInfo( required string parcelID, required string parcelType, required query formData ) {
		var result = {status = 200, message="Start", locus = "setPermitInfo"};
		var bContinue = true;
		var stQueryResult = {};
		var cacheKey = 'q-GetPermit-#arguments.parcelID#-Status-#arguments.formData.status#';

		//writeDump(arguments.formData);

		try {

			var q = new Query(datasource="#dsn.name#",sql="call p_SavePermitInfo (
				:p_PermitID, :p_ParcelID, :p_ParcelType, :p_ActualCompletionDate, :p_Amount, :p_FinalInspectionDate,
				:p_DateIssued, :p_JurisdictionCode, :p_PermitName, :p_permitNumber, :p_PermitCode,
				:p_FirstInspectionSchedDate, :p_InspectionSchedDate, :p_issuedTo, :p_issuedBy, :p_status, :p_mode
			)");

			q.addParam(cfsqltype="cf_sql_integer",		name="p_PermitID",					value=arguments.formData.permitID, null=len(trim(arguments.formData.permitID)) ? false : true);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_ParcelID",					value=arguments.parcelID);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_ParcelType",				value=arguments.parcelType);
			q.addParam(cfsqltype="cf_sql_date",			name="p_ActualCompletionDate",		value=arguments.formData.actualCompletionDate, null=len(trim(arguments.formData.actualCompletionDate)) ? false : true);
			q.addParam(cfsqltype="cf_sql_money",		name="p_Amount",					value=arguments.formData.amount);
			q.addParam(cfsqltype="cf_sql_date",			name="p_FinalInspectionDate",		value=arguments.formData.finalInspectionDate, null=len(trim(arguments.formData.finalInspectionDate)) ? false : true);

			q.addParam(cfsqltype="cf_sql_date",			name="p_DateIssued",				value=dateFormat(arguments.formData.dateIssued), null=len(trim(arguments.formData.dateIssued)) ? false : true);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_JurisdictionCode",			value=arguments.formData.jurisdictionCode);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_PermitName",				value=arguments.formData.permitName);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_permitNumber",				value=arguments.formData.permitNumber);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_PermitCode",				value=arguments.formData.permitCode);

			q.addParam(cfsqltype="cf_sql_date",			name="p_FirstInspectionSchedDate",	value=arguments.formData.firstInspectionSchedDate, null=len(trim(arguments.formData.firstInspectionSchedDate)) ? false : true);
			q.addParam(cfsqltype="cf_sql_date",			name="p_InspectionSchedDate",		value=arguments.formData.inspectionSchedDate, null=len(trim(arguments.formData.inspectionSchedDate)) ? false : true);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_issuedTo",					value=arguments.formData.issuedTo);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_issuedBy",					value=arguments.formData.issuedBy);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_status",					value=arguments.formData.status);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_mode",						value=arguments.formData.mode);

/*
			q.addParam(cfsqltype="cf_sql_timestamp",name="recordDate",			value=LSParseDateTime(arguments.formData.recordDate[arguments.row]));

*/
			stQueryResult = q.execute().getPrefix();

			if(stQueryResult.recordcount != 1) {
				result.status = -5;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
				result.qryResults = stQueryResult;

				//Clear out the saved permit cache for this Parcel ID
				if( cache.lookup(cacheKey) ){
					log.info ( "#cacheKey# is cleared" );
					cache.clear(cacheKey);
				}

			};
		} catch(any) {
			result.status = -6;
			result.message = cfcatch;
		}

		//writeDump(result); abort;

		return result;
	}

	/**
     * setPermitResequence method for Resequencing the Permits for the selected Parcel
     * @parcelID.hint The selected Parcel ID that is being Resequenced
     * @status.hint The Status of the permits that will be displayed. Default is null and will display only the active permits.
     * @resequenceList.hint Ordered List of the permits items that are being resequenced. Highest items is at the top of the list
     */
	function setPermitResequence(
						required string parcelID,
						required string status
						required number permitID,
						required number sequenceID,
						required string objectType ) {

		var result = {status = 200, message="Start", locus = "setPermitInfo"};
		var bContinue = true;
		var stQueryResult = {};
		var cacheKey = 'q-GetPermit-#arguments.parcelID#-Status-#arguments.status#';

		//writeDump(arguments);

		try {

			var q = new Query(datasource="#dsn.name#",sql="call p_SaveResequence  (
				:p_UniqueID, :p_SequenceNumber, :p_ObjectType
			)");

			q.addParam(cfsqltype="cf_sql_integer",	name="p_UniqueID",			value=arguments.permitID);
			q.addParam(cfsqltype="cf_sql_integer",	name="p_SequenceNumber",	value=arguments.sequenceID);
			q.addParam(cfsqltype="cf_sql_varchar",	name="p_ObjectType",		value=arguments.objectType);

			stQueryResult = q.execute().getPrefix();

			if(stQueryResult.recordcount != 1) {
				result.status = -5;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
				result.qryResults = stQueryResult;

				//Clear out the saved permit cache for this Parcel ID
				if( cache.lookup(cacheKey) ){
					log.info ( "#cacheKey# is cleared" );
					cache.clear(cacheKey);
				}
			};

		} catch(any) {
			result.status = -6;
			result.message = cfcatch;
		}

		//writeDump(result); abort;

		return result;
	}
}