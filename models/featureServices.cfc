/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dropdownsDAO"	inject=model;
	property name="featureDAO"		inject=model;

	/**
	 * Constructor
	 */
	featureServices function init(){
		return this;
	}

	function getAllFeaturesData(required struct persistentCriteria) {
		var results = {};

		// ----- Stored Proc to retrieve the Features Information ----->
		results.getFeaturesInfo = getFeaturesInfo( persistentCriteria = arguments.persistentCriteria);

		<!----- Stored Proc to retrieve Feature Unit Type Codes ----->
		results.qryFeatureUnitTypeCodes = getFeatureUnitTypeCodes();

		<!----- Stored Proc to retrieve Features Unit Type ----->
		results.getFeaturesCode = getFeaturesCode(taxYear = arguments.persistentCriteria.taxYear);

		<!----- Stored Proc to retrieve Features Quality Codes ----->
		results.getFeaturesQualityCodes = getFeaturesQualityCodes(taxYear = arguments.persistentCriteria.taxYear);

		<!----- Stored Proc to retrieve Neighborhoods ----->
		results.getNeighborhoods = getNeighborhoods( taxYear = arguments.persistentCriteria.taxYear );

		<!----- Stored Proc to retrieve Special Conditions ----->
		results.qryGetSpecialConditions = getSpecialConditions();

		results.error = false;
		results.message = "Features data retrieved successfully.";

		return results;
	}

	function getFeaturesInfo(required struct persistentCriteria) {
		return featureDAO.getFeaturesInfo( persistentCriteria = arguments.persistentCriteria );
	}

	function getFeaturesCode(required number taxYear) {
		return featureDAO.getFeaturesCode( taxYear = arguments.taxYear );
	}

	function getFeaturesQualityCodes(required number taxYear) {
		return featureDAO.getFeaturesQualityCodes( taxYear = arguments.taxYear );
	}

	function getFeatureUnitTypeCodes() {
		return featureDAO.getFeaturesUnitTypeCodes();
	}

	function getNeighborhoods( required string taxYear ) {
		return dropdownsDAO.getNeighborhoods( taxYear = arguments.taxYear );
	}

	function getSpecialConditions() {
		return dropdownsDAO.getSpecialConditionCodes();
	}

	function saveExtraFeaturesInfo(required struct formData) {
		cfwddx(action="WDDX2CFML", input=arguments.formData.extraFeaturesData, output="arguments.formData.qryExtraFeatureData");

		return featureDAO.saveExtraFeaturesInfo(formData = arguments.formData.qryExtraFeatureData);
	}

}