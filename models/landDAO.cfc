/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";

	/**
	 * Constructor
	 */
	landDAO function init(){

		return this;
	}


	function getLandInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetLandInfo (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	function getUserDefinedAdjustmentInfo(required string parcelID, required string taxYear) {
		var q = new Query(datasource="#dsn.name#",sql="call p_getUserDefinedAdjustmentInfo (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");

		return q.execute().getResult();
	}

	function getAppraisalInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call P_getAppraisalInfo (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");

		return q.execute().getResult();
	}

	function getLandCodes(required string taxYear, required string codeType) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetLandCodes (:taxYear, :codeType)");
		q.addParam(name="taxYear",	value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);
		q.addParam(name="codeType",	value=arguments.codeType, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.codeType)) ? false : true);

		return q.execute().getResult();
	}


	function getLandDepthTables(required string taxYear) {
		var q = new Query(datasource="#dsn.name#",sql="call p_getLandDepthTables (:taxYear)");
		q.addParam(name="taxYear",	value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	function getLandDepthTablePercent(
			required string parcelID,
			required string taxYear,
			required string type,
			required number itemNumber) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetLandDepthTablePercent (:parcelID, :taxYear, :type, :itemNumber)");
			q.addParam(name="ParcelID",		value=arguments.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.ParcelID)) ? false : true);
			q.addParam(name="taxYear",		value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="type",			value="R", 					cfsqltype="cf_sql_varchar");
			q.addParam(name="itemNumber",	value=arguments.taxYear, 	cfsqltype="cf_sql_integer", 	null=len(trim(arguments.itemNumber)) ? false : true);

		return q.execute().getResult();
	}


	function getCurrentRowUDFs( query qryLandUDFs, string iIncomingItem, string strIncomingType) {
		/* Query of Queries code! */
		var q = new Query(sql="SELECT * FROM qryLandUDFs WHERE item = :iIncomingItem AND laadjtyp = :strIncomingType");
		/* Denote this is a QoQ */
		q.setAttributes(dbtype="query");
		/* Set the query we will be using */
		q.setAttributes(qryLandUDFs = arguments.qryLandUDFs);
		/* Set the parameters for the where clause */
		q.addParam(name="iIncomingItem",	value=ARGUMENTS.iIncomingItem, 		cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.iIncomingItem)) ? false : true);
		q.addParam(name="strIncomingType",	value=ARGUMENTS.strIncomingType, 	cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.strIncomingType)) ? false : true);

		return q.execute().getResult();
	}

	function getLandTypeBasedOnCode(string typeCode, string typeYear) {
		var q = new Query(datasource="#dsn.name#", sql="CALL p_GetLandTypeBasedOnCode (:typeCode, :typeYear)");
		q.addParam(name="typeCode",	value=ARGUMENTS.typeCode, 	cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.typeCode)) ? false : true);
		q.addParam(name="typeYear",	value=ARGUMENTS.typeYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.typeYear)) ? false : true);

		return q.execute().getResult();
	}

	/* Original adjustments SP */
	function getAdjustmentTypes(required string taxYear, required string adjDesc) {
		var q = new Query(datasource="#dsn.name#", sql="call p_GetAdjustmentTypes (:taxYear, :adjDesc)");
			q.clearParams();
			q.addParam(name="taxYear",	value=ARGUMENTS.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.taxYear)) ? false : true);
			q.addParam(name="adjDesc",	value=ARGUMENTS.adjDesc, 	cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.adjDesc)) ? false : true);

		return q.execute().getResult();
	}

	/* Adjustments SP. Returns additional columns needed for adjustments popup */
	function getLandAdjustments(required string landid, required string adjDesc) {
		var q = new Query(datasource="#dsn.name#", sql="call p_getLandAttributes (:landid, :adjDesc)");
			q.clearParams();
			q.addParam(name="landid",	value=ARGUMENTS.landid, 	cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.landid)) ? false : true);
			q.addParam(name="adjDesc",	value=ARGUMENTS.adjDesc, 	cfsqltype="cf_sql_varchar", 	null=len(trim(ARGUMENTS.adjDesc)) ? false : true);

		return q.execute().getResult();
	}

	function saveLandInfo(required struct formData) {
		var result = {status = 200, message="Start", locus = "saveLandInfo"};
		var stQueryResult = {};

		try {

			result.status = 200;
			result.message = "Success";
		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}

		return result;
	}

	function getNewDepthPercentage(string landID, string depthTableCode, string depth) {
		var q = new Query(datasource="#dsn.name#", sql="call p_GetLandDepthAdjustment (:landID, :depthTableCode, :depth)");
		q.addParam(name="landID", value=ARGUMENTS.landID, cfsqltype="cf_sql_varchar", null=len(trim(ARGUMENTS.landID)) ? false : true);
		q.addParam(name="depthTableCode", value=ARGUMENTS.depthTableCode, cfsqltype="cf_sql_varchar", null=len(trim(ARGUMENTS.depthTableCode)) ? false : true);
		q.addParam(name="depth", value=ARGUMENTS.depth, cfsqltype="cf_sql_varchar", null=len(trim(ARGUMENTS.depth)) ? false : true);

		return q.execute().getResult();
	}

}