/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="permitsDAO" 		inject=model;
	property name="dropdownsDAO"	inject=model;


	/**
	 * Constructor
	 */
	permitsServices function init(){

		return this;
	}


	/**
     * getAllPermitData method for retrieveing all of the Parcel Permits Data
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     * @status.hint The Status of the permits that will be displayed. Default is null and will display only the active permits.
     */
	function getAllPermitData( required string parcelID, required string parcelType, required string status ){

		var results = {};
		var intOptTableVar = "";

		// ----- Stored Proc to retrieve the Permit Information ----->
		results.getPermitsInfo = getPermitInfo(
										parcelID = arguments.parcelID,
										parcelType = arguments.parcelType,
										status = arguments.status
										);


		<!----- Stored Proc to retrieve the Permit Types ----->
		results.getPermitTypes = getPermitTypes();

		<!----- Stored Proc to retrieve the Opt Table values for the Issued To drop down ----->
		intOptTableVar = 2;
		results.getIssuedTo = dropdownsDAO.getOptTableValues( fieldType = intOptTableVar );

		<!----- Stored Proc to retrieve the Opt Table values for the Issued By drop down ----->
		intOptTableVar = 3;
		results.getIssuedBy = dropdownsDAO.getOptTableValues( fieldType = intOptTableVar );

		results.error = false;
		results.message = "Permit data retrieved successfully.";

		return results;
	}


	/**
     * getAddPermitsData method for retrieveing the drop down values for the Add Permit form
     * @returns.hint The Status of the permits that will be displayed. Default is null and will display only the active permits.
     */
	function getAddPermitsData(){

		var results = {};
		var intOptTableVar = "";

		<!----- Stored Proc to retrieve the Permit Types ----->
		results.getPermitTypes = getPermitTypes();

		<!----- Stored Proc to retrieve the Opt Table values for the Issued To drop down ----->
		intOptTableVar = 2;
		results.getIssuedTo = dropdownsDAO.getOptTableValues( fieldType = intOptTableVar );

		<!----- Stored Proc to retrieve the Opt Table values for the Issued By drop down ----->
		intOptTableVar = 3;
		results.getIssuedBy = dropdownsDAO.getOptTableValues( fieldType = intOptTableVar );

		results.error = false;
		results.message = "Permit data retrieved successfully.";

		return results;
	}


	/**
     * getPermitInfo method for retrieveing the Parcel Permits Information
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     * @status.hint The Status of the permits that will be displayed. Default is null and will display only the active permits.
     */
	function getPermitInfo( required string parcelID, required string parcelType, required string status ){

		return permitsDAO.getPermitInfo(
								parcelID = arguments.parcelID,
								parcelType = arguments.parcelType,
								status = arguments.status
								);
	}


	/**
     * getPermitTypes method for retrieveing the list of Parcel Types
     */
	function getPermitTypes( ){

		return permitsDAO.getPermitTypes();
	}


	/**
     * setPermitInfo method for Updating the Permit Information for the selected Parcel
     * @parcelID.hint The selected Parcel ID that is being Updated
	 * @parcelType.hint The parcel type of the selected Parcel ID that is being Updated
     * @formData.hint Query Stiructure that contains all of the fields that were submitted on the form to be saved
     */
	function setPermitInfo( required string parcelID, required string parcelType, required struct formData ) {

		cfwddx(action="WDDX2CFML", input=arguments.formData.permitsData, output="arguments.formData.qryPermitsData");

		//writeDump(arguments.formData.qryPermitsData); abort;

		stResult = permitsDAO.setPermitInfo( parcelID = arguments.parcelID,
											parcelType = arguments.parcelType,
											formData = arguments.formData.qryPermitsData );

		return stResult;
	}


	/**
     * setPermitResequence method for Resequencing the Permits for the selected Parcel
     * @parcelID.hint The selected Parcel ID that is being Resequenced
     * @resequenceList.hint Ordered List of the permits items that are being resequenced. Lowest item is at the bottom of the list
     * @status.hint The Status of the permits that will be displayed. Default is null and will display only the active permits.
     */
	function setPermitResequence( required string parcelID, required string resequenceList, required string status ) {

		var strObjectType = "permit";
		var arrResequence = arguments.resequenceList.split(",");

		//writeDump(arguments.resequenceList);
		//writeDump(arrResequence);

		for (i=1;i LTE ArrayLen(arrResequence);i=i+1) {
			//WriteOutput(arrResequence[i]);
			//WriteOutput("#i# -- #arrResequence[i]# : #LSParseNumber(arrResequence[i])# : #isNumeric(arrResequence[i])#<br>");

			stResult = permitsDAO.setPermitResequence(
										parcelID = arguments.parcelID,
										status = arguments.status,
										permitID = int(arrResequence[i]),
										sequenceID = i,
										objectType = strObjectType );

		}

		return stResult;
	}

}