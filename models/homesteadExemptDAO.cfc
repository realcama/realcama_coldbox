/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";

	/**
	 * Constructor
	 */
	homesteadExemptDAO function init(){

		return this;
	}

	/**
	* getHexInfo
	*/
	function getHexInfo(){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetHXInfo (:parcelID, :taxYear, :propertyType)");
		q.addParam(name="parcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="propertyType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	/**
	* setHexInfo
	*/
	function setHexInfo(){

	}


}