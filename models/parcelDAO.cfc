/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";
	property name="dropdownsDAO"	inject=model;

	/**
	 * Constructor
	 */
	parcelDAO function init(){

		return this;
	}
	function getStates() {
		var intOptTableVar = "";
		<!----- Stored Proc to retrieve the Opt Table values for the Issued To drop down ----->
		intOptTableVar = 1;
		results.getOptTableVars = dropdownsDAO.getOptTableValues( fieldType = intOptTableVar );

		return results.getOptTableVars;
	}

	function getCountries() {
		var intOptTableVar = "";
		<!----- Stored Proc to retrieve the Opt Table values for the Issued To drop down ----->
		intOptTableVar = 57;
		results.getOptTableVars = dropdownsDAO.getOptTableValues( fieldType = intOptTableVar );

		return results.getOptTableVars;
	}

	function getInstrumentType(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="CALL p_GetSalesInstrumentCodes");
		q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
		return q.execute().getResult();
	}
	function getSalesChangeCodes() {
		var q = new Query(datasource="#dsn.name#",sql="CALL p_GetSalesChangeCodes");
		q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
		return q.execute().getResult();
	}
	function getSalesReasonCodes(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="CALL p_GetSalesReasonCodes (:taxYear)");
		q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		return q.execute().getResult();
	}
	function getExemptionCodes() {
		var q = new Query(datasource="#dsn.name#",sql="CALL p_GetParcelExemptionCodes");
		q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
		return q.execute().getResult();
	}
	/*
	function getGetParcelLegalInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelLegalInfo (:parcelID, :taxYear, :propType)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}
	*/



	function getParcelInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelInfo (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}
	function getParcelSitus(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelSitus (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}
	function getGetParcelSales(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelSales (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}
	function getGetParcelSalesMultiParcelInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelSalesMultiParcelInfo (:parcelID, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}
	function getGetParcelLegalInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelLegalInfo (:parcelID, :taxYear, :propType)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}



	function getGetParcelExemptionInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelExemptionInfo (:parcelID, :taxYear, :propType)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}
	function getGetParcelExemptionApplicationInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelExemptionApplicationInfo (:parcelID, :taxYear, :propType)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	function getSpecificParcelUseCode(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetSpecificParcelUseCode (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}



	function setParcelInfo(required struct persistentCriteria, required query formData) {
		var result = {status = 200, message="Start", locus = "setParcelInfo"};
		var bContinue = true;
		var stQueryResult = {};

		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_SaveParcelInfo (
				:p_parcelMasterID, :p_ParcelID, :p_TaxYear, :p_OwnerName, :p_OwnerName2, :p_pamara, :p_Name, :p_Address1,
				:p_Address2, :p_Address3, :p_City, :p_State, :p_Country, :p_Zipcode, :p_Zipcode4, :p_TaxDistrict, :p_TaxCity,
				:p_TaxCounty, :p_Resoh, :p_Retagl, :p_Renew, :p_RenewA, :p_UseCode, :p_NeighborhoodCode, :p_CondoID,
				:p_Subdivision, :p_JustValueChangeCode, :p_Confidential
			)");

			for(var i=1; i LTE arguments.formData.recordcount; i=i+1) {
				if(result.status EQ 200) {
					q.clearParams();

					q.addParam(cfsqltype="cf_sql_varchar",	name="p_parcelMasterID",		value=arguments.formData.parcelMasterID);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_ParcelID",				value=arguments.persistentCriteria.parcelID);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_TaxYear",				value=arguments.persistentCriteria.taxYear);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_OwnerName",				value=arguments.formData.OwnerName[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_OwnerName2",			value=arguments.formData.Ownername2[i]);
					<!--- q.addParam(cfsqltype="cf_sql_varchar",	name="MarketAreaCode",			value=arguments.formData.MarketAreaCode[i]); --->
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_pamara",				value='');
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Name",					value='');
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Address1",				value=arguments.formData.ownerAddress1[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Address2",				value=arguments.formData.ownerAddress2[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Address3",				value=arguments.formData.ownerAddress3[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_City",					value=arguments.formData.ownerCity[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_State",					value=arguments.formData.ownerState[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Country",				value=arguments.formData.ownerCountry[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Zipcode",				value=arguments.formData.ownerZip[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Zipcode4",				value='');
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_TaxDistrict",			value=arguments.formData.asfltxdt[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_TaxCity",				value='');
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_TaxCounty",				value='');
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Resoh",					value=arguments.formData.asflresoh[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Retagl",				value=arguments.formData.asflretagl[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Renew",					value=arguments.formData.asflrenew[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_RenewA",				value=arguments.formData.asflrenewa[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_UseCode",				value=arguments.formData.UseCode[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_NeighborhoodCode",		value=arguments.formData.NeighborhoodCode[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_CondoID",				value=arguments.formData.CondoID[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Subdivision",			value=arguments.formData.Subdivision[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_JustValueChangeCode",	value=arguments.formData.pvjvchgcd[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="p_Confidential",			value=arguments.formData.confidential[i]);

					stQueryResult = q.execute().getPrefix();

					if(stQueryResult.recordcount != 1) {
						result.status = -1;
						result.message = stQueryResult;
					} else {
						result.status=200;
						result.message = "Success";
					};
				} /*<!--- END result.status eq 200 --->*/
			}
		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}

		return result;
	}

	function setSitusInfo(required struct persistentCriteria, required query formData) {
		var result = {status = 200, message="Start", locus = "setSitusInfo"};
		var bContinue = true;
		var stQueryResult = {};

		try{

			var q = new Query(datasource="#dsn.name#",sql="call p_SaveParcelSitus (
				:parcelAddressID, :parcelID, :taxYear, :SitusHousePrefix, :SitusHouseNumber, :SitusHouseSuffix, :SitusStreetName,
				:SitusStreetType, :SitusStreetDirection, :situsaptnumber, :SitusCity, :SitusState, :SitusZip4, :SitusZip
			)");

			for(var i=1; i LTE arguments.formData.recordcount; i=i+1) {
				if(result.status EQ 200) {
					q.clearParams();
					q.addParam(cfsqltype="cf_sql_varchar",	name="parcelAddressID",			value=arguments.formData.parcelAddressID);
					q.addParam(cfsqltype="cf_sql_varchar",	name="parcelID",				value=arguments.persistentCriteria.parcelID);
					q.addParam(cfsqltype="cf_sql_varchar",	name="taxYear",					value=arguments.persistentCriteria.taxYear);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusHousePrefix",		value=arguments.formData.SitusHousePrefix[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusHouseNumber",		value=arguments.formData.SitusHouseNumber[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusHouseSuffix",		value=arguments.formData.SitusHouseSuffix[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusStreetName",			value=arguments.formData.SitusStreetName[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusStreetType",			value=arguments.formData.SitusStreetType[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusStreetDirection",	value=arguments.formData.SitusStreetDirection[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="situsaptnumber",			value=arguments.formData.situsaptnumber[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusCity",				value=arguments.formData.SitusCity[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusState",				value=arguments.formData.SitusState[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusZip4",				value=arguments.formData.SitusZip4[i]);
					q.addParam(cfsqltype="cf_sql_varchar",	name="SitusZip",				value=arguments.formData.SitusZip[i]);

					stQueryResult = q.execute().getPrefix();

					if(stQueryResult.recordcount != 1) {
						result.status = -3;
						result.message = stQueryResult;
					} else {
						result.status=200;
						result.message = "Success";
					};
				} /*<!--- END result.status eq 200 --->*/
			}

		} catch(any){
			result.status = -4;
			result.message = cfcatch;
		}

		return result;
	}


	function setSalesInfo(required struct persistentCriteria, required query formData, required string row) {
		var result = {status = 200, message="Start", locus = "setSalesInfo"};
		var bContinue = true;
		var stQueryResult = {};

		try {

			var q = new Query(datasource="#dsn.name#",sql="call p_SaveParcelSales (
				:saleMasterID, :parcelID, :taxYear, :propertyType, :saleReasonCode, :grantee, :grantor, :InstrumentType,
				:itemNumber, :saleChangeDate, :salePrice, :qualifiedFlag, :bookNumber,
				:recordDate, :pageNumber, :saleChangeCode, :snapshotParcelID, :documentStampAmt, :VacantImprovedFlag
			)");


			q.addParam(cfsqltype="cf_sql_varchar",	name="saleMasterID",		value=arguments.formData.saleMasterID);
			q.addParam(cfsqltype="cf_sql_varchar",	name="parcelID",			value=arguments.persistentCriteria.parcelID);
			q.addParam(cfsqltype="cf_sql_varchar",	name="taxYear",				value=arguments.persistentCriteria.taxYear);
			q.addParam(cfsqltype="cf_sql_varchar",	name="propertyType",		value=arguments.formData.propertyType[arguments.row]);

			q.addParam(cfsqltype="cf_sql_varchar",	name="saleReasonCode",		value=arguments.formData.saleReasonCode[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="grantee",				value=arguments.formData.grantee[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="grantor",				value=arguments.formData.grantor[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="InstrumentType",		value=arguments.formData.InstrumentType[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="itemNumber",			value=arguments.formData.itemNumber[arguments.row]);
			q.addParam(cfsqltype="cf_sql_timestamp",name="saleChangeDate",		value=LSParseDateTime(arguments.formData.saleChangeDate[arguments.row]));
			q.addParam(cfsqltype="cf_sql_varchar",	name="salePrice",			value=arguments.formData.salePrice[arguments.row]);

			q.addParam(cfsqltype="cf_sql_varchar",	name="qualifiedFlag",		value=arguments.formData.qualifiedFlag[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="bookNumber",			value=arguments.formData.bookNumber[arguments.row]);
			q.addParam(cfsqltype="cf_sql_timestamp",name="recordDate",			value=LSParseDateTime(arguments.formData.recordDate[arguments.row]));
			q.addParam(cfsqltype="cf_sql_varchar",	name="pageNumber",			value=arguments.formData.pageNumber[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="saleChangeCode",		value=arguments.formData.saleChangeCode[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="snapshotParcelID",	value=arguments.formData.snapshotParcelID[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="documentStampAmt",	value=arguments.formData.documentStampAmt[arguments.row]);
			q.addParam(cfsqltype="cf_sql_varchar",	name="VacantImprovedFlag",	value=arguments.formData.VacantImprovedFlag[arguments.row]);

			stQueryResult = q.execute().getPrefix();

			if(stQueryResult.recordcount != 1) {
				result.status = -5;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};
		} catch(any) {
			result.status = -6;
			result.message = cfcatch;
		}
		return result;
	}


	function setLegalInfo(required struct persistentCriteria, required query formData) {
		var result = {status = 200, message="Start", locus="setLegalInfo"};
		var bContinue = true;
		var stQueryResult = {};

		//writedump(arguments.formData);
		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_SaveParcelLegal (
				:parcelID, :taxYear, :LegalID, :PropertyType, :LegalDescription, :editByName
			)");

			for(var i=1; i LTE arguments.formData.recordcount; i=i+1) {
				if(result.status EQ 200) {
					q.clearParams();
					q.addParam(cfsqltype="cf_sql_varchar",		name="parcelID",			value=arguments.persistentCriteria.parcelID);
					q.addParam(cfsqltype="cf_sql_varchar",		name="taxYear",				value=arguments.persistentCriteria.taxYear);
					//q.addParam(cfsqltype="cf_sql_varchar",		name="LegalDescriptionID",	value=arguments.formData.qryPacketOneData.LegalDescriptionID[i]);
					q.addParam(cfsqltype="cf_sql_varchar",		name="LegalID",				value=arguments.formData.LegalID[i]);
					q.addParam(cfsqltype="cf_sql_varchar",		name="PropertyType",		value=arguments.formData.PropertyType[i]);
					q.addParam(cfsqltype="cf_sql_longvarchar",	name="LegalDescription",	value=arguments.formData.LegalDescription[i]);
					q.addParam(cfsqltype="cf_sql_varchar",		name="editByName",			value="");
					//q.addParam(cfsqltype="cf_sql_varchar",		name="editByName",			value=Controller.getSetting( "editByName"));

					stQueryResult = q.execute().getPrefix();

					if(stQueryResult.recordcount != 1) {
						result.status = 404;
						result.message = stQueryResult;
					} else {
						result.status=200;
						result.message = "Success";
					};
				} /*<!--- END result.status eq 200 --->*/
			}
		} catch(any) {
			result.status = -7;
			result.message = cfcatch;
		}

		return result;
	}



	function setExemptionInfo(required struct persistentCriteria, required query formDataOne, required query formDataTwo) {
		var result = {status = 200, message="Start", locus="setLegalInfo"};
		var bContinue = true;
		var stQueryResult = {};

		writedump(arguments.formDataOne);

		writedump(arguments.formDataTwo);

		try {
			var q1 = new Query(datasource="#dsn.name#",sql="call p_SaveParcelExemptionInfo (
				:parcelID, :taxYear, :PropertyType, :exemptionID, :exemptionCode, :ExemptionAmount, :ExemptionPct, :mode
			)");

			for(var i=1; i LTE arguments.formDataOne.recordcount; i=i+1) {
				q1.clearParams();
				if(result.status EQ 200) {
					q1.addParam(cfsqltype="cf_sql_varchar",	name="parcelID",		value=arguments.persistentCriteria.parcelID);
					q1.addParam(cfsqltype="cf_sql_varchar",	name="taxYear",			value=arguments.persistentCriteria.taxYear);
					q1.addParam(cfsqltype="cf_sql_varchar",	name="PropertyType",	value=arguments.formDataOne.PropertyType[i]);
					q1.addParam(cfsqltype="cf_sql_varchar",	name="exemptionID",		value=arguments.formDataOne.exemptionID[i]);
					q1.addParam(cfsqltype="cf_sql_varchar",	name="exemptionCode",	value=arguments.formDataOne.exemptionCode[i]);
					q1.addParam(cfsqltype="cf_sql_varchar",	name="ExemptionAmount",	value=arguments.formDataOne.ExemptionAmount[i]);
					q1.addParam(cfsqltype="cf_sql_varchar",	name="ExemptionPct",	value=arguments.formDataOne.ExemptionPct[i]);
					q1.addParam(cfsqltype="cf_sql_varchar",	name="mode",			value=arguments.formDataOne.mode[i]);

					stQueryResult = q1.execute().getPrefix();

					/*
					if(stQueryResult.recordcount != 1) {
						result.status = 404;
						result.message = stQueryResult;
					} else {
						result.status=200;
						result.message = "Success";
					};
					*/
				} /*<!--- END result.status eq 200 --->*/
			}


			var q2 = new Query(datasource="#dsn.name#",sql="call p_SaveParcelExemptionApplicationInfo (
				:parcelID, :taxYear, :PropertyType, :asflhxappl, :asflnumown, :asflnumclm, :pvamdays, :editByName
			)");

			for(var i=1; i LTE arguments.formDataTwo.recordcount; i=i+1) {
				q2.clearParams();
				if(result.status EQ 200) {
					q2.addParam(cfsqltype="cf_sql_varchar",	name="parcelID",		value=arguments.persistentCriteria.parcelID);
					q2.addParam(cfsqltype="cf_sql_varchar",	name="taxYear",			value=arguments.persistentCriteria.taxYear);
					q2.addParam(cfsqltype="cf_sql_varchar",	name="PropertyType",	value=arguments.formDataOne.PropertyType[i]);
					q2.addParam(cfsqltype="cf_sql_varchar",	name="asflhxappl",		value=arguments.formDataTwo.asflhxappl);
					q2.addParam(cfsqltype="cf_sql_varchar",	name="asflnumown",		value=arguments.formDataTwo.asflnumown);
					q2.addParam(cfsqltype="cf_sql_varchar",	name="asflnumclm",		value=arguments.formDataTwo.asflnumclm);
					q2.addParam(cfsqltype="cf_sql_varchar",	name="pvamdays",		value=arguments.formDataTwo.pvamdays);
					q2.addParam(cfsqltype="cf_sql_varchar",	name="editByName",		value='');
					//q2.addParam(cfsqltype="cf_sql_varchar",	name="editByName",		value=Controller.getSetting( "editByName"));

					stQueryResult = q2.execute().getPrefix();
					/*
					if(stQueryResult.recordcount != 1) {
						result.status = -9;
						result.message ="Failure";
					} else {
						result.status=200;
						result.message ="Success";
					};*/
				} /*<!--- END result.status eq 200 --->*/
			}

		} catch(any) {
			result.status = -10;
			result.message = cfcatch;
		}

		//writedump(result);

		return result;
	}
}

