/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="condoUnitDAO"		inject=model;
	property name="dropdownsDAO"		inject=model;
	property name="log"					inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	condounitServices function init(){

		return this;
	}


	function getCondoUnitInfo(required struct persistentCriteria){
		return condoUnitDAO.getCondoUnitInfo( persistentCriteria = arguments.persistentCriteria );
	}

	function getNeighborhoods( required string taxYear ){
		return dropdownsDAO.getNeighborhoods( taxYear = arguments.taxYear );
	}

	function getSpecialConditionCodes(){
		return dropdownsDAO.getSpecialConditionCodes( );
	}

	function getCondoUnitSpecificCodes(required string condoUnitID){
		log.info ( "inside condoUnitServices" );
		return condoUnitDAO.getCondoUnitSpecificCodes( arguments.condoUnitID );
	}


	function setCondoUnitInfo(required struct persistentCriteria, required struct formData) {
		//writeDump(formData);
		//abort;

		<!--- Convert the data packet --->
		cfwddx(action="WDDX2CFML", input=arguments.formData.condoUnitData, output="arguments.formData.qryPacketOneData");
		cfwddx(action="WDDX2CFML", input=arguments.formData.condoUnitCodesData, output="arguments.formData.qryPacketTwoData");

		stResult = condoUnitDAO.setCondoUnitInfo(persistentCriteria = arguments.persistentCriteria, formData1 = arguments.formData.qryPacketOneData, formData2 = arguments.formData.qryPacketTwoData);
		return stResult;
	}

}