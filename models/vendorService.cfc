/**
* I am a new Model Object
*/
component accessors="true" singleton{
	
	// Properties
	property name="vendorDAO"	inject=model;
	property name="log"			inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	vendorService function init(){
		
		return this;
	}
	

	function getVendorRecord(strDomainName){

		log.info( "Inside getVendorRecord Service ");
		var qryVenderDAO = vendorDAO.getRecord(strDomainName);

		return qryVenderDAO;
	}


}