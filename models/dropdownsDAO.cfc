/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn"		inject="coldbox:datasource:ws_realcama_nassau";
	property name="cache"	inject="cachebox:default";
	property name="log"		inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	dropdownsDAO function init(){

		return this;
	}


	query function getSubdivisions( required string taxYear ){
		var qGetSubdivisions = [];
		var cacheKey = 'q-Subdivisions';

		if( cache.lookup(cacheKey) ){
			qGetSubdivisions = cache.get(cacheKey);
			log.info ( "qGetSubdivisions is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetSubdivisions (:taxYear)");
				q.addParam(name="taxYear",	value=arguments.taxYear,	cfsqltype="cf_sql_varchar",	null=len(trim(arguments.taxYear)) ? false : true);

			qGetSubdivisions = q.execute().getResult();
			cache.set(cacheKey, qGetSubdivisions, 30, 30);
			log.info ( "running qGetSubdivisions query" );
		}

		return qGetSubdivisions;
	}


	query function getNeighborhoods(required string taxYear){
		var qGetNeighborhoods = [];
		var cacheKey = 'q-Neighborhoods';

		if( cache.lookup(cacheKey) ){
			qGetNeighborhoods = cache.get(cacheKey);
			log.info ( "qGetNeighborhoods is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetNeighborhoods (:taxYear)");
				q.addParam(name="taxYear",	value=arguments.taxYear,	cfsqltype="cf_sql_varchar",	null=len(trim(arguments.taxYear)) ? false : true);
//				q.addParam(name="taxYear",	value='',	cfsqltype="cf_sql_varchar");

			qGetNeighborhoods = q.execute().getResult();
			cache.set(cacheKey, qGetNeighborhoods, 30, 30);
			log.info ( "running qGetNeighborhoods query" );
		}

		return qGetNeighborhoods;
	}


	query function getPrimaryUse(){
		var qGetPrimaryUses = [];
		var cacheKey = 'q-PrimaryUse';

		if( cache.lookup(cacheKey) ){
			qGetPrimaryUses = cache.get(cacheKey);
			log.info ( "qGetPrimaryUses is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetUseCodes ()");

			qGetPrimaryUses = q.execute().getResult();
			cache.set(cacheKey, qGetPrimaryUses, 30, 30);
			log.info ( "running qGetPrimaryUses query" );
		}

		return qGetPrimaryUses;
	}


	query function getSpecialConditionCodes(){
		var qGetSpecialConditionCodess = [];
		var cacheKey = 'q-SpecialConditionCodes';

		if( cache.lookup(cacheKey) ){
			qGetSpecialConditionCodess = cache.get(cacheKey);
			log.info ( "qGetSpecialConditionCodess is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_getSpecialConditionCodes ()");

			qGetSpecialConditionCodess = q.execute().getResult();
			cache.set(cacheKey, qGetSpecialConditionCodess, 30, 30);
			log.info ( "running qGetSpecialConditionCodess query" );
		}

		return qGetSpecialConditionCodess;
	}


	query function getBuildingTypes(){
		var qGetBuildingTypes = [];
		var cacheKey = 'q-BuildingTypes';

		if( cache.lookup(cacheKey) ){
			qGetBuildingTypes = cache.get(cacheKey);
			log.info ( "qGetBuildingTypes is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingTypes ()");

			qGetBuildingTypes = q.execute().getResult();
			cache.set(cacheKey, qGetBuildingTypes, 30, 30);
			log.info ( "running qGetBuildingTypes query" );
		}

		return qGetBuildingTypes;
	}


	query function getTaxDistricts( required string taxYear ){
		var qGetTaxDistricts = [];
		var cacheKey = 'q-TaxDistricts-#arguments.taxYear#';

		if( cache.lookup(cacheKey) ){
			qGetTaxDistricts = cache.get(cacheKey);
			log.info ( "qGetTaxDistricts is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetTaxDistricts (:taxYear)");
				q.addParam(name="taxYear",	value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

			qGetTaxDistricts = q.execute().getResult();
			cache.set(cacheKey, qGetTaxDistricts, 30, 30);
			log.info ( "running qGetTaxDistricts query" );
		}

		return qGetTaxDistricts;
	}


	query function getLandUseCodes(required struct persistentCriteria){
		var qGetLandUseCodes = [];
		var cacheKey = 'q-LandUseCodes';

		if( cache.lookup(cacheKey) ){
			qGetLandUseCodes = cache.get(cacheKey);
			log.info ( "qGetLandUseCodes is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetLandUseCodes (:taxYear)");
				q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);

			qGetLandUseCodes = q.execute().getResult();
			cache.set(cacheKey, qGetLandUseCodes, 30, 30);
			log.info ( "running qGetLandUseCodes query" );
		}

		return qGetLandUseCodes;
	}


	query function getStatesList(){
		var qGetStatesList = [];
		var cacheKey = 'q-StatesList';

		if( cache.lookup(cacheKey) ){
			qGetStatesList = cache.get(cacheKey);
			log.info ( "qGetStatesList is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetStates ()");

			qGetStatesList = q.execute().getResult();
			cache.set(cacheKey, qGetStatesList, 30, 30);
			log.info ( "running qGetStatesList query" );
		}

		return qGetStatesList;
	}

	query function getGenericExemptions(){
		var qExemptions = [];
		var cacheKey = 'q-Exemptions';

		if( cache.lookup(cacheKey) ){
			qExemptions = cache.get(cacheKey);
			log.info ( "qExemptions is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelExemptionCodes ()");

			qExemptions = q.execute().getResult();
			cache.set(cacheKey, qExemptions, 30, 30);
			log.info ( "running qExemptions query" );
		}

		return qExemptions;
	}


	query function getOptTableValues( required number fieldType ){
		var qGetPrimaryUse = [];
		var cacheKey = 'q-getOptTableValues-#arguments.fieldType#';

		if( cache.lookup(cacheKey) ){
			qGetPrimaryUse = cache.get(cacheKey);
			log.info ( "#cacheKey# is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_getOptTableValues (:p_FieldType)");
				q.addParam(name="p_FieldType",	value=arguments.fieldType, 	cfsqltype="cf_sql_integer", 	null=len(trim(arguments.fieldType)) ? false : true);

			qGetPrimaryUse = q.execute().getResult();
			cache.set(cacheKey, qGetPrimaryUse, 30, 30);
			log.info ( "running #cacheKey# query" );
		}

		return qGetPrimaryUse;
	}

}