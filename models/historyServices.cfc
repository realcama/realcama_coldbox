/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="historyDAO" 	inject=model;

	/**
	 * Constructor
	 */
	historyServices function init(){

		return this;
	}


	/**
     * getHistoryInfo method for retrieveing the Parcel History Information
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     */
	function getHistoryInfo( required string parcelID, required string taxYear, required string parcelType ){

		return historyDAO.getHistoryInfo(
								parcelID = arguments.parcelID,
								taxYear = arguments.taxYear,
								parcelType = arguments.parcelType
								);
	}


	/**
     * getMillageRates method for retrieveing the Millage Rates for the selected Parcel
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     */
	function getMillageRates( required string parcelID, required string taxYear, required string parcelType ){

		return historyDAO.getMillageRates(
								parcelID = arguments.parcelID,
								taxYear = arguments.taxYear,
								parcelType = arguments.parcelType
								);
	}


	/**
     * getTaxRates method for retrieveing the Tax Rates for the selected Parcel
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     */
	function getTaxRates( required string parcelID, required string taxYear, required string parcelType ){

		return historyDAO.getTaxRates(
								parcelID = arguments.parcelID,
								taxYear = arguments.taxYear,
								parcelType = arguments.parcelType
								);

	}


	boolean function getEandIFlag( required query getHistoryInfo, required number startRow, required number iYearsToDisplay ) {

		var bEandIDefined = false;

		for( var iLoopCounter = arguments.startRow; iLoopCounter LTE arguments.iYearsToDisplay ; iLoopCounter = iLoopCounter + 1) {

			if( LEN(TRIM(arguments.getHistoryInfo.EINumber[iLoopCounter])) NEQ 0) {
				bEandIDefined = true;
			}

		}
		return bEandIDefined;

	}



}