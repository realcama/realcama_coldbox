/**
* I am a new Model Object
*/
component accessors="true"{
	
	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";

	/**
	 * Constructor
	 */
	compareDAO function init(){
		
		return this;
	}
	
	query function getComparisonTabInfo(parcelID, taxYear, propertyType){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetComparisonTabInfo (:parcelID, :taxYear, :propertyType)");
			q.addParam(cfsqltype="CF_SQL_VARCHAR", name="ParcelID", value="#arguments.parcelID#", null="No");
			q.addParam(cfsqltype="CF_SQL_INTEGER", name="TaxYear", value="#arguments.taxYear#", null="No");
			q.addParam(cfsqltype="CF_SQL_VARCHAR", name="PropertyType", value="#arguments.propertyType#", null="No");
		return q.execute().getResult();		
	}

	query function getLandImage(parcelID, taxYear, propertyType){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetLandImage (:parcelID, :taxYear, :propertyType)");
			q.addParam(cfsqltype="CF_SQL_VARCHAR", name="ParcelID", value="#arguments.parcelID#", null="No");
			q.addParam(cfsqltype="CF_SQL_INTEGER", name="TaxYear", value="#arguments.taxYear#", null="No");
			q.addParam(cfsqltype="CF_SQL_VARCHAR", name="PropertyType", value="#arguments.propertyType#", null="No");
		return q.execute().getResult();		
	}

/*

	query function getSearchSavedSearches(userID, currentTab) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetSaveSearches (:userID, :currentTab)");			
		q.addParam(cfsqltype="CF_SQL_INTEGER", name="userID", value="#arguments.userID#", null="No");
		q.addParam(cfsqltype="CF_SQL_VARCHAR", name="currentTab", value="#arguments.currentTab#", null="No");		
		return q.execute().getResult();		
	}




	*/
}