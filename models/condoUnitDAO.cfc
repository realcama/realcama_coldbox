/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" 	inject="coldbox:datasource:ws_realcama_nassau";
	property name="log" 	inject="logbox:logger:{this}";


	/**
	 * Constructor
	 */
	condoUnitDAO function init(){

		return this;
	}

	function getCondoUnitInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoUnitInfo (:parcelID, :taxYear, :propType)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");

		return q.execute().getResult();
	}


	function getCondoUnitSpecificCodes(required string condoUnitID) {
		log.info( 'condoUnitID: ' & arguments.condoUnitID );

		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoUnitSpecificCodes (:condoUnitID)");
			q.addParam(name="condoUnitID",	value=arguments.condoUnitID, 				cfsqltype="cf_sql_integer", 	null=len(trim(arguments.condoUnitID)) ? false : true);
		return q.execute().getResult();
	}


	function getCondoCodes(required integer taxYear, required string condoID, required string codeType) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoCodes (:condoUnitID)");
			q.addParam(name="taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_integer", 	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="condoID",		value=arguments.condoID,		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.condoUnitID)) ? false : true);
			q.addParam(name="codeType",		value=arguments.codeType,		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.codeType)) ? false : true);

		return q.execute().getResult();
	}


	function setCondoUnitInfo(required struct persistentCriteria, required query formData1, required query formData2) {
		var result = {status = 200, message="Start", locus = "setCondoUnitInfo"};
		var bContinue = true;
		var stQueryResult = {};

		//writeDump("#arguments.formData1#");
		//writeDump("#arguments.formData2#");
		//abort;

		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_SaveCondoUnitInfo (
				:p_condounitid, :p_unittype, :p_unitnumber, :p_floornumber, :p_appraisedby, :p_appraiseddate, :p_totaladjustments, :p_ecoobsolescencepct, :p_percentgood, :p_specialconditioncode, :p_specialconditionpct, :p_damageReplacement, :p_updatedBy
			)");

			q.clearParams();

			q.addParam(cfsqltype="cf_sql_integer",		name="p_condounitid",			value=arguments.formData1.condoUnitID);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_unittype",				value=arguments.formData1.unitType);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_unitnumber",			value=arguments.formData1.unitNumber);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_floornumber",			value=arguments.formData1.floorNumber);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_appraisedby",			value=arguments.formData1.appraisedby);
			q.addParam(cfsqltype="cf_sql_timestamp",	name="p_appraiseddate",			value=arguments.formData1.appraiseddate);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_totaladjustments",		value=arguments.formData1.totaladjustments);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_ecoobsolescencepct",	value=arguments.formData1.ecoobsolescencepct);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_percentgood",			value=arguments.formData1.percentgood);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_specialconditioncode",	value=arguments.formData1.specialconditioncode);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_specialconditionpct",	value=arguments.formData1.specialconditionpct);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_damageReplacement",		value=arguments.formData1.damageReplacement);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_updatedBy",				value="");

			stQueryResult = q.execute().getPrefix();

			if(stQueryResult.recordcount != 1) {
				result.status = -1;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};

			//writedump(arguments.formData2);
			//writeoutput("<P>arguments.formData2.recordcount: #arguments.formData2.recordcount#<P>");

			for(var i=1; i LTE arguments.formData2.recordcount; i=i+1) {

				if (arguments.formData2[ "mode" ][ i ] NEQ "Unchanged") {

					//WriteOutput( arguments.formData2[ "condoUnitCodeJoinID" ][ i ] & "<BR>");

					var q = new Query(datasource="#dsn.name#",sql="call p_SaveCondoUnitSpecificCodes (
						:p_condoUnitCodeJoinID, :p_condoUnitID, :p_condoCodeID, :p_tempdecode, :p_mode, :p_updatedBy
					)");
						q.clearParams();

						q.addParam(cfsqltype="cf_sql_integer",		name="p_condoUnitCodeJoinID",	value=arguments.formData2[ "condoUnitCodeJoinID" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_condoUnitID",			value=arguments.formData2[ "condoUnitID" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_condoCodeID",			value=arguments.formData2[ "condoCodeID" ][ i ]);
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_tempdecode",			value=arguments.formData2[ "codeType" ][ i ]);
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_mode",					value=arguments.formData2[ "mode" ][ i ]);
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_updatedBy",				value="");

						stQueryResult = q.execute().getPrefix();

						//writedump(q);
						//writeDump(stQueryResult);

						if(stQueryResult.recordcount != 1) {
							result.status = -1;
							result.message = stQueryResult;
						} else {
							result.status=200;
							result.message = "Success";
						};

				}	/*<!--- END if (arguments.formData2[ "mode" ][ i ] NEQ "Unchanged") --->*/

			}
		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}

		return result;
	}
}