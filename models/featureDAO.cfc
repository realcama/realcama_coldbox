/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" 	inject="coldbox:datasource:ws_realcama_nassau";
	property name="log" 	inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	featureDAO function init(){

		return this;
	}

	<!----- Stored Proc to retrieve Feature Info ----->
	query function getFeaturesInfo(required struct persistentCriteria){

		var q = new Query(datasource="#dsn.name#",sql="call p_GetExtraFeaturesInfo (:parcelID, :taxYear, :propType)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	<!----- Stored Proc to retrieve Feature Unit Type Codes ----->
	query function getFeaturesUnitTypeCodes(){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetFeaturesUnitTypeCodes ()");
		return q.execute().getResult();
	}

	<!----- Stored Proc to retrieve Features Codes ----->
	query function getFeaturesCode(required number taxYear){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetFeaturesCodes ( :taxYear)");
			q.addParam(name="taxYear", value=arguments.taxYear, cfsqltype="cf_sql_integer", null=len(trim(arguments.taxYear)) ? false : true);
		return q.execute().getResult();
	}

	<!----- Stored Proc to retrieve Features quality Codes ----->
	query function getFeaturesQualityCodes(required number taxYear){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetFeaturesQualityCodes ( :taxYear)");
			q.addParam(name="taxYear", value=arguments.taxYear, cfsqltype="cf_sql_integer", null=len(trim(arguments.taxYear)) ? false : true);
		return q.execute().getResult();
	}

	function saveExtraFeaturesInfo(required query formData){
		var result = {status = 200, message="Start", locus = "setExtraFeaturesInfo"};
		var bContinue = true;
		var stQueryResult = {};

		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_saveExtraFeaturesInfo (
				:p_parcelID, :p_taxYear, :p_ExtraFeaturesCode, :p_ExtraFeaturesCodeDescription,
				:p_Dimension1, :p_Dimension2, :p_Dimension3, :p_Value1, :p_ExtendedValue1,
				:p_NumberUnits, :p_UnitTypeCode, :p_BuildingItemNumber, :p_QualityCode, :p_NeighborhoodCode,
				:p_EffectiveYearBuilt, :p_depreciationTable, :p_SpecialConditionCode, :p_SpecialConditionPercent,
				:p_Amendment10Flag, :p_NewConstructionFlag, :p_ReplacementCapFlag, :p_propertyType,
				:p_ItemNumber, :p_mode
			)");

			if(result.status EQ 200) {
				q.clearParams();

				q.addParam(cfsqltype="cf_sql_varchar",		name="p_parcelID",						value=arguments.formData.parcelID);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_taxYear",						value=arguments.formData.taxYear);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_extraFeaturesCode",				value=arguments.formData.extraFeaturesCode);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_extraFeaturesCodeDescription",	value=arguments.formData.extraFeaturesCodeDescription);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_dimension1",					value=arguments.formData.dimension1);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_dimension2",					value=arguments.formData.dimension2);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_dimension3",					value=arguments.formData.dimension3);
				q.addParam(cfsqltype="cf_sql_decimal",		name="p_value1",						value=arguments.formData.value1);
				q.addParam(cfsqltype="cf_sql_decimal",		name="p_extendedValue1",				value=arguments.formData.extendedValue1);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_numberUnits",					value=arguments.formData.numberUnits);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_unitTypeCode",					value=arguments.formData.unitTypeCode);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_buildingItemNumber",			value=arguments.formData.buildingItemNumber);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_qualityCode",					value=arguments.formData.qualityCode);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_neighborhoodCode",				value=arguments.formData.neighborhoodCode);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_effectiveYearBuilt",			value=arguments.formData.effectiveYearBuilt);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_depreciationTable",				value=arguments.formData.depreciationTable);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_specialConditionCode",			value=arguments.formData.specialConditionCode);
				q.addParam(cfsqltype="cf_sql_decimal",		name="p_specialConditionPercent",		value=arguments.formData.specialConditionPercent);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_amendment10Flag",				value=arguments.formData.amendment10Flag);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_newConstructionFlag",			value=arguments.formData.newConstructionFlag);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_ReplacementCapFlag",			value=arguments.formData.replacementCapFlag);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_propertyType",					value=arguments.formData.propertyType);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_itemNumber",					value=arguments.formData.itemNumber);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_mode",							value=arguments.formData.mode);
			}

			stQueryResult = q.execute().getPrefix();

			if(stQueryResult.recordcount != 1) {
				result.status = -1;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};


		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}

		return result;
	}
}
