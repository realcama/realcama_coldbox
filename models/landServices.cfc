/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dropdownsDAO"	inject=model;
	property name="landDAO" 		inject=model;
	property name="persistentDAO" 	inject=model;
	property name="QueryHelper"		inject="QueryHelper@cbcommons";

	/**
	 * Constructor
	 */
	landServices function init(){

		return this;
	}

	function getAllLandInfo(required struct persistentCriteria) {
		var results = {};

		/*// ----- Stored Proc to retrieve Land Information -----*/
		results.qryGetLandInfo = getLandInfo( persistentCriteria = arguments.persistentCriteria);

		/*// ----- Stored Proc to retrieve User Defined Adjustments -----*/
		//results.qryUDFs = getUserDefinedAdjustmentInfo(parcelID = arguments.persistentCriteria.parcelID, taxYear = arguments.persistentCriteria.taxYear);

		/*// ----- Stored Proc to retrieve Neighborhoods -----*/
		results.qryNeighborhoods = getNeighborhoods( persistentCriteria = arguments.persistentCriteria );

		/*// ----- Stored Proc to retrieve the Land Use Codes -----*/
		results.qryLandUseCodes = getLandUseCodes( persistentCriteria = arguments.persistentCriteria );

		/*// ----- Stored Proc to retrieve the Appraisal Record -----*/
		results.qryAppraisalRecords = getAppraisalInfo( persistentCriteria = arguments.persistentCriteria );

		/*// ----- Stored Proc to retrieve User Defined Adjustments -----*/
		var strCodeType = "Unit Type";
		results.qryLandCodes = getLandCodes(taxYear = arguments.persistentCriteria.taxYear, codeType = strCodeType );

		/* Stored Proc to retrieve the Land Depth Records */
		results.qryLandDepthRecords = getLandDepthTables(taxYear = arguments.persistentCriteria.taxYear);

		/*results.qryCustom = getLandAdjustments(taxYear = arguments.persistentCriteria.taxYear, adjDesc = 'Custom');
		results.qryRoads = getLandAdjustments(taxYear = arguments.persistentCriteria.taxYear, adjDesc = 'Roads');
		results.qryTopo = getLandAdjustments(taxYear = arguments.persistentCriteria.taxYear, adjDesc = 'Topography');
		results.qryUtils = getLandAdjustments(taxYear = arguments.persistentCriteria.taxYear, adjDesc = 'Utilities');
		results.qryZoning = getLandAdjustments(taxYear = arguments.persistentCriteria.taxYear, adjDesc = 'Zoning');*/

		// Combine both Building Elements Queries
		results.qryAllAdjustments = QueryHelper.doQueryAppend(
											qryFrom = getLandAdjustments(landid = results.qryGetLandInfo.landid, adjDesc = 'Zoning'),
											qryTo = getLandAdjustments(landid = results.qryGetLandInfo.landid, adjDesc = 'Roads')
											);
		results.qryAllAdjustments = QueryHelper.doQueryAppend(
											qryFrom = getLandAdjustments(landid = results.qryGetLandInfo.landid, adjDesc = 'Utilities'),
											qryTo = results.qryAllAdjustments
											);
		results.qryAllAdjustments = QueryHelper.doQueryAppend(
											qryFrom = getLandAdjustments(landid = results.qryGetLandInfo.landid, adjDesc = 'Topography'),
											qryTo = results.qryAllAdjustments
											);
		results.qryUDFs = QueryHelper.filterQuery(
											qry = results.qryAllAdjustments,
											field = 'selected',
											value = '1'
											);

		QueryAddColumn(results.qryUDFs, "mode", "varchar");
		QueryAddColumn(results.qryUDFs, "item", "integer");
		for ( i = 1; i lte results.qryUDFs.recordcount; i++ ) {
			QuerySetCell(results.qryUDFs, "mode", "none", i);
			QuerySetCell(results.qryUDFs, "item", i, i);
		}

		results.error = false;
		results.message = "Land data retrieved successfully.";

		return results;
	}

	function getAllLandInfoPopup(required struct URLData) {
		var results = {};

		if ( isValid('integer',URLData.row) ) {
			/* Stored Proc to retrieve the Land Depth Records */
			results.qryLandDepthRecords = getLandDepthTables(taxYear = arguments.URLData.taxYear);

			results.qryAdjustmentTypes = getAdjustmentTypes(taxYear = arguments.URLData.taxYear, adjDesc = 'Custom');
			results.qryZoning = getLandAdjustments(landid = arguments.URLData.itemNumber, adjDesc = 'Zoning');
			results.qryRoads = getLandAdjustments(landid = arguments.URLData.itemNumber, adjDesc = 'Roads');
			results.qryUtils = getLandAdjustments(landid = arguments.URLData.itemNumber, adjDesc = 'Utilities');
			results.qryTopo = getLandAdjustments(landid = arguments.URLData.itemNumber, adjDesc = 'Topography');

			/*results.qryZoning = getAdjustmentTypes(taxYear = arguments.URLData.taxYear, adjDesc = 'Zoning');
			results.qryRoads = getAdjustmentTypes(taxYear = arguments.URLData.taxYear, adjDesc = 'Roads');
			results.qryUtils = getAdjustmentTypes(taxYear = arguments.URLData.taxYear, adjDesc = 'Utilities');
			results.qryTopo = getAdjustmentTypes(taxYear = arguments.URLData.taxYear, adjDesc = 'Topography');*/

			results.error = false;
			results.message = "Adjustment data retrieved successfully.";
		} else {
			results.error = true;
			results.message = "Adjustment data could not be found.";
		}

		return results;
	}

	function addBlankLine(required string taxYear, required string parcelID) {
		var results = {};
		var stPersistentCriteria = {};
			stPersistentCriteria.parcelID = arguments.parcelID;
			stPersistentCriteria.taxYear = arguments.taxYear;

		results.qryGetLandInfo = getLandInfo( persistentCriteria = stPersistentCriteria);
		results.qryNeighborhoods = getNeighborhoods( persistentCriteria = stPersistentCriteria );
		results.qryZoning = getAdjustmentTypes(taxYear = stPersistentCriteria.taxYear, adjDesc = 'Zoning');
		results.qryLandUseCodes = getLandUseCodes( persistentCriteria = stPersistentCriteria );

		var strCodeType = "Unit Type";
		results.qryLandCodes = getLandCodes(taxYear = stPersistentCriteria.taxYear, codeType = strCodeType );

		return results;
	}

	function getLandInfo(required struct persistentCriteria) {
		return landDAO.getLandInfo(persistentCriteria = arguments.persistentCriteria);
	}

	function getUserDefinedAdjustmentInfo(required string parcelID, required string taxYear) {
		return landDAO.getUserDefinedAdjustmentInfo(parcelID = arguments.parcelID, taxYear = arguments.taxYear);
	}

	function getNeighborhoods(required struct persistentCriteria) {
		return dropdownsDAO.getNeighborhoods(taxYear = arguments.persistentCriteria.taxYear);
	}

	function getLandUseCodes(required struct persistentCriteria) {
		return dropdownsDAO.getLandUseCodes(persistentCriteria = arguments.persistentCriteria);
	}

	function getAppraisalInfo(required struct persistentCriteria) {
		return landDAO.getAppraisalInfo(persistentCriteria = arguments.persistentCriteria);
	}

	function getLandCodes(required string taxYear, required string codeType) {
		return landDAO.getLandCodes(taxYear = arguments.taxYear, codeType = arguments.codeType);
	}

	function getLandDepthTables(required string taxYear) {
		return landDAO.getLandDepthTables(taxYear = arguments.taxYear);
	}

	/* V1 of SP to get land adjustment data */
	function getAdjustmentTypes(required string taxYear, required string adjDesc) {
		return landDAO.getAdjustmentTypes(taxYear, adjDesc)
	}

	/* V2 of SP to get land adjustment data */
	function getLandAdjustments(required string landid, required string adjDesc) {
		return landDAO.getLandAdjustments(landid, adjDesc)
	}

	function saveLandInfo(required struct formData) {
		cfwddx(action="WDDX2CFML", input=arguments.formData.landData, output="arguments.formData.qrylandData");
		cfwddx(action="WDDX2CFML", input=arguments.formData.udfData, output="arguments.formData.qryudfData");

		return landDAO.saveLandInfo(formData = arguments.formData);
	}

	function testParcelPropURLValues(required struct persistentCriteria) {

		var testedData = { };

		if(NOT IsDefined("arguments.persistentCriteria.parcelID") OR LEN(TRIM(URLDecode(arguments.persistentCriteria.parcelID))) EQ 0) {
			testedData.parcelID = "invalid";
		} else {
			testedData.parcelID = "valid";
		}


		if(NOT IsDefined("arguments.persistentCriteria.taxYear") OR LEN(TRIM(URLDecode(arguments.persistentCriteria.taxYear))) EQ 0) {
			testedData.taxYear = "invalid";
		} else {
			testedData.taxYear = "valid";
		}

		if(NOT IsDefined("arguments.persistentCriteria.TPP") OR LEN(TRIM(URLDecode(arguments.persistentCriteria.TPP))) EQ 0) {
			testedData.TPP = "invalid";
		} else {
			testedData.TPP = "valid";
		}

		return testedData;

	}


	function getPropPersistentData(required struct persistentCriteria) {

		var results = {};

		var qryPersistentInfo = persistentDAO.getPropPersistent(persistentCriteria = arguments.persistentCriteria);

		/* check to see if we need to set data for this parcel */
		<!---
		if(LEN(TRIM(qryPersistentInfo.totalLandLines)) EQ 0) {
			/* Set and then return the updated persistent data */
			qryPersistentInfo = fixPersistentData(persistentCriteria = arguments.persistentCriteria);
		}
		--->

		return qryPersistentInfo;
	}


	function getPropPersistentInfoTotals(required struct persistentCriteria) {

		var results = {};

		var qryPersistentInfoTotals = persistentDAO.getPropPersistentInfoTotals(persistentCriteria = arguments.persistentCriteria);

		return qryPersistentInfoTotals;
	}


	function getPropPersistentInfoCounts(required struct persistentCriteria) {

		var results = {};

		var qryPersistentInfoCounts = persistentDAO.getPropPersistentInfoCounts(persistentCriteria = arguments.persistentCriteria);

		return qryPersistentInfoCounts;
	}


	function fixPersistentData(persistentCriteria) {
		/*<!--- Pull the land data --->*/
		var qryLandRecords = landDAO.getLandInfo(persistentCriteria = arguments.persistentCriteria);

		//dump("landServices.cfc - line 68 -- TO DO");
		//abort;

		/*<!--- Pull the UDF's for this parcel --->*/
		var qryLandUDFs = landDAO.getUserDefinedAdjustmentInfo(arguments.persistentCriteria.parcelID, arguments.persistentCriteria.taxYear);

		/*<!--- Pull the appraisal info since we need that for the update process --->*/
		var qryAppraisalRecords = landDAO.getAppraisalInfo(persistentCriteria = arguments.persistentCriteria);

		/*<!--- Get the initial land calculations --->*/
		var stDataToUpdate = getInitialLandCalculations(
														parcelID = arguments.persistentCriteria.parcelID,
														taxYear = arguments.persistentCriteria.taxYear,
														qryLandRecords = qryLandRecords,
														fromSaveFuntion = false
														);


		/*<!--- pull the persistent data (so we don't need to pass it in) --->*/
		var qryPersistentInfo = persistentDAO.getPropPersistent(persistentCriteria = arguments.persistentCriteria);

		/*<!--- Update the persistent data --->*/
		var stLandSpecificInfo = {
									parcelID = arguments.persistentCriteria.ParcelID,
									taxYear = arguments.persistentCriteria.taxYear,
									type = "R",
									iLandLines = stDataToUpdate.iLandLines,
									iAgriculturalLines = stDataToUpdate.iAgriculturalLines,
									fTotalLandPrice = stDataToUpdate.fTotalLandPrice,
									fTotalAgriculturalPrice = stDataToUpdate.fTotalAgriculturalPrice,
									pbvalue = qryPersistentInfo.pbvalue,
									pxvalbld = qryPersistentInfo.pxvalbld,
									fTotalAcres = stDataToUpdate.fTotalAcres,
									fTotalSqFeet = stDataToUpdate.fTotalSqFeet
		};

		persistentDAO.setPropPersistent(persistentCriteria = stLandSpecificInfo);

		/*<!--- Return a fresh set of data since we just updated it --->*/
		return persistentDAO.getPropPersistent(persistentCriteria = arguments.persistentCriteria);

	}




	function calculateUDFValues(query qryLandUDFs, numeric iIncomingItem, string strIncomingType, boolean useDepthTable) {

		var iTotalItems = 0;

		/*<!--- set a default value for fValues otherwise you'll get a NaN returned to the caller if you don't meet all the IF criteria --->*/
		var fValues = 0;

		/*<!---  Get the UDF's for the current row  --->*/
		var qryCurrentRowUDFs = landDAO.getCurrentRowUDFs(arguments.qryLandUDFs, arguments.iIncomingItem, arguments.strIncomingType);

		for (
       		var intRow = 1 ;
        	intRow LTE qryCurrentRowUDFs.RecordCount ;
        	intRow = (intRow + 1)
        ){
			var bUseValue = true;
			if(useDepthTable EQ false AND qryCurrentRowUDFs["adjdesc"][ intRow ] EQ "Depths") {
				bUseValue = false;
			}

			if(bUseValue EQ true) {
				iTotalItems = iTotalItems + 1;

				if(qryCurrentRowUDFs["laadjtyp"][intRow] EQ "F") {
					if(iTotalItems EQ 1) {
						/*<!--- Don't multiple the first entry times 0, because you'll junk the data --->*/
						fValues = qryCurrentRowUDFs["laadjvalue"][intRow] ;
					} else {
						/*<!--- All percentages are multipled against each other --->*/
						fValues =  fValues * qryCurrentRowUDFs["laadjvalue"][intRow];
					}
				} else {
					fValues = fValues + qryCurrentRowUDFs["laadjvalue"][intRow];
				}
			}

		} /* end FOR loop*/

		return fValues;
	}



	function getInitialLandCalculations(string parcelID, string taxYear, query qryLandRecords, boolean fromSaveFuntion) {
		var stDataToUpdate = {};
		var strLocus = "";

		stDataToUpdate.fTotalLandPrice = 0;
		stDataToUpdate.fTotalAgriculturalPrice = 0;
		stDataToUpdate.fTotalAcres = 0;
		stDataToUpdate.fTotalSqFeet = 0;
		stDataToUpdate.iLandLines = 0;
		stDataToUpdate.iAgriculturalLines = 0;

		var qryUserDefinedAdjustmentInfo = landDAO.getUserDefinedAdjustmentInfo(arguments.parcelID, arguments.taxYear);

		for (
       		var intRow = 1 ;
        	intRow LTE arguments.qryLandRecords.RecordCount ;
        	intRow = (intRow + 1)
        ){

			var units = arguments.qryLandRecords["NumberUnits"][intRow];
			var unitPrice = arguments.qryLandRecords["unitPrice"][intRow];
			var depthTableAdjustments = arguments.qryLandRecords["depthTableAdjustments"][intRow];
			var marketUnitPrice = arguments.qryLandRecords["marketUnitPrice"][intRow];
			var NeighborhoodFactor = arguments.qryLandRecords["NeighborhoodAdjustment"][intRow];

			var Frontage =  arguments.qryLandRecords["front"][intRow];
			var Depth =  arguments.qryLandRecords["depth"][intRow];
			var Type = arguments.qryLandRecords["propertyType"][intRow];

			var RateAddOns = calculateUDFValues(qryUserDefinedAdjustmentInfo,intRow,'R',false);
			var LumpSumAddOns = calculateUDFValues(qryUserDefinedAdjustmentInfo, intRow,'L',false);
			var AssessmentFlag = arguments.qryLandRecords["AgricultureFlag"][intRow];

			if(arguments.qryLandRecords["UnitTypeCode"][intRow] EQ "EF" OR arguments.qryLandRecords["UnitTypeCode"][intRow] EQ "FF"){
				/* Include the Depth Table adjustment */
				var PercentageAddOns = calculateUDFValues(qryUserDefinedAdjustmentInfo,intRow,'F',true);
			} else {
				/* Exclude the Depth Table adjustment */
				var PercentageAddOns = calculateUDFValues(qryUserDefinedAdjustmentInfo,intRow,'F',false);
			}

			if(AssessmentFlag NEQ "M") {
				switch(arguments.qryLandRecords["UnitTypeCode"][intRow]) {
					case "AC":
						stDataToUpdate.fTotalAcres = stDataToUpdate.fTotalAcres + units;
						stDataToUpdate.fTotalSqFeet = stDataToUpdate.fTotalSqFeet + ( units * 43560);
						break;

					case "FF":
						stDataToUpdate.fTotalAcres = stDataToUpdate.fTotalAcres + ((Frontage * Depth) / 43560);
						stDataToUpdate.fTotalSqFeet = stDataToUpdate.fTotalSqFeet + (Frontage * Depth);
						break;

					case "EF":
						stDataToUpdate.fTotalAcres = stDataToUpdate.fTotalAcres + ((Frontage * Depth) / 43560);
						stDataToUpdate.fTotalSqFeet = stDataToUpdate.fTotalSqFeet + (Frontage * Depth);
						break;

					case "SF":
						stDataToUpdate.fTotalAcres = stDataToUpdate.fTotalAcres + (units / 43560);
						stDataToUpdate.fTotalSqFeet = stDataToUpdate.fTotalSqFeet + units;
						break;

					case "LT":
						stDataToUpdate.fTotalAcres = stDataToUpdate.fTotalAcres + ((Frontage * Depth) / 43560);
						stDataToUpdate.fTotalSqFeet = stDataToUpdate.fTotalSqFeet + (Frontage * Depth);
						break;
				}
			}


			if(marketUnitPrice EQ 0) {

				/* 1. The rate add on is added to the unit price */
				var preliminaryUnitPrice = unitPrice + RateAddOns;

				/* 2.  The percentages are multiplied against each other to get net adjustment.  */
				var netAdjustment = PercentageAddOns;

				/* 3.  The net adjustment is multiplied against the preliminary unit price  */
				if(netAdjustment == 0) {
					/* If there are no adjustments defined,  we want to default the value to 1 otherwise this land line is going to get multiplied by ZERO */
					netAdjustment = 1;
				}
				var adjustedUnitPrice = preliminaryUnitPrice * netAdjustment;

				/* 4.  The adjusted unit price is multiplied against the number of units  */
				var preliminaryMarketValue = adjustedUnitPrice * units;

				/* 5.  The Lump Sum is added to the calculated value from step 4.  */
				var appraisedValue = preliminaryMarketValue + LumpSumAddOns;

			} else {

				/* 1. The rate add on is added to the unit price */
				var preliminaryUnitPrice = unitPrice;

				/* 4.  The adjusted unit price is multiplied against the number of units  */
				var preliminaryMarketValue = preliminaryUnitPrice * units;

				/* 5.  The Lump Sum is added to the calculated value from step 4.  */
				var appraisedValue = preliminaryMarketValue;

			}

			// TO DO
			strLocus = inAgRange(Type,taxYear,"cf");


			if(marketUnitPrice GT 0 OR strLocus EQ "agricultural") {
				/* 1. The rate add on is added to the market unit price */
				var preliminaryMarketUnitPrice = marketUnitPrice + RateAddOns;

				/* 2.  The percentages are multiplied against each other to get net adjustment.  */
				var netAdjustment = PercentageAddOns;

				/* 3.  The net adjustment is multiplied against the preliminary market unit price  */
				var adjustedMarketUnitPrice = preliminaryMarketUnitPrice * netAdjustment;

				/* 4.  The adjusted unit price is multiplied against the number of units  */
				var preliminaryAGMarketValue = adjustedMarketUnitPrice * units;

				/* 5.  The Lump Sum is added to the calculated value from step 4.  */
				var AGValue = preliminaryAGMarketValue + LumpSumAddOns;

				bUseAG = true;

			} else {

				bUseAG = false;
			}

			if(strLocus EQ "land" AND bUseAG) {
				strLocus = "agricultural";
			}

			if(strLocus EQ "agricultural") {
				stDataToUpdate.fTotalAgriculturalPrice = stDataToUpdate.fTotalAgriculturalPrice + appraisedValue;
				stDataToUpdate.iAgriculturalLines = stDataToUpdate.iAgriculturalLines + 1;
			}
			if(strLocus EQ "land") {
				stDataToUpdate.fTotalLandPrice = stDataToUpdate.fTotalLandPrice + appraisedValue;
				stDataToUpdate.iLandLines = stDataToUpdate.iLandLines + 1;
			}


		} /* end FOR loop*/



		stDataToUpdate.fTotalLandPrice = CEILING(stDataToUpdate.fTotalLandPrice);
		stDataToUpdate.fTotalAgriculturalPrice = CEILING(stDataToUpdate.fTotalAgriculturalPrice);

		return stDataToUpdate;
	}



	function inAgRange(string typeCode, string typeYear, string valType) {
		/* Default the type */
		var strIsType = "land";

		var qryTypeCode = landDAO.getLandTypeBasedOnCode(arguments.typeCode, arguments.typeYear)

		if(qryTypeCode.AgricultureFlag EQ "Y") {
			strIsType = "agricultural";
		} else {
			if( qryTypeCode.AgricultureFlag EQ "N") {
				strIsType = "land";
			} else {
				if(qryTypeCode.AgricultureFlag EQ "M") {
					strIsType = "market";
				}
			}
		}

		if( ARGUMENTS.valType EQ "js") {
			write(strIsType);
		} else {
			return strIsType;
		}

	}

	function getComparisonRates(string parcelID, numeric propertyYear, string propertyType) {

		/***********
		__          __     _____  _   _ _____ _   _  _____
		\ \        / /\   |  __ \| \ | |_   _| \ | |/ ____|
		 \ \  /\  / /  \  | |__) |  \| | | | |  \| | |  __
		  \ \/  \/ / /\ \ |  _  /| . ` | | | | . ` | | |_ |
		   \  /\  / ____ \| | \ \| |\  |_| |_| |\  | |__| |
		    \/  \/_/    \_\_|  \_\_| \_|_____|_| \_|\_____|

		If you modify ***ANY*** of this funtion, you will need to alter the persistantData_setInitialLandCalculations.cfm,
		the landinfo.js file as well otherwise you'll cause all sorts of data errors!!

		************/

		/***** Create a temp structure of data so we can call some land functions need for these calculations ******/
		var stPersistantData = {
							parcelID = arguments.parcelID,
							taxYear = arguments.propertyYear,
							type = arguments.propertyType
							};

		//writedump(stPersistantData);

		/***** Pull the land data ******/
		var qLandRecords = landDAO.getLandInfo( persistentCriteria = stPersistantData);

		/***** Pull the UDF's for this parcel ******/
		var qLandUDFs = landDAO.getUserDefinedAdjustmentInfo( arguments.parcelID, arguments.propertyYear);

		//writedump(qLandRecords);
		//writedump(qLandUDFs);

		/***** Get the land calculations *****/
		var stData = getInitialLandCalculations(
											parcelID = arguments.parcelID,
											taxYear = arguments.propertyYear,
											qryLandRecords = qLandRecords,
											fromSaveFuntion = false
											);

		// Format the return values
		stData.fTotalAppraisedValue = NumberFormat(stData.fTotalLandPrice,",");
		stData.fTotalAcres = NumberFormat(stData.fTotalAcres,",.00");
		stData.fTotalSqFeet = NumberFormat(stData.fTotalSqFeet,",.00");

		//writedump(stData);

		return stData;
	}

	function getNewDepthPercentage(string landID, string depthTableCode, string depth) {

		var landDepthAdjustment = landDAO.getNewDepthPercentage(
														landID = arguments.landID,
														depthTableCode = arguments.depthTableCode,
														depth = arguments.depth
														);
		return landDepthAdjustment;
	}

}
