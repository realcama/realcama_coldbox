/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="homesteadExemptDAO" inject=model;

	/**
	 * Constructor
	 */
	homesteadExemptServices function init(){

		return this;
	}

	/**
	* getHexInfo
	*/
	function getHexInfo(){
		return homesteadExemptDAO.getHexInfo(persistentCriteria = arguments.persistentCriteria);
	}

}