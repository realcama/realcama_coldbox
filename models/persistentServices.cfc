/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="persistentDAO" 	inject=model;

	/**
	 * Constructor
	 */
	persistent function init(){

		return this;
	}

.
	function getPropPersistent(required struct persistentCriteria) {
		return persistentDAO.getPropPersistent(persistentCriteria = arguments.persistentCriteria);
	}

	function getTppPersistent(persistentCriteria) {
		return persistentDAO.getTppPersistent(persistentCriteria = arguments.persistentCriteria);
	}

	function getTppLookup(required struct persistentCriteria) {
		return persistentDAO.getTppLookup(persistentCriteria = arguments.persistentCriteria);
	}


	function getGetFavoriteMarkerStatus(required struct persistentCriteria) {
		return persistentDAO.getGetFavoriteMarkerStatus(persistentCriteria = arguments.persistentCriteria);
	}

	function getPrevPropID(required struct persistentCriteria) {
		return persistentDAO.getPrevPropID(persistentCriteria = arguments.persistentCriteria);
	}

	function getNextPropID(required struct persistentCriteria) {
		return persistentDAO.getNextPropID(persistentCriteria = arguments.persistentCriteria);
	}

	function getPropertyYears(required struct persistentCriteria) {
		return persistentDAO.getPropertyYears(persistentCriteria = arguments.persistentCriteria);
	}

}