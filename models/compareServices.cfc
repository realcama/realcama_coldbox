/**
* I am a new Model Object
*/
component accessors="true"{
	
	// Properties
	property name="compareDAO"			inject="model";
	property name="landServices" 		inject="model";
	property name="buildingServices" 	inject="model";
	property name="QueryHelper" 		inject="QueryHelper@cbcommons";

	//property name="getCompareData" type="string";
	

	/**
	 * Constructor
	 */
	compareServices function init(){
		
		return this;
	}
	
	function getAllComparedData( required struct persistentCriteria, required struct rc ) {
		var results = {};
		var rightRecord = {};
		var qAdjustments = buildingServices.getBuildingAdjustments();

		var results.qLeftAdjustments = QueryNew( "adjustment,adjustmentCode,adjustmentType", "varchar,varchar,varchar" );
		var results.qRightAdjustments = QueryNew( "adjustment,adjustmentCode,adjustmentType", "varchar,varchar,varchar" );

		results.qLeftRecord = getComparisonTabInfo( arguments.rc.leftParcelID, arguments.rc.leftYear, arguments.persistentCriteria.propertyType );
		results.qLeftImage = getLandImage( arguments.rc.leftParcelID, arguments.rc.leftYear, arguments.persistentCriteria.propertyType );

		results.qLeftBuildingLine = buildingServices.getBuldingLineInfo( persistentCriteria = arguments.persistentCriteria );
		results.qLeftBuildingAddInsData = buildingServices.getBuildingAddInsData( buildingID = results.qLeftRecord.buildingID );

		for ( i = 1; i lte qAdjustments.recordcount; i++ ) {
			qryBuildingAdjustment = buildingServices.getBuildingAttributes( buildingID = results.qLeftRecord.buildingID, codeType = qAdjustments.adjustment[i] );
			qryBuildingAdjustment = QueryHelper.filterQuery( qryBuildingAdjustment, "selected", "1" );
			
			for ( j = 1; j lte qryBuildingAdjustment.recordcount; j++ ) {
				queryAddRow( results.qLeftAdjustments, '1' );
				querySetCell( results.qLeftAdjustments, "adjustment", qryBuildingAdjustment.buildingCodeDescription[j] );
				querySetCell( results.qLeftAdjustments, "adjustmentCode", qryBuildingAdjustment.buildingCode[j] );
				querySetCell( results.qLeftAdjustments, "adjustmentType", qAdjustments.adjustment[i] );
			}
		}

		if ( StructKeyExists( arguments.rc, "rightYear" ) eq False ) {
			arguments.rc.rightYear = leftYear;
		}

		results.qRightRecord = getComparisonTabInfo( arguments.rc.rightParcelID, arguments.rc.rightYear, arguments.persistentCriteria.propertyType );
		results.qRightImage = getLandImage( arguments.rc.rightParcelID, rightYear, arguments.persistentCriteria.propertyType );

		// This structure is created so we do not have to change the getBuldingLineInfo function because persistent data is passed to it.
		rightRecord.ParcelID = results.qRightRecord.parcelID;
		rightRecord.taxYear = results.qRightRecord.taxYear;

		results.qRightBuildingLine = buildingServices.getBuldingLineInfo( persistentCriteria = rightRecord );
		results.qRightBuildingAddInsData = buildingServices.getBuildingAddInsData( buildingID = results.qRightRecord.buildingID );

		for ( i = 1; i lte qAdjustments.recordcount; i++ ) {
			qryBuildingAdjustment = buildingServices.getBuildingAttributes( buildingID = results.qRightRecord.buildingID, codeType = qAdjustments.adjustment[i] );
			qryBuildingAdjustment = QueryHelper.filterQuery( qryBuildingAdjustment, "selected", "1" );
			
			for ( j = 1; j lte qryBuildingAdjustment.recordcount; j++ ) {
				queryAddRow( results.qRightAdjustments, '1' );
				querySetCell( results.qRightAdjustments, "adjustment", qryBuildingAdjustment.buildingCodeDescription[j] );
				querySetCell( results.qRightAdjustments, "adjustmentCode", qryBuildingAdjustment.buildingCode[j] );
				querySetCell( results.qRightAdjustments, "adjustmentType", qAdjustments.adjustment[i] );
			}
		}

		// Get the Comparison Rates for the
		results.qleftComparisonRates = landServices.getComparisonRates( arguments.rc.leftParcel, arguments.rc.leftYear, "R" );
		results.qRightComparisonRates = landServices.getComparisonRates( arguments.rc.leftParcel, arguments.rc.leftYear, "R" );

		return results;
	}

	function getComparisonTabInfo(parcelID, taxYear, propertyType){
		return compareDAO.getComparisonTabInfo( arguments.parcelID, arguments.taxYear, arguments.propertyType);
	}


	function getLandImage(parcelID, taxYear, propertyType){
		return compareDAO.getLandImage( arguments.parcelID, arguments.taxYear, arguments.propertyType);
	}

}