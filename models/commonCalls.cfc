/**
* I am a new Model Object
*/
component accessors="true"{

	property name="sessionStorage" 	inject="sessionStorage@cbstorages";
	property name="landServices" 	inject=model;
	property name="persistentDAO" 	inject=model;


	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* Common Page Includes
	*/
	function commonPageIncludes( rc ){


		var stDataTest = landServices.testParcelPropURLValues(arguments.rc);
		var commonCalls = {};

		//writeDump(stDataTest);

		if (structKeyExists(rc, "displayMode") EQ "No") {
			rc.displayMode = "Parcel";
		}

		// Check to see which set of common calls we should make  -- Main Parcel or TPP
		if (rc.displayMode EQ "Parcel" ) {
			// Grab all of the Common Parcel Information queries

			if(stDataTest.parcelID EQ "invalid" OR stDataTest.taxYear EQ "invalid") {
				setNextEvent( "search/index/missingData/true" );
			} else {
				rc.parcelID = URLDecode(rc.parcelID);
			}

			/*<!---
					Get the Property Persistent data, if there are no land lines defined the code will go about cycling through the land lines/adjustments
					and calculate it and then return the new value to here
			--->*/
			commonCalls.qryPersistentInfo = landServices.getPropPersistentData( persistentCriteria = arguments.rc );
			commonCalls.qryPersistentInfoTotals = landServices.getPropPersistentInfoTotals( persistentCriteria = arguments.rc );
			commonCalls.qryPersistentInfoCounts = landServices.getPropPersistentInfoCounts( persistentCriteria = arguments.rc );

			/*<!--- Create a structure that we'll use throught this code and the site that has the 3 pieces of the property we need to run queries with--->*/
			commonCalls.stPersistentInfo = {
										parcelID = commonCalls.qryPersistentInfo.parcelID[1],
										taxYear = commonCalls.qryPersistentInfo.taxYear[1],
										parcelDisplayID = commonCalls.qryPersistentInfo.parcelDisplayID[1],
										propertyType = "R"
									};


			/*<!--- Pull the TPP data (if there is any) --->*/
			commonCalls.qryTPPInfo = persistentDAO.getTppLookup(persistentCriteria=commonCalls.stPersistentInfo);


			/*<!--- Header navigation queries --->*/
			commonCalls.qryFavoriteMarkerStatus =  persistentDAO.getGetFavoriteMarkerStatus(
																	userID = 1,
																	taxYear = commonCalls.stPersistentInfo.taxYear,
																	parcelID = commonCalls.stPersistentInfo.parcelID );
			if ( sessionStorage.exists("parcelSandBox") ) {
				commonCalls.currentParcelPosition = searchParcelSandbox( rc );
				commonCalls.totalParcels = listlen( sessionStorage.getVar("parcelSandBox"), "," );
				commonCalls.qryPrevProp = getPrevParcel( rc );
				commonCalls.qryNextProp = getNextParcel( rc );

			} else {
				commonCalls.qryPrevProp = persistentDAO.getPrevPropID( persistentCriteria = commonCalls.stPersistentInfo );
				commonCalls.qryNextProp = persistentDAO.getNextPropID( persistentCriteria = commonCalls.stPersistentInfo );
			}
			commonCalls.qryPropYearRange = persistentDAO.getPropertyYears(persistentCriteria = commonCalls.stPersistentInfo);

			/*<!--- Property specific data --->*/
			commonCalls.iStartYear = commonCalls.qryPropYearRange.firstYear; /* client.currentTaxYear */
			commonCalls.iEndYear = commonCalls.qryPropYearRange.lastYear;

			/*<!--- Check to see if there is persistent data in the database for this parcel, if not, it'll populate it for us to use --->*/
			commonCalls.stLandData = landServices.getInitialLandCalculations(
													parcelID = commonCalls.stPersistentInfo.parcelID,
													taxYear = commonCalls.stPersistentInfo.taxYear,
													qryLandRecords = landServices.getLandInfo(persistentCriteria = commonCalls.stPersistentInfo),
													fromSaveFuntion = false
													);

		} else {
			// Grab all of the Common TPP Information queries

			if(stDataTest.TPP EQ "invalid" OR stDataTest.parcelID EQ "invalid" OR stDataTest.taxYear EQ "invalid") {
				setNextEvent( "search/index/missingData/true" );
			} else {
				rc.parcelID = URLDecode(rc.parcelID);
			}

			commonCalls.qGetPersistentInfoTPP = persistentDAO.getTPPPersistentInfo(
															accountNumber = rc.TPP,
															taxYear = rc.taxYear);

			/*<!--- Create a structure that we'll use throught this code and the site that has the 3 pieces of the property we need to run queries with--->*/
			commonCalls.stPersistentInfo = {
										parcelID = commonCalls.qGetPersistentInfoTPP.parcelID[1],
										taxYear = commonCalls.qGetPersistentInfoTPP.taxYear[1],
										parcelDisplayID = commonCalls.qGetPersistentInfoTPP.parcelDisplayID[1],
										accountNumber = commonCalls.qGetPersistentInfoTPP.accountNumber[1],
										propertyType = "R"
									};

			/* Header navigation queries */
			commonCalls.qryFavoriteMarkerStatus =  persistentDAO.getGetFavoriteMarkerStatus(
																	userID = 1,
																	taxYear = commonCalls.qGetPersistentInfoTPP.taxYear[1],
																	parcelID = commonCalls.qGetPersistentInfoTPP.parcelID[1],
																	accountNumber = commonCalls.qGetPersistentInfoTPP.accountNumber[1]);

			if ( sessionStorage.exists("parcelSandBox") ) {
				commonCalls.currentParcelPosition = searchParcelSandbox( rc );
				commonCalls.totalParcels = listlen( sessionStorage.getVar("parcelSandBox"), "," );
				commonCalls.qPrevTangibleProp = getPrevTppParcel( rc );
				commonCalls.qNextTangibleProp = getNextTppParcel( rc );

			} else {
				commonCalls.qPrevTangibleProp = persistentDAO.getPrevTangiblePropID( persistentCriteria = commonCalls.stPersistentInfo );
				commonCalls.qNextTangibleProp = persistentDAO.getNextTangiblePropID( persistentCriteria = commonCalls.stPersistentInfo );
			}

		}

		return commonCalls;
	}

	function parseParcelSandbox( i ) {
		var parcelStruct = {};

		if ( listlen( listfirst(sessionStorage.getVar("parcelSandBox"), ","), "|" ) eq 3 ) {
			parcelStruct.taxYear = val( listfirst( listgetat(sessionStorage.getVar("parcelSandBox"), i, ","), "|" ) );
			parcelStruct.ParcelID = listgetat( listgetat(sessionStorage.getVar("parcelSandBox"), i, ","), 2, "|");
			parcelStruct.tpp = listlast( listgetat(sessionStorage.getVar("parcelSandBox"), i, ","), "|");
		} else {
			parcelStruct.taxYear = val( listfirst( listgetat(sessionStorage.getVar("parcelSandBox"), i, ","), "|" ) );
			parcelStruct.ParcelID = listlast( listgetat(sessionStorage.getVar("parcelSandBox"), i, ","), "|");
		}

		return parcelStruct;
	}

	function searchParcelSandbox( rc ) {
		var currentParcel = rc.taxYear & "|" & rc.parcelID;
		var currentPosition = "";

		if ( isdefined("rc.tpp") ) {
			currentParcel = currentParcel & "|" & rc.tpp;
		}

		currentPosition = listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )

		return currentPosition;
	}

	function getPrevParcel( rc ) {
		var currentParcel = rc.taxYear & "|" & rc.parcelID;
		var parcelPageStruct = {};

		if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) == 1 ) {
			parcelPageStruct.pvprop = "";
			parcelPageStruct.pvTaxYear = "";
		} else if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) > 1 ) {
			parcelPageStruct.pvprop = listlast( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )-1, "," ), "|" );
			parcelPageStruct.pvTaxYear = listfirst( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )-1, "," ), "|" );
		}

		return parcelPageStruct;
	}

	function getNextParcel( rc ) {
		var currentParcel = rc.taxYear & "|" & rc.parcelID;
		var parcelPageStruct = {};

		if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) == listlen( sessionStorage.getVar("parcelSandBox"), "," ) ) {
			parcelPageStruct.pvprop = "";
			parcelPageStruct.pvTaxYear = "";
		} else if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) < listlen( sessionStorage.getVar("parcelSandBox"), "," ) ) {
			parcelPageStruct.pvprop = listlast( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )+1, "," ), "|" );
			parcelPageStruct.pvTaxYear = listfirst( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )+1, "," ), "|" );
		}

		return parcelPageStruct;
	}

	function getPrevTppParcel( rc ) {
		var currentParcel = rc.taxYear & "|" & rc.parcelID & "|" & rc.tpp;
		var parcelPageStruct = {};

		if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) == 1 ) {
			parcelPageStruct.accountNumber = "";
			parcelPageStruct.taxYear = "";
			parcelPageStruct.parcelID = "";
		} else if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) > 1 ) {
			parcelPageStruct.accountNumber = listlast( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )-1, "," ), "|" );
			parcelPageStruct.taxYear = listfirst( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )-1, "," ), "|" );
			parcelPageStruct.parcelID = listgetat( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )-1, "," ), 2, "|" );
		}

		return parcelPageStruct;
	}

	function getNextTppParcel( rc ) {
		var currentParcel = rc.taxYear & "|" & rc.parcelID & "|" & rc.tpp;
		var parcelPageStruct = {};

		if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) == listlen( sessionStorage.getVar("parcelSandBox"), "," ) ) {
			parcelPageStruct.accountNumber = "";
			parcelPageStruct.taxYear = "";
			parcelPageStruct.parcelID = "";
		} else if ( listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," ) < listlen( sessionStorage.getVar("parcelSandBox"), "," ) ) {
			parcelPageStruct.accountNumber = listlast( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )+1, "," ), "|" );
			parcelPageStruct.taxYear = listfirst( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )+1, "," ), "|" );
			parcelPageStruct.parcelID = listgetat( listgetat( sessionStorage.getVar("parcelSandBox"), listfind( sessionStorage.getVar("parcelSandBox"), currentParcel, "," )+1, "," ), "2", "|" );
		}

		return parcelPageStruct;
	}

}