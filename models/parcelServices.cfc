/**
* I am a new Model Object
*/
component accessors="true"{
	
	// Properties
	property name="dropdownsDAO"	inject=model;
	property name="parcelDAO"		inject=model;

	/**
	 * Constructor
	 */
	parcelServices function init(){

		return this;
	}



	/* Put functions here */
	function getSubdivisions( required string taxYear ) {
		return dropdownsDAO.getSubdivisions( taxYear = arguments.taxYear );
	}
	function getNeighborhoods( required string taxYear ) {
		return dropdownsDAO.getNeighborhoods( taxYear = arguments.taxYear );
	}

	function getPrimaryUse() {
		return dropdownsDAO.getPrimaryUse();
	}


	function getStates() {
		return parcelDAO.getStates();
	}
	function getCountries() {
		return parcelDAO.getCountries();
	}
	function getInstrumentType(required struct persistentCriteria) {
		return parcelDAO.getInstrumentType(persistentCriteria = arguments.persistentCriteria);
	}
	function getSalesChangeCodes() {
		return parcelDAO.getSalesChangeCodes();
	}
	function getSalesReasonCodes(required struct persistentCriteria) {
		return parcelDAO.getSalesReasonCodes(persistentCriteria = arguments.persistentCriteria);
	}
	function getExemptionCodes() {
		return parcelDAO.getExemptionCodes();
	}

	function getParcelInfo(required struct persistentCriteria) {
		return parcelDAO.getParcelInfo(persistentCriteria = arguments.persistentCriteria);
	}
	function getParcelSitus(required struct persistentCriteria) {
		return parcelDAO.getParcelSitus(persistentCriteria = arguments.persistentCriteria);
	}
	function getGetParcelSales(required struct persistentCriteria) {
		return parcelDAO.getGetParcelSales(persistentCriteria = arguments.persistentCriteria);
	}
	function getGetParcelSalesMultiParcelInfo(required struct persistentCriteria) {
		return parcelDAO.getGetParcelSalesMultiParcelInfo(persistentCriteria = arguments.persistentCriteria);
	}
	function getGetParcelLegalInfo(required struct persistentCriteria) {
		return parcelDAO.getGetParcelLegalInfo(persistentCriteria = arguments.persistentCriteria);
	}

	function getGetParcelExemptionInfo(required struct persistentCriteria) {
		return parcelDAO.getGetParcelExemptionInfo(persistentCriteria = arguments.persistentCriteria);
	}
	function getGetParcelExemptionApplicationInfo(required struct persistentCriteria) {
		return parcelDAO.getGetParcelExemptionApplicationInfo(persistentCriteria = arguments.persistentCriteria);
	}

	function getSpecificParcelUseCode(required struct persistentCriteria) {
		parcelDAO.getSpecificParcelUseCode(persistentCriteria = arguments.persistentCriteria);
	}



	function setParcelInfo(required struct persistentCriteria, required struct formData) {
		cfwddx(action="WDDX2CFML", input=arguments.formData.packet1, output="arguments.formData.qryPacketOneData");

		stResult = parcelDAO.setParcelInfo(persistentCriteria = arguments.persistentCriteria, formData = arguments.formData.qryPacketOneData);
		return stResult;
	}

	function setSitusInfo(required struct persistentCriteria, required struct formData) {
		cfwddx(action="WDDX2CFML", input=arguments.formData.packet1, output="arguments.formData.qryPacketOneData");

		stResult = parcelDAO.setSitusInfo(persistentCriteria = arguments.persistentCriteria, formData = arguments.formData.qryPacketOneData);
		return stResult;
	}

	function setSalesInfo(required struct persistentCriteria, required struct formData, required string row) {
		cfwddx(action="WDDX2CFML", input=arguments.formData.packet1, output="arguments.formData.qryPacketOneData");

		stResult = parcelDAO.setSalesInfo(persistentCriteria = arguments.persistentCriteria, formData = arguments.formData.qryPacketOneData, row = arguments.row);
		return stResult;
	}

	function setLegalInfo(required struct persistentCriteria, required struct formData) {
		cfwddx(action="WDDX2CFML", input=arguments.formData.packet1, output="arguments.formData.qryPacketOneData");

		stResult = parcelDAO.setLegalInfo(persistentCriteria = arguments.persistentCriteria, formData = arguments.formData.qryPacketOneData);
		return stResult;
	}

	function setExemptionInfo(required struct persistentCriteria, required struct formData) {
		cfwddx(action="WDDX2CFML", input=arguments.formData.packet1, output="arguments.formData.qryPacketOneData");
		cfwddx(action="WDDX2CFML", input=arguments.formData.packet2, output="arguments.formData.qryPacketTwoData");

		stResult = parcelDAO.setExemptionInfo(persistentCriteria = arguments.persistentCriteria, formDataOne = arguments.formData.qryPacketOneData, formDataTwo = arguments.formData.qryPacketTwoData);
		return stResult;
	}


}