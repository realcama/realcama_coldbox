/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="correlationDAO" 		inject=model;
	property name="persistentDAO" 		inject=model;

	/**
	 * Constructor
	 */
	correlationServices function init(){

		return this;
	}

	function getCorrelationInfo(required struct persistentCriteria) {
		return correlationDAO.getCorrelationInfo(persistentCriteria = arguments.persistentCriteria);
	}


	function saveCorrelationInfo(required struct persistentCriteria, required struct formData) {
		return correlationDAO.saveCorrelationInfo(persistentCriteria = arguments.persistentCriteria, formData = arguments.formData);
	}

}