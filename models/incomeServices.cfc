/**
* I am a new Model Object
*/
component accessors="true"{
	
	// Properties
	property name="incomeDAO"		inject=model;

	/**
	 * Constructor
	 */
	incomeServices function init(){
		
		return this;
	}


	function getIncomeInfo(required struct persistentCriteria){
		return incomeDAO.getIncomeInfo( persistentCriteria = arguments.persistentCriteria );	
	}

}