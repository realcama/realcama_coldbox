/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" 	inject="coldbox:datasource:ws_realcama_nassau";
	property name="log" 	inject="logbox:logger:{this}";


	/**
	 * Constructor
	 */
	condoMasterDAO function init(){

		return this;
	}


	function getCondoLookup(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoLookup (:parcelID, :taxYear, :propType)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	function getCondoMasterData(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoMasterData (:parcelID, :taxYear, :propType)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	function getCondoMasterUnitTypes(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoMasterUnitTypes (:parcelID, :taxYear, :propType)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	function getCondoMasterUnitTypicalCodes(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoMasterUnitTypicalCodes (:parcelID, :taxYear, :propType)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}

	function getNeighborhoods(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetNeighborhoods (:taxYear)");
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		return q.execute().getResult();
	}

	function getCondoMasterTypes(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoImprovementCodes (:taxYear)");
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		return q.execute().getResult();
	}

	function getCondoQualityCodes(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoQualityCodes (:taxYear)");
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		return q.execute().getResult();
	}

	function getCondoCodes(required string taxYear, required string condoID,  required string strCodeType) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCondoCodes (:taxYear, :condoID, :codeType)");
			q.addParam(name="taxYear",	value=arguments.taxYear, 	cfsqltype="cf_sql_integer", 	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="condoID",	value=arguments.condoID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.condoID)) ? false : true);
			q.addParam(name="codeType",	value=arguments.strCodeType, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.strCodeType)) ? false : true);

		return q.execute().getResult();
	}

	function setCondoMasterInfo(required struct persistentCriteria, required query formData) {
		var result = {status = 200, message="Start", locus = "setCondoMasterInfo"};
		var bContinue = true;
		var stQueryResult = {};
		var iFixedCondoID = "";

		//writeDump("#arguments.formData#");
		//abort;

		// Remove the decimal point if it is present as it will cause a lookup join failure in the [p_GetCondoLookup] SP
		iFixedCondoID = ReplaceNoCase(TRIM(arguments.formData.condoID),".","","ALL");
		// Format the number so we strip the leading zeros
		iFixedCondoID = NumberFormat(iFixedCondoID,"9999999");

		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_SaveCondoMasterData (
				:p_CondoMasterID, :P_CondoYear, :p_CondoName, :p_ParcelID, :p_CondoID, :p_ActualYearBuilt, :p_EffectiveYearBuilt,
				 :p_ImprovementCode, :p_QualityCode, :p_parcelDisplayID, :p_updatedBy
			)");

				q.clearParams();

				q.addParam(cfsqltype="cf_sql_integer",		name="p_CondoMasterID",			value=arguments.formData.condoMasterID);
				q.addParam(cfsqltype="cf_sql_integer",		name="P_CondoYear",				value=arguments.formData.condoYear);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_CondoName",				value=arguments.formData.condoName);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_ParcelID",				value=Trim(arguments.formData.parcelID));
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_CondoID",				value=iFixedCondoID);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_ActualYearBuilt",		value=arguments.formData.actualYearBuilt);
				q.addParam(cfsqltype="cf_sql_integer",		name="p_EffectiveYearBuilt",	value=arguments.formData.effectiveYearBuilt);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_ImprovementCode",		value=arguments.formData.improvementCode);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_QualityCode",			value=arguments.formData.qualityCode);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_parcelDisplayID",		value=arguments.formData.parcelDisplayID);
				q.addParam(cfsqltype="cf_sql_varchar",		name="p_updatedBy",				value="");

				//writeDump(q);
				stQueryResult = q.execute().getPrefix();
				//writeDump(stQueryResult);
				//abort;
			/*
			if(stQueryResult.recordcount != 1) {
				result.status = -1;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};
			*/

			result.status=200;
			result.message = "Success";

		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}
		return result;
	}


	function setCondoMasterUnitTypes(required struct persistentCriteria, required query formData) {
		var result = {status = 200, message="Start", locus = "setCondoMasterInfo"};
		var bContinue = true;
		var stQueryResult = {};
		var iFixedCondoID = "";

		//writeDump("#arguments.formData#");
		//writeoutput("<P>arguments.formData.recordcount: #arguments.formData.recordcount#<P>");
		//abort;

		try{
			for(var i=1; i LTE arguments.formData.recordcount; i=i+1) {

				// Filter only the data we want to use - anything tagged as unchanged will be
				//		filtered out as it wasn't altered in any way
				if (arguments.formData[ "mode" ][ i ] NEQ "Unchanged") {

					//WriteOutput( arguments.formData[ "condoTypicalID" ][ i ] & "<BR>");

					var q = new Query(datasource="#dsn.name#",sql="call p_SaveCondoMasterUnitTypes (
						:p_CondoTypicalID, :p_CondoID, :p_UnitType, :p_NumberUnits, :p_CondoYear,
						:p_NumberBedrooms, :p_NumberBaths, :p_NumberRooms, :p_SqFt, :p_FloorFrom,
						:p_FloorTo,	:p_UnitBaseAmt, :p_FloorRateAdj, :p_FloorRateAdjTotal, :p_UpdatedBy
					)");
						q.clearParams();

						q.addParam(cfsqltype="cf_sql_integer",		name="p_CondoTypicalID",	value=arguments.formData[ "condoTypicalID" ][ i ]);  // This is the unique ID for the table
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_CondoID",			value=arguments.formData[ "condoID" ][ i ]);
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_UnitType",			value=arguments.formData[ "unitType" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_NumberUnits",		value=arguments.formData[ "numberUnits" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_CondoYear",			value=arguments.formData[ "condoYear" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_NumberBedrooms",	value=arguments.formData[ "numberBedrooms" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_NumberBaths",		value=arguments.formData[ "numberBaths" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_NumberRooms",		value=arguments.formData[ "numberRooms" ][ i ]);
						q.addParam(cfsqltype="cf_sql_integer",		name="p_SqFt",				value=arguments.formData[ "SqFt" ][ i ]);
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_FloorFrom",			value=arguments.formData[ "floorFrom" ][ i ]);
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_FloorTo",			value=arguments.formData[ "floorTo" ][ i ]);
						q.addParam(cfsqltype="cf_sql_decimal",		name="p_UnitBaseAmt",		value=arguments.formData[ "unitBaseAmt" ][ i ]);
						q.addParam(cfsqltype="cf_sql_decimal",		name="p_FloorRateAdj",		value=arguments.formData[ "floorRateAdj" ][ i ]);
						q.addParam(cfsqltype="cf_sql_decimal",		name="p_FloorRateAdjTotal",	value=arguments.formData[ "floorRateAdjTotal" ][ i ]);
						q.addParam(cfsqltype="cf_sql_varchar",		name="p_UpdatedBy",			value="");

						writedump(q);
						stQueryResult = q.execute().getPrefix();

						//writedump(q);
						//writeDump(stQueryResult);

						if(stQueryResult.recordcount != 1) {
							result.status = -1;
							result.message = stQueryResult;
						} else {
							result.status=200;
							result.message = "Success";
						};

				}	/*<!--- END if (arguments.formData2[ "mode" ][ i ] NEQ "Unchanged") --->*/

			}

		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}
		return result;
	}

}