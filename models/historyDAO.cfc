/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";

	/**
	 * Constructor
	 */
	historyDAO function init(){

		return this;
	}


	/**
     * getHistoryInfo method for retrieveing the Parcel History Information
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     */
	function getHistoryInfo( required string parcelID, required string taxYear, required string parcelType ){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetHistoryInfo (:parcelID, :taxYear, :type)");
			q.addParam(name="parcelID",	value=arguments.ParcelID, 		cfsqltype="cf_sql_varchar",	null=len(trim(arguments.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.taxYear, 		cfsqltype="cf_sql_varchar",	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="type",		value=arguments.parcelType, 	cfsqltype="cf_sql_varchar",	null=len(trim(arguments.parcelType)) ? false : true);

		return q.execute().getResult();
	}


	/**
     * getMillageRates method for retrieveing the Millage Rates for the select Parcel
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     */
	function getMillageRates( required string parcelID, required string taxYear, required string parcelType ){

		var q = new Query(datasource="#dsn.name#",sql="call p_GetMillage (:parcelID, :taxYear, :type)");
			q.addParam(name="parcelID",	value=arguments.ParcelID, 		cfsqltype="cf_sql_varchar",	null=len(trim(arguments.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.taxYear, 		cfsqltype="cf_sql_varchar",	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="type",		value=arguments.parcelType, 	cfsqltype="cf_sql_varchar",	null=len(trim(arguments.parcelType)) ? false : true);

		return q.execute().getResult();
	}


	/**
     * getTaxRates method for retrieveing the Tax Rates for the selected Parcel
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     * @parcelType.hint The Parcel Type that is being retrieved
     */
	function getTaxRates( required string parcelID, required string taxYear, required string parcelType ){

		var q = new Query(datasource="#dsn.name#",sql="call p_GetTax (:parcelID, :taxYear, :type)");
			q.addParam(name="parcelID",	value=arguments.ParcelID, 		cfsqltype="cf_sql_varchar",	null=len(trim(arguments.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.taxYear, 		cfsqltype="cf_sql_varchar",	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="type",		value=arguments.parcelType, 	cfsqltype="cf_sql_varchar",	null=len(trim(arguments.parcelType)) ? false : true);

		return q.execute().getResult();

	}

}