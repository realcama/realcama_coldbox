/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";

	/**
	 * Constructor
	 */
	correlationDAO function init(){

		return this;
	}

	function getCorrelationInfo(required struct persistentCriteria) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetCorrelationInfo (:parcelID, :taxYear, :type)");
		q.addParam(name="ParcelID",	value=arguments.persistentCriteria.ParcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
		q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
		q.addParam(name="type",	value="R", 	cfsqltype="cf_sql_varchar");
		return q.execute().getResult();
	}


	function saveCorrelationInfo(required struct persistentCriteria, required struct formData) {
		var result = {status = 200, message="Start", locus = "setCondoUnitInfo"};
		var bContinue = true;
		var stQueryResult = {};

		//writeDump("#arguments.formData#");


		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_saveCorrelation (
				:p_correlationID, :p_parcelID, :p_taxYear, :p_propertyType, :p_landMethod, :p_landAgricultureMethod,
				:p_buildingMethod, :p_featureMethod, :p_costLandValue, :p_costLandAgricultureValue, :p_costBuildingValue,
				:p_costFeatureValue, :p_micaLandValue, :p_micaAgricultureValue, :p_micaBuildingValue, :p_micaFeatureValue,
				:p_incomeLandValue, :p_incomeAgricultureValue, :p_incomeBuildingValue, :p_incomeFeatureValue, :p_marketLandValue,
				:p_marketAgricultureValue, :p_marketBuildingValue, :p_marketFeatureValue, :p_overrideLandValue,
				:p_overrideAgricultureValue, :p_overrideBuildingValue, :p_overrideFeatureValue, :p_overrideYear,
				:p_blendedLandValue, :p_blendedAgricultureValue, :p_blendedBuildingValue, :p_blendedFeatureValues,
				:p_costTotalValue, :p_micaTotalValue, :p_incomeTotalValue, :p_marketTotalValue, :p_blendedTotalValue,
				:p_overrideTotalValue
			)");

			q.clearParams();

			q.addParam(cfsqltype="cf_sql_integer",		name="p_correlationID",				value=arguments.formData.correlationID);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_parcelID",					value=arguments.formData.parcelID);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_taxYear",					value=arguments.formData.taxYear);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_propertyType",				value="R");

			q.addParam(cfsqltype="cf_sql_integer",		name="p_landMethod",				value=arguments.formData.land);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_landAgricultureMethod",		value=arguments.formData.land_ag);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_buildingMethod",			value=arguments.formData.building);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_featureMethod",				value=arguments.formData.features);

			q.addParam(cfsqltype="cf_sql_integer",		name="p_costLandValue",				value=arguments.formData.land_cost);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_costLandAgricultureValue",	value=arguments.formData.land_ag_cost);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_costBuildingValue",			value=arguments.formData.building_cost);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_costFeatureValue",			value=arguments.formData.features_cost);

			q.addParam(cfsqltype="cf_sql_integer",		name="p_micaLandValue",				value=arguments.formData.land_mica);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_micaAgricultureValue",		value=arguments.formData.land_ag_mica);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_micaBuildingValue",			value=arguments.formData.building_mica);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_micaFeatureValue",			value=arguments.formData.features_mica);

			q.addParam(cfsqltype="cf_sql_integer",		name="p_incomeLandValue",			value=arguments.formData.land_income);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_incomeAgricultureValue",	value=arguments.formData.land_ag_income);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_incomeBuildingValue",		value=arguments.formData.building_income);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_incomeFeatureValue",		value=0);				<!--- Features Income is always blank --->

			q.addParam(cfsqltype="cf_sql_integer",		name="p_marketLandValue",			value=arguments.formData.land_market);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_marketAgricultureValue",	value=arguments.formData.land_ag_market);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_marketBuildingValue",		value=arguments.formData.building_market);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_marketFeatureValue",		value=arguments.formData.features_market);

			q.addParam(cfsqltype="cf_sql_integer",		name="p_overrideLandValue",			value=arguments.formData.land_override);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_overrideAgricultureValue",	value=arguments.formData.land_ag_override);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_overrideBuildingValue",		value=arguments.formData.building_override);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_overrideFeatureValue",		value=arguments.formData.features_override);

			// --- Override year ---
			if (LEN(TRIM(peflovrthru)) NEQ 0) {
				q.addParam(cfsqltype="cf_sql_integer",	name="p_overrideYear",				value=arguments.formData.peflovrthru);
			} else {
				q.addParam(cfsqltype="cf_sql_integer",	name="p_overrideYear",				value=0);
			}

			q.addParam(cfsqltype="cf_sql_integer",		name="p_blendedLandValue",			value=arguments.formData.land_blended);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_blendedAgricultureValue",	value=arguments.formData.land_ag_blended);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_blendedBuildingValue",		value=arguments.formData.building_blended);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_blendedFeatureValues",		value=arguments.formData.features_blended);


			q.addParam(cfsqltype="cf_sql_integer",		name="p_costTotalValue",			value=ReplaceNoCase(arguments.formData.COST_TOTAL,',','','ALL'));
			q.addParam(cfsqltype="cf_sql_integer",		name="p_micaTotalValue",			value=ReplaceNoCase(arguments.formData.MICA_TOTAL,',','','ALL'));
			q.addParam(cfsqltype="cf_sql_integer",		name="p_incomeTotalValue",			value=ReplaceNoCase(arguments.formData.INCOME_TOTAL,',','','ALL'));
			q.addParam(cfsqltype="cf_sql_integer",		name="p_marketTotalValue",			value=ReplaceNoCase(arguments.formData.MARKET_TOTAL,',','','ALL'));
			q.addParam(cfsqltype="cf_sql_integer",		name="p_blendedTotalValue",			value=ReplaceNoCase(arguments.formData.BLENDED_TOTAL,',','','ALL'));
			q.addParam(cfsqltype="cf_sql_integer",		name="p_overrideTotalValue",		value=ReplaceNoCase(arguments.formData.OVERRIDE_TOTAL,',','','ALL'));

			//writeDump(q);
			stQueryResult = q.execute().getPrefix();

			//writeDump(stQueryResult);
			//abort;

			if(stQueryResult.recordcount != 1) {
				result.status = -1;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};

		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}


		return result;
	}

}