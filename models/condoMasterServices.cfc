/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="condoMasterDAO"		inject=model;
	property name="log"					inject="logbox:logger:{this}";


	/**
	 * Constructor
	 */
	condoMasterServices function init(){

		return this;
	}


	function getCondoLookup(required struct persistentCriteria){
		return condoMasterDAO.getCondoLookup( persistentCriteria = arguments.persistentCriteria );
	}

	function getCondoMasterData(required struct persistentCriteria){
		return condoMasterDAO.getCondoMasterData( persistentCriteria = arguments.persistentCriteria );
	}

	function getCondoMasterUnitTypes(required struct persistentCriteria){
		return condoMasterDAO.getCondoMasterUnitTypes( persistentCriteria = arguments.persistentCriteria );
	}

	function getCondoMasterUnitTypicalCodes(required struct persistentCriteria){
		return condoMasterDAO.getCondoMasterUnitTypicalCodes( persistentCriteria = arguments.persistentCriteria );
	}

	function getNeighborhoods(required struct persistentCriteria){
		return condoMasterDAO.getNeighborhoods( persistentCriteria = arguments.persistentCriteria );
	}

	function getCondoMasterTypes(required struct persistentCriteria){
		return condoMasterDAO.getCondoMasterTypes( persistentCriteria = arguments.persistentCriteria );
	}

	function getCondoQualityCodes(required struct persistentCriteria){
		return condoMasterDAO.getCondoQualityCodes( persistentCriteria = arguments.persistentCriteria );
	}

	function getCondoCodes(required string taxYear, required string strCondoID, required string strCodeType){
		return condoMasterDAO.getCondoCodes( taxYear = arguments.taxYear, condoID = arguments.strcondoID, strCodeType = arguments.strCodeType);
	}



	function setCondoMasterInfo(required struct persistentCriteria, required struct formData) {

		var strUpdateTab = formData.tab;
		//writeDump(formData);

		<!--- Convert the data packet --->
		cfwddx(action="WDDX2CFML", input=arguments.formData.packet, output="arguments.formData.qryPacketData");
		//writeDump(arguments.formData);

		switch(strUpdateTab) {

			case "info":

				stResult = condoMasterDAO.setCondoMasterInfo(persistentCriteria = arguments.persistentCriteria, formData = arguments.formData.qryPacketData);

				break;

			case "modalUnitTypes":

				stResult = condoMasterDAO.setCondoMasterUnitTypes(persistentCriteria = arguments.persistentCriteria, formData = arguments.formData.qryPacketData);

				break;

		}

		return stResult;

	}


}