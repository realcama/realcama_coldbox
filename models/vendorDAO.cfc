/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsnAdmin"	inject="coldbox:datasource:ws_realcama_Admin";
	property name="log"			inject="logbox:logger:{this}";


	/**
	 * Constructor
	 */
	vendorDAO function init(){

		return this;
	}

	query function getRecord( string DomainName ){

		log.info ( "Inside vendorDAO getRecord function" );
		var q = new Query(datasource="#dsnAdmin.name#",	sql="call p_getVendorId (" & ":p_domainUrl )" );
			q.addParam(name="p_domainUrl", value=arguments.DomainName, 	cfsqltype="cf_sql_varchar", null=len(trim( arguments.DomainName)) ? false : true);

		return q.execute().getResult();
    }

}