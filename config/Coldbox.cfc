component{

	// Configure ColdBox Application
	function configure(){

		// coldbox directives
		coldbox = {
			//Application Setup
			appName 				= "RealCAMA",
			eventName 				= "event",

			//Development Settings
			reinitPassword			= "", //R@alCaMa
			handlersIndexAutoReload = true,

			//Implicit Events
			defaultEvent			= "",
			requestStartHandler		= "Main.onRequestStart",
			requestEndHandler		= "",
			applicationStartHandler = "Main.onAppInit",
			applicationEndHandler	= "",
			sessionStartHandler 	= "",
			sessionEndHandler		= "",
			missingTemplateHandler	= "",

			//Extension Points
			applicationHelper 			= "includes/helpers/ApplicationHelper.cfm",
			viewsHelper					= "",
			modulesExternalLocation		= [ "modules_app" ],
			viewsExternalLocation		= "",
			layoutsExternalLocation 	= "",
			handlersExternalLocation  	= "",
			requestContextDecorator 	= "",
			controllerDecorator			= "",

			//Error/Exception Handling
			invalidHTTPMethodHandler = "",
			exceptionHandler		= "main.onException",
			onInvalidEvent			= "",
			customErrorTemplate		= "",

			//Application Aspects
			handlerCaching 			= true,
			eventCaching			= true,
			viewCaching				= true,


			VendorDBDomainName = 'prod_url_goes_here'
		};

		// custom settings
		settings = {
			page_size = 10,
			legalDescriptionLengthBreakpoint = 80,
			bShowPacketLinks = true,
			editByName = "missing_name",
			iYearsToDisplay = 4,
			iExemptionsToDisplay = 3,
			PagingMaxRows = 10,
			PagingBandGap = 2,
			pageSizeList = ["10","20","50","100","250","500"],
			bMultiSelect = false
		};

		// let CB know about datasources
		datasources = {
			realcama = {name="realcama", dbtype="mySQL"},
			ws_realcama_nassau = {name="ws_realcama_nassau", dbtype="mySQL"},
			ws_realcama_admin = {name="ws_realcama_admin", dbtype="mySQL"}
		};

		// environment settings, create a detectEnvironment() method to detect it yourself.
		// create a function with the name of the environment so it can be executed if that environment is detected
		// the value of the environment is a list of regex patterns to match the cgi.http_host.
		environments = {
			local = "localhost,^127\.0\.0\.1,^10.10.11.43,^10.10.11.15,^10.10.11.46,^10\.10\.*",
			development = "dev$",
			stage = "\wstaging"
		};

		// Module Directives
		modules = {
			//Turn to false in production
			autoReload = false,
			// An array of modules names to load, empty means all of them
			include = [],
			// An array of modules names to NOT load, empty means none
			exclude = []
		};

		//LogBox DSL
		logBox = {
			// Define Appenders
			appenders = {
				coldboxTracer = { class="coldbox.system.logging.appenders.ConsoleAppender" }
			},
			// Root Logger
			root = { levelmax="INFO", appenders="*" },
			// Implicit Level Categories
			info = [ "coldbox.system" ]
		};

		//Layout Settings
		layoutSettings = {
			defaultLayout = "",
			defaultView   = ""
		};

		//Interceptor Settings
		interceptorSettings = {
			throwOnInvalidStates = false,
			customInterceptionPoints = ""
		};

		//Register interceptors as an array, we need order
		interceptors = [
			//SES
			{class="coldbox.system.interceptors.SES",
			 properties={}
			}
		];


		debugger = {
		    // Activate debugger for everybody
		    debugMode = false, // dev function turns it on
		    // Setup a password for the panel
		    debugPassword = "",
		    enableDumpVar = true,
		    persistentRequestProfiler = true,
		    maxPersistentRequestProfilers = 10,
		    maxRCPanelQueryRows = 50,
		    showTracerPanel = true,
		    expandedTracerPanel = true,
		    showInfoPanel = true,
		    expandedInfoPanel = true,
		    showCachePanel = true,
		    expandedCachePanel = false,
		    showRCPanel = true,
		    expandedRCPanel = false,
		    showModulesPanel = true,
		    expandedModulesPanel = false,
		    showRCSnapshots = false,
		    wireboxCreationProfiler=false
		}

		validation = {
		    // The third-party validation manager to use, by default it uses CBValidation.
		    //manager = "",
		    // You can store global constraint rules here with unique names
		    sharedConstraints = {
		    	parcelData = {
		        	parcelID = { required=true, requiredMessage="A parcel ID is required." },
		        	taxYear = { required=true, requiredMessage="Tax year is required.", type="integer", typeMessage="Tax year must be a numeric value." }
		    	},
		        featuresLineForm = {
		        	extraFeaturesData = { required=true, requiredMessage="Extra features information is required." }
		        }
		    }
		}

		// flash scope configuration
		flash = {
			scope = "session",
			properties = { session:"bMultiSelect"}, // constructor properties for the flash scope implementation
			inflateToRC = true, // automatically inflate flash data into the RC scope
			inflateToPRC = false, // automatically inflate flash data into the PRC scope
			autoPurge = true, // automatically purge flash data for you
			autoSave = true // automatically save flash scopes at end of a request and on relocations.
		};

		s3sdk = {
			// Your amazon access key
			accessKey = "AKIAJF5QVP5AYSPJU5UA",
			// Your amazon secret key
			secretKey = "6K2y4RZpwFclFRu4Pz0s/A4b4XTckDejknFpUc0M",
			// The default encryption character set
			encryption_charset = "utf-8",
			// SSL mode or not on cfhttp calls.
			ssl = false,


   			 // Temp directory before uploading to s3
    		tempuploaddirectory = "/_tmp"
		};


		/*
		// module setting overrides
		moduleSettings = {
			moduleName = {
				settingName = "overrideValue"
			}
		};

		// flash scope configuration
		flash = {
			scope = "session,client,cluster,ColdboxCache,or full path",
			properties = {}, // constructor properties for the flash scope implementation
			inflateToRC = true, // automatically inflate flash data into the RC scope
			inflateToPRC = false, // automatically inflate flash data into the PRC scope
			autoPurge = true, // automatically purge flash data for you
			autoSave = true // automatically save flash scopes at end of a request and on relocations.
		};

		//Register Layouts
		layouts = [
			{ name = "login",
		 	  file = "Layout.tester.cfm",
			  views = "vwLogin,test",
			  folders = "tags,pdf/single"
			}
		];

		//Conventions
		conventions = {
			handlersLocation = "handlers",
			viewsLocation 	 = "views",
			layoutsLocation  = "layouts",
			modelsLocation 	 = "models",
			eventAction 	 = "index"
		};

		*/

	}

	/**
	* Development environment
	*/
	function local(){
		reinitPassword			= "";
		coldbox.customErrorTemplate = "/coldbox/system/includes/BugReport.cfm";
		debugger.debugMode = true;
		settings.environmentName = 'DEV';
		settings.useSSL = 'false';
		coldbox.VendorDBDomainName = 'nassaucamadev.tekgroupweb.com';

		//Application Aspects
		handlerCaching 			= false;
		eventCaching			= false;
		viewCaching				= false;
	}
	function development(){
		reinitPassword			= "";
		coldbox.customErrorTemplate = "/coldbox/system/includes/BugReport.cfm";
		debugger.debugMode = true;
		settings.environmentName = 'DEV';
		coldbox.VendorDBDomainName = 'nassaucamadev.tekgroupweb.com';

		//Application Aspects
		handlerCaching 			= false;
		eventCaching			= false;
		viewCaching				= false;
	}
	function stage(){
		coldbox.customErrorTemplate = "/coldbox/system/includes/BugReport.cfm";
		debugger.debugMode = true;
		coldbox.environmentName = 'STAGE';
		coldbox.VendorDBDomainName = 'nassaustaging.tekgroupweb.com';

	}
}
