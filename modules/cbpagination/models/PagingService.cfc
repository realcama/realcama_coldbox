/**
* Copyright Ortus Solutions, Corp
* www.ortussolutions.com
*
* paging service
*
* Conversion Author:  Robert Cruz
* Conversion of original Paging plugin to coldbox 4 paging service utility.  Drop in your models folder.
* Also now has links to go to first and last record in addition to next and previous inks.
*
*/
component output="false"  {

	// injections - get the settings necessary for paging to work
	property name="pagingMaxRows" inject="coldbox:setting:pagingMaxRows";
	property name="PagingBandGap" inject="coldbox:setting:PagingBandGap";
	property name="listPageSizes" inject="coldbox:setting:pageSizeList";

	//Constructor

	public pagingService function init(){
		return this;
	}

	numeric function  getPagingMaxRows (pagingMaxRows) {
		if ( isDefined ( arguments.pagingMaxRows ) ) {
			pagingMaxRows = arguments.pagingMaxRows;
		}

		return PagingMaxRows = pagingMaxRows;
	}

	numeric function  getPagingBandGap () {
		return PagingBandGap = PagingBandGap;
	}

	any function  getBoundaries (page,pagingMaxRows) {
			var boundaries = structnew();
			//var page = arguments.page;
			var maxRows = getPagingMaxRows (pagingMaxRows);

			/* Check for Override need to come back to this */
			if( structKeyExists(arguments,"PagingMaxRows") ){
				maxRows = arguments.pagingMaxRows;
			}

			boundaries.startrow = (arguments.page * maxrows - maxRows)+1;
			boundaries.maxrow = boundaries.startrow + maxRows - 1;

			return boundaries;
	}

	any function renderit (FoundRows,link,page,pagingMaxRows) {

		var pagingTabs = "";
		var maxRows = getPagingMaxRows(pagingMaxRows);
		var bandGap = getPagingBandGap();
		var totalPages = 0;
		var theLink = arguments.link;
		// Paging vars
		var currentPage = arguments.page;
		var pageFrom = 0;
		var pageTo = 0;
		var pageIndex = 0;


		if ( arguments.foundRows neq 0 ) {
			totalPages = ceiling( arguments.FoundRows / maxRows );
		}
		// output pagination totals and carousel
		savecontent variable="pagingtabs" {
			writeOutput( '<nav aria-label="Page navigation">' );
			// output paging totals
			writeOutput( '<div class="pagingTabsTotals"><strong>Total Records: </strong>' & #arguments.FoundRows# & '&nbsp;&nbsp;' & '<strong>Total Pages: </strong>' & #totalPages# & '</div>');
			// start the pagination carousel
			writeOutput( '<ul class="pagination">' );
			// PREVIOUS PAGE
			if ( currentPage-1 gt 0 ) {
				writeOutput ( '<li><a href="#replace(theLink,"@page@",1)#" aria-label="first"><span aria-hidden="true">&laquo;&laquo;</span></a></li>' );
				writeOutput ( '<li><a href="#replace(theLink,"@page@",currentPage-1)#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>' );
			} else {
				writeOutput ( '<li><span aria-hidden="true">&laquo;&laquo;</span></li><li><span aria-hidden="true">&laquo;</span></li>' );
			}
			// Calcualte PageFrom Carrousel
			pageFrom=1;

			if ( (currentPage-bandGap) gt 1 ) {
				pageFrom = currentPage-bandGap;
				writeOutput( '<li><a href="#replace(theLink,"@page@",1)#">1</a></li>' );
			}
			// Page TO of Carrousel
			pageTo = (currentPage+bandGap);

			if ( ( currentPage + bandGap ) gt totalPages ) {
				pageTo = totalPages;
			}

			var pageStatusClass = "";
			// loop and create each page link
			for (
					pageIndex = pageFrom;
					pageIndex LTE pageTo;
					pageindex ++
				)
				{
					if ( currentPage eq pageIndex ) {
						pageStatusClass='class="active"';
					}
					writeOutPut ( '<li #pageStatusClass#><a href="#replace(theLink,"@page@",pageIndex)#"' & '>' & #pageIndex# & '</a></li>');
					pageStatusClass = "";
				}

			// End Token
			if ( ( currentPage + bandGap ) lt totalPages ) {
				writeOutput( '<li><a href="#replace(theLink,"@page@",totalPages)#">' & #totalPages# & '</a></li>' );
			}

			// NEXT PAGE
			if ( currentPage lt totalPages ) {
				writeOutput ( '<li><a href="#replace(theLink,"@page@",currentPage+1)#" aria-hidden="true"><span aria-hidden="true">&raquo;</span></a></li>' );
				writeOutput ( '<li><a href="#replace(theLink,"@page@",totalPages)#" aria-hidden="true"><span aria-hidden="true">&raquo;&raquo;</span></a></li>' );
			} else {
				writeOutput ( '<li><span aria-hidden="true">&raquo;</span></li>' );
				writeOutput ( '<li><span aria-hidden="true">&raquo;&raquo;</span></li>' )
			}

			writeOutPut( '</ul>' );
			writeOutPut( '</nav>' );
		}
		// this return is for testing only
		return pagingtabs;
	}

	any function selectPageSize(maxRows) {

		savecontent variable="selPageSize" {
			writeOutput( '<ul class="pagination page-size">' );
			writeOutPut( '<li><span class="pagination-form-group pagination-page-size text-left"><label for="selPageSize">Show</label> <select name="selPageSize" id="selPageSize" size="1" class="form-control">' );
			for (i=1;i LTE ArrayLen(listPageSizes);i=i+1) {
				if (maxRows eq listPageSizes[i]) {
					writeOutPut( '<option value="#listPageSizes[i]#" selected>#listPageSizes[i]#</option>' );
				} else {
					writeOutPut( '<option value="#listPageSizes[i]#">#listPageSizes[i]#</option>' );
				}
			}
			writeOutPut( '</select></span></li>');
			writeOutput( '</ul>' );
		};

		return selPageSize;
	}

	any function refresh_div_renderit (FoundRows,link,page,pagingMaxRows) {
		var pagingTabs = "";
		var maxRows = getPagingMaxRows(pagingMaxRows);
		var bandGap = getPagingBandGap();
		var totalPages = 0;
		var theLink = arguments.link;

		// Paging vars
		var currentPage = arguments.page;
		var pageFrom = 0;
		var pageTo = 0;
		var pageIndex = 0;

		if ( arguments.foundRows neq 0 ) {
			totalPages = ceiling( arguments.FoundRows / maxRows );
		}

		// output pagination totals and carousel
		savecontent variable="pagingtabs" {
			// start the pagination carousel
			writeOutput( '<nav aria-label="Page navigation">' );

			writeOutput( '<ul class="pagination">' );
			// PREVIOUS PAGE
			if ( currentPage-1 gt 0 ) {
				writeOutput( '<li><a id="page-prev" aria-label="Previous" onclick="pageIncrement(-1);"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></span></a></li>' );
			} else {
				writeOutput( '<li><a id="page-prev" class="disabled" aria-label="Previous" onclick="pageIncrement(-1);"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></span></a></li>' );
			}

			writeOutput( '<li class="text-center"><span class="pagination-form-group"><input type="text" name="page" id="pagination-input" value="#currentPage#" class="form-control text-center"> /<span id="totalPages">' & #totalPages# & '</span></span></li>' );

			// NEXT PAGE
			if ( currentPage lt totalPages ) {
				writeOutput( '<li><a id="page-next" aria-hidden="true" onclick="pageIncrement(1);"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></span></a></li>' );
			} else {
				writeOutput( '<li><a id="page-next" class="disabled" aria-hidden="true" onclick="pageIncrement(1);"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></span></a></li>' );
			}

			writeOutPut( '</ul></nav>' );
		}

		return pagingtabs;
	}

	any function refresh_page_renderit (FoundRows,link,page,pagingMaxRows) {

		var pagingTabs = "";
		var maxRows = getPagingMaxRows(pagingMaxRows);
		var bandGap = getPagingBandGap();
		var totalPages = 0;
		var theLink = arguments.link;

		// Paging vars
		var currentPage = arguments.page;
		var pageFrom = 0;
		var pageTo = 0;
		var pageIndex = 0;


		if ( arguments.foundRows neq 0 ) {
			totalPages = ceiling( arguments.FoundRows / maxRows );
		}

		// output pagination totals and carousel
		savecontent variable="pagingtabs" {
			// start the pagination carousel
			writeOutput( '<nav aria-label="Page navigation">' );

			writeOutput( selectPageSize(maxRows) );

			writeOutput( '<ul class="pagination text-right">' );
			// PREVIOUS PAGE
			if ( currentPage-1 gt 0 ) {
				writeOutput( '<li><a href="#replace(theLink,"@page@",currentPage-1)#" id="extrafeatures-page-prev" aria-label="Previous"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></span></a></li>' );
			} else {
				writeOutput( '<li><a id="extrafeatures-page-prev" class="disabled" aria-label="Previous" onclick="pageIncrement(-1);"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-left"></i></span></a></li>' );
			}

			writeOutput( '<li class="text-center"><span class="pagination-form-group"><input type="text" name="page" id="pagination-input" value="#currentPage#" class="form-control text-center"> /<span id="totalPages">' & #totalPages# & '</span></span></li>' );

			// NEXT PAGE
			if ( currentPage lt totalPages ) {
				writeOutput( '<li><a href="#replace(theLink,"@page@",currentPage+1)#" id="extrafeatures-page-next" aria-hidden="true"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></span></a></li>' );
			} else {
				writeOutput( '<li><a id="extrafeatures-page-next" class="disabled" aria-hidden="true" onclick="pageIncrement(1);"><span aria-hidden="true"><i class="glyphicon glyphicon-white glyphicon-chevron-right"></i></span></a></li>' );
			}
			writeOutPut( '</ul></nav>' );
		}
		// this return is for testing only
		return pagingtabs;
	}

	// Custom Function
	any function getPageStart(rc, qryRecordCount, defaultPageSize) {
		var pageStart = structnew();

		if ( StructKeyExists(rc, "page_size") eq false or isnumeric(rc.page_size) eq FALSE) {
			pageStart.page_size = defaultPageSize;
		} else {
			pageStart.page_size	= rc.page_size;
		};

		if ( StructKeyExists(rc, "page") eq FALSE or isnumeric(rc.page) eq FALSE ) {
			pageStart.page = 1;
		} else if (rc.page lt 1) {
			pageStart.page = 1;
		} else {
			pageStart.page = rc.page;
		};

		if ( pageStart.page gt ceiling(qryRecordCount/pageStart.page_size) ) {
			pageStart.page = ceiling(qryRecordCount/pageStart.page_size);
			pageStart.start_row = ( (pageStart.page-1) * pageStart.page_size ) + 1;
		} else {
			pageStart.start_row = ( (pageStart.page-1) * pageStart.page_size ) + 1;
		};

		return pageStart;
	}

}