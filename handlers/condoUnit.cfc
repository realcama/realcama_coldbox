/**
* I am a new handler
*/
component{

	property name="landServices"		inject=model;
	property name="condoUnitServices"	inject=model;
	property name="condoMasterServices"	inject=model;
	property name="commonCalls" 		inject=model;
	property name="htmlhelper" 			inject="htmlhelper@coldbox";
	property name="log"					inject="logbox:logger:{this}";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = "GET",
		condoUnitSave = "POST"
	};


	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/


	function index( event, rc, prc ){

		prc.pageTitle = "RealCAMA Condo Unit";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );

		prc.page_size = Controller.getSetting( "page_size" );

		structAppend(prc,stCommonCalls);
		prc.getCondoUnitInfo = condoUnitServices.getCondoUnitInfo( persistentCriteria = prc.stPersistentInfo );

		prc.condoUnitID = prc.getCondoUnitInfo.CondoUnitID;
		log.info ( "Inside condounit handler" );

		<!----- Stored Proc to retrieve Neighborhoods ----->
		prc.getNeighborhoods = condoUnitServices.getNeighborhoods( taxYear = prc.stPersistentInfo.taxYear );

		<!----- Stored Proc to retrieve Special Condition Codes ----->
		prc.getSpecialConditionCodes = condoUnitServices.getSpecialConditionCodes( );

		<!----- Stored Proc to retrieve Condo Unit Codes ----->
		prc.getCondoUnitSpecificCodes = condoUnitServices.getCondoUnitSpecificCodes( prc.condoUnitID);
		log.info ( "After call to getCondoUnitSpecificCodes" );

		if(prc.getCondoUnitInfo.recordcount EQ 0) {
			event.setView( "condoUnit/noRecords" );
		} else {
			event.setView( "condoUnit/index" );
		}

	}


	/**
	* showCondoUnitPopup
	*/
	function showCondoUnitPopup( event, rc, prc ){
		var strCondoType = "";
		/*writeDump(rc)
		abort; */
		<!----- Stored Proc to retrieve Condo Location Codes ----->
		strCondoType = "Location";
		prc.qryGetLocationCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Views";
		prc.qryGetViewsCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Desirability";
		prc.qryGetDesirabilityCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Recreation";
		prc.qryGetRecreationCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Balcony";
		prc.qryGetBalconyCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Parking";
		prc.qryGetParkingCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		event.setView( "condoUnit/showCondoUnitPopup" );
	}


	/**
	* condoUnitSave
	*/
	function condoUnitSave( event, rc, prc ){
		prc.qryPersistentInfo = landServices.getPropPersistentData( persistentCriteria=rc );
		<!---
		writedump(#prc.qryPersistentInfo#);
		abort;
		--->
		var stPersistentInfo = {
									parcelID = prc.qryPersistentInfo.parcelID[1],
									taxYear = prc.qryPersistentInfo.taxYear[1],
									parcelDisplayID = prc.qryPersistentInfo.parcelDisplayID[1]
								};

		prc.stResults = condoUnitServices.setCondoUnitInfo(persistentCriteria = stPersistentInfo, formData = rc);

		<!---
		writedump(#prc.stResults#);
		abort;
		--->

		if(prc.stResults.status EQ 200) {
			setNextEvent( "condoMaster/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		} else {
			event.noLayout();
			hideDebugger();
			event.setView( "condoMaster/saveError" );
		}
	}

}
