/**
* I am a new handler
*/
component{

	property name="sessionStorage" 	inject="sessionStorage@cbstorages";
	property name="parcelServices" 	inject=model;
	property name="landServices" 	inject=model;
	property name="commonCalls" 	inject=model;
	property name="htmlhelper" 		inject="htmlhelper@coldbox";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		/*index = "GET",*/
		parcelSave = "POST",
		newParcelExemptionRow = "GET",
		showParcelMultisalesPopup =" GET",
		getUseCode = "GET,POST"
	};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){

		if ( sessionStorage.exists("parcelSandBox") ) {
			if ( not isdefined("rc.taxYear") ) {
				rc.taxYear = commonCalls.parseParcelSandbox("1").taxYear;
				rc.ParcelID = commonCalls.parseParcelSandbox("1").ParcelID;
			}
		};

		prc.pageTitle = "RealCAMA Parcel";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );

		structAppend(prc,stCommonCalls);

		prc.getSubdivisions = parcelServices.getSubdivisions( taxYear = prc.stPersistentInfo.taxYear );
		prc.getStates = parcelServices.getStates();
		prc.getCountries = parcelServices.getCountries();
		prc.getPrimaryUse = parcelServices.getPrimaryUse();
		prc.getNeighborhoods = parcelServices.getNeighborhoods( taxYear = prc.stPersistentInfo.taxYear );
		prc.getInstrumentType = parcelServices.getInstrumentType(persistentCriteria = prc.stPersistentInfo);
		prc.getSalesChangeCodes = parcelServices.getSalesChangeCodes(persistentCriteria = prc.stPersistentInfo);
		prc.getSalesReasonCodes = parcelServices.getSalesReasonCodes(persistentCriteria = prc.stPersistentInfo);
		prc.getExemptionCodes = parcelServices.getExemptionCodes(persistentCriteria = prc.stPersistentInfo);

		prc.qrySitus = parcelServices.getParcelSitus(persistentCriteria = prc.stPersistentInfo);
		prc.qryParcelInfo = parcelServices.getParcelInfo(persistentCriteria = prc.stPersistentInfo);
		prc.qryParcelSales = parcelServices.getGetParcelSales(persistentCriteria = prc.stPersistentInfo);
		prc.qryParcelMultiSales = parcelServices.getGetParcelSalesMultiParcelInfo(persistentCriteria = prc.stPersistentInfo);
		prc.qryParcelLegalInfo = parcelServices.getGetParcelLegalInfo(persistentCriteria = prc.stPersistentInfo);

		prc.qryParcelExemptionInfo = parcelServices.getGetParcelExemptionInfo(persistentCriteria = prc.stPersistentInfo);
		prc.qryParcelExemptionApplicationInfo = parcelServices.getGetParcelExemptionApplicationInfo(persistentCriteria = prc.stPersistentInfo);

		event.setView( "parcel/index" );
	}

	/**
	* parcelsave
	*/
	function parcelSave( event, rc, prc ){


		prc.qryPersistentInfo = landServices.getPropPersistentData( persistentCriteria=rc );


		var stPersistentInfo = {
									parcelID = prc.qryPersistentInfo.parcelID[1],
									taxYear = prc.qryPersistentInfo.taxYear[1],
									parcelDisplayID = prc.qryPersistentInfo.parcelDisplayID[1]
								};

		switch(#rc.tab#) {

			case "info":
				prc.stResults = parcelServices.setParcelInfo(persistentCriteria = stPersistentInfo, formData = rc);
				break;

			case "situs":
				prc.stResults = parcelServices.setSitusInfo(persistentCriteria = stPersistentInfo, formData = rc);
				break;

			case "sales":
				prc.stResults = parcelServices.setSalesInfo(persistentCriteria = stPersistentInfo, formData = rc, row = rc.row);
				break;

			case "legal":
				prc.stResults = parcelServices.setLegalInfo(persistentCriteria = stPersistentInfo, formData = rc);
				break;

			case "exemptions":
				prc.stResults = parcelServices.setExemptionInfo(persistentCriteria = stPersistentInfo, formData = rc);
				break;
		}

		if(prc.stResults.status EQ 200) {
			setNextEvent( "parcel/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		} else {
			event.noLayout();
			hideDebugger();
			event.setView( "parcel/saveError" );
		}
	}


	function newParcelExemptionRow( event, rc, prc ){
		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		prc.getExemptionCodes = parcelServices.getExemptionCodes(event, rc, prc);
		event.setView( "parcel/newExemptionRow" );

	}

	function showParcelMultisalesPopup( event, rc, prc ){
		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		prc.getPrimaryUse = parcelServices.getPrimaryUse(event, rc, prc);
		event.setView( "parcel/parcelMultisalesPopup" );
	}

	function getUseCode( event, rc, prc ){
		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		prc.getSpecificParcelUseCode = parcelServices.getPrimaryUse(event, rc, prc);
	}

}
