/**
* I am a new handler
*/
component{

	property name="historyServices" 	inject=model;
	property name="commonCalls" 		inject=model;
	property name="htmlhelper" 			inject="htmlhelper@coldbox";

	param name="rc.startRow" default="1";


	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){

		var stCommonCalls = commonCalls.commonPageIncludes( rc );
		var parcelType = 'R';

		prc.pageTitle = "RealCAMA History";
		prc.iYearsToDisplay = Controller.getSetting( "iYearsToDisplay" );

		structAppend(prc,stCommonCalls);
		prc.getHistoryInfo = historyServices.getHistoryInfo(
												parcelID = prc.stPersistentInfo.parcelID,
												taxYear = '',			// Pass it in as empty string so we get all records back
												parcelType = parcelType
												);


		if(prc.getHistoryInfo.recordcount EQ 0) {
			event.setView( "history/noRecords" );
		} else {
			event.setView( "history/index" );
		}

	}


	/**
	* scrollData
	*/
	function scrollData( event, rc, prc ){
		event.noLayout();
		hideDebugger();

		if( rc.startRow LTE 1 OR NOT IsNumeric(rc.startRow) ) {
			rc.startRow = 1;
		}

		var stCommonCalls = commonCalls.commonPageIncludes( rc );
		var parcelType = 'R';
		prc.iYearsToDisplay = Controller.getSetting( "iYearsToDisplay" );

		structAppend(prc,stCommonCalls);

		prc.getHistoryInfo = historyServices.getHistoryInfo(
												parcelID = prc.stPersistentInfo.parcelID,
												taxYear = '', 			// Pass it in as empty string so we get all records back
												parcelType = parcelType
												);

		// Loop through all of the Tax years and get all of the Millage data
		for (loopYear in prc.getHistoryInfo) {

			//writeOutput("[ #prc.getHistoryInfo.currentRow# of #prc.getHistoryInfo.recordCount# ] " & loopYear.taxYear & "<br />");

			var millageData = historyServices.getMillageRates(
												parcelID = prc.stPersistentInfo.parcelID,
												taxYear =  loopYear.taxYear,
												parcelType = parcelType
												);

			prc.getMillageRates[loopYear.taxYear] = millageData;

			var taxData = historyServices.getTaxRates(
												parcelID = prc.stPersistentInfo.parcelID,
												taxYear =  loopYear.taxYear,
												parcelType = parcelType
												);

			prc.getTaxRates[loopYear.taxYear] = taxData;

		}


		var  bEandIDefined = historyServices.getEandIFlag( getHistoryInfo = prc.getHistoryInfo, startRow = rc.startRow, iYearsToDisplay = prc.iYearsToDisplay );

		if(bEandIDefined) {
			prc.strLabel = "Corrected";
		} else {
			prc.strLabel = "Certified";
		}

		event.setView( "history/scrollData" );
	}

}
