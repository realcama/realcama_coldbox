/**
* I am a new handler
*/
component{

	property name="landServices" 		inject=model;
	property name="commonCalls" 		inject=model;
	property name="htmlhelper" 			inject="htmlhelper@coldbox";
	property name="pagingService" 		inject="PagingService@cbpagination";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = 'GET,POST',
		landSave = 'POST',
		addBlankLine = 'POST'
	};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){
		var paginationSettings = "";

		prc.pageTitle = "RealCAMA Land";

		/*var validationResults = validateModel( target=rc, constraints="parcelData" );

		if (validationResults.hasErrors() gt 0) {
			prc.stResults.status = -10;
			prc.stResults.message = "";
			prc.qryGetLandInfo.recordcount = 0;

			for( item in validationResults.getErrors() ) {
			    prc.stResults.message = prc.stResults.message & item.getMessage() & '<br>';
			};

		} else {
			structAppend( prc, commonCalls.commonPageIncludes( rc ) );
			structAppend( prc, landServices.getAllLandInfo( persistentCriteria = prc.stPersistentInfo ) );
			prc.page_size = Controller.getSetting( "page_size" );
		};*/

		structAppend( prc, commonCalls.commonPageIncludes( rc ) );
		structAppend( prc, landServices.getAllLandInfo( persistentCriteria = prc.stPersistentInfo ) );

		if(prc.qryGetLandInfo.recordcount EQ 0) {
			event.setView( "land/noRecords" );
		} else {
			paginationSettings = pagingService.getPageStart(rc, prc.qryGetLandInfo.RecordCount, Controller.getSetting("PagingMaxRows") );
			rc.page = paginationSettings.page;
			rc.page_size = paginationSettings.page_size;
			rc.start_row = paginationSettings.start_row;

			event.setView( "land/index" );
		}

	}

	/**
	* landSave
	*/
	function landSave( event, rc, prc ){
		prc.stResults = landServices.saveLandInfo(formData = rc);
		/*var validationResults = validateModel( target=rc, constraints="parcelData" );

		if (validationResults.hasErrors() gt 0) {
			prc.stResults.status = -10;
			prc.stResults.message = "";

			for( item in validationResults.getErrors() ) {
			    prc.stResults.message = prc.stResults.message & item.getMessage() & '<br>';
			}

		} else {
			validationResults = validateModel( target=rc, constraints="featuresLineForm" );

			if (validationResults.hasErrors() gt 0) {
				prc.stResults.status = -11;
				prc.stResults.message = validationResults.getAllErrors();
			} else {
				prc.stResults = landServices.saveLandInfo(formData = rc);
			}
		}*/

		if(prc.stResults.status EQ 200) {
			setNextEvent( "land/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#/message/#prc.stResults.status#" );
		} else {
			/*event.noLayout();*/
			/*hideDebugger();*/
			event.setView( "land/saveError" );
		}
	}

	function showLandPopup( event, rc, prc ){
		/* MODAL WINDOW */
		event.noLayout();
		hideDebugger();

		structAppend( prc, landServices.getAllLandInfoPopup( URLData = rc ) );

		event.setView( "land/viewlets/_showLandPopup" );
	}

	/**
	* addBlankLine
	* Adds a blank line to the Land Lines section
	*/
	function addBlankLine( event, rc, prc ){
		prc.pageTitle = "RealCAMA Land Blank";
		structAppend( prc, landServices.addBlankLine( taxYear = rc.taxYear, parcelID = rc.parcelID ) );

		event.noLayout();
		event.renderData( data=prc, formats="html" );
	}

	function getNewDepthPercentage(event, rc, prc) {
		prc.getNewDepthPercentage = landServices.getNewDepthPercentage( landID=rc.landid, depthTableCode=rc.depthTable, depth=rc.depthValue );
		event.noLayout();
		event.setView( "land/getnewdepthpercentage" );
	}
}
