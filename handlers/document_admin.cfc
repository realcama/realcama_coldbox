/**
* I am a new handler
*/
component{

	property name="documentServices"	inject=model;
	property name="htmlhelper" 			inject="htmlhelper@coldbox";
	property name="log"					inject="logbox:logger:{this}";
	property name="AmazonS3"			inject="AmazonS3@s3sdk";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = "GET",
		loadDocument = "GET,POST",
		documentType = "GET",
		showParcelIDPopup = "GET",
		documentSave = "POST"
	};


	function index( event, rc, prc ){

		log.info ( "Inside Document Admin handler (30):" );
		var strCategoryCode = "Category";
		var strIncomingBucketName = Controller.getSetting( "strAmazonS33StagingBucket" );
		var VendorInfo = Controller.getSetting( "vendorInfo" );

		prc.vendorName = VendorInfo.vendorName;
		prc.pageTitle = "RealCAMA Document Admin";

		// Call the Document Services and get the the list of Document Categories and documents to process
		structAppend(prc, documentServices.getDocumentsAdminData(
													codeType = strCategoryCode,
													bucketName = strIncomingBucketName ));

		//writeDump( strIncomingBucketName );

		// writeDump(AmazonS3.getURLEndPoint());
		// writeDump(AmazonS3.getObjectInfo(strIncomingBucketName, 'Blindspot.jpg'));

		// var tempURL = AmazonS3.getAuthenticatedURL(strIncomingBucketName, 'Blindspot.jpg');

		// writeOutput("<img src=" & tempURL & ">");

		// writeDump(AmazonS3.getAccessKEY());
		// writeDump(AmazonS3.getSecretKEY());
		// writeDump( AmazonS3.getBucket( strIncomingBucketName ));
		// writeDump( AmazonS3 );
		// writeDump( prc.GETDOCUMENTSTOPROCESS ); abort;

		//event.noLayout();
		event.setView( "document_admin/index" );

	}


	/**
	* loadDocument
	*/
	function loadDocument( event, rc, prc ){

		var strIncomingBucketName = Controller.getSetting( "strAmazonS33StagingBucket" );

		log.info ( "Inside loadDocument Admin handler (72):" );
		log.info ( "rc: " & rc.strObjectKey );
		//writeDump(rc); abort;

		/* ----- Stored Proc to retrieve AmazonS3 Object Data for this Document  ----- */
		var getAmazonS3ObjectData = documentServices.getAmazonS3ObjectData( strIncomingBucketName, rc.strObjectKey );

		structAppend( prc, getAmazonS3ObjectData );

		/* ----- Check to see if this document has been saved previosuly with incomplete information  ----- */
		if ( isDefined("rc.documentID") and rc.documentID <> '' ) {

			/* ----- Stored Proc to retrieve AmazonS3 Object Data for this Document  ----- */
			var getDocumentDataByID = documentServices.getDocumentDataByID( documentID, Val(rc.documentID) );

			structAppend( prc, getDocumentDataByID );

		}

		<!--- Call the AmazonS3 service to get the public URL to the file --->
		prc.strDocumentURL = AmazonS3.getAuthenticatedURL( strIncomingBucketName, rc.strObjectKey );

		//log.info ("loadDocument(94) rc: " & prc.getAmazonS3ObjectData.ETAG );
		log.info ("loadDocument(95) rc: " & prc.strDocumentURL );

		//writeOutput("strDocumentURL:<BR>" & prc.strDocumentURL & "<P>");
		//writeDump(prc); writeDump(rc); abort;

		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		event.setView( "document_admin/viewlets/_documentLoad" );
	}


	/**
	* documentType
	*/
	function documentType( event, rc, prc ){

		//log.info ( "Inside documentType Admin handler (112):" );
		log.info ( "documentType rc.categoryID: " & rc.categoryID );
		// writeDump(rc); abort;

		// Stored Proc to retrieve the Document Types
		prc.getDocumentTypes = documentServices.getDocumentCodes( codeType = rc.categoryID );
		log.info ( "getDocumentTypes: " & prc.getDocumentTypes.RecordCount() );

		//writeOutput("strDocumentURL:<BR>" & prc.strDocumentURL & "<P>");
		//writeDump(prc); writeDump(rc); abort;

		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		event.setView( "document_admin/viewlets/_documentType" );
	}


	function showParcelIDPopup( event, rc, prc ){

		log.info ( "Inside showParcelIDPopup Admin handler (107):" );
		//writeDump(rc); abort;

		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		event.setView( "document_admin/viewlets/_showDocumentParcelIDPopup" );
	}


	function documentSave( event, rc, prc ){

		log.info ( "Inside documentSave Admin handler (146):" );

		var strCategoryCode = "Category";
		var strStageBucketName = Controller.getSetting( "strAmazonS33StagingBucket" );
		var strProdBucketName = Controller.getSetting( "strAmazonS33ProductionBucket" );

		// default to user id 1 for now
		var userID = 1;

		//writeDump(rc); abort;

		prc.stResults = documentServices.setDocumentDetailsInfo(
															formData = rc,
															stageBucketName = strStageBucketName,
															prodBucketName = strProdBucketName,
															userID = userID );

		// Call the Document Services and get the the list of Document Categories and documents to process
		structAppend(prc, documentServices.getDocumentsAdminData(
													codeType = strCategoryCode,
													bucketName = strStageBucketName ));

 		//writeDump(prc); abort;

		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		event.setView( "document_admin/viewlets/_refreshDocuments" );
	}


	function documentReset( event, rc, prc ){

		log.info ( "Inside documentReset Admin handler (180):" );

		var strStageBucketName = Controller.getSetting( "strAmazonS33StagingBucket" );

		var lstFileNames = "Blindspot.jpg,2013Tee_500.jpg,background_default.jpg,14_hatposter_back_500.jpg,amex2.gif,test-doc.doc,test-pdf.pdf,data.txt";
		var arrFileNames = listToArray(lstFileNames);


		writeOutput("strStageBucketName: " & strStageBucketName & "<P>");

		// Get the list of documents that are currently in the Amazon S3 Staging bucket
		arrayAmazonS3 = AmazonS3.getBucket( strStageBucketName );

		// Loop through the array of the Items in the Amazon S3 bucket
		for ( loop=1; loop LE ArrayLen(arrayAmazonS3); loop++ ) {

			writeOutput(" Deleting " & arrayAmazonS3[loop]["Key"] & ".......<BR>" );
			AmazonS3.deleteObject( strStageBucketName, arrayAmazonS3[loop]["Key"] );

		}
		writeOutput("<p>");

		// Push up the original list of test documents
		//for (i = 1; i LTE arrayLen(arrFileNames); i++ ) {
		for (i = 1; i LTE 1; i++ ) {

			writeOutput(arrFileNames[i] & "<BR>");
			var filepath = expandPath("\views\document_admin\temp_files\" & arrFileNames[i]);
			var strContentType = getPageContext().getServletContext().getMimeType(filepath);


			writeOutput("filepath: " & filepath & "<BR>");
			writeOutput("strContentType: " & strContentType & "<p>");
			//writeOutput("<img src='" & filepath & "'>");


/*
			var strA3Pushject= AmazonS3.putObjectFile(
												bucketName = strStageBucketName,
												filepath = filepath,
												//uri = arrFileNames[i],
												uri = 'test.jpg',
												contentType = 'image/jpeg',
												acl = 'public-read',
												expires = 60 );
*/

			//writeDump(strA3Pushject);

		}

		documentServices.setDocumentReset();

		//writeOutput("Document Reset complete");

//
		event.setView( "document_admin/index" );

	}
}