/**
* I am a new handler
*/
component{

	property name="correlationServices" 	inject=model;
	property name="landServices" 			inject=model;
	property name="commonCalls" 			inject=model;
	property name="htmlhelper" 				inject="htmlhelper@coldbox";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){

		prc.pageTitle = "RealCAMA Correlation";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );

		structAppend(prc,stCommonCalls);

		//writedump(prc.stPersistentInfo);

		// ----- Stored Proc to retrieve Land Information -----
		prc.qryGetCorrelationInfo = correlationServices.getCorrelationInfo( persistentCriteria = prc.stPersistentInfo);


		if(prc.qryGetCorrelationInfo.recordcount EQ 0) {
			event.setView( "correlation/noRecords" );
		} else {
			event.setView( "correlation/index" );
		}

	}


	/**
	* condoUnitSave
	*/
	function correlationSave( event, rc, prc ){
		prc.qryPersistentInfo = landServices.getPropPersistentData( persistentCriteria = rc );

		//writedump(prc.qryPersistentInfo);
		//writedump(rc);
		//abort;

		var stPersistentInfo = {
									parcelID = prc.qryPersistentInfo.parcelID[1],
									taxYear = prc.qryPersistentInfo.taxYear[1],
									parcelDisplayID = prc.qryPersistentInfo.parcelDisplayID[1]
								};

		prc.stResults = correlationServices.saveCorrelationInfo(persistentCriteria = stPersistentInfo, formData = rc);


		//writedump(#prc.stResults#);
		//abort;


		if(prc.stResults.status EQ 200) {
			setNextEvent( "correlation/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		} else {
			event.noLayout();
			hideDebugger();
			event.setView( "correlation/saveError" );
		}
	}
}
