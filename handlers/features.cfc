/**
* I am a new handler
*/
component{

	property name="featureServices" 	inject=model;
	property name="commonCalls" 		inject=model;
	property name="htmlhelper" 			inject="htmlhelper@coldbox";
	property name="pagingService" 		inject="PagingService@cbpagination";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = "GET,POST",
		featureSave = "POST"
	};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){
		var paginationSettings = "";

		prc.pageTitle = "RealCAMA Features";

		structAppend(prc,commonCalls.commonPageIncludes( rc ));
		structAppend(prc,featureServices.getAllFeaturesData(persistentCriteria = prc.stPersistentInfo));

		/*<!----- Stored Proc to retrieve the Features Information ----->
		prc.getFeaturesInfo = featureServices.getFeaturesInfo( persistentCriteria = prc.stPersistentInfo);

		<!----- Stored Proc to retrieve Feature Unit Type Codes ----->
		prc.qryFeatureUnitTypeCodes = featureServices.getFeatureUnitTypeCodes();

		<!----- Stored Proc to retrieve Features Unit Type ----->
		prc.getFeaturesCode = featureServices.getFeaturesCode(taxYear = prc.stPersistentInfo.taxYear);

		<!----- Stored Proc to retrieve Features Quality Codes ----->
		prc.getFeaturesQualityCodes = featureServices.getFeaturesQualityCodes(taxYear = prc.stPersistentInfo.taxYear);

		<!----- Stored Proc to retrieve Neighborhoods ----->
		prc.getNeighborhoods = featureServices.getNeighborhoods(persistentCriteria = prc.stPersistentInfo);

		<!----- Stored Proc to retrieve Special Conditions ----->
		prc.qryGetSpecialConditions = featureServices.getSpecialConditions();*/

		if(prc.getFeaturesInfo.recordcount EQ 0) {
			event.setView( "features/noRecords" );
		} else {
			paginationSettings = pagingService.getPageStart(rc, prc.getFeaturesInfo.RecordCount, Controller.getSetting("PagingMaxRows") );
			rc.page = paginationSettings.page;
			rc.page_size = paginationSettings.page_size;
			rc.start_row = paginationSettings.start_row;

			event.setView( "features/index" );
		}
	}

	/**
	* featureSave
	*/
	function featureSave( event, rc, prc ){

		var validationResults = validateModel( target=rc, constraints="parcelData" );

		if (validationResults.hasErrors() gt 0) {
			prc.stResults.status = -10;
			prc.stResults.message = "";

			for( item in validationResults.getErrors() ) {
			    prc.stResults.message = prc.stResults.message & item.getMessage() & '<br>';
			}

		} else {
			validationResults = validateModel( target=rc, constraints="featuresLineForm" );

			if (validationResults.hasErrors() gt 0) {
				prc.stResults.status = -11;
				prc.stResults.message = validationResults.getAllErrors();
			} else {
				prc.stResults = featureServices.saveExtraFeaturesInfo(formData = rc);
			}
		}

		if(prc.stResults.status EQ 200) {
			setNextEvent( "features/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		} else {
			event.noLayout();
			hideDebugger();
			event.setView( "features/saveError" );
		}
	}
}
