/**
* I am a new handler
*/
component{

	property name="landServices"		inject=model;
	property name="condoMasterServices"	inject=model;
	property name="commonCalls" 		inject=model;
	property name="htmlhelper" 			inject="htmlhelper@coldbox";
	property name="log"					inject="logbox:logger:{this}";


	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/


	function index( event, rc, prc ){

		prc.pageTitle = "RealCAMA Condo Master";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );

		prc.page_size = Controller.getSetting( "page_size" );
		structAppend(prc,stCommonCalls);

		var strCondoType = "";

		prc.getCondoLookup = condoMasterServices.getCondoLookup( persistentCriteria = prc.stPersistentInfo );

		<!----- Stored Proc to retrieve Condo Master Record ----->
		prc.qryCondoMasterData = condoMasterServices.getCondoMasterData( persistentCriteria = prc.stPersistentInfo );

		<!----- Stored Proc to retrieve Condo Unit Types ----->
		prc.qryCondoUnitTypesData = condoMasterServices.getCondoMasterUnitTypes( persistentCriteria = prc.stPersistentInfo );

		<!----- Stored Proc to retrieve Condo Unit Typical Codes ----->
		prc.qryCondoUnitTypicalCodesData = condoMasterServices.getCondoMasterUnitTypicalCodes( persistentCriteria = prc.stPersistentInfo );

		<!----- Stored Proc to retrieve Neighborhoods ----->
		prc.qryNeighborhoods = condoMasterServices.getNeighborhoods( persistentCriteria = prc.stPersistentInfo );

		<!----- Stored Proc to retrieve Condo Master Types ----->
		prc.qryCondoMasterTypes = condoMasterServices.getCondoMasterTypes( persistentCriteria = prc.stPersistentInfo );

		<!----- Stored Proc to retrieve Condo Quality Codes ----->
		prc.qryCondoQualityCodes = condoMasterServices.getCondoQualityCodes( persistentCriteria = prc.stPersistentInfo );

		<!----- Stored Proc to retrieve Condo Location Codes ----->
		strCondoType = "Location";
		prc.qryCondoLocations = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID =  prc.qryCondoMasterData.condoID, strCodeType = strCondoType);

		strCondoType = "Views";
		prc.qryCondoViews = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID =  prc.qryCondoMasterData.condoID, strCodeType = strCondoType);

		strCondoType = "Desirability";
		prc.qryCondoDesirability = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID =  prc.qryCondoMasterData.condoID, strCodeType = strCondoType);

		strCondoType = "Recreation";
		prc.qryCondoRecreation = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID =  prc.qryCondoMasterData.condoID, strCodeType = strCondoType);

		strCondoType = "Balcony";
		prc.qryCondoPorches = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID =  prc.qryCondoMasterData.condoID, strCodeType = strCondoType);

		strCondoType = "Parking";
		prc.qryCondoParking = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID =  prc.qryCondoMasterData.condoID, strCodeType = strCondoType);

		event.setView( "condoMaster/index" );

	}


	/**
	* showCondoMasterBaseValueUpdaterPopup
	*/
	function showCondoMasterBaseValueUpdaterPopup( event, rc, prc ){

		prc.pageTitle = "RealCAMA Condo Master";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );
		structAppend(prc,stCommonCalls);

		//writeDump(rc) abort;

		<!----- Stored Proc to retrieve Condo Unit Types ----->
		prc.qryCondoUnitTypesData = condoMasterServices.getCondoMasterUnitTypes( persistentCriteria = prc.stPersistentInfo );

		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		event.setView( "condoMaster/showCondoMasterBaseValueUpdaterPopup" );
	}


	function showCondoMasterValues ( event, rc, prc ){

		/* MODAL WINDOW */
		event.noLayout();
		hideDebugger();

		/* Setting up this tempoary structure until I come back through and update the Condo master calls to just pass in the
				individual variables instead of a struct */
		stPersistentInfo = {
							parcelID = parcelID,
							taxYear = taxYear,
							propertyType = "R"
							};

		<!----- Stored Proc to retrieve Condo Master Record ----->
		prc.qryCondoMasterData = condoMasterServices.getCondoMasterData( persistentCriteria = stPersistentInfo );

		<!----- Stored Proc to retrieve Condo Unit Types ----->
		prc.qryCondoUnitTypesData = condoMasterServices.getCondoMasterUnitTypes( persistentCriteria = stPersistentInfo );

		event.setView( "condoMaster/condoMasterValues" );
	}


	/**
	* showCondoMasterBaseValueUpdaterPopup
	*/
	function showCondoMasterUnitTypesPopup( event, rc, prc ){
		var strCondoType = "";

		//writeDump(rc)
		//abort;

		<!----- Stored Proc to retrieve Condo Location Codes ----->
		strCondoType = "Location";
		prc.qryGetLocationCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Views";
		prc.qryGetViewsCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Desirability";
		prc.qryGetDesirabilityCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Recreation";
		prc.qryGetRecreationCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Balcony";
		prc.qryGetBalconyCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);

		strCondoType = "Parking";
		prc.qryGetParkingCodes = condoMasterServices.getCondoCodes( taxYear = rc.taxYear, strCondoID = rc.condoID, strCodeType = strCondoType);


		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		event.setView( "condoMaster/showCondoMasterUnitTypesPopup" );
	}


	/**
	* condoMasterSave
	*/
	function condoMasterSave( event, rc, prc ){
		prc.qryPersistentInfo = landServices.getPropPersistentData( persistentCriteria=rc );
		<!---
		writedump(#prc.qryPersistentInfo#);
		abort;
		--->
		var stPersistentInfo = {
									parcelID = prc.qryPersistentInfo.parcelID[1],
									taxYear = prc.qryPersistentInfo.taxYear[1],
									parcelDisplayID = prc.qryPersistentInfo.parcelDisplayID[1]
								};

		prc.stResults = condoMasterServices.setCondoMasterInfo(persistentCriteria = stPersistentInfo, formData = rc);

		<!---
		writedump(#prc.stResults#);
		abort;
		--->

		if(prc.stResults.status EQ 200) {
			setNextEvent( "condoMaster/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		} else {
			event.noLayout();
			hideDebugger();
			event.setView( "condoMaster/saveError" );
		}
	}

}
