/**
* I am a new handler
*/
component{

	property name="compareServices" 	inject="model";
	property name="landServices" 		inject="model";
	property name="commonCalls" 		inject="model";
	property name="htmlhelper" 			inject="htmlhelper@coldbox";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
						index = 'GET',
						compare = 'POST'
						};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){

		prc.pageTitle = "RealCAMA Compare";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );

		structAppend(prc,stCommonCalls);

		event.setView( "compare/index" );
	}

	/**
	* compare
	*/
	function compare( event, rc, prc ){

		prc.pageTitle = "RealCAMA Compare";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );

		structAppend( prc,stCommonCalls );
		structAppend( prc,compareServices.getAllComparedData( persistentCriteria = prc.stPersistentInfo, rc = rc ) );

		// Recordsets for the Parcel #1 - Left Side
		/*prc.qLeftRecord = compareServices.getComparisonTabInfo( leftParcelID, leftYear, prc.stPersistentInfo.propertyType );
		prc.qLeftImage = compareServices.getLandImage( leftParcelID, leftYear, prc.stPersistentInfo.propertyType );
		prc.qLeftAdjustments = buildingServices.getBuildingFeaturesData( prc.qLeftRecord.buildingID );

		if (StructKeyExists(form,"rightYear")) {
			rightYear = leftYear;
		}

		// Recordsets for the Parcel #1 - Right Side
		prc.qRightRecord = compareServices.getComparisonTabInfo( rightParcelID, rightYear, prc.stPersistentInfo.propertyType );
		prc.qRightImage = compareServices.getLandImage( rightParcelID, rightYear, prc.stPersistentInfo.propertyType );
		prc.qRightAdjustments = buildingServices.getBuildingFeaturesData( prc.qRightRecord.buildingID );

		// Get the Comparison Rates for the
		prc.qleftComparisonRates = landServices.getComparisonRates( leftParcel, leftYear, "R" );
		prc.qRightComparisonRates = landServices.getComparisonRates( leftParcel, leftYear, "R" );
		*/

		event.setView( "compare/index" );

	}

}
