/**
* I am the handler for the homestead exeption application form
*/
component{
	property name="parcelServices" 	inject=model;
	property name="homesteadExemptServices" inject=model;
	property name="commonCalls" 	inject=model;
	property name="htmlhelper" 		inject="htmlhelper@coldbox";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = "POST",
		hexSave = "POST"
	};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* edit
	*/
	function index( event, rc, prc ){

		var stCommonCalls = commonCalls.commonPageIncludes( rc );
		prc.pageTitle = 'Original Application For Homestead And Related Tax Exemptions';

		structAppend(prc,stCommonCalls);

		prc.getHexInfo = homesteadExemptServices.getHexInfo(persistentCriteria = prc.stPersistentInfo);
		prc.getLegalInfo = parcelServices.getGetParcelLegalInfo(persistentCriteria = prc.stPersistentInfo);

		event.setView( "homesteadExemptform/index" );
	}

	/**
	* save
	*/
	function hexSave( event, rc, prc ){
		event.setView( "homesteadExemptform/save" );
	}



}
