/**
* I am a new handler
*/
component{

	property name="permitsServices" 	inject=model;
	property name="landServices" 		inject=model;
	property name="commonCalls" 		inject=model;
	property name="htmlhelper" 			inject="htmlhelper@coldbox";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";

	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
						index = 'GET',
						compare = 'POST',
						permitSave = 'POST',
						permitResequence = 'GET',
						permitResequenceSave = 'POST',
						addPermit = 'POST'
						};


	function preHandler( event, rc, prc, action, eventArguments ){
/*
		writeOutput("prc:");
		writeDump(prc);
		writeOutput("rc:");
		writeDump(rc);
		writeOutput("action:");
		writeDump(action);
		writeOutput("eventArguments:");
		writeDump(eventArguments);
		abort;
*/
		// Get the Names of the Labels from the Vendor table
		var VendorInfo = Controller.getSetting( "vendorInfo" );
		prc.permitsInspectionDateLabel = VendorInfo.permitsInspectionDateLabel;
		prc.permitsNextDateLabel = VendorInfo.permitsNextDateLabel;
		prc.permitsFinalDateLabel = VendorInfo.permitsFinalDateLabel;
		prc.permitsCODateLabel = VendorInfo.permitsCODateLabel;

		if ((event.getCurrentRoutedURL() NEQ "permits/permitResequence/")
			and (event.getCurrentRoutedURL() NEQ "permits/permitResequenceSave/")
			and (event.getCurrentRoutedURL() NEQ "permits/addPermit/")) {

			structAppend(prc, commonCalls.commonPageIncludes( rc ));
			prc.pageTitle = "RealCAMA Permits";
			prc.page_size = Controller.getSetting( "page_size" );
		}

	}

	/**
	* index
	*/
	function index( event, rc, prc ){

		prc.parcelType = 'R';
//writeDump(rc); abort;

		if(structKeyExists(rc, "status") and rc.status EQ 2) {
			prc.viewStatus = "D";
		} else {
			prc.viewStatus = "";
		}

		prc.getPermitsData = permitsServices.getAllPermitData(
												parcelID = prc.stPersistentInfo.parcelID,
												parcelType = prc.parcelType,
												status = prc.viewStatus
												);

		// Set the total number of pages
		prc.iTotalPages = Ceiling(prc.getPermitsData.getPermitsInfo.RecordCount / prc.page_size);

		//Track the number of permits in case they add any
		prc.iPermitCount = prc.getPermitsData.getPermitsInfo.RecordCount;

		prc.iPermitCount++;

		rc.page_size = prc.page_size;
		rc.page = 1;

		event.setView( "permits/index" );

	}


	/**
	* permitSave
	*/
	function permitSave( event, rc, prc ){

		var strPropertyType = 'R';

		prc.qryPersistentInfo = landServices.getPropPersistentData( persistentCriteria = rc );

		prc.stResults = permitsServices.setPermitInfo(
											parcelID = prc.qryPersistentInfo.parcelID[1],
											parcelType = strPropertyType,
											formData = rc);

		if(prc.stResults.status EQ 200) {
			setNextEvent( "permits/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		} else {
			event.noLayout();
			hideDebugger();
			writeOutput("Error!!!!! <BR>");
			writeDump(prc.stResults); abort;
			event.setView( "permits/saveError" );
		}
	}


	function permitResequence ( event, rc, prc ){
		/* MODAL WINDOW */
		event.noLayout();
		hideDebugger();


		prc.getPermitsData = permitsServices.getPermitInfo(
												parcelID = rc.parcelID,
												parcelType = rc.parcelType,
												status = rc.status
												);

		event.setView( "permits/permitResequence" );
	}


	/**
	* permitResequenceSave
	*/
	function permitResequenceSave( event, rc, prc ){

		//writeDump(rc); abort;

		prc.stResults = permitsServices.setPermitResequence(
												parcelID = rc.parcelID,
												resequenceList = rc.resequenceIDs,
												status = rc.status );

		if(prc.stResults.status EQ 200) {
			// Will need to log or display an error here
			setNextEvent( "permits/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		} else {
			event.noLayout();
			hideDebugger();
			setNextEvent( "permits/index/parcelID/#rc.parcelID#/taxYear/#rc.taxYear#" );
		}

	}

	function addPermit( event, rc, prc ){

		/*<!--- MODAL WINDOW --->*/
		event.noLayout();
		hideDebugger();

		prc.itemNumber = rc.rowNum;

		prc.getAddPermitsData = permitsServices.getAddPermitsData();

		//prc.getPermitTypes = permitsServices.getPermitTypes();

		event.setView( "permits/viewlets/_newPermitRow" );

	}

}
