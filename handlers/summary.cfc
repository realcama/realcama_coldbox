/**
* I am a new handler
*/
component{
	property name="commonCalls" 	inject=model;
	property name="summaryServices" inject=model;
	property name="htmlhelper" 		inject="htmlhelper@coldbox";
	
	
	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = "GET"	
	};
	
	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/
		
	/**
	* index
	*/
	function index( event, rc, prc ){			
		
		prc.pageTitle = "RealCAMA Summary";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );				
			
		structAppend(prc,stCommonCalls);
			
		prc.getSummaryData = summaryServices.getSummaryData( persistentCriteria = prc.stPersistentInfo );	
		prc.getSummaryExemptions = summaryServices.getSummaryExemptions( persistentCriteria = prc.stPersistentInfo );		
		prc.getSummaryPreviousYearsData = summaryServices.getSummaryPreviousYearsData( persistentCriteria = prc.stPersistentInfo );	
				
		event.setView( "summary/index" );						
	}
	
}
