/**
* A normal ColdBox Event Handler
*/
component{

	property name="sessionStorage" 			inject="sessionStorage@cbstorages";
	property name="searchServices" 			inject="searchServices@searchModule";
	property name="log"						inject="logbox:logger:{this}";
	property name="htmlhelper" 				inject="htmlhelper@coldbox";
	property name="pagingService" 			inject="PagingService@cbpagination";
	property name="userFavoritesServices"	inject="userFavoritesServices@searchModule";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";

	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = "GET",
		results = "POST",
		loadCriteria = "GET",
		saveCriteria = "POST",
		deleteFavorite = "POST,GET",
		setFavorite = "POST",
		getUpdatedSearchDropdownSelections = "POST"

	};

	function preHandler( event, rc, prc, action, eventArguments ){

		prc.pageTitle = "RealCAMA Search";
		prc.loggedInUserID = "1";

		if ( isdefined("rc.bMultiSelect") eq "no") {
			rc.bMultiSelect = controller.getSetting( "bMultiSelect" );
		}
	}

	/**
	* index
	*/
	function index( event, rc, prc ){
		sessionStorage.deleteVar("parcelSandBox");
		prc.pageTitle = "RealCAMA Search";

		var taxYear = "";
		var intCurrentTab = 0;
		//prc.loggedInUserID = "1";


		//var mConfig = controller.getSetting( "modules" );

		/* Retrieve the queries for the search drop downs */
		prc.getSearchData = searchServices.getSearchDropDowns( taxYear = taxYear );

		/* Retrieve the queries for the User favorites and saved searches */
		prc.getUserSearchData = searchServices.getUserSearchData( userID = prc.loggedInUserID );

		event.setView( "search/index" );
	}


	/**
	* results
	*/
	function results( event, rc, prc ){
		var paginationSettings = "";
		param name="rc.page" default="1";

		prc.pageTitle = "RealCAMA Search";

		if(rc.currentTab eq 0) {
			prc.getSearchResults = searchServices.getSearchPropertyResults( searchCriteriaRequest = rc );

			paginationSettings = pagingService.getPageStart(rc, prc.getSearchResults.RecordCount, Controller.getSetting("PagingMaxRows") );
			rc.page = paginationSettings.page;
			rc.page_size = paginationSettings.page_size;
			rc.start_row = paginationSettings.start_row;

			event.setView( "search/viewlets/_results_prop" );
		} else {
			prc.getSearchResults = searchServices.getSearchTPPResults( searchCriteriaRequest = rc );

			paginationSettings = pagingService.getPageStart(rc, prc.getSearchResults.RecordCount, Controller.getSetting("PagingMaxRows") );
			rc.page = paginationSettings.page;
			rc.page_size = paginationSettings.page_size;
			rc.start_row = paginationSettings.start_row;

			event.setView( "search/viewlets/_results_tpp" );
		}
	}

	/**
	* load saved search criteria
	*/
	function loadCriteria( event, rc, prc ){
		event.noLayout();
		//hideDebugger();

		prc.qrySearchCriteria = searchServices.loadCriteria( userID = rc.userID, searchID = rc.searchID );

		event.setView( "search/loadCriteria" );
	}

	/**
	* save search criteria
	*/
	function saveCriteria( event, rc, prc ){
		event.noLayout();
		hideDebugger();


		var stSaveCriteria = searchServices.saveCriteria( formData = rc );

		if(stSaveCriteria.status EQ 200) {
			/*<!--- Now that the search is saved, pull ALL of the saved search criteria and return it to the screen --->*/
			prc.getPropertySavedSearches = userFavoritesServices.getSearchSavedSearches( userID = rc.userID, currentTab = rc.currentTab);
			event.setView( "search/loadSavedSearches" );
		} else {
			/*<!--- And error occured while saving the search criteria --->*/
			event.setView( "search/errorSaving" );
		}
	}

	/**
	* delete saved search
	*/
	function deleteSearch( event, rc, prc ){

		userFavoritesServices.deleteSavedSearch( userID = 1, searchToNuke = rc.searchToNuke );

		/* Now that the search was removed, send the user back to the search page */
		setNextEvent( "search" );
	}

	function setFavorite( event, rc, prc ){
		event.noLayout();
		prc.loggedInUserID = 1;

		// Check to see if the TPP variable was passed in or not
		if (structKeyExists(rc, "TPP") EQ "NO") {
			// Set the TPP variable to an an empry string
			rc.TPP = "";
		}

		if ( val(rc.favoriteMarker) eq 0 ) {
			var stSaveFavorite = userFavoritesServices.setSaveFavorite(
											userID = prc.loggedInUserID,
											taxYear = rc.taxYear,
											parcelID = rc.parcelID,
											TPP = rc.TPP);
		} else {
			var stSaveFavorite = userFavoritesServices.setDeleteFavorite( 
											userID = prc.loggedInUserID,
											favoriteID = rc.favoriteMarker
											);
		}

		if(stSaveFavorite.status EQ 200) {
			prc.stSaveFavorite = stSaveFavorite.stFavoriteMarkerId;
		}
		event.setView( "search/saveFavorite" );
	}

	/**
	* delete saved favorite
	*/
	function deleteFavorite( event, rc, prc ){

		userFavoritesServices.deleteSavedFavorite( userID = 1, favoriteToNuke = rc.favoriteToNuke );

		/* Now that the favorite was removed, send the user back to the search page */
		setNextEvent( "search" );
	}

	/**
	* Change dropdowns based on year the user selected
	*/
	function getUpdatedSearchDropdownSelections( event, rc, prc ){

		event.noLayout();
		hideDebugger();

		prc.getNeighborhoods = searchServices.getNeighborhoods( taxYear = taxYear );
		prc.getSubdivisions = searchServices.getSubdivisions( taxYear = taxYear );
		prc.getTaxDistricts = searchServices.getTaxDistricts( taxYear = taxYear );

		event.setView( "search/getUpdatedSearchDropdownSelections" );
	}

	/**
	* Stores the parcels the user selected from the search results
	* parcelSandBox variable is used in commonCalls.cfc and property.cfm to determine
	* which parcel to display.
	*/
	function goToParcel(event, rc, prc) {

		if ( listlen(rc.parcelList, ",") > 1 ) {
			sessionStorage.setVar("parcelSandBox", rc.parcelList);
		};

		setNextEvent( "parcel/index/taxYear/#listfirst( listfirst( rc.parcelList, "," ), "|" )#/parcelID/#listlast( listfirst( rc.parcelList, "," ), "|" )#" );
	}

	/**
	* Stores the parcels the user selected from the search results
	* parcelSandBox variable is used in commonCalls.cfc and property.cfm to determine
	* which parcel to display.
	*/
	function goToTangible(event, rc, prc) {

		if ( listlen(rc.parcelList, ",") > 1 ) {
			sessionStorage.setVar("parcelSandBox", rc.parcelList);
		};

		setNextEvent( "tangible/index/tpp/#listlast( listfirst( rc.parcelList, "," ), "|" )#/taxYear/#listfirst( listfirst( rc.parcelList, "," ), "|" )#/parcelID/#listgetat( listfirst( rc.parcelList, "," ), 2, "|" )#" );
	}
}