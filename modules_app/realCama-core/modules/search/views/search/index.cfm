<cfoutput>

<!--- TO DO --->
<CFSET userID = 1>

<cfparam Name="client.currentTaxYear" Default="2016">


<script>
	var currentTaxYear = #client.currentTaxYear#;

	function checkForm() {
		var isValid = false;
		$(".doNotLeaveEmpty").each(function() {
		   var element = $(this);
		   console.log(element);
		   if (element.val() != "") {
		       isValid = true;
		   }
		});
		if(isValid == false) {
			$('##searchEmpty').modal('show');
		}
		return isValid;
	}

</script>

<CFSET whatTabIsDisplayed = 0>
<CFIF IsDefined("CLIENT.SearchTab")>
	<CFSET whatTabIsDisplayed = CLIENT.SearchTab>

	<CFIF CLIENT.SearchTab EQ 0>
		<CFSET locationHash = "property">
	<CFELSE>
		<CFSET locationHash = "tpp">
	</CFIF>
	<script>
		$( document ).ready(function() {
			// show active tab on reload
  				$('##searchTabs a[href="###locationHash#"]').tab('show');
		});
	</script>
</CFIF>


<!--- Used to tell the "search" button which form to submit --->
	<input type="hidden" name="whatTabIsDisplayed" id="whatTabIsDisplayed" value="#whatTabIsDisplayed#">

	<span class="pull-left"><input type="button" onclick="frmRefresh()"  value="Clear and Refresh"></span>
	<span class="pull-right"><input type="button" onclick="frmSubmit()" value="SEARCH" class="search-button-text"></span>

	<br clear="all"/>
	<br clear="all"/>


	<ul id="searchTabs" class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="/index.cfm/#event.getCurrentRoutedURL()###property" aria-controls="property" role="tab" data-toggle="tab">Real Estate Search</a></li>
		<li role="presentation"><a href="/index.cfm/#event.getCurrentRoutedURL()###tpp" aria-controls="tpp" role="tab" data-toggle="tab">Tangible Personal Property Search</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="property">
			<form action="#event.buildLink('search/results')#" method="post" id="frmSearchProp" name="frmSearchProp" onsubmit="return checkForm()">
				<input type="hidden" name="mode" value="search">
				<input type="hidden" name="userID" value="#userID#">
				<input type="hidden" name="currentTab" id="currentTab" value="0">

				<div class="search-left-box">
					<div id="realEstateSearchBox">
						<table class="search-table">
							<thead>
							<tr>
								<td class="content-bar-label">Real Estate Search</td>
							</tr>
							</thead>
						</table>
						<table class="search-table">
						<colgroup>
							<col width="20">
							<col width="116">
							<col width="491">
						</colgroup>
						<tbody>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the parcel's Owner's Name as recorded by the county."></td>
								<td class="formatted-data-label">Owner Name</td>
								<td><input type="text" name="ownerName" id="ownerName" class="form-control doNotLeaveEmpty"></td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter  by the parcel's Parcel Identifier as determined by the county."></td>
								<td class="formatted-data-label">Parcel ID Number</td>
								<td ><input type="text" name="parcelNumber" id="parcelNumber" class="form-control doNotLeaveEmpty"></td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter  by the  Situs Address as determined by the county."></td>
								<td class="formatted-data-label">Situs Address</td>
								<td>
									<table id="situs-address">
									<tr>
										<td class="search-text-align-center formatted-data-label">Number</td>
										<td class="search-text-align-center formatted-data-label">Dir</td>
										<td class="search-text-align-center formatted-data-label">Street Name </td>
										<td class="search-text-align-center formatted-data-label">Type</td>
										<td class="search-text-align-center formatted-data-label">Post</td>
									</tr>
									<tr>
										<td><input type="text" name="number" id="number" size="6" class="form-control doNotLeaveEmpty"></td>
										<td><input type="text" name="dir" id="dir" size="2" class="form-control doNotLeaveEmpty"></td>
										<td><input type="text" name="street" id="street" size="30" class="form-control doNotLeaveEmpty"></td>
										<td><input type="text" name="type" id="type" size="3" class="form-control doNotLeaveEmpty"></td>
										<td><input type="text" name="post" id="post" size="4" class="form-control doNotLeaveEmpty"></td>
									</tr>
									<tr>
										<td colspan="3" class="search-text-align-center formatted-data-label">City</td>
										<td colspan="2" class="search-text-align-center formatted-data-label">Zip Code</td>
									</tr>
									<tr>
										<td colspan="3" class="search-text-align-center formatted-data-label"><input type="text" name="city" id="city" class="form-control doNotLeaveEmpty"></td>
										<td colspan="2" class="search-text-align-center formatted-data-label"><input type="text" name="zipcode" id="zipcode" class="form-control doNotLeaveEmpty"></td>
									</tr>

									</table>
								</td>
							</tr>

							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the  Subdivision Name or Code as determined by the county."></td>
								<td class="formatted-data-label">Subdivision</td>
								<td class="subdivision">
									<select name="subdivisionCode" id="subdivisionCode" size="1" class="form-control two-items-on-row-longerCode select2-2piece-new-larger doNotLeaveEmpty">
										<option value="" data-val="" data-desc="ALL">ALL</option>

										<cfloop query="prc.getSearchData.getSubdivisions">
											<option value="#prc.getSearchData.getSubdivisions.subDivisionCode#" data-val="#prc.getSearchData.getSubdivisions.subDivisionCode#" data-desc="#prc.getSearchData.getSubdivisions.subDivisionCodeDescription#">#prc.getSearchData.getSubdivisions.subDivisionCode#: #prc.getSearchData.getSubdivisions.subDivisionCodeDescription#</option>
										</cfloop>

									</select>
								</td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This filter determines the year(s) to search"></td>
								<td class="formatted-data-label">Tax Year</td>
								<td>
									<select name="year" id="year" size="1" class="form-control two-items-on-row select2-2piece-new-larger doNotLeaveEmpty" onchange="filterYears(this.value)">
										<option value="" data-val="" data-desc="ALL">ALL</option>
										<cfloop index="iYear" from="#year(now())#" to="#year(now())-9#" step="-1">
											<option value="#iYear#" data-val="" data-desc="#iYear#" <CFIF client.currentTaxYear EQ iYear>selected</CFIF>>#iYear#</option>
										</cfloop>
									</select>
								</td>
							</tr>
						</tbody>
						</table>
					</div>


					<div id="specialFiltersSearchBox">
						<table class="search-table">
							<thead>
							<tr>
								<td class="content-bar-label">Special Filters</td>
							</tr>
							</thead>
						</table>

						<table class="search-table">
						<colgroup>
							<col width="20">
							<col width="116">
							<col width="491">
						</colgroup>
						<tbody>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the  Building Permit Number as listed on the Property Appraisers Files"></td>
								<td class="formatted-data-label">Building Permit Number</td>
								<td><input type="text" name="buildingPermit" id="buildingPermit" class="form-control doNotLeaveEmpty"></td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Exemption code(s) as listed on the Property Appraisers Files"></td>
								<td class="formatted-data-label">Exemption</td>
								<td>
									<select name="exemption" id="exemption" class="selectpicker form-control" title="Select one or more.."
											multiple data-selected-text-format="count > 2" data-actions-box="true"  data-width="100%" data-size="12" data-header="Select exemptions">
											<option style="display:none" value=""></option>
										<cfloop query="prc.getSearchData.getExemptions">
											<option value="#prc.getSearchData.getExemptions.code#" data-val="#prc.getSearchData.getExemptions.code#" data-desc="#prc.getSearchData.getExemptions.ExemptionCodeDescription#&nbsp;">#prc.getSearchData.getExemptions.code#: #prc.getSearchData.getExemptions.ExemptionCodeDescription#&nbsp;</option>
										</cfloop>
									</select>
								</td>
							</tr>
						<!---	<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Exemption code(s) as listed on the Property Appraisers Files"></td>
								<td class="formatted-data-label">Exemption</td>
								<td>
									<select name="exemption2" id="exemption2" class="form-control two-items-on-row-longerCode select2-2piece-new-larger " title="Select one or more.."
											multiple data-selected-text-format="count > 2" data-actions-box="true"  data-width="100%" data-size="10"  data-header="Select exemptions">
										<cfloop query="prc.getSearchData.getExemptions">
											<option value="#prc.getSearchData.getExemptions.code#" data-val="#prc.getSearchData.getExemptions.code#" data-desc="#prc.getSearchData.getExemptions.ExemptionCodeDescription#&nbsp;">#prc.getSearchData.getExemptions.code#: #prc.getSearchData.getExemptions.ExemptionCodeDescription#&nbsp;</option>
										</cfloop>
									</select>
								</td>
							</tr> --->
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Legal Section, Township, Range, Subdivision, Block, and Lot as determined by the county."></td>
								<td class="formatted-data-label">Legal</td>
								<td>
									<table>
										<tr>
											<td class="search-text-align-center formatted-data-label">Section</td>
											<td class="search-text-align-center formatted-data-label">Township</td>
											<td class="search-text-align-center formatted-data-label">Range</td>
											<td rowspan="2">&nbsp;&nbsp;</td>
											<td class="search-text-align-center formatted-data-label">Subdivision</td>
											<td class="search-text-align-center formatted-data-label">Block</td>
											<td class="search-text-align-center formatted-data-label">Lot</td>
										</tr>

										<tr>
											<td><input type="text" name="section" id="section" class="form-control doNotLeaveEmpty"></td>
											<td><input type="text" name="township" id="township" class="form-control doNotLeaveEmpty"></td>
											<td><input type="text" name="range" id="range" class="form-control doNotLeaveEmpty"></td>
											<td><input type="text" name="subdivision" id="subdivision" class="form-control doNotLeaveEmpty"></td>
											<td><input type="text" name="block" id="block" class="form-control doNotLeaveEmpty"></td>
											<td><input type="text" name="lot" id="lot" class="form-control doNotLeaveEmpty"></td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Map number as determined by the county"></td>
								<td class="formatted-data-label">Map Number</td>
								<td ><input type="text" name="mapNumber" id="mapNumber" class="form-control doNotLeaveEmpty"></td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Neighborhood Name or Code as determined by the county"></td>
								<td class="formatted-data-label">Neighborhood</td>
								<td class="neighborhood">
									<select name="neighborhoodCode" id="neighborhoodCode" size="1" class="form-control two-items-on-row-longerCode select2-2piece-new-larger doNotLeaveEmpty">
										<option value="" data-val="" data-desc="ALL">ALL</option>

										<cfloop query="prc.getSearchData.getNeighborhoods">
											<option value="#prc.getSearchData.getNeighborhoods.nbhdcd#" data-val="#prc.getSearchData.getNeighborhoods.nbhdcd#" data-desc="#prc.getSearchData.getNeighborhoods.nbhdds#&nbsp;">#prc.getSearchData.getNeighborhoods.nbhdcd#: #prc.getSearchData.getNeighborhoods.nbhdds#&nbsp;</option>
										</cfloop>
									</select>
								</td>
							</tr>

							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Use Codeas determined by the county"></td>
								<td class="formatted-data-label">Primary Use</td>
								<td class="usecode">
									<select name="usecode" id="usecode" size="1" class="form-control two-items-on-row-longerCode select2-2piece-new-larger doNotLeaveEmpty">
										<option value="" data-val="" data-desc="ALL">ALL</option>

										<cfloop query="prc.getSearchData.getPrimaryUse">
											<option value="#prc.getSearchData.getPrimaryUse.useCode#" data-val="#prc.getSearchData.getPrimaryUse.useCode#" data-desc="#prc.getSearchData.getPrimaryUse.useCodeDescription#">#prc.getSearchData.getPrimaryUse.useCode#: #prc.getSearchData.getPrimaryUse.useCodeDescription#</option>
										</cfloop>

									</select>
								</td>
							</tr>

							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Tax District Name or Code as determined by the county"></td>
								<td class="formatted-data-label">Tax District</td>
								<td class="taxdistrict">
									<select name="taxDistrict" id="taxDistrict" size="1" class="form-control two-items-on-row select2-2piece-new-larger doNotLeaveEmpty">
										<option value="" data-val="" data-desc="ALL">ALL</option>

										<cfloop query="prc.getSearchData.getTaxDistricts">
											<CFIF LEN(TRIM(prc.getSearchData.getTaxDistricts.txdtds)) NEQ 0>
												<CFSET strText = prc.getSearchData.getTaxDistricts.txdtds>
											<CFELSE>
												<CFSET strText = "&nbsp;">
											</CFIF>
											<option value="#prc.getSearchData.getTaxDistricts.txdtcd#" data-val="#prc.getSearchData.getTaxDistricts.txdtcd#" data-desc="#strText#">#prc.getSearchData.getTaxDistricts.txdtcd#: #strText#</option>
										</cfloop>

									</select>
								</td>
							</tr>
						</tbody>
						</table>

					</div>
					<!---
					<div id="sortSearchBox">
						<table class="search-table">
							<thead>
							<tr>
								<td class="content-bar-label">Sort Order</td>
							</tr>
							</thead>
						</table>

						<table class="search-table">
						<colgroup>
							<col width="20">
							<col width="116">
							<col width="491">
						</colgroup>
						<tbody>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This you to set the primary search criteria for returned results"></td>
								<td class="formatted-data-label">Primary Sort</td>
								<td>
									<select name="sort_order" id="sort_order" size="1" class="form-control two-items-on-row-longerCode select2-2piece-new-larger doNotLeaveEmpty">
										<!--- <option value="lanbhdc" data-val="" data-desc="Neighborhood">Neighborhood</option> --->
										<option value="laprop" data-val="" data-desc="Parcel Number">Parcel Number</option>
										<option value="OwnerName" data-val="" data-desc="Owner Name">Owner Name</option>
										<option value="UseCode" data-val="" data-desc="Use Code">Use Code</option>
										<option value="PEFLTXDIST" data-val="" data-desc="Tax District">Tax District</option>
										<option value="lapyear" data-val="" data-desc="Tax Year">Tax Year</option>
										<option value="AG" data-val="" data-desc="AG">AG</option>
										<option value="Exemptions" data-val="" data-desc="Exemption(s)">Exemption(s)</option>
										<option value="SitusHouseNumber" data-val="" data-desc="Situs Address">Situs Address</option>
									</select>
								</td>
							</tr>
						</tbody>
						</table>

					</div>
					 --->
				 	<input type="hidden" id="sort_order" name="sort_order" value="parcelID">
				</div>

				<div class="search-right-box">
					<div class="savedSearchesBox">
						<table class="search-table">
						<thead>
							<tr>
								<td width="20"><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="left" title="This feature allows you to save you search criteria for future use. To save a search, enter a label in the text box provided, then click 'Save Search Criteria' to store your criteria"></td>
								<td class="content-bar-label">Save Search Criteria</td>
							</tr>
						</thead>
						</table>
						<span class="formatted-data-label">Enter name and click Save to store search:</span><br/>
						<input type="text" name="saveSearchName" id="saveSearchName" class="form-control">
						<br/>
						<input type="button"  value="Save Search Criteria" onclick="saveSearchCriteriaProp()">
						<br/>
						<br/>
						<table class="search-table-scroll">
							<thead>
							<tr>
								<td width="20"><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This feature allows you to load previously saved search criteria. To load, click on the label of a saved search, verify your criteria, then click 'Search' to view the results"></td>
								<td class="content-bar-label">Load Saved Search</td>
							</tr>
							</thead>
							<tbody id="save-search-results">
							<tr>
								<td></td>
								<td><span class="formatted-data-label">Click name to load a previously saved search:</span></td>
							</tr>

							<cfloop query="prc.getUserSearchData.getPropertySavedSearches">
								<tr>
									<td><a href="javascript:loadSearch(#prc.getUserSearchData.getPropertySavedSearches.savedSearchID#,#userID#)">#prc.getUserSearchData.getPropertySavedSearches.searchName#</a></td>
									<td width="20"><a href="javascript:deleteConfirm(#prc.getUserSearchData.getPropertySavedSearches.savedSearchID#);"><img src="/includes/images/search/x_icon.png"></a></td>
								</tr>
							</cfloop>

							</tbody>

						</table>
					</div>


					<div class="FavoritesBox">
						<table class="search-table">
							<thead>
							<tr>
								<td width="20"><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This feature allows you to load previously saved favorites. To load, click on the label of a favorite, verify your criteria, then click 'Search' to view the results"></td>
								<td class="content-bar-label">Favorites</td>
							</tr>
							</thead>
						</table>

						<table class="search-table-scroll-small">
						<tbody>
							<tr>
								<td></td>
								<td><span class="formatted-data-label">Click parcel to load a previously saved favorite:</span></td>
							</tr>

							<cfloop query="prc.getUserSearchData.getPropertyFavorites">
								<tr>
									<td><a href="javascript:setFavSearchData('#prc.getUserSearchData.getPropertyFavorites.taxYear#','#prc.getUserSearchData.getPropertyFavorites.ParcelID#','#prc.getUserSearchData.getPropertyFavorites.tpp#')"><CFIF LEN(TRIM(prc.getUserSearchData.getPropertyFavorites.ParcelID)) NEQ 0>#prc.getUserSearchData.getPropertyFavorites.taxYear# #prc.getUserSearchData.getPropertyFavorites.ParcelID#<CFELSE>#formatTPPNumber(prc.getUserSearchData.getPropertyFavorites.tpp)#</CFIF></a></td>
									<td width="20"><a href="javascript:deleteFavConfirm(#prc.getUserSearchData.getPropertyFavorites.favoriteID#);"><img src="/includes/images/search/x_icon.png"></a></td>
								</tr>
							</cfloop>

						</tbody>
						</table>
					</div>


				</div>
			</form>
		</div>

		<div role="tabpanel" class="tab-pane" id="tpp">
			<form action="#event.buildLink('search/results')#" method="post" id="frmSearchTpp" onsubmit="return checkForm()">
				<input type="hidden" name="mode" value="search">
				<input type="hidden" name="userID" value="#userID#">
				<input type="hidden" name="currentTab" id="currentTab" value="1">

				<div class="search-left-box">

					<div id="TangiblePersonalPropertySearchBox">
						<table class="search-table">
							<thead>
							<tr>
								<td class="content-bar-label">Tangible Personal Property</td>
							</tr>
							</thead>
						</table>


						<table class="search-table">
						<colgroup>
							<col width="20">
							<col width="116">
							<col width="491">
						</colgroup>
						<tbody>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter  by the Owner Name Identifier as determined by the county."></td>
								<td class="formatted-data-label">Owner Name</td>
								<td><input type="text" name="ownerName2" id="ownerName2" class="form-control doNotLeaveEmpty"></td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter  by the Account Number Identifier as determined by the county."></td>
								<td class="formatted-data-label">Account Number</td>
								<td><input type="text" name="accountNumber" id="accountNumber" class="form-control doNotLeaveEmpty"></td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter  by the Accounts Alternate Identifier as determined by the county."></td>
								<td class="formatted-data-label">Alternate ID</td>
								<td><input type="text" name="parcelNumber2" id="parcelNumber2" class="form-control doNotLeaveEmpty"></td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by theAccounts Business Name as reported on the return"></td>
								<td class="formatted-data-label">Business Name</td>
								<td><input type="text" name="businessName" id="businessName" class="form-control doNotLeaveEmpty"></td>
							</tr>
						<!---	<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Location of the property as determined by the county"></td>
								<td class="formatted-data-label">Location</td>
								<td><input type="text" name="location" id="location" class="form-control doNotLeaveEmpty"></td>
							</tr> --->
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Location of the property as determined by the county"></td>
								<td class="formatted-data-label">Location</td>
								<td>
									<table id="situs-address">
									<tr>
										<td class="search-text-align-center formatted-data-label">Number</td>
										<td class="search-text-align-center formatted-data-label">Street Name </td>
										<td class="search-text-align-center formatted-data-label">Type</td>
										<td class="search-text-align-center formatted-data-label">Unit</td>
									</tr>
									<tr>
										<td><input type="text" name="number" id="number" size="6" class="form-control doNotLeaveEmpty"></td>
										<td><input type="text" name="street" id="street" size="30" class="form-control doNotLeaveEmpty"></td>
										<td><input type="text" name="type" id="type" size="3" class="form-control doNotLeaveEmpty"></td>
										<td><input type="text" name="post" id="post" size="4" class="form-control doNotLeaveEmpty"></td>
									</tr>
									<tr>
										<td colspan="3" class="search-text-align-center formatted-data-label">City</td>
										<td colspan="2" class="search-text-align-center formatted-data-label">Zip Code</td>
									</tr>
									<tr>
										<td colspan="3" class="search-text-align-center formatted-data-label"><input type="text" name="city" id="city" class="form-control doNotLeaveEmpty"></td>
										<td colspan="2" class="search-text-align-center formatted-data-label"><input type="text" name="zipcode" id="zipcode" class="form-control doNotLeaveEmpty"></td>
									</tr>

									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This search allows you to filter by the Tax District Name or Code as determined by the county"></td>
								<td class="formatted-data-label">Tax District</td>
								<td>
									<select name="taxDistrict2" id="taxDistrict2" size="1" class="form-control two-items-on-row select2-2piece-new-larger doNotLeaveEmpty">
										<option value="" data-val="" data-desc="ALL">ALL</option>

										<cfloop query="prc.getSearchData.getTaxDistricts">
											<CFIF LEN(TRIM(prc.getSearchData.getTaxDistricts.txdtds)) NEQ 0>
												<CFSET strText = prc.getSearchData.getTaxDistricts.txdtds>
											<CFELSE>
												<CFSET strText = "&nbsp;">
											</CFIF>
											<option value="#prc.getSearchData.getTaxDistricts.txdtcd#" data-val="#prc.getSearchData.getTaxDistricts.txdtcd#" data-desc="#strText#">#prc.getSearchData.getTaxDistricts.txdtcd#: #strText#</option>
										</cfloop>

									</select>
								</td>
							</tr>
							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This filter allows you to search Accounts by the last 4 digits of the Tax ID number as reported on the return"></td>
								<td class="formatted-data-label">Tax ID Number (TIN)</td>
								<td><input type="text" name="einssn" id="einssn" class="form-control doNotLeaveEmpty"></td>
							</tr>

							<tr>
								<td><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This filter determines the year(s) to search"></td>
								<td class="formatted-data-label">Tax Year</td>
								<td>
									<select name="year2" id="year2" size="1" class="form-control two-items-on-row select2-2piece-new-larger doNotLeaveEmpty">
										<option value="" data-val="" data-desc="ALL">ALL</option>
										<cfloop index="iYear" from="#year(now())#" to="#year(now())-9#" step="-1">
											<option value="#iYear#" data-val="" data-desc="#iYear#"  <CFIF client.currentTaxYear EQ iYear>selected</CFIF>>#iYear#</option>
										</cfloop>
									</select>
								</td>
							</tr>
						</tbody>
						</table>
					</div>


				</div>

				<div class="search-right-box">
					<div class="savedSearchesBox">
						<table class="search-table">
						<thead>
							<tr>
								<td width="20"><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="left" title="This feature allows you to save you search criteria for future use. To save a search, enter a label in the text box provided, then click 'Save Search Criteria' to store your criteria"></td>
								<td class="content-bar-label">Save Search Criteria</td>
							</tr>
						</thead>
						</table>
						<span class="formatted-data-label">Enter name and click Save to store search:</span><br/>
						<input type="text" name="saveSearchName2" id="saveSearchName2" class="form-control">
						<br/>
						<input type="button" value="Save Search Criteria" onclick="saveSearchCriteriaTpp()">
						<br/>
						<br/>
						<table class="search-table-scroll">
							<thead>
							<tr>
								<td width="20"><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This feature allows you to load previously saved search criteria. To load, click on the label of a saved search, verify your criteria, then click 'Search' to view the results"></td>
								<td class="content-bar-label">Load Saved Search</td>
							</tr>
							</thead>
							<tbody id="save-search-results-tpp">
							<tr>
								<td></td>
								<td><span class="formatted-data-label">Click name to load a previously saved search:</span></td>
							</tr>

							<cfloop query="prc.getUserSearchData.getTPPSavedSearches">
							<tr>
								<td><a href="javascript:void(0);" onclick="loadSearch(#prc.getUserSearchData.getTPPSavedSearches.savedSearchID#,#userID#)">#prc.getUserSearchData.getTPPSavedSearches.searchName#</a></td>
								<td width="20"><a href="javascript:deleteConfirm(#prc.getUserSearchData.getTPPSavedSearches.savedSearchID#);"><img src="/includes/images/search/x_icon.png"></a></td>
							</tr>
							</cfloop>
							</tbody>

						</table>
					</div>


					<div class="FavoritesBox">
						<table class="search-table">
							<thead>
							<tr>
								<td width="20"><img src="/includes/images/search/info_icon.png" data-toggle="tooltip" data-placement="top" title="This feature allows you to load previously saved favorites. To load, click on the label of a favorite, verify your criteria, then click 'Search' to view the results"></td>
								<td class="content-bar-label">Favorites</td>
							</tr>
							</thead>
						</table>

						<table class="search-table-scroll-small">
						<tbody>
							<tr>
								<td></td>
								<td><span class="formatted-data-label">Click parcel to load a previously saved favorite:</span></td>
							</tr>

							<cfloop query="prc.getUserSearchData.getTPPFavorites">
								<tr>
									<td><a href="javascript:setFavSearchData('#prc.getUserSearchData.getTPPFavorites.taxYear#','#prc.getUserSearchData.getTPPFavorites.ParcelID#','#prc.getUserSearchData.getTPPFavorites.tpp#')"><CFIF LEN(TRIM(prc.getUserSearchData.getTPPFavorites.ParcelID)) NEQ 0>#prc.getUserSearchData.getTPPFavorites.taxYear# #prc.getUserSearchData.getTPPFavorites.ParcelID#<CFELSE>#formatTPPNumber(prc.getUserSearchData.getTPPFavorites.tpp)#</CFIF></a></td>
									<td width="20"><a href="javascript:deleteFavConfirm(#prc.getUserSearchData.getTPPFavorites.favoriteID#);"><img src="/includes/images/search/x_icon.png"></a></td>
								</tr>
							</cfloop>

						</tbody>
						</table>
					</div>
				</div>
			</form>
		</div>

	</div>


	<br clear="all"/>
	<br clear="all"/>

	<span class="pull-left"><input type="button" onclick="frmRefresh()" value="Clear and Refresh"></span>
	<span class="pull-right"><input type="button" onclick="frmSubmit()" value="SEARCH" class="search-button-text"></span>

	#addAsset('/includes/css/bootstrap-select.css')#
	#addAsset('/includes/js/search.js')#
	#addAsset('/includes/js/bootstrap-select.js')#
</cfoutput>