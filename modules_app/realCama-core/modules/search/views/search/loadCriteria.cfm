<cfsetting showdebugoutput="no">
<cfoutput query="prc.qrySearchCriteria">

	<CFIF prc.qrySearchCriteria.tab EQ "0">
		$("##buildingPermit").val("#prc.qrySearchCriteria.buildingPermit#");
		$("##exemption").val("#prc.qrySearchCriteria.exemption#");
		$("##section").val("#prc.qrySearchCriteria.section#");
		$("##township").val("#prc.qrySearchCriteria.township#");
		$("##range").val("#prc.qrySearchCriteria.range#");
		$("##subdivision").val("#prc.qrySearchCriteria.subdivision#");
		$("##block").val("#prc.qrySearchCriteria.block#");
		$("##lot").val("#prc.qrySearchCriteria.lot#");
		$("##mapNumber").val("#prc.qrySearchCriteria.mapnumber#");
		$("##ownerName").val("#prc.qrySearchCriteria.ownerName#");
		$("##parcelNumber").val("#prc.qrySearchCriteria.parcelID#");
		$("##number").val("#prc.qrySearchCriteria.number#");
		$("##street").val("#prc.qrySearchCriteria.street#");
		$("##type").val("#prc.qrySearchCriteria.type#");
		$("##city").val("#prc.qrySearchCriteria.city#");
		$("##zipcode").val("#prc.qrySearchCriteria.zipcode#");
		$("##taxDistrict").select2("val","#prc.qrySearchCriteria.taxDistrict#");
		$("##year").select2("val","#prc.qrySearchCriteria.taxYear#");
		$("##usecode").select2("val","#prc.qrySearchCriteria.UseCode#");
		$("##neighborhoodCode").select2("val","#prc.qrySearchCriteria.neighborhoodCode#");
		$("##sort_order").select2("val","#prc.qrySearchCriteria.sort_order#");
		$('##searchTabs a[href="##property"]').tab('show');
	<CFELSE>
		$("##ownerName2").val("#prc.qrySearchCriteria.ownerName2#");
		$("##accountNumber").val("#prc.qrySearchCriteria.accountNumber#");
		$("##businessName").val("#prc.qrySearchCriteria.businessName#");
		$("##einssn").val("#prc.qrySearchCriteria.einssn#");
		$("##location").val("#prc.qrySearchCriteria.location#");
		$("##parcelNumber2").val("#prc.qrySearchCriteria.parcelNumber2#");
		$("##taxDistrict2").select2("val","#prc.qrySearchCriteria.taxDistrict2#");
		$("##subdivisionCode").select2("val","#prc.qrySearchCriteria.subdivisionCode#");
		$("##year2").select2("val","#prc.qrySearchCriteria.taxYear2#");
		$('##searchTabs a[href="##tpp"]').tab('show');
	</CFIF>

</cfoutput>


