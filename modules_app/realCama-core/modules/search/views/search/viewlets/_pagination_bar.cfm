<cfoutput>
<cfparam name="rc.start_row" default="1">
<cfparam name="link" default="">
<cfparam name="rc.page" default="1">
<cfparam name="rc.pgParcelList" default="">
<cfset rcKeyExceptions = "start_row,fieldnames,pgParcelList,bMultiSelect" />

<cfscript>
	link = "javascript:searchPage(@page@);";
	p = getInstance( 'PagingService@cbpagination' );
</cfscript>

<div class="table-header content-bar-label pagination-bar">
	<!--- rc.bMultiSelect is initially set in the flash scope in the coldbox.cfc --->
	<cfif rc.bMultiSelect>
		<cfset btnMultiSelectDisplay = "Cancel" />
		<cfset multiSelectValue = "false" />
	<cfelse>
		<cfset btnMultiSelectDisplay = "Select" />
		<cfset multiSelectValue = "true" />
	</cfif>
	<span class="col-sm-5">Your search yielded #prc.getSearchResults.RecordCount# match<CFIF prc.getSearchResults.RecordCount NEQ 1>es</CFIF>:</span>
	<span class="col-sm-2 text-center"><button type="button" name="btnMultiSelect" id="btnMultiSelect" value="#multiSelectValue#" class="btn btn-small btn-primary">#btnMultiSelectDisplay#</button></span>
	<span class="col-sm-5 text-right">#p.refresh_page_renderit(prc.getSearchResults.RecordCount, link, rc.page, rc.page_size)#</span>
</div>

<form action="#event.buildLink('search/results')#" method="post" name="frmSearchPagination" id="frmSearchPagination">
	<input type="hidden" name="start_row" id="start_row" value="#rc.start_row#">
	<input type="hidden" name="pgParcelList" id="pgParcelList" value="#rc.pgParcelList#">
	<input type="hidden" name="bMultiSelect" id="bMultiSelect" value="#rc.bMultiSelect#">
	<cfloop collection=#rc# item="structKey">
		<cfif listfindnocase(rcKeyExceptions, structKey, ",") eq 0>
			<input type="hidden" name="#lcase(structKey)#" id="#lcase(structKey)#" value="#Evaluate("rc." & structKey)#">
		</cfif>
	</cfloop>
</form>
</cfoutput>