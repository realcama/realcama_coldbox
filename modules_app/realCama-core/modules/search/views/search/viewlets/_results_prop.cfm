<cfoutput>
<div class="container-border container-spacer">
	<!--- Results\Pagination Bar --->
	#renderView('search/views/search/viewlets/_pagination_bar')#
	<form action="#event.buildLink('search/goToParcel')#" method="post" id="fmSearchResults" name="fmSearchResults">
	<input type="hidden" name="parcelList" id="parcelList" value="#rc.pgParcelList#">
	<cfif rc.bMultiSelect eq true and prc.getSearchResults.recordcount gt 10>
		<div class="text-right col-xs-12"><button name="sbtBtn" id="sbtBtn">Submit</button></div>
	</cfif>
	<cfif rc.bMultiSelect>
		<table class="table table-cols">
	<cfelse>
		<table id="search-results" class="table table-cols">
	</cfif>
	<thead>
		<tr>
		<td></td>
		<td class="formatted-data-label">Owner Name(s)</td>
		<td class="formatted-data-label" width="220">Parcel Number</td>
		<td class="formatted-data-label" width="60">Tax Year</td>
		<td class="formatted-data-label" width="62">Use Code</td>
		<td class="formatted-data-label" width="76">Tax District</td>
		<td class="formatted-data-label" width="26">AG?</td>
		<td class="formatted-data-label" width="90">Exemption(s)</td>
		<td class="formatted-data-label" width="144">Situs Address</td>
		</tr>
	</thead>
	<tbody>
		<cfset i = 1 />
		<cfloop query="prc.getSearchResults" startrow="#rc.start_row#"  maxrows="#rc.page_size#">
		<!--- The multiselect can be enabled\disabled by button in the _pagination_bar.cfm template --->
		<cfif rc.bMultiSelect>
			<tr>
				<td><input type="checkbox" name="parcelNum" id="parcelNum#i#" value="#TRIM(prc.getSearchResults.taxYear)#|#TRIM(prc.getSearchResults.parcelID)#" onClick="saveCheckMark('#i#');"<cfif listfind(rc.pgParcelList, '#TRIM(prc.getSearchResults.taxYear)#|#TRIM(prc.getSearchResults.parcelID)#', ',')> checked</cfif>></td>
		<cfelse>
			<tr onclick="goToPage('#TRIM(prc.getSearchResults.parcelID)#','#TRIM(prc.getSearchResults.taxYear)#');">
				<td></td>
		</cfif>
				<td class="formatted-data-values">#prc.getSearchResults.OwnerName#</td> <!--- <a href="#event.buildLink('parcel/index')#/taxYear/#TRIM(prc.getSearchResults.taxYear)#/ParcelID/#TRIM(prc.getSearchResults.parcelID)#">#prc.getSearchResults.OwnerName#</a> --->
				<td class="formatted-data-values"> #prc.getSearchResults.ParcelDisplayID#</td> <!--- Parcel Number --->
				<td class="formatted-data-values text-right">#prc.getSearchResults.taxYear#</td> <!--- Tax Year --->
				<td class="formatted-data-values"><span data-toggle="tooltip" data-placement="top" title="#prc.getSearchResults.useCodeDescription#">#prc.getSearchResults.useCode#</span></td> <!--- Use Code --->
				<td class="formatted-data-values"><span data-toggle="tooltip" data-placement="top" title="#prc.getSearchResults.taxDistrictCodeDescription#">#formatTaxDistrictCode(prc.getSearchResults.taxdistrictcode)#</span></td> <!--- Tax District --->
				<td class="formatted-data-values">#Left(YesNoFormat(prc.getSearchResults.ag),1)#</td> <!--- AG --->
				<td class="formatted-data-values">#prc.getSearchResults.EXEMPTIONS#</td> <!--- Exemption(s) --->
				<td class="formatted-data-values">#prc.getSearchResults.SitusHousePrefix# #prc.getSearchResults.SitusHouseNumber# #prc.getSearchResults.SitusStreetName# #prc.getSearchResults.SitusHouseSuffix# #prc.getSearchResults.SitusStreetType# #prc.getSearchResults.SitusStreetDirection#</td> 	<!--- Situs Address --->
			</tr>
			<cfset i = i +1 />
		</cfloop>
	</tbody>
	<cfif rc.bMultiSelect eq true and prc.getSearchResults.recordcount>
		<tfoot>
		<tr>
			<td colspan="9">
				<div class="text-right col-xs-12"><button name="sbtBtn" id="sbtBtn">Submit</button></div>
			</td>
		</tr>
		</tfoot>
	</cfif>
	</table>



	</form>

	<script type="text/javascript">
		function goToPage(ParcelID, taxYear) {
			strURL = "#event.buildLink('parcel/index')#/taxYear/" + taxYear + "/ParcelID/" + ParcelID;
			location.href = strURL;
		}
	</script>
	#addAsset('/includes/js/search.js')#
</cfoutput>
