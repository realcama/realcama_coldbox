<cfoutput>
<div class="container-border container-spacer">
	<!--- Results\Pagination Bar --->
	#renderView('search/views/search/viewlets/_pagination_bar')#
	<form action="#event.buildLink('search/goToTangible')#" method="post" id="fmSearchResults" name="fmSearchResults">
	<input type="hidden" name="parcelList" id="parcelList" value="#rc.pgParcelList#">
	<cfif rc.bMultiSelect eq true and prc.getSearchResults.recordcount gt 10>
		<div class="text-right col-xs-12"><button name="sbtBtn" id="sbtBtn">Submit</button></div>
	</cfif>
	<cfif rc.bMultiSelect>
		<table class="table table-cols">
	<cfelse>
		<table id="search-results" class="table table-cols">
	</cfif>
	<thead>
		<td></td>
		<td class="formatted-data-label">Account Number</td>
		<td class="formatted-data-label">Owner Name</td>
		<td class="formatted-data-label">Business Name</td>
		<td class="formatted-data-label">Tax Year</td>
		<td class="formatted-data-label">Tax District</td>
		<td class="formatted-data-label">Location</td>
	</thead>

	<tbody>
		<cfset i = 1 />
		<cfoutput query="prc.getSearchResults" startrow="#rc.start_row#" maxrows="#rc.page_size#">
		<!--- The multiselect can be enabled\disabled by button in the _pagination_bar.cfm template --->
		<cfif rc.bMultiSelect>
			<tr>
				<td><input type="checkbox" name="parcelNum" id="parcelNum#i#" value="#TRIM(prc.getSearchResults.taxYear)#|#TRIM(prc.getSearchResults.alternateID)#|#TRIM(prc.getSearchResults.accountNumber)#" onClick="saveCheckMark('#i#');"<cfif listfind(rc.pgParcelList, '#TRIM(prc.getSearchResults.taxYear)#|#TRIM(prc.getSearchResults.alternateID)#|#TRIM(prc.getSearchResults.accountNumber)#', ',')> checked</cfif>></td>
		<cfelse>
			<tr onclick="goToPageTPP('#TRIM(prc.getSearchResults.accountNumber)#','#TRIM(prc.getSearchResults.taxYear)#', '#TRIM(prc.getSearchResults.alternateID)#');">
				<td></td>
		</cfif>
				<td class="formatted-data-values">#formatTPPNumber(TRIM(prc.getSearchResults.accountNumber))#</td> <!--- <a href="#event.buildLink('tangible/index/')#tpp/#TRIM(prc.getSearchResults.accountNumber)#/taxYear/#TRIM(prc.getSearchResults.taxYear)#/ParcelID/#TRIM(prc.getSearchResults.alternateID)#">#formatTPPNumber(TRIM(prc.getSearchResults.accountNumber))#</a> --->
				<td class="formatted-data-values">#TRIM(prc.getSearchResults.ownerName)#</td> 		<!--- ppname Owner Name --->
				<td class="formatted-data-values">#TRIM(prc.getSearchResults.businessName)#</td> 	<!--- ppaltnam Business Name --->
				<td class="formatted-data-values">#TRIM(prc.getSearchResults.taxYear)#</td> 		<!--- ppyear Tax Year --->
				<td class="formatted-data-values">#formatTaxDistrictCode(prc.getSearchResults.taxDistrictCode)#</td>	<!--- pptxdt Tax District --->
				<td class="formatted-data-values">#TRIM(prc.getSearchResults.locationCity)#</td> 		<!--- pplocty Location --->
			</tr>
			<cfset i = i +1 />
		</cfoutput>
	</tbody>
	<cfif rc.bMultiSelect eq true and prc.getSearchResults.recordcount>
		<tfoot>
			<tr>
				<td colspan="7"><div class="text-right col-xs-12"><button name="sbtBtn" id="sbtBtn">Submit</button></div></td>
			</tr>
		</tfoot>
	</cfif>
	</table>
	</form>

	<script type="text/javascript">
		function goToPageTPP(AccountNumber, taxYear, parcelID) {
			strURL = "#event.buildLink('tangible/index/tpp/')#" + AccountNumber + "/taxYear/" + taxYear + "/parcelID/" + parcelID;
			location.href = strURL;
		}
	</script>
	#addAsset('/includes/js/search.js')#
</cfoutput>