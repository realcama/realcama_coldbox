<cfsetting showdebugoutput="No">
<cfoutput>
$(".subdivision").html("");
$(".neighborhood").html("");
$(".taxdistrict").html("");

<cfsavecontent variable="subdivision"><select name='subdivisionCode' id='subdivisionCode' size='1' class='form-control two-items-on-row-longerCode select2-2piece-new-larger doNotLeaveEmpty'><option value='' data-val='' data-desc='ALL'>ALL</option><cfloop query="prc.getSubdivisions"><option value='#prc.getSubdivisions.SubdivisionCode#' data-val='#prc.getSubdivisions.SubdivisionCode#' data-desc='#prc.getSubdivisions.SubDivisionCodeDescription#'>#prc.getSubdivisions.SubdivisionCode#: #prc.getSubdivisions.SubDivisionCodeDescription#</option></cfloop></select></cfsavecontent>
<cfsavecontent variable="neighborhood"><select name='neighborhoodCode' id='neighborhoodCode' size='1' class='form-control two-items-on-row-longerCode select2-2piece-new-larger doNotLeaveEmpty'><option value='' data-val='' data-desc='ALL'>ALL</option><cfloop query="prc.getNeighborhoods"><option value='#prc.getNeighborhoods.nbhdcd#' data-val='#prc.getNeighborhoods.nbhdcd#' data-desc='#prc.getNeighborhoods.nbhdds#&nbsp;'>#prc.getNeighborhoods.nbhdcd#: #prc.getNeighborhoods.nbhdds#&nbsp;</option></cfloop></select></cfsavecontent>
<cfsavecontent variable="taxdistrict"><select name='taxDistrict' id='taxDistrict' size='1' class='form-control two-items-on-row select2-2piece-new-larger doNotLeaveEmpty'><option value='' data-val='' data-desc='ALL'>ALL</option><cfloop query="prc.getTaxDistricts"><CFIF LEN(TRIM(prc.getTaxDistricts.txdtds)) NEQ 0><CFSET strText = prc.getTaxDistricts.txdtds><CFELSEIF LEN(TRIM(prc.getTaxDistricts.txdtcity)) NEQ 0><CFSET strText = prc.getTaxDistricts.txdtcity><CFELSE><CFSET strText = "&nbsp;"></CFIF><option value='#prc.getTaxDistricts.txdtcd#' data-val='#prc.getTaxDistricts.txdtcd#' data-desc='#strText#'>#prc.getTaxDistricts.txdtcd#: #strText#</option></cfloop></select></cfsavecontent>

$(".subdivision").html("#subdivision#");
$(".neighborhood").html("#neighborhood#");
$(".taxdistrict").html("#taxdistrict#");
</cfoutput>