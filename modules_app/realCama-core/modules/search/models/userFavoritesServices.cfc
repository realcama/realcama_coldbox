/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="userFavoritesDAO" 	inject=model;
	property name="persistentDAO"		inject=model;
	property name="log"					inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	userFavoritesServices function init(){

		return this;
	}


	/**
     * getSearchSavedSearches method for getting the saved searches for this user
     * @userID.hint The selected User ID that we are getting the list of saved searches for
     * @currentTab.hint The Current tab that is being viewed. Either Parcel or TPP
     */
	function getSearchSavedSearches( required string userID, required number currentTab ) {

		return userFavoritesDAO.getSearchSavedSearches(arguments.userID, arguments.currentTab);
	}


	/**
     * getSearchFavorites method for getting the saved favorites for this user
     * @userID.hint The selected User ID that we are getting the list of saved favorites for
     * @currentTab.hint The Current tab that is being viewed. Either Parcel or TPP
     */
	function getSearchFavorites( required string userID, required number currentTab ) {

		return userFavoritesDAO.getSearchFavorites(userID = arguments.userID, currentTab = arguments.currentTab);
	}



	function setSearchFavorites( userID, event, rc, prc ) {


	}


	/**
     * setSaveFavorite method for deleting a saved favorite for this user
     *			This function is called from the Parcel page Favorite Icon
     * @userID.hint The selected User ID that we are saving this Favorite for
     * @taxYear.hint The selected Tax Year of the Parcel that is being saved as a favorite for this user
     * @parcelID.hint The selected Parcel ID that is being saved as a favorite for this user
     * @tpp.hint The selected TPP value of the Parcel that is being saved as a favorite for this user
     */
	function setSaveFavorite(
		required string userID,
		required string taxYear,
		required string parcelID,
		required string tpp) {

		var result = {};

		if ( arguments.tpp != "" ) {
			arguments.tpp = replaceNoCase(arguments.tpp, "-", "");

			var stQueryResult = userFavoritesDAO.setSaveFavorite(
														userID = arguments.userID,
														taxYear = arguments.taxYear,
														parcelID = '',
														tpp = arguments.tpp );
		} else {
			var stQueryResult = userFavoritesDAO.setSaveFavorite(
														userID = arguments.userID,
														taxYear = arguments.taxYear,
														parcelID = arguments.parcelID,
														tpp = arguments.tpp );
		}

		if (stQueryResult.recordcount != 1) {
			result.status=404;
			result.message ="Failure";
		} else {
			result.status=200;
			result.message ="Success";

			result.stFavoriteMarkerID = persistentDAO.getGetFavoriteMarkerStatus(
																userID = arguments.userID,
																taxYear = arguments.taxYear,
																parcelID = arguments.parcelID,
																accountNumber = arguments.tpp );
		};
		
		return result;
	}


	/**
     * setDeleteFavorite method for deleting a saved favorite for this user
     *			This function is called from the Parcel page Favorite Icon
     * @userID.hint The selected User ID that we are deleting this Favorite for
     * @taxYear.hint The selected Tax Year of the Parcel that is being deleting as a favorite for this user
     * @parcelID.hint The selected Parcel ID that is being deleting as a favorite for this user
     * @tpp.hint The selected TPP value of the Parcel that is being deleting as a favorite for this user
     * @favoriteID.hint The ID value from DB.
     */
	function setDeleteFavorite(	required string userID, required string favoriteID ) {
		/*required string userID,
		required string taxYear,
		required string parcelID,
		required string tpp*/

		var result = {};

		var stQueryResult = userFavoritesDAO.deleteSavedFavorite( userID = arguments.userID, favoriteToNuke = arguments.favoriteID );

			result.status = 200;
			result.message ="Success";
			result.stFavoriteMarkerID.favoriteID = 0;

		/*if ( stQueryResult.recordcount != 1 ) {
			result.status = 404;
			result.message ="Failure";
		} else {
			result.status = 200;
			result.message ="Success";
			var userID = arguments.formData.userID;

			result.stFavoriteMarkerID = persistentDAO.getGetFavoriteMarkerStatus(
																userID = arguments.userID,
																taxYear = arguments.taxYear,
																parcelID = arguments.parcelID,
																accountNumber = arguments.tpp );
		};*/

		return result;
	}



	/**
     * deleteSavedSearch method for deleting a saved search for this user
     * @userID.hint The selected User ID that we are deleting this Saved Favorite for
     * @searchToNuke.hint The selected saved search to delete for this user
     */
	function deleteSavedSearch( required number userID, required number searchToNuke ) {

		userFavoritesDAO.deleteSavedSearch(  userID = arguments.userID, searchToNuke = arguments.searchToNuke );

		return;
	}


	/**
     * deleteSavedFavorite method for deleting a saved favorite for this user
     * @userID.hint The selected User ID that we are deleting this Saved Favorite for
     * @favoriteToNuke.hint The selected saved favorite to delete for this user
     */
	function deleteSavedFavorite( required number userID, favoriteToNuke ) {

		userFavoritesDAO.deleteSavedFavorite( userID = arguments.userID, favoriteToNuke = arguments.favoriteToNuke );

		return;
	}

}