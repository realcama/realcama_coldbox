/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";

	/**
	 * Constructor
	 */
	searchDAO function init(){

		return this;
	}



	function getSearchPropertyResults(required struct searchCriteriaRequest){

		param name="searchCriteriaRequest.exemption" default="";

		var q = new Query(datasource="#dsn.name#",	sql="call p_advancedSearch (" &
							":buildingPermit, :exemption, :township, :section, :range, :subdivision, :block," &
							":lot,:mapnumber, :neighborhoodCode, :ownerName, :parcelNumber, :number, :street," &
							":situs_mode, :city, :zipcode, :subdivisionCode, :taxDistrict, :year, :UseCode, :sort_order)"
						);
			q.setAttributes(cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#");
			q.addParam(name="buildingPermit",	value=arguments.searchCriteriaRequest.buildingPermit, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.buildingPermit)) ? false : true);
			q.addParam(name="exemption",		value=arguments.searchCriteriaRequest.exemption, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.exemption)) ? false : true);
			q.addParam(name="township",			value=arguments.searchCriteriaRequest.township, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.township)) ? false : true);
			q.addParam(name="section",			value=arguments.searchCriteriaRequest.section, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.section)) ? false : true);
			q.addParam(name="range",			value=arguments.searchCriteriaRequest.range, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.range)) ? false : true);
			q.addParam(name="subdivision",		value=arguments.searchCriteriaRequest.subdivision, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.subdivision)) ? false : true);
			q.addParam(name="block",			value=arguments.searchCriteriaRequest.block, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.block)) ? false : true);
			q.addParam(name="lot",				value=arguments.searchCriteriaRequest.lot, 				cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.lot)) ? false : true);
			q.addParam(name="mapnumber",		value=arguments.searchCriteriaRequest.mapnumber, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.mapnumber)) ? false : true);
			q.addParam(name="neighborhoodCode",	value=arguments.searchCriteriaRequest.neighborhoodCode, cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.neighborhoodCode)) ? false : true);
			q.addParam(name="ownerName",		value=arguments.searchCriteriaRequest.ownerName, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.ownerName)) ? false : true);
			q.addParam(name="parcelNumber",		value=arguments.searchCriteriaRequest.parcelNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.parcelNumber)) ? false : true);
			q.addParam(name="number",			value=arguments.searchCriteriaRequest.number, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.number)) ? false : true);
			q.addParam(name="street",			value=arguments.searchCriteriaRequest.street, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.street)) ? false : true);
			q.addParam(name="situs_mode",		value="", 												cfsqltype="cf_sql_varchar", 	null=true); //len(trim(arguments.searchCriteriaRequest.situs_mode)) ? false : true
			q.addParam(name="city",				value=arguments.searchCriteriaRequest.city, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.city)) ? false : true);
			q.addParam(name="zipcode",			value=arguments.searchCriteriaRequest.zipcode, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.zipcode)) ? false : true);
			q.addParam(name="subdivisionCode",	value=arguments.searchCriteriaRequest.subdivisionCode, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.subdivisionCode)) ? false : true);
			q.addParam(name="taxDistrict",		value=arguments.searchCriteriaRequest.taxDistrict, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.taxDistrict)) ? false : true);
			q.addParam(name="year",				value=arguments.searchCriteriaRequest.year, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.year)) ? false : true);
			q.addParam(name="UseCode",			value=arguments.searchCriteriaRequest.UseCode, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.UseCode)) ? false : true);
			q.addParam(name="sort_order",		value=arguments.searchCriteriaRequest.sort_order, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.sort_order)) ? false : true);

		return q.execute().getResult();
	}


	function getSearchTPPResults(struct searchCriteriaRequest) {

		var q = new Query(datasource="#dsn.name#",	sql="call p_TPPSearch (" &
								":p_ownerName, :p_accountNumber, :p_alternateID, :p_businessName,
								:p_locationHouse, :p_locationStreet, :p_locationType, :p_locationUnit,
								:p_locationCity, :p_locationZip, :p_taxDistrictCode, :p_TIN, :p_taxYear, :p_sortOrder)"
							);

			q.addParam(name="p_ownerName",			value=arguments.searchCriteriaRequest.ownerName2, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.ownerName2)) ? false : true);
			q.addParam(name="p_accountNumber",		value=arguments.searchCriteriaRequest.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.accountNumber)) ? false : true);
			q.addParam(name="p_alternateID",		value=arguments.searchCriteriaRequest.parcelNumber2, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.parcelNumber2)) ? false : true);
			q.addParam(name="p_businessName",		value=arguments.searchCriteriaRequest.businessName, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.businessName)) ? false : true);
			q.addParam(name="p_locationHouse",		value=arguments.searchCriteriaRequest.number, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.number)) ? false : true);
			q.addParam(name="p_locationStreet",		value=arguments.searchCriteriaRequest.street, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.street)) ? false : true);
			q.addParam(name="p_locationType",		value=arguments.searchCriteriaRequest.type, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.type)) ? false : true);
			q.addParam(name="p_locationUnit",		value=arguments.searchCriteriaRequest.post, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.post)) ? false : true);
			q.addParam(name="p_locationCity",		value=arguments.searchCriteriaRequest.city, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.city)) ? false : true);
			q.addParam(name="p_locationZip",		value=arguments.searchCriteriaRequest.zipcode, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.zipcode)) ? false : true);

			q.addParam(name="p_taxDistrictCode",	value=arguments.searchCriteriaRequest.taxDistrict2, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.taxDistrict2)) ? false : true);
			q.addParam(name="p_TIN",				value=arguments.searchCriteriaRequest.einssn, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.einssn)) ? false : true);
			q.addParam(name="p_taxYear",			value=arguments.searchCriteriaRequest.year2, 			cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchCriteriaRequest.year2)) ? false : true);
			q.addParam(name="p_sortOrder",			value="", 												cfsqltype="cf_sql_varchar",		null=true);

		return q.execute().getResult();

	}

}