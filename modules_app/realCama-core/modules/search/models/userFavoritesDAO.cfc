/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" inject="coldbox:datasource:ws_realcama_nassau";
	property name="log"	inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	userFavoritesDAO function init(){

		return this;
	}

	/**
     * getSearchSavedSearches method for getting the list of Saved Searches for this user
     * @userID.hint The User ID of the user that we are getting the list of Saved Searches for
     * @currentTab.hint The Current tab that is being viewed. Either Parcel or TPP
     */
	query function getSearchSavedSearches( required string userID, required number currentTab ) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetSaveSearches (:userID, :currentTab)");
			q.addParam(cfsqltype="CF_SQL_INTEGER", name="userID", value="#arguments.userID#", null="No");
			q.addParam(cfsqltype="CF_SQL_VARCHAR", name="currentTab", value="#arguments.currentTab#", null="No");

		return q.execute().getResult();
	}

	/**
     * getSearchFavorites method for getting the List of Saved Favorites for this user
     * @userID.hint The User ID of the user that we are getting the list of Saved Favorites for
     * @currentTab.hint The Current tab that is being viewed. Either Parcel or TPP
     */
	query function getSearchFavorites( required string userID, required number currentTab ){
		var q = new Query(datasource="#dsn.name#",sql="call p_GetFavorites (:userID, :currentTab)");
			q.addParam(name="userID",		value=userID,		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.userID)) ? false : true);
			q.addParam(name="currentTab",	value=currentTab,	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.currentTab)) ? false : true);

		return q.execute().getResult();
	}


	/**
     * getSearchSavedCriteria method for getting the saved Search Criteria for this user
     * @userID.hint The User ID of the user that we are getting the saved Search Criteria for
     * @searchID.hint The saved Search Criteria ID that the user would like to retrive
     */
	query function getSearchSavedCriteria( required number userID, required number searchID ) {
		var q = new Query(datasource="#dsn.name#",sql="call p_GetUserSavedSearch (:userID, :searchID)");
			q.addParam(name="userID",	value=arguments.userID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.userID)) ? false : true);
			q.addParam(name="searchID",	value=arguments.searchID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.searchID)) ? false : true);

		return q.execute().getResult();
	}



	/**
     * setSearchSavedCriteria method for Saving the Search Criteria for this user
     * @formData.hint Data from all of the different search fields that the user could enter on the search page
     */
	function setSearchSavedCriteria(required struct formData) {

		var q = new Query(datasource="#dsn.name#",sql="call p_SaveSearch (
			:saveSearchName, :userID, :buildingPermit, :exemption, :section, :township, :range, :subdivision, :block, :lot,
			:mapnumber, :neighborhoodCode, :ownerName, :parcelNumber, :number, :street, :type, :city, :zipcode, :subdivisionCode,
			:UseCode, :year, :ownerName2, :accountNumber, :businessName, :einssn, :location, :parcelNumber2, :taxDistrict,
				:taxDistrict2, :year2, :sort_order, :currentTab
		)");

			q.addParam(cfsqltype="CF_SQL_VARCHAR", name="saveSearchName", 	value="#arguments.formData.saveSearchName#", 		null=len(trim(arguments.formData.saveSearchName)) ? false : true);
			q.addParam(cfsqltype="CF_SQL_INTEGER", name="userID", 			value="#arguments.formData.userID#", 				null=len(trim(arguments.formData.userID)) ? false : true);

			if(arguments.formData.currentTab EQ 0) {
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="buildingPermit", 	value="#arguments.formData.buildingPermit#", 	null=len(trim(arguments.formData.buildingPermit)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="exemption", 		null=true);
				/* q.addParam(cfsqltype="CF_SQL_VARCHAR", name="exemption", 		value="#arguments.formData.exemption#",		null=len(trim(arguments.formData.exemption)) ? false : true); */
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="section", 			value="#arguments.formData.section#", 			null=len(trim(arguments.formData.section)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="township", 		value="#arguments.formData.township#", 			null=len(trim(arguments.formData.township)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="range", 			value="#arguments.formData.range#", 			null=len(trim(arguments.formData.range)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="subdivision", 		value="#arguments.formData.subdivision#", 		null=len(trim(arguments.formData.subdivision)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="block", 			value="#arguments.formData.block#", 			null=len(trim(arguments.formData.block)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="lot", 				value="#arguments.formData.lot#", 				null=len(trim(arguments.formData.lot)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="mapnumber", 		value="#arguments.formData.mapnumber#", 		null=len(trim(arguments.formData.mapnumber)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="neighborhoodCode", value="#arguments.formData.neighborhoodCode#", 	null=len(trim(arguments.formData.neighborhoodCode)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="ownerName", 		value="#arguments.formData.ownerName#", 		null=len(trim(arguments.formData.ownerName)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="parcelNumber", 	value="#arguments.formData.parcelNumber#", 		null=len(trim(arguments.formData.parcelNumber)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="number", 			value="#arguments.formData.number#", 			null=len(trim(arguments.formData.number)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="street", 			value="#arguments.formData.street#", 			null=len(trim(arguments.formData.street)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="type", 			value="#arguments.formData.type#", 				null=len(trim(arguments.formData.type)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="city", 			value="#arguments.formData.city#", 				null=len(trim(arguments.formData.city)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="zipcode", 			value="#arguments.formData.zipcode#", 			null=len(trim(arguments.formData.zipcode)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="subdivisionCode", 	value="#arguments.formData.subdivisionCode#", 	null=len(trim(arguments.formData.subdivisionCode)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="UseCode", 			value="#arguments.formData.UseCode#", 			null=len(trim(arguments.formData.UseCode)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="year", 			value="#arguments.formData.year#", 				null=len(trim(arguments.formData.year)) ? false : true);

				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="ownerName2", 		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="accountNumber", 	null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="businessName", 	null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="einssn", 			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="location", 		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="parcelNumber2", 	null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="taxDistrict", 		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="taxDistrict2", 	null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="year2", 			null=true);

			} else {
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="buildingPermit", 	null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="exemption", 		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="section", 			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="township",  		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="range", 			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="subdivision", 		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="block", 			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="lot", 				null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="mapnumber", 		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="neighborhoodCode", null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="ownerName", 		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="parcelNumber", 	null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="number", 			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="street", 			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="type",  			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="city", 			null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="zipcode",  		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="subdivisionCode", 	null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="UseCode",  		null=true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="year", 			null=true);

				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="ownerName2", 		value="#arguments.formData.ownerName2#", 		null=len(trim(arguments.formData.ownerName2)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="ownerName2", 		value="#arguments.formData.ownerName2#", 		null=len(trim(arguments.formData.ownerName2)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="accountNumber", 	value="#arguments.formData.accountNumber#", 	null=len(trim(arguments.formData.accountNumber)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="businessName", 	value="#arguments.formData.businessName#", 		null=len(trim(arguments.formData.businessName)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="einssn", 			value="#arguments.formData.einssn#", 			null=len(trim(arguments.formData.einssn)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="location", 		value="#arguments.formData.location#", 			null=len(trim(arguments.formData.location)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="parcelNumber2", 	value="#arguments.formData.parcelNumber2#", 	null=len(trim(arguments.formData.parcelNumber2)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="taxDistrict", 		value="#arguments.formData.taxDistrict#", 		null=len(trim(arguments.formData.taxDistrict)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="taxDistrict2", 	value="#arguments.formData.taxDistrict2#", 		null=len(trim(arguments.formData.taxDistrict2)) ? false : true);
				q.addParam(cfsqltype="CF_SQL_VARCHAR", name="year2", 			value="#arguments.formData.year2#", 			null=len(trim(arguments.formData.year2)) ? false : true);
			}
			q.addParam(cfsqltype="CF_SQL_VARCHAR", name="sort_order", 		value="#arguments.formData.sort_order#", 			null=len(trim(arguments.formData.sort_order)) ? false : true);
			q.addParam(cfsqltype="CF_SQL_INTEGER", name="currentTab", 		value="#arguments.formData.currentTab#", 			null=len(trim(arguments.formData.currentTab)) ? false : true);

		return q.execute().getPrefix();
	}


	/**
     * setSaveFavorite method for saving a saved favorite for this user
     * @userID.hint The User ID of the user that we are saving this Favorite for
     * @taxYear.hint The selected Tax Year of the Parcel that is being saved as a favorite for this user
     * @parcelID.hint The selected Parcel ID that is being saved as a favorite for this user
     * @tpp.hint The selected TPP value of the Parcel that is being saved as a favorite for this user
     */
	function setSaveFavorite(
			required string userID,
			required string taxYear,
			required string parcelID,
			required string tpp) {

		var q = new Query(datasource="#dsn.name#",sql="call p_SaveFavorite (:userID, :taxYear, :parcelID, :tpp)");
			q.addParam(name="userID",	value="#arguments.userID#",		cfsqltype="cf_sql_integer", null=len(trim(arguments.userID)) ? false : true);
			q.addParam(name="taxYear",	value="#arguments.taxYear#",	cfsqltype="cf_sql_varchar", null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="parcelID",	value="#arguments.parcelID#",	cfsqltype="cf_sql_varchar", null=len(trim(arguments.parcelID)) ? false : true);
			q.addParam(name="tpp",		value="#arguments.tpp#",		cfsqltype="cf_sql_varchar");

		return q.execute().getPrefix();
	}


	/**
     * deleteFavorite method for deleting a saved favorite for this user
     * @userID.hint The User ID of the user that we are deleting this Saved Favorite for
     * @favoriteToNuke.hint The selected favorite ID that is being deleted from the Saved favorite list for this user
     */
	function deleteSavedFavorite( required number userID, required number favoriteToNuke ) {

		var q1 = new Query(datasource="#dsn.name#",	sql="call p_deleteFavorite ( :userID, :favoriteID )");
			q1.addParam(name="userID",		value=arguments.userID, 			cfsqltype="cf_sql_integer", 	null=len(trim(arguments.userID)) ? false : true);
			q1.addParam(name="favoriteID",	value=arguments.favoriteToNuke, 	cfsqltype="cf_sql_integer", 	null=len(trim(arguments.favoriteToNuke)) ? false : true);

		//var qryLookup = q1.execute().getResult();

		return q1.execute().getResult();
	}


	/**
     * deleteSavedSearch method for deleting a Saved Search for this user
     * @userID.hint The User ID of the user that we are deleting this Saved Search for
     * @searchToNuke.hint The selected favorite ID that is being deleted from the Saved Search list for this user
     */
	function deleteSavedSearch ( required number userID, required number searchToNuke) {

		var q = new Query(datasource="#dsn.name#",	sql="call p_DeleteSavedSearch ( :userID, :searchID )");
			q.addParam(name="userID",		value=arguments.userID, 		cfsqltype="cf_sql_integer", 	null=len(trim(arguments.userID)) ? false : true);
			q.addParam(name="searchID",		value=arguments.searchToNuke, 	cfsqltype="cf_sql_integer", 	null=len(trim(arguments.searchToNuke)) ? false : true);

		q.execute().getResult();

		return;
	}


}