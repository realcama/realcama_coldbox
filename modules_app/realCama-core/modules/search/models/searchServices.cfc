/**
* Queries used to populate the common Drop down choices used throughout the site
*/
component accessors="true"{

	// Properties
	property name="searchDAO" 				inject=model;
	property name="dropdownsDAO" 			inject=model;
	property name="userFavoritesDAO" 		inject=model;

	/**
	 * Constructor
	 */
	function init(){

		return this;
	}


	/**
     * getSearchDropDowns method for retrieveing the drop down values for the Search form
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     * @returns.hint Struct of the data elements needed for the search drop downs
     */
	function getSearchDropDowns( required string taxYear ){

		var results = {};

		<!----- Call function to retrieve the list of Sub divisions ----->
		results.getSubdivisions = getSubdivisions( taxYear = arguments.taxYear );

		<!----- Call function to retrieve the list of Neighborhoods ----->
		results.getNeighborhoods = getNeighborhoods( taxYear = arguments.taxYear );

		<!----- Call function to retrieve the list of Tax Districts ----->
		results.getTaxDistricts = getTaxDistricts( taxYear = arguments.taxYear );

		<!----- Call function to retrieve the Primary Use items ----->
		results.getPrimaryUse = getPrimaryUse();

		<!----- Call function to retrieve the Generic Exemptions ----->
		results.getExemptions = getGenericExemptions();

		results.error = false;
		results.message = "Search data retrieved successfully.";

		return results;
	}


	/**
     * getUserSearchData method for retrieveing the users saved search settings
     * @userID.hint The selected User ID that we are retrieving the saved data for
     * @returns.hint Struct of the data elements needed for the Users saved search favorites and queries
     */
	function getUserSearchData( required number userID ){

		var results = {};
		var intCurrentTab = 0;

		/*<!--- Property --->*/
		<!----- Call function to retrieve the saved searches for this user ----->
		results.getPropertySavedSearches = userFavoritesDAO.getSearchSavedSearches( userID = arguments.userID, currentTab = Val(intCurrentTab) );
		<!----- Call function to retrieve the saved favorites for this user ----->
		results.getPropertyFavorites = userFavoritesDAO.getSearchFavorites( userID = arguments.userID, currentTab = Val(intCurrentTab) );


		/*<!--- TPP --->*/
		intCurrentTab = 1;
		<!----- Call function to retrieve the saved searches for this user ----->
		results.getTPPSavedSearches = userFavoritesDAO.getSearchSavedSearches( userID = arguments.userID, currentTab = Val(intCurrentTab) );
		<!----- Call function to retrieve the saved favorites for this user ----->
		results.getTPPFavorites = userFavoritesDAO.getSearchFavorites( userID = arguments.userID, currentTab = Val(intCurrentTab) );


		results.error = false;
		results.message = "User Search data retrieved successfully.";

		return results;
	}


	/**
     * getSubdivisions method for Getting the List of Subdivsions for the county
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     */
	function getSubdivisions( required string taxYear ) {
		return dropdownsDAO.getSubdivisions( taxYear = arguments.taxYear );
	}

	/**
     * getNeighborhoods method for Getting the List of Neighborhoods for the county
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     */
	function getNeighborhoods( required string taxYear ) {
		return dropdownsDAO.getNeighborhoods( taxYear = arguments.taxYear );
	}

	/**
     * getPrimaryUse method for Getting the List of Primary Usages for the county
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     */
	function getPrimaryUse() {
		return dropdownsDAO.getPrimaryUse();
	}

	/**
     * getTaxDistricts method for Getting the List of Tax Districts for the county
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     */
	function getTaxDistricts( required string taxYear ) {
		return dropdownsDAO.getTaxDistricts( taxYear = arguments.taxYear );
	}


	/**
     * getGenericExemptions method for Getting the List of Generic Exemption Codes
     *
     */
	function getGenericExemptions(  ) {
		return dropdownsDAO.getGenericExemptions(  );
	}


	/***** Action Events *****/
	/**
     * getSearchPropertyResults method for Getting the Parcel Property Search Results
     * @searchCriteriaRequest.hint Request Collection data struct of all of the Search fields that the User could enter
     */
	function getSearchPropertyResults (required struct searchCriteriaRequest ) {

		return searchDAO.getSearchPropertyResults(searchCriteriaRequest = arguments.searchCriteriaRequest);
	}


	/**
     * getSearchTPPResults  method for Getting the TPP Property Search Results
     * @searchCriteriaRequest.hint Request Collection data struct of all of the Search fields that the User could enter
     */
	function getSearchTPPResults( required struct searchCriteriaRequest ) {

		if ( StructKeyExists(searchCriteriaRequest, "taxDistrict2") EQ false ) {
			searchCriteriaRequest.taxDistrict2 = "";
		}

		return searchDAO.getSearchTPPResults(searchCriteriaRequest = arguments.searchCriteriaRequest);
	}


	/**
     * loadCriteria method loads the Saved Search Details that were perviously saved for this user
     * @LoadCriteriaRequest.hint Request Collection data struct of criteria fields that the User wants to load
     */
	function loadCriteria( required number userID, required number searchID ) {

		return userFavoritesDAO.getSearchSavedCriteria( userID = arguments.userID, searchID = arguments.searchID );
	}


	/**
     * saveCriteria method for saving the criteria that the user has on the form
     * @formData.hint Request Collection data struct of Search criteria fields that the User is saving
     */
	function saveCriteria( required struct formData ) {

		var result = {};

		var stQueryResult = userFavoritesDAO.setSearchSavedCriteria( formData = arguments.formData );

		if(stQueryResult.recordcount != 1) {
			result.status=404;
			result.message ="Failure";
		} else {
			result.status=200;
			result.message ="Success";
		};

		return result;
	}

}