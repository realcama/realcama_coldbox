/**
* I am a new handler
*/
component{

	property name="commonCalls" 		inject="model";
	property name="htmlhelper" 			inject="htmlhelper@coldbox";
	property name="buildingServices" 	inject="model";
	property name="pagingService" 		inject="PagingService@cbpagination";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
						index = 'GET',
						showBuildingPopup = 'GET',
						showBuildingAddinPopup = 'GET',
						showBuildingTotalsPopup = 'GET',
						buildingSave = 'POST'
						};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){
		var paginationSettings = "";

		prc.pageTitle = "RealCAMA Building";
		var stCommonCalls = commonCalls.commonPageIncludes( rc );

		structAppend(prc, stCommonCalls);
		structAppend(prc, buildingServices.getAllBuildingData( persistentCriteria = prc.stPersistentInfo ));

		paginationSettings = pagingService.getPageStart( rc, prc.getBuldingLineInfo.RecordCount, Controller.getSetting("PagingMaxRows") );

		rc.page = paginationSettings.page;
		rc.page_size = paginationSettings.page_size;
		rc.start_row = paginationSettings.start_row;

		// Set the total number of pages
		prc.iTotalPages = Ceiling( prc.getBuldingLineInfo.RecordCount / rc.page_size );

		event.setView( "building/index" );
	}


	/**
	* showBuildingPopup
	*/
	function showBuildingPopup( event, rc, prc ){

		//writeDump(rc); //abort;

		<!----- Stored Proc to retrieve Building Features ----->
		structAppend(prc, buildingServices.getBuildingFeaturesData( rc.buildingID ));

		//writeDump(prc); abort;

		/**** MODAL WINDOW ****/
		event.noLayout();
		hideDebugger();

		event.setView( "building/viewlets/_showBuildingPopup" );
	}


	/**
	* showBuildingAddinPopup
	*/
	function showBuildingAddinPopup( event, rc, prc ){

		// Stored Proc to retrieve Building Add-Ins
		structAppend(prc, buildingServices.getAllBuildingAddinsData( rc.buildingID, rc.taxYear ));

		/**** MODAL WINDOW ****/
		event.noLayout();
		hideDebugger();

		event.setView( "building/viewlets/_showBuildingAddinPopup" );
	}


	/**
	* showBuildingTotalsPopup
	*/
	function showBuildingTotalsPopup( event, rc, prc ){

		var VendorInfo = Controller.getSetting( "vendorInfo" );
   		prc.baseYear = VendorInfo.baseDepreciationYearProperty;

		//writeDump(rc); //abort;

		<!----- Stored Proc to retrieve Building Totals for this buildingID  ----->
		structAppend(prc, buildingServices.getBuildingTotalsData( rc.buildingID ));

		//writeDump(prc); abort;

		/**** MODAL WINDOW ****/
		event.noLayout();
		hideDebugger();

		event.setView( "building/viewlets/_showBuildingTotalsPopup" );
	}


	/**
	* showBuildingTotalsPopup
	*/
	function showBuildingElementsTotals( event, rc, prc ){

		var VendorInfo = Controller.getSetting( "vendorInfo" );
   		var strMode = "POINTS";
   		var strValuationMethod = ConvertValuationMethod(rc.valuationMethod);
   		prc.baseYear = VendorInfo.baseDepreciationYearProperty;

		//writeDump(rc); abort;

		<!----- Stored Proc to retrieve Building Totals for this buildingID  ----->
		structAppend(prc, buildingServices.getBuildingElementsTotalData( rc.buildingID, strValuationMethod, prc.baseYear, strMode ));

		//writeDump(prc); abort;

		/**** MODAL WINDOW ****/
		event.noLayout();
		hideDebugger();

		event.setView( "building/viewlets/_showBuildingElementsPopup" );
	}


	/**
	* buildingDetailsSave
	*/
	function buildingSave( event, rc, prc ){

		var VendorInfo = Controller.getSetting( "vendorInfo" );

		prc.stResults = buildingServices.setBuildingDetailsInfo(
													formData = rc,
													baseYear = VendorInfo.baseDepreciationYearProperty );

		if(prc.stResults.stBuildingResults.status EQ 200) {
			setNextEvent( "building/index/taxYear/#val(prc.stResults.stBuildingResults.taxYear)#/parcelID/#trim(prc.stResults.stBuildingResults.parcelID)#/item/#val(prc.stResults.itemNumber)#/" );
		} else {
			event.noLayout();
			hideDebugger();
			writedump(prc.stResults);
			writeOutput("<P>In Update failure<P>")
			abort;
		}
	}
}
