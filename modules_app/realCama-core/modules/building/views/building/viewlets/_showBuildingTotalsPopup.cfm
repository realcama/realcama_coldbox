<cfsetting showdebugoutput="No">
<cfparam name="rc.row" default="0">
<cfoutput>

<cfif NOT IsNumeric(rc.row) or NOT Isdefined("rc.row")>
	#addAsset('/includes/js/jquery.js,/includes/js/jquery.fancybox.js')#
	<script type="text/javascript">
		parent.$.fancybox.close();
	</script>
	<cfabort>
<cfelse>
<!---
	#addAsset('includes/js/jquery-validate.bootstrap-tooltip.js')#
--->
</cfif>

<script>

	function doClose() {

		parent.$.fancybox.close();
	}

	$('[data-toggle="tooltip"]').tooltip();
</script>


<cfscript>
	//  Calculate the Total Depreciation value
	var fltTotalDepreciation = Round(Val(prc.getBuildingValueBreakdown.NormalDepreciationPct) +  Val(prc.getBuildingValueBreakdown.Tier2DepreciationPct), 3);

	strAdditionalDecpreciationTooltip = createAdditionalDepreciationRollover( prc.getBuildingValueBreakdown );
</cfscript>

<!---
<cfset strAdditionalDecpreciationTooltip = createAdditionalDepreciationRollover( prc.getBuildingValueBreakdown )>
--->

<div id="popBuildingUDF" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-building-popupUDF">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Totals for Building Line ###rc.row#</h4>
			</div>
			<div id="building-modal-adjustments" class="modal-body">
				<div id="errorContainer" class="row"></div>
				<div id="adjContainer">
					<div class="row fix-bs-row">
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Parcel Number</span>
						</div>
						<div class="col-xs-5">
							<span class="formatted-data-values">#prc.getBuildingValueBreakdown.parcelDisplayID#</span>
						</div>
						<div class="col-xs-2 rightAlign databox">
							<span class="formatted-data-label">Tax Roll Year</span>
						</div>
						<div class="col-xs-2 rightAlign">
							<span class="formatted-data-values">#prc.getBuildingValueBreakdown.taxYear#</span>
						</div>
					</div>
					<div class="row fix-bs-row">
						<div class="col-xs-6">
							<span class="formatted-data-values">Building Information</span>
							<div class="hr-spacer"></div>
						</div>

						<div class="col-xs-6">
							<span class="formatted-data-values">Rate Build Up</span>
							<div class="hr-spacer"></div>
						</div>
					</div>
					<div class="row fix-bs-row">
						<div class="col-xs-6">
							<div class="row">
								<div class="col-xs-7">
									<span class="formatted-data-label">Building Type</span><p />
								</div>

								<div class="col-xs-5 rightAlign">
									<span class="formatted-data-values">#formatBuildingModelNumber(prc.getBuildingValueBreakdown.modelNumber)#<br />#prc.getBuildingValueBreakdown.modelNumberDescription#</span><br />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="hr-spacer"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<span class="formatted-data-label">Building Use Code</span><p />
								</div>

								<div class="col-xs-6 rightAlign">
									<span class="formatted-data-values">#prc.getBuildingValueBreakdown.buildingUseCode#<br />#prc.getBuildingValueBreakdown.BuildingUseCodeDescription#</span><br />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="hr-spacer"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-7">
									<span class="formatted-data-label">Actual Area</span><br />
									<span class="formatted-data-label">Heated Area</span><br />
									<span class="formatted-data-label">Number of Rooms</span><br />
									<span class="formatted-data-label">Actual Year Built</span><br />
									<span class="formatted-data-label">Effective Year Built</span><br />
									<span class="formatted-data-label">Depreciation Table / Base Year</span><p />
								</div>

								<div class="col-xs-5 rightAlign">
									<span class=" formatted-data-values">#NumberFormat(prc.getBuildingValueBreakdown.ActualSqFt)#</span><br />
									<span class="formatted-data-values">#NumberFormat(prc.getBuildingValueBreakdown.HeatedSqFt)#</span><br />
									<span class="formatted-data-values">#prc.getBuildingValueBreakdown.NumberRooms#</span><br />
									<span class="formatted-data-values">#prc.getBuildingValueBreakdown.ActualYearBuilt#</span><br />
									<span class="formatted-data-values">#prc.getBuildingValueBreakdown.EffectiveYearBuilt#</span><br />
									<span class="formatted-data-values">#prc.getBuildingValueBreakdown.depreciationTableID# / #prc.baseYear#</span><p />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<span class="formatted-data-values">Depreciation</span>
									<div class="hr-spacer"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-7">
									<span class="formatted-data-label">Tier 1 Depreciation (Normal)</span><br />
									<span class="formatted-data-label">Tier 2 Depreciation (Additional)</span><br />
								</div>
								<div class="col-xs-5 rightAlign">
									<span class="formatted-data-values">#formatBuildingTotals2Digits(prc.getBuildingValueBreakdown.NormalDepreciationPct)#</span><br />
									<span class="formatted-data-values" data-toggle="tooltip" data-placement="top" title="#strAdditionalDecpreciationTooltip#" data-html="true">#formatBuildingTotals2Digits(prc.getBuildingValueBreakdown.Tier2DepreciationPct)#</span><br />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="hr-spacer"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-7">
									<span class="formatted-data-label">Total Depreciation</span><p />
								</div>
								<div class="col-xs-5 rightAlign">
									<span class="formatted-data-values">#formatBuildingTotals2Digits(fltTotalDepreciation)#</span><p />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<span class="formatted-data-values"><a onclick="doBuildingElementsTotals(#rc.row#,#rc.buildingID#,#rc.blItem#)" rel="tooltip" title="Click to View Building Structural Elements Totals">Structural Elements</a></span>
									<div class="hr-spacer"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-7">
									<span class="formatted-data-label">Total Points</span><br />
									<span class="formatted-data-label">Total Factor</span><p />
								</div>
								<div class="col-xs-5 rightAlign">
									<span class="formatted-data-values">#formatBuildingTotals(prc.getBuildingValueBreakdown.totalPoints)#</span><br />
									<span class="formatted-data-values">#formatBuildingTotals(prc.getBuildingValueBreakdown.totalFactor)#</span><br />
								</div>
							</div>
						</div>

						<div class="col-xs-6">
							<div class="row">
								<div class="col-xs-8">
									<span class="formatted-data-label">Base Rate</span><br />
									<span class="formatted-data-label">Rate Add-ons</span><br />
									<span class="formatted-data-label">CAMA Index</span><br />
									<span class="formatted-data-label">Effective Base Rate</span><p />

									<span class="formatted-data-label">Replacement Cost New</span><br />
									<span class="formatted-data-label">Percent Good</span><br />
									<span class="formatted-data-label">Replacement Cost New Less Depreciation</span><br />
								</div>

								<div class="col-xs-4 rightAlign">
									<span class="formatted-data-values">#formatBuildingTotals(prc.getBuildingValueBreakdown.BaseRate)#</span><br />
									<span class="formatted-data-values">#prc.getBuildingValueBreakdown.TotalRateAddOn#<br /></span>
									<span class="formatted-data-values">#formatBuildingTotals(prc.getBuildingValueBreakdown.CAMAIndex)#<br /></span>
									<span class="formatted-data-values">#formatBuildingTotals(prc.getBuildingValueBreakdown.effectiveBaseRate)#<p /></span>

									<span class="formatted-data-values">#formatMoneyValue(prc.getBuildingValueBreakdown.RCN)#<br /></span>
									<span class="formatted-data-values">#formatBuildingTotals2Digits(prc.getBuildingValueBreakdown.goodPct)#<br /></span>
									<span class="formatted-data-values">#formatMoneyValue(prc.getBuildingValueBreakdown.RCNLD)#<br /></span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="hr-spacer"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<span class="formatted-data-label">Neighborhood Code</span><p />
								</div>

								<div class="col-xs-6 rightAlign">
									<span class="formatted-data-values">
											#formatNeighborhoodCode(prc.getBuildingValueBreakdown.neighborhoodCode)#<br />#prc.getBuildingValueBreakdown.neighborhoodCodeDescription#<br />
									</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="hr-spacer"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-8">
									<span class="formatted-data-label">Neighborhood Percentage</span><p /><br />
								</div>

								<div class="col-xs-4 rightAlign">
									<span class="formatted-data-values">#formatBuildingTotals2Digits(prc.getBuildingValueBreakdown.neighborhoodPCT)#<br /></span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-8">
									<span class="modal-formatted-data-values-large">Appraised Value</span><br />
								</div>

								<div class="col-xs-4 rightAlign">
									<span class="modal-formatted-data-values-large">#formatMoneyValue(prc.getBuildingValueBreakdown.AppraisedValue)#<br /></span>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button"  class="btn btn-default"  onclick="doClose()"><i class="glyphicon glyphicon-ok-sign"></i> Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-building-popupUDF -->
</div><!-- /.modal -->
</CFOUTPUT>




