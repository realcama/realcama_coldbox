<cfsetting showdebugoutput="No">
<cfparam name="rc.row" default="0">
<cfoutput>

<cfif NOT IsNumeric(rc.row)>
	#addAsset('/includes/css/select2.css,/includes/js/select2.js,/includes/js/jquery.js,/includes/js/jquery.fancybox.js')#
	<script type="text/javascript">
		parent.$.fancybox.close();
	</script>
	<cfabort>
</cfif>

<div id="popLandUDF" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-land-popupUDF">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="cancelChanges();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Add-Ins for Item ###row#</h4>
			</div>
			<div class="modal-body">
				<div id="errorContainer" class="row"></div>
				<div id="adjContainer">
					<div id="adjHeader"><span class="adjHeaderAdj">Adjustments</span><span class="adjHeaderCode">Code</span><span class="adjHeaderDesc">Description</span><span class="adjHeaderPercentage adjHeaderSmall text-center">Contributing<br>Percentage</span><span class="adjHeaderPoints adjHeaderSmall text-center">Points</span><span class="adjHeaderLump adjHeaderSmall text-center">Lump Sum</span><span class="adjHeaderFactor adjHeaderSmall text-center">Factor</span><span class="adjHeaderRate adjHeaderSmall text-center">Rate<br>Add-On</span></div>
					<div id="adjContent"></div>
				</div>
			</div>

			<div class="modal-footer">
				<span class="col-ends"></span>
				<span class="text-center col-center">
					<button id="addRowButton" class="btn btn-primary btn-add-option-udf" type="button" onclick="addAdjRow()"><i class="glyphicon glyphicon-plus-sign"></i> Add Another Option</button>
				</span>
				<span class="col-ends">
					<button type="button"  class="btn btn-default"  onclick="cancelChanges();"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</button>
					<button type="button"  class="btn btn-default"  onclick="savePopUpChanges();"><i class="glyphicon glyphicon-ok-sign"></i> Finished</button> <!--- parent.$.fancybox.close(); --->
				</span>
			</div>
		</div>
	</div>
</div>

<cfset arrAdjustmentsList = valueList(prc.getBuildingCustomAttributeList.codeType) />

<!--- Remember that JS is 0 based, and CF is 1 based --->
<cfset jsRowNumber = row - 1>

<script type="text/javascript">
	var adjustments = '#arrAdjustmentsList#';
	var arrAdjustments = adjustments.split(',');
	var iOptionsOnModal = 0;
	var iUrlRow = #rc.row#;
	var jsRowNumber = #jsRowNumber#;
	var bl_id = #rc.buildingid#;
	var iBLItem = '#rc.blitem#';
	//var iBudAdjColumn = 1;

	function createAdjEntryRow(iRow, adjdesc, mode) {
		var strHTML = "";
		var intLoopCount = 1;

		strHTML = strHTML + "<div id='adjRow_" + iRow + "' class='udfrow'>"
		strHTML = strHTML + "<i class='glyphicon glyphicon-remove-sign' onclick='removeAdjRow(" + iRow + ")' data-placement='right' data-toggle='tooltip' data-original-title='Remove This Adjustment'></i>";

		// Create the list of the options for this Adjustment
		strHTML = strHTML + "<select name='codeType' id='codeType_" + iRow + "' data-rownumber=" + iRow + " class='form-control select2-1piece-new' onchange='changeAdjAdjustment(" + iRow +",this);changeUDFMethod(this)'>";
		strHTML = strHTML + "<option value='' data-val='' data-desc='Select...'>Select</option>";
		<cfloop query="prc.getBuildingCustomAttributeList">
			strHTML = strHTML + "<option value='#codeType#' data-val='#codeType#' data-desc='#codeType#' data-modalsortorder='" + intLoopCount + "'>#codeType#</option>";
			intLoopCount++;
		</cfloop>
		strHTML = strHTML + "	</select>";

		<cfset i = 0 />
		// Loop thru each adjustment type to create each set of code type select boxes
		<cfloop index="strAttribute" list="#arrAdjustmentsList#" delimiters=",">
			<cfset i = i + 1>
			<cfset strCurrentQuery = "prc.getBuildingCustomAttributes_" & strAttribute />

			<cfif isdefined("#strCurrentQuery#")>
				strDisplay = (adjdesc == '#strAttribute#') ? "" : " style='display:none'";
				strHTML = strHTML + "<span class='selectCodeContainer'><select name='#strAttribute#' id='#strAttribute#_" + iRow + "' data-tags='true' class='form-control select2-2piece-1hidden' " + strDisplay + " onchange='changeAdjAdjustment(" + iRow + ",this,\"#strAttribute#\")'>";
				strHTML = strHTML + "<option value='' data-val='' data-desc='Select...' data-adjdata1='' data-adjtyp=''>Select...</option>";

				<cfloop query="#strCurrentQuery#">
					<cfif len(trim(buildingCustomCodeID)) gt 0>
						strHTML = strHTML + "<option value='#val(buildingCustomCodeID)#' data-val='#val(buildingCustomCode)#' data-desc='#trim(buildingCustomCodeDescription)#' data-adjdata1='#buildingCustomCodeID#' data-adjtyp='#strAttribute#'><cfif buildingCustomCode neq "">#buildingCustomCode#: </cfif>#buildingCustomCodeDescription#</option>";
					</cfif>
				</cfloop>
				strHTML = strHTML + "</select></span>";
			</cfif>
		</cfloop>

		// sprinklers
		/*strDisplay = (adjdesc == 'Sprinkler') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select name='Sprinkler' id='Sprinkler_" + iRow + "' data-tags='true'  class='form-control select2-2piece-1hidden'" + strDisplay +" onchange='changeAdjAdjustment(" + iRow +",this);'>";
		strHTML = strHTML + "<option value='N/A' data-val='N/A' data-desc='N/A' data-adjdata1='N/A' data-adjtyp='F'>N/A</option>";
		<cfloop query="prc.getBuildingCustomAttributes_Sprinkler">
			strHTML = strHTML + "<option value='#prc.getBuildingCustomAttributes_Sprinkler.buildingCustomCodeID#' data-val='#prc.getBuildingCustomAttributes_Sprinkler.buildingCustomCode#' data-desc='#trim(prc.getBuildingCustomAttributes_Sprinkler.buildingCustomCodeDescription)#' data-adjdata1='#prc.getBuildingCustomAttributes_Sprinkler.buildingCustomCodeID#' data-adjtyp='Sprinkler'><cfif prc.getBuildingCustomAttributes_Sprinkler.buildingCustomCode neq "">#prc.getBuildingCustomAttributes_Sprinkler.buildingCustomCode#: </cfif>#prc.getBuildingCustomAttributes_Sprinkler.buildingCustomCodeDescription#</option>";
		</cfloop>
		strHTML = strHTML + "</select></span>";*/

		// User-Defined2
		strDisplay = (adjdesc == 'User-Defined2') ? "" : " style='display:none'";
		strHTML = strHTML + "<span class='selectCodeContainer'><select name='User-Defined2' id='User-Defined2_" + iRow + "' data-tags='true'  class='form-control select2-2piece-1hidden'" + strDisplay +" onchange='changeAdjAdjustment(" + iRow +",this);'>"; //select2-2piece-new
		strHTML = strHTML + "<option value='N/A' data-val='N/A' data-desc='N/A' data-adjdata1='N/A' data-adjtyp='F'>N/A</option>";
		<cfloop query="prc.getBuildingCustomAttributes_UserDefined2">
			strHTML = strHTML + "<option value='#prc.getBuildingCustomAttributes_UserDefined2.buildingCustomCodeID#' data-val='#prc.getBuildingCustomAttributes_UserDefined2.buildingCustomCode#' data-desc='#trim(prc.getBuildingCustomAttributes_UserDefined2.buildingCustomCodeDescription)#' data-adjdata1='#prc.getBuildingCustomAttributes_UserDefined2.buildingCustomCodeID#' data-adjtyp='User-Defined2'><cfif prc.getBuildingCustomAttributes_UserDefined2.buildingCustomCode neq "">#prc.getBuildingCustomAttributes_UserDefined2.buildingCustomCode#: </cfif>#prc.getBuildingCustomAttributes_UserDefined2.buildingCustomCodeDescription#</option>";
		</cfloop>
		strHTML = strHTML + "</select></span>";

		// Adjustment value 
		strHTML = strHTML + "<input name='percentageFactor' id='percentageFactor_" + iRow + "' class='form-control two-part-small numeric' type='text' disabled value='100'>%";
		strHTML = strHTML + "<input name='bLPoints' id='bLPoints_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHTML = strHTML + "<input name='bLLumpSum' id='bLLumpSum_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHTML = strHTML + "<input name='bLFactor' id='bLFactor_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHTML = strHTML + "<input name='bLRateAddOn' id='bLRateAddOn_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHTML = strHTML + "</div>";

		return strHTML;
	}
</script>

#addAsset('/includes/css/select2.css,/includes/js/select2.js,/includes/js/buildingLineAddIn.js,/includes/js/formatSelect.js')#

</cfoutput>

<script type="text/javascript">
	$( document ).ready(function() {
		buildModal();
		changeSelectsToSelect2s();
	});
</script>
