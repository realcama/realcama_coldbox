<cfoutput>
	<cfparam name="CurrentRow" default="1">
	<cfparam name="strOptions" default="">
	<cfparam name="PercentageAddOns" default="">
	<cfparam name="pointsAddOns" default="">
	<cfparam name="LumpSumAddOns" default="">
	<cfparam name="factorAddOns" default="">
	<cfparam name="RateAddOns" default="">

	<cfset PercentageAddOns = 0 />
	<table class="building-udf-table">
	<thead>
		<tr>
			<td class="adjustmentCell formatted-data-label"><span class="formatted-data-label">Add-Ins</span></td>
			<td class="codeCell formatted-data-label"></td>
			<td class="codeDescCell formatted-data-label"></td>
			<td class="text-right smallCell formatted-data-label">Contributing<br>Percentage</td>
			<td class="text-right smallCell formatted-data-label">Points</td>
			<td class="text-right smallCell formatted-data-label">Lump Sum</td>
			<td class="text-right smallCell formatted-data-label">Factor</td>
			<td class="text-right smallCell formatted-data-label">Rate<br>Add-On</td>
		</tr>
	</thead>
	<tbody id="addIn_#CurrentRow#">
		<cfif prc.getBuildingCustomAttributesAssigned.recordcount>
			<cfloop query="prc.getBuildingCustomAttributesAssigned">
				<tr>
					<td class="adjustmentCell formatted-data-values">#codeType#</td>
					<td class="codeCell formatted-data-values">#buildingCustomCode#</td>
					<td class="codeDescCell formatted-data-values"> #buildingCustomCodeDescription#</td>
					<td class="text-right smallCell formatted-data-values">#percentageFactor#%</td>
					<td class="text-right smallCell formatted-data-values">&nbsp;</td>
					<td class="text-right smallCell formatted-data-values">&nbsp;</td>
					<td class="text-right smallCell formatted-data-values">&nbsp;</td>
					<td class="text-right smallCell formatted-data-values">&nbsp;</td>
				</tr>
				<cfif PercentageAddOns EQ 0>
					<cfset PercentageAddOns = percentageFactor/100 />
				<cfelse>
					<cfset PercentageAddOns = PercentageAddOns*( percentageFactor/100 ) />
				</cfif>
			</cfloop>
		<cfelse>
			<tr><td colspan="8" class="formatted-data-values">None</td></tr>
		</cfif>
		<cfset PercentageAddOns = PercentageAddOns*100 />
	</tbody>
	<tfoot class="adjustmentFooter">
		<tr>
			<td></td>
			<td></td>
			<td class="text-right formatted-data-label">Net Adjustments</td>
			<td class="text-right smallCell formatted-data-label" id="addIn_net_percentage_#CurrentRow#">#PercentageAddOns#%</td>
			<td class="text-right smallCell formatted-data-label" id="addIn_net_points_#CurrentRow#">#NumberFormat(pointsAddOns,"0.000")#</td>
			<td class="text-right smallCell formatted-data-label" id="addIn_net_lumpsum_#CurrentRow#">#NumberFormat(LumpSumAddOns,"0.000")#</td>
			<td class="text-right smallCell formatted-data-label" id="addIn_net_factor_#CurrentRow#">#NumberFormat(factorAddOns,"0.000")#</td>
			<td class="text-right smallCell formatted-data-label" id="addIn_net_rateaddon_#CurrentRow#">#NumberFormat(RateAddOns,"0.000")#</td>
		</tr>
	</tfoot>
	</table>


</cfoutput>