<cfsetting showdebugoutput="No">
<cfparam name="rc.row" default="0">
<cfparam name="fltPoints" Default="0">
<cfparam name="fltLumpSum" Default="0">
<cfparam name="fltFactor" Default="0">
<cfparam name="fltRate" Default="0">

<cfparam name="fltElementPoints" Default="0">
<cfparam name="fltElementLumpSums" Default="0">
<cfparam name="fltElementFactor" Default="0">
<cfparam name="fltElementRate" Default="0">

<cfparam name="fltAddInsPoints" Default="0">
<cfparam name="fltAddInsLumpSums" Default="0">
<cfparam name="fltAddInsFactor" Default="0">
<cfparam name="fltAddInsRate" Default="0">

<cfoutput>

<cfif NOT IsNumeric(rc.row) or NOT Isdefined("rc.row")>
	#addAsset('/includes/js/jquery.js,/includes/js/jquery.fancybox.js')#
	<script type="text/javascript">
		parent.$.fancybox.close();
	</script>
	<cfabort>
</cfif>

<script>
	function doClose() {
		parent.$.fancybox.close();
	}
</script>

<cfsilent>
	<cfloop query="prc.qryCombinedElementsData">
		<cfswitch expression="#AdjustmentType#">
			<cfcase value="P">
				<cfif percentageFactor NEQ 100>
					<cfset fltPoints = fltPoints + (Adjustment * (".#percentageFactor#"))>
				<cfelse>
					<cfset fltPoints = fltPoints + Adjustment>
				</cfif>
			</cfcase>
			<cfcase value="F">
				<cfif Adjustment NEQ 1>
					<cfif fltFactor GT 0>
						<cfset fltFactor = fltFactor * Adjustment>
					<cfelse>
						<cfset fltFactor = Adjustment>
					</cfif>
				</cfif>
			</cfcase>
			<cfcase value="L">
				<cfset fltLumpSum = fltLumpSum + Adjustment>
			</cfcase>
			<cfcase value="R">
				<cfset fltRate = fltRate + Adjustment>
			</cfcase>
		</cfswitch>
		<BR>
	</cfloop>

	<cfloop query="prc.qryBuildingAddInsData">
		<cfswitch expression="#AdjustmentType#">
			<cfcase value="P">
				<cfset fltAddInsPoints = fltAddInsPoints + Adjustment>
			</cfcase>
			<cfcase value="F">
				<cfif Adjustment NEQ 1>
					<cfset fltAddInsFactor = fltAddInsFactor * Adjustment>
				</cfif>
			</cfcase>
			<cfcase value="L">
				<cfset fltAddInsLumpSums = fltAddInsLumpSums + Adjustment>
			</cfcase>
			<cfcase value="R">
				<cfset fltAddInsRate = fltAddInsRate + Adjustment>
			</cfcase>
		</cfswitch>
	</cfloop>
</cfsilent>

<div id="popBuildingUDF" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-building-popupUDF">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Structural Element Totals for Building Line ###rc.row#</h4>
			</div>
			<div id="building-modal-adjustments" class="modal-body">
				<div id="errorContainer" class="row"></div>
				<div id="adjContainer">
					<div id="adjHeader">
						<div class="col-xs-8 ">
							<span class="formatted-data-values"><br>Summary</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right">
							<span class="formatted-data-label">Points<br>Adjusted</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right">
							<span class="formatted-data-label">Lump Sum<br>Adjusted</span>
						</div>
						<div class="col-xs-1 text-center text-bottom">
							<span class="formatted-data-label">Factor<br>Adjusted</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right text-nowrap">
							<span class="formatted-data-label">Rate Add On<br>Adjusted</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="hr-spacer"></div>
						</div>
					</div>
					<div id="adjContent">
						<div class="row fix-bs-row">
							<div class="col-xs-8">
								<span class="formatted-data-label">Structural Elements</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltPoints)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltLumpSum)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltFactor)#</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltRate)#</span>
							</div>
						</div>
					</div>
					<div id="adjContent">
						<div class="row fix-bs-row">
							<div class="col-xs-8">
								<span class="formatted-data-label">Add-Ins</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltAddInsPoints)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltAddInsLumpSums)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltAddInsFactor)#</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltAddInsRate)#</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="hr-spacer"></div>
						</div>
					</div>
					<div id="adjContent">
						<div class="row fix-bs-row bottom30">
							<div class="col-xs-8">
								<span class="formatted-data-label">Totals</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltPoints)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltLumpSum)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltFactor)#</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltRate)#</span>
							</div>
						</div>
					</div>
				</div>

				<cfset fltPoints = 0>
				<cfset fltLumpSum = 0>
				<cfset fltFactor = 0>
				<cfset fltRate = 0>

				<div id="adjContainer">
					<div id="adjHeader">
						<div class="col-xs-3" <!---style="border: solid black 1px;" --->>
							<span class="formatted-data-values"><br>Structural Elements</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right" <!--- style="border: solid red 1px;" --->>
							<span class="formatted-data-label"><br>Code</span>
						</div>
						<div class="col-xs-2 text-left" <!--- style="border: solid black 1px;" --->>
							<span class="formatted-data-label"><br>Description</span>
						</div>
						<div class="col-xs-2 nopadding-col text-right" <!--- style="border: solid red 1px;" --->>
							<span class="formatted-data-label">Contributing<br>Percentage</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right" <!--- style="border: solid red 1px;" --->>
							<span class="formatted-data-label">Points<br>Adjusted</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right" <!--- style="border: solid black 1px;" --->>
							<span class="formatted-data-label">Lump Sum<br>Adjusted</span>
						</div>
						<div class="col-xs-1 text-center"<!---  style="border: solid red 1px;" --->>
							<span class="formatted-data-label">Factor<br>Adjusted</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right text-nowrap" <!--- style="border: solid black 1px;" --->>
							<span class="formatted-data-label">Rate Add On<br>Adjusted</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="hr-spacer"></div>
						</div>
					</div>
					<div id="adjContent">
						<cfloop query="prc.qryCombinedElementsData">

							<cfsilent>
								<cfset blnDisplayElement = TRUE>
								<cfswitch expression="#AdjustmentType#">
									<cfcase value="P">
										<cfif percentageFactor NEQ 100>
											<cfset fltPoints = (Adjustment * (".#percentageFactor#"))>
										<cfelse>
											<cfset fltPoints = Adjustment>
										</cfif>

										<cfset fltElementPoints = fltElementPoints + fltPoints>
									</cfcase>
									<cfcase value="F">
										<cfif Adjustment GT 0 AND Adjustment NEQ 1>
											<cfset fltFactor = Adjustment>
											<cfif fltElementFactor GT 0>
												<cfset fltElementFactor = fltElementFactor * fltFactor>
											<cfelse>
												<cfset fltElementFactor = fltFactor>
											</cfif>
										<cfelse>
											<cfset blnDisplayElement = FALSE>
										</cfif>
									</cfcase>
									<cfcase value="L">
										<cfset fltLumpSum = Adjustment>
										<cfset fltElementLumpSums = fltElementLumpSums + fltLumpSum>
									</cfcase>
									<cfcase value="R">
										<cfset fltRate = Adjustment>
										<cfset fltElementRate = fltElementRate + fltRate>
									</cfcase>
								</cfswitch>

								<cfif code NEQ "">
									<cfset fltCodeDisplay = code>
								<cfelse>
									<cfset fltCodeDisplay = value>
								</cfif>
							</cfsilent>

							<cfif (blnDisplayElement)>
								<div class="row fix-bs-row rightalign">
									<div class="col-xs-3">
										<span class="formatted-data-label">#codeType#</span>
									</div>
									<div class="col-xs-1 nopadding-col text-right">
										<span class="formatted-data-label">#fltCodeDisplay#</span>
									</div>
									<div class="col-xs-2">
										<span class="formatted-data-label">#codeDescription#</span>
									</div>
									<div class="col-xs-2 nopadding-col text-right">
										<span class="formatted-data-label">#percentageFactor# %</span>
									</div>
									<div class="col-xs-1 nopadding-col text-right">
										<span class="formatted-data-label">#formatBuildingTotals(fltPoints, "000")#</span>
									</div>
									<div class="col-xs-1 text-right">
										<span class="formatted-data-label">#formatMoneyValue(fltLumpSum)#</span>
									</div>
									<div class="col-xs-1 text-right">
										<span class="formatted-data-label">#formatBuildingTotals(fltFactor)#</span>
									</div>
									<div class="col-xs-1 nopadding-col text-right">
										<span class="formatted-data-label">#formatMoneyValue(fltRate)#</span>
									</div>
								</div>
							</cfif>

							<cfset fltPoints = 0>
							<cfset fltLumpSum = 0>
							<cfset fltFactor = 0>
							<cfset fltRate = 0>
						</cfloop>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="hr-spacer"></div>
						</div>
					</div>
					<div id="adjContent">
						<div class="row fix-bs-row bottom30">
							<div class="col-xs-8">
								<span class="formatted-data-label">Structural Elements Subtotals</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltElementPoints)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltElementLumpSums)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltElementFactor)#</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltElementRate)#</span>
							</div>
						</div>
					</div>
				</div>


				<div id="adjContainer">
					<div id="adjHeader">
						<div class="col-xs-3" <!---style="border: solid black 1px;" --->>
							<span class="formatted-data-values"><br>Add-Ins</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right" <!--- style="border: solid red 1px;" --->>
							<span class="formatted-data-label"><br>Code</span>
						</div>
						<div class="col-xs-2 text-left" <!--- style="border: solid black 1px;" --->>
							<span class="formatted-data-label"><br>Description</span>
						</div>
						<div class="col-xs-2 nopadding-col text-right" <!--- style="border: solid red 1px;" --->>
							<span class="formatted-data-label">Contributing<br>Percentage</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right" <!--- style="border: solid red 1px;" --->>
							<span class="formatted-data-label">Points<br>Adjusted</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right" <!--- style="border: solid black 1px;" --->>
							<span class="formatted-data-label">Lump Sum<br>Adjusted</span>
						</div>
						<div class="col-xs-1 text-center"<!---  style="border: solid red 1px;" --->>
							<span class="formatted-data-label">Factor<br>Adjusted</span>
						</div>
						<div class="col-xs-1 nopadding-col text-right text-nowrap" <!--- style="border: solid black 1px;" --->>
							<span class="formatted-data-label">Rate Add On<br>Adjusted</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="hr-spacer"></div>
						</div>
					</div>
					<div id="adjContent">
						<cfloop query="prc.qryBuildingAddInsData">
							<cfsilent>
								<cfswitch expression="#AdjustmentType#">
									<cfcase value="P">
										<cfif percentageFactor NEQ 100>
											<cfset fltPoints = (Adjustment * (".#percentageFactor#"))>
											<!--- <cfset fltPoints = (Adjustment * ".#percentageFactor#")> --->
										<cfelse>
											<cfset fltPoints = Adjustment>
										</cfif>

										<cfset fltAddInsPoints = fltAddInsPoints + fltPoints>
									</cfcase>
									<cfcase value="F">
										<cfset fltFactor = Adjustment>
										<cfset fltAddInsFactor = fltAddInsFactor + fltFactor>
									</cfcase>
									<cfcase value="L">
										<cfset fltLumpSum = Adjustment>
										<cfset fltAddInsLumpSums = fltAddInsLumpSums + fltLumpSum>
									</cfcase>
									<cfcase value="R">
										<cfset fltRate = Adjustment>
										<cfset fltAddInsRate = fltAddInsRate + fltRate>
									</cfcase>
								</cfswitch>
							</cfsilent>

							<div class="row fix-bs-row rightalign">
								<div class="col-xs-3">
									<span class="formatted-data-label">#codeType#</span>
								</div>
								<div class="col-xs-1 nopadding-col text-right">
									<span class="formatted-data-label">#code#</span>
								</div>
								<div class="col-xs-2">
									<span class="formatted-data-label">#codeDescription#</span>
								</div>
								<div class="col-xs-2 nopadding-col text-right">
									<span class="formatted-data-label">#percentageFactor# %</span>
								</div>
								<div class="col-xs-1 nopadding-col text-right">
									<span class="formatted-data-label">#formatBuildingTotals2Digits(fltPoints, "000")#</span>
								</div>
								<div class="col-xs-1 text-right">
									<span class="formatted-data-label">#formatMoneyValue(fltLumpSum)#</span>
								</div>
								<div class="col-xs-1 text-right">
									<span class="formatted-data-label">#formatBuildingTotals(fltFactor)#</span>
								</div>
								<div class="col-xs-1 nopadding-col text-right">
									<span class="formatted-data-label">#formatMoneyValue(fltRate)#</span>
								</div>
							</div>

							<cfset fltPoints = 0>
							<cfset fltLumpSum = 0>
							<cfset fltFactor = 0>
							<cfset fltRate = 0>
						</cfloop>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="hr-spacer"></div>
						</div>
					</div>
					<div id="adjContent">
						<div class="row fix-bs-row bottom30">
							<div class="col-xs-8">
								<span class="formatted-data-label">Add-Ins Subtotals</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltAddInsPoints)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltAddInsLumpSums)#</span>
							</div>
							<div class="col-xs-1 text-right">
								<span class="formatted-data-label">#formatBuildingTotals(fltAddInsFactor)#</span>
							</div>
							<div class="col-xs-1 nopadding-col text-right">
								<span class="formatted-data-label">#formatMoneyValue(fltAddInsRate)#</span>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="modal-footer">
				<button type="button"  class="btn btn-default"  onclick="doClose()"><i class="glyphicon glyphicon-ok-sign"></i> Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-building-popupUDF -->
</div><!-- /.modal -->

</CFOUTPUT>

