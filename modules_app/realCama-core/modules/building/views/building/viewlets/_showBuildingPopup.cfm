<cfsetting showdebugoutput="No">
<cfparam name="rc.row" default="0">
<cfoutput>
<cfif NOT IsNumeric(rc.row) or NOT Isdefined("rc.row")>
	#addAsset('/includes/js/jquery.js,/includes/js/jquery.fancybox.js')#
	<script type="text/javascript">
		parent.$.fancybox.close();
	</script>
	<cfabort>
</cfif>
<div id="popBuildingUDF" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-building-popupUDF">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="parent.$.fancybox.close();">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Adjustments for Item ###rc.row#</h4>
			</div>
			<div id="building-modal-adjustments" class="modal-body">
				<div id="errorContainer" class="row"></div>
				<div id="adjContainer">
					<div id="adjHeader"><span class="adjHeaderAdj">Adjustments</span><span class="adjHeaderCode">Code</span><span class="adjHeaderDesc">Description</span><span class="adjHeaderPercentage adjHeaderSmall text-center">Contributing<br>Percentage</span><span class="adjHeaderPoints adjHeaderSmall text-center">Points</span><span class="adjHeaderLump adjHeaderSmall text-center">Lump Sum</span><span class="adjHeaderFactor adjHeaderSmall text-center">Factor</span><span class="adjHeaderRate adjHeaderSmall text-center">Rate<br>Add-On</span></div>
					<!--- The body of the modal window is generated via javascript. --->
					<div id="adjContent"></div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="col-ends"></span>
				<span class="text-center col-center">
					<button id="addRowButton" class="btn btn-primary btn-add-option-udf" type="button" onclick="addAdjRow()"><i class="glyphicon glyphicon-plus-sign"></i> Add New Adjustment</button>
				</span>
				<span class="col-ends">
					<button type="button"  class="btn btn-default"  onclick="rollBackFeatures();"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</button>
					<button type="button"  class="btn btn-default"  onclick="doCalcs()"><i class="glyphicon glyphicon-ok-sign"></i> Finished</button>
				</span>
			</div>
		</div>
	</div>
</div>
<!---
The values in lstBuildingAttributes should match the list in the getAllBuildingData function in buildingServices.
The order of the values in lstBuildingAttributes and lstQueryNames must be the same. ListGetAt function is
used below to get the correct query name.
--->
<cfset lstBuildingAttributes = "ExteriorWall,Frame,Foundation,RoofStructure,RoofCover,Floor,InteriorWall,Heat,AirConditioning,Kitchen,Windows">
<cfset lstQueryNames = "prc.qryExteriorWall,prc.qryFrame,prc.qryFoundation,prc.qryRoofStructure,prc.qryRoofCover,prc.qryFloor,prc.qryInteriorWall,prc.qryHeat,prc.qryAirConditioning,prc.qryKitchen,prc.qryWindows">

<!--- Remember that JS is 0 based, and CF is 1 based --->
<script type="text/javascript">
	var jsBlItem = #rc.blitem#
	var jsRcRow = #rc.row#;
	var jsRowNumber = jsRcRow - 1;
	var iOptionsOnModal = 0;
	var arrBuildingAttributes = ["ExteriorWall","Frame","Foundation","RoofStructure","RoofCover","Floor","InteriorWall","Heat","AirConditioning","Kitchen","Windows"];

	<cfloop index="strName" list="#lstBuildingAttributes#">
		<!--- var #strName# = 0; --->
		var #strName#_display_count = 0;
		var #strName#_#rc.row#_extra = "";
		var #strName#_#rc.row#_percentageFactor = 0;
	</cfloop>


	/* Creates each row. Creates the */
	function createAdjEntryRow(iRow, adjdesc, mode) {
		var strHTML = "";
		strHTML = strHTML + "<div id='adjRow_" + iRow + "' class='udfrow'>";

		strHTML = strHTML + "	<i class='glyphicon glyphicon-remove-sign' onclick='removeAdjRow(" + iRow + ")' data-placement='right' data-toggle='tooltip' data-original-title='Remove This Adjustment'></i>";

		/* This select box is defaulted to the selected value by the buildModel function */
		strHTML = strHTML + "<select name='improve_class' id='improve_class_" + iRow + "' data-rownumber='" + iRow + "' class='form-control select2-1piece-new' onchange='changeUDFMethod(this)'>"; //changeImproveClassVal(" + iRow + ",this); select2-2piece-dropdown-small-new
		strHTML = strHTML + "	<option value='' data-val='' data-desc='Select...' data-improveclass='' data-dsname='Select...'>Select</option>";
		<cfloop index="strType" list="#lstBuildingAttributes#" delimiters=",">
			<cfswitch expression="#strType#">
				<cfcase value="ExteriorWall"><cfset strTypeName = "Exterior Walls"><cfset iModalSortOrder = 2></cfcase>
				<cfcase value="Frame"><cfset strTypeName = "Frame"><cfset iModalSortOrder = 5></cfcase>
				<cfcase value="Foundation"><cfset strTypeName = "Foundation"><cfset iModalSortOrder = 4></cfcase>
				<cfcase value="RoofStructure"><cfset strTypeName = "Roof Structure"><cfset iModalSortOrder = 10></cfcase>
				<cfcase value="RoofCover"><cfset strTypeName = "Roof Cover"><cfset iModalSortOrder = 9></cfcase>
				<cfcase value="Floor"><cfset strTypeName = "Flooring"><cfset iModalSortOrder = 3></cfcase>
				<cfcase value="InteriorWall"><cfset strTypeName = "Interior Walls"><cfset iModalSortOrder = 7></cfcase>
				<cfcase value="Heat"><cfset strTypeName = "Heat"><cfset iModalSortOrder = 6></cfcase>
				<cfcase value="AirConditioning"><cfset strTypeName = "Air Conditioning"><cfset iModalSortOrder = 1></cfcase>
				<cfcase value="Kitchen"><cfset strTypeName = "Kitchen"><cfset iModalSortOrder = 8></cfcase>
				<cfcase value="Windows"><cfset strTypeName = "Windows"><cfset iModalSortOrder = 11></cfcase>
			</cfswitch>
			strHTML = strHTML + "<option value='#strType#' data-val='#strType#' data-desc='#strTypeName#' data-modalsortorder='#iModalSortOrder#' data-improveclass='#strType#' data-dsname='#strTypeName#'>#strTypeName#</option>";
		</cfloop>
		strHTML = strHTML + "	</select>";

		/* These select boxes are displayed\hiddened and defaulted to selected item by the buildModel function. */
		<cfset iPosition = 0>
		<cfloop index="strAttribute" list="#lstBuildingAttributes#" delimiters=",">
			<cfset iPosition = iPosition + 1>
			<cfset strCurrentQuery = ListGetAt(lstQueryNames,iPosition)>

			strDisplay = (adjdesc == '#strAttribute#') ? "" : " style='display:none'";
			strHTML = strHTML + "<span class='selectCodeContainer'><select name='#strAttribute#' id='#strAttribute#_" + iRow + "' data-tags='true' class='two-part-large select2-2piece-new' " + strDisplay + " onchange='changeImproveTypeVal(" + iRow + ",this,\"#strAttribute#\")'>";
			strHTML = strHTML + "<option value='' data-val='' data-desc='Select...' data-adjcd='' data-adjds='Select...' data-adjustmenttype=''>Select...</option>";

			<cfloop query="#strCurrentQuery#">
				<cfset valCode = Trim(buildingCode)>
				<cfset valCodeID = Trim(buildingCodeID)>
				<cfset valDS = Trim(buildingCodeDescription)>
				<cfset valCodeType = Trim(codeType)>
				<CFIF LEN(TRIM(valCodeID)) NEQ 0>
					strHTML = strHTML + "<option value='#valCodeID#' data-val='#valCode#' data-desc='#valDS#' data-adjcd='#valCode#' data-adjds='#valDS#' data-adjustmenttype='#valCodeType#'>#valCode#: #valDS#</option>";
				</CFIF>
			</cfloop>
			strHTML = strHTML + "</select></span>";
		</cfloop>
		strHTML = strHTML + "<input name='percentageFactor' id='percentageFactor_" + iRow + "' class='form-control two-part-small numeric' type='text' value='0'>%"; // onchange='changeAdjVal(" + iRow +",this)'

		/* These inputs are populated by the buildModal function */
		strHTML = strHTML + "<input name='bLPoints' id='bLPoints_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHTML = strHTML + "<input name='bLLumpSum' id='bLLumpSum_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHTML = strHTML + "<input name='bLFactor' id='bLFactor_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHTML = strHTML + "<input name='bLRateAddOn' id='bLRateAddOn_" + iRow + "' type='text' value='' class='form-control two-part-small numeric' disabled>";
		strHRML = strHTML;

		strHTML = strHTML + "</div>";

		return strHTML;
	}

	// Called in createLabel and document ready functions
	function setTypeCounts() {
		/* Clear all of the labels that exist on the calling page */
		<cfloop index="strType" list="#lstBuildingAttributes#" delimiters=",">
			#strType#_display_count = 0;
			#strType#_display_count_current = 0;
			#strType#_display_count_multiple_items_blocker = 0;

			#strType#_#row#_extra = "";
			#strType# = 0;
		</cfloop>

		<!--- Set up the counts for the individual attributes, so we can do label formatting --->
		for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
			// Hard code to 1 for now until we test with multiple building items
			blatitem = jsBlItem;
			strMode = parent.buildingFeaturesData.getField(iCurrentRow,"mode");
			strCurrentAdjValue = parseFloat(parent.buildingFeaturesData.getField(iCurrentRow,"percentageFactor"));
			strCurrentType = parent.buildingFeaturesData.getField(iCurrentRow,"feature");

			if(isNaN(strCurrentAdjValue)) {
				<!--- Set the value to 0 in the WDDX packet as well as in the form input --->
				parent.buildingFeaturesData.setField(iCurrentRow,"percentageFactor", 0);
				$("##percentageFactor_" + iCurrentRow).val(0);
			}
			if(strCurrentAdjValue < 0) {
				<!--- Set the value to 0 in the WDDX packet as well as in the form input --->
				alert("Value must be greater than 0");
				parent.buildingFeaturesData.setField(iCurrentRow,"percentageFactor", 0);
				$("##percentageFactor_" + iCurrentRow).val(0);
			}

			if(strMode != "Delete" && strCurrentType != "N/A") {
				if(blatitem == jsBlItem) {
					switch(strCurrentType) {
						<cfloop index="strType" list="#lstBuildingAttributes#" delimiters=",">
							case "#strType#":
								#strType#_display_count++;
								break;
						</cfloop>
					}
				}
			}
		}
	}

	function createLabel() {
		var strLabel = "";
		var iPercentage = 0;

		/* Clear all of the labels that exist on the calling page */
		<cfloop index="strType" list="#lstBuildingAttributes#" delimiters=",">
			$("span###strType#_#row#").html("");
		</cfloop>

		<!--- Set up the counts for the individual attributes, so we can do label formatting --->
		setTypeCounts();
		<!--- Set improve percentage for attributes that only have 1 attribute to 100% (instead of showing 0) --->
		changeZerosToOneHundreds();

		<!--- Do the actual label creation now that we know how many items are in each adjustment type --->
		for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
			// Hard code to 1 for now until we test with multiple building items
			blatitem = jsBlItem;
			strCurrentType = parent.buildingFeaturesData.getField(iCurrentRow,"feature");
			strCurrentCode = parent.buildingFeaturesData.getField(iCurrentRow,"buildingCode");
			strCurrentCodeID = parent.buildingFeaturesData.getField(iCurrentRow,"buildingCodeID");
			strMode = parent.buildingFeaturesData.getField(iCurrentRow,"mode");
			strCurrentAdjValue = parseFloat(parent.buildingFeaturesData.getField(iCurrentRow,"percentageFactor"));

			if(isNaN(strCurrentAdjValue))  strCurrentAdjValue = 0;

			if(strMode != "Delete" && strCurrentType != "N/A") {
				strLabel = "";
				if(blatitem == jsBlItem) {
					switch(strCurrentType) {
						<cfloop index="strType" list="#lstBuildingAttributes#" delimiters=",">
							case "#strType#":
								strCurrentDS = parent.buildingFeaturesData.getField(iCurrentRow,"buildingCodeDescription");

								if(strCurrentCode != null && strCurrentCode.length != 0) {
									strLabel = strCurrentCode + ": " + strCurrentDS;
									strLabel = strLabel + " (" + strCurrentAdjValue + "%)";
									#strType# = #strType# + strCurrentAdjValue;
									strLabel = strLabel + "<br/>";
									// if there are more than 2 entries per attribute, display "Multiple Items" label.
									if(#strType#_display_count > 2) {
										if(#strType#_display_count_current == 0) {
											$("span###strType#_#row#").append("<span id='#strType#_#row#_extra' class='building-more-attributes' class='building-more-attributes' data-toggle='tooltip'>Multiple Items</span>");
										}
										#strType#_#row#_extra = #strType#_#row#_extra + strLabel; // Add the options to a hover over field we'll append later
										#strType#_display_count_current++;
									} else {
										$("span###strType#_#row#").append(strLabel);
										#strType#_display_count_current++;
									}
								}
								break;
						</cfloop>
					}
				}
			}
		} /* end FOR*/
		// Executes loop from CFWDDX function
		for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
			blatitem = jsBlItem;
			strCurrentType = parent.buildingFeaturesData.getField(iCurrentRow,"feature");

			if(blatitem == jsBlItem) {
				switch(strCurrentType) {
					<cfloop index="strType" list="#lstBuildingAttributes#" delimiters=",">
						case "#strType#":
							if(#strType#_display_count_multiple_items_blocker == 0) {
								$("span###strType#_#row#_extra").append(" (" + #strType# + "%)");
								strMakeLink = '<a href="##" class="building-line-miltiple-items-link" onclick="doBuildingPopup(#row#)">' + $("###strType#_#row#_extra").html() + '</a>';
								$("span###strType#_#row#_extra").html(strMakeLink);
								if(#strType#_#row#_extra != "") {
									$("span###strType#_#row#_extra").prop("title",#strType#_#row#_extra);
									#strType#_#row#_extra = "";
								}
								#strType#_display_count_multiple_items_blocker++
							}
							break;
					</cfloop>
				}
			}
		}
		parent.$('[data-toggle="tooltip"]').tooltip({html: true});
	}

</script>

#addAsset('/includes/css/select2.css,/includes/css/madison.css,/includes/js/select2.js,/includes/js/buildingLineAdjustments.js,/includes/js/formatSelect.js')#
</CFOUTPUT>

<script type="text/javascript">

jQuery.fn.sortDivs = function sortDivs() {
    $("> div", this[0]).sort(dec_sort).appendTo(this[0]);
    function dec_sort(a, b){ return ($(b).data("sort")) < ($(a).data("sort")) ? 1 : -1; }
}

$( document ).ready(function() {
	buildModal();
	changeSelectsToSelect2s();
	$('[data-toggle="tooltip"]').tooltip();
	$("#adjContent").sortDivs();
	setTypeCounts();
	changeZerosToOneHundreds();
});


</script>