<cfoutput>

<cfsilent>

	<cfsavecontent variable="strAdjustmentOptions">
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Exterior Wall</span><br />
			<span class="formatted-data-values" id="ExteriorWall_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "ExteriorWall">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Frame</span><br />
			<span class="formatted-data-values" id="Frame_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "Frame">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-6 databox">
			<span class="formatted-data-label">Foundation</span><br />
			<span class="formatted-data-values" id="Foundation_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "Foundation">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Roof Structure</span><br />
			<span class="formatted-data-values" id="RoofStructure_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "RoofStructure">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Roof Cover</span><br />
			<span class="formatted-data-values" id="RoofCover_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "RoofCover">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Flooring</span><br />
			<span class="formatted-data-values" id="Floor_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "Floor">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Interior Wall</span><br />
			<span class="formatted-data-values" id="InteriorWall_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "InteriorWall">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>

		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Heating</span><br />
			<span class="formatted-data-values" id="Heat_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "Heat">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Air Conditioning</span><br />
			<span class="formatted-data-values" id="AirConditioning_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "AirConditioning">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Kitchen</span><br />
			<span class="formatted-data-values" id="Kitchen_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "Kitchen">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
		<div class="col-xs-3 databox">
			<span class="formatted-data-label">Windows</span><br />
			<span class="formatted-data-values" id="Windows_#prc.getBuldingLineInfo.currentrow#">
				<cfloop index="i" from="1" to="#arraylen(prc.arrayBuildingLineFeatures)#" step="1">
					<cfif prc.arrayBuildingLineFeatures[i][1] eq prc.getBuldingLineInfo.buildingID and prc.arrayBuildingLineFeatures[i][2] eq "Windows">
						#prc.arrayBuildingLineFeatures[i][3]#: #prc.arrayBuildingLineFeatures[i][4]# (#prc.arrayBuildingLineFeatures[i][5]#%)<br>
					</cfif>
				</cfloop>
			</span>
		</div>
	</cfsavecontent>

</cfsilent>


	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading#prc.getBuldingLineInfo.currentrow#"  aria-expanded="false" >
		<table class="land-line-summary">
		<tr href="##collapse#prc.getBuldingLineInfo.currentrow#" class="section-bar-expander" onclick="toggleChevronChild(#prc.getBuldingLineInfo.currentrow#)" data-parent="heading#prc.getBuldingLineInfo.currentrow#" data-toggle="collapse" aria-controls="collapse#prc.getBuldingLineInfo.currentrow#" >
			<td width="5%" class="section-bar-label">###prc.getBuldingLineInfo.currentrow#</td>
			<td width="10%" class="section-bar-label" onclick="event.cancelBubble=true;"><a onclick="doBuildingTotalsPopup(#prc.getBuldingLineInfo.CurrentRow#,#prc.getBuldingLineInfo.buildingID#,#prc.getBuldingLineInfo.itemNumber#)" rel="tooltip" title="Click to View Building Line Item Totals">View Totals</a></td>
			<td width="30%" class="section-bar-label">#prc.getBuldingLineInfo.BuildingLabel#</td>
			<td width="15%" class="section-bar-label">#prc.getBuldingLineInfo.modelNumber# #prc.getBuldingLineInfo.modelNumberDescription#</td>
			<td width="10%" class="section-bar-label">#prc.getBuldingLineInfo.effectiveYearBuilt#</td>
			<td width="10%" class="section-bar-label"></td>
			<td width="15%" class="section-bar-label text-right">$ #NumberFormat(prc.getBuldingLineInfo.AppraisedValue, ",.00")#</td>
			<td width="5%" class="section-bar-label text-right"><span id="chevron_#prc.getBuldingLineInfo.currentrow#" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse#prc.getBuldingLineInfo.currentrow#" class="panel-collapse collapse buildingline" role="tabpanel" aria-labelledby="heading#prc.getBuldingLineInfo.currentrow#">
		<div id="view_mode_item_#prc.getBuldingLineInfo.currentrow#">
			<div class="building-part-action-buttons">
				<a href="javascript:editThisRow(#prc.getBuldingLineInfo.currentrow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
			</div>
			<div class="row fix-bs-row">
				<div class="col-xs-12 databox">
					<span class="formatted-data-label">Building ID</span><br>
					<span class="formatted-data-values">#prc.getBuldingLineInfo.buildingID#</span>
				</div>
			</div>
			<div class="row fix-bs-row">
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Custom Label</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.BuildingLabel#</span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Neighborhood</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.NeighborhoodCode#: #prc.getBuldingLineInfo.NeighborhoodCodeDescription#</span>
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Use</span><br>
					<span class="formatted-data-values">#prc.getBuldingLineInfo.buildingUseCode#: #prc.getBuldingLineInfo.buildingUseCodeDescription#</span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Building Type</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.modelNumber#: #prc.getBuldingLineInfo.ModelNumberDescription#</span>
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Actual Year Built</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.actualYearBuilt#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Effective Year Built</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.effectiveYearBuilt#</span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Quality</span><br />
					<span class="formatted-data-values">
						<cfloop query="prc.getBuildingAttributeDropDowns_Quality">
							<cfif selected>
								#buildingCode#: #buildingCodeDescription#
							</cfif>
						</cfloop>
					</span>
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Bathrooms</span><br/>
					<span class="formatted-data-values">#prc.getBuldingLineInfo.numberBaths#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Bedrooms</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.numberBedRooms#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Stories</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.numberStories#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Architectural Style</span><br />
					<span class="formatted-data-values">
						<cfloop query="prc.getBuildingAttributeDropDowns_ArchitecturalStyle">
							<cfif selected>
								#buildingCode#: #buildingCodeDescription#
							</cfif>
						</cfloop>
					</span>
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Economic Obs.</span><br />
					<span class="formatted-data-values">#numberformat(prc.getBuldingLineInfo.economicObsolescencePct, '999')#%</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Functional Obs.</span><br />
				<span class="formatted-data-values">#numberformat(prc.getBuldingLineInfo.functionalObsolescencePct, '999')#%</span>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Special Condition / %</span><br />
					<span class="formatted-data-values">
						<cfif len(trim(prc.getBuldingLineInfo.specialConditionCode))>
							#prc.getBuldingLineInfo.specialConditionCode#: #prc.getBuldingLineInfo.specialConditionCodeDescription# (#prc.getBuldingLineInfo.SpecialConditionPct#%)
						</cfif>
					</span>
				</div>
			</div>

			<div class="row fix-bs-row add-in-box">
				#strAdjustmentOptions#
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Shape / Size</span><br />
					<span class="formatted-data-values">
						<cfloop query="prc.getBuildingAttributeDropDowns_SizeShape">
							<cfif selected>
								#buildingCode#: #buildingCodeDescription#
							</cfif>
						</cfloop>
					</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Plumbing Fixtures</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.NumberPlumbingFixtures#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Ceiling</span><br />
					<span class="formatted-data-values">
						<cfloop query="prc.getBuildingAttributeDropDowns_Ceiling">
							<cfif selected>
								#buildingCode#: #buildingCodeDescription#
							</cfif>
						</cfloop>
					</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Rooms</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.numberRooms#</span>
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Units</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.numberUnits#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Perimeter</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.perimeter#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Height</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.roomHeight#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Class</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.ClassCode#: #prc.getBuldingLineInfo.ClassCodeDescription#</span>
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Condition</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.ConditionCode#: #prc.getBuldingLineInfo.ConditionCodeDescription#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Common Wall Factor</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.commonWallPct#%</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Occupancy</span><br />
					<span class="formatted-data-values">#prc.getBuldingLineInfo.OccupancyCode#: #prc.getBuldingLineInfo.OccupancyCodeDescription#</span>
				</div>
				<div class="col-xs-3 databox"></div>
			</div>
<!---
			<cfif prc.getBuildingCustomAttributeList.recordcount>
				<div class="row fix-bs-row">
					<cfloop query="prc.getBuildingCustomAttributeList">
						<cfset strCustomAttributeQuery = Replace(prc.getBuildingCustomAttributeList.codetype, "-", "", "ALL")>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">#codeType#</span><br />
							<span class="formatted-data-values"> </span>
							<cfloop query="prc.getBuildingCustomAttributes_#strCustomAttributeQuery#">
								<cfif selected>
									#buildingCustomCode#: #buildingCustomCodeDescription#
								</cfif>
							</cfloop>
						</div>
					</cfloop>

				</div>
			</cfif>
--->

			<div class="row fix-bs-row add-in-box">
				<div class="col-xs-12">
					#renderView('building/viewlets/_buildingAddIns')#
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-2 databox">
					<div class="formatted-data-label text-center">Amendment Eligible</div>
					<div class="formatted-data-values text-center">#prc.getBuldingLineInfo.amendmentEligibleFlag#</div>
				</div>
				<div class="col-xs-2 databox"><br/>
					<div class="formatted-data-label text-center">New Construction</div>
					<div class="formatted-data-values text-center">#prc.getBuldingLineInfo.newConstructionFlag#</div>
				</div>
				<div class="col-xs-2 databox"><br/>
					<div class="formatted-data-label text-center">Replacement</div>
					<div class="formatted-data-values text-center">#prc.getBuldingLineInfo.replacementFlag#</div>
				</div>
				<div class="col-xs-3 databox"><br/>
					<span class="formatted-data-label">Normal Depreciation</span><br />
					<span class="formatted-data-values">#numberformat(prc.getBuldingLineInfo.normalDepreciationPct, ',.00')#%</span>
				</div>
				<div class="col-xs-3 databox"><br/>
					<span class="formatted-data-label">Appraised Value</span><br />
					<span class="formatted-data-values">$#NumberFormat(prc.getBuldingLineInfo.AppraisedValue, ",.00")#</span>
				</div>
			</div>

			<div class="row fix-bs-row">
				<div class="col-xs-12">
					<div class="building-part-action-buttons">
						<a href="javascript:editThisRow(#prc.getBuldingLineInfo.currentrow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>

		</div> <!--- id="view_mode_item_#prc.getBuldingLineInfo.currentrow#" --->


		<!---
		 _____    _ _ _      ____          _
		| ____|__| (_) |_   / ___|___   __| | ___
		|  _| / _` | | __| | |   / _ \ / _` |/ _ \
		| |__| (_| | | |_  | |__| (_) | (_| |  __/
		|_____\__,_|_|\__|  \____\___/ \__,_|\___|

		  --->
		<div id="edit_mode_item_#prc.getBuldingLineInfo.currentrow#" style="display:none;">
			<div class="building-part-action-buttons">
				<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
				&nbsp;&nbsp;
				<a href="javascript:saveWDDX(#TRIM(prc.getBuldingLineInfo.itemNumber)#)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
			</div>
			<div class="clearfix"></div>

			<div class="row fix-bs-row">
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Custom Label</span><br />
					<input type="text" name="BuildingLabel" class="form-control" value="#prc.getBuldingLineInfo.BuildingLabel#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Neighborhood</span><br />
					<select data-tags="true" class="form-control select2-3piece-new" name="neighborhoodcode" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getNeighborhoods">
							<option value="#NumberFormat(prc.getNeighborhoods.nbhdcd,'9.99')#" data-val="#NumberFormat(prc.getNeighborhoods.nbhdcd,'9.99')#" data-desc="#prc.getNeighborhoods.nbhdds#" data-adjval="#NumberFormat(prc.getNeighborhoods.nbhdfa,'9.999')#" <cfif prc.getBuldingLineInfo.neighborhoodcode EQ prc.getNeighborhoods.nbhdcd> selected</cfif>>#prc.getNeighborhoods.nbhdcd#: #prc.getNeighborhoods.nbhdds#</option>
						</cfloop>
					</select>
				</div>


				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Use</span><br />
					<select data-tags="true" class="form-control select2-2piece-new" name="buildingUseCode" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getBuildingUseCodes">
							<option value="#prc.getBuildingUseCodes.code#" data-val="#prc.getBuildingUseCodes.code#" data-desc="#prc.getBuildingUseCodes.codeDescription#" <cfif prc.getBuldingLineInfo.buildingUseCode EQ prc.getBuildingUseCodes.code> selected</cfif>>#prc.getBuildingUseCodes.code#: #prc.getBuildingUseCodes.codeDescription#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Building Type</span><br />
					<!--- TODO   -- onchange="unitBuildingTypeChange(#prc.getBuldingLineInfo.CurrentRow-1#,this)" --->
					<select data-tags="true" class="form-control select2-2piece-new" name="modelNumber" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.getBuildingTypes">
							<option value="#prc.getBuildingTypes.modelNumber#" data-val="#prc.getBuildingTypes.modelNumber#" data-desc="#prc.getBuildingTypes.description#" <cfif prc.getBuldingLineInfo.modelNumber EQ prc.getBuildingTypes.modelNumber> selected</cfif>>#prc.getBuildingTypes.modelNumber#: #prc.getBuildingTypes.description#</option>
						</cfloop>
					</select>
				</div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Actual Year Built</span><br />
					<input type="text" name="ActualYearBuilt" class="form-control numeric" value="#prc.getBuldingLineInfo.ActualYearBuilt#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Effective Year Built</span><br />
					<input type="text" name="EffectiveYearBuilt" class="form-control numeric" value="#prc.getBuldingLineInfo.EffectiveYearBuilt#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-6 databox">
					<span class="formatted-data-label">Quality</span><br />
					<span id="blqual_#prc.getBuldingLineInfo.CurrentRow-1#">
						<select data-tags="true"  class="form-control select2-2piece-new" name="quality" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getBuildingAttributeDropDowns_Quality">
								<option value="#prc.getBuildingAttributeDropDowns_Quality.buildingCodeID#" data-val="#prc.getBuildingAttributeDropDowns_Quality.buildingCode#" data-desc="#prc.getBuildingAttributeDropDowns_Quality.buildingCodeDescription#" <cfif prc.getBuildingAttributeDropDowns_Quality.selected> selected</cfif>>#prc.getBuildingAttributeDropDowns_Quality.buildingCode#: #prc.getBuildingAttributeDropDowns_Quality.buildingCodeDescription#</option>
							</cfloop>
						</select>
					</span>
				</div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Bathrooms</span><br />
					<input type="text" name="NumberBaths" class="form-control numeric" value="#prc.getBuldingLineInfo.NumberBaths#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Bedrooms</span><br />
					<input type="text" name="NumberBedrooms" class="form-control numeric" value="#prc.getBuldingLineInfo.NumberBedrooms#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Stories</span><br />
					<input type="text" name="NumberStories" class="form-control numeric" value="#prc.getBuldingLineInfo.NumberStories#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Architectural Style</span><br />
					<span id="blarch_#prc.getBuldingLineInfo.CurrentRow-1#">
						<select data-tags="true" class="form-control select2-2piece-new" name="architecturestyle" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getBuildingAttributeDropDowns_ArchitecturalStyle">
								<option value="#prc.getBuildingAttributeDropDowns_ArchitecturalStyle.buildingCodeID#" data-val="#prc.getBuildingAttributeDropDowns_ArchitecturalStyle.buildingCode#" data-desc="#prc.getBuildingAttributeDropDowns_ArchitecturalStyle.buildingCodeDescription#" <cfif prc.getBuildingAttributeDropDowns_ArchitecturalStyle.selected> selected</cfif>>#prc.getBuildingAttributeDropDowns_ArchitecturalStyle.buildingCode#: #prc.getBuildingAttributeDropDowns_ArchitecturalStyle.buildingCodeDescription#</option>
							</cfloop>
						</select>
					</span>
				</div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Economic Obs.</span><br />
					<input type="text" name="economicObsolescencePct" class="form-control numeric width-90percent" value="#NumberFormat(prc.getBuldingLineInfo.economicObsolescencePct,"0.000")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">%
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Functional Obs.</span><br />
					<input type="text" name="functionalObsolescencePct" class="form-control numeric width-90percent" value="#NumberFormat(prc.getBuldingLineInfo.functionalObsolescencePct,"0.000")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">%
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Special Conditions</span><br />
					<span id="SpecialConditionCode_#prc.getBuldingLineInfo.CurrentRow-1#">
						<select data-tags="true"  class="form-control select2-2piece-new" name="SpecialConditionCode" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getSpecialConditions">
								<option data-val="#prc.getSpecialConditions.Code#" data-desc="#prc.getSpecialConditions.CodeDescription#" value="#prc.getSpecialConditions.Code#" <CFIF prc.getBuldingLineInfo.SpecialConditionCode EQ prc.getSpecialConditions.Code>selected</CFIF>>#prc.getSpecialConditions.Code#:#prc.getSpecialConditions.CodeDescription#</option>
							</cfloop>
						</select>
					</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label"></span><br/>
					<input type="text" name="SpecialConditionPct" class="form-control numeric width-80percent" data-toggle="tooltip" data-placement="top" title="Special Conditions Percentage" value="#TRIM(prc.getBuldingLineInfo.SpecialConditionPct)#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">%
				</div>
			</div>


			<div class="clearfix">&nbsp;</div>

			<div class="row fix-bs-row add-in-box">
				#strAdjustmentOptions#
				<div class="col-xs-12 building-button-row">
					<a onclick="doBuildingPopup(#prc.getBuldingLineInfo.CurrentRow#)" data-toggle="tooltip" data-placement="top" title="Click to alter Adjustments"><img src="/includes/images/v2/addEdit_blue.gif" alt="" width="156" height="26" border="0"></a>
				</div>
			</div>


			<div class="clearfix">&nbsp;</div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Shape / Size</span><br />
					<span class="formatted-data-values">
						<select data-tags="true" class="form-control select2-2piece-new" name="sizeshape" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getBuildingAttributeDropDowns_SizeShape">
								<option value="#prc.getBuildingAttributeDropDowns_SizeShape.buildingCodeID#" data-val="#prc.getBuildingAttributeDropDowns_SizeShape.buildingCode#" data-desc="#prc.getBuildingAttributeDropDowns_SizeShape.buildingCodeDescription#" <cfif prc.getBuildingAttributeDropDowns_SizeShape.selected> selected</cfif>>#prc.getBuildingAttributeDropDowns_SizeShape.buildingCode#: #prc.getBuildingAttributeDropDowns_SizeShape.buildingCodeDescription#</option>
							</cfloop>
						</select>
					</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Plumbing Fixtures</span><br />
					<input type="text" name="NumberPlumbingFixtures" class="form-control"  value="#prc.getBuldingLineInfo.NumberPlumbingFixtures#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Ceiling</span><br />
					<span id="blceil_#prc.getBuldingLineInfo.CurrentRow-1#">
						<select data-tags="true" class="form-control select2-2piece-new" name="ceiling" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getBuildingAttributeDropDowns_Ceiling">
								<option value="#prc.getBuildingAttributeDropDowns_Ceiling.buildingCodeID#" data-val="#prc.getBuildingAttributeDropDowns_Ceiling.buildingCode#" data-desc="#prc.getBuildingAttributeDropDowns_Ceiling.buildingCodeDescription#" <cfif prc.getBuildingAttributeDropDowns_Ceiling.selected> selected</cfif>>#prc.getBuildingAttributeDropDowns_Ceiling.buildingCode#: #prc.getBuildingAttributeDropDowns_Ceiling.buildingCodeDescription#</option>
							</cfloop>
						</select>
					</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Rooms</span><br />
					<input type="text" name="numberRooms" class="form-control numeric" value="#NumberFormat(prc.getBuldingLineInfo.numberRooms,"0.000")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Units</span><br />
					<input type="text" name="NumberUnits" class="form-control numeric" value="#NumberFormat(prc.getBuldingLineInfo.NumberUnits,"0.000")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Perimeter</span><br />
					<input type="text" name="perimeter" class="form-control numeric" value="#NumberFormat(prc.getBuldingLineInfo.perimeter,"0.000")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Height</span><br />
					<input type="text" name="RoomHeight" class="form-control numeric" value="#NumberFormat(prc.getBuldingLineInfo.RoomHeight,"0.000")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Class</span><br />
					<span id="blclas_#prc.getBuldingLineInfo.CurrentRow-1#">
						<select data-tags="true" class="form-control select2-2piece-new" name="classCode" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getBuildingAttributeDropDowns_Class">
								<option value="#prc.getBuildingAttributeDropDowns_Class.code#" data-val="#prc.getBuildingAttributeDropDowns_Class.code#" data-desc="#prc.getBuildingAttributeDropDowns_Class.codedescription#" <cfif prc.getBuldingLineInfo.ClassCode EQ prc.getBuildingAttributeDropDowns_Class.code> selected</cfif>>#prc.getBuildingAttributeDropDowns_Class.code#: #prc.getBuildingAttributeDropDowns_Class.codedescription#</option>
							</cfloop>
						</select>
					</span>
				</div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Condition</span><br />
					<span id="blcond_#prc.getBuldingLineInfo.CurrentRow-1#">
						<select data-tags="true" class="form-control select2-2piece-new" name="conditionCode" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getBuildingAttributeDropDowns_Condition">
								<option value="#prc.getBuildingAttributeDropDowns_Condition.code#" data-val="#prc.getBuildingAttributeDropDowns_Condition.code#" data-desc="#prc.getBuildingAttributeDropDowns_Condition.codedescription#" <cfif prc.getBuldingLineInfo.ConditionCode EQ prc.getBuildingAttributeDropDowns_Condition.code> selected</cfif>>#prc.getBuildingAttributeDropDowns_Condition.code#: #prc.getBuildingAttributeDropDowns_Condition.codedescription#</option>
							</cfloop>
						</select>
					</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Common Wall Factor</span><br />
					<input type="text" name="CommonWallPct" class="form-control rightAlign width-90percent" value="#prc.getBuldingLineInfo.CommonWallPct#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">%
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Occupancy</span><br />
					<span id="blocc_#prc.getBuldingLineInfo.CurrentRow-1#">
						<select data-tags="true" class="form-control select2-2piece-new" name="occupancyCode" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
							<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
							<cfloop query="prc.getBuildingAttributeDropDowns_Occupancy">
								<option value="#prc.getBuildingAttributeDropDowns_Occupancy.code#" data-val="#prc.getBuildingAttributeDropDowns_Occupancy.code#" data-desc="#prc.getBuildingAttributeDropDowns_Occupancy.codedescription#" <cfif prc.getBuldingLineInfo.OccupancyCode EQ prc.getBuildingAttributeDropDowns_Occupancy.code> selected</cfif>>#prc.getBuildingAttributeDropDowns_Occupancy.code#: #prc.getBuildingAttributeDropDowns_Occupancy.codedescription#</option>
							</cfloop>
						</select>
					</span>
				</div>
				<div class="col-xs-3 databox"></div>
			</div>

			<div class="clearfix">&nbsp;</div>
<!---
			<cfif prc.getBuildingCustomAttributeList.recordcount>
				<div class="row fix-bs-row">
					<cfloop query="prc.getBuildingCustomAttributeList">
						<cfset strQuery  = Replace(codetype, "-", "", "ALL")>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">#codeType#</span><br />
							<span id="blocc_#prc.getBuldingLineInfo.CurrentRow-1#">
								<select data-tags="true" class="form-control select2-2piece-new" name="codeType" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
									<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
									<cfloop query="prc.getBuildingCustomAttributes_#strQuery#">
										<option value="#buildingCustomCode#" data-val="#buildingCustomCode#" data-desc="#buildingCustomCodeDescription#" >#buildingCustomCode#: #buildingCustomCodeDescription#</option>
									</cfloop>
								</select>
							</span>
						</div>
					</cfloop>

				</div>

				<div class="clearfix">&nbsp;</div>
			</cfif>
--->
			<div class="row fix-bs-row add-in-box">
				<div class="col-xs-12">
					#renderView('building/viewlets/_buildingAddIns')#
				</div>
				<div class="col-xs-12 building-button-row">
					<span class="land-line-adj-button" onclick="doBuildingAddinPopup(#prc.getBuldingLineInfo.CurrentRow#,#prc.getBuldingLineInfo.buildingID#,#prc.getBuldingLineInfo.itemNumber#)" rel="tooltip" title="Click to alter Add-Ins"><img src="/includes/images/v2/addedit_ADDins_blue.gif" alt="" width="134" height="26" border="0"></span>
				</div>

			</div>

			<div class="clearfix">&nbsp;</div>

			<div class="fix-bs-row">
				<div class="col-xs-2 databox">
					<div class="formatted-data-label" align="center">Amendment Eligible</div>
					<div align="center"><input type="checkbox" name="AmendmentEligibleFlag" id="AmendmentEligibleFlag" value="Y" <CFIF prc.getBuldingLineInfo.AmendmentEligibleFlag EQ "Y">checked</CFIF> onchange="changeCheckboxVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)"></div>
				</div>
				<div class="col-xs-2 databox"><br/>
					<div class="formatted-data-label" align="center">New Construction</div>
					<div align="center"><input type="checkbox" name="newConstructionFlag" id="newConstructionFlag" value="Y" <CFIF prc.getBuldingLineInfo.NewConstructionFlag EQ "Y">checked</CFIF> onchange="changeCheckboxVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)"></div>
				</div>
				<div class="col-xs-2 databox"><br/>
					<div class="formatted-data-label" align="center">Replacement</div>
					<div align="center"><input type="checkbox" name="replacementFlag" id="replacementFlag" value="Y" <CFIF prc.getBuldingLineInfo.replacementFlag EQ "Y">checked</CFIF> onchange="changeCheckboxVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)"></div>
				</div>
				<div class="col-xs-3 databox"><br/>
					<span class="formatted-data-label">Normal Depreciation</span><br />
					<input type="text" name="normaldepreciationpct" class="form-control numeric width-90percent two-digits" disabled value="#NumberFormat(prc.getBuldingLineInfo.normalDepreciationPct,",.99")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">%
				</div>
				<div class="col-xs-3 databox"><br/>
					<span class="formatted-data-label">Appraised Value</span><br />
					<input type="text" name="AppraisedValue" class="form-control numeric" disabled value="#NumberFormat(round(val(prc.getBuldingLineInfo.AppraisedValue)),",")#" onblur="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)" onchange="changeVal(#prc.getBuldingLineInfo.CurrentRow-1#,this)">
				</div>
			</div>


			<div class="clearfix">&nbsp;</div>

			<div class="building-part-action-buttons">
				<a href="javascript:cancelEverything()"><img src="/includes/images/v2/cancel.png" alt="Cancel" border="0"></a>
				&nbsp;&nbsp;
				<a href="javascript:saveWDDX(#TRIM(prc.getBuldingLineInfo.itemNumber)#)"><img src="/includes/images/v2/homepageCarouselSettings.png" alt="Save" border="0"></a>
			</div>

			<br clear="all"/>

		</div> <!--- END div id="edit_mode_item_#prc.getBuldingLineInfo.currentrow#" style="display:none;" --->

	</div> <!--- id="collapse#prc.getBuldingLineInfo.currentrow#" --->
</cfoutput>