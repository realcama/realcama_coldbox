<cfoutput>
<cfset showTabOptions = "building" /> <!--- used in miniNavigationBar.cfm --->
<cfset iPageNumber = 0 />
<cfset tab = "building" /> <!--- used in audit-boxes.cfm --->

<cfif prc.getBuldingLineInfo.recordcount eq "0">
	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			No building line records found for the selected parcel.
		</div>
	</div>
<cfelse>
	#renderView('/_templates/alertMsgs')#

	<script>
		var taxYear = #prc.stPersistentInfo.taxYear#;
		var parcelID = "#prc.stPersistentInfo.parcelID#";

		var iCurrentPage = 1;

		<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.getBuldingLineInfo#"
				TOPLEVELVARIABLE="buildingData">
		<!---
		getBuildingLineFeatures is created by querynew function in the getAllBuildingData function in buildingServices.cfc.
		--->
		<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.getBuildingLineFeatures#"
				TOPLEVELVARIABLE="buildingFeaturesData">

		<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.getBuildingLineFeatures#"
				TOPLEVELVARIABLE="buildingFeaturesDataOrig">

		<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.getBuildingCustomAttributesAssigned#"
				TOPLEVELVARIABLE="buildingAddInsData">
		<!---
		This result set has all the possible Adjustment items for this building.
		It is used by the changeImproveTypeVal javascript function to populate
		the Points, Lump Sum, Factor, or Rate Add-On input boxes in the
		Adjustments pop up.
		--->
		<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.getAllBuildingLineFeatures#"
				TOPLEVELVARIABLE="allBuildingFeatures">

<!---
		<CFWDDX ACTION="CFML2JS"
				INPUT="#qryUDFs#"
				TOPLEVELVARIABLE="udfData">

		function pageIncrement(position) {
			if((iCurrentPage == 1 && position == -1) || (iCurrentPage == #prc.iTotalPages# && position == 1 ))
			{
				/* Do Nothing */
			}
			else {
				for(i=1; i <= #prc.iTotalPages#; i++ ) {
					$("##page-" + i).hide();
				}
				iCurrentPage = iCurrentPage + position;
				$("##page-" + iCurrentPage).show();
				$("##current-page").html(iCurrentPage);
			}

			if(iCurrentPage == #prc.iTotalPages# && position == 1) {
				$("##building-page-next").addClass("disabled");
			} else {
				$("##building-page-next").removeClass("disabled");
			}

			if(iCurrentPage == 1 && position == -1) {
				$("##building-page-prev").addClass("disabled");
			} else {
				$("##building-page-prev").removeClass("disabled");
			}
		}
--->

	</script>

	<form action="#event.buildLink('building/buildingSave')#" method="post" name="frmSave" id="frmSave">
		<input type="hidden" name="buildingData" id="buildingData" value="">
		<input type="hidden" name="buildingAddInsData" id="buildingAddInsData" value="">
		<input type="hidden" name="buildingFeaturesData" id="buildingFeaturesData" value="">
	</form>

	<cfif Controller.getSetting( "bShowPacketLinks")>
		<div class="alert alert-info">
			<A href="javascript:void(0);" title="Building Packet" onclick="WriteRaw(buildingData);">Building Packet</A>&nbsp;&nbsp;&nbsp;
			<A href="javascript:void(0);" title="Building Features" onclick="WriteRaw(buildingFeaturesData);">Building Features Packet</A>&nbsp;&nbsp;&nbsp;
			<A href="javascript:void(0);" title="Building Features" onclick="WriteRaw(buildingFeaturesDataOrig);">Building Features Packet Backup</A>&nbsp;&nbsp;&nbsp;
			<A href="javascript:void(0);" title="Add-Ins Packet" onclick="WriteRaw(buildingAddInsData);">Add-Ins Packet</A>&nbsp;&nbsp;&nbsp;
			<A href="javascript:void(0);" title="All Building Features Packet" onclick="WriteRaw(allBuildingFeatures);">All Building Features Packet</A>&nbsp;&nbsp;&nbsp;
			<!--- <A href="javascript:void(0);" title="UDF Packet" onclick="WriteRaw(udfData);">UDF Packet</A>&nbsp;&nbsp;&nbsp; --->
		</div>
	</cfif>

	#renderView('/_templates/audit-boxes')#

	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			#renderView('_templates/miniNavigationBar')#
			Building Lines <span id="totalLandLines">(#prc.getBuldingLineInfo.recordcount#)</span>
		</div>

		<div id="expander-holders" class="panel panel-default">
			<cfloop query="prc.getBuldingLineInfo">
				<cfif prc.getBuldingLineInfo.CurrentRow MOD rc.page_size EQ 1>
					<cfset iPageNumber = iPageNumber + 1 />
					<div id="page-#iPageNumber#" <cfif rc.page neq iPageNumber>class="hide"</cfif>>
				</cfif>

				#renderView('building/viewlets/_buildingLine')#

				<cfif prc.getBuldingLineInfo.CurrentRow MOD rc.page_size EQ 0>
					</div>
				</cfif>
			</cfloop>

			<cfif prc.getBuldingLineInfo.CurrentRow MOD rc.page_size NEQ 0>
				</div> <!--- This ensures the ' div id="page-#iPageNumber#" ' to be closed correctly --->
			</cfif>
		</div> <!--- div id="expander-holders" class="panel panel-default" --->
	</div>
</cfif>

#addAsset('/includes/js/header.js,/includes/js/buildinginfo.js,/includes/js/formatSelect.js')#

</cfoutput>

