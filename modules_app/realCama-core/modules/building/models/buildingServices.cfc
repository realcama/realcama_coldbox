/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dropdownsDAO"	inject=model;
	property name="buildingDAO"		inject=model;
	property name="persistentDAO" 	inject=model;
	property name="QueryHelper" 	inject="QueryHelper@cbcommons";


	/**
	 * Constructor
	 */
	buildingServices function init(){
		return this;
	}


	function getAllBuildingData(required struct persistentCriteria) {

		// list order is based on front end display order
		var listFeatures = "ExteriorWall,Frame,Foundation,RoofStructure,RoofCover,Floor,InteriorWall,Heat,AirConditioning,Kitchen,Windows";
		var arrayFeatures = ListToArray(listFeatures);
		var results = {};
		var arrayBuildingFeatures = arraynew(2);
		var arrayAttributeDataValue = arraynew(1);
		var arrayAttributeDataCodeID = arraynew(1);
		var strGetCodeType = "";

		// Create empty recordset for the Building Features
		// These column names match column names returned by p_GetBuildingAttributes SP
		var qryBuildingFeatures = QueryNew("buildingID,itemNumber,modalsortingorder,buildingCodeJoinID,feature,
											buildingCode,buildingCodeID,buildingCodeDescription,percentageFactor,mode,Adjustment,AdjustmentType",
											"integer,integer,integer,integer,varchar,integer,integer,varchar,integer,varchar,integer,varchar");
		var qryAllBuildingFeatures = QueryNew(	"buildingID,Adjustment,AdjustmentType,codetype",
												"integer,integer,varchar,varchar"
												);

		var structfeatureLine = StructNew();

		// Stored Proc to retrieve the Features Information
		results.getBuldingLineInfo = getBuldingLineInfo( persistentCriteria = arguments.persistentCriteria );

		if ( results.getBuldingLineInfo.recordcount > 0 ) {
			// Loop thru building query
			var m = 1;
			for ( i = 1; i lte results.getBuldingLineInfo.recordcount; i++ ) {
				// Loop thru list of features
				for ( j = 1; j lte arrayLen(arrayFeatures); j++ ) {
					// Runs stored proc to get attributes
					if (j < 399) {
						qryBuildingLineFeatures = getBuildingAttributes( buildingID = results.getBuldingLineInfo["buildingID"][i], codeType = arrayFeatures[j] );

						for ( k = 1; k lte qryBuildingLineFeatures.recordcount; k++ ) {
							if ( qryBuildingLineFeatures["selected"][k] eq 1 ) {
								arrayBuildingFeatures[m][1] = results.getBuldingLineInfo["buildingID"][i];
								arrayBuildingFeatures[m][2] = arrayFeatures[j];
								arrayBuildingFeatures[m][3] = qryBuildingLineFeatures["buildingCode"][k];
								arrayBuildingFeatures[m][4] = qryBuildingLineFeatures["buildingCodeDescription"][k];
								arrayBuildingFeatures[m][5] = qryBuildingLineFeatures["percentageFactor"][k];
								arrayBuildingFeatures[m][6] = qryBuildingLineFeatures["Adjustment"][k];
								arrayBuildingFeatures[m][7] = qryBuildingLineFeatures["AdjustmentType"][k];
								m++;

								queryAddRow(qryBuildingFeatures,
											[{
												buildingID = results.getBuldingLineInfo["buildingID"][i],
												itemNumber = results.getBuldingLineInfo["itemNumber"][i],
												mode = 'none',
												modalsortingorder = 0,
												buildingCodeJoinID = qryBuildingLineFeatures["buildingCodeJoinID"][k],
												feature = arrayFeatures[j],
												buildingCode = qryBuildingLineFeatures["buildingCode"][k],
												buildingCodeID = qryBuildingLineFeatures["buildingCodeID"][k],
												buildingCodeDescription = qryBuildingLineFeatures["buildingCodeDescription"][k],
												percentageFactor = qryBuildingLineFeatures["percentageFactor"][k],
												Adjustment = qryBuildingLineFeatures["Adjustment"][k],
												AdjustmentType = qryBuildingLineFeatures["AdjustmentType"][k]
											} ]);
							}
							queryAddRow(qryAllBuildingFeatures,
											[{
											buildingID = qryBuildingLineFeatures["buildingCodeID"][k],
											Adjustment = qryBuildingLineFeatures["Adjustment"][k],
											AdjustmentType = qryBuildingLineFeatures["AdjustmentType"][k],
											codetype = qryBuildingLineFeatures["codetype"][k]
											}]
										);
						}
					}
				}
			}
			results.arrayBuildingLineFeatures = arrayBuildingFeatures;
			results.getBuildingLineFeatures = qryBuildingFeatures;
			results.getAllBuildingLineFeatures = qryAllBuildingFeatures;

			// Get the values for the Attributes that can potentially contain multiple values
			var listMultiDropDowns = "ArchitecturalStyle,Ceiling,SizeShape,Quality";
			//var listMultiDropDowns = "Ceiling";
			var arrayMultiDropDowns = ListToArray(listMultiDropDowns);

			for ( a = 1; a lte arrayLen(arrayMultiDropDowns); a++ ) {
				// Stored Proc to retrieve the Building Attribute Drop Downs

				results["getBuildingAttributeDropDowns_" & arrayMultiDropDowns[a]]
										= getBuildingAttributes(
																buildingID = results.getBuldingLineInfo.buildingID,
																codeType = arrayMultiDropDowns[a] );

				// Need to set the add the selected option to new column in the master buildinng line record for ease of use updating
				// changes on the building screen.
				if (results["getBuildingAttributeDropDowns_" & arrayMultiDropDowns[a]].recordCount) {
					for ( k = 1; k lte results["getBuildingAttributeDropDowns_" & arrayMultiDropDowns[a]].recordCount; k++ ) {
						if ( results["getBuildingAttributeDropDowns_" & arrayMultiDropDowns[a]]["selected"][k] eq 1 ) {
							arrayAttributeDataValue = ListToArray(results["getBuildingAttributeDropDowns_" & arrayMultiDropDowns[a]]["buildingCodeID"][k]);
							arrayAttributeDataCodeID = ListToArray(results["getBuildingAttributeDropDowns_" & arrayMultiDropDowns[a]]["buildingCodeJoinID"][k]);
						}
					}

				} else {
					// set the column value to an empty value
					arrayAttributeDataValue = ListToArray("");
					arrayAttributeDataCodeID =  ListToArray("");

				}

				// Add the Data value to the building line item
				queryAddColumn(results.getBuldingLineInfo, arrayMultiDropDowns[a], "Varchar", arrayAttributeDataValue);
				// Add the attribute join ID value to the building line item
				queryAddColumn(results.getBuldingLineInfo, arrayMultiDropDowns[a] & "_ID", "Varchar", arrayAttributeDataCodeID);

			}


			// Get the values for the Attributes that are single selection
			var listDropDowns = "Occupancy,Class,Condition";
			var arrayDropDowns = ListToArray(listDropDowns);
			var strColumnName = "";

			for ( a = 1; a lte arrayLen(arrayDropDowns); a++ ) {

				strColumnName = a;
				// Stored Proc to retrieve the Building Attribute Drop Downs
				results["getBuildingAttributeDropDowns_" & arrayDropDowns[a]]
										= getBuildingAttributeDropDowns(
																		taxYear = arguments.persistentCriteria.taxYear,
																		modelNumber = results.getBuldingLineInfo.modelNumber,
																		codeType = arrayDropDowns[a] );


			}


			// Stored Proc to retrieve the Building custom Attributes that are setup for this Building Line
			results.getBuildingCustomAttributesAssigned = getBuildingCustomAttributesAssigned( buildingID = results.getBuldingLineInfo.buildingID );

			// Call function to retrieve the list of Neighborhoods
			results.getNeighborhoods = getNeighborhoods( taxYear = arguments.persistentCriteria.taxYear );

			// Call function to retrieve the list of Special Conditions
			results.getSpecialConditions = getSpecialConditions();

			// Call function to retrieve the list of Primary Use Codes
			results.getPrimaryUse = getPrimaryUse();

			// Call function to retrieve the list of Primary Use Codes
			results.getBuildingUseCodes = getBuildingUseCodes( taxYear = arguments.persistentCriteria.taxYear );

			// Call function to retrieve the list of Primary Use Codes
			results.getBuildingTypes = getBuildingTypes();

		}
		results.error = false;
		results.message = "Features data retrieved successfully.";

		return results;
	}


	function getBuildingFeaturesData( required string buildingID ) {

		// list order is based on front end display order
		var listFeatures = "ExteriorWall,Frame,Foundation,RoofStructure,RoofCover,Floor,InteriorWall,Heat,AirConditioning,Kitchen,Windows";
		var arrayFeatures = ListToArray(listFeatures);
		var results = {};
		var strQueryName = "";
		 //Create empty recordset myTeam
		var qryBuildingFeatures = QueryNew("buildingID,mode,modalsortingorder,buildingCodeJoinID,feature,buildingCode,
											buildingCodeID,buildingCodeDescription,percentageFactor",
											"integer,varchar,integer,integer,varchar,integer,integer,varchar,integer");

		// Loop thru list of features
		for ( j = 1; j lte arrayLen(arrayFeatures); j++ ) {
			// Runs stored proc to get attributes
			strQueryName = arrayFeatures[j];
			qryBuildingLineFeatures = getBuildingAttributes( buildingID = arguments.buildingID, codeType = arrayFeatures[j] );
			
			// Create a copy of the RS that we can use in the dropdowns
			results["qry" & strQueryName] = qryBuildingLineFeatures;
			//writeDump(qryBuildingLineFeatures);
			for ( k = 1; k lte qryBuildingLineFeatures.recordcount; k++ ) {
				if ( qryBuildingLineFeatures["selected"][k] eq 1 && K < 3) {

					queryAddRow(qryBuildingFeatures,
								[{
									buildingID = arguments.buildingID,
									mode = 'none',
									modalsortingorder = 0,
									buildingCodeJoinID = qryBuildingLineFeatures["buildingCodeJoinID"][k],
									feature = arrayFeatures[j],
									buildingCode = qryBuildingLineFeatures["buildingCode"][k],
									buildingCodeID = qryBuildingLineFeatures["buildingCodeID"][k],
									buildingCodeDescription = qryBuildingLineFeatures["buildingCodeDescription"][k],
									percentageFactor = qryBuildingLineFeatures["percentageFactor"][k]
								} ]);
				}
			}

			results.getBuildingLineFeatures = qryBuildingFeatures;
		}

		return results;

	}


	function getAllBuildingAddinsData( required string buildingID, required string taxYear ) {


		var results = {};

		// Stored Proc to retrieve the Building custom Attributes
		//results.getBuildingAddIns = getBuildingAddIns( buildingID = arguments.buildingID );


		// Stored Proc to retrieve the Building custom Attributes
		results.getBuildingCustomAttributeList = getBuildingCustomAttributeList( taxYear = arguments.taxYear);

		// Check to see if there were any Building custom Attributes returned
		if ( results.getBuildingCustomAttributeList.recordcount > 0 ) {

			// Loop thru Building custom Attributes query
			for ( i = 1; i lte results.getBuildingCustomAttributeList.recordcount; i++ ) {

					// Runs stored proc to get attributes
					strCustomBuildingCode = results.getBuildingCustomAttributeList["codeType"][i];
					strCustomBuildingQueryName  = Replace(strCustomBuildingCode, "-", "", "ALL");

					//results.getBuildingCustomAttributeDropDowns["query"][i] = getBuildingCustomAttributeDropDowns(
					//												taxYear = arguments.persistentCriteria.taxYear,
					//												modelNumber = results.getBuldingLineInfo.modelNumber,
					//												codeType = strCustomBuildingCode );

					results["getBuildingCustomAttributes_" & strCustomBuildingQueryName]
										= getBuildingCustomAttributes(
															buildingID = arguments.buildingID,
															codeType = strCustomBuildingCode );
			}

		}

		return results;

	}


	function getBuildingTotalsData( required string buildingID ) {


		var results = {};

		// Stored Proc to retrieve the Building Totals
		results.getBuildingValueBreakdown = buildingDAO.getBuildingValueBreakdown( buildingID = arguments.buildingID );


		return results;

	}


	function getBuildingElementsTotalData(
										required number buildingID,
										required string valuationMethod,
										required number depreciationYear,
										required string mode ) {


		var results = {};

		// Stored Proc to retrieve the Building Structural Elements
		var getBuildingElementsTotalData = getBuildingStructuralElementsData( buildingID = arguments.buildingID );

		// Stored Proc to retrieve the Building Structural Elements
		var getAdditionalBuildingElementsTotalData = getAdditionalBuildingStructuralElementsData(
																								buildingID = arguments.buildingID,
																								valuationMethod = arguments.valuationMethod,
																								depreciationYear = arguments.depreciationYear,
																								mode = arguments.mode );

		// Business Rule -- Filter out Additional elements where the AdjustmentType value is NULL
		getAdditionalBuildingElementsTotalData = QueryHelper.filterNull(
															qry = getAdditionalBuildingElementsTotalData,
															field = 'AdjustmentType',
															null = 'NOT NULL' );

		// Combine both Building Elements Queries
		var qryCombinedElementsData = QueryHelper.doQueryAppend(
																qryFrom = getBuildingElementsTotalData,
																qryTo = getAdditionalBuildingElementsTotalData );

/*
		getAdditionalBuildingElementsTotalData = QueryHelper.filterQuery(
															qry = qryCombinedElementsData,
															field = 'Adjustment',
															value = '1',
															cfsqltype = 'int',
															list = 'NOT IN' );
*/

		// Sort the combined record set
		results.qryCombinedElementsData = QueryHelper.sortQuery(
														qry  = qryCombinedElementsData,
														sortBy = 'codeType' );
		//writeDump( qryCombinedElementsData ); abort;
		// Stored Proc to retrieve the Building Add-Ins Data
		results.qryBuildingAddInsData = getBuildingAddInsData( buildingID = arguments.buildingID );


		return results;

	}


	function getBuildingStructuralElementsData( required string buildingID ) {
		return buildingDAO.getBuildingStructuralElementsData( buildingID = arguments.buildingID );
	}


	function getAdditionalBuildingStructuralElementsData(
														required number buildingID,
														required string valuationMethod,
														required number depreciationYear,
														required string mode ) {

		return buildingDAO.getAdditionalBuildingStructuralElementsData(
																	buildingID = arguments.buildingID,
																	valuationMethod = arguments.valuationMethod,
																	depreciationYear = arguments.depreciationYear,
																	mode = arguments.mode );
	}


	function getBuildingAddInsData( required string buildingID ) {
		return buildingDAO.getBuildingAddInsData( buildingID = arguments.buildingID );
	}


	function getBuldingLineInfo( required struct persistentCriteria ) {
		return buildingDAO.getBuldingLineInfo( persistentCriteria = arguments.persistentCriteria );
	}


	function getBuildingAttributes( required string buildingID, required string codeType ) {
		return buildingDAO.getBuildingAttributes( buildingID = arguments.buildingID, codeType = arguments.codeType );
	}


	function getBuildingCustomAttributesAssigned( required string buildingID ) {
		return buildingDAO.getBuildingCustomAttributesAssigned( buildingID = arguments.buildingID );
	}


	function getBuildingCustomAttributeList( required string taxYear ) {
		return buildingDAO.getBuildingCustomAttributeList( taxYear = arguments.taxYear );
	}


	function getBuildingCustomAttributeDropDowns( required number taxYear, required number modelNumber, required string codeType) {

		return buildingDAO.getBuildingCustomAttributeDropDowns(
																taxYear = arguments.taxYear,
																modelNumber = arguments.modelNumber,
																codeType = arguments.codeType);
	}


	function getBuildingCustomAttributes( required string buildingID, required string codeType ) {
		return buildingDAO.getBuildingCustomAttributes( buildingID = arguments.buildingID, codeType = arguments.codeType );
	}


	function getBuildingAttributeDropDowns( required number taxYear, required number modelNumber, required string codeType) {

		return buildingDAO.getBuildingAttributeDropDowns(
															taxYear = arguments.taxYear,
															modelNumber = arguments.modelNumber,
															codeType = arguments.codeType);
	}

	function getBuildingAddIns( required number buildingID) {

		return buildingDAO.getBuildingAddIns( buildingID = arguments.buildingID );
	}


	/**
     * getNeighborhoods method for Getting the List of Neighborhoods for the county
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     */
	function getNeighborhoods( required string taxYear ) {
		// Passing the tax year as an empty string since that returns a different record set -- arguments.taxYear
		return dropdownsDAO.getNeighborhoods( taxYear = arguments.taxYear );
	}

	/**
     * getNeighborhoods method for Getting the List of Neighborhoods for the county
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     */
	function getBuildingUseCodes( required string taxYear ) {
		// Passing the tax year as an empty string since that returns a different record set -- arguments.taxYear
		return buildingDAO.getBuildingUseCodes( taxYear = arguments.taxYear );
	}


	function getSpecialConditions() {
		return dropdownsDAO.getSpecialConditionCodes();
	}

	function getPrimaryUse() {
		return dropdownsDAO.getPrimaryUse();
	}


	function getBuildingTypes() {
		return dropdownsDAO.getBuildingTypes();
	}


	function setBuildingDetailsInfo( required struct formData, required number baseYear ) {

		var results = {};
		// List of Attributes that we need to cycle trhough and update
		var listAttributeUpdates = "ArchitecturalStyle,Ceiling,SizeShape,Quality";
		var arrayAttributes = ListToArray(listAttributeUpdates);
		var strUpdateMode = "";

		//writeDump(formData); //abort;

		<!--- Convert the data packet --->
		cfwddx(action="WDDX2CFML", input=arguments.formData.buildingData, output="arguments.formData.qryBuildingData");
		cfwddx(action="WDDX2CFML", input=arguments.formData.buildingFeaturesData, output="arguments.formData.qryBuildingFeaturesData");
		cfwddx(action="WDDX2CFML", input=arguments.formData.buildingAddInsData, output="arguments.formData.qryBuildingAddInsData");

		//writeDump(arguments.formData.qryBuildingData);
		//writeDump(arguments.formData.qryBuildingFeaturesData);

		// Set the line itme number that we are trying to modify in the resutls structure
		results.itemNumber = arguments.formData.qryBuildingData.itemNumber;

		// Update the main Building Features
		results.stFeaturesResults = buildingDAO.setBuildingFeaturesInfo(
											formData1 = arguments.formData.qryBuildingFeaturesData );


		// Loop through the attributes that we need to update individually
		for ( j = 1; j lte arrayLen(arrayAttributes); j++ ) {
		// Runs stored proc to get attributes

			//writeOutput("j = " & arrayAttributes[j] & "<br>");
			//writeDump(arguments.formData.qryBuildingData[arrayAttributes[j]]);
			//writeDump(arguments.formData.qryBuildingData[arrayAttributes[j]&"_ID"]);
			//writeOutput("<P>");

			if (arguments.formData.qryBuildingData[arrayAttributes[j] & "_ID"] != "" ) {
				strUpdateMode = "Edit";
			} else {
				strUpdateMode = "Insert";
			}

			// Check to see if we need to this Building Attribute
			if ( strUpdateMode != "none") {
				// Stored Proc to Update the Building Attribute
				results["setBuildingAttributes_" & j]
					=  setBuildingAttributeInfo(
									buildingCodeJoinID = Val(arguments.formData.qryBuildingData[arrayAttributes[j] & "_ID"]),
									buildingID = Val(arguments.formData.qryBuildingData.buildingID),
	 								buildingCodeID = Val(arguments.formData.qryBuildingData[arrayAttributes[j]]),
	 								percentageFactor = 100,
									mode = strUpdateMode
									);

				//writeDump(results.setBuildingAttributes);
			}

		}

		//writeDump(arguments.formData.qryBuildingAddInsData);
		// Loop through the Building Custom Attributes that we need to update individually

		if ( arguments.formData.qryBuildingAddInsData.recordcount > 0 ) {
			//writeDump(arguments.formData.qryBuildingAddInsData);
			// Loop thru Add Ins query
			for ( i = 1; i lte arguments.formData.qryBuildingAddInsData.recordcount; i++ ) {

				strUpdateMode = arguments.formData.qryBuildingAddInsData["mode"][i];

				/*
				if (arguments.formData.qryBuildingAddInsData["mode"][i]) {
					strUpdateMode = "Edit";
				} else {
					strUpdateMode = "Insert";
				}
				*/

				WriteOutput(
					i & " | " &
					strUpdateMode & " | " &
					arguments.formData.qryBuildingAddInsData["buildingid"][i] & " | " &
					arguments.formData.qryBuildingAddInsData["buildingCustomCodeDescription"][i] & " | " &
					"<br />"
				);

				// Check to see if we need to this Building Attribute
				if ( strUpdateMode != "none") {

					// Stored Proc to Update the Building Attribute
					//results["getBuildingAttributeDropDowns_" & arrayMultiDropDowns[a]]
					results["setBuildingCustomAttributeInfo_" & i]
						=  setBuildingCustomAttributeInfo(
									buildingCustomCodeJoinID = Val(arguments.formData.qryBuildingAddInsData["buildingCustomCodeJoinID"][i]),
									buildingID = Val(arguments.formData.qryBuildingAddInsData["buildingid"][i]),
	 								buildingCustomCodeID = Val(arguments.formData.qryBuildingAddInsData["buildingCustomCodeID"][i]),
									mode = strUpdateMode
									);
				}

			}
		}


		// Update the main Building Information
		//		- This should be the last call made that updates the building information since this runs the final
		// 		- calculations on the building totals.
		results.stBuildingResults = buildingDAO.setBuildingDetailsInfo(
											formData1 = arguments.formData.qryBuildingData,
											baseYear = arguments.baseYear );


		results.cacheClear = persistentDAO.clearPersistentCache (
											parcelID = arguments.formData.qryBuildingData.parcelID,
											taxYear = arguments.formData.qryBuildingData.taxYear );

		//writeOutput("<P><br>"); writeDump(results); abort;

		return results;
	}


	function setBuildingAttributeInfo(
									required string buildingCodeJoinID,
									required string buildingID,
 									required string buildingCodeID,
 									required string percentageFactor,
									required string mode ) {


		return buildingDAO.setBuildingAttributeInfo(
											buildingCodeJoinID = arguments.buildingCodeJoinID,
											buildingID = arguments.buildingID,
											buildingCodeID = arguments.buildingCodeID,
											percentageFactor = arguments.percentageFactor,
											mode = arguments.mode );

	}


	function setBuildingCustomAttributeInfo(
									required string buildingCustomCodeJoinID,
									required string buildingID,
 									required string buildingCustomCodeID,
									required string mode ) {


		return buildingDAO.setBuildingCustomAttributeInfo(
											buildingCustomCodeJoinID = arguments.buildingCustomCodeJoinID,
											buildingID = arguments.buildingID,
											buildingCustomCodeID = arguments.buildingCustomCodeID,
											mode = arguments.mode );


	}

	function getBuildingAdjustments() {
		return buildingDAO.getBuildingAdjustments();
	}
}