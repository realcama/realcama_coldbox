/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" 	inject="coldbox:datasource:ws_realcama_nassau";
	property name="log" 	inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	buildingDAO function init(){

		return this;
	}


	// Stored Proc to retrieve building line info
	query function getBuldingLineInfo(required struct persistentCriteria){

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingInfo (:parcelID, :taxYear, :propType)");
			q.addParam(name="ParcelID",	value=arguments.persistentCriteria.parcelID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.ParcelID)) ? false : true);
			q.addParam(name="taxYear",	value=arguments.persistentCriteria.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.persistentCriteria.taxYear)) ? false : true);
			q.addParam(name="propType",	value="R", 	cfsqltype="cf_sql_varchar");

		return q.execute().getResult();
	}


	// Stored proc to retrieve building line feature info
	query function getBuildingAttributes( required string buildingID, required string codeType ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingAttributes (:buildingID, :codeType)");
			q.addParam(name="buildingID",	value=arguments.buildingID, cfsqltype="cf_sql_varchar", null=len(trim(arguments.buildingID)) ? false : true);
			q.addParam(name="codeType",		value=arguments.codeType,	cfsqltype="cf_sql_varchar", null=len(trim(arguments.codeType)) ? false : true);
		return q.execute().getResult();
	}

	// Stored Proc to retrieve Building Custom Attributes List
	query function getBuildingCustomAttributeList( required string taxYear ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingCustomAttributeList (:taxYear)");
			q.addParam(name="taxYear",	value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	// Stored proc to retrieve Building Custom Attributes that are setup for this Building Line
	query function getBuildingCustomAttributesAssigned( required string buildingID ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingCustomAttributesAssigned (:p_buildingID )");
			q.addParam(name="p_buildingID",	value=arguments.buildingID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.buildingID)) ? false : true);

		return q.execute().getResult();
	}

	// Stored proc to retrieve Building Custom Attributes List
	query function getBuildingCustomAttributeDropDowns(required number taxYear, required number modelNumber, required string codeType) {

		//writeDump(arguments); abort;

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingCustomAttributeDropDowns (:p_taxYear, :p_modelnumber, :p_codetype)");
			q.addParam(name="p_taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_integer", 	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="p_modelnumber",	value=arguments.modelNumber,	cfsqltype="cf_sql_integer", 	null="No");
			q.addParam(name="p_codetype",		value=arguments.codeType, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.codeType)) ? false : true);

		return q.execute().getResult();

	}

	// Stored proc to retrieve building custom attributes
	query function getBuildingCustomAttributes(required string buildingID, required string codeType) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingCustomAttributes (:buildingID, :codeType)");
			q.addParam(name="buildingID",	value=arguments.buildingID,		cfsqltype="cf_sql_varchar", null=len(trim(arguments.buildingID)) ? false : true);
			q.addParam(name="codeType",		value=arguments.codeType,		cfsqltype="cf_sql_varchar", null=len(trim(arguments.codeType)) ? false : true);

		return q.execute().getResult();
	}

	// Stored proc to retrieve Building Add-Ins
	query function getBuildingAddIns(required number buildingID) {

		//writeDump(arguments); abort;

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingAddIns (:p_buildingID)");
			q.addParam(name="p_buildingID",		value=arguments.buildingID,		cfsqltype="cf_sql_integer", 	null="No");

		return q.execute().getResult();

	}

	// Stored proc to retrieve Building AAttribute Drop Down Values
	query function getBuildingAttributeDropDowns(required number taxYear, required number modelNumber, required string codeType) {

		//writeDump(arguments); abort;

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingAttributeDropDowns (:p_taxYear, :p_modelnumber, :p_codetype)");
			q.addParam(name="p_taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_integer", 	null=len(trim(arguments.taxYear)) ? false : true);
			q.addParam(name="p_modelnumber",	value=arguments.modelNumber,	cfsqltype="cf_sql_integer", 	null="No");
			q.addParam(name="p_codetype",		value=arguments.codeType, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.codeType)) ? false : true);


		return q.execute().getResult();
	}

	/**
     * getNeighborhoods method for Getting the List of Neighborhoods for the county
     * @taxYear.hint The selected tax year that we use to retrieve the data for
     */
	function getBuildingUseCodes( required string taxYear ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetBuildingUseCodes (:p_taxYear)");
			q.addParam(name="p_taxYear",	value=arguments.taxYear,	cfsqltype="cf_sql_integer",		null=len(trim(arguments.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	// Stored proc to retrieve the Building Values Breakdown for the requested Building Line
	query function getBuildingValueBreakdown( required string buildingID ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_getBuildingValueBreakdown (:p_buildingID )");
			q.addParam(name="p_buildingID",		value=arguments.buildingID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.buildingID)) ? false : true);

		return q.execute().getResult();
	}


	// Stored proc to retrieve the Building Structural Elements Breakdown for the requested Building Line
	query function getBuildingStructuralElementsData( required string buildingID ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_CalcBuildingPointsDetails (:p_buildingID )");
			q.addParam(name="p_buildingID",		value=arguments.buildingID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.buildingID)) ? false : true);

		return q.execute().getResult();
	}


	// Stored proc to retrieve the Building Add-Ins Breakdown for the requested Building Line
	query function getBuildingAddInsData( required string buildingID ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_CalcBuildingAddOnPointsDetails (:p_buildingID )");
			q.addParam(name="p_buildingID",		value=arguments.buildingID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.buildingID)) ? false : true);

		return q.execute().getResult();
	}

	// Stored proc to retrieve the Additional Building Structural Elements Breakdown for the requested Building Line
	query function getAdditionalBuildingStructuralElementsData(
													required number buildingID,
													required string valuationMethod,
													required number depreciationYear,
													required string mode ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_CalcBuildingValueBreakdown (:p_buildingID, :p_ValueMethod, :p_baseDepreciationYearProperty, :p_mode)");
			q.addParam(name="p_buildingID",						value=arguments.buildingID, 		cfsqltype="cf_sql_integer", 	null=len(trim(arguments.buildingID)) ? false : true);
			q.addParam(name="p_ValueMethod",					value=arguments.valuationMethod, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.valuationMethod)) ? false : true);
			q.addParam(name="p_baseDepreciationYearProperty",	value=arguments.depreciationYear,	cfsqltype="cf_sql_integer", 	null="No");
			q.addParam(name="p_mode",							value=arguments.mode, 				cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.mode)) ? false : true);

		return q.execute().getResult();
	}


	// Stored proc to retrieve the Building Structural Elements Breakdown for the requested Building Line
	query function getBuildingStructuralElementsData( required string buildingID ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_CalcBuildingPointsDetails (:p_buildingID )");
			q.addParam(name="p_buildingID",		value=arguments.buildingID, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.buildingID)) ? false : true);

		return q.execute().getResult();
	}


	function setBuildingDetailsInfo( required query formData1, required number baseYear ) {
		/*, required query formData2 */
		var result = {status = 200, message="Start", locus = "setBuildingInfo"};
		var stQueryResult = {};

		//writeDump("#arguments.formData1#");
		//writeDump("#arguments.formData2#");
		//abort;

		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_SaveBuildingInfo (
				:p_buildingID, :p_buildingLabel, :p_parcelID, :p_taxYear, :p_propertyType, :p_itemNumber,
				:p_sequenceNumber, :p_modelNumber, :p_buildingUseCode, :p_neighborhoodCode, :p_numberbedrooms,
				:p_numberBaths, :p_numberPlumbingFixtures, :p_numberRooms, :p_numberStories, :p_numberUnits,
				:p_actualYearBuilt, :p_effectiveYearBuilt, :p_classCode, :p_conditionCode, :p_occupancyCode,
				:p_roomHeight, :p_perimeter, :p_commonWallPct, :p_economicobsolescencepct, :p_functionalObsolescencePct,
				:p_specialConditionCode, :p_specialConditionPct, :p_amendmentEligibleFlag, :p_newConstructionFlag,
				:p_replacementFlag, :p_baseYear, :p_mode
			)");

			q.clearParams();

			q.addParam(cfsqltype="cf_sql_integer",		name="p_buildingID",				value=arguments.formData1.buildingID);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_buildingLabel",				value=arguments.formData1.buildingLabel);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_parcelID",					value=Trim(arguments.formData1.parcelID));
			q.addParam(cfsqltype="cf_sql_integer",		name="p_taxYear",					value=arguments.formData1.taxYear);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_propertyType",				value=arguments.formData1.propertyType);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_itemNumber",				value=arguments.formData1.itemNumber);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_sequenceNumber",			value=arguments.formData1.sequenceNumber);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_modelNumber",				value=arguments.formData1.modelNumber);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_buildingUseCode",			value=arguments.formData1.buildingUseCode);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_neighborhoodCode",			value=arguments.formData1.neighborhoodCode);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_numberbedrooms",			value=arguments.formData1.numberBedrooms);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_numberBaths",				value=arguments.formData1.numberBaths);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_numberPlumbingFixtures",	value=arguments.formData1.numberPlumbingFixtures);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_numberRooms",				value=arguments.formData1.numberRooms);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_numberStories",				value=arguments.formData1.numberStories);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_numberUnits",				value=arguments.formData1.numberUnits);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_actualYearBuilt",			value=arguments.formData1.actualYearBuilt);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_effectiveYearBuilt",		value=arguments.formData1.effectiveYearBuilt);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_classCode",					value=arguments.formData1.classCode);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_conditionCode",				value=arguments.formData1.conditionCode);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_occupancyCode",				value=arguments.formData1.occupancyCode);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_roomHeight",				value=arguments.formData1.roomHeight);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_perimeter",					value=arguments.formData1.perimeter);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_commonWallPct",				value=arguments.formData1.commonWallPct);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_economicobsolescencepct",	value=arguments.formData1.economicobsolescencepct);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_functionalObsolescencePct",	value=arguments.formData1.functionalObsolescencePct);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_specialConditionCode",		value=arguments.formData1.specialConditionCode);
			q.addParam(cfsqltype="cf_sql_decimal",		name="p_specialConditionPct",		value=arguments.formData1.specialConditionPct);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_amendmentEligibleFlag",		value=arguments.formData1.amendmentEligibleFlag);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_newConstructionFlag",		value=arguments.formData1.newConstructionFlag);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_replacementFlag",			value=arguments.formData1.replacementFlag);
			q.addParam(cfsqltype="cf_sql_integer",		name="p_baseYear",					value=arguments.baseYear);
			q.addParam(cfsqltype="cf_sql_varchar",		name="p_mode",						value=arguments.formData1.mode);


			stQueryResult = q.execute().getPrefix();

			//writeDump(stQueryResult); abort;
			result.parceLID = arguments.formData1.parcelID;
			result.taxYear = arguments.formData1.taxYear;

			if(stQueryResult.recordcount != 1) {
				result.status = -1;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};

		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}

		return result;
	}


	function setBuildingFeaturesInfo( required query formData1 ) {
		/*, required query formData2 */
		var result = {status = 200, message="Start", locus = "setBuildingFeaturesInfo"};
		var iRow = 1;
		var stQueryResult = {};

		//writeDump("#arguments.formData1#");

 		for ( iRow = 1 ;  iRow LTE arguments.formData1.RecordCount ;  iRow = (iRow + 1)) {

			if (arguments.formData1[ "mode" ][ iRow ] != "none") {


				try{
					var q = new Query(datasource="#dsn.name#",sql="call p_SaveBuildingAttributes (
						:p_buildingCodeJoinID, :p_buildingID, :p_buildingCodeID, :p_percentageFactor, :p_mode
					)");

/*
					WriteOutput(
						arguments.formData1[ "buildingcodejoinid" ][ iRow ] & " | " &
						arguments.formData1[ "buildingid" ][ iRow ] & " | " &
						arguments.formData1[ "buildingcodeID" ][ iRow ] & " | " &
						arguments.formData1[ "percentageFactor" ][ iRow ] & " | " &
						arguments.formData1[ "mode" ][ iRow ] & " | " &
						"<br />"
					);
*/
					q.clearParams();
					q.addParam(name="p_buildingCodeJoinID",		value=arguments.formData1[ "buildingCodeJoinID" ][ iRow ], 	cfsqltype="cf_sql_integer",		null=len(trim(arguments.formData1[ "buildingCodeJoinID" ][ iRow ])) ? false : true);
					q.addParam(name="p_buildingID",				value=arguments.formData1[ "buildingID" ][ iRow ], 			cfsqltype="cf_sql_integer");
					q.addParam(name="p_buildingCodeID",			value=arguments.formData1[ "buildingCodeID" ][ iRow ], 		cfsqltype="cf_sql_integer");
					q.addParam(name="p_percentageFactor",		value=arguments.formData1[ "percentageFactor" ][ iRow ], 	cfsqltype="cf_sql_integer");
					q.addParam(name="p_mode",					value=arguments.formData1[ "mode" ][ iRow ],				cfsqltype="cf_sql_varchar");

					stQueryResult = q.execute().getPrefix();

					//writeDump(stQueryResult);
					//result.parceLID = arguments.formData1.parcelID;
					//result.taxYear = arguments.formData1.taxYear;

					if(stQueryResult.recordcount != 1) {
						result.status = -1;
						result.message = stQueryResult;
					} else {
						result.status=200;
						result.message = "Success";
					};

				} catch(any){
					result.status = -2;
					result.message = cfcatch;
				}

			}		// if (arguments.formData1[ "mode" ][ iRow ] == "Edit")

 		}		//	for ( iRow = 1 ;  iRow LTE arguments.formData1.RecordCount ;  iRow = (iRow + 1))

		return result;
	}


	function setBuildingAttributeInfo(
									required number buildingCodeJoinID,
									required number buildingID,
 									required number buildingCodeID,
 									required number percentageFactor,
									required string mode ) {

		var result = {status = 200, message="Start", locus = "setBuildingAttributeInfo"};
		var stQueryResult = {};

		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_SaveBuildingAttributes (
				:p_buildingCodeJoinID, :p_buildingID, :p_buildingCodeID, :p_percentageFactor, :p_mode
			)");

			/*
			WriteOutput(
				arguments.buildingcodejoinid & " | " &
				arguments.buildingid & " | " &
				arguments.buildingcodeID & " | " &
				arguments.percentageFactor & " | " &
				arguments.mode & " | " &
				"<br />"
			);
			*/

			q.clearParams();
			q.addParam(name="p_buildingCodeJoinID",		value=arguments.buildingCodeJoinID, 	cfsqltype="cf_sql_integer",		null=len(trim(arguments.buildingCodeJoinID)) ? false : true);
			q.addParam(name="p_buildingID",				value=arguments.buildingID, 			cfsqltype="cf_sql_integer");
			q.addParam(name="p_buildingCodeID",			value=arguments.buildingCodeID, 		cfsqltype="cf_sql_integer");
			q.addParam(name="p_percentageFactor",		value=arguments.percentageFactor, 		cfsqltype="cf_sql_integer");
			q.addParam(name="p_mode",					value=arguments.mode,					cfsqltype="cf_sql_varchar");

			stQueryResult = q.execute().getPrefix();

			//writeDump(stQueryResult);

			if(stQueryResult.recordcount != 1) {
				result.status = -1;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};

		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}

		return result;
	}


	function setBuildingCustomAttributeInfo(
									required string buildingCustomCodeJoinID,
									required string buildingID,
 									required string buildingCustomCodeID,
									required string mode ) {


		var result = {status = 200, message="Start", locus = "setBuildingCustomAttributeInfo"};
		var stQueryResult = {};

		try{
			var q = new Query(datasource="#dsn.name#",sql="call p_SaveBuildingCustomAttributes (
				:p_buildingCustomCodeJoinID, :p_buildingID, :p_buildingCustomCodeID, :p_mode
			)");

			/*
			WriteOutput(
				arguments.buildingCustomCodeJoinID & " | " &
				arguments.buildingid & " | " &
				arguments.buildingCustomCodeID & " | " &
				arguments.mode & " | " &
				"<br />"
			);
			*/

			q.clearParams();
			q.addParam(name="p_buildingCustomCodeJoinID",	value=arguments.buildingCustomCodeJoinID, 	cfsqltype="cf_sql_integer",		null=len(trim(arguments.buildingCustomCodeJoinID)) ? false : true);
			q.addParam(name="p_buildingID",					value=arguments.buildingID, 				cfsqltype="cf_sql_integer");
			q.addParam(name="p_buildingCustomCodeID",		value=arguments.buildingCustomCodeID, 		cfsqltype="cf_sql_integer");
			q.addParam(name="p_mode",						value=arguments.mode,						cfsqltype="cf_sql_varchar");

			stQueryResult = q.execute().getPrefix();

			//writeDump(stQueryResult);

			if(stQueryResult.recordcount != 1) {
				result.status = -1;
				result.message = stQueryResult;
			} else {
				result.status=200;
				result.message = "Success";
			};

		} catch(any){
			result.status = -2;
			result.message = cfcatch;
		}

		return result;
	}

	function getBuildingAdjustments() {
		var listFeatures = "ExteriorWall,Frame,Foundation,RoofStructure,RoofCover,Floor,InteriorWall,Heat,AirConditioning,Kitchen,Windows,Ceiling,Quality";
		var arrayFeatures = ListToArray(listFeatures);

		var qryBuildingFeatures = QueryNew( "Adjustment", "varchar" );
		for ( j = 1; j lte arrayLen(arrayFeatures); j++ ) {
			queryAddRow( qryBuildingFeatures, '1' );
			QuerySetCell( qryBuildingFeatures, "Adjustment", arrayFeatures[j] );
		}

		return qryBuildingFeatures;
	}
}
