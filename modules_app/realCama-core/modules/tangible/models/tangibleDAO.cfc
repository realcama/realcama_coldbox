/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" 	inject="coldbox:datasource:ws_realcama_nassau";
	property name="cache"	inject="cachebox:default";
	property name="log"		inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	tangibleDAO function init(){

		return this;
	}

	/**
     * getTangibleOwnerInfo method for retrieveing the Tangible Property Owner information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleOwnerInfo(required string accountNumber, required string parcelID, required string taxYear) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetTangibleOwnerInfo (:p_accountNumber, :p_parcelID, :p_taxYear)");
			q.addParam(name="p_accountNumber",	value=arguments.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.accountNumber)) ? false : true);
			q.addParam(name="p_parcelID",		value=arguments.parcelID, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.parcelID)) ? false : true);
			q.addParam(name="p_taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	/**
     * getTangibleLocationInfo method for retrieveing the Tangible Property Location information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleLocationInfo(required string accountNumber, required string parcelID, required string taxYear) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetTangibleLocationInfo (:p_accountNumber, :p_parcelID, :p_taxYear)");
			q.addParam(name="p_accountNumber",	value=arguments.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.accountNumber)) ? false : true);
			q.addParam(name="p_parcelID",		value=arguments.parcelID, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.parcelID)) ? false : true);
			q.addParam(name="p_taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	/**
     * getTangibleTaxInfo method for retrieveing the Tangible Property Tax information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleTaxInfo(required string accountNumber, required string parcelID, required string taxYear) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetTangibleTaxInfo (:p_accountNumber, :p_parcelID, :p_taxYear)");
			q.addParam(name="p_accountNumber",	value=arguments.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.accountNumber)) ? false : true);
			q.addParam(name="p_parcelID",		value=arguments.parcelID, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.parcelID)) ? false : true);
			q.addParam(name="p_taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	/**
     * getTangibleAssetInfo method for retrieveing the Tangible Property Asset information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleAssetInfo(required string accountNumber, required string parcelID, required string taxYear) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetTangibleAssetInfo (:p_accountNumber, :p_parcelID, :p_taxYear)");
			q.addParam(name="p_accountNumber",	value=arguments.accountNumber, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.accountNumber)) ? false : true);
			q.addParam(name="p_parcelID",		value=arguments.parcelID, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.parcelID)) ? false : true);
			q.addParam(name="p_taxYear",		value=arguments.taxYear, 		cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

		return q.execute().getResult();
	}


	/**
     * getTangibleUseCodes method for retrieveing the Tangible Property Use Codes
     * @taxYear.hint The selected Tax Year that we are retrieveing the Tangible Property Use Codes fpr
     */
	function getTangibleUseCodes(required string taxYear){
		var qGetTangibleUseCodes = [];
		var cacheKey = 'q-TangibleUseCodes';

		if( cache.lookup(cacheKey) ){
			qGetTangibleUseCodes = cache.get(cacheKey);
			log.info ( "qGetTangibleUseCodes is cached" );
		} else {
			var q = new Query(datasource="#dsn.name#",sql="call p_GetTangibleUseCodes (:taxYear)");
				q.addParam(name="taxYear",	value=arguments.taxYear, 	cfsqltype="cf_sql_varchar", 	null=len(trim(arguments.taxYear)) ? false : true);

			qGetTangibleUseCodes = q.execute().getResult();
			cache.set(cacheKey, qGetTangibleUseCodes, 30, 30);
			log.info ( "running qGetTangibleUseCodes query" );
		}

		return qGetTangibleUseCodes;
	}

}