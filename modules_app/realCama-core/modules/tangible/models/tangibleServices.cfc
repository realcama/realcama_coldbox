/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="tangibleDAO"		inject=model;
	property name="dropdownsDAO"	inject=model;


	/**
	 * Constructor
	 */
	tangibleServices function init(){

		return this;
	}


	/**
     * getPersistentInfoTPP method for retrieveing the Persistent Information of the Tangible Property
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getPersistentInfoTPP(required string accountNumber, required string taxYear) {

		return tangibleDAO.getPersistentInfoTPP(
								accountNumber = arguments.accountNumber,
								taxYear = arguments.taxYear);

	}


	/**
     * getAllTangibleOwnerData method for retrieveing all of the Tangible Property data queries that are used on the
	 *     	Tangible Owner Tab
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getAllTangibleOwnerData( required string accountNumber, required string parcelID, required string taxYear ) {
		var results = {};

		//writeDump(persistentCriteria);

		// ----- Stored Proc to retrieve the Tangible Owner Information ----->
		results.qGetTangibleOwnerInfo = getTangibleOwnerInfo(
											accountNumber = arguments.accountNumber,
											parcelID = arguments.parcelID,
											taxYear = arguments.taxYear
											);

		// ----- Stored Proc to retrieve the Tangible Location Information ----->
		results.qGetTangibleLocationInfo = getTangibleLocationInfo(
											accountNumber = arguments.accountNumber,
											parcelID = arguments.parcelID,
											taxYear = arguments.taxYear
											);

		// ----- Stored Proc to retrieve the Tangible Location Information ----->
		results.qGetTangibleTaxInfo = getTangibleTaxInfo(
											accountNumber = arguments.accountNumber,
											parcelID = arguments.parcelID,
											taxYear = arguments.taxYear
											);

		<!----- Stored Proc to retrieve Tax Districts ----->
		results.qGetTaxDistricts = getTaxDistricts( taxYear = arguments.taxYear );

		<!----- Stored Proc to retrieve the List of States ----->
		results.qGetStateList = getStateList();

		<!----- Stored Proc to retrieve the Tangible Use Codes ----->
		results.qGetTangibleUseCodes = getTangibleUseCodes(	taxYear = arguments.taxYear );


		results.error = false;
		results.message = "TPP Owner data retrieved successfully.";

		//writeDump(results);

		return results;
	}


	/**
     * getTangibleOwnerInfo method for retrieveing the Tangible Property Owner information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleOwnerInfo( required string accountNumber, required string parcelID, required string taxYear ) {

		return tangibleDAO.getTangibleOwnerInfo(
								accountNumber = arguments.accountNumber,
								parcelID = arguments.parcelID,
								taxYear = arguments.taxYear
								);
	}


	/**
     * getTangibleLocationInfo method for retrieveing the Tangible Property Location information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleLocationInfo( required string accountNumber, required string parcelID, required string taxYear ) {

		return tangibleDAO.getTangibleLocationInfo(
								accountNumber = arguments.accountNumber,
								parcelID = arguments.parcelID,
								taxYear = arguments.taxYear
								);
	}


	/**
     * getTangibleTaxInfo method for retrieveing the Tangible Property Tax information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleTaxInfo (required string accountNumber, required string parcelID, required string taxYear ) {

		return tangibleDAO.getTangibleTaxInfo(
								accountNumber = arguments.accountNumber,
								parcelID = arguments.parcelID,
								taxYear = arguments.taxYear
								);
	}


	/**
     * getTangibleAssetInfo method for retrieveing the Tangible Property Asset information
     * @accountNumber.hint The TPP account number of the Parcel that is being retrieved
     * @parcelID.hint The selected Parcel ID that is being retrieved
     * @taxYear.hint The selected Tax Year of the Parcel that is being retrieved
     */
	function getTangibleAssetInfo( required string accountNumber, required string parcelID, required string taxYear ) {

		return tangibleDAO.getTangibleAssetInfo(
								accountNumber = arguments.accountNumber,
								parcelID = arguments.parcelID,
								taxYear = arguments.taxYear
								);
	}


	/**
     * getTaxDistricts method for retrieveing the list of Tax Districts
     * @taxYear.hint The selected Tax Year that we are retrieveing the list of Tax Districts for
     */
	function getTaxDistricts( required string taxYear ) {

 		return dropdownsDAO.getTaxDistricts( taxYear = arguments.taxYear );
	}


	/**
     * getStateList method for retrieveing the list of US States
     */
 	function getStateList() {

 		return dropdownsDAO.getStatesList();
	}


	/**
     * getTangibleUseCodes method for retrieveing the Tangible Property Use Codes
     * @taxYear.hint The selected Tax Year that we are retrieveing the Tangible Property Use Codes fpr
     */
	function getTangibleUseCodes( required string taxYear ) {

		return tangibleDAO.getTangibleUseCodes( taxYear = arguments.taxYear );
	}

}