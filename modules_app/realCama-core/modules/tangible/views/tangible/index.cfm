
<cfoutput>
#addAsset('/includes/js/tangible.js')#

<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		<CFSET showTabOptions = "TPP">
		#renderView('_templates/miniNavigationBar')#
		Tangible - Owner / DBA
	</div>

	<div id="expander-holders" class="panel panel-default">
		#renderView('tangible/viewlets/_tangibleOwner')#
		#renderView('tangible/viewlets/_tangibleTaxInfo')#
		#renderView('tangible/viewlets/_tangibleLocation')#
		#renderView('tangible/viewlets/_tangibleContactInfo')#
	</div>
</div>
</cfoutput>
