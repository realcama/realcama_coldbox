<CFSET strHeader = "Tangible">
<cfset tab = "assets">
<CFSET showTabOptions = "tangable-asset">
<cfoutput>
<cfscript>
	iPageNumber = 0;
</cfscript>

#addAsset('/includes/js/tangible.js')#

#renderView('/_templates/audit-boxes-tpp')#

<div class="container-border container-spacer">
	<div class="table-header content-bar-label">
		#renderView('_templates/miniNavigationBar')#
		Tangible Property - Asset Detail
	</div>

	<div id="expander-holders" class="panel panel-default">
		<cfloop query="prc.qGetTangibleAssetInfo">
			<cfif prc.qGetTangibleAssetInfo.CurrentRow MOD rc.page_size EQ 1>
				<cfset iPageNumber = iPageNumber + 1>
				<div id="page-#iPageNumber#" <cfif rc.page neq iPageNumber>class="hide"</cfif>>
			</cfif>
			#renderView('tangible/viewlets/_assetlist')#
			<cfif prc.qGetTangibleAssetInfo.CurrentRow MOD rc.page_size EQ 0>
				</div>
			</cfif>
		</cfloop>
		<cfif prc.qGetTangibleAssetInfo.CurrentRow MOD rc.page_size NEQ 0>
			</div> <!--- This ensures the ' div id="page-#iPageNumber#" ' to be closed correctly --->
		</cfif>
	</div>
</div>

<script type="text/javascript">
	var totalPages = #ceiling(prc.qGetTangibleAssetInfo.RecordCount/rc.page_size)#;
	var iCurrentPage = 1;
</script>
#addAsset('/includes/js/header.js')#
</cfoutput>
