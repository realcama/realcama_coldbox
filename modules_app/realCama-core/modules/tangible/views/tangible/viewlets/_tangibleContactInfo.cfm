<cfoutput>
	<!--- owner information --->
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading4"  aria-expanded="false" >
		<table class="land-line-summary">
		<tr href="##collapse4" class="section-bar-expander" onclick="toggleChevronChild(4)" data-parent="heading1" data-toggle="collapse" aria-controls="collapse4">
			<td class="section-bar-label">Contact Information</td>
			<td width="5%" align="right" class="section-bar-label"><span id="chevron_4" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
		<div id="view_mode_item_4">
			<div class="row fix-bs-row">

				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Contact Person</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Phone Number</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Birth Dates</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Employer Names</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Employer Address</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Employer Phone Number</span><br />
					<span class="formatted-data-values"></span>
				</div>



				<div class="clearfix"></div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(4);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
		<div id="edit_mode_item_4" style="display:none;">
			<div class="row fix-bs-row">
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Contact Person</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Phone Number</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Birth Dates</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Employer Names</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Employer Address</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-4 databox">
					<span class="formatted-data-label">Employer Phone Number</span><br />
					<span class="formatted-data-values"></span>
				</div>

				<div class="clearfix"></div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(4);"><img src="/includes/images/v2/realCAMA_icon_view2.png" alt="View" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</cfoutput>