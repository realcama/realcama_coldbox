<cfoutput> <!---  query="prc.qGetTangibleAssetInfo" --->
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading1"  aria-expanded="false" >
		<table class="land-line-summary">
		<tr href="##collapse#prc.qGetTangibleAssetInfo.CurrentRow#" class="section-bar-expander" onclick="toggleChevronChild(#prc.qGetTangibleAssetInfo.CurrentRow#)" data-parent="heading#prc.qGetTangibleAssetInfo.CurrentRow#" data-toggle="collapse" aria-controls="collapse#prc.qGetTangibleAssetInfo.CurrentRow#">
			<td width="10%" class="section-bar-label">#prc.qGetTangibleAssetInfo.CurrentRow#</td>
			<td width="15%" class="section-bar-label">#prc.qGetTangibleAssetInfo.abstractCode#</td>
			<td width="15%" class="section-bar-label">#prc.qGetTangibleAssetInfo.abstractCodeDescription#</td>
			<td width="10%" class="section-bar-label">#prc.qGetTangibleAssetInfo.abstractYear#</td>
			<td width="15%" class="section-bar-label">$#NumberFormat(prc.qGetTangibleAssetInfo.originalCost,",")#</td>
			<td width="15%" class="section-bar-label">#prc.qGetTangibleAssetInfo.Description#</td>
			<td width="15%" class="section-bar-label">$#NumberFormat(prc.qGetTangibleAssetInfo.calculatedValue,",")#</td>
			<td width="5%" align="right" class="section-bar-label"><span id="chevron_1" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse#prc.qGetTangibleAssetInfo.CurrentRow#" class="panel-collapse collapse" role="tabpane#prc.qGetTangibleAssetInfo.CurrentRow#" aria-labelledby="heading#prc.qGetTangibleAssetInfo.CurrentRow#">
		<div id="view_mode_item_#prc.qGetTangibleAssetInfo.CurrentRow#">
			<div class="row fix-bs-row">
				<span class="formatted-data-values">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Appreciation</span> <br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.appreciationCode#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Depreciation</span> <br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.depreciationCode#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Life</span> <br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.life#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Obsolescence</span> <br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.obsolescence#</span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Year Added</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.addedDate#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Year Purchased</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.purchasedDate#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Leasee</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.leaseeName#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Lessor</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleAssetInfo.leasorName#</span>
				</div>

				<div class="clearfix"></div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(#prc.qGetTangibleAssetInfo.CurrentRow#);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
		<div id="edit_mode_item_#prc.qGetTangibleAssetInfo.CurrentRow#" style="display:none;">
			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Appreciation</span> <br />
					<input type="text" class="form-control" value="#prc.qGetTangibleAssetInfo.appreciationCode#" name="abstapr"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Depreciation</span> <br />
					<input type="text" class="form-control" value="#prc.qGetTangibleAssetInfo.depreciationCode#" name="abstdpr"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Life</span> <br />
					<input type="text" class="form-control" value="#prc.qGetTangibleAssetInfo.life#" name="abslif"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Obsolescence</span> <br />
					<input type="text" class="form-control" value="#prc.qGetTangibleAssetInfo.obsolescence#" name="abstobs"/>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Year Added</span><br />
					<input type="text" class="form-control datefield-yearonly-parcel" value="#prc.qGetTangibleAssetInfo.addedDate#" name="abadat"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Year Purchased</span><br />
					<input type="text" class="form-control datefield-yearonly-parcel" value="#prc.qGetTangibleAssetInfo.purchasedDate#" name="abpdat"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Leasee</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleAssetInfo.leaseeName#" name="ables"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Lessor</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleAssetInfo.leasorName#" name="ables"/>
				</div>

				<div class="clearfix"></div>
				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(#prc.qGetTangibleAssetInfo.CurrentRow#);"><img src="/includes/images/v2/realCAMA_icon_view2.png" alt="View" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</cfoutput>
