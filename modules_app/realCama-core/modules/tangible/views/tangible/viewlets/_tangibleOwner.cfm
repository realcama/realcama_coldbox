<cfoutput>
	<!--- owner information --->
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading1"  aria-expanded="false" >
		<table class="land-line-summary">
		<tr href="##collapse1" class="section-bar-expander" onclick="toggleChevronChild(1)" data-parent="heading1" data-toggle="collapse" aria-controls="collapse1">
			<td class="section-bar-label">Owner Information</td>
			<td width="5%" align="right" class="section-bar-label"><span id="chevron_1" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
		<div id="view_mode_item_1">
			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Name</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.OwnerName#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Secondary Name</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.alternateName#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">DBA</span><br />
					<span class="formatted-data-values"><!--- #prc.qGetTangibleOwnerInfo.ppaltnam# ---></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Filing Status</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street Number</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.Address1#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.Address2#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Mode</span> <br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.Address3#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Number</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">City</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.city#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">State</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.State#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.Zip#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip+4</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleOwnerInfo.Zip4#</span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>


		<div id="edit_mode_item_1" style="display:none;">
			<div class="row fix-bs-row" <cfif isdefined("search_button")>style="display:none;"</cfif> >

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Name</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.OwnerName#" name="ppname"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Secondary Name</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.alternateName#" name="opname"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">DBA</span><br />
					<input type="text" class="form-control" value="<!--- #prc.qGetTangibleOwnerInfo.ppaltnam# --->" name"ppaltnam"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Filing Status</span><br />
					<select data-tags="true" class="form-control select2-1piece-new" name="filingstatus">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<option value="Corporation" data-val="Corporation" data-desc="Corporation">Corporation</option>
						<option value="Sole Proprietor" data-val="Sole Proprietor" data-desc="Sole Proprietor">Sole Proprietor</option>
						<option value="S Corp" data-val="S Corp" data-desc="S Corp">S Corp</option>
						<option value="C Corp" data-val="C Corp" data-desc="C Corp">C Corp</option>
						<option value="LLC" data-val="LLC" data-desc="LLC">LLC</option>
						<option value="Owner" data-val="Owner" data-desc="Owner">Owner</option>
					</select>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street Number</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.Address1#" name="ppaddr1" />
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.Address2#" name="ppaddr2"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Mode</span> <br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.Address3#" name="ppaddr3"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Number</span><br />
					<input type="text" class="form-control" value="" name="number"/>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">City</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.city#" name="ppcity" />
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">State</span><br />
					<select data-tags="true" class="form-control select2-2piece-new" name="state">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.qGetStateList">
							<option value="#prc.qGetStateList.field_abbrev#" data-val="#prc.qGetStateList.field_abbrev#" data-desc="#prc.qGetStateList.field_option#" <cfif prc.qGetTangibleOwnerInfo.state EQ prc.qGetStateList.field_abbrev> selected</cfif>>#prc.qGetStateList.field_abbrev#: #prc.qGetStateList.field_option#</option>
						</cfloop>
					</select>

				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.zip#" name="ppzip5"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip+4</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleOwnerInfo.zip#" name="ppzip4"/>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(1);"><img src="/includes/images/v2/realCAMA_icon_view2.png" alt="View" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</cfoutput>