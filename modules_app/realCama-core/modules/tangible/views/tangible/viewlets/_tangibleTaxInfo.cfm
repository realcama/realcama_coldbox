<cfoutput>
	<CFSET iAmount = val(prc.qGetTangibleTaxInfo.ExemptionAmount1) + val(prc.qGetTangibleTaxInfo.ExemptionAmount2) + val(prc.qGetTangibleTaxInfo.ExemptionAmount3) + val(prc.qGetTangibleTaxInfo.ExemptionAmount4) + val(prc.qGetTangibleTaxInfo.ExemptionAmount5)>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading2"  aria-expanded="false" >
		<table class="land-line-summary">
		<tr href="##collapse2" class="section-bar-expander" onclick="toggleChevronChild(2)" data-parent="heading2" data-toggle="collapse" aria-controls="collapse2">
			<td class="section-bar-label">Tax Information</td>
			<td width="5%" align="right" class="section-bar-label"><span id="chevron_2" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
		<div id="view_mode_item_2">
			<div class="row fix-bs-row">

				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Tax District</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleTaxInfo.taxDistrictCode#</span>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">EIN / SSN</span><br />
					<span class="formatted-data-values">*****#Right(prc.qGetTangibleTaxInfo.TaxID1,4)#</span>
				</div>
				<div class="col-xs-8 databox">
					<span class="formatted-data-label">Primary Use / Description</span> <br />
					<span class="formatted-data-values">#prc.qGetTangibleTaxInfo.UseCode#: #prc.qGetTangibleTaxInfo.UseCodeDescription#</span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Exemption Code</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleTaxInfo.ExemptionCode1# #prc.qGetTangibleTaxInfo.ExemptionCode2# #prc.qGetTangibleTaxInfo.ExemptionCode3# #prc.qGetTangibleTaxInfo.ExemptionCode4# #prc.qGetTangibleTaxInfo.ExemptionCode5#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Exemption Amount</span><br />
					<span class="formatted-data-values">$#NumberFormat(iAmount,",")#</span>

				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Penalty Amount</span><br />
					<span class="formatted-data-values">#NumberFormat(prc.qGetTangibleTaxInfo.Penalty,",")#%</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Previous Value</span><br />
					<span class="formatted-data-values">$#NumberFormat(prc.qGetTangibleTaxInfo.SchoolTaxableValue,",")#</span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Alternate PIN Number</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleTaxInfo.ParcelDisplayID#</span>
				</div>
				<div class="col-xs-3 databox"></div>
				<div class="col-xs-3 databox"></div>
				<div class="col-xs-3 databox"></div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Business Category</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Business Started</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Business Ended</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-3 databox"></div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Fiscal Year End</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Last File Date</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Last Audit Date</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="col-xs-3 databox"></div>
				<div class="clearfix"></div>


				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(2);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
		<div id="edit_mode_item_2" style="display:none;">
			<div class="row fix-bs-row" <cfif isdefined("search_button")>style="display:none;"</cfif>>

				<div class="col-xs-2 databox">
					<span class="formatted-data-label">Tax District</span><br />
					<select name="pptxdt" id="pptxdt" size="1" class="form-control select2-2piece-new">
						<option value="" data-val="" data-desc="ALL">ALL</option>
						<cfloop query="prc.qGetTaxDistricts">
							<CFIF LEN(TRIM(prc.qGetTaxDistricts.txdtds)) NEQ 0>
								<CFSET strText = prc.qGetTaxDistricts.txdtds>
							<CFELSEIF LEN(TRIM(prc.qGetTaxDistricts.txdtcity)) NEQ 0>
								<CFSET strText = prc.qGetTaxDistricts.txdtcity>
							<CFELSE>
								<CFSET strText = "&nbsp;">
							</CFIF>
							<option <CFIF prc.qGetTangibleTaxInfo.taxDistrictCode EQ prc.qGetTaxDistricts.txdtcd>selected</CFIF> value="#prc.qGetTaxDistricts.txdtcd#" data-val="#prc.qGetTaxDistricts.txdtcd#" data-desc="#strText#">#prc.qGetTaxDistricts.txdtcd#: #strText#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-2 databox">
					<span class="formatted-data-label">EIN / SSN</span><br />
					<input type="text" class="form-control formatted-data-values" value="*****#Right(prc.qGetTangibleTaxInfo.TaxID1,4)#" name="pptaxid1"/>
				</div>
				<div class="col-xs-8 databox">
					<span class="formatted-data-label">Primary Use / Description</span> <br />
					<select name="pruse" id="pruse" size="1" class="form-control select2-2piece-tpp-use-new">
						<option value="" data-val="" data-desc="ALL">ALL</option>
						<cfloop query="prc.qGetTangibleUseCodes">
							<option <CFIF prc.qGetTangibleTaxInfo.UseCode EQ prc.qGetTangibleUseCodes.code>selected </CFIF> value="#prc.qGetTangibleUseCodes.code#" data-val="#prc.qGetTangibleUseCodes.code#" data-desc="#prc.qGetTangibleUseCodes.codeDescription#">#prc.qGetTangibleUseCodes.code#: #prc.qGetTangibleUseCodes.codeDescription#</option>
						</cfloop>
					</select>
				</div>

				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Exemption Code</span><br />
					<input type="text" class="form-control formatted-data-values" value="" name="tax_exemptioncode" />
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Exemption Amount</span><br />
					<input type="text" class="form-control formatted-data-values" value="" name="tax_exemptionamount"/>

				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Penalty Amount</span><br />
					<input type="text" class="form-control formatted-data-values" value="#prc.qGetTangibleTaxInfo.Penalty#" name="pppen"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Previous Value</span><br />
					<input type="text" class="form-control formatted-data-values" value="#prc.qGetTangibleTaxInfo.SchoolTaxableValue#" name="hifltaxbl1"/>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Alternate PIN Number</span><br />
					<input type="text" class="form-control formatted-data-values" value="#prc.qGetTangibleTaxInfo.ParcelDisplayID#" name="xpapin" />
				</div>
				<div class="col-xs-3 databox"></div>
				<div class="col-xs-3 databox"></div>
				<div class="col-xs-3 databox"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Business Category</span><br />
					<span class="formatted-data-values"><input type="text" class="form-control formatted-data-values" value="" name="businessCategory"/></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Business Started</span><br />
					<span class="formatted-data-values"><input type="text" class="form-control datefield formatted-data-values" value="" name="businessStarted"/></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Business Ended</span><br />
					<span class="formatted-data-values"><input type="text" class="form-control datefield formatted-data-values" value="" name="businessEnded"/></span>
				</div>
				<div class="col-xs-3 databox"></div>
				<div class="clearfix"></div>


				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Fiscal Year End</span><br />
					<span class="formatted-data-values"><input type="text" class="form-control datefield-yearonly" value="" name="fiscalYearEnd"/></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Last File Date</span><br />
					<span class="formatted-data-values"><input type="text" class="form-control datefield formatted-data-values" value="" name="lastFileDate"/></span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Last Audit Date</span><br />
					<span class="formatted-data-values"><input type="text" class="form-control datefield formatted-data-values" value="" name="lastAuditDate"/></span>
				</div>
				<div class="col-xs-3 databox"></div>
				<div class="clearfix"></div>


				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(2);"><img src="/includes/images/v2/realCAMA_icon_view2.png" alt="View" width="53" height="24" border="0"></a>
					</div>
				</div>

			</div>
		</div>
	</div>
</cfoutput>
