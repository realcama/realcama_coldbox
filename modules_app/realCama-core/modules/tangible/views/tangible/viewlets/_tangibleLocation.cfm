<cfoutput>
	<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading3"  aria-expanded="false" >
		<table class="land-line-summary">
		<tr href="##collapse3" class="section-bar-expander" onclick="toggleChevronChild(3)" data-parent="heading3" data-toggle="collapse" aria-controls="collapse3">
			<td class="section-bar-label">Location Address</td>
			<td width="5%" align="right" class="section-bar-label"><span id="chevron_3" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
		</tr>
		</table>
	</div>
	<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
		<div id="view_mode_item_3">
			<div class="pull-right">&nbsp;&nbsp;</div>
			<div class="clearfix"></div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street Number</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleLocationInfo.LocationHouse#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleLocationInfo.LocationStreet#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Mode</span><br />
					<span class="formatted-data-values">&nbsp;</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Number</span><br />
					<span class="formatted-data-values">&nbsp;</span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-3 databox">
					<span class="formatted-data-label">City</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleLocationInfo.locationCity#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">State</span><br />
					<span class="formatted-data-values">&nbsp;</span>

				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip</span><br />
					<span class="formatted-data-values">#prc.qGetTangibleLocationInfo.locationZip#</span>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip+4</span><br />
					<span class="formatted-data-values"></span>
				</div>
				<div class="clearfix"></div>

				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(3);"><img src="/includes/images/v2/realCAMA_icon_LineEdit.png" alt="Edit" width="53" height="24" border="0"></a>
					</div>
				</div>
			</div>
		</div>
		<div id="edit_mode_item_3" style="display:none;">
			<div class="pull-right"><span class="text-small"><input type="checkbox" name="different_addr" value="" checked>&nbsp;&nbsp;Same as Mailing Address</span></div>
			<div class="clearfix"></div>

			<div class="row fix-bs-row">
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street Number</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleLocationInfo.LocationHouse#" name="pplohse" />
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Street</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleLocationInfo.LocationStreet#" name="pplostr"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Mode</span> <br />
					<input type="text" class="form-control" value="" name="mode"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Number</span><br />
					<input type="text" class="form-control" value="" name="number"/>
				</div>
				<div class="clearfix"></div>



				<div class="col-xs-3 databox">
					<span class="formatted-data-label">City</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleLocationInfo.locationCity#" name="pplocty" />
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">State</span><br />
					<select data-tags="true" class="form-control select2-2piece-new" name="ppstate">
						<option value="" data-val="&nbsp;" data-desc="Select..." data-adjval="N/A">Select...</option>
						<cfloop query="prc.qGetStateList">
							<option value="#prc.qGetStateList.field_abbrev#" data-val="#prc.qGetStateList.field_abbrev#" data-desc="#prc.qGetStateList.field_option#">#prc.qGetStateList.field_abbrev#: #prc.qGetStateList.field_option#</option>
						</cfloop>
					</select>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip</span><br />
					<input type="text" class="form-control" value="#prc.qGetTangibleLocationInfo.locationZip#" name="pplozip5"/>
				</div>
				<div class="col-xs-3 databox">
					<span class="formatted-data-label">Zip+4</span><br />
					<input type="text" class="form-control" value="" name="zipfour"/>
				</div>

				<div class="clearfix"></div>
				<div class="col-xs-12">
					<div class="land-part-action-buttons">
						<a href="javascript:editThisRow(3);"><img src="/includes/images/v2/realCAMA_icon_view2.png" alt="View" width="53" height="24" border="0"></a>
					</div>
				</div>


			</div>
		</div>
	</div>
</cfoutput>