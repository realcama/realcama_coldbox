/**
* A normal ColdBox Event Handler
*/
component{

	property name="commonCalls" 			inject="model";
	property name="sessionStorage" 			inject="sessionStorage@cbstorages";
	property name="tangibleServices"		inject="tangibleServices@tangibleModule";
	property name="log"						inject="logbox:logger:{this}";
	property name="htmlhelper" 				inject="htmlhelper@coldbox";
	property name="pagingService" 			inject="PagingService@cbpagination";


	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";

	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = "GET"

	};

	function index( event, rc, prc ){

		if ( sessionStorage.exists("parcelSandBox") ) {
			if ( not isdefined("rc.taxYear") ) {
				rc.taxYear = commonCalls.parseParcelSandbox("1").taxYear;
				rc.ParcelID = commonCalls.parseParcelSandbox("1").ParcelID;
				rc.tpp = commonCalls.parseParcelSandbox("1").tpp;
			}
		};

		prc.pageTitle = "RealCAMA TPP";
		// Set the Display mode we know which set of SP's to run in the CommonCalls
		rc.displayMode = "TPP";

		var stCommonCalls = commonCalls.commonPageIncludes( rc );
		structAppend(prc, stCommonCalls);

		structAppend(prc, tangibleServices.getAllTangibleOwnerData(
											accountNumber = prc.stPersistentInfo.accountNumber,
											parcelID = prc.stPersistentInfo.parcelID,
											taxYear = prc.stPersistentInfo.taxYear
											));

		event.setView( "tangible/index" );
	}

	function assets( event, rc, prc ){
		prc.pageTitle = "RealCAMA TPP Assets";

		// Set the Display mode we know which set of SP's to run in the CommonCalls
		rc.displayMode = "TPP";

		var stCommonCalls = commonCalls.commonPageIncludes( rc );
		structAppend(prc, stCommonCalls);

		prc.qGetTangibleAssetInfo = tangibleServices.getTangibleAssetInfo(
											accountNumber = prc.stPersistentInfo.accountNumber,
											parcelID = prc.stPersistentInfo.parcelID,
											taxYear = prc.stPersistentInfo.taxYear
											);

		paginationSettings = pagingService.getPageStart(rc, prc.qGetTangibleAssetInfo.RecordCount, Controller.getSetting("PagingMaxRows") );
		rc.page = paginationSettings.page;
		rc.page_size = paginationSettings.page_size;
		rc.start_row = paginationSettings.start_row;

		//	writeDump(prc);

		event.setView( "tangible/assets" );

	}
}