
<CFIF prc.getParcelDocumentsData.RecordCount EQ 0>
	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			No document records found for the selected parcel
		</div>
	</div>
<CFELSE>

	<cfoutput>
		<cfloop query="prc.getParcelDocumentsData">
			<div class="panel-heading" role="tab" class="panel-collapse collapse" id="heading#prc.getParcelDocumentsData.currentrow#"  aria-expanded="false" >
				<table class="land-line-summary">
				<tr href="##collapse#prc.getParcelDocumentsData.currentrow#" class="section-bar-expander" onclick="toggleChevronChild(#prc.getParcelDocumentsData.currentrow#)" data-parent="heading#prc.getParcelDocumentsData.currentrow#" data-toggle="collapse" aria-controls="collapse#prc.getParcelDocumentsData.currentrow#" >
					<td width="10%" class="section-bar-label">###prc.getParcelDocumentsData.CurrentRow#</td>
					<td width="30%" class="section-bar-label">#prc.getParcelDocumentsData.Name#</td>
					<td width="10%" class="section-bar-label">#prc.getParcelDocumentsData.Year#</td>
					<td width="20%" class="section-bar-label">#prc.getParcelDocumentsData.CategoryTypeDescription#</td>
					<td width="25%" class="section-bar-label">#prc.getParcelDocumentsData.DocumentTypeDescription#</td>
					<td width="5%" align="right" class="section-bar-label"><span id="chevron_#prc.getParcelDocumentsData.currentrow#" class="block-expander glyphicon glyphicon-chevron-down collapsed"></span></td>
				</tr>
				</table>
			</div>

			<div id="collapse#prc.getParcelDocumentsData.currentrow#" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading#prc.getParcelDocumentsData.currentrow#">
				<div id="view_mode_item_#prc.getParcelDocumentsData.currentrow#">
					<div class="row fix-bs-row">
						<div class="col-xs-4 databox">
							<span class="formatted-data-label">File Name</span><br />
							<span class="formatted-data-values">#prc.getParcelDocumentsData.Name#</span>
						</div>
						<div class="col-xs-6 databox">
							<span class="formatted-data-label">Title</span><br />
							<span class="formatted-data-values">#prc.getParcelDocumentsData.Title#</span>
						</div>
						<div class="col-xs-2 databox">
							<span class="formatted-data-label">Year</span><br/>
							<span class="formatted-data-values">#prc.getParcelDocumentsData.Year#</span>
						</div>
						<div class="clearfix"></div>

						<div class="col-xs-4 databox">
							<span class="formatted-data-label">Category</span><br />
							<span class="formatted-data-values">#prc.getParcelDocumentsData.CategoryTypeDescription#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Document Type</span><br />
							<span class="formatted-data-values">#prc.getParcelDocumentsData.DocumentTypeDescription#</span>
						</div>
						<div class="col-xs-3 databox">
							<span class="formatted-data-label">Date Uploaded</span><br />
							<span class="formatted-data-values">#DateTimeFormat(prc.getParcelDocumentsData.DateLastModified)#</span>
						</div>
						<div class="clearfix"></div>

						<div class="col-xs-12 databox">
							<span class="formatted-data-label">Notes</span><br />
							<span class="formatted-data-values">#prc.getParcelDocumentsData.Notes#</span>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="row fix-bs-row">
						<CFIF LEN(TRIM( prc.getParcelDocumentsData.Name )) NEQ 0>
							<!--- Document File Preview --->
							<cfswitch expression="#Right( prc.getParcelDocumentsData.name, 4 )#">
								<cfcase value=".pdf,.doc,docx,.txt">
									<cfset strPreviewHTML = "<embed src='#prc.getParcelDocumentsData.authenticatedLink#' width='100%' height='300px' border='1'/>">
								</cfcase>

								<cfdefaultcase>
									<cfset strPreviewHTML = "<img src='#prc.getParcelDocumentsData.authenticatedLink#'/>">
								</cfdefaultcase>
							</cfswitch>

							<div class="col-xs-12">
								<span class="formatted-data-label">File</span><br />
								<span class="formatted-data-values">
									#strPreviewHTML#
									<P><BR>
								</span>
							</div>
							<div class="clearfix"></div>
						</CFIF>
					</div>

				</div>
			</div>
		</cfloop>
	</cfoutput>
</CFIF>
