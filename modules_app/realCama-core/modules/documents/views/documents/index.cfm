<cfoutput>
<cfset showTabOptions = "document" /> <!--- used in miniNavigationBar.cfm --->
<cfset iPageNumber = 0 />
<cfset tab = "documents" /> <!--- used in audit-boxes.cfm --->

<cfif prc.getParcelDocumentsData.recordcount eq "0">
	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			No Documents records found for the selected parcel.
		</div>
	</div>
<cfelse>
	#renderView('/_templates/alertMsgs')#
	#addAsset('/includes/js/buildinginfo.js')#

	<script>
		var taxYear = #prc.stPersistentInfo.taxYear#;
		var parcelID = "#prc.stPersistentInfo.parcelID#";

		var iCurrentPage = 1;

		<CFWDDX ACTION="CFML2JS"
				INPUT="#prc.getParcelDocumentsData#"
				TOPLEVELVARIABLE="documentData">

<!---

		function pageIncrement(position) {
			if((iCurrentPage == 1 && position == -1) || (iCurrentPage == #prc.iTotalPages# && position == 1 ))
			{
				/* Do Nothing */
			}
			else {
				for(i=1; i <= #prc.iTotalPages#; i++ ) {
					$("##page-" + i).hide();
				}
				iCurrentPage = iCurrentPage + position;
				$("##page-" + iCurrentPage).show();
				$("##current-page").html(iCurrentPage);
			}

			if(iCurrentPage == #prc.iTotalPages# && position == 1) {
				$("##building-page-next").addClass("disabled");
			} else {
				$("##building-page-next").removeClass("disabled");
			}

			if(iCurrentPage == 1 && position == -1) {
				$("##building-page-prev").addClass("disabled");
			} else {
				$("##building-page-prev").removeClass("disabled");
			}
		}
--->

	</script>


	<cfif Controller.getSetting( "bShowPacketLinks")>
		<div class="alert alert-info">
			<A href="javascript:void(0);" title="Documents Packet" onclick="WriteRaw(documentData);">Documents Packet</A>&nbsp;&nbsp;&nbsp;
		</div>
	</cfif>

	<div class="container-border container-spacer">
		<div class="table-header content-bar-label">
			#renderView('_templates/miniNavigationBar')#
			Documents <span id="totalLandLines">(#prc.getParcelDocumentsData.recordcount#)</span>
		</div>

		<div id="expander-holders" class="panel panel-default">
			#renderView('documents/viewlets/_documentLine')#

			<!---
			<cfloop query="prc.getParcelDocumentsData">
				<cfif prc.getParcelDocumentsData.CurrentRow MOD rc.page_size EQ 1>
					<cfset iPageNumber = iPageNumber + 1 />
					<div id="page-#iPageNumber#" <cfif rc.page neq iPageNumber>class="hide"</cfif>>
				</cfif>

				#renderView('documents/viewlets/_documentLine')#

				<cfif prc.getParcelDocumentsData.CurrentRow MOD rc.page_size EQ 0>
					</div>
				</cfif>
			</cfloop>
			--->
			<cfif prc.getParcelDocumentsData.CurrentRow MOD rc.page_size NEQ 0>
				</div> <!--- This ensures the ' div id="page-#iPageNumber#" ' to be closed correctly --->
			</cfif>
		</div> <!--- div id="expander-holders" class="panel panel-default" --->
	</div>


</cfif>

#addAsset('/includes/js/header.js')#

</cfoutput>
