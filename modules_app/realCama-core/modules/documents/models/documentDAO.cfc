/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="dsn" 	inject="coldbox:datasource:ws_realcama_nassau";
	property name="log" 	inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	documentDAO function init(){

		return this;
	}


	// Stored proc to retrieve the Parcel Documents Data
	query function getParcelDocumentsData( required string parcelID ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetParcelDocuments (:p_ParcelID )");
			q.addParam(name="p_ParcelID", value=arguments.parcelID, cfsqltype="cf_sql_varchar", null=len(trim(arguments.parcelID)) ? false : true);



		return q.execute().getResult();
	}


	// Stored proc to retrieve the Parcel Documents Data
	query function getDocumentDataByID( required string documentID ) {

		var results = {};

		// Stored Proc to retrieve the Document data
		var q = new Query(datasource="#dsn.name#",sql="call p_GetDocumentByID (:p_documentID )");
			q.addParam(name="p_documentID", value=arguments.documentID, cfsqltype="cf_sql_integer", null=len(trim(arguments.documentID)) ? false : true);

		return q.execute().getResult();
	}


	// Stored proc to retrieve the Document Codes
	query function getDocumentCodes( required string codeType ) {

		var q = new Query(datasource="#dsn.name#",sql="call p_GetDocumentCodes (:p_codeType )");
			q.addParam(name="p_codeType", value=arguments.codeType, cfsqltype="cf_sql_varchar", null=len(trim(arguments.codeType)) ? false : true);

		return q.execute().getResult();
	}


	// Stored proc to retrieve the Document Codes
	query function getIncompleteDocumentsToProcess( ) {

		log.info ( "getIncompleteDocumentsToProcess (55):" );
		var q = new Query(datasource="#dsn.name#",sql="call p_GetDocumentsIncomplete ()");

		return q.execute().getResult();
	}


	// Stored proc to set the document details info
	function setDocumentDetailsInfo(
									required number documentID,
									required number year,
									required number cateogryType,
									required number documentType,
									required string originalFileName,
									required string fileName,
									required string title,
									required string notes,
									required string notice,
									required datetime scanDate,
									required number updatedBy,
									required number statusFlag	) {

		//var dtScanDate = "dateTimeFormat(arguments.scanDate)#  #timeFormat(arguments.scanDate)#;
		log.info ( "setDocumentDetailsInfo (78) scanDate: " & arguments.scanDate );
		log.info ( "setDocumentDetailsInfo (79) scanDate: " & dateTimeFormat(arguments.scanDate) );


		var q = new Query(datasource="#dsn.name#",sql="call p_SaveDocument (:p_documentID, :p_year,
									:p_cateogryType, :p_documentType, :p_originalFileName, :p_fileName,
									:p_title, :p_notes, :p_notice, :p_scanDate, :p_updateBY, :p_statusFlag)");
			q.addParam(name="p_documentID",			cfsqltype="cf_sql_integer",		value=arguments.documentID,					null=len(trim(arguments.documentID)) ? false : true);
			q.addParam(name="p_year",				cfsqltype="cf_sql_integer",		value=arguments.year,						null=len(trim(arguments.year)) ? false : true);
			q.addParam(name="p_cateogryType",		cfsqltype="cf_sql_integer",		value=arguments.cateogryType,				null=len(trim(arguments.cateogryType)) ? false : true);
			q.addParam(name="p_documentType",		cfsqltype="cf_sql_integer",		value=arguments.documentType,				null=len(trim(arguments.documentType)) ? false : true);
			q.addParam(name="p_originalFileName",	cfsqltype="cf_sql_varchar",		value=arguments.originalFileName,			null=len(trim(arguments.originalFileName)) ? false : true);
			q.addParam(name="p_fileName",			cfsqltype="cf_sql_varchar",		value=arguments.fileName,					null=len(trim(arguments.fileName)) ? false : true);
			q.addParam(name="p_title",				cfsqltype="cf_sql_varchar",		value=arguments.title,						null=len(trim(arguments.title)) ? false : true);
			q.addParam(name="p_notes",				cfsqltype="cf_sql_varchar",		value=arguments.notes,						null=len(trim(arguments.notes)) ? false : true);
			q.addParam(name="p_notice",				cfsqltype="cf_sql_varchar",		value=arguments.notice,						null=len(trim(arguments.notice)) ? false : true);
			q.addParam(name="p_scanDate",			cfsqltype="cf_sql_date",		value=dateTimeFormat(arguments.scanDate),	null=len(trim(arguments.scanDate)) ? false : true);
			q.addParam(name="p_updateBY",			cfsqltype="cf_sql_integer",		value=arguments.updatedBy,					null="No");
			q.addParam(name="p_statusFlag",			cfsqltype="cf_sql_bit",			value=arguments.statusFlag,					null=len(trim(arguments.statusFlag)) ? false : true);

		return q.execute().getResult();

	}


	// Stored proc to set the document Parcel IDs
	function setDocumentParcelID(
								required number documentID,
								required number parcelID ) {


		var q = new Query(datasource="#dsn.name#",sql="call p_SaveDocumentParcel (:p_documentID, :p_parcelID)");
			q.addParam(name="p_documentID",			cfsqltype="cf_sql_integer",		value=arguments.documentID,			null=len(trim(arguments.documentID)) ? false : true);
			q.addParam(name="p_parcelID",			cfsqltype="cf_sql_integer",		value=arguments.parcelID,			null=len(trim(arguments.parcelID)) ? false : true);

		return q.execute().getResult();

	}


	// Stored proc to set the document Parcel IDs
	function setDocumentParcels(
								required number documentID,
								required string parcelIDs ) {


		var q = new Query(datasource="#dsn.name#",sql="call p_SaveDocumentParcels (:p_documentID, :p_listParcelIDs)");
			q.addParam(name="p_documentID",			cfsqltype="cf_sql_integer",		value=arguments.documentID,			null=len(trim(arguments.documentID)) ? false : true);
			q.addParam(name="p_listParcelIDs",		cfsqltype="cf_sql_varchar",		value=arguments.parcelIDs,			null=len(trim(arguments.parcelIDs)) ? false : true);

		return q.execute().getResult();

	}

	// Stored proc to set the document Parcel IDs
	function setDocumentReset() {

		var q = new Query(datasource="#dsn.name#",sql="DELETE FROM document");
		results = q.execute().getResult();

		var q = new Query(datasource="#dsn.name#",sql="DELETE FROM documentParcel");
		results = q.execute().getResult();


		return results

	}
}
