/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="documentDAO"		inject=model;
	property name="AmazonS3"		inject="AmazonS3@s3sdk";
	property name="log" 			inject="logbox:logger:{this}";

	/**
	 * Constructor
	 */
	documentServices function init(){
		return this;
	}


	function getDocumentsAdminData( required string codeType, required string bucketName ) {

		var results = {};
		log.info ( "getDocumentsAdminData (22):" );

		// Stored Proc to retrieve the Document Category Types
		results.getDocumentCategoryTypes = getDocumentCodes( codeType = arguments.codeType );

		// Stored Proc to retrieve the Documents that need to be processed
		results.getDocumentsToProcess = getDocumentsToProcess( strBucketName = arguments.bucketName );

		return results;

	}


	function getParcelDocumentsData( required string parcelID ) {

		var results = {};

		// Stored Proc to retrieve the Parcel Documents
		results = documentDAO.getParcelDocumentsData( parcelID = arguments.parcelID );

		return results;

	}


	function getDocumentDataByID( required string documentID ) {

		var results = {};

		// Stored Proc to retrieve the Parcel Documents
		results.getDocumentDataByID = documentDAO.getDocumentDataByID( documentID = Val(arguments.documentID) );

		return results;

	}


	function getDocumentCodes( required string codeType ) {

		var results = {};

		// Stored Proc to retrieve the Building Types. Will return either Category or Document Types
		results = documentDAO.getDocumentCodes( codeType = arguments.codeType );

		return results;

	}


	function getDocumentsToProcess( required string strBucketName ) {

		log.info ( "getDocumentsToProcess (73):" );
		var arrayAmazonS3 = {};
		var results = QueryNew("Etag,Key,LastModified,Size,Year,DocumentID,CategoryType,DocumentType,ParcelNumbers,Title,Notes,CategoryTypeDescription,DocumentTypeDescription",
								"varchar,varchar,varchar,integer,Integer,Integer,Integer,Integer,Varchar,Varchar,Varchar,Varchar,Varchar");
		var strCategoryCode = "Category";

		// Get the list of documents that are in the Amazon S3 bucket
		arrayAmazonS3 = AmazonS3.getBucket( arguments.strBucketName );

		// Stored Proc to retrieve the Incomplete Documents that have not been finished
		qryIncompleteDocumentsToProcess = getIncompleteDocumentsToProcess();

		// Loop through the array of the Items in the Amazon S3 bucket
		for (loop=1; loop LE ArrayLen(arrayAmazonS3); loop = loop+1) {
       			myStruct=StructNew();
       			myStruct={Etag="N/A"};
				QueryAddRow( results, myStruct );
				//myStruct2={};
				//QueryAddRow( results, myStruct2 );
				results.setCell("Etag", arrayAmazonS3[loop]["Etag"], loop);
				results.setCell("Key", arrayAmazonS3[loop]["Key"], loop);
				results.setCell("LastModified", arrayAmazonS3[loop]["LastModified"], loop);
				results.setCell("Size", arrayAmazonS3[loop]["Size"], loop);
		}


		if ( qryIncompleteDocumentsToProcess.RecordCount ) {

			// Loop through the array of the Items in the Amazon S3 bucket
			for (loop=1; loop LE ArrayLen(arrayAmazonS3); loop = loop+1) {
       			//myStruct=StructNew();
       			//myStruct={Etag="N/A"};
				//QueryAddRow( results, myStruct );

				/*<!--- Query of Queries code! --->*/
				var q = new Query(sql="SELECT * FROM qryIncompleteDocumentsToProcess WHERE name = :name");
					/*<!--- Denote this is a QoQ --->*/
					q.setAttributes(dbtype="query");
					/*<!--- Set the query we will be using --->*/
					q.setAttributes(qryIncompleteDocumentsToProcess = qryIncompleteDocumentsToProcess);
					/*<!--- Set the parameters for the where clause --->*/
					q.addParam(name="name",	value=arrayAmazonS3[loop]["Key"],	cfsqltype="cf_sql_varchar",	null=len(trim(arrayAmazonS3[loop]["Key"])) ? false : true);

				qryDocumentCheck = q.execute().getResult();

				//writeDump(qryDocumentCheck);

				if ( qryDocumentCheck.RecordCount ) {

					results.setCell("DocumentID", qryDocumentCheck.DocumentID, loop);
					results.setCell("Year", qryDocumentCheck.Year, loop);
					results.setCell("CategoryType", qryDocumentCheck.CategoryType, loop);
					results.setCell("CategoryTypeDescription", qryDocumentCheck.CategoryTypeDescription, loop);
					results.setCell("DocumentType", qryDocumentCheck.DocumentType, loop);

					results.setCell("DocumentTypeDescription", qryDocumentCheck.DocumentTypeDescription, loop);
					results.setCell("ParcelNumbers", qryDocumentCheck.ParcelNumbers, loop);
					results.setCell("Title", qryDocumentCheck.Title, loop);
					results.setCell("Notes", qryDocumentCheck.Notes, loop);
				}

			}

		}

		//writeDump(results);
		//writeDump(arrayAmazonS3); abort;

		return results;

	}


	function getAmazonS3ObjectData( required string strBucketName, required string strObjectKey ) {

		var results = {};

		// Call the Amazon S3 Service to get the information on the requested Object Key
		results.getAmazonS3ObjectData = AmazonS3.getObjectInfo( arguments.strBucketName, arguments.strObjectKey )

		return results;

	}



	function renameAmazonS3Object(
								required string strOldBucketName,
								required string strOldFileKey,
								required string strNewBucketName,
								required string strNewFileKey ) {

		var results = {};

		// Call the Amazon S3 Service to get the information on the requested Object Key
		results.renameAmazonS3Object = AmazonS3.renameObject(
														arguments.strOldBucketName,
														arguments.strOldFileKey,
														arguments.strNewBucketName,
														arguments.strNewFileKey )

		return results;

	}


	function getIncompleteDocumentsToProcess( ) {

		var results = {};

		// Call the Amazon S3 Service to get the information on the requested Object Key
		results = documentDAO.getIncompleteDocumentsToProcess( )

		return results;

	}


	function setDocumentDetailsInfo(
									required struct formData,
									required string stageBucketName,
									required string prodBucketName,
									required string userID ) {

		log.info ( "Inside setDocumentDetailsInfo Admin handler (108):" );
		var results = {};

		var parcelNumbers = arguments.formdata.documentParcelID;
		var documentID = arguments.formdata.documentID;
		var documentYear = Val(arguments.formdata.documentYear);
		//var documentType = Val(arguments.formdata.documentType);
		var categoryType = Val(arguments.formdata.documentCategory);
		var originalFileName = arguments.formdata.originalFilename;
		var objectKey = arguments.formdata.objectKey;
		var title = arguments.formdata.documentTitle;
		var notes = arguments.formdata.documentNote;
		var scanDate = arguments.formdata.originalUploadDate;
		var updatedBy =arguments.userID;
		var documentType = Val(arguments.formdata.documentType);

		var landType = 1;
		var fileName = "";
		var notice = "";
		var StatusFlag = 0;
		var blnMoveS3File = "false";

		if ( documentID == "" ) {
			documentID = 0;
		}

		log.info ( "(198) documentID: " & documentID );
		log.info ( "(199) parcelNumber: " & parcelNumbers );
		log.info ( "(200) documentYear: " & documentYear );
		log.info ( "(201) categoryType: " & categoryType );
		log.info ( "(202) documentType: " & documentType );
		log.info ( "(203) originalFileName: " & originalFileName );
		log.info ( "(204) fileName: " & fileName );
		log.info ( "(205) objectKey: " & objectKey );
		log.info ( "(206) title: " & title );
		log.info ( "(207) notes: " & notes );
		log.info ( "(208) notice: " & notice )
		log.info ( "(209) scanDate: " & scanDate );
		log.info ( "(210) updatedBy: " & updatedBy );
		log.info ( "(211) statusFlag: " & statusFlag );

		//writeDump( formData );

		//  Check to see if the Document has all of the required fields entered in
		if ( parcelNumbers NEQ "" AND documentYear NEQ "" AND categoryType GT 0 AND documentType GT 0) {
			StatusFlag = 1;
			blnMoveS3File = TRUE;
		}

		// Stored Proc to Insert/Update the Document Information
		results.setDocumentDetailsInfo = documentDAO.setDocumentDetailsInfo(
																documentID = documentID,
																year = documentYear,
																cateogryType = categoryType,
																documentType = documentType,
																originalFileName = originalFileName,
																fileName = fileName,
																title = title,
																notes = notes,
																notice = notice,
																scanDate = scanDate,
																updatedBy = updatedBy,
																statusFlag = statusFlag );


		if ( documentID == 0 ) {
			documentID = results.setDocumentDetailsInfo.documentID
		}

		// Stored Proc to retrieve the Document Category Types
		results.setDocumentParcels = documentDAO.setDocumentParcels(
																documentID = documentID,
																parcelIDs = parcelNumbers );

/*
		var parcelIDs = listToArray(parcelNumber);

		// Loop through the array of the Parcels submitted
		for (loop=1; loop LE ArrayLen(parcelIDs); loop = loop+1) {
			log.info ( "setDocumentDetailsInfo (245): parcelID: " & parcelIDs[loop] );
			//writeOutput( "<BR>count: " & loop & " Value: " & parcelIDs[loop]);

			// Stored Proc to retrieve the Document Category Types
			results["setDocumentParcels" & loop] = documentDAO.setDocumentParcels(
																documentID = documentID,
																parcelIDs = parcelIDs[loop] );
		}
*/

		//writeDump( documentID ); abort;

		/* Check to see if we need to move the current document */
		if ( blnMoveS3File ) {
			log.info ( "(290) Inside if blnMoveS3File:" );

			/* Document values have been entered in and now we need to move the document
					from the staging bucket to the production buket. */

			/* ----- Stored Proc to retrieve AmazonS3 Object Data for this Document  ----- */
			var renameAmazonS3Object = renameAmazonS3Object(
														arguments.stageBucketName,
														objectKey,
														arguments.prodBucketName,
														objectKey );


		/*

		On-click "Save", the system shall validate the record to determine if all elements needed to create the
			system file name have been entered.

			The elements needed to create the system file as follows:

		    State FIPS Code
		    County FIPS Code
		    Year
		    ParcelID
		    CategoryID
		    DocumentID

			The system file name shall be structured as follows:

		    STFIPS+CTYFIPS_YEAR_PARCELID_CATID_DOCID_DATE/TIME.EXT
    		Example: 12089_2016_123445658910_4_6.PDF


    	If true, create the system file name(s) and transmit the file to the appropriate end point bucket.*
    	Else, save the tags only. The record shall remain in the listing pane until all elements needed to
    			create the new system file name have been entered.

		*/

		<!---
		<cfoutput>
			strLocalPath: #strLocalPath#<BR>
			strContentType: #strContentType#<BR>
			Application.strAmazonS33ProductionBucket: #Application.strAmazonS33ProductionBucket#<BR>
			Application.strAmazonS33StagingBucket: #Application.strAmazonS33StagingBucket#<BR>
			ObjectKey: #ObjectKey#<BR>+
			<cfabort>
		</cfoutput>
		 --->


		}

		// writeDump( renameAmazonS3Object );
		// writeDump( results ); abort;
		// writeDump(arrayAmazonS3); abort;

		return results;

	}

	function setDocumentReset() {

		var results = {};

		// Stored Proc to retrieve the Parcel Documents
		results.setDocumentReset = documentDAO.setDocumentReset();

		return results;

	}

}