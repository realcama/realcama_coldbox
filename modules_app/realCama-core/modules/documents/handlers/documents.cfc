/**
* I am a new handler
*/
component{

	property name="commonCalls" 		inject="model";
	property name="htmlhelper" 			inject="htmlhelper@coldbox";
	property name="documentServices" 	inject="model";
	property name="pagingService" 		inject="PagingService@cbpagination";
	property name="AmazonS3"			inject="AmazonS3@s3sdk";

	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		index = 'GET'
	};

	/**
	IMPLICIT FUNCTIONS: Uncomment to use
	function preHandler( event, rc, prc, action, eventArguments ){
	}
	function postHandler( event, rc, prc, action, eventArguments ){
	}
	function aroundHandler( event, rc, prc, targetAction, eventArguments ){
		// executed targeted action
		arguments.targetAction( event );
	}
	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
	}
	function onError( event, rc, prc, faultAction, exception, eventArguments ){
	}
	function onInvalidHTTPMethod( event, rc, prc, faultAction, eventArguments ){
	}
	*/

	/**
	* index
	*/
	function index( event, rc, prc ){

		var paginationSettings 		= "";
		var stParcelDocumentsData	= {};
		var getParcelDocumentsData	= "";
		var strTempURL				= "";
		var stCommonCalls			= commonCalls.commonPageIncludes( rc );
		var strIncomingBucketName	= Controller.getSetting( "strAmazonS33ProductionBucket" );

		prc.pageTitle = "RealCAMA Documents";

		structAppend(prc, stCommonCalls);

		getParcelDocumentsData = documentServices.getParcelDocumentsData( parcelID = prc.stPersistentInfo.parcelDisplayID );


		// Loop through the record set and determine the AuthenticatedURL for each document
		for ( var i = 1; i lte getParcelDocumentsData.recordcount; i++ ) {

			strTempURL = AmazonS3.getAuthenticatedURL(
												bucketName 			= strIncomingBucketName,
												uri 				= getParcelDocumentsData.name[i],
												virtualHostStyle	= "true"
												);

			// Update the record set with the AuthenticatedURL
			QuerySetCell( getParcelDocumentsData, "authenticatedLink", strTempURL, i );
		}
		stParcelDocumentsData.getParcelDocumentsData = getParcelDocumentsData;

		structAppend( prc, stParcelDocumentsData );

		paginationSettings = pagingService.getPageStart( rc, prc.getParcelDocumentsData.RecordCount, Controller.getSetting("PagingMaxRows") );

		rc.page = paginationSettings.page;
		rc.page_size = paginationSettings.page_size;
		rc.start_row = paginationSettings.start_row;

		// Set the total number of pages
		prc.iTotalPages = Ceiling(prc.getParcelDocumentsData.RecordCount / rc.page_size);


		prc.tempURL = AmazonS3.getAuthenticatedURL(
												bucketName 			= strIncomingBucketName,
												uri 				= '519791-101_Warning-128.png',
												virtualHostStyle	= "true"
												);


		event.setView( "documents/index" );
	}


}
