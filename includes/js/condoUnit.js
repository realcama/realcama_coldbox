
$(function() {

	$(".numeric").numeric();
	$(".datefield").datepicker();
	changeSelectsToSelect2s();

});

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format3Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";
}

function format3PieceSmallLeft(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left-small formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle-small formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right-small formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";
}




/* 2017.12.07 - TDD - Moved to formatSelect.js
function changeSelectsToSelect2s(){
	$('.select2-2piece-new').select2({
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	// Remove the start up class, so if we have to create a new row on the fly, recalling the select2 init code won't cause problems in already built ones.
	$( ".select2-2piece-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-new").addClass("select2-2piece");
	});

	$('.select2-3piece-new').select2({
		formatResult: format3Piece,
		formatSelection: format3Piece,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-new").addClass("select2-3piece");
	});

	$('.select2-3piece-smallleft-new').select2({
		formatResult: format3PieceSmallLeft,
		formatSelection: format3PieceSmallLeft,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-smallleft-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-smallleft-new").addClass("select2-3piece-smallleft");
	});
}*/

function changeCheckboxVal(iRow,obj) {
	if(obj.checked == true) {
		condoUnitData.setField(iRow,obj.name,"1");
	} else {
		condoUnitData.setField(iRow,obj.name,"0");
	}
	condoUnitData.setField(iRow,'mode','edit');
}

function changeVal(iRow,obj) {
	/* Updates the WDDX packet*/
	inVal = $(obj).val();
	inVal = inVal.replace('$','');
	inVal = inVal.replace(',','');


	if(inVal == "-junk-") {
		/*
			The "Select..." value for the dropdowns that are recreated when a building type is changed are set to "-junk-" to allow the select2 to fire off a change action when
			there is a NA option but no code value assigned to it and a "Select..." option available to the user (as both options are a null value, the onchange function never fires)
		 */
		inVal = null;
	}

	condoUnitData.setField(iRow,obj.name,inVal);
	condoUnitData.setField(iRow,'mode','edit');
}


function saveWDDX() {

	wddxSerializer = new WddxSerializer();
	wddxCondoUnitData1 = wddxSerializer.serialize(condoUnitData);
	$("#condoUnitData").val(wddxCondoUnitData1);

	wddxCondoUnitData2 = wddxSerializer.serialize(condoUnitCodesData);
	$("#condoUnitCodesData").val(wddxCondoUnitData2);

	$("#frmSave").submit();

}


function doCondoPopup() {

	$('#myPleaseWait').modal('show');
    $.ajax({
		type: "get",
		cache: false,
		url: "/index.cfm/condoUnit/showCondoUnitPopup/",
		//url: "/index.cfm/condoUnit/showCondoUnitPopup/taxYear/" + taxYear +"/condoID/" + condoID + "/",
		data: {
			taxYear: taxYear,
			condoID: thisCondoID,
			condoUnitID: thisCondoUnitID
		}, // all form fields
		success: function (data) {
			// on success, post (preview) returned data in fancybox
			$.fancybox(data, {
				// fancybox API options
				'padding' : 0,
				'closeBtn': false,
				helpers : { overlay : {
										locked : true ,
										closeClick: false
									} } /* this prevents screen from shifting */
			}); // fancybox
			$('#myPleaseWait').modal('hide');
		} // success
    }); // ajax

}
