var runs = 0;
var override_runs = 0;
var bOverrideFlag = false;
var allSameTypes = 0;
var totalAppraisedValue = 0;

$( document ).ready(function() {
	$("#edit-correlation").hide();						
	$(".correlation-entry").numeric();
	checkData() ;
	calcTotals();
});

function noNullsEntry(obj) {	
	thisVal = $.trim($( obj ).val());
	
	if(isNaN(thisVal) || thisVal.length == 0){
		// Set the value to ZERO so it's not a null value
		$( obj ).val(0);
	}
}
function setAllToIncome() {	
	/* Anytime someone clicks an INCOME radio button, per JAN we need to set them ALL to INCOME */
	$("input[name=land][value=3]").prop("checked",true);
	$("input[name=land_ag][value=3]").prop("checked",true);
	$("input[name=building][value=3]").prop("checked",true);
	$("input[name=features][value=3]").prop("checked",true);	
}


function checkData() {

	iCheck1 = $('input[name=land]:checked').val();
	iCheck2 = $('input[name=land_ag]:checked').val();
	iCheck3 = $('input[name=building]:checked').val();
	iCheck4 = $('input[name=features]:checked').val();
	
	$("#cost-total-check").removeClass("glyphicon glyphicon-ok");
	$("#mica-total-check").removeClass("glyphicon glyphicon-ok");
	$("#income-total-check").removeClass("glyphicon glyphicon-ok");
	$("#market-total-check").removeClass("glyphicon glyphicon-ok");
	$("#blended-total-check").removeClass("glyphicon glyphicon-ok");		
	$("#override-total-check").removeClass("glyphicon glyphicon-ok");	
		
	if(iCheck1 == iCheck2 && iCheck2 == iCheck3 && iCheck3 == iCheck4) {
		allSameTypes = iCheck1;
		
		// Same data, so destory the blended data per Doug		
		$("#land_blended").val(0);
		$("#land_ag_blended").val(0);
		$("#building_blended").val(0);
		$("#features_blended").val(0);	
		
		switch(iCheck1) {
			case "1":
				$("#cost-total-check").addClass("glyphicon glyphicon-ok");							
				break;
			case "2":
				$("#mica-total-check").addClass("glyphicon glyphicon-ok");
				break;
			case "3":
				$("#income-total-check").addClass("glyphicon glyphicon-ok");
				break;
			case "4":
				$("#market-total-check").addClass("glyphicon glyphicon-ok");
				break;
			case "5":		
				$("#override-total-check").addClass("glyphicon glyphicon-ok");						
				break;
			case "6":
				$("#blended-total-check").addClass("glyphicon glyphicon-ok");		
				break;
		}
		
	} else {
		allSameTypes = 0;
	
		// Different data, so push data over to the blended column	
		$("#blended-total-check").addClass("glyphicon glyphicon-ok");	
		
			
		switch(iCheck1) {
			case "1":
				$("#land_blended").val($("#land_cost").val());
				break;
			case "2":
				$("#land_blended").val($("#land_mica").val());
				break;
			case "3":
				$("#land_blended").val($("#land_income").val());
				break;
			case "4":
				$("#land_blended").val($("#land_market").val());
				break;
			case "5":	
				$("#land_blended").val($("#land_override").val());			
				break;
			case "6":
				break;
		}
		switch(iCheck2) {
			case "1":
				$("#land_ag_blended").val($("#land_ag_cost").val());	
				break;
			case "2":
				$("#land_ag_blended").val($("#land_ag_mica").val());	
				break;
			case "3":
				$("#land_ag_blended").val($("#land_ag_income").val());
				break;
			case "4":
				$("#land_ag_blended").val($("#land_ag_market").val());
				break;
			case "5":	
				$("#land_ag_blended").val($("#land_ag_override").val());			
				break;
			case "6":
				break;
		}
		
		switch(iCheck3) {
			case "1":
				$("#building_blended").val($("#building_cost").val());
				break;
			case "2":
				$("#building_blended").val($("#building_mica").val());
				break;
			case "3":
				$("#building_blended").val($("#building_income").val());
				break;
			case "4":
				$("#building_blended").val($("#building_market").val());
				break;
			case "5":	
				$("#building_blended").val($("#building_override").val());				
				break;
			case "6":
				break;
		}
		
		switch(iCheck4) {
			case "1":
				$("#features_blended").val($("#features_cost").val());
				break;
			case "2":
				$("#features_blended").val($("#features_mica").val());
				break;
			case "3":
				$("#features_blended").val($("#features_income").val());
				break;
			case "4":
				$("#features_blended").val($("#features_market").val());
				break;
			case "5":	
				$("#features_blended").val($("#features_override").val());				
				break;
			case "6":
				break;
		}		
	}	
}
function calcTotals(){
	
	var cost_total = 0;
	var mica_total = 0;
	var income_total = 0;
	var market_total = 0;
	var blended_total = 0;
	var override_total = 0;
	totalAppraisedValue = 0;

	landSelected = $("input[name=land]:checked").val();
	landAgSelected = $("input[name=land_ag]:checked").val();
	buildingSelected = $("input[name=building]:checked").val();
	featuresSelected = $("input[name=features]:checked").val();
	
	// Cost
	cost_total = cost_total += parseFloat($("#land_cost").val());	
	cost_total = cost_total += parseFloat($("#land_ag_cost").val());	
	cost_total = cost_total += parseFloat($("#building_cost").val());	
	cost_total = cost_total += parseFloat($("#features_cost").val());
	
	if(landSelected == 1) {						
		totalAppraisedValue += parseFloat($("#land_cost").val());				
	}
	if(landAgSelected == 1) {
		totalAppraisedValue += parseFloat($("#land_ag_cost").val());
	}
	if(buildingSelected == 1) {			
		totalAppraisedValue += parseFloat($("#building_cost").val());	
	}
	if(featuresSelected == 1) {			
		totalAppraisedValue += parseFloat($("#features_cost").val());				
	}
	
	// Mica
	mica_total = mica_total += parseFloat($("#land_mica").val());
	mica_total = mica_total += parseFloat($("#land_ag_mica").val());	
	mica_total = mica_total += parseFloat($("#building_mica").val());	
	mica_total = mica_total += parseFloat($("#features_mica").val());		
	
	if(landSelected == 2) {				
		totalAppraisedValue += parseFloat($("#land_mica").val());		
	}
	if(landAgSelected == 2) {		
		totalAppraisedValue += parseFloat($("#land_ag_mica").val());			
	}
	if(buildingSelected == 2) {			
		totalAppraisedValue += parseFloat($("#building_mica").val());			
	}
	if(featuresSelected == 2) {		
		totalAppraisedValue += parseFloat($("#features_mica").val());		
	}
	
	// Income
	income_total = income_total += parseFloat($("#land_income").val());	
	income_total = income_total += parseFloat($("#land_ag_income").val());	
	income_total = income_total += parseFloat($("#building_income").val());		
	income_total = income_total += parseFloat($("#features_income").val());	
		
	if(landSelected == 3) {			
		totalAppraisedValue += parseFloat($("#land_income").val());		
	}
	if(landAgSelected == 3) {		
		totalAppraisedValue += parseFloat($("#land_ag_income").val());			
	}
	if(buildingSelected == 3) {		
		totalAppraisedValue += parseFloat($("#building_income").val());		
	}
	if(featuresSelected == 3) {		
		totalAppraisedValue += parseFloat($("#features_income").val());		
	}
	
	// Market
	market_total = market_total += parseFloat($("#land_market").val());
	market_total = market_total += parseFloat($("#land_ag_market").val());	
	market_total = market_total += parseFloat($("#building_market").val());	
	market_total = market_total += parseFloat($("#features_market").val());		
	
	if(landSelected == 4) {			
		totalAppraisedValue += parseFloat($("#land_market").val());			
	}
	if(landAgSelected == 4) {		
		totalAppraisedValue += parseFloat($("#land_ag_market").val());			
	}
	if(buildingSelected == 4) {		
		totalAppraisedValue += parseFloat($("#building_market").val());			
	}
	if(featuresSelected == 4) {		
		totalAppraisedValue += parseFloat($("#features_market").val());		
	}
	
	// Override
	override_total = override_total += parseFloat($("#land_override").val());	
	override_total = override_total += parseFloat($("#land_ag_override").val());	
	override_total = override_total += parseFloat($("#building_override").val());		
	override_total = override_total += parseFloat($("#features_override").val());	
	
	if(landSelected == 5) {			
		totalAppraisedValue += parseFloat($("#land_override").val());		
	}
	if(landAgSelected == 5) {		
		totalAppraisedValue += parseFloat($("#land_ag_override").val());			
	}
	if(buildingSelected == 5) {		
		totalAppraisedValue += parseFloat($("#building_override").val());		
	}
	if(featuresSelected == 5) {			
		totalAppraisedValue += parseFloat($("#features_override").val());		
	}
	
	// Blended
	/* No radio box so ignore */
	
	$("#cost_total").val(cost_total.formatMoney(0,',','.'));
	$("#mica_total").val(mica_total.formatMoney(0,',','.'));
	$("#income_total").val(income_total.formatMoney(0,',','.'));
	$("#market_total").val(market_total.formatMoney(0,',','.'));
	$("#override_total").val(override_total.formatMoney(0,',','.'));
		
	
	if(allSameTypes == 0) {
		$("#blended_total").val(totalAppraisedValue.formatMoney(0,',','.'));		
	} else {
		// since they are all the same type, we don't need to show the value in blended
		$("#blended_total").val(0);	
	}
	
	/* Set the appraised value on the EDIT page */	
	$("#appraisedValueEdit").val(totalAppraisedValue.formatMoney(0,',','.'));
	if(runs == 0) {
		/* Set the appraised value on the VIEW page */
		$("#indicatedvalue_view").val(totalAppraisedValue.formatMoney(0,',','.'));
		runs++
	}
	
	
	
	changeOptions();
}

function changeOptions() {
	bChangeOtherCells = true;	
	
	if(
		($("input[name=land]:checked").val() == 5 && $("input[name=building]:checked").val() == 1) 
		||
		($("input[name=land_ag]:checked").val() == 5 && $("input[name=building]:checked").val() == 1)
	) {		
		// Allow the user to put in an override value, so set the following variable to tell the code below to not step on the override values	
		bChangeOtherCells = false;
	} else {		
		
		// Allow the user to NOT put in an override value, so set the following variable to tell the code below to step on the override values
		bChangeOtherCells = true;
		
		// Set the override columns to readonly (just in case they were editable previously)
		$("#land_override").prop("readonly",true);
		$("#land_ag_override").prop("readonly",true);
		
		// Destory the values in the override column since the override radio buttons are not selected for LAND and LAND AG	(*** DO NOT TOUCH THE OVERRIDE COLUMN !!!! ***)		
		if(allSameTypes > 0 && allSameTypes != 5) {			
			$("#land_override").val(0);
			$("#land_ag_override").val(0);
			$("#building_override").val(0);
			$("#features_override").val(0);	
			$("#land_blended").val(0);
			$("#land_ag_blended").val(0);
			$("#building_blended").val(0);
			$("#features_blended").val(0);
		}	
		
	}	
	
	
	
	if($("input[name=building]:checked").val() == 1 && bChangeOtherCells == true) {								
		// Land's COST is pulled from Land's MICA column
		land = 	parseFloat($("#land_mica").val());
		//Set the visual component for Land's COST
		$("#land_cost").val(land);
		// Land AG's COST is pulled from Land's MICA column
		landag = parseFloat($("#land_ag_mica").val());
		//Set the visual component for Land AG's COST
		$("#land_ag_cost").val(landag);
		
		// Set the field to editable
		$("#building_cost").prop("readonly",false);		
		
	}
	if($("input[name=building]:checked").val() != 1){
		// Set the field to readonly
		$("#building_cost").prop("readonly",true);
		$("input[name=land][value=1]").prop("disabled",true);
		$("input[name=land_ag][value=1]").prop("disabled",true);
	}	
	
	if($("input[name=features]:checked").val() == 1) {	
		// Set the field to editable
		$("#features_cost").prop("readonly",false);							
	}
	
	if($("input[name=features]:checked").val() != 1){
		// Set the field to readonly
		$("#features_cost").prop("readonly",true);
	}	

	if($("input[name=building]:checked").val() == 4  && bChangeOtherCells == true) {
		// Land's MARKET is pulled from Land's MICA column
		land = 	parseFloat($("#land_mica").val());
		//Set the visual component for Land's MARKET
		$("#land_market").val(land);
		// Land AG's MARKET is pulled from Land's MICA column
		landag = 	parseFloat($("#land_ag_mica").val());
		//Set the visual component for Land AG's MARKET
		$("#land_ag_market").val(landag);
	}	
	
	if($("input[name=building]:checked").val() == 4  && bChangeOtherCells == true) {
		// Land's MARKET is pulled from Land's MICA column
		land = 	parseFloat($("#land_mica").val());
		//Set the visual component for Land's MARKET
		$("#land_market").val(land);
		// Land AG's MARKET is pulled from Land's MICA column
		landag = 	parseFloat($("#land_ag_mica").val());
		//Set the visual component for Land AG's MARKET
		$("#land_ag_market").val(landag);
	}
	
	if($("input[name=land]:checked").val() == 5) {
		$("#land_override").prop('readonly', false);
		$("#land_blended").val(parseFloat($("#land_override").val()));			
		bOverrideFlag = true;	
	} else {
		$("#land_override").prop('readonly', true);
	}
	
	if($("input[name=land_ag]:checked").val() == 5) {
		$("#land_ag_override").prop('readonly', false);
		$("#land_ag_blended").val(parseFloat($("#land_ag_override").val()));			
		bOverrideFlag = true;			
	} else {
		$("#land_ag_override").prop('readonly', true);
	}
	
	if($("input[name=building]:checked").val() == 5) {
		$("#building_override").prop('readonly', false);
		$("#building_blended").val(parseFloat($("#building_override").val()));			
		bOverrideFlag = true;				
	} else {
		$("#building_override").prop('readonly', true);
	}
	
	if($("input[name=features]:checked").val() == 5) {
		$("#features_override").prop('readonly', false);	
		$("#features_blended").val(parseFloat($("#features_override").val()));			
		bOverrideFlag = true;		
	} else {
		$("#features_override").prop('readonly', true);	
	}
		
	if(bOverrideFlag) {
		$(".datefield-yearonly").datepicker(arDatePickerOptions);		
		$(".datefield-yearonly").prop("readonly",false);			
	} else {
		$(".datefield-yearonly").datepicker('remove');		
		$(".datefield-yearonly").prop("readonly",true);		
	}
	
	var bChangeVal = false;
	if($("input[name=land]:checked").val() == 1 &&  $("input[name=building]:checked").val() != 1) {
		bChangeVal = true;
	}
	if($("input[name=land]:checked").val() == 4 &&  $("input[name=building]:checked").val() != 4) {
		bChangeVal = true;
	}
	
	if(bChangeVal){
		iCurrentCheck = $("input[name=building]:checked").val();
		$("input[name=land][value=" + iCurrentCheck + "]").prop("checked",true);
		$("input[name=land_ag][value=" + iCurrentCheck + "]").prop("checked",true);							
	}

}



function fixRadios(iColumn) {	
	if (iColumn == 1 || iColumn == 2 || iColumn == 4) {
		// Change the checkbox options for Land COST and Land AG Cost
		$("input[name=land][value=" + iColumn + "]").prop("checked",true);
		$("input[name=land][value=" + iColumn + "]").removeAttr("disabled");
		$("input[name=land_ag][value=" + iColumn + "]").prop("checked",true);
		$("input[name=land_ag][value=" + iColumn + "]").removeAttr("disabled");
	}
}

function sendData() {	
	frmCorrelation.submit();
}