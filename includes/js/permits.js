
$(function() {
	$(".numeric").numeric();
	$(".datefield").datepicker();
	changeSelectsToSelect2s();
});

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format3Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";
}

function format3PieceSmallLeft(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left-small formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle-small formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right-small formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";
}

function changeVal(iRow,obj) {
	/* Updates the WDDX packet*/
	inVal = $(obj).val();
	inVal = inVal.replace('$','');
	inVal = inVal.replace(',','');

	permitsData.setField(iRow,obj.name,inVal);
}

function changeNotesVal(obj) {
	rownumber = $(obj).data("rownumber");
	inVal = $(obj).val();
	notesData.setField(rownumber,"nonote",inVal);
}



/* 2017.12.07 - TDD - Moved to formatSelect.js
function changeSelectsToSelect2s(){
	$('.select2-2piece-new').select2({
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	// Remove the start up class, so if we have to create a new row on the fly, recalling the select2 init code won't cause problems in already built ones.
	$( ".select2-2piece-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-new").addClass("select2-2piece");
	});

	$('.select2-3piece-new').select2({
		formatResult: format3Piece,
		formatSelection: format3Piece,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-new").addClass("select2-3piece");
	});

	$('.select2-3piece-smallleft-new').select2({
		formatResult: format3PieceSmallLeft,
		formatSelection: format3PieceSmallLeft,
		escapeMarkup: function(m) { return m; }
	});

	$( ".select2-3piece-smallleft-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-smallleft-new").addClass("select2-3piece-smallleft");
	});
}*/


function addBlankRow(intRowNum) {
	permitsData.addRows(1);
	iNewRow = permitsData.getRowCount() -1;
	strCPItem = "X_" + permitsData.getRowCount();

	//strCPItem = "X_" + intRowNum;

	/* Set necessary data for this record */

	permitsData.setField(iNewRow, "mode", "insert");
	permitsData.setField(iNewRow, "sequencenumber", intRowNum);
	permitsData.setField(iNewRow, "itemnumber", strCPItem);


	/* Create a new line and tie the WDDX record to that row */
	$.ajax({
		url: "index.cfm/permits/addPermit",
		type: "post",
		async: false, //This variable makes the function wait until a response has been returned from the page
		data: {
			rowNum: strCPItem,
			itemNumber: permitsData.getRowCount(),
			//iNotesCurrentLine: iNotesCurrentLine,
		},
	  	success: function(data){
			$("#expander-holders").prepend(data);
			$(".numeric").numeric();
			formatSelectsToSelect2();
			$('.datefield-permits').datepicker(arDatePickerOptions4Permits).on('changeDate', function(ev){ $(this).datepicker('hide');});
			$("#totalPermiLines").html("(" + permitsData.getRowCount() + ")");
/*
			// First comment Line
			for(i=1; i < 4; i++) {
				notesData.addRows(1);
				notesData.setField((notesData.getRowCount()-1),"noitem",strCPItem);
				notesData.setField((notesData.getRowCount()-1),"noprop",ParcelID);
				notesData.setField((notesData.getRowCount()-1),"noptyp","R");
				notesData.setField((notesData.getRowCount()-1),"noscrn","PRMT");
				notesData.setField((notesData.getRowCount()-1),"norecno",i);
				notesData.setField((notesData.getRowCount()-1),"mode","insert");
				iNotesCurrentLine++ //Increment the notes counter
			}
*/
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){

		}
	});




}




function deletePermit(iRow) {
	cfRow = iRow + 1;
	permitsData.setField(iRow,"mode","delete");
	permitsData.setField(iRow,"status","D");
	$(".permit_row_" + iRow).prop("readonly",true);
	$(".permit_row_" + iRow).prop("disabled",true);
	$("#restorePermit_" + iRow).show();
	$("#deletePermit_" + iRow).hide();
	//$("#permitState_delete_marker_" + iRow).show();
}


function restoreDeletedPermit(iRow) {
	cfRow = iRow + 1;
	permitsData.setField(iRow,"mode","edit");
	permitsData.setField(iRow,"status","");
	$(".permit_row_" + iRow).prop("readonly",false);
	$(".permit_row_" + iRow).prop("disabled",false);
	$("#restorePermit_" + iRow).hide();
	$("#deletePermit_" + iRow).show();
	//$("#permitState_delete_marker_" + iRow).hide();
}


function doPermitResequencePopup(fStatus, fParcelID, fParcelType, fTaxYear) {


	$('#myPleaseWait').modal('show');
    $.ajax({
		type: "get",
		cache: false,
		url: '/index.cfm/permits/permitResequence',
		data: {
			status: fStatus,
			parcelID: fParcelID,
			parcelType: fParcelType,
			taxYear: fTaxYear
		}, // all form fields
		success: function (data) {
			// on success, post (preview) returned data in fancybox
			$.fancybox(data, {
				// fancybox API options
				'padding' : 0,
				'closeBtn': false,
				helpers : { overlay : {
										locked : true ,
										closeClick: false
									} } /* this prevents screen from shifting */
			}); // fancybox
			$('#myPleaseWait').modal('hide');
		} // success
    }); // ajax
}


function saveWDDX(permitItem) {
	/* Pull data out of the WDDX packets that are not relevant to the row we want to submit*/
	permitRow = createPacket(permitsData, "itemnumber", permitItem);
	//notesForRow = createPacket(notesData, "noitem", permitItem);

	/* Serialize the new WDDX packets */
	wddxSerializer = new WddxSerializer();
	wddxPermitsData = wddxSerializer.serialize(permitRow);
	//wddxSerializer = new WddxSerializer();
	//wddxNotesData = wddxSerializer.serialize(notesForRow);
	$("#permitsData").val(wddxPermitsData);
	//$("#notesData").val(wddxNotesData);

	$("#frmSave").submit();
}
