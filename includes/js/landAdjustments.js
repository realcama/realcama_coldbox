var iOptionsOnModal = 0;
function format2Piece1(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc') + "</span>";
}

// Called in the document ready function in _showLandPopUp.cfm
function buildModal() {
	var iModalRow = 0; // Used for create ID attribute values in modal window.
	for ( var iCurrentRow = 0; iCurrentRow < parent.udfData.getRowCount(); iCurrentRow++ ) {
		// sets all options to 0
		for ( var i = 0; i < arrlandAdjustments.length; i++ ) {
			$("#" + arrlandAdjustments[i] + "_" + iCurrentRow).val('');
		}
	}

	for ( var iCurrentRow = 0; iCurrentRow < parent.udfData.getRowCount(); iCurrentRow++ ) {
		
		if ( parent.udfData.getField(iCurrentRow,"mode") != "Delete" ) {
			try {
				adj_id = parent.udfData.getField(iCurrentRow,"code").toString();
				adj_id = adj_id.trim();
			} catch(err) {
				adj_id = "N/A";
			}

			var strHTML = "";
			
			// Calls function to create row in the modal window
			strHTML = createAdjEntryRow(iModalRow, parent.udfData.getField(iCurrentRow,"codetype"), 'Edit');

			if(iOptionsOnModal === 0) {
				$("#adjContent").html(strHTML);
			} else {
				$("#adjContent").append(strHTML);
			}
			
			// update adjustment code select boxes to default to selected option
			for ( var i = 0; i < arrlandAdjustments.length; i++ ) {
				if ( arrlandAdjustments[i] == parent.udfData.getField(iCurrentRow, "codetype") ) {
					$("#" + arrlandAdjustments[i] + "_" + iCurrentRow).val(adj_id);
				}
			}
			/*switch( parent.udfData.getField(iCurrentRow, "codetype") ) {
			case 'Depths':
				$("#Depths_" + iModalRow).val(adj_id);
				break;
			case 'Roads':
				$("#Roads_" + iModalRow).val(adj_id);
				break;
			case 'Topography':
				$("#Topography_" + iModalRow).val(adj_id);
				break;
			case 'Utilities':
				$("#Utilities_" + iModalRow).val(adj_id);
				break;
			case 'Zoning':
				$("#Zoning_" + iModalRow).val(adj_id);
				break;
			}*/
			
			$("#percentageFactor_" + iModalRow).val( parseFloat (parent.udfData.getField(iCurrentRow,"percentagefactor") ).toFixed(0) );
			
			switch( parent.udfData.getField(iCurrentRow,"adjustmenttype") ) {
			case 'P':
				$("#bLPoints_" + iModalRow).val( parseFloat( parent.udfData.getField(iCurrentRow,"adjustment") ).toFixed(0) );
				break;
			case 'L':
				$("#bLLumpSum_" + iModalRow).val( parseFloat( parent.udfData.getField(iCurrentRow,"adjustment") ) );
				break;
			case 'F':
				$("#bLFactor_" + iModalRow).val( parseFloat (parent.udfData.getField(iCurrentRow,"adjustment") ).toFixed(2) );
				break;
			case 'R':
				$("#bLRateAddOn_" + iModalRow).val( parseFloat( parent.udfData.getField(iCurrentRow,"adjustment") ).toFixed(3) );
					break;
			}
			iOptionsOnModal++;
		}
		iModalRow++;
	}

	changeSelectsToSelect2s();

	if(iOptionsOnModal == 0) {
		$("#adjContent").html("No adjustments defined for this item");
	}

}

// This function displays the adjustment attribute select boxes
// Called in document ready function
function toggleAdjustmentAttrib(){
	for ( var iCurrentRow = 0; iCurrentRow < parent.udfData.getRowCount(); iCurrentRow++ ) {
		if ( parent.udfData.getField(iCurrentRow,"mode") != "Delete" ) {
			showOption( parent.udfData.getField(iCurrentRow, "codetype"), iCurrentRow );
		}
	}
}
// Called in the onChange event of the adjustments select box
function changeUDFMethod(obj) {
	iRowToSetSelects = $(obj).data("rownumber");

	turnOffSelects(iRowToSetSelects);
	showOption(obj.value,iRowToSetSelects);

}

function turnOffSelects(iRowToSetSelects){
	for ( var i = 0; i < arrlandAdjustments.length; i++ ) {
		$("#" + arrlandAdjustments[i] + "_" + iRowToSetSelects).hide();
		$("#s2id_" + arrlandAdjustments[i] + "_" + iRowToSetSelects).hide();
		$("#" + arrlandAdjustments[i] + "_" + iRowToSetSelects).val(-1);
	}
}

function showOption( method, iRowToSetSelects ) {
	$("#s2id_" + method + "_" + iRowToSetSelects).show();
	$("#s2id_" + method + "_" + iRowToSetSelects).css("display","inline-block");
}

// Called when adjustment's select box on adjustment pop up changes.
// Disable wddx packet update.
function chgAdjustments(iRow,obj) {
	//parent.udfData.setField( iRow, "codetype", $(obj).val() );
	parent.udfData.setField( iRow, "mode", 'Edit' );
}

// Updates WDDX packet when adjustment code changes
function changeAdjVal(iRow,obj) {
	parent.udfData.setField( iRow, "mode", 'Edit' );
}

function removeAdjRow(iRow) {
	var choice = confirm("Are you sure you want to delete this row?");
	if(choice == true) {
		if ( parent.udfData.getField(iRow, "mode") != 'Insert' ) {
			parent.udfData.setField( iRow, "mode", "DeleteNew" );
		} else {
			parent.udfData.setField( iRow, "mode", "Delete" );
		}

		$("#adjRow_" + iRow).remove();
		createLabel();
		$('[data-toggle="tooltip"]').tooltip();
		parent.landUseCalculations(true);
	}
}

function createLabel(){
	var strLabel = "";
	var iPercentage = 0;
	var iPercentageSum = 0;
	var iPointsSum = 0;
	var iLumpSum = 0;
	var iFactorSum = 0;
	var iRateAddOn = 0;

	// find out if we have to use depth or not in the calculation of the net adjustment row
	var bUseDepth = (parent.landData.getField(jsRowNumber,"unitTypeCode") == "EF" || landData.getField(jsRowNumber,"unitTypeCode") == "FF") ? true : false;

	for ( var iCurrentRow = 0; iCurrentRow < parent.udfData.getRowCount(); iCurrentRow++ ) {
		mode = parent.udfData.getField(iCurrentRow,"mode");
		adjcd = parent.udfData.getField( iCurrentRow, "codetype"); // adjcd
		adj_id = parent.udfData.getField( iCurrentRow, "landid"); // adj_id

		if ( mode != "Delete" && mode != "InsertTemp" ) {
			adjcd = parent.udfData.getField( iCurrentRow, "code"); // adjcd
			method = parent.udfData.getField( iCurrentRow, "codedescription"); // adjdesc
			adjds = parent.udfData.getField( iCurrentRow, "percentagefactor"); // adjds
			laadjvalue = parseFloat(parent.udfData.getField( iCurrentRow, "adjustment")); // laadjvalue
			laadjtyp = parent.udfData.getField( iCurrentRow, "adjustmenttype"); // laadjtyp

			if(
				(adjcd == null || adjcd == "N/A" || adjcd === "") &&
				(adjds == null ||  adjds == "N/A") &&
				(method == "Depths" && adjcd == "")
				) {

			} else {
				if( parent.udfData.getField( iCurrentRow, "adjustmenttype") != "0" ) {
					
					iPercentageSum = iPercentageSum + ( parent.udfData.getField( iCurrentRow, "percentagefactor") / 100 );

					strLabel = strLabel + "<tr>";
					strLabel = strLabel + "<td class='formatted-data-values'>" + parent.udfData.getField( iCurrentRow, "codetype") + "</td>";
					strLabel = strLabel + "<td class='formatted-data-values'>" + parent.udfData.getField( iCurrentRow, "code") + "</td>";
					strLabel = strLabel + "<td class='formatted-data-values'>" + parent.udfData.getField( iCurrentRow, "codedescription") + "</td>";
					strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>" + parent.udfData.getField( iCurrentRow, "percentagefactor") + "%</td>";
					var pFactor = "";
					var lFactor = "";
					var fFactor = "";
					var rFactor = "";
					var marker = "";

					if ( parent.udfData.getField( iCurrentRow, "adjustmenttype") == 'P' ) {
						pFactor = parseFloat( parent.udfData.getField( iCurrentRow, "adjustment") ).toFixed(3);
						
						iRateAddOn = iRateAddOn + laadjvalue;

					} else if ( parent.udfData.getField( iCurrentRow, "adjustmenttype") == 'L' ) {
						lFactor = parseFloat( parent.udfData.getField( iCurrentRow, "adjustment") ).toFixed(3);
						iLumpSum = iLumpSum + laadjvalue;

					} else if ( parent.udfData.getField( iCurrentRow, "adjustmenttype") == 'F' ) {
						fFactor = parseFloat( parent.udfData.getField( iCurrentRow, "adjustment") ).toFixed(3);

						var marker = "";
						if(method == "Depths") {
							if(!bUseDepth) {
								marker = "<span class='warning' data-toggle='tooltip' data-placement='top' title='Value is ignored due to unit type'>*</span>";
							}
						}

						// Take the percentage and multiply that against the sum of the percentage adjustments (since we don't add percentage adjustments, we multiply)
						if(marker == "") {
							if(iPercentage == 0) {
								iPercentage = laadjvalue;
							} else {
								iPercentage = iPercentage * (laadjvalue/100);
							}
						}

						if ( iFactorSum == 0 ) {
							iFactorSum = laadjvalue;
						} else {
							iFactorSum = iFactorSum * laadjvalue;	
						}
						

					} else if ( parent.udfData.getField( iCurrentRow, "adjustmenttype") == 'R' ) {
						rFactor = parseFloat( parent.udfData.getField( iCurrentRow, "adjustment") ).toFixed(3);

					}
					strLabel = strLabel + "<td class='formatted-data-values'>" + pFactor + "</td>"; // Points
					strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>" + lFactor + "</td>"; // Lump Sum
					strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>" + fFactor + marker + "</td>"; // Factor
					strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>" + rFactor + "</td>"; // Rate Add On
					strLabel = strLabel + "</tr>";

				}
			}
		}
	}

	if(strLabel === "") {
		strLabel = "<tr><td colpan='8'>None</td></tr>";
	}
	
	parent.$("tbody#udf_" + iUrlRow).html( strLabel );
	parent.$("td#udf_netadj_percentage_" + iUrlRow).html( ( ( iPercentageSum/parent.udfData.getRowCount() ) * 100 ).toFixed(0) + "%" );
	parent.$("td#udf_netadj_points_" + iUrlRow).html(iPointsSum.toFixed(3));
	parent.$("td#udf_netadj_factor_" + iUrlRow).html(iFactorSum.toFixed(3));
	parent.$("td#udf_netadj_lumpsum_" + iUrlRow).html(iLumpSum.toFixed(3));
	parent.$("td#udf_netadj_rateaddon_" + iUrlRow).html(iRateAddOn.toFixed(3));
}

/*function returnFriendlyType(type) {
	var strValue = "";
	switch(type) {
		case "R":
			strValue = "Rate Add-On";
			break;
		case "L":
			strValue = "Lump Sum Add-On";
			break;
		case "F":
			strValue = "Percentage";
			break;
	}
	return strValue;
}*/

function addAdjRow() {

	parent.udfData.addRows(1);

	iNewRow = parent.udfData.getRowCount() -1;

	//parent.udfData.setField( iNewRow, "item", iUrlRow );
	parent.udfData.setField( iNewRow, "landid", la_id );
	parent.udfData.setField( iNewRow, "percentagefactor", "100" );
	parent.udfData.setField( iNewRow, "selected", "1" );
	parent.udfData.setField( iNewRow, "adjustment", "0" );
	parent.udfData.setField( iNewRow, "defaultadjustment", "1" );
	parent.udfData.setField( iNewRow, "adjustmenttype", "F" );
	parent.udfData.setField( iNewRow, "codetype", "Custom" );
	// This gets flipped to Insert when Finish button clicked.
	parent.udfData.setField( iNewRow, "mode", "InsertTemp" );

	var strHTML = "";
	strHTML = createAdjEntryRow(iNewRow, 'Custom', 'Insert');

	if(iOptionsOnModal <= 0) {
		$("#adjContent").html(strHTML);
	} else {
		$("#adjContent").append(strHTML);
	}

	$(".numeric").numeric();

	iOptionsOnModal++;

	changeSelectsToSelect2s();
}


function changeDepthTable(iRow,obj) {
	// This process is called only when the Depth Table select box has been changed by the user
	var iDepthValue = parent.landData.getField(jsRowNumber,"depth");

	for ( var iCurrentRow = 0; iCurrentRow < parent.udfData.getRowCount(); iCurrentRow++ ) {
		//iCurrentRowItem = parent.udfData.getField(iCurrentRow,"item");
		if ( parent.udfData.getField(iCurrentRow,"item") == iUrlRow ) {
			if ( parent.udfData.getField(iCurrentRow,"mode") != "Delete" ){
				//adjdesc = parent.udfData.getField( iCurrentRow, "codetype");
				if ( parent.udfData.getField( iCurrentRow, "codetype") == "Depths" ) {
					var iDepthTable = parent.udfData.getField( iCurrentRow, "code");
				}
			}
		}
	}

	bGetDepthPercentage = true;

	$('#modalPleaseWait').show();

	if(iDepthValue === null || isNaN(iDepthValue)) {
		// Warn the user that no depth is specified so we need to default the adjustment to 1 (as it won't return results from the proc)
		$('#modalNoDepth').modal('show');
		iDepthValue = 0;
		bGetDepthPercentage = false;
	}

	if(isNaN(iDepthValue) || iDepthTable === null || bGetDepthPercentage === false || iDepthTable === "N/A") {
		depthTableAdjustments = 1;
	} else {
		$.ajax({
	        url:'/cfc/content.cfc',
	        async: false, //This variable makes the function wait until a response has been returned from the page
			data: {
				method		: 'getNewDepthPercentage',
				ParcelID	: ParcelID,
				propYear	: taxYear,
				rowNumber	: iUrlRow,
				depthTable	: iDepthTable,
				depthValue	: iDepthValue
			},
			success: function( data ) {
				depthTableAdjustments = parseFloat(data);
			}
	    });
	}

	/* update WDDX and visual field */
	parent.landData.setField( iRow, "depth", depthTableAdjustments.toFixed(3) );
	parent.$("#depth").val( depthTableAdjustments.toFixed(3) );
	//$('#laadjvalue_' + iRow).val(depthTableAdjustments.toFixed(3));

	createLabel();

	$('#modalPleaseWait').hide();

	$('[data-toggle="tooltip"]').tooltip();
	parent.landUseCalculations(true);
}

function savePopUpChanges() {
	var iErrors = 0;
	var strAttCodeErr = "";
	var strError = "<div class='col-xs-12 formatted-data-values padding10'>Correction required.</div>";

	for ( var iUDFRow = 0; iUDFRow < parent.udfData.getRowCount(); iUDFRow++ ) {
		if ( $("select#" + $("select#adjdesc_"+iUDFRow+ " option:selected").val() + "_"+iUDFRow+ " option:selected").val() == '' ) {
			iErrors++;
			strError = strError + "<div class='col-xs-12 col-sm-4 formatted-data-values' data-sort='" + iUDFRow + "'>Please select an adjustment code for " + $("select#adjdesc_"+iUDFRow+ " option:selected").val() + ".</div>";
		}
	}

	if ( iErrors > 0 ) {
		$("#errorContainer").html(strError);
		$("#errorContainer").addClass("modal-error");
		$("#errorContainer").addClass("bg-danger");
		$("#building-modal-adjustments").scrollTop( 0 )

	} else {
		for ( var iUDFRow = 0; iUDFRow < parent.udfData.getRowCount(); iUDFRow++ ) {
			if ( parent.udfData.getField( iUDFRow, "mode" ) != 'Delete' ) {
				parent.udfData.setField( iUDFRow, "codetype", $("select#adjdesc_"+iUDFRow+ " option:selected").val() );
				parent.udfData.setField( iUDFRow, "landcodeid", $("select#" + $("select#adjdesc_"+iUDFRow+ " option:selected").val() + "_"+iUDFRow+ " option:selected").val() );
				parent.udfData.setField( iUDFRow, "code", $("#s2id_" + $("select#adjdesc_"+iUDFRow+ " option:selected").val() + "_" + iUDFRow + " span.select2-2part-left-building").text()  );
				parent.udfData.setField( iUDFRow, "codedescription", $("#s2id_" + $("select#adjdesc_"+iUDFRow+ " option:selected").val() + "_" + iUDFRow + " span.select2-2part-right-building").text()  );
				parent.udfData.setField( iUDFRow, "percentagefactor", $("#percentageFactor_"+iUDFRow).val() );
				if ( $("#bLFactor_"+iUDFRow).val() == "" ) {
					parent.udfData.setField( iUDFRow, "adjustment", '0' );	
				} else {
					parent.udfData.setField( iUDFRow, "adjustment", $("#bLFactor_"+iUDFRow).val() );	
				}
				if ( parent.udfData.getField( iUDFRow, "mode" ) != 'InsertTemp' ) {
					parent.udfData.setField( iUDFRow, "mode", 'Insert' );
				}
				
			}
		}

		createLabel();
		$('[data-toggle="tooltip"]').tooltip();
		parent.landUseCalculations(true);

		parent.$.fancybox.close();
	}	
}

function cancelChanges() {
	for ( var i = 0; i < parent.udfData.getRowCount(); i++ ) {
		if ( parent.udfData.getField( i, "mode" ) == 'InsertTemp' ) {
			parent.udfData.setField( i, "mode", "Delete" );
		} else {
			parent.udfData.setField( i, "mode", "none" );
		}
	}
	createLabel();
	parent.$.fancybox.close();
}