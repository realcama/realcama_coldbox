function createPacket(pullFromPacket, lookupColumn, lookupValue) {
	// Create a DEEP COPY of the original WDDX packet
	submitPacketName = jQuery.extend(true, {}, pullFromPacket);
	// Go throught he packet removing the rows we are not looking for
	for ( var iCurrentRow = submitPacketName.getRowCount(); iCurrentRow != 0; iCurrentRow-- ) {
		// The WDDX getRowCount is 1 based, so we need to turn it into 0 based for JS calls
		jsRow = iCurrentRow - 1;
		// Pull the value we want to compare and TRIM IT!!
		currentRowValue = $.trim(submitPacketName.getField(jsRow,lookupColumn));
		if(currentRowValue != lookupValue) {
			// Remove this row from the array using SPLICE (native JS call)
			$.each(submitPacketName, function( index, element ) {
				if(typeof element === "object") {
					element.splice(jsRow, 1);
				}
			});
		}
	}
	return submitPacketName;
};
function validateGoToPage(position) {
	if ( $.isNumeric(position) == false || position < 1 ) {
		pageNum = "1";
	} else if ( position > totalPages ) {
		pageNum = totalPages;
	} else {
		pageNum = position;
	}
	return pageNum;
}

function disablePaginationButtons(position) {
	if ( position == "1" ) {
		$("#page-prev").addClass("disabled");
		$("#page-next").removeClass("disabled");
	} else if ( position == totalPages ) {
		$("#page-prev").removeClass("disabled");
		$("#page-next").addClass("disabled");
	} else {
		$("#page-prev").removeClass("disabled");
		$("#page-next").removeClass("disabled");
	}
}

function hideAllPages() {
	for(i=1; i <= totalPages; i++ ) {
		$("#page-" + i).addClass("hide");
	}
}

function showPage(pageNum) {
	$("#page-" + pageNum).removeClass("hide");
	$("#pagination-input").val(pageNum);
}

function pageIncrement(position) {
	if ( (iCurrentPage == 1 && position == -1) || (iCurrentPage == totalPages && position == 1 ) ) {
		if ( position == -1 ) {
			$("#pagination-input").val("1");
		}
	} else {
		if (position == -1) {
			iCurrentPage--;
		} else if (position == 1 ) {
			iCurrentPage++;
		} else if ( $.isNumeric(position) == false ) {
			iCurrentPage = "1";
			position = "-1";
		} else if (position < 1) {
			iCurrentPage = "1";
			position = "-1";
		} else if (position > totalPages) {
			iCurrentPage = totalPages;
			position = "1";
		} else {
			iCurrentPage = position;
		};

		hideAllPages();
		showPage(iCurrentPage);
	}

	disablePaginationButtons(iCurrentPage);
};

function goToPage(pageNum) {
	var iCurrentPage = validateGoToPage(pageNum);

	hideAllPages();

	showPage(iCurrentPage);

	disablePaginationButtons(iCurrentPage);
};

$( document ).ready(function() {
	// Initiate datepicker
	$('input.datefield').datepicker({}).on('changeDate', function(ev){ $(this).datepicker('hide');});
	$('.datefield-yearonly-correlation').datepicker(arDatePickerOptions).on('changeDate', function(ev){ $(this).datepicker('hide');});
	$('.datefield-yearonly-parcel').datepicker(arDatePickerOptions2).on('changeDate', function(ev){ $(this).datepicker('hide');});
	$('.datefield-yearonly').datepicker(arDatePickerOptions2).on('changeDate', function(ev){ $(this).datepicker('hide');});
	$('.datefield-permits').datepicker(arDatePickerOptions4Permits).on('changeDate', function(ev){ $(this).datepicker('hide');});
	//$('.autobox').autobox();
	$('[data-toggle="tooltip"]').tooltip({html: true});

	$("#pagination-input").keydown( function(e) {
	    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	    if(key == 13) {
	        e.preventDefault();
	        iPageTo = $("#pagination-input").val();
	        goToPage(iPageTo);
	    };
	});

	$("#selPageSize").change(function() {
		iPageTo = $("#pagination-input").val();
		$("#page").val( iPageTo );
		$("#page_size").val( $("#selPageSize").val() );
		$("#frmPageSize").submit();
	});

});
