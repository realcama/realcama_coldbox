$(function() {

	$(".numeric").numeric();
	$(".datefield-condo-master").datepicker(arDatePickerOptions2).on('changeDate', function(ev){ $(this).datepicker('hide');});

	changeSelectsToSelect2s();

	$(".showLeft").attr("disabled",true);

	fixUnitTypeCellSizes();
	setInitialTableMode();

});


function doCondoMasterValues(fParcelID, fTaxYear) {


	$('#myPleaseWait').modal('show');
    $.ajax({
		type: "get",
		cache: false,
		url: '/index.cfm/condoMaster/showCondoMasterValues',
		data: {
			parcelID: fParcelID,
			taxYear: fTaxYear
		}, // all form fields
		success: function (data) {
			// on success, post (preview) returned data in fancybox
			$.fancybox(data, {
				// fancybox API options
				'padding' : 0,
				'closeBtn': false,
				helpers : { overlay : {
										locked : true ,
										closeClick: false
									} } /* this prevents screen from shifting */
			}); // fancybox
			$('#myPleaseWait').modal('hide');
		} // success
    }); // ajax
}


function setInitialTableMode() {
	/* When the pages loads we only want to show the first 5 columns, everything else needs to be hidden */
	for(j=8;j < 15; j++) {
		$(".col_" + j).hide();
	}
}

function fixUnitTypeCellSizes() {
	/* Display all of the cells since they have to be visable to get the height/width measurements */
	var arData = [];

	/* Set the state of the columsn and then turn them on */
	for(j=1;j < 13; j++) {
		arData[j-1] = $(".col_" + j).is(':visible');
		$(".col_" + j).show();
	}
	/* Remove all of the STYLE tags on the table TDs */
	$("#condo-master-unit-types tbody td").each(function() {
		$(this).attr("style","");
	});

	/* find the maximum height of ANY cell in this table and then set all the cells to this height */
	var maxHeight = 0;

	$("#condo-master-unit-types tbody td").each(function() {
	    maxHeight = Math.max($(this).height(), maxHeight);
	}).height(maxHeight-11);

	/* find the maximum width of ANY cell in this table and then set all the cells to this width */
	/* ***DOUG wants this off***
	var maxWidth = 0;
    $("#condo-master-unit-types tbody td").each(function() {
        maxWidth = Math.max($(this).width(), maxWidth);
    }).width(maxWidth);
	*/

	/* turn off the columns that were previously hidden */
	for(j=1;j < 13; j++) {
		if(arData[j-1] == false) {
			$(".col_" + j).hide();
		}
	}


}

var iEdit= 0;

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format3Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";
}

function format3PieceSmallLeft(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left-small formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle-small formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right-small formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";
}


/* 2017.12.07 - TDD - Moved to formatSelect.js
function changeSelectsToSelect2s(){
	$('.select2-2piece-new').select2({
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	// Remove the start up class, so if we have to create a new row on the fly, recalling the select2 init code won't cause problems in already built ones.
	$( ".select2-2piece-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-new").addClass("select2-2piece");
	});

	$('.select2-3piece-new').select2({
		formatResult: format3Piece,
		formatSelection: format3Piece,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-new").addClass("select2-3piece");
	});

	$('.select2-3piece-smallleft-new').select2({
		formatResult: format3PieceSmallLeft,
		formatSelection: format3PieceSmallLeft,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-smallleft-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-smallleft-new").addClass("select2-3piece-smallleft");
	});
}*/


function changeVal(packet, iRow, obj) {
	//eval(packet).setField( iRow, obj.name, inVal );

	if ( obj.name == "improvementtype" ) {
		eval(packet).setField( iRow, "improvementcode", $(obj).val() );
		eval(packet).setField( iRow, "improvementcodedescription", $("#s2id_" + obj.name + " span.select2-2part-right-building").html() );
	} else if ( obj.name == "quality" ) {
		eval(packet).setField( iRow, "qualitycode", $(obj).val() );
		eval(packet).setField( iRow, "qualitycodedescription", $("#s2id_" + obj.name + " span.select2-2part-right-building").html() );
	}

	// 2017.12.07 - TDD - Don't think this is used anymore
	if(packet == "unitTypesData") {
		eval(packet).setField(iRow,"mode","Edit"); // Set the mode to EDIT so that we only process this row
		cfRow = iRow + 1;

		if(obj.name == "floorfrom" || obj.name == "floorto") {
			floorFrom = eval(packet).getField(iRow,"floorfrom");
			floorTo = eval(packet).getField(iRow,"floorto");
			$(".floorfrom_" + cfRow).html(floorFrom + " thru " + floorTo);
		} else {
			$("." + obj.name + "_" + cfRow).html( $(obj).val() );
		}
	}
	// Make sure that numeric values are NOT null value or blank spaces
	noNulls();
}




function saveWDDX(tab) {

	wddxSerializer = new WddxSerializer();


	switch(tab){
		case "info":
			wddxData1 = wddxSerializer.serialize(masterData);
			break;
		case "situs":
			wddxData1 = wddxSerializer.serialize(situsData);
			break;
		case "legal":
			wddxData1 = wddxSerializer.serialize(legalData);
			break;
		case "unitTypes":
			wddxData1 = wddxSerializer.serialize(unitTypesData);
			wddxData2 = wddxSerializer.serialize(unitTypicalCodesData);
			$("#packet2").val(wddxData2);

			break;
	}
	$("#packet").val(wddxData1);
	$("#tab").val(tab);

	$("#frmSave").submit();
}



var iLowestShownColumn = 1;

function moveRight() {

	if(iLowestShownColumn < 6) {
		$(".showLeft").attr("disabled",false);

		$(".col_" + iLowestShownColumn).hide();
		$(".col_" + (iLowestShownColumn + 7)).show();
		iLowestShownColumn++;

		if(iLowestShownColumn == 6) {
			$(".showRight").attr("disabled",true);
		}
	}

}

function moveLeft() {

	if(iLowestShownColumn != 1) {
		$(".showRight").attr("disabled",false);

		iLowestShownColumn--;

		$(".col_" + iLowestShownColumn).show();
		$(".col_" + (iLowestShownColumn + 7)).hide();

		if(iLowestShownColumn == 1) {
			$(".showLeft").attr("disabled",true);
		}
	}

}

function editUnitTypes(condoID,condoTypicalID,cfRow) {
	$('#myPleaseWait').modal('show');
    $.ajax({
		type: "get",
		cache: false,
		url: "/index.cfm/condoMaster/showCondoMasterUnitTypesPopup/",
		//url: "showCondoMasterUnitTypesPopup.cfm",
		data: {
			taxYear: taxYear,
			condoID: condoID,
			condoTypicalID: condoTypicalID,
			row: cfRow
		}, // all form fields
		success: function (data) {
			// on success, post (preview) returned data in fancybox
			$.fancybox(data, {
				// fancybox API options
				'padding' : 0,
				'closeBtn': false,
				helpers : { overlay : {
										locked : true ,
										closeClick: false
									} } /* this prevents screen from shifting */
			}); // fancybox
			$('#myPleaseWait').modal('hide');
		} // success
    }); // ajax

}


function massBaseValueUpdater() {
	$('#myPleaseWait').modal('show');

	 $.ajax({
		type: "get",
		cache: false,
		url: "/index.cfm/condoMaster/showCondoMasterBaseValueUpdaterPopup/",
		//url: "showCondoMasterBaseValueUpdaterPopup.cfm",
		data: {
			taxYear: taxYear,
			parcelID: parcelID,
			parcelType: parcelType
		}, // all form fields
		success: function (data) {
			// on success, post (preview) returned data in fancybox
			$.fancybox(data, {
				// fancybox API options
				'padding' : 0,
				'closeBtn': false,
				helpers : { overlay : {
										locked : true ,
										closeClick: false
									} } /* this prevents screen from shifting */
			}); // fancybox
			$('#myPleaseWait').modal('hide');
		} // success
    }); // ajax

}

// This was imported from header.js file on old dev site. It may be a global function.
function noNulls() {
	$(".numeric").each(function(index) {
		thisVal = $.trim($( this ).val());
		
		if(isNaN(thisVal) || thisVal.length == 0){
			// Set the value to ZERO so it's not a null value
			$( this ).val(0);
		}		
	});	
}
