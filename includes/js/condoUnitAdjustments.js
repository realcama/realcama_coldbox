function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function returnFriendlyType(type) {
	var strValue = "";
	switch(type) {
		case "R":
			strValue = "Rate Add-On";
			break;
		case "L":
			strValue = "Lump Sum Add-On";
			break;
		case "F":
			strValue = "Percentage";
			break;
		case "P":
			strValue = "Points";
			break;
	}
	return strValue;
}

function changeUDFMethod(obj) {
	iRowToSetSelects = $(obj).data("rownumber");

	turnOffSelects(iRowToSetSelects);
	showOption(obj.value,iRowToSetSelects);

}

function removeAdjRow(iRow) {
	var choice = confirm("Are you sure you want to delete this row?");
	if(choice == true) {
		parent.condoUnitCodesData.setField( iRow, "mode", "Delete" );
		$("#condounitcoderow_" + iRow).remove();
		createLabel();
		$('[data-toggle="tooltip"]').tooltip();
	}
}

function addAdjRow() {

	parent.condoUnitCodesData.addRows(1);

	iNewRow = parent.condoUnitCodesData.getRowCount() -1;
	parent.condoUnitCodesData.setField( iNewRow, "condounitid", condoUnitID );
	parent.condoUnitCodesData.setField( iNewRow, "mode", "Insert" );
	parent.condoUnitCodesData.setField( iNewRow, "adjustmentfactor", 0 );
	parent.condoUnitCodesData.setField( iNewRow, "adjustmentamount", 0 );

	var strHTML = "";
	strHTML = createAdjEntryRow(iNewRow, '', '', 'Insert');

	if(iOptionsOnModal <= 0) {
		$("#adjContent").html(strHTML);
	} else {
		$("#adjContent").append(strHTML);
	}

	$(".numeric").numeric();

	iOptionsOnModal++;

	changeSelectsToSelect2s();
}

function buildModal() {
	for ( var iCurrentRow = 0; iCurrentRow < parent.condoUnitCodesData.getRowCount(); iCurrentRow++ ) {

		mode = parent.condoUnitCodesData.getField(iCurrentRow,"mode");

		if ( mode != "Delete" ) {

			codeID = parent.condoUnitCodesData.getField(iCurrentRow,"condocodeid");
			codetype = parent.condoUnitCodesData.getField(iCurrentRow,"codetype");

			var strHTML = "";
			strHTML = createAdjEntryRow(iCurrentRow, codeID, codetype, 'Edit');

			if(iOptionsOnModal === 0) {
				$("#adjContent").html(strHTML);
			} else {
				$("#adjContent").append(strHTML);
			}

			iOptionsOnModal++;
		}
	}

	changeSelectsToSelect2s();
	$('[data-toggle="tooltip"]').tooltip();

	if(iOptionsOnModal == 0) {
		$("#adjContent").html("No condo unit codes defined");
	}
}

