function addAdjRow() {
	var strHTML = "";

	parent.buildingFeaturesData.addRows(1);
	iNewRow = parent.buildingFeaturesData.getRowCount() -1;

	parent.buildingFeaturesData.setField( iNewRow, "buildingID", parent.buildingData.getField(jsRowNumber, "buildingID") );
	parent.buildingFeaturesData.setField( iNewRow, "itemNumber", parent.buildingData.getField(jsRowNumber, "itemNumber") );
	parent.buildingFeaturesData.setField( iNewRow, "buildingCodeJoinID", "" );
	parent.buildingFeaturesData.setField( iNewRow, "feature", "" );
	parent.buildingFeaturesData.setField( iNewRow, "percentageFactor", "0" );
	parent.buildingFeaturesData.setField( iNewRow, "mode", "Insert" );

	strHTML = createAdjEntryRow(iNewRow, 'ExteriorWall', 'Insert', 999);
	$("#adjContent").append(strHTML);
	$(".numeric").numeric();

	changeSelectsToSelect2s();

	iOptionsOnModal++;

	var d = $("#building-modal-adjustments");
	d.scrollTop(d.prop("scrollHeight"));
}

function changeImproveTypeVal(iRow,obj,attrType) {
	var selectedOption = $(obj).find('option:selected');
	var strDescription = selectedOption.data('adjds');
	var strBuildingCode = selectedOption.data('adjcd');
	var strAdjustmentType = selectedOption.data('adjustmenttype');
	var strValue = $(obj).val();

	parent.buildingFeaturesData.setField( iRow, "buildingCodeID", strValue );					// Code value
	parent.buildingFeaturesData.setField( iRow, "buildingCode", strBuildingCode );				// Building Code Display value
	parent.buildingFeaturesData.setField( iRow, "buildingCodeDescription", strDescription); 	// Building Code Description value
	if (parent.buildingFeaturesData.getField(iRow, "mode") != "Insert") {
		parent.buildingFeaturesData.setField( iRow, "mode", "Edit" );
	}

	$("#adjustmenttype_" + iRow).val(strAdjustmentType);
	$("#s2id_adjustmenttype_" + iRow).select2("val", strAdjustmentType);
	
	createLabel();
	
	/* Loops thru wddx packet created in the index template. Look for the matching
	 * value in the record set. Populate the appropriate input box based on
	 * the adjustment type; 
	 */
	$("#bLPoints_" + iRow).val(0);
	$("#bLLumpSum_" + iRow).val(0);
	$("#bLFactor_" + iRow).val(0);
	$("#bLRateAddOn_" + iRow).val(0);
	for ( var iCurrentRow = 0; iCurrentRow < parent.allBuildingFeatures.getRowCount(); iCurrentRow++ ) {
		if ( parent.allBuildingFeatures.getField(iCurrentRow,"buildingID") == $(obj).val() ) {
			// Resets the input boxes
			$("#bLPoints_" + iRow).val();
			$("#bLLumpSum_" + iRow).val();
			$("#bLFactor_" + iRow).val();
			$("#bLRateAddOn_" + iRow).val();
			
			parent.buildingFeaturesData.setField( iRow, "adjustment", parseFloat( parent.allBuildingFeatures.getField(iCurrentRow,"adjustment") ) );
			parent.buildingFeaturesData.setField( iRow, "adjustmenttype", parent.allBuildingFeatures.getField(iCurrentRow,"adjustmenttype") );

			//Populates the input box
			switch ( parent.allBuildingFeatures.getField(iCurrentRow,"AdjustmentType") ) {
				case 'P':
					$("#bLPoints_" + iRow).val( parseFloat( parent.allBuildingFeatures.getField(iCurrentRow,"Adjustment") ).toFixed(0) );
					break;
				case 'L':
					$("#bLLumpSum_" + iRow).val( parseFloat( parent.allBuildingFeatures.getField(iCurrentRow,"Adjustment") ) );
					break;
				case 'F':
					$("#bLFactor_" + iRow).val( parseFloat (parent.allBuildingFeatures.getField(iCurrentRow,"Adjustment") ).toFixed(2) );
					break;
				case 'R':
					$("#bLRateAddOn_" + iRow).val( parseFloat( parent.allBuildingFeatures.getField(iCurrentRow,"Adjustment") ).toFixed(3) );
					break;
			}
			break;
		}
	
	}
}

function removeAdjRow(iRow) {
	var choice = confirm("Are you sure you want to delete this row?");
	if(choice == true) {
		parent.buildingFeaturesData.setField( iRow, "mode", "Delete" );
		$("#adjRow_" + iRow).remove();
		createLabel();
		$("#addRowButton").show();
	}
}
/* This function is called when the Adjustments select box changes value */ 
function changeUDFMethod(obj) {
	iRowToSetSelects = $(obj).data("rownumber");
	turnOffSelects(iRowToSetSelects);
	showOption(obj.value,iRowToSetSelects); //Passes option value and javascript row number
}

// Comment out. wddx packet should not update until hit finish
/*function changeImproveClassVal(iRow,obj) {
	parent.buildingFeaturesData.setField( iRow, "feature", $(obj).val() );

	if (parent.buildingFeaturesData.getField(iRow, "mode") != "Insert") {
		parent.buildingFeaturesData.setField( iRow, "mode", "Edit" );
	}
	createLabel();
}*/

function removeAdjRow(iRow) {
	var choice = confirm("Are you sure you want to delete this row?");
	if(choice == true) {
		parent.buildingFeaturesData.setField( iRow, "mode", "Delete" );
		$("#adjRow_" + iRow).remove();
		createLabel();
	}
}
// Called when percentage value changed. Removed from input
/*function changeAdjVal(iRow,obj) {
	parent.buildingFeaturesData.setField( iRow, obj.name, $(obj).val() );

	if (parent.buildingFeaturesData.getField(iRow, "mode") != "Insert") {
		parent.buildingFeaturesData.setField( iRow, "mode", "Edit" );
	}

	createLabel();
}*/

/* This is called in the changeUDFMethod function that is located in the buildingLineAdjustments.js.
* I think the s2id_ id value is a dynamic value generated by changeselettoselect plugin. */
function turnOffSelects(iRowToSetSelects){
	for ( var i = 0; i < arrBuildingAttributes.length; i++ ) {
		$("#s2id_" + arrBuildingAttributes[i] + "_" + iRowToSetSelects).hide();
		$("#s2id_" + arrBuildingAttributes[i] + "_" + iRowToSetSelects).val(-1);
	}
}

// Called in the changeUDFMethod function
function showOption(method,iRowToSetSelects) {
	$("#s2id_" + method + "_" + iRowToSetSelects).show();
	$("#s2id_" + method + "_" + iRowToSetSelects).css("display","inline-block");
}

// Called in createLabel and document ready functions
function changeZerosToOneHundreds() {
	/* Set improve percentage for attributes that only have 1 attribute to 100% (instead of showing 0).
	* parent.buildingFeaturesData is created by the CFWDDX in index template
	*/
	var suffixDisplayCount = "_display_count";
	
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
		// Hard code to 1 for now until we test with multiple building items
		blatitem = jsBlItem;
		strMode = parent.buildingFeaturesData.getField(iCurrentRow,"mode");
		strCurrentAdjValue = parseFloat(parent.buildingFeaturesData.getField(iCurrentRow,"percentageFactor"));
		strCurrentType = parent.buildingFeaturesData.getField(iCurrentRow,"feature");

		if(blatitem == jsRcRow) {
			if(strMode != "Delete" && strCurrentType != "N/A") {
				// If there is only one of this adjustment type for this land line, and the percentage is either 0 or blank(i.e. a null value), default it to 100%
				//if ( eval(strCurrentType+"_display_count") == 1 && (isNaN(strCurrentAdjValue) || strCurrentAdjValue == 0) ) {
				if ( this[strCurrentType + "_display_count"] == 1 && (isNaN(strCurrentAdjValue) || strCurrentAdjValue == 0) ) {
					parent.buildingFeaturesData.setField(iCurrentRow,"percentageFactor", 100);
					$("#percentageFactor_" + iCurrentRow).val(100);
				}
			}
		}
	}
}

//Stores values into an array. Used to reset packet values if there is an error.
function getPacketValues() {
	var arrPacketValues = [];
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
		arrPacketValues[iCurrentRow] = parseFloat(parent.buildingFeaturesData.getField(iCurrentRow,"percentageFactor"));
	}
	return arrPacketValues;
}

//Compares input values against packet value. Update packet with pop-up values if different.
//Flags row as Edit. Rows flagged as Edit will be updated.
function updatePacketValues() {
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
		if ( $("#percentageFactor_"+iCurrentRow).val() != parseFloat(parent.buildingFeaturesData.getField(iCurrentRow,"percentageFactor")) ) {
			parent.buildingFeaturesData.setField( iCurrentRow, "percentageFactor", $("#percentageFactor_"+iCurrentRow).val() );
			if (parent.buildingFeaturesData.getField( iCurrentRow, "mode" ) != "Insert") {
				parent.buildingFeaturesData.setField( iCurrentRow, "mode", "Edit" );
			}
		}

		// Check for changes in the adjustments select box
		if ( $("#improve_class_"+iCurrentRow).val() != parent.buildingFeaturesData.getField(iCurrentRow,"feature") ) {
			parent.buildingFeaturesData.setField( iCurrentRow, "feature", $("#improve_class_"+iCurrentRow).val() );
			if (parent.buildingFeaturesData.getField( iCurrentRow, "mode" ) != "Insert") {
				parent.buildingFeaturesData.setField( iCurrentRow, "mode", "Edit" );
			}

		}


	}
}

//Resets packet values if there is an error.
function resetPacketPercentages(arr) {
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
		parent.buildingFeaturesData.setField( iCurrentRow, "percentageFactor", arr[iCurrentRow] );
		parent.buildingFeaturesData.setField( iCurrentRow, "mode", "none" );
	}
}

// Loop thru selected adjustments, then checks selected value for code
function checkCodeSelection() {
	var aErrors = new Array();

	for ( var i = 0; i < parent.buildingFeaturesData.getRowCount(); i++ ) {
		if ( $("#" + $("#improve_class_" + i).val() + "_" + i).val() == '' ) {
			aErrors.push( $("#improve_class_" + i).val() );
		}
	}
	return aErrors;
}

// Called when Save button is clicked.
function doCalcs() {
	//Get values from packet for temp storage
	var arrPacketValues = getPacketValues();

	var strAttCodeErr = "";
	var strError = "<div class='col-xs-12 formatted-data-values padding10'>Correction required. One or more adjustments entered do not total 100%</div>";
	var iErrors = 0;


	//Update packet with values from pop up
	updatePacketValues();

	// Call the label function so that the values for all the adjustments have been set (this is needed to be done when the modal is loaded and then closed)
	createLabel();

	// Loops thru array of attribute types
	for ( var i = 0; i < arrBuildingAttributes.length; i++ ) {

		// Checks if an adjustment type has an entry in the popup
		if ( this[arrBuildingAttributes[i]+"_display_count"] > 0 ) {

			// Checks if the adjustment total percentage is not equal to 100
			if ( this[arrBuildingAttributes[i]] != 100 ) {

				switch ( arrBuildingAttributes[i] ) {
					case 'ExteriorWall':
						//var strTypeName = "Exterior Walls";
						var iModalSortOrder = 2;
						break;
					case 'Frame':
						//var strTypeName = "Frame";
						var iModalSortOrder = 5;
						break;
					case 'Foundation':
						//var strTypeName = "Foundation";
						var iModalSortOrder = 4;
						break;
					case 'RoofStructure':
						//var strTypeName = "Roof Structure";
						var iModalSortOrder = 10;
						break;
					case 'RoofCover':
						//var strTypeName = "Roof Cover";
						var iModalSortOrder = 9;
						break;
					case 'Floor':
						//var strTypeName = "Flooring";
						var iModalSortOrder = 3;
						break;
					case 'InteriorWall':
						//var strTypeName = "Interior Walls";
						var iModalSortOrder = 7;
						break;
					case 'Heat':
						//var strTypeName = "Heat";
						var iModalSortOrder = 6;
						break;
					case 'AirConditioning':
						//var strTypeName = "Air Conditioning";
						var iModalSortOrder = 1;
						break;
					case 'Kitchen':
						//var strTypeName = "Kitchen";
						var iModalSortOrder = 8;
						break;
					case 'Windows':
						//var strTypeName = "Windows";
						var iModalSortOrder = 11;
						break;
				}
				strError = strError + "<div class='col-xs-4 formatted-data-values' data-sort='" + iModalSortOrder + "'>" + getAttributeDisplayName( arrBuildingAttributes[i] ) + " is at " + this[arrBuildingAttributes[i]] + "/100%</div>";
				iErrors++;
			}
		}
	}

	strAttCodeErr = checkCodeSelection();
	
	if ( strAttCodeErr.length > 0 ) {
		for ( var i = 0; i < strAttCodeErr.length; i++ ) {
			strError = strError + "<div class='col-xs-6 formatted-data-values' data-sort='12'>Please select a code for the " + getAttributeDisplayName( strAttCodeErr[i] ) + " attribute.</div>";
		}
		iErrors++;
	}
	
	if(iErrors != 0) {
		$("#errorContainer").html(strError);
		$("#errorContainer").addClass("modal-error");
		$("#errorContainer").addClass("bg-danger");
		//$("#building-modal-adjustments").scrollTop( 0 )
		

		//Reset packet values
		resetPacketPercentages(arrPacketValues);

		//Reset the display
		createLabel();
	} else {

		updateFeatureOrig();
		parent.$.fancybox.close();
	}
}

var arrFeatWDDXKeys = ["buildingcodejoinid","feature","buildingcode","buildingcodeid","buildingcodedescription","percentagefactor","mode","adjustment","adjustmenttype"];
// Updated the wddx packet that is used when cancel button is clicked.
function updateFeatureOrig() {
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
		for ( var i = 0; i < arrFeatWDDXKeys.length; i++ ) {
			parent.buildingFeaturesDataOrig.setField( iCurrentRow, arrFeatWDDXKeys[i], parent.buildingFeaturesData.getField( iCurrentRow, arrFeatWDDXKeys[i] ) );	
		}
	}
}

function rollBackFeatures() {
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
		for ( var i = 0; i < arrFeatWDDXKeys.length; i++ ) {
			parent.buildingFeaturesData.setField( iCurrentRow, arrFeatWDDXKeys[i], parent.buildingFeaturesDataOrig.getField( iCurrentRow, arrFeatWDDXKeys[i] ) );	
		}
	}
	doCalcs();
}

function getAttributeDisplayName(strAttribute) {

	switch ( strAttribute ) {
		case 'ExteriorWall':
			var strTypeName = "Exterior Walls";
			break;
		case 'Frame':
			var strTypeName = "Frame";
			break;
		case 'Foundation':
			var strTypeName = "Foundation";
			break;
		case 'RoofStructure':
			var strTypeName = "Roof Structure";
			break;
		case 'RoofCover':
			var strTypeName = "Roof Cover";
			break;
		case 'Floor':
			var strTypeName = "Flooring";
			break;
		case 'InteriorWall':
			var strTypeName = "Interior Walls";
			break;
		case 'Heat':
			var strTypeName = "Heat";
			break;
		case 'AirConditioning':
			var strTypeName = "Air Conditioning";
			break;
		case 'Kitchen':
			var strTypeName = "Kitchen";
			break;
		case 'Windows':
			var strTypeName = "Windows";
			break;
	}

	return strTypeName;
}
/*
* The buildModal function gets the query data from the parent window (index.cfm).
* Then loops thru data to populate form fields in the modal window.
* parent.buildingFeaturesData: Look at CFWDDX tag in index.cfm
* */
function buildModal() {
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingFeaturesData.getRowCount(); iCurrentRow++ ) {
		blatitem = jsBlItem;

		strCurrentType = parent.buildingFeaturesData.getField(iCurrentRow,"feature");
		strCurrentCodeID = parent.buildingFeaturesData.getField(iCurrentRow,"buildingCodeID");
		strCurrentAdjValue = parseFloat(parent.buildingFeaturesData.getField(iCurrentRow,"percentageFactor"));

		if(isNaN(strCurrentAdjValue))  strCurrentAdjPercent = 0;

		if(blatitem == jsBlItem) {
			//Make sure this entry is not a delete entry
			if( parent.buildingFeaturesData.getField(iCurrentRow,"mode") != "Delete" ) {

				var strHtml = "";
				strHTML = createAdjEntryRow(iCurrentRow, strCurrentType, 'Edit');
				$("#adjContent").append(strHTML);

				/* Set the values for the Adjustments select box that was created */
				$("#improve_class_"+ iCurrentRow).val(strCurrentType); /* Type */
				strDropDown = "#" + strCurrentType + "_"+ iCurrentRow;

				if(strCurrentCodeID != null && strCurrentCodeID.length != 0) {
					/* Attribute value */
					$(strDropDown + " option[value='" + strCurrentCodeID + "']").prop('selected', true);
				}

				// Populates the percentage inputs
				$("#percentageFactor_" + iCurrentRow).val(strCurrentAdjValue); /* Adjustment Amount*/

				// Default inputs to zero.
				$("#bLPoints_" + iCurrentRow).val( 0 );
				$("#bLLumpSum_" + iCurrentRow).val( 0 );
				$("#bLFactor_" + iCurrentRow).val( 0 );
				$("#bLRateAddOn_" + iCurrentRow).val( 0 );

				// Populates the Points, Lump Sum, Factor, and Rate Add-On inputs.
				switch ( parent.buildingFeaturesData.getField(iCurrentRow,"AdjustmentType") ) {
					case 'P':
						$("#bLPoints_" + iCurrentRow).val( parseFloat( parent.buildingFeaturesData.getField(iCurrentRow,"Adjustment") ).toFixed(0) );
						break;
					case 'L':
						$("#bLLumpSum_" + iCurrentRow).val( parseFloat( parent.buildingFeaturesData.getField(iCurrentRow,"Adjustment") ) );
						break;
					case 'F':
						$("#bLFactor_" + iCurrentRow).val( parseFloat (parent.buildingFeaturesData.getField(iCurrentRow,"Adjustment") ).toFixed(2) );
						break;
					case 'R':
						$("#bLRateAddOn_" + iCurrentRow).val( parseFloat( parent.buildingFeaturesData.getField(iCurrentRow,"Adjustment") ).toFixed(3) );
						break;
				}

				// Set the values for the select boxes in the Code\Description Column
				for ( var i = 0; i < arrBuildingAttributes.length; i++ ) {
					if ( arrBuildingAttributes[i] == strCurrentType ) {
						$("#" + arrBuildingAttributes[i] + "_" + iCurrentRow).val(strCurrentCodeID);
					} else {
						$("#" + arrBuildingAttributes[i] + "_" + iCurrentRow).val();
					}
				}

				iOptionsOnModal++;

			} /* If 'Delete' */
		}	/* If CurrentRowItem */
	} /* For Loop */

	if(iOptionsOnModal == 0) {
		addAdjRow();
	}
	changeSelectsToSelect2s();
}


/*function format2PieceType(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left-building-typecode formatted-data-values'>" + $(originalOption).data('dsname') + "</span>";
}
function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function changeSelectsToSelect2s() {
	// turn all of the select2 boxes into formatted select boxes
	$('.select2-2piece-dropdown-small-new').select2({
		width: "200px",
		formatResult: format2PieceType,
		formatSelection: format2PieceType,
		escapeMarkup: function(m) { return m; }
	});
	// Go through all of the DOM elements and change "select2-dropdown-new" to "select2-dropdown" (remove the"-new" part of the class so subsequent sweeps don't hit them
	$( ".select2-2piece-dropdown-small-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-dropdown-small-new").addClass("select2-2piece-dropdown-small");
	});

	$('.select2-2piece-dropdown-new').select2({
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-2piece-dropdown-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-dropdown-new").addClass("select2-2piece-dropdown");
	});
}*/
