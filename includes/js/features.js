$(function() {		
	$(".numeric").numeric();	
	$(".datefield").datepicker();
	changeSelectsToSelect2s();
});	

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format3PieceSmallLeft(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left-small formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle-small formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right-small formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";	   
}

function changeVal(iRow,obj) {			
	/* Updates the WDDX packet*/
	inVal = $(obj).val();
	inVal = inVal.replace('$','');
	inVal = inVal.replace(',','');		 
		
	extraFeaturesData.setField(iRow,obj.name,inVal);				
}	


function changeCheckboxVal(iRow,obj) {
	if(obj.checked == true) {
		extraFeaturesData.setField(iRow,obj.name,"Y");
	} else {
		extraFeaturesData.setField(iRow,obj.name,"N");	
	}
}


function saveWDDX(xfitem) {
	/* Pull data out of the WDDX packets that are not relevant to the row we want to submit*/
	featuresRow = createPacket(extraFeaturesData, "itemNumber", xfitem);
	
	/* Serialize the new WDDX packets */
	wddxSerializer = new WddxSerializer(); 
	wddxExtraFeaturesData = wddxSerializer.serialize(featuresRow);	
	$("#extraFeaturesData").val(wddxExtraFeaturesData);	
	
	$("#frmSave").submit();
	
}

function comingSoon() {	
	$("#myComingSoon").modal('show');
};
