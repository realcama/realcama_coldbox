function format1Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-1part-left-building-type formatted-data-values'>" + $(originalOption).data('desc') + "</span>";
}
function format2Piece1Hide(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc') + "</span><span class='formatted-data-values select2-2part-hidden-1 hidden'>" + $(originalOption).data('adjdata1') + "</span>";
}
function format2Piece2Hide(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc') + "</span><span class='formatted-data-values select2-2part-hidden-1 hidden'>" + $(originalOption).data('adjdata1') + "</span><span class='formatted-data-values select2-2part-hidden-2 hidden'>" + $(originalOption).data('adjdata2') + "</span>";
}

function format3Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-middle formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-right formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";	   
}

function changeSelectsToSelect2s(){
	$('.select2-1piece-new').select2({
		formatResult: format1Piece,
		formatSelection: format1Piece,
		escapeMarkup: function(m) { return m; }
	});
	// Remove the start up class, so if we have to create a new row on the fly, recalling the select2 init code won't cause problems in already built ones.
	$( ".select2-1piece-new" ).each(function( ) {
		$(this).removeClass("select2-1piece-new").addClass("select2-1piece");
	});

	// format2Piece is in each cfm template until it is standardized.
	$('.select2-2piece-new').select2({
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-2piece-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-new").addClass("select2-2piece");
	});
	
	$('.select2-3piece-new').select2({
		formatResult: format3Piece,
		formatSelection: format3Piece,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-new").addClass("select2-3piece");
	});

	/*$('.select2-3piece-smallleft-new').select2({
		formatResult: format3PieceSmallLeft,
		formatSelection: format3PieceSmallLeft,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-smallleft-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-smallleft-new").addClass("select2-3piece-smallleft");
	});*/
	
	// Used only on features?
	$('.select2-2piece-search-new').select2({
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-2piece-search-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-search-new").addClass("select2-2piece-search");
	});

	$('.select2-2piece-1hidden').select2({
		formatResult: format2Piece1Hide,
		formatSelection: format2Piece1Hide,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-2piece-1hidden" ).each(function( ) {
		$(this).removeClass("select2-2piece-1hidden").addClass("select2-3piece");
	});

	$('.select2-2piece-2hidden').select2({
		formatResult: format2Piece2Hide,
		formatSelection: format2Piece2Hide,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-2piece-2hidden" ).each(function( ) {
		$(this).removeClass("select2-2piece-2hidden").addClass("select2-3piece");
	});

}