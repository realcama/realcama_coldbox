var iMasterRow = 0;
var objMaterObj = "";

function resetMasters() {
	iMasterRow = 0;
	objMaterObj = "";
}

function killBuildingChange() {
	/* the user doesn't want to change the Building Type, so undo the change they just did  (reset the Building Type select box) */
	$(objMaterObj).select2("val",buildingData.getField(iMasterRow,objMaterObj.name));
	$('#building-type-change').modal('hide');
	resetMasters();
}


//function updateBuildingValue() {
//	/* This process is called only when the Building Type select box has been changed by the user and the user selected that they DO want to change the type */
//	changeVal(iMasterRow,objMaterObj);
//	$('#building-type-change').modal('hide');

//	blmodl = $(objMaterObj).val();

//	// Clear all of the HTML for the options that use BLMODL
//	$("#blqual_" + iMasterRow).html("");
//	$("#blarch_" + iMasterRow).html("");
//	$("#blpkm_" + iMasterRow).html("");
//	$("#blceil_" + iMasterRow).html("");
//	$("#blclas_" + iMasterRow).html("");
//	$("#blcond_" + iMasterRow).html("");
//	$("#blocc_" + iMasterRow).html("");

	// We need to pass in the values that the dropdowns are, so that if that selection currently in place does
	// not exist in the new dropdown options, we have to show the user that they need to make a new selection

	// Populate all of the HTML for the options that use BLMODL
//	$('#myPleaseWait').modal('show');
//	$.ajax({
//		type: "get",
//		cache: false,
//		async: false, //This variable makes the function wait until a response has been returned from the page
//		url: "/cfc/content.cfc?method=getUpdatedBuildingTypeDropdownSelections",
//		data: {
//			blmodl: blmodl,
//			taxYear: taxYear,
//			iRowNumber: iMasterRow,
//			inQual: changeNullToZero(buildingData.getField(iMasterRow,"blqual")),
//			inArch: changeNullToZero(buildingData.getField(iMasterRow,"blarch")),
//			inPlumFix: changeNullToZero(buildingData.getField(iMasterRow,"blpkm")),
//			inCeil: changeNullToZero(buildingData.getField(iMasterRow,"blceil")),
//			inClas: changeNullToZero(buildingData.getField(iMasterRow,"blclas")),
//			inCond: changeNullToZero(buildingData.getField(iMasterRow,"blcond")),
//			inOcc: changeNullToZero(buildingData.getField(iMasterRow,"blocc"))
//		}, // all form fields
//		success: function (data) {
//			eval(data);
//		} // success
//   }); // ajax

	// Build the new select2 boxes we just created
//	formatSelectsToSelect2();
//	$('#myPleaseWait').modal('hide');

	// Set a tooltip to show the user that they need to select a new value (have the tooltip showing all the time, not just on rollover)
//	$( ".select2-container" ).each(function( ) {
//		if($(this).hasClass("fix-me-i-am-an-error")){
//			$(this).attr("data-toggle", "tooltip2");
//			$(this).prop("title", "Please select a new value");
//			$(this).tooltip({html: true}).tooltip('show');
//		}
//	});

//	resetMasters();
//}



function unitBuildingTypeChange(iRow,obj) {
	/* This process is called only when the Building Type select box has been changed by the user */
	iMasterRow = iRow;
	objMaterObj = obj;
	$('#building-type-change').modal('show');
}

function changeNullToZero(inVal) {
	inVal = inVal.trim();
	if(inVal.length == 0) {
		inVal = -999;
	}
	return inVal;
}

function doBuildingPopup(row) {
	blmodl = buildingData.getField((parseInt(row)-1), "modelNumber");
	blitem = buildingData.getField((parseInt(row)-1), "itemNumber");
	buildingid	= buildingData.getField((parseInt(row)-1), "buildingid");

	$('#myPleaseWait').modal('show');
    $.ajax({
		type: "get",
		cache: false,
		url: "/index.cfm/building/showBuildingPopup/",
		data: {
			taxYear: taxYear,
			buildingid: buildingid,
			row: row,
			blmodl: blmodl,
			blitem: blitem
		}, // all form fields
		success: function (data) {
			// on success, post (preview) returned data in fancybox
			$.fancybox(data, {
				// fancybox API options
				'padding' : 0,
				'closeBtn': false,
				helpers : { overlay : {
										locked : true ,
										closeClick: false
									} } /* this prevents screen from shifting */
			}); // fancybox
			$('#myPleaseWait').modal('hide');
		} // success
    }); // ajax
}


function doBuildingAddinPopup(row,bl_id,blitem) {

	blmodl = buildingData.getField((parseInt(row)-1), "modelNumber");
	blitem = buildingData.getField((parseInt(row)-1), "itemNumber");
	buildingid	= buildingData.getField((parseInt(row)-1), "buildingid");

	if(isNaN(blmodl) || blmodl == null) {
		alert("Please select a Building Type before trying to apply Add-Ins");
	} else {

		$.ajax({
			type: "get",
			cache: false,
			url: "/index.cfm/building/showBuildingAddinPopup/",
			data: {
				taxYear: taxYear,
				buildingid: buildingid,
				row: row,
				blmodl: blmodl,
				blitem: blitem
			}, // all form fields
			success: function (data) {
				// on success, post (preview) returned data in fancybox
				$.fancybox(data, {
					// fancybox API options
					'padding' : 0,
					'closeBtn': false,
					helpers : { overlay : {
											locked : true ,
											closeClick: false
										} } /* this prevents screen from shifting */
				}); // fancybox
				$('#myPleaseWait').modal('hide');
			} // success
	    }); // ajax
	}
}


function doBuildingTotalsPopup(row,bl_id,blitem) {

	blmodl = buildingData.getField((parseInt(row)-1), "modelNumber");
	blitem = buildingData.getField((parseInt(row)-1), "itemNumber");
	buildingid	= buildingData.getField((parseInt(row)-1), "buildingid");

	if(isNaN(blmodl) || blmodl == null) {
		alert("Please select a Building Line Item");
	} else {

		$.ajax({
			type: "get",
			cache: false,
			url: "/index.cfm/building/showBuildingTotalsPopup/",
			data: {
				taxYear: taxYear,
				buildingid: buildingid,
				row: row,
				blmodl: blmodl,
				blitem: blitem
			}, // all form fields
			success: function (data) {
				// on success, post (preview) returned data in fancybox
				$.fancybox(data, {
					// fancybox API options
					'padding' : 0,
					'closeBtn': false,
					helpers : { overlay : {
											locked : true ,
											closeClick: false
										} } /* this prevents screen from shifting */
				}); // fancybox
				$('#myPleaseWait').modal('hide');
			} // success
	    }); // ajax
	}
}

function doBuildingElementsTotals(row,bl_id,blitem) {

	blmodl = buildingData.getField((parseInt(row)-1), "modelNumber");
	blitem = buildingData.getField((parseInt(row)-1), "itemNumber");
	buildingid	= buildingData.getField((parseInt(row)-1), "buildingid");
	valueMethod = buildingData.getField((parseInt(row)-1), "valuationmethod");

	if(isNaN(blmodl) || blmodl == null) {
		alert("Please select a Building Line Item");
	} else {

		$.ajax({
			type: "get",
			cache: false,
			url: "/index.cfm/building/showBuildingElementsTotals/",
			data: {
				taxYear: taxYear,
				buildingid: buildingid,
				valuationMethod: valueMethod,
				row: row,
				blmodl: blmodl,
				blitem: blitem
			}, // all form fields
			success: function (data) {
				// on success, post (preview) returned data in fancybox
				$.fancybox(data, {
					// fancybox API options
					'padding' : 0,
					'closeBtn': false,
					helpers : { overlay : {
											locked : true ,
											closeClick: false
										} } /* this prevents screen from shifting */
				}); // fancybox
				$('#myPleaseWait').modal('hide');
			} // success
	    }); // ajax
	}
}


function changeVal(iRow,obj) {
	/* Updates the WDDX packet*/
	inVal = $(obj).val();
	inVal = inVal.replace('$','');
	inVal = inVal.replace(',','');


	if(inVal == "-junk-") {
		/*
			The "Select..." value for the dropdowns that are recreated when a building type is changed are set to "-junk-" to allow the select2 to fire off a change action when
			there is a NA option but no code value assigned to it and a "Select..." option available to the user (as both options are a null value, the onchange function never fires)
		 */
		inVal = null;
	}

	buildingData.setField(iRow,obj.name,inVal);

	/* Remove any 'we changed your value to a "select..." option because what you have previously doesn't exist now'*/
	if($("#s2id_" + obj.name + "_dd_" + iRow).hasClass("fix-me-i-am-an-error")){
		$("#" + obj.name + "_dd_" + iRow).removeClass("fix-me-i-am-an-error");
		$("#s2id_" + obj.name + "_dd_" + iRow).removeClass("fix-me-i-am-an-error");
		$("#s2id_" + obj.name + "_dd_" + iRow).tooltip('destroy');
	}


}


function fullTrim(str) {
	str1 = str.replace(/[^\x21-\x7E]+/g, ' '); // change non-printing chars to spaces
	str2 = str1.replace(/^\s+|\s+$/g, '');      // remove leading/trailing spaces
    return str2;
}



$(function() {

	$(".numeric").numeric();
	$(".datefield").datepicker();
	changeSelectsToSelect2s(); // 2017.12.07 - TDD - Function replaced with formatSelect.js
});


function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format3PieceSmallLeft(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left-small formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle-small formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right-small formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";
}

function popAddBlankRow() {
	$("#s2id_pop-blmodl").select2("val",null);
	$('#popBuildingNewLine').modal('show');
}

function popAddBlankRowCheck(blmodl){
	if(blmodl != "") {
		 addBlankRow(blmodl);
	}
}


function addBlankRow(blmodl) {
	$('#popBuildingNewLine').modal('hide');
	buildingData.addRows(1);
	iNewRow = buildingData.getRowCount() -1;

	blitem = "X_" + buildingData.getRowCount();

	/* Set necessary data for this record */
	buildingData.setField(iNewRow, "mode", "insert");
	buildingData.setField(iNewRow, "blprop", ParcelID);
	buildingData.setField(iNewRow, "blpyr", taxYear);
	buildingData.setField(iNewRow, "bl_id", -iNewRow);
	buildingData.setField(iNewRow, "bltyp", "R");
	buildingData.setField(iNewRow, "blmodl", blmodl);
	buildingData.setField(iNewRow, "blitem", blitem);


	/* Create a new line and tie the WDDX record to that row */
	$.ajax({
		url: "/cfc/content.cfc?method=addNewBuildingLine",
		type: "post",
		async: false, //This variable makes the function wait until a response has been returned from the page
		data: {
			itemNumber: buildingData.getRowCount(),
			rowNumber: iNewRow,
			taxYear: taxYear,
			blmodl: blmodl,
			bl_id: -iNewRow,
			blitem: blitem
		},
	  	success: function(data){
			$("#expander-holders").append(data);
			location.href = "#item_" + buildingData.getRowCount();
			$(".numeric").numeric();
			formatSelectsToSelect2();
			$('[data-toggle="tooltip"]').tooltip({html: true});
			$("#totalBuildingLines").html("(" + buildingData.getRowCount() + ")");
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){

		}
	});
}


function saveWDDX(blitem) {

	/* Pull data out of the WDDX packets that are not relevant to the row we want to submit*/
	buildingRow = createPacket(buildingData, "itemNumber", blitem);
	buildingFeaturesForRow = createPacket(buildingFeaturesData, "itemNumber", blitem);
	buildingAddInsRow = createPacket(buildingAddInsData,"itemNumber", blitem);

	/* Serialize the new WDDX packets */
	wddxSerializer = new WddxSerializer();
	wddxBuildingData = wddxSerializer.serialize(buildingRow);
	$("#buildingData").val(wddxBuildingData);
	wddxBuildingFeaturesData = wddxSerializer.serialize(buildingFeaturesForRow);
	$("#buildingFeaturesData").val(wddxBuildingFeaturesData);
	wddxAddInsData = wddxSerializer.serialize(buildingAddInsRow);
	$("#buildingAddInsData").val(wddxAddInsData);

	$("#frmSave").submit();
}
