$(function() {			
	$(".numeric").numeric();	
	$(".datefield").datepicker();
	formatSelectsToSelect2();
});


function format1Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left-building formatted-data-values'></span><span class='select2-2part-right-building formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";	   
}

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left-building formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right-building formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";	   
}

function format2PieceTppUse(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left-tpp-use formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right-tpp-use formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";	   
}


function format3Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";	   
}

function format3PieceSmallLeft(state) {
	var originalOption = state.element;
	return "<span class='select2-3part-building-left-small formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-3part-building-middle-small formatted-data-values'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-building-right-small formatted-data-values'>" + $(originalOption).data('adjval')+ "</span>";	   
}




function formatSelectsToSelect2(){
	$('.select2-1piece-new').select2({
		minimumResultsForSearch: -1,							
		formatResult: format1Piece,
		formatSelection: format1Piece,
		escapeMarkup: function(m) { return m; }
	});
	
	$('.select2-2piece-new').select2({
		minimumResultsForSearch: -1,							
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	
	$('.select2-2piece-tpp-use-new').select2({
		minimumResultsForSearch: -1,							
		formatResult: format2PieceTppUse,
		formatSelection: format2PieceTppUse,
		escapeMarkup: function(m) { return m; }
	});	
	
	
	
	
	$('.select2-3piece-new').select2({
		minimumResultsForSearch: -1,
		formatResult: format3Piece,
		formatSelection: format3Piece,
		escapeMarkup: function(m) { return m; }
	});
	
	$('.select2-3piece-smallleft-new').select2({
		minimumResultsForSearch: -1,
		formatResult: format3PieceSmallLeft,
		formatSelection: format3PieceSmallLeft,
		escapeMarkup: function(m) { return m; }
	});
							
	/*<!--- Remove the start up class, so if we have to create a new row on the fly, recalling the select2 init code won't cause problems in already built ones. --->*/
	$( ".select2-1piece-new" ).each(function( ) {			
		$(this).removeClass("select2-1piece-new").addClass("select2-1piece");
	});
	$( ".select2-2piece-new" ).each(function( ) {			
		$(this).removeClass("select2-2piece-new").addClass("select2-2piece");
	});
	$( ".select2-2piece-tpp-use-new" ).each(function( ) {			
		$(this).removeClass("select2-2piece-tpp-use-new").addClass("select2-2piece-tpp-use");
	});
	
	
	$( ".select2-3piece-new" ).each(function( ) {			
		$(this).removeClass("select2-3piece-new").addClass("select2-3piece");
	});
	$( ".select2-3piece-smallleft-new" ).each(function( ) {			
		$(this).removeClass("select2-3piece-smallleft-new").addClass("select2-3piece-smallleft");
	});
}



$('.popTrigger').click(function(){ closePopshell() })	
$('.popShell-close').click(function(){ closePopshell() })	

function closePopshell() {
	var shell = $('.popShell');
	shell.css("opacity",0)
	setTimeout(function(){
		shell.css("visibility","hidden")
	},200);
}

$('.btn-add-option').click(function(){
	$('.popEditContent .content').append($('.popEditContent').find('#blank-row').html());
	$('.popEditContent select').last().focus();
})



$(document).ready(function(){
	<!--- $('.typeahead').typeahead() --->
})

// activate all tool-tips
$(function(){
	$("[rel='tooltip']").tooltip({ delay: { show: 500, hide: 100 } });
})

// intercept click on filters button
$('#filters-box').find("span").click(function(){
	showFilters()
})

// show/hide the filters section
function showFilters() {
	var img = $('#filters-img');
	var box = $('#filters-box');
	if ( box.css("height") != '0px' ) {
		img.removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
		box.css("height","0px");
	} else {
		img.removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
		box.css("height","300px");
	}
}

// process the filters 
function runFilters() {
	showFilters();
	$('#search-results-none').css("display","none");
	$('#search-results').css("visibility","visible");
}

// checkboxes
$('.id-checkboxes').click(function(){
	var bttn = $('#btn-view-selected');
	if ( $(this).is(":checked") ) { $('#btn-view-selected').removeClass('disabled') }
	if ( $('.id-checkboxes:checked').length == 0 ) {
		$('#checkall').prop("checked",false);
		bttn.addClass('disabled');
	} else {
		if ( $('.id-checkboxes').length == $('.id-checkboxes:checked').length ) { $('#checkall').prop("checked",true) } else { $('#checkall').prop("checked",false) }
		bttn.removeClass('disabled');
	}
})

// check all option 
$('#checkall').click(function(){
	var todo = ($(this).is(":checked")) ? 'on' : 'off' ;
	var bttn = $('#btn-view-selected');
	$('.id-checkboxes').each(function(){
		$(this).prop("checked", (todo=='on')?true:false);
		if ( todo == 'off' ) { bttn.addClass('disabled'); } else { bttn.removeClass('disabled') }
	})
})

// temp show property card
function showCard() {
	$('#search-card').css("display","none");
	$('#property-card').css("display","block");
}
