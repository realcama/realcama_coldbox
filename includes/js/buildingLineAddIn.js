// Before putting in formatSelect.js, would need to verify that other instances are using the same data attributes
function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function buildModal() {
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingAddInsData.getRowCount(); iCurrentRow++ ) {
		iCurrentRowItem = parent.buildingAddInsData.getField(iCurrentRow,"itemNumber");

		if (iCurrentRowItem == iBLItem) {

			if ( parent.buildingAddInsData.getField(iCurrentRow,"mode") != 'Delete' && parent.buildingAddInsData.getField(iCurrentRow,"mode") != 'DeleteNew' ) {
				try {
					buildingAddInID = parent.buildingAddInsData.getField(iCurrentRow,"buildingCustomJoinID").toString();
				} catch(err) {
					buildingAddInID = "-1";
				}

				adjdesc = parent.buildingAddInsData.getField(iCurrentRow,"codeType");
				bladjvalue = parseFloat(parent.buildingAddInsData.getField(iCurrentRow,"percentageFactor"));
				bladjtyp = parent.buildingAddInsData.getField(iCurrentRow,"codeType");
				adj_id = parent.buildingAddInsData.getField(iCurrentRow,"buildingCustomCodeID");
				
				var strHTML = "";
				strHTML = createAdjEntryRow(iCurrentRow, adjdesc, 'Edit');

				if(iOptionsOnModal === 0) {
					$("#adjContent").html(strHTML);
				} else {
					$("#adjContent").append(strHTML);
				}

				$("#codeType_" + iCurrentRow).val(bladjtyp);

				// update adjustment code select boxes to default to selected option
				for ( var i = 0; i < arrAdjustments.length; i++ ) {
					if ( arrAdjustments[i] == parent.buildingAddInsData.getField(iCurrentRow, "codetype") ) {
						$("#" + arrAdjustments[i] + "_" + iCurrentRow).val(adj_id);
					}
				}

				//$("#Sprinkler_" + iCurrentRow).val(adj_id);
				$("#User-Defined2_" + iCurrentRow).val(adj_id);

				if(!isNaN(bladjvalue))
					$("#percentageFactor_" + iCurrentRow).val(bladjvalue.toFixed(0));
				else
					$("#percentageFactor_" + iCurrentRow).val(100);
				
				$("#bLPoints_" + iCurrentRow).val( 0 );
				$("#bLLumpSum_" + iCurrentRow).val( 0 );
				$("#bLFactor_" + iCurrentRow).val( 0 );
				$("#bLRateAddOn_" + iCurrentRow).val( 0 );
				
				iOptionsOnModal++;
			}
		}
	}

	changeSelectsToSelect2s();

	if(iOptionsOnModal == 0) {
		$("#adjContent").html("<div class='formatted-data-values'>No adjustments defined for this item.</div>");
	}

}
function changeUDFMethod(obj) {
	iRowToSetSelects = $(obj).data("rownumber");

	turnOffSelects(iRowToSetSelects);
	showOption(obj.value,iRowToSetSelects);
}

function removeAdjRow(iRow) {
	var choice = confirm("Are you sure you want to delete this row?");
	if ( choice == true ) {
		if ( parent.buildingAddInsData.getField(iRow, "mode") != 'Insert' ) {
			parent.buildingAddInsData.setField( iRow, "mode", "DeleteNew" );
		} else {
			parent.buildingAddInsData.setField( iRow, "mode", "Delete" );	
		}
		
		$("#adjRow_" + iRow).remove();
		createLabel();
		$('[data-toggle="tooltip"]').tooltip();
	}
}

function changeAdjAdjustment(iRow,obj) {
	if ( parent.buildingAddInsData.getField(iRow, "mode") != 'Insert' && parent.buildingAddInsData.getField(iRow, "mode") != 'InsertTemp' ) {
		parent.buildingAddInsData.setField( iRow, "mode", "Edit" );
	}
}

function addAdjRow() {
	parent.buildingAddInsData.addRows(1);
	iNewRow = parent.buildingAddInsData.getRowCount() -1;

	parent.buildingAddInsData.setField( iNewRow, "buildingID", bl_id );
	parent.buildingAddInsData.setField( iNewRow, "itemNumber", parent.buildingData.getField(jsRowNumber, "itemNumber") );
	//parent.buildingAddInsData.setField( iNewRow, "codeType", "" );
	//parent.buildingAddInsData.setField( iNewRow, "buildingCustomCode", "" );
	//parent.buildingAddInsData.setField( iNewRow, "buildingCustomCodeDescription", "" );
	parent.buildingAddInsData.setField( iNewRow, "percentageFactor", "100" );
	// Row is flipped to Insert when Finish button clicked, else get flagged as Delete
	parent.buildingAddInsData.setField( iNewRow, "mode", "InsertTemp" );

	var strHTML = "";
	strHTML = createAdjEntryRow(iNewRow, 'Sprinkler', 'Insert');

	if(iOptionsOnModal <= 0) {
		$("#adjContent").html(strHTML);
	} else {
		$("#adjContent").append(strHTML);
	}

	$(".numeric").numeric();

	iOptionsOnModal++;

	changeSelectsToSelect2s();
}

function createLabel(){
	var strLabel = "";
	var iPercentage = 0;

	/* find out if we have to use depth or not in the calculation of the net adjustment row */
	for ( var iCurrentRow = 0; iCurrentRow < parent.buildingAddInsData.getRowCount(); iCurrentRow++ ) {
		iCurrentRowItem = parent.buildingAddInsData.getField(iCurrentRow,"itemNumber");
		
		if ( iCurrentRowItem == iBLItem ) {
			if ( parent.buildingAddInsData.getField(iCurrentRow, "mode") != 'Delete' && parent.buildingAddInsData.getField(iCurrentRow,"mode") != 'DeleteNew' ) {
				adjcd = parent.buildingAddInsData.getField( iCurrentRow, "buildingCustomCode");
				adjds = parent.buildingAddInsData.getField( iCurrentRow, "buildingCustomCodeDescription");
				
				if ( (adjcd == null || adjcd == "N/A" || adjcd === "") && (adjds == null ||  adjds == "N/A") ) {
					//this is a junk entry, not all required pieces are selected
				} else {
					if ( parent.buildingAddInsData.getField( iCurrentRow, "codeType") != "0" ) {
						strLabel = strLabel + "<tr>";
						strLabel = strLabel + "<td class='adjustmentCell formatted-data-values'>" + parent.buildingAddInsData.getField( iCurrentRow, "codeType") + "</td>";
						strLabel = strLabel + "<td class='codeCell formatted-data-values'>" + parent.buildingAddInsData.getField( iCurrentRow, "buildingCustomCode") + "</td>";
						strLabel = strLabel + "<td class='codeDescCell formatted-data-values'>" + parent.buildingAddInsData.getField( iCurrentRow, "buildingCustomCodeDescription") + "</td>";
						strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>" + parseFloat(parent.buildingAddInsData.getField( iCurrentRow, "percentageFactor")) + "%</td>";
						strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "<td class='text-right smallCell formatted-data-values'>&nbsp;</td>";
						strLabel = strLabel + "</tr>";

						//Take the percentage and multiply that against the sum of the percentage adjustments (since we don't add percentage adjustments, we multiply)
						if ( iPercentage == 0 ) {
							iPercentage = parseFloat( parent.buildingAddInsData.getField( iCurrentRow, "percentageFactor" ) )/100;
						} else {
							iPercentage = iPercentage * ( parseFloat( parent.buildingAddInsData.getField( iCurrentRow, "percentagefactor") )/100 );
						}
					}
				}
			}
		}
	}
	
	if ( strLabel === "" ) {
		strLabel = "<tr><td colpan='8' class='formatted-data-values'>None</td></tr>";
	}
	
	parent.$("tbody#addIn_" + iUrlRow).html(strLabel);
	parent.$("td#addIn_net_percentage_" + iUrlRow).html( (iPercentage*100).toFixed(0) + "%" );
}

function turnOffSelects(iRowToSetSelects){
	for ( var i = 0; i < arrAdjustments.length; i++ ) {
		$("#" + arrAdjustments[i] + "_" + iRowToSetSelects).hide();
		$("#s2id_" + arrAdjustments[i] + "_" + iRowToSetSelects).hide();
		$("#" + arrAdjustments[i] + "_" + iRowToSetSelects).val(-1);
	}
}

function showOption(method,iRowToSetSelects) {
	for ( var i = 0; i < arrAdjustments.length; i++ ) {
		if ( arrAdjustments[i] == method ) {
			$("#s2id_" + arrAdjustments[i] + "_" + iRowToSetSelects).show();
			$("#s2id_" + arrAdjustments[i] + "_" + iRowToSetSelects).css("display","inline-block");
			break;
		}
	}
}

function savePopUpChanges() {
	var iErrors = 0;
	var strAttCodeErr = "";
	var strError = "<div class='col-xs-12 formatted-data-values padding10'>Correction required.</div>";

	for ( var i = 0; i < parent.buildingAddInsData.getRowCount(); i++ ) {
		if ( parent.buildingAddInsData.getField( i, "mode" ) != 'Delete' && parent.buildingAddInsData.getField(i,"mode") != 'DeleteNew' ) {
			if ( $("select#codeType_" + i + " option:selected").val() == '' ) {
				iErrors++;
				strError = strError + "<div class='col-xs-12 col-sm-4 formatted-data-values' data-sort='" + i + "'>Please select an adjustment.</div>";
			} else {
				if 	(	$("select#" + $("select#codeType_"+i+ " option:selected").val() + "_" + i + " option:selected").val() == ''
					|| 	$("select#" + $("select#codeType_" + i + " option:selected").val() + "_" + i + " option:selected").length == '0' ) {
					iErrors++;
					strError = strError + "<div class='col-xs-12 col-sm-4 formatted-data-values' data-sort='" + i + "'>Please select an code for " + $("select#codeType_"+i+ " option:selected").val() + ".</div>";
				}
			}
		}
	}

	if ( iErrors > 0 ) {
		$("#errorContainer").html(strError);
		$("#errorContainer").addClass("modal-error");
		$("#errorContainer").addClass("bg-danger");
		$("#building-modal-adjustments").scrollTop( 0 );
	} else {		
		for ( var i = 0; i < parent.buildingAddInsData.getRowCount(); i++ ) {
			// DeleteNew are new adjustments that were added, but user decided to delete new item
			if ( parent.buildingAddInsData.getField(i,"mode") == 'DeleteNew' ) {
				parent.buildingAddInsData.setField( i, "mode", 'Delete' );
			} else if ( parent.buildingAddInsData.getField( i, "mode" ) == 'InsertTemp' ) {
				parent.buildingAddInsData.setField( i, "mode", 'Insert' );
			}
			if ( parent.buildingAddInsData.getField( i, "mode" ) != 'Delete' ) {
				parent.buildingAddInsData.setField( i, "codeType", $("select#codeType_"+i+ " option:selected").val() );
				parent.buildingAddInsData.setField( i, "buildingCustomCode", $("#s2id_" + $("select#codeType_"+i+ " option:selected").val() + "_" + i + " span.select2-2part-left").text() );
				parent.buildingAddInsData.setField( i, "buildingCustomCodeDescription", $("#s2id_" + $("select#codeType_"+i+ " option:selected").val() + "_" + i + " span.select2-2part-right").text()  );
				parent.buildingAddInsData.setField( i, "buildingCustomCodeID", $("#s2id_" + $("select#codeType_"+i+ " option:selected").val() + "_" + i + " span.select2-2part-hidden-1").text()  );
				parent.buildingAddInsData.setField( i, "percentageFactor", $("#percentageFactor_"+i).val() );
			}
		}
		createLabel();
		parent.$.fancybox.close();
	}
}

function cancelChanges() {
	for ( var i = 0; i < parent.buildingAddInsData.getRowCount(); i++ ) {
		if ( parent.buildingAddInsData.getField( i, "mode" ) == 'InsertTemp' || parent.buildingAddInsData.getField( i, "mode" ) == 'Delete' ) {
			parent.buildingAddInsData.setField( i, "mode", "Delete" );
		} else {
			parent.buildingAddInsData.setField( i, "mode", "none" );
		}
	}
	createLabel();
	parent.$.fancybox.close();
}