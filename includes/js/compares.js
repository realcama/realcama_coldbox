function copyParcelOneToTwo() {
	$("#rightParcel").val($("#leftParcel").val());
	$("#rightParcelID").val($("#leftParcelID").val());
}

function checkVals() {
	strLeft = $("#leftParcel").val();
	strRight = $("#rightParcel").val();

	if($.trim(strLeft) === $.trim(strRight)) {
		$("#rightYear").removeAttr("disabled");
	} else {
		$("#rightYear").attr("disabled", "disabled");
		$("#rightYear").val($("##leftYear").val());
	}
}

function isEmpty(td) {
	if (td.html() == '' || $.trim(td.html()) == '' || td.html() == '&nbsp;' || td.html() == ': ') {
		return true;
	}
	return false;
}

$("#pop-left").on("click", function() {
   $('#imagepreview').attr('src', $('#imageresource-left').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});
$("#pop-right").on("click", function() {
   $('#imagepreview').attr('src', $('#imageresource-right').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});

$(document).ready(function() {
	changeSelectsToSelect2s();

	$("#frmCompare").validate({
		rules: {
			rightParcel: {
				required: true
			}
		},
		messages: {
			rightParcel: "Missing or invalid parcel number. Please enter a valid parcel ID for Parcel ##2"
		},
		tooltip_options: {
			rightParcel: {
				placement: 'bottom',
				html: true
			}
		}
	});
});
