function saveSearchCriteriaProp() {
	var saveSearchName = $('#saveSearchName').val();
	if ( saveSearchName == null || saveSearchName == "" ) {
		alert("Please supply a search name.");
		return false;
	}

	var data = $('#frmSearchProp').serializeArray();
	$.post("/index.cfm/search/saveCriteria", data, function(data, status){
        $( "#save-search-results" ).html( data );
    });
}

function saveSearchCriteriaTpp() {
	var saveSearchName = $('#saveSearchName2').val();
	if ( saveSearchName == null || saveSearchName == "" ) {
		alert("Please supply a search name.");
		return false;
	}

	var data = $('#frmSearchTpp').serializeArray();
	$.post("searchSave.cfm", data, function(data, status){
        $( "#save-search-results-tpp" ).html( data );
    });
}

function loadSearch(saveSearchID,userID) {
	$.get(
		"/index.cfm/search/loadCriteria/searchID/" + saveSearchID + "/userID/" + userID,
		{},
		function(data, status){
       		eval( data );
    	});
}

function deleteConfirm(searchID) {
	$('#searchToNuke').val(searchID);
	$('#myModal').modal('show');
}

function deleteFavConfirm(favoriteID) {
	$('#favoriteToNuke').val(favoriteID);
	$('#nukeFavorite').modal('show');
}

function saveCheckMark(a) {
	var tempParcelList = $('#pgParcelList').val();

	if ( $("#parcelNum"+a).is(":checked") == true ) {
    	if ( tempParcelList == "" ) {
    		tempParcelList = $("#parcelNum"+a).val();
    	} else {
    		tempParcelList = tempParcelList + "," + $("#parcelNum"+a).val();
    	}
	} else {
		var tmp = tempParcelList.split(',');
		var index = tmp.indexOf( $("#parcelNum"+a).val() );
		if (index !== -1) {
		    tmp.splice(index, 1);
		    tempParcelList = tmp.join(',');
		}
	}
	$('#pgParcelList').val(tempParcelList);
	$('#parcelList').val($('#pgParcelList').val());
}

function searchPage(startRow) {
	$("#page").val(startRow);
	$("#frmSearchPagination").submit();
}

function enableMultiSelect(a) {
	$("#bMultiSelect").val(a);
	$("input:checkbox").removeAttr("checked");
	$("#pgParcelList").val("");
	$("#parcelList").val("");
	$("#frmSearchPagination").submit();
}

$( document ).ready(function() {
	formatSelectsToSelect2();

	$('#searchTabs a').click(function (e) {
		e.preventDefault();
		$("#whatTabIsDisplayed").val( $('#searchTabs a').index(this) );
		$(this).tab('show')
	})

    // remember the hash in the URL without jumping
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
       if(history.pushState) {
            history.pushState(null, null, '#'+$(e.target).attr('href').substr(1));
       } else {
            location.hash = '#'+$(e.target).attr('href').substr(1);
       }
    });

	$("#pagination-input").keydown( function(e) {
	    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	    if(key == 13) {
	        e.preventDefault();
	        iPageTo = $("#pagination-input").val();
	        searchPage(iPageTo)
	    };
	});

	$("#selPageSize").change(function() {
		$("#page_size").val($("#selPageSize").val());
		iPageTo = $("#pagination-input").val();
		searchPage(iPageTo)
	});

	// 
	$("#btnMultiSelect").click( function(a) {
		enableMultiSelect( $("#btnMultiSelect").val() );
	});

	/*
    $("#city").typeahead({
        name : 'city',
        remote: {
            url : '/cfc/content.cfc?method=getCitySuggestions&query=%QUERY'
        },
        limit: 10,
		minLength: 2
    });
	$("#street").typeahead({
        name : 'street',
        remote: {
            url : '/cfc/content.cfc?method=getStreetSuggestions&query=%QUERY'
        },
        limit: 10,
		minLength: 1,
		hint: false
    });

	$("#type").typeahead({
        name : 'streettype',
        remote: {
            url : '/cfc/content.cfc',
			cache: false,
			replace: function(url, uriEncodedQuery) {
		      var newUrl = url + '?method=getStreetTypeSuggestions&query=' + uriEncodedQuery;
		      newUrl += '&street=' + $('#street').val();
		      return newUrl;
		    },
			hint: false
        },
        limit: 10
    });
	*/
});

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}
function format2PieceLarger(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left-longerCode formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right-longerCode formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}
function frmRefresh() {
	$(':input','#frmSearchProp')
		.not(':button, :submit, :reset')
		.val('')
		.removeAttr('checked')
		.removeAttr('selected');

	//$("#exemption").deselectAll();
	$('.selectpicker').selectpicker('deselectAll');
	$("#exemption2").select2("val","");
	$("#neighborhoodCode").select2("val","");
	$("#year").select2("val",currentTaxYear);
	$("#taxDistrict").select2("val","");
	$("#subdivisionCode").select2("val","");
	$("#usecode").select2("val","");
	$("#sort_order").select2("val","");

	$(':input','#frmSearchTpp')
		.not(':button, :submit, :reset')
		.val('')
		.removeAttr('checked')
		.removeAttr('selected');

	$("#taxDistrict2").select2("val","");
	$("#year2").select2("val",currentTaxYear);

/*
$('#exemption')
    .find('option')
    .remove()
    .end()
    .val('')
;
*/

}

function frmSubmit() {
	formToSub = $("#whatTabIsDisplayed").val();
	if(formToSub == 0) {
		$("#frmSearchProp").submit();
	} else {
		$("#frmSearchTpp").submit();
	}
}

function setFavSearchData(taxYear,ParcelID,tpp) {
	frmRefresh();
	if(ParcelID.length > 0) {
		$("#parcelNumber").val(ParcelID);
		$("#year").select2("val",taxYear);
		$("#currentTab").val(0);
		$('#searchTabs a[href="#property"]').tab('show');
	} else {
		$("#accountNumber").val(tpp);
		$("#year2").select2("val",taxYear);
		$("#currentTab").val(1);
		$('#searchTabs a[href="#tpp"]').tab('show');
	}
}

function formatSelectsToSelect2(){
	$('.select2-2piece-new').select2({
		formatResult: format2Piece,
		formatSelection: format2Piece,
		escapeMarkup: function(m) { return m; }
	});
	$('.select2-2piece-new-larger').select2({
		formatResult: format2PieceLarger,
		formatSelection: format2PieceLarger,
		escapeMarkup: function(m) { return m; }
	});


	$('.select2').select2({
		escapeMarkup: function(m) { return m; }
	});

	$( ".select2-2piece-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-new").addClass("select2-2piece");
	});
	$( ".select2-2piece-new-larger" ).each(function( ) {
		$(this).removeClass("select2-2piece-new-larger").addClass("select2-2piece");
	});
}

function filterYears(inVal) {

	$('#myPleaseWait').modal('show');
	$.ajax({
		url: "index.cfm/search/getUpdatedSearchDropdownSelections",
		type: "post",
		async: false, //This variable makes the function wait until a response has been returned from the page
		data: {
			taxYear: inVal,
		},
		success: function(data){
			eval(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){

		}
	});
	formatSelectsToSelect2();
	$('#myPleaseWait').modal('hide');
}

function favorite() {
	/* This bEnabledYr variable is used when in multiselect results to 
	enable the year input in order for it to be sent in the rc.
	*/
	var bEnableYr = 0;
	if ( $("#search-header-y").is(":disabled") == true ) {
		bEnableYr = 1;
	}
	if ( bEnableYr == 1 ) {
		$("#search-header-y").removeAttr("disabled");	
	}
	
	var hdrFormData = $('#frmHeader').serializeArray();

	if ( bEnableYr == 1 ) {
		$("#search-header-y").attr('disabled', 'disabled');
	}

	$.post('index.cfm/search/setFavorite', hdrFormData, function(data, status){
		$('#favoriteMarker').val(data);
		if ( data == 0 ) {
	       	$('#favorites').removeClass('favoriteOn');
	       	$('#favorites').addClass('favoriteOff');
		} else {
	       	$('#favorites').addClass('favoriteOn');
	       	$('#favorites').removeClass('favoriteOff');
		}
    });

}
