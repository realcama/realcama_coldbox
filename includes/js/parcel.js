$( document ).ready(function() {
	changeSelectsToSelect2s();
	formatSelectsToSelect2();
	$(".numeric").numeric();
	$(".datefield").datepicker();
});


function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format2PieceUseCode(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left-parcel-usecode'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right-parcel-usecode'>" + $(originalOption).data('desc')+ "</span>";
}

function format2PieceLarger(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left-longerCode formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right-longerCode formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

function format3PieceExemption(state) {
	var originalOption = state.element;
	switch($(originalOption).data('type')) {
		case "P":
			return "<span class='select2-3part-parcel-left'>" + $(originalOption).data('val') + "</span><span class='select2-3part-parcel-middle'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-parcel-right'>" + $(originalOption).data('adjval')+ "</span>";
			break;
		case "A":
			return "<span class='select2-3part-parcel-left'>" + $(originalOption).data('val') + "</span><span class='select2-3part-parcel-middle'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-parcel-right'>" + $(originalOption).data('adjval')+ "</span>";
			break;
		default:
			return "<span class='select2-3part-parcel-left'>" + $(originalOption).data('val') + "</span><span class='select2-3part-parcel-middle'>" + $(originalOption).data('desc')+ "</span><span class='select2-3part-parcel-right'>" + $(originalOption).data('adjval')+ "</span>";
			break;
	}
}

function formatSelectsToSelect2(){
	$('.select2-2piece-usecode-new').select2({
		formatResult: format2PieceUseCode,
		formatSelection: format2PieceUseCode,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-2piece-usecode-new" ).each(function( ) {
		$(this).removeClass("select2-2piece-usecode-new").addClass("select2-2piece-usecode");
	});

	$('.select2-2piece-new-larger').select2({
		formatResult: format2PieceLarger,
		formatSelection: format2PieceLarger,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-2piece-new-larger" ).each(function( ) {
		$(this).removeClass("select2-2piece-new-larger").addClass("select2-2piece");
	});

	$('.select2-3piece-parcel-new').select2({
		formatResult: format3PieceExemption,
		formatSelection: format3PieceExemption,
		escapeMarkup: function(m) { return m; }
	});
	$( ".select2-3piece-parcel-new" ).each(function( ) {
		$(this).removeClass("select2-3piece-parcel-new").addClass("select2-3piece-parcel");
	});
}


function countLength(obj,num) {
	var currentlen = $(obj).val().length;
	$("#legal-line-" + num).html(currentlen);
}

function changeExemptions(packetName,iRow,obj){

	var selectedOption = $(obj).find('option:selected');
	var adjustments = parseFloat(selectedOption.data('adjval-amt'));
	var type = selectedOption.data('type');

	var strCode = selectedOption.val();
	var strDecode = selectedOption.data("desc");
	$("#exemptionamount_" + iRow).attr('readonly',false);
	$("#exemptionpct_" + iRow).attr('readonly',false);

	switch(type) {
		case "P":
			/* Percentage */
			$("#exemptionamount_" + iRow).val('0.000');
			$("#exemptionamount_" + iRow).attr('readonly',true);
			$("#exemptionpct_" + iRow).val(adjustments.toFixed(3));
			fAmount = "0.000";
			fPercent = adjustments.toFixed(3);
			break;
		case "A":
			/* Automatic Amount */
			$("#exemptionpct_" + iRow).val('0.000');
			$("#exemptionpct_" + iRow).attr('readonly',true);

			$("#exemptionamount_" + iRow).val(adjustments.toFixed(3));
			//$("#amount_" + iRow).attr('readonly',true);

			fAmount = adjustments.toFixed(3);
			fPercent = "0.000";
			break;
		default:
			/* User Input (Or the "select" option) */
			$("#exemptionamount_" + iRow).val('0.000');
			$("#exemptionpct_" + iRow).val('0.000');
			fAmount = "0";
			fPercent = "0";
	}

	/* Set the WDDX Packet data */
	eval(packetName).setField(iRow,"exemptioncode", strCode);
	eval(packetName).setField(iRow,"decode", strDecode);
	eval(packetName).setField(iRow,"exemptionamount", fAmount);
	eval(packetName).setField(iRow,"exemptionpct", fPercent);

}


function changeVal(packet,iRow,obj) {

	//alert("Begin ChangeVal packet: " + packet + " obj: " + obj.name);
	/* Updates the WDDX packet*/
	inVal = $(obj).val();


	inVal = inVal.replace('$','');
	inVal = inVal.replace(',','');

	if($(obj).hasClass("numeric") === true && !isNaN(inVal)) {
		inVal =  parseFloat(inVal);
		inVal != "NaN" ? $(obj).val(inVal) : inVal = null;
	}
	if($(obj).hasClass("radioButton") === true)
	{
		arName = (obj.name).split(".");
		eval(packet).setField(iRow,arName[0],inVal);
	} else {
		eval(packet).setField(iRow,obj.name,inVal);
	}
	//alert("End ChangeVal");

}


function doParcelPopup(sayear,row,itemNumber) {

	hrefToLoad = 'index.cfm/parcel/showParcelMultisalesPopup/taxYear/' + sayear  +'/row/' + row + '/itemNumber/' + itemNumber + "/parcelID/" + encodeURIComponent(ParcelID);
	$.fancybox ({
		'padding' : 0,
		'closeBtn': false,
		'type': 'ajax',
		'href': hrefToLoad,
		helpers : { overlay : { locked : false } } /* this prevents screen from shifting */
	});
}


function lockParcelEditButton(row,obj) {
	inVal = $(obj).val();
	if(inVal == "Yes") {
		$("#parcel-edit-button-" + row).attr("disabled",false);
	} else {
		$("#parcel-edit-button-" + row).attr("disabled",true);
	}

}

function updateRatio (obj,iRow) {

	salesPrice = $(obj).val();
	var iRatio = iTotalHeaderValue / salesPrice;
	$("#ratio_" + iRow).val(iRatio.formatMoney(3,',','.'));
}


function addExemption() {

	exemptionData.addRows(1);
	iNewRow = exemptionData.getRowCount() -1;
	cfRowNumber = exemptionData.getRowCount();


	exemptionData.setField(iNewRow, "mode", "insert");

	exemptionData.setField(iNewRow, "taxyear", taxYear);
	exemptionData.setField(iNewRow, "parcelid", ParcelID);
	exemptionData.setField(iNewRow, "propertytype", "R");

	exemptionData.setField(iNewRow, "exemptioncode", null);
	exemptionData.setField(iNewRow, "decode", null);
	exemptionData.setField(iNewRow, "exemptionamount", 0);
	exemptionData.setField(iNewRow, "exemptionpct", 0);


	$('#myPleaseWait').modal('show');
	$.ajax({
		type: "get",
		cache: false,
		async: false, //This variable makes the function wait until a response has been returned from the page
		url: "index.cfm/parcel/newParcelExemptionRow",
		data: {
			ParcelID: ParcelID,
			taxYear: taxYear,
			cfRowNumber: cfRowNumber,
		}, // all form fields
		success: function (data) {
			$("#parcel-ememptions").append(data);
		} // success
    }); // ajax

	// Build the new select2 boxes we just created
	formatSelectsToSelect2();
	$('#myPleaseWait').modal('hide');

}



function saveWDDX(tab,row) {

	wddxSerializer = new WddxSerializer();

	wddxData2 = null;

	switch(tab){
		case "info":
			wddxData1 = wddxSerializer.serialize(parcelData);
			break;
		case "situs":
			wddxData1 = wddxSerializer.serialize(situsData);
			break;
		case "sales":
			wddxData1 = wddxSerializer.serialize(salesData);
			wddxData2 = wddxSerializer.serialize(multiSalesData);
			break;
		case "legal":
			wddxData1 = wddxSerializer.serialize(legalDescriptionData);
			break;
		case "exemptions":
			wddxData1 = wddxSerializer.serialize(exemptionData);
			wddxData2 = wddxSerializer.serialize(exemptionApplicationData);
			break;
	}

	$("#packet1").val(wddxData1);
	$("#packet2").val(wddxData2);

	$("#tab").val(tab);
	$("#row").val(row);

	$("#frmSave").submit();
}

function changeRadioVal(packet,iRow,obj) {

	eval(packet).setField(iRow,obj.name,$(obj).val());
}

function calcRatio(row) {
	try {
		dRatio = iTotalHeaderValue / parseFloat(salesData.getField(row-1,"saleprice"));
		$("#ratio_" + row).val(dRatio.toFixed(3));
	} catch(err) {};
}


