
function hasOptions(obj) {
    if (obj != null && obj.options != null) {
        return true;
    }
    return false;
}

function swapOptions(obj, i, j) {
    var o = obj.options;
    var i_selected = o[i].selected;
    var j_selected = o[j].selected;
    var temp = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
    var temp2 = new Option(o[j].text, o[j].value, o[j].defaultSelected, o[j].selected);
    o[i] = temp2;
    o[j] = temp;
    o[i].selected = j_selected;
    o[j].selected = i_selected;
}

function moveOptionUp(obj) {
	//alert("Up 2");
	//alert(obj.options.length);

    if (!hasOptions(obj)) {
		return;
    }

    for (i = 0; i < obj.options.length; i++) {
    	//alert("inside for");
        if (obj.options[i].selected) {
        	//alert("inside if");
            if (i != 0 && !obj.options[i - 1].selected) {
            	//alert("inside for");
                swapOptions(obj, i, i - 1);
                obj.options[i - 1].selected = true;
            }
        }
    }
}

function moveOptionDown(obj) {

    if (!hasOptions(obj)) {
        return;
    }
    for (i = obj.options.length - 1; i >= 0; i--) {
        if (obj.options[i].selected) {
            if (i != (obj.options.length - 1) && !obj.options[i + 1].selected) {
                swapOptions(obj, i, i + 1);
                obj.options[i + 1].selected = true;
            }
        }
    }
}


function submitReseqForm()
{
    selectBox = document.getElementById("resequenceIDs");

    for (var i = 0; i < selectBox.options.length; i++)
    {
         selectBox.options[i].selected = true;
    }

    resequenceForm.submit();
}
