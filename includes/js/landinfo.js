
$(function() {
	landUseCalculations(false);			
	$(".numeric").numeric();	
	$(".datefield").datepicker();
	changeSelectsToSelect2s();
});	

function format2Piece(state) {
	var originalOption = state.element;
	return "<span class='select2-2part-left formatted-data-values'>" + $(originalOption).data('val') + "</span><span class='select2-2part-right formatted-data-values'>" + $(originalOption).data('desc')+ "</span>";
}

/* Using static values here now for the following 2 items, change it later! */
var LumpSumAddOns = new Number(0);				
var RateAdjustments = new Number(0);

var iMasterRow = 0;
var objMaterObj = "";
function resetMasters() {
	iMasterRow = 0;
	objMaterObj = "";
}

function killLautChange() {
	/* the user doesn't want to change the Unit Type, so undo the change they just did  (reset the Unit Type select box) */
	$(objMaterObj).select2("val",landData.getField(iMasterRow,objMaterObj.name));
	$('#modalLautChange').modal('hide');	
	resetMasters();
}
function updateLautValue() {	
	/* calculate the units and then update the WDDX so it does the line calculations */
	switch($(objMaterObj).val()) {
		case "AC":
			resetDepthAdjustment(iMasterRow);
			doPerAcre(iMasterRow);			
			break;
		case "EF":
			doEffectiveFrontFeet(iRow);
			break;
		case "FF":
			/* do nothing */
			break;		
		case "SF":
			resetDepthAdjustment(iMasterRow);
			doSquareFoot(iRow);
			break;
		case "LT":
			resetDepthAdjustment(iMasterRow);
			doLot(iRow);
			break;
	}	
	changeVal(iMasterRow,objMaterObj);				
	$('#modalLautChange').modal('hide');	
	resetMasters();
}			
function doPerAcre(iRow) {
	/* Fired off when the Unit Type is "AC" */
	iUnits = landData.getField(iRow,"numberunits");			
	if(iUnits == "0.000" || isNaN(iUnits)) {
		doPerAcreCalc(iRow);
	}
}
function doPerAcreCalc(iRow) {
	/* Calculates the PER ACRE Units based on Frontage and Depth */
	iFrontage =  parseFloat(landData.getField(iRow,"front"));			
	iDepth =  parseFloat(landData.getField(iRow,"depth"));
	iCalcUnits = (iFrontage * iDepth) / 43560;
	landData.setField(iRow,"numberunits",iCalcUnits.toFixed(3));
	
	$('#numberunits_' + iRow).val(iCalcUnits.toFixed(3));
}	
	
function doEffectiveFrontFeet(iRow) {
	/* Fired off when the Unit Type is "EF" */
	iUnits = landData.getField(iRow,"numberunits");			
	if(iUnits == "0.000" || isNaN(iUnits)) {
		doEffectiveFrontFeetCalc(iRow);				
	}
}
function doEffectiveFrontFeetCalc(iRow) {
	/* Calculates the Effective Front Feet (Units are the same thing) based on frontage and back */
	Frontage = parseFloat(landData.getField(iRow,"front"));
	Backage = parseFloat(landData.getField(iRow,"back"));

	if(Frontage > Backage) {
		effectiveFrontFeet = Frontage * 0.65 + Backage * 0.35;							
	} else {
		effectiveFrontFeet = Frontage * 0.35 + Backage * 0.65;	
	}	
	
	/* Set the units to the EFF value */
	landData.setField(iRow,"numberunits",effectiveFrontFeet.toFixed(3));				
	$('#numberunits_' + iRow).val(effectiveFrontFeet.toFixed(3));
	
}

function doSquareFoot(iRow) {
	/* Fired off when the Unit Type is "SF" */
	iUnits = landData.getField(iRow,"numberunits");			
	if(iUnits == "0.000" || isNaN(iUnits)) {
		doSquareFootCalc(iRow);				
	}
}	
function doSquareFootCalc(iRow) {
	/* Calculates the Square Footage based  Frontage X Depth = number of Square Feet and units */
	iFrontage =  parseFloat(landData.getField(iRow,"front"));			
	iDepth =  parseFloat(landData.getField(iRow,"depth"));
	iCalcUnits = iFrontage * iDepth;
	
	landData.setField(iRow,"numberunits",iCalcUnits.toFixed(3));			
	$('#numberunits_' + iRow).val(iCalcUnits.toFixed(3));
}

function doLot(iRow) {
	/* Fired off when the Unit Type is "LT" */
	iUnits = landData.getField(iRow,"numberunits");			
	if(iUnits == "0.000" || isNaN(iUnits)) {
		doLotCalc(iRow);				
	}
}
function doLotCalc(iRow) {
	iFrontage =  parseFloat(landData.getField(iRow,"front"));	
	landData.setField(iRow,"numberunits",iFrontage.toFixed(3));			
	$('#numberunits_' + iRow).val(iFrontage.toFixed(3));	
}

function doFrontFootCalc(iRow) {
	iFrontage =  parseFloat(landData.getField(iRow,"front"));	
	landData.setField(iRow,"numberunits",iFrontage.toFixed(3));			
	$('#numberunits_' + iRow).val(iFrontage.toFixed(3));	
}

function changeCheckboxVal(iRow,obj) {
	if(obj.checked == true) {
		landData.setField(iRow,obj.name,"Y");
	} else {
		landData.setField(iRow,obj.name,"N");	
	}
}


function changeVal(iRow,obj) {
	/* Updates the WDDX packet*/
	inVal = $(obj).val();
	inVal = inVal.replace('$','');
	inVal = inVal.replace(',','');	
	 
	if($(obj).hasClass("numeric") === true && !isNaN(inVal)) {		
		inVal =  parseFloat(inVal);
		obj.name == "unitprice" || obj.name == "marketunitprice" ? inVal = inVal.toFixed(2) : inVal = inVal.toFixed(3);
		inVal != "NaN" ? $(obj).val(inVal) : inVal = null;
	}
	
	landData.setField(iRow,obj.name,inVal);			
	
	/* Do some calculations if the "units" field is blank based on the Unit Type */
	launit = landData.getField(iRow,"numberunits");			
	if(launit == "0.000" || isNaN(launit) || launit == null) {
		switch(landData.getField(iRow,"unittypecode")) {
			case "AC":
				doPerAcreCalc(iRow);
				break;
			case "EF":
				doEffectiveFrontFeetCalc(iRow);
				break;
			case "FF":
				/* Copy over the frontage to the units field */
				doFrontFootCalc(iRow);
				break;	
			case "SF":
				doSquareFootCalc(iRow);
				break;
			case "LT":
				doLotCalc(iRow);
				break;			
		}					
	}
	/* Fire off the process that calculates the visual aspects of the row */
	landUseCalculations(true);			
}		
function unitTypeChange(iRow,obj) {
	/* This process is called only when the Unit Type select box has been changed by the user */
	iMasterRow = iRow;
	objMaterObj = obj;
	$('#modalLautChange').modal('show');
}		
function changeDepthTable(iRow,obj) {
	changeVal(iRow,obj);
	/* Call the proc that pulls the depth table adjustment and set it in the WDDX so we don't need to call the proc everytime we do a row calc */
	getDepthTableAdjustments(iRow);	
	landUseCalculations(true);	
}
function changeLandType(iRow,obj) {
	changeVal(iRow,obj);
	
	var selectedOption = $(obj).find('option:selected');
	var lvalds = selectedOption.data('desc');
	
	landData.setField(iRow,"landusecode",lvalds);
	landUseCalculations(true);
}

function changeNeighborhood(iRow,obj) {
	changeVal(iRow,obj);
	
	//getNewNeighborhoodAdjustment(iRow);	/* old Method */
	
	/* Call the proc that pulls the neighborhood adjustment and set it in the WDDX so we don't need to call the proc everytime we do a row calc */
	var selectedOption = $(obj).find('option:selected');
	var neighborhoodAdjustments = parseFloat(selectedOption.data('adjval'));
	var nbhdds = selectedOption.data('desc');
	
	/* update WDDX */
	landData.setField(iRow,"neighborhoodadjustment",neighborhoodAdjustments.toFixed(3));	
	landData.setField(iRow,"neighborhoodcodedescription",nbhdds);
	
	landUseCalculations(true);	
}

function updateDepthValue(iRow,obj) {

	inVal =  parseFloat($(obj).val());
	inVal = inVal.toFixed(3);
	$(obj).val(inVal);

	/* Set the WDDX manually, so we don't fire off the landUseCalculations function twice */	
	landData.setField(iRow,"depth",inVal);
	
	/* Get Depth Table Adjustments based on the Unit Type */
	switch(landData.getField(iRow,"unittypecode")) {
		case "AC":
			/* do nothing */
			break;
		case "EF":
			getDepthTableAdjustments(iRow);
			break;
		case "FF":
			getDepthTableAdjustments(iRow);	
			break;	
		case "SF":
			/* do nothing */
			break;	
		case "LT":
			/* do nothing */
			break;			
	}	
	landUseCalculations(true);	
}	
function fullTrim(str) {
	str1 = str.replace(/[^\x21-\x7E]+/g, ' '); // change non-printing chars to spaces
	str2 = str1.replace(/^\s+|\s+$/g, '');      // remove leading/trailing spaces			
    return str2;
}		
function getDepthTableAdjustments(iRow) {
	/* This process is called only when the Depth Table select box has been changed by the user */
	var iDepthTable = landData.getField(iRow,"depthcode");
	var iDepthValue = landData.getField(iRow,"depth");
	var landID = landData.getField(iRow,"landid");
	
	bGetDepthPercentage = true;
	
	if(iDepthValue === null || isNaN(iDepthValue)) {		
		/* Warn the user that no depth is specified so we need to default the adjustment to 1 (as it won't return results from the proc) */		
		$('#modalNoDepth').modal('show');
		iDepthValue = 0;
		bGetDepthPercentage = false;
	}			
	cfRowNumber = iRow + 1;
	
	console.log( landID + " | " + taxYear + " | " + cfRowNumber + " | " + iDepthTable + " | " + iDepthValue );
	
	if(isNaN(iDepthValue) || iDepthTable === null || bGetDepthPercentage == false) { 
		depthTableAdjustments = 1;
	} else {
		$.get(
				"/index.cfm/land/getNewDepthPercentage/landID/" + landID + "/taxYear/" + taxYear + "/rowNumber/" + cfRowNumber + "/depthTable/" + iDepthTable + "/depthValue/" + iDepthValue,
				{},
				function(data, status){
		       		// update WDDX and visual field
		       		landData.setField(iRow, "depthtableadjustments", data);
		       		$('#depthTableAdjustment_' + iRow).val(data);
		    	});
	}
	
}	

function landUseCalculations(bAlterLandValueLabel) {	
	/*
		__          __     _____  _   _ _____ _   _  _____ 
		\ \        / /\   |  __ \| \ | |_   _| \ | |/ ____|
		 \ \  /\  / /  \  | |__) |  \| | | | |  \| | |  __ 
		  \ \/  \/ / /\ \ |  _  /| . ` | | | | . ` | | |_ |
		   \  /\  / ____ \| | \ \| |\  |_| |_| |\  | |__| |
		    \/  \/_/    \_\_|  \_\_| \_|_____|_| \_|\_____|
		                                                   
		If you modify ***ANY*** of this funtion, you will need to alter the persistantData_setInitialLandCalculations.cfm,
		the cfc/content.cfc (getComparisonRates) file as well otherwise you'll cause all sorts of data errors!!
	*/		
	
	
	/* Calculates each row and creates a grand total for display */
	var fTotalLandPrice = 0;
	var fTotalAgriculturalPrice = 0;
	var fTotalAcres = 0;
	var fTotalSqFeet = 0;
	var iLandLines = 0;
	var iAgriculturalLines = 0;
	
	
	for(iRow=0; iRow < landData.length; iRow++) {
		cfRowNumber = iRow + 1; // CF is Row based, JS is 0 based
		units = parseFloat(landData.getField(iRow,"numberUnits"));	
		unitPrice = parseFloat(landData.getField(iRow,"unitprice"));
		depthTableAdjustments = parseFloat(landData.getField(iRow,"depthtableadjustments"));
		marketUnitPrice = parseFloat(landData.getField(iRow,"marketunitprice"));
		NeighborhoodFactor = parseFloat(landData.getField(iRow,"neighborhoodadjustment"));
		
		Frontage =  parseFloat(landData.getField(iRow,"front"));			
		Depth =  parseFloat(landData.getField(iRow,"depth"));
		
		Type = landData.getField(iRow,"landusecode");
		AssessmentFlag = landData.getField(iRow,"lvalag");
		
		if(AssessmentFlag != "M") {
			switch(landData.getField(iRow,"laut")) {
				case "AC":
					fTotalAcres = fTotalAcres + units;
					fTotalSqFeet = fTotalSqFeet + ( units * 43560);
					break;
					
				case "FF":
					fTotalAcres = fTotalAcres + ((Frontage * Depth) / 43560);
					console.log(fTotalAcres);
					fTotalSqFeet = fTotalSqFeet + (Frontage * Depth);		
					break;	
					
				case "EF":
					fTotalAcres = fTotalAcres + ((Frontage * Depth) / 43560);
					fTotalSqFeet = fTotalSqFeet + (Frontage * Depth);							
					break;
					
				case "SF":
					fTotalAcres = fTotalAcres + (units / 43560);	
					fTotalSqFeet = fTotalSqFeet + units;							
					break;
				
				case "LT":
					fTotalAcres = fTotalAcres + ((Frontage * Depth) / 43560);	
					fTotalSqFeet = fTotalSqFeet + (Frontage * Depth);				
					break;
			}
		}
		
		
			
		/* Pull the UDF values for this item */
		RateAddOns = calculateUDFValues(cfRowNumber,'R',false);
		LumpSumAddOns = calculateUDFValues(cfRowNumber,'L',false);	
		
		if(landData.getField(iRow,"laut") == "EF" || landData.getField(iRow,"laut") == "FF"){
			/* Include the Depth Table adjustment */			
			PercentageAddOns = calculateUDFValues(cfRowNumber,'F',true);
		} else {
			/* Exclude the Depth Table adjustment */			
			PercentageAddOns = calculateUDFValues(cfRowNumber,'F',false);
		}
		
		/*
		console.log("RateAdjustments for row " + cfRowNumber + " : " + RateAdjustments);
		console.log("LumpSumAddOns for row " + cfRowNumber + ": " + LumpSumAddOns);
		console.log("percentageUDF for row " + cfRowNumber + ":" + percentageUDF);
		*/
		
		/***************************/
		/* Unit Price Calculations */
		/***************************/
		
		/* If market value is greater then 0, ignore ALL adjustments for Unit price */		
		if(marketUnitPrice.formatMoney(3,',','.') == 0.000) {
		
			/* 1. The rate add on is added to the unit price */
			preliminaryUnitPrice = unitPrice + RateAddOns;			
			
			/* 2.  The percentages are multiplied against each other to get net adjustment.  */
			netAdjustment = PercentageAddOns;			
			
			/* 3.  The net adjustment is multiplied against the preliminary unit price  */
			if(netAdjustment == 0) {
				/* If there are no adjustments defined,  we want to default the value to 1 otherwise this land line is going to get multiplied by ZERO */
				netAdjustment = 1;				
			}			
			adjustedUnitPrice = preliminaryUnitPrice * netAdjustment;
			
			/* 4.  The adjusted unit price is multiplied against the number of units  */
			preliminaryMarketValue = adjustedUnitPrice * units;
			
			/* 5.  The Lump Sum is added to the calculated value from step 4.  */
			appraisedValue = preliminaryMarketValue + LumpSumAddOns;
			
		} else {
		
			/* 1. The rate add on is added to the unit price */
			preliminaryUnitPrice = unitPrice;
			
			/* 4.  The adjusted unit price is multiplied against the number of units  */
			preliminaryMarketValue = preliminaryUnitPrice * units;		
			
			/* 5.  The Lump Sum is added to the calculated value from step 4.  */
			appraisedValue = preliminaryMarketValue;
			
		}
		
		$("#appraisedValue_" + iRow).html(appraisedValue.formatMoney(2,',','.'));
		$("#finalLandViewValue_" + iRow).html(appraisedValue.formatMoney(2,',','.'));
		$("#finalLandViewValue2_" + iRow).html(appraisedValue.formatMoney(2,',','.'));
		
		/********************************/
		/* AG Market Price Calculations */
		/********************************/
		
		/*var locus = (Type >= 005100 && Type <= 006999) ? "agricultural" : "land";*/
		$.ajax({
			url: "/cfc/content.cfc?method=inAgRange",
			type: "post",
			async: false, //This variable makes the function wait until a response has been returned from the page
			data: {
				typeCode: Type,
				typeYear: taxYear,
				valType: "js"
			},
		  	success: function(data){					
				locus = $.trim(data);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){					
								
			}
		});	
		
		
		if(marketUnitPrice > 0 || locus == "agricultural") {
			
			/* 1. The rate add on is added to the market unit price */
			preliminaryMarketUnitPrice = marketUnitPrice + RateAddOns;
			
			/* 2.  The percentages are multiplied against each other to get net adjustment.  */
			netAdjustment = PercentageAddOns;
			
			/* 3.  The net adjustment is multiplied against the preliminary market unit price  */
			adjustedMarketUnitPrice = preliminaryMarketUnitPrice * netAdjustment;
			
			/* 4.  The adjusted unit price is multiplied against the number of units  */
			preliminaryAGMarketValue = adjustedMarketUnitPrice * units;		
			
			/* 5.  The Lump Sum is added to the calculated value from step 4.  */
			AGValue = preliminaryAGMarketValue + LumpSumAddOns;
			//alert("Item " + cfRowNumber + " AG Market Value:" + AGValue);
			$("#AGMarketValue_" + iRow).html(AGValue.formatMoney(2,',','.'));
			$("#AGMarketValueFinal_" + iRow).html(AGValue.formatMoney(2,',','.'));
			var bUseAG = true;
			
		} else {			
			$("#AGMarketValue_" + iRow).html("0.00");
			$("#AGMarketValueFinal_" + iRow).html("0.00");	
			var bUseAG = false;		
		}		
		
		
		if(locus == "land" && bUseAG) {		
			locus = "agricultural";
		}
		
		if(locus == "agricultural") {			
			fTotalAgriculturalPrice = fTotalAgriculturalPrice + appraisedValue;
			iAgriculturalLines++;
		} 		
		if(locus == "land") {
			fTotalLandPrice = fTotalLandPrice + appraisedValue;	
			iLandLines++;
		}	
		
	}	
	
	if(bAlterLandValueLabel) {
		fTotalAppraisedValue = fTotalLandPrice + fTotalAgriculturalPrice + fBuilding + fExtraFeatures;
		// Set master label values 		
		
		/* Do not try to update the master header values */
		$('#landValue').html(fTotalLandPrice.formatMoney(0,',','.'));		
		$('#landLineCount').html(iLandLines);		
		$('#agriculturalValue').html(fTotalAgriculturalPrice.formatMoney(0,',','.'));	
		$('#agriculturalLineCount').html(iAgriculturalLines);
					
		$('#tavValue').html(fTotalAppraisedValue.formatMoney(0,',','.'));			
		
		$('#land-total-sq-feet').val(fTotalSqFeet.formatMoney(2,',','.'));	
		$('#land-total-acres').val(fTotalAcres.formatMoney(2,',','.'));
		
		$('#plvalue').val(fTotalLandPrice);
		$('#totalLandLines').val(iLandLines);
		$('#agtotal').val(fTotalAgriculturalPrice);
		$('#totalAgLines').val(iAgriculturalLines);	
		$('#totalsqfeet').val(fTotalSqFeet);
		$('#totalacres').val(fTotalAcres);	
	}
	
}

function saveWDDX(laitem) {
	$('#myPleaseWait').modal('show');
	
	/* Pull data out of the WDDX packets that are not relevant to the row we want to submit*/
	
	landRow = createPacket(landData, "itemNumber", laitem);
	landAdjustmentsForRow = createPacket(udfData, "la_id", laitem);
	
	wddxSerializer = new WddxSerializer(); 
	wddxLandData = wddxSerializer.serialize(landRow);
	$("#landData").val(wddxLandData);
	
	wddxUdfData = wddxSerializer.serialize(landAdjustmentsForRow);
	$("#udfData").val(wddxUdfData);	
	
	$("#frmSave").submit();
}
		
function addBlankRow(taxYear,parcelID) {
	landData.addRows(1);
	iNewRow = landData.getRowCount() -1;
	la_id = "X_" + landData.getRowCount();
	
	// Set necessary data for this record
	landData.setField(iNewRow, "mode", "Insert");
	landData.setField(iNewRow, "parcelid", parcelID);
	landData.setField(iNewRow, "taxyear", taxYear);
	//landData.setField(iNewRow, "rownumber", landData.getRowCount() );
	landData.setField(iNewRow, "itemNumber", la_id);
	landData.setField(iNewRow, "propertytype", "R");		
	landData.setField(iNewRow, "neighborhoodadjustment", "1.000"); //nbhdfa
	landData.setField(iNewRow, "depthtableadjustments", "1.000");				
	landData.setField(iNewRow, "zoningadjustment", "1.000"); //lazoneadj
	landData.setField(iNewRow, "roadadjustment", "1.000"); //laroadadj
	landData.setField(iNewRow, "utilitiesadjustment", "1.000"); //lautiladj
	landData.setField(iNewRow, "topographyadjustment", "1.000"); //latopoadj
	
	landData.setField(iNewRow, "unitprice", "0.000"); //lautpr
	landData.setField(iNewRow, "marketunitprice", "0.000"); //lamktutpr
	landData.setField(iNewRow, "front", "0.000"); //lafront
	landData.setField(iNewRow, "back", "0.000"); //laback
	landData.setField(iNewRow, "depth", "0.000"); //ladepth
	//landData.setField(iNewRow, "lautpr", 0.000); //lautpr
	landData.setField(iNewRow, "numberunits", "0.000"); //launit
	
	landData.setField(iNewRow, "leflfmvq", "N"); //leflfmvq
	landData.setField(iNewRow, "leflae", "N"); //leflae
	landData.setField(iNewRow, "lefldemol", "N"); //lefldemol
	
	landData.setField(iNewRow, "effectivebaserate", "0.000");
	landData.setField(iNewRow, "extendedvalue", "0.000");
	landData.setField(iNewRow, "marketvalue", "0.000");
	landData.setField(iNewRow, "finallandvalue", "0.000");
	
	landData.setField(iNewRow, "netadjustments", "1.000");
	
		
	/* Create a new line and tie the WDDX record to that row
	$.ajax({
		url: addBlankLineURL, // "/land/index/addBlankLine" /cfc/content.cfc?method=addNewLandLine
		type: "post",
		async: false, //This variable makes the function wait until a response has been returned from the page
		data: {
			itemNumber: landData.getRowCount(), 
			rowNumber: iNewRow,
			taxYear: taxYear,
			parcelID: parcelID
		},
	  	success: function(data){					
			$("#expander-holders").append(data);				
			location.href = "#edit_mode_item_" + landData.getRowCount();
			$(".numeric").numeric();	
			$("[rel='tooltip']").tooltip({ delay: { show: 500, hide: 100 } });
			$("#totalLandLines").html("(" + landData.getRowCount() + ")");
			formatSelectsToSelect2();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){					
		}
	}); */
	
	$.post(
		addBlankLineURL, 
		{ itemNumber: landData.getRowCount(), rowNumber: iNewRow, taxYear: taxYear, parcelID: parcelID },
		function( data ) {
			$("#expander-holders").append(data);
			//location.href = landLineURL + "#edit_mode_item_" + landData.getRowCount(); 
			$(".numeric").numeric();
			$("[rel='tooltip']").tooltip({ delay: { show: 500, hide: 100 } });
			$("#totalLandLines").html("(" + landData.getRowCount() + ")");
			formatSelectsToSelect2();
		}, 
		"html"
	);	
}		


function doLandPopup(row,la_id) {
	$.fancybox ({
		'padding' : 0,
		'closeBtn': false,
		'type': 'ajax', 
		'href': 'showLandPopup.cfm?taxYear=' + taxYear  +'&row=' + row + '&la_id=' + la_id, 
		helpers : { overlay : { locked : false } } /* this prevents screen from shifting */
	});
}

function addUdfRow() {
				
	udfData.addRows(1);
	
	iNewRow = udfData.getRowCount() -1;
	udfData.setField( iNewRow, "item", $("#item").val() );
	udfData.setField( iNewRow, "laadjvalue", "0" );
	udfData.setField( iNewRow, "laadjtyp", "R" );
	udfData.setField( iNewRow, "la_id", $("#la_id").val() );
	udfData.setField( iNewRow, "mode", "Insert" );
	
	var strHTML = "";	
	strHTML = createUDFEntryRow(iNewRow, 'Insert');	
	$("#udfContainer").append(strHTML);
	$(".numeric").numeric();
}



function removeUdfRow(iRow) {
	var choice = confirm("Are you sure you want to delete this row?");
	if(choice == true) {
		udfData.setField( iRow, "mode", "Delete" );
		$("#udfRow_" + iRow).remove();
		createLabel();
	}
}


function changeUdfVal(iRow,obj) {
	udfData.setField( iRow, obj.name, $(obj).val() );
	createLabel();
}

		
function changeUDFDefaultAdjustment(iRow,obj) {	
	iAdjustment = $("#laadjvalue_" + iRow).val();
	var selectedOption = $(obj).find('option:selected');
	
	if(iAdjustment === "0" || isNaN(iAdjustment)) {		 
		var strDefault = selectedOption.data('default');
		udfData.setField( iRow, "laadjvalue", strDefault );
		$("#laadjvalue_" + iRow).val(strDefault);
	}	
	/* When the user changes the adjustment, change the adjustment type per Jan */
	var strAdjTyp = selectedOption.data('adjtyp');
	$("#laadjtyp_" + iRow).val(strAdjTyp);
	
}	

function calculateUDFValues(iIncomingItem,strIncomingType,useDepthTable) {
	
	/*
		__          __     _____  _   _ _____ _   _  _____ 
		\ \        / /\   |  __ \| \ | |_   _| \ | |/ ____|
		 \ \  /\  / /  \  | |__) |  \| | | | |  \| | |  __ 
		  \ \/  \/ / /\ \ |  _  /| . ` | | | | . ` | | |_ |
		   \  /\  / ____ \| | \ \| |\  |_| |_| |\  | |__| |
		    \/  \/_/    \_\_|  \_\_| \_|_____|_| \_|\_____|
		                                                   
		If you modify ***ANY*** of this funtion, you will need to alter the persistantData_setInitialLandCalculations.cfm
			file as well otherwise you'll cause all sorts of data errors!!
	*/
	
	fValues = new Number(0);
	iTotalItems = 0;
	// set a default value for fValues otherwise you'll get a NaN returned to the caller if you don't meet all the IF criteria
	fValues = 0;
	
	for ( var iCurrentRow = 0; iCurrentRow < udfData.getRowCount(); iCurrentRow++ ) {
		iCurrentRowItem = udfData.getField(iCurrentRow,"item");
		laadjtyp = udfData.getField(iCurrentRow,"laadjtyp");
		mode = udfData.getField(iCurrentRow,"mode");
		adjdesc = udfData.getField(iCurrentRow,"adjdesc");
		
		bUseValue = true;
		if(useDepthTable == false && adjdesc == "Depths"){
			bUseValue = false;
		} 		
		
		if(iIncomingItem == iCurrentRowItem && strIncomingType == laadjtyp && mode != "Delete" && bUseValue == true) {
			
			iTotalItems++;
			laadjvalue = parseFloat(udfData.getField(iCurrentRow,"laadjvalue"));			
			
			if(laadjtyp === "F") {
				if(iTotalItems == 1) {
					// Don't multiple the first entry times 0, because you'll junk the data
					fValues = laadjvalue;	
				} else {
					// All percentages are multipled against each other
					fValues = fValues * laadjvalue;	
				}		
			} else {
				fValues = fValues + laadjvalue;	
			}			
		}
	}
	
	return fValues;
}

function resetDepthAdjustment(jsRow) {
	/* the incoming value is a JS value, so we need to add 1 so it is CF number based */
	var cfRow = jsRow + 1;
	
	for ( var iCurrentRow = 1; iCurrentRow < udfData.getRowCount(); iCurrentRow++ ) {

		if ( udfData.getField(iCurrentRow,"codetype") == "Depths" ) {
			laadjtyp = udfData.setField(iCurrentRow,"landcodeid","N/A");
			laadjtyp = udfData.setField(iCurrentRow,"code","N/A");
			laadjtyp = udfData.setField(iCurrentRow,"codedescription","N/A");
			laadjtyp = udfData.setField(iCurrentRow,"adjustment","1");
			la_id = udfData.getField(iCurrentRow,"landid");
		}
		
		/*iCurrentRowItem = udfData.getField(iCurrentRow,"item");		
		adjdesc = udfData.getField(iCurrentRow,"adjdesc");
		
		if(iCurrentRowItem == cfRow && adjdesc == "Depths") {
			laadjtyp = udfData.setField(iCurrentRow,"adj_id","N/A");
			laadjtyp = udfData.setField(iCurrentRow,"adjcd","N/A");
			laadjtyp = udfData.setField(iCurrentRow,"adjds","N/A");
			laadjtyp = udfData.setField(iCurrentRow,"laadjvalue","1");	
			la_id = udfData.getField(iCurrentRow,"la_id");			
		}*/	
	}

	/* Call the modal window and then fire off the Create Label function */ 
	$.fancybox ({
		'padding' : 0,
		'closeBtn': false,		
		'type': 'ajax', 
		'href': 'showLandPopup.cfm?taxYear=' + taxYear  +'&row=' + cfRow + '&la_id=' + udfData.getField(iCurrentRow,"landid"), //la_id
		helpers : { overlay : { locked : false } }, /* this prevents screen from shifting */
		beforeShow: function() {			
			parent.$(".fancybox-overlay").hide();		
			parent.$(".fancybox-wrap").hide();			
		},
		afterShow : function() {			
			createLabel();	
			$.fancybox.close(true);		
		}
	});
	
}

function doLandPopup(row,itemNumber) {
	hrefToLoad = 'index.cfm/land/showLandPopup/taxYear/' + taxYear + "/parcelID/" + encodeURIComponent(ParcelID)  +'/row/' + row + '/itemNumber/' + itemNumber;
	$.fancybox ({
		'padding' : 0,
		'closeBtn': false,
		'type': 'ajax', 
		'href': hrefToLoad, 
		helpers : { overlay : { locked : false } } 
	});
}

