<!--- All methods in this helper will be available in all handlers,views & layouts --->
<cfscript>
	/*<!--- Special functions --->*/
	function formatTPPNumber( accountNumber ) {
		var outString = LEFT(arguments.accountNumber,5) & "-" & RIGHT(ARGUMENTS.accountNumber,3);
		return outString;
	}


	function formatTaxDistrictCode( taxDistrictCode ) {
		// Formats the value to 3 characters. (ex: 003 vs 3)

		if (LEN(TRIM(ARGUMENTS.taxDistrictCode)) LTE 3) {
			strOutString = NumberFormat(ARGUMENTS.taxDistrictCode, "000") ;
		}

		return strOutString;
	}


	function formatNeighborhoodCode( neighborhoodCode ) {
		// Formats the value to 3 characters. (ex: 0000003.00 vs 3)

		if (LEN(TRIM(ARGUMENTS.neighborhoodCode)) NEQ "") {
			strOutString = NumberFormat(ARGUMENTS.neighborhoodCode, "0000000.00") ;
		}

		return strOutString;
	}


	function formatMoneyValue( moneyValue ) {
		// Formats the value to standard Monetary display that is used through the site

		strOutString = "$ " & NumberFormat(moneyValue, ",");


		return strOutString;
	}


	function formatBuildingModelNumber( modelNumber ) {
		// Formats the value to 2 characters. (ex: 30 vs 3)

		if (LEN(TRIM(ARGUMENTS.modelNumber)) NEQ "") {
			strOutString = NumberFormat(ARGUMENTS.modelNumber, "00") ;
		}

		return strOutString;
	}


	function formatComplexID( complexID ) {

		var strComplexID = TRIM(ARGUMENTS.complexID);
		var structVendorInfo = Controller.getSetting( "vendorInfo");


		if (LEN(TRIM(strComplexID)) NEQ 0) {

			if (strComplexID DOES NOT CONTAIN ".") {

				strComplexID = Reverse(strComplexID);
				strComplexID = Insert(".",strComplexID,2);
				strComplexID = Reverse(strComplexID);
			}

			if (LEN(TRIM(strComplexID)) LT 10) {
				strComplexID = NumberFormat(strComplexID, structVendorInfo.complexIDFormat ) ;
			}

		};

		return TRIM( strComplexID );
	}


	function formatBuildingTotals( buildingTotal ) {
		// Formats the value to 3 characters. (ex: 003 vs 3)
		var strOutString = "";

		if (LEN(TRIM(ARGUMENTS.buildingTotal)) NEQ 0) {
			strOutString = NumberFormat(ARGUMENTS.buildingTotal, ".999") ;
		}

		return strOutString;
	}


	function formatBuildingTotals2Digits( buildingTotal ) {
		// Formats the value to 3 characters. (ex: 003 vs 3)
		var strOutString = "";

		if (LEN(TRIM(ARGUMENTS.buildingTotal)) NEQ 0) {
			strOutString = NumberFormat(ARGUMENTS.buildingTotal, ".99") ;
		}

		return strOutString;
	}


	function ConvertValuationMethod( valueMethod ) {
		// Converts the Valuation integer value to display String value (ex: 1 vs cost)
		var strOutString = "";

		switch( arguments.valueMethod ){
			// Cache Commands
			case "1"    : { strOutString = "COST"; break; }
			case "2"    : { strOutString = "MICA"; break; }
			case "3"  	: { strOutString = "INCOME"; break; }
			case "4"  	: { strOutString = "MARKET"; break; }
			case "5"  	: { strOutString = "OVERRIDE"; break; }

			default: return false;
		}


		return strOutString;
	}


	function makeDatePretty( inDate ) {
		if( IsDate(ARGUMENTS.inDate) ) {
			return DateFormat(ARGUMENTS.inDate,"mm/dd/yyyy");
		}
		if( ARGUMENTS.inDate EQ 0 ) {
			return;
		} else {
			try {
				var strMonth = MID(ARGUMENTS.inDate,5,2);
				var strDay = RIGHT(ARGUMENTS.inDate,2);
				var strYear = LEFT(ARGUMENTS.inDate,4);
				var outDate = CreateDate(strYear,  strMonth,  strDay);
				return DateFormat(outDate,"mm/dd/yyyy");
			} catch(err) {
				return;
			}
		}
	}


	function fixDbDate( inDate ) {
		if( IsDate(ARGUMENTS.inDate) ) {
			return ARGUMENTS.inDate;
		} else {
				try {

					var outDate = INSERT("-", ARGUMENTS.inDate, 6);
					outDate = INSERT("-", outDate, 4);

					return outDate;
				} catch(err) {
					return now();
				}
			}
	}


	function breakStringAtLength( sentence, partLength ) {
		words = ARGUMENTS.sentence.Split(" ");
		parts = arrayNew(1);
		partCounter = 1;
		parts[partCounter] = "";

		for (loop=1; loop LE ArrayLen(words); loop = loop+1) {
			word = words[loop];
			if(LEN(parts[partCounter]) + LEN(word) < ARGUMENTS.partLength) {
				if(LEN(TRIM(parts[partCounter])) EQ 0) {
					parts[partCounter] = word;
				} else {
					parts[partCounter] = parts[partCounter] & " " & word;
				}
			} else {
				partCounter = partCounter + 1;
				parts[partCounter] = word;
			}
		}

		formattedString = "";
		for (loop=1; loop LE ArrayLen(parts); loop = loop+1) {
			if(LEN(TRIM(formattedString)) EQ 0) {
				formattedString = formattedString & parts[loop];
			} else {
				formattedString = formattedString & "<br/>"  & parts[loop];
			}
		}

		return formattedString;
	}

	function columnTotal( qryColumn ){
		return arraySum(listToArray(evaluate("valueList(" & qryColumn & ")")));
	}

</cfscript>


<cffunction name="createTaxesRollover">
	<cfargument name="qryRecords" type="query">

	<cfset strTooltip = "">

	<cfoutput>
	<cfloop query="qryRecords">
		<cfset strLabel = jurisdictionCodeDescription>
		<cfset strValue = TaxAmount>

		<cfset strTooltip = strTooltip & "<tr><td align='left' width='300'>#strLabel#</td><td align='right'>$ #NumberFormat(strValue,',.00')#</td></tr>">

	</cfloop>

	<cfif Len(Trim(strTooltip)) NEQ 0>
		<cfset strTooltip = "<table>" & strTooltip & "</table>">
	</cfif>
	</cfoutput>

	<cfreturn strTooltip>
</cffunction>


<cffunction name="createMillageRollover">
	<cfargument name="qryRecords" type="query">

	<cfset strTooltip = "">

	<cfoutput>
	<cfloop query="qryRecords">
		<cfset strLabel = jurisdictionCodeDescription>
		<cfset strValue = millageFinal>

		<cfset strTooltip = strTooltip & "<tr><td align='left' width='300'>#strLabel#</td><td align='right'>#NumberFormat(strValue,',.00000000')#</td></tr>">

	</cfloop>

	<cfif Len(Trim(strTooltip)) NEQ 0>
		<cfset strTooltip = "<table>" & strTooltip & "</table>">
	</cfif>
	</cfoutput>

	<cfreturn strTooltip>
</cffunction>


<cffunction name="createAdditionalDepreciationRollover">
	<cfargument name="qryRecords" type="query">

	<cfset strTooltip = "">

	<cfoutput>

	<cfif qryRecords.RecordCount()>

		<cfset strTooltip = strTooltip & "<tr><td align='left' width='80%'>Eco Obsolescence: </td><td align='right'>#qryRecords.economicObsolescencePct#</td></tr>">
		<cfset strTooltip = strTooltip & "<tr><td align='left'>Func Obsolescence: </td><td align='right'>#qryRecords.functionalObsolescencePct#</td></tr>">
		<cfset strTooltip = strTooltip & "<tr><td align='left'>Special Condition Pct: </td><td align='right'>#qryRecords.specialConditionPct#</td></tr>">

	</cfif>


	<cfif Len(Trim(strTooltip)) NEQ 0>
		<cfset strTooltip = "<table width='225'>" & strTooltip & "</table>">
	</cfif>

	</cfoutput>

	<cfreturn strTooltip>

</cffunction>
